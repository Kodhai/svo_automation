Feature: Demo Scripts for Additional Vehicle

  @SSEC_631 @Demo
  Scenario: Script1 User is able to add multiple vehicles to a single opportunity
    Given Access to SVO Portal with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    #Given User Access SVO Portal for Specific User Role
    When User navigates to Opportunity tab
    And User creates new SVO Bespoke Opportunity with mandatory fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User navigates to Additional Vehicle Link from Quick Links
    And User adds one Vehicle Record with mandatory fields like Brand "Additional_Vehicles,A,3", Model "Additional_Vehicles,B,3", Series Name "Additional_Vehicles,C,3", Derivative "Additional_Vehicles,D,3" and Model Year "Additional_Vehicles,E,3"
    Then User clicks on Save & New button to add one more vehicle record
    And User adds one Vehicle Record with fields like Brand "Additional_Vehicles,A,3", Model "Additional_Vehicles,B,3", Series Name "Additional_Vehicles,C,3", Derivative "Additional_Vehicles,D,3" and Model Year "Additional_Vehicles,E,3"
    Then User navigates to Additional Vehicle hyperlink from Related Quick Links
    And User verifies that multiple vehicles are created under Additional Vehicle section

  #Then Logout from SVO Portal
  @SSEC_624 @Demo
  Scenario: Script2 Additional Vehicle section is present in related quick links list of a closed Bespoke Opportunity
    #Given Access to SVO Portal with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Given User Access SVO Portal for Specific User Role
    When User navigates to Opportunity tab
    And User creates new SVO Bespoke Opportunity with mandatory fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User closes the Opportunity
    Then User navigates to Additional Vehicle Link from Quick Links
    And User adds one Vehicle Record with fields like Brand "Additional_Vehicles,A,3", Model "Additional_Vehicles,B,3", Series Name "Additional_Vehicles,C,3", Derivative "Additional_Vehicles,D,3" and Model Year "Additional_Vehicles,E,3"
    Then User navigates to Additional Vehicle Section of an opportunity
    And User verifies that Brand, Model and Series Name is displayed in Mini page
    And User verifies that full vehicle information is displayed in Additional Vehicle page

  #Then Logout from SVO Portal
  @SSEC_634 @Demo
  Scenario: Script3 User is  able to update the vehicle record on order page of an opportunity
    #Given Access to SVO Portal with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Given User Access SVO Portal for Specific User Role
    When User navigates to Opportunity tab
    And User creates new SVO Bespoke Opportunity with mandatory fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User moves the opportunity to Design Brief & Quote stage
    And User moves Oppotunity to Order Placed stage after accepting Quote for Retailer Contact "Opportunity,J,2"
    Then User navigates to Additional Vehicle Link from Quick Links
    And User adds one Vehicle Record with fields like Brand "Additional_Vehicles,A,3", Model "Additional_Vehicles,B,3", Series Name "Additional_Vehicles,C,3", Derivative "Additional_Vehicles,D,3" and Model Year "Additional_Vehicles,E,3"
    Then User navigates to Additional Vehicle hyperlink from Related Quick Links
    And User verifies that single vehicle is created under Additional Vehicle section
    Then User navigates to Orders page for the same Opportunity for Verification
    Then User navigates to Additional Vehicle Section of an opportunity from Orders Page
    Then User edits the vehicle record from Orders page which is created like Model "Additional_Vehicles,B,2" and Model Year "Additional_Vehicles,E,2"
    And Verify that same information flows to an Opportunity

  #Then Logout from SVO Portal
  @SSEC_629 @Demo
  Scenario: Script4 Additional Vehicle section/hyperlink is not present for any Private Office Opportunity
    #Given Access to SVO Portal with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Given User Access SVO Portal for Specific User Role
    When User navigates to Opportunity tab
    And User opens an opportunity details for Private Office Opportunity
    Then User verifies that an Additional vehicle section or hyperlink is not present

  #Then Logout from SVO Portal
  @SSEC_630 @Demo
  Scenario: Script5 User is not able to view Additional Vehicle section under files section of an Orders page for Private Office Opportunity
    #Given Access to SVO Portal with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Given User Access SVO Portal for Specific User Role
    When User navigates to Opportunity tab
    And User opens an opportunity details for Private Office Opportunity which is in "Order Placed" stage
    Then User navigates to Orders page for the same Opportunity
    Then User verifies that an Additional vehicle section or hyperlink is not present
    Then Logout from SVO Portal

  @SSEC_637 @Demo
  Scenario: Script6 User is not able to add new vehicle record to an opportunity under Additional vehicle section
    Given Access to SVO Portal with Private Office Username "LoginUsers,B,4" and Password "LoginUsers,C,4"
    When User navigates to Opportunity tab
    And User opens an opportunity details for SVO Bespoke Opportunity
    Then User navigates to Additional Vehicle Link from Quick Links
    Then User verifies that user is not able to create Additional vehicle Record for an opportunity
    Then Logout from SVO Portal
