@tag
Feature: Script to add Case Management-Queues, Groups and Sharing Rules Setup

  @CM1
  Scenario: User is able to create Classic Sales Queue
    Given Access to SVO portal as Username "LoginUsers,B,5" and password "LoginUsers,C,5"
    And user navigate to Cases tab
    Then User creates Classic Sales record type with mandatory fields like first name
    Then User saves the new case
    And Verify that Case Owner is Classic Sales Queue
    Then user logs a call on the activity tab
    Then User closes the case
    And User logouts from the SVO portal
    
  @CM2
  Scenario: User is able to create Classic Service Queue Case
    Given Access to SVO portal as username "LoginUsers,B,12" and password "LoginUsers,C,12"
    And user navigate to Cases tab
    Then User creates Classic Service record type with mandatory fields like first name
    Then User saves the new case
    And Verify that Case Owner is Classic Service Queue
    Then user logs a call on the activity tab
    Then User closes the case
    And User logouts from the SVO portal
    
  @CM3
  Scenario: User is able to view All Classic Service Queue Case
    Given Access to SVO portal as username "LoginUsers,B,12" and password "LoginUsers,C,12"
    And user navigate to Cases tab
    Then User creates Classic Service record type with mandatory fields like first name
    Then User saves the new case
    And User verify that All Classic Service queues are dispalyed in Cases tab
    And User logouts from the SVO portal
    
  @CM4
  Scenario: User is able to view All Classic Sales Queue Case
    Given Access to SVO portal as Username "LoginUsers,B,5" and password "LoginUsers,C,5"
    And user navigate to Cases tab
    Then User creates Classic Sales record type with mandatory fields like first name
    Then User saves the new case
    And User verify that All Classic Sales queues are dispalyed in Cases tab
    And User logouts from the SVO portal
    
  @CM5
  Scenario: User is able to switch from Classic Sales Queue to Classic Service Queue
    Given Access to SVO portal as Username "LoginUsers,B,5" and password "LoginUsers,C,5"
    And user navigate to Cases tab
    Then User creates Classic Sales record type with mandatory fields like first name
    Then User saves the new case
    And User edits the Case Owner from Classic Sales Queue to Classic Service Queue
    Then User verify that Case Owner is changed to Classic Service Queue
    And User logouts from the SVO portal
    
  @CM6
  Scenario: User is able to switch Queue from the button on the Case page
    Given Access to SVO portal as Username "LoginUsers,B,5" and password "LoginUsers,C,5"
    And user navigate to Cases tab
    Then User creates Classic Sales record type with mandatory fields like first name
    Then User saves the new case
    And Verify that Case Owner is Classic Sales Queue
    Then User clicks on Change Owner button on Cases page at the top
    And User edits the Case Owner from Classic Sales Queue to Classic Service Queue at the top
    And Verify that Case Owner is Classic Service Queue
    And User logouts from the SVO portal
    
  @CM7
  Scenario: User is able to edit Classic Sales Queue Case
    Given Access to SVO portal as Username "LoginUsers,B,5" and password "LoginUsers,C,5"
    And user navigate to Cases tab
    Then User creates Classic Sales record type with mandatory fields like first name
    Then User saves the new case
    And User edits the Case Origin as Email from the Cases page
    Then User saves the edited case
    And User logouts from the SVO portal
    
  @CM8
  Scenario: User is able to edit Classic Service Queue Case
    Given Access to SVO portal as username "LoginUsers,B,12" and password "LoginUsers,C,12"
    And user navigate to Cases tab
    Then User creates Classic Service record type with mandatory fields like first name
    Then User saves the new case
    And User edits the Case Origin as Web from the Cases page
    Then User saves the edited case
    And User logouts from the SVO portal
    
  @CM9
  Scenario: User is unable to remove Case Owner from Classic Service Case
    Given Access to SVO portal as username "LoginUsers,B,12" and password "LoginUsers,C,12"
    And user navigate to Cases tab
    Then User creates Classic Service record type with mandatory fields like first name
    Then User saves the new case
    And Click on edit button of Case Owner and verify that no remove option is available
    Then User logouts from the SVO portal
    







  






  
    
    








  
        
    
    
    
    
    
    
    
  
    
  
