Feature: Script to view Case created in Classic Parts user

  #User is able to create a Classic Customer Service Case on Salesforce
  @SSEC_1239_CaseVisibilityInSF_01
  Scenario: User is able to create a Classic Customer Service Case on Salesforce
    Given Access to SVO portal as Customer Services user with UserName "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User navigates to Cases tab on SVO portal
    And User creates new Customer Services case with all mandatory fields
    Then User Saves the case
    Then User Validates new cust case
    And User Logout from the portal

  #User is able to create Parts and Technical Case through Web
  @SSEC_1239_CaseVisibilityInSF_02
  Scenario: User is able to create Parts and Technical Case through Web
    Given Access to the Classic Parts "CPData,G,2" web portal
    Then Navigate to Contact Us Section
    Then Submit Contact Us form with details like Email "ClassicPartsContactDet,B,2" First name "ClassicPartsContactDet,C,2" Last name "ClassicPartsContactDet,D,2" Country "ClassicPartsContactForm,I,2" Phone number "ClassicPartsContactDet,H,3" Enquiry Type "ClassicPartsContactForm,S,2" VIN number "ClassicPartsContactForm,J,2"
    And Logout from the browser
    Then User Access to SVO Portal as Classic Parts and technical user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    Then User navigates to Cases tab
    And Filters the Cases as All Cases and Searches the CaseNumber copied
    And Verify the Case origin Case owner and Web Email
    And User Logout from the portal

  #User is able to create Parts and Technical Case through Email
  @SSEC_1239_CaseVisibilityInSF_03
  Scenario: User is able to create Parts and Technical Case through Email
    Given Login to gmail account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    When Customer sends a Case creation mail to user "CaseCreation,A,2"
    And Logout from the user Email account
    Then Verify User receives an outlook email Notification on Parts and Technical case creation
    Given User Access to SVO Portal as Classic Parts and technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User navigates to Cases tab
    And Verify that Parts and Technical case is automatically created on SVO Portal of Classic Parts
    And Verify that Case Origin is Email
    And User Logout from the portal

  @CC4 @SSEC-1991
  Scenario: User is not able to create Classic Customer Case on SVO portal
    Given Access to Classic Operations with Username "LoginUsers,B,9" and password "LoginUsers,C,9"
    And User Verifies that no Cases tab is present
    Then User Logout from the portal

  @CC5 @SSEC-1992
  Scenario: User is not able to create Classic Parts and Technical Case on SVO portal
    Given Access to Classic Finance with Username "LoginUsers,B,10" and password "LoginUsers,C,10"
    And User Verifies that no Cases tab is present
    Then User Logout from the portal

  @CC6 @SSEC-1993
  Scenario: User is not able to view Classic Customer Case on SVO Portal
    Given Access to Classic Sales with Username "LoginUsers,B,5" and password "LoginUsers,C,5"
    And User Verifies that no Cases tab is present
    Then User Logout from the portal

  #User is not able to create Classic Customer Case on SVO portal
  @SSEC_1239_CaseVisibilityInSF_04
  Scenario: User is not able to create Classic Customer Case on SVO portal
    Given Access to Classic Operations with Username "LoginUsers,B,9" and password "LoginUsers,C,9"
    And User Verifies that no Cases tab is present
    Then User Logout from the portal

  #User is not able to create Classic Parts and Technical Case on SVO portal
  @SSEC_1239_CaseVisibilityInSF_05
  Scenario: User is not able to create Classic Parts and Technical Case on SVO portal
    Given Access to Classic Finance with Username "LoginUsers,B,10" and password "LoginUsers,C,10"
    And User Verifies that no Cases tab is present
    Then User Logout from the portal

  #User is not able to view Classic Customer Case on SVO Portal
  @SSEC_1239_CaseVisibilityInSF_06
  Scenario: User is not able to view Classic Customer Case on SVO Portal
    Given Access to Classic Sales with Username "LoginUsers,B,5" and password "LoginUsers,C,5"
    And User Verifies that no Cases tab is present
    Then User Logout from the portal
