Feature: Secondary Owner able to receive an email stating Contract has been signed

  @EmailNotificationContractSigned_01 @Batch @SSEC-
  Scenario: Secondary Owner is able to receive an email notification when contract is signed for Classic Limited Edition Opportunity
   	Given Access to SVO Portal as Classic Admin user with Username "LoginUsers,B,13" and Password "LoginUsers,C,13"
		When User navigates to Opportunity tab
    And User creates Classic Limitted Edition Opportunity with all fields like Stage "Opportunity,B,2" Record Type "OpportunitySecondaryOwner,B,5" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" SAP Account "Opportunity,Q,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Programme "BillingAddress,B,3" Model "Opportunity,M,2"
    Then Verify that opportunity is in Qualified stage
    And User adds user "AdditionalOwner,A,5" as secondary owner for an classic opportunity
    And User moves the opportunity to Contracting stage	
    Then User sends contract for Signature using Send Sales Agreement 
    And User signs the Contract by logging in UserName "LoginUsers,B,18" and Password "LoginUsers,C,18" 
    Then User verifies that Seconday owner receives an email stating the contract has been signed
    Then Logout from the Special vehicle operations Portal
    	
 	@EmailNotificationContractSigned_02 @Batch @SSEC-	
  Scenario: An email notification should be received by Secondary Owner for Classic Works Legend Opportunity
    Given Access to SVO Portal as classic Sales user with Username "LoginUsers,B,5" and Password "LoginUsers,C,5"
		When User navigates to Opportunity tab
    And User creates Classic Works Legend Opportunity with fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    And User adds user "LoginUsers,B,13" as secondary owner for an classic opportunity
    And User moves the opportunity to Contracting stage
    Then User sends contract for Signature using Send Sales Agreement 
    And User signs the Contract by logging in UserName "LoginUsers,B,18" and Password "LoginUsers,C,18" 
    Then User verifies that Seconday owner receives an email stating the contract has been signed
    Then Logout from the Special vehicle operations Portal
    
  @EmailNotificationContractSigned_03 @Batch @SSEC-
  Scenario: User is able to edit cc person while sending contract for signature for Classic Bespoke Opportunity
    Given Access to SVO Portal as classic Sales user with Username "LoginUsers,B,5" and Password "LoginUsers,C,5"
		When User navigates to Opportunity tab
  	And User creates an Classic Bespoke Opportunity with all fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    And User adds user "LoginUsers,B,10" as secondary owner for an classic opportunity
    And User moves the opportunity to Contracting stage
    Then User sends contract for Signature using Send Sales Agreement after editing CC email address to "LoginUsers,B,5"
    And User signs the Contract by logging in UserName "LoginUsers,B,18" and Password "LoginUsers,C,18" 
    Then User verifies that Seconday owner receives an email stating the contract has been signed
    Then Logout from the Special vehicle operations Portal 
    	
  @EmailNotificationContractSigned_04 @Batch @SSEC-	
  Scenario: An email address written in cc should be able to receive an email notification when contract is signed
    Given Access to SVO Portal as Classic Admin user with Username "LoginUsers,B,13" and Password "LoginUsers,C,13"
		When User navigates to Opportunity tab
    And User creates an Classic Bespoke Opportunity with all fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    And User adds user "LoginUsers,B,10" as secondary owner for an classic opportunity
    And User moves the opportunity to Contracting stage
    Then User sends contract for Signature using Send Sales Agreement after editing CC email address to "LoginUsers,B,4"
    And User signs the Contract by logging in UserName "LoginUsers,B,18" and Password "LoginUsers,C,18" 
    Then User verifies that CC receipent receives an email stating the contract has been signed
    Then Logout from the Special vehicle operations Portal 
    
  @EmailNotificationContractSigned_05 @Batch @SSEC-
  Scenario: Secondary Owner is not able to receive an email notification if secondary owner is not added for Classic Reborn Opportunity
    Given Access to SVO Portal as classic Sales user with Username "LoginUsers,B,5" and Password "LoginUsers,C,5"
		When User navigates to Opportunity tab
    And User create a Classic Reborn Opportunity with all fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    And User moves the opportunity to Contracting stage
    Then User sends contract for Signature using Send Sales Agreement 
    And User signs the Contract by logging in UserName "LoginUsers,B,18" and Password "LoginUsers,C,18" 
    Then Verify that secondary owner does not receives an email stating the contract has been signed
    Then Logout from the Special vehicle operations Portal
    
  @EmailNotificationContractSigned_06 @Batch @SSEC-	
  Scenario: An email notification should not be received by Secondary Owner for Classic Continuation Opportunity
   	Given Access to SVO Portal as Classic Admin user with Username "LoginUsers,B,13" and Password "LoginUsers,C,13"
		When User navigates to Opportunity tab
    And User create a Classic Continuation Opportunity with all fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    And User adds user "LoginUsers,B,12" as secondary owner for an classic opportunity
    And User moves the opportunity to Contracting stage
    Then User sends contract for Signature using Send Sales Agreement 
    And User signs the Contract by logging in UserName "LoginUsers,B,18" and Password "LoginUsers,C,18" 
    Then Verify that secondary owner does not receives an email stating the contract has been signed
    Then Logout from the Special vehicle operations Portal
    	
  @EmailNotificationContractSigned_07 @Batch @SSEC-
  Scenario: User is not able to receive Contract Signed email notification for Classic Service Opportunity
   	Given Access to SVO Portal with Classic Service user with the Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
		When User navigates to Opportunity tab
    And User creates an Classic Bespoke Opportunity with all fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User is not able to add Secondary owner thus cannot validate email notification
    Then Logout from the Special vehicle operations Portal	
    
  @EmailNotificationContractSigned_08 @Batch @SSEC-
  Scenario: A Contract cannot be sent for signature, if email address written in cc is invalid for any Classic Opportunity
   	Given Access to SVO Portal by super uat user with Username "LoginUsers,B,2" and Password "LoginUsers,C,2"
  	Then User navigates to the Opportunity tab
   	And User create a Classic Works Legend Opportunity with fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    And User adds user "LoginUsers,B,12" as secondary owner for an classic opportunity
    And User moves the opportunity to Contracting stage
    Then User tries to send contract to Invalid email address for Signature using Send Sales Agreement 
    And Verify an error occurred when user sends contract for signature
    Then Logout from the Special vehicle operations Portal
    		