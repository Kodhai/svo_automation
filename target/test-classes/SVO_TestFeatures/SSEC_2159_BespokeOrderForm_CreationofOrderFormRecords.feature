@SSEC_2159
Feature: Validate Creation of Order form for Bespoke user

  @SSEC_2159_01 @sonica
  Scenario: User is able to create a Design Brief record automatically when Quote is created in Salesforce through Opportunity
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User creates a Bespoke Opportunity with check cleared like Stage "Opportunity,B,3" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,I,3" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,3"
    Then User verifies that new Opportunity is created
    Then User navigates to Quotes from Related Links
    And User creates new Bespoke Design Brief quotes "Opportunity,J,4" with mandatory details
    Then Verify that new Bespoke Design Brief record is created
    Then User Logout from SVO Portal

  @SSEC_2159_02 @sonica
  Scenario: Bespoke User is able to Submit the Design Brief record created in Quotes tab
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User creates a Bespoke Opportunity with check cleared like Stage "Opportunity,B,3" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,I,3" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,3"
    Then User verifies that new Opportunity is created
    Then User navigates to Quotes from Related Links
    And User creates new Bespoke Design Brief quotes "Opportunity,J,4" with mandatory details
    Then User verifies that new quote is created
    Then Navigate to newly created Bespoke Design Brief quote then navigate to Design Brief in the related quick links
    And Click on Design Brief quote name and click on Submit button
    Then Click on return render pack button
    Then User Logout from SVO Portal

  @SSEC_2159_03 @sonica
  Scenario: Bespoke User is able to create an Order in Quotes page
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User creates a Bespoke Opportunity with check cleared like Stage "Opportunity,B,3" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,I,3" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,3"
    Then User verifies that new Opportunity is created
    Then User navigates to Quotes from Related Links
    And User creates new Bespoke Design Brief quotes "Opportunity,J,4" with mandatory details
    Then User verifies that new quote is created
    Then Navigate to newly created Bespoke Design Brief quote then navigate to Design Brief in the related quick links
    And Click on Design Brief quote name and click on Submit button
    Then Click on return render pack button
    And Navigate to Orders tab
    And User fills all the mandatory details for a Order record and Status as Committed to build "Order,A,2"
    And Verify that Order is created
    Then User Logout from SVO Portal

  @SSEC_2159_04 @sonica
  Scenario: User is able to automatically create Order Form  in Salesforce when order is marked Committed to build
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User creates a Bespoke Opportunity with check cleared like Stage "Opportunity,B,3" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,I,3" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,3"
    Then User verifies that new Opportunity is created
    Then User navigates to Quotes from Related Links
    And User creates new Bespoke Design Brief quotes "Opportunity,J,4" with mandatory details
    Then User verifies that new quote is created
    Then Navigate to newly created Bespoke Design Brief quote then navigate to Design Brief in the related quick links
    And Click on Design Brief quote name and click on Submit button
    Then Click on return render pack button
    And Navigate to Orders tab
    And User fills all the mandatory details for a Order record and Status as Committed to build "Order,A,2"
    And Verify that Order is created
    And Verify that Order form is automatically created in the Orders record
    Then User Logout from SVO Portal

  @SSEC_2159_05 @sonica
  Scenario: Bespoke User is able to verify the order form details populated from design brief record
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User creates a Bespoke Opportunity with check cleared like Stage "Opportunity,B,3" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,I,3" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,3"
    Then User verifies that new Opportunity is created
    Then User navigates to Quotes from Related Links
    And User creates new Bespoke Design Brief quotes "Opportunity,J,4" with mandatory details
    Then User verifies that new quote is created
    Then Navigate to newly created Bespoke Design Brief quote then navigate to Design Brief in the related quick links
    And Click on Design Brief quote name and click on Submit button
    Then Click on return render pack button
    And Navigate to Orders tab
    And User fills all the mandatory details for a Order record and Status as Committed to build "Order,A,2"
    And Verify that Order is created
    And Verify that Order form is automatically created in the Orders record
    And Verify that the Order form details are populated from Design Brief details
    Then User Logout from SVO Portal

  @SSEC_2159_06 @sonica
  Scenario: Verify that when Order status is other than Committed to build, no order form is created
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User creates a Bespoke Opportunity with check cleared like Stage "Opportunity,B,3" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,I,3" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,3"
    Then User verifies that new Opportunity is created
    Then User navigates to Quotes from Related Links
    And User creates new Bespoke Design Brief quotes "Opportunity,J,4" with mandatory details
    Then User verifies that new quote is created
    Then Navigate to newly created Bespoke Design Brief quote then navigate to Design Brief in the related quick links
    And Click on Design Brief quote name and click on Submit button
    Then Click on return render pack button
    And Navigate to Orders tab
    And User fills all the mandatory details for a Order record and select status other than Committed to build
    And Verify that Order form is not created in the Orders record
    Then User Logout from SVO Portal
