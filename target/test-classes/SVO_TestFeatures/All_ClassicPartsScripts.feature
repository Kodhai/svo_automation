Feature: Validations on Classic Parts User stories

  #User is able to create new case and logout
  @SSEC_1227_CaseCreation_1 @CustomerService
  Scenario: User is able to create new case and logout
    Given Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    #Given One Time Login to SVO Portal as Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,B,20"
    When User navigates to cases tab
    And User creates new Parts and Technical Record type case with mandatory fields like VIN
    Then User saves the case
    And User validates the new case created with the VIN
    #And User logout from the portal
	
  #User is able to create new customer services case
  @SSEC_1227_CaseCreation_2 @CustomerService
  Scenario: User is able to create new customer services case
    #Given User login to SVO Portal by Classic Customer Service user with Username "LoginUsers, B, 21" and Password "LoginUsers, C, 21"
    Given One Time Login to SVO Portal as Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,B,20"
    When User navigates to cases tab
    And User creates new Customer services case with all mandatory fields
    Then User saves the case
    Then User validates new cust case
    #And User logout from the portal

  #User is able to create multiple cases for same record type
  @SSEC_1227_CaseCreation_3
  Scenario: User is able to create multiple cases for same record type
    Given Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User navigates to cases tab
    And User creates new Parts and Technical Record type case with mandatory fields like VIN
    Then User saves and creates a new case with the same record type
    Then User validates the new parts and technical case
    And User logout from the portal

  #User is able to edit details of the created customer services case
  @SSEC_1227_CaseCreation_4
  Scenario: User is able to edit details of the created customer services case
    #Given User login to SVO Portal by Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    Given One Time Login to SVO Portal as Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,B,20"
    When User navigates to cases tab
    And User creates new Customer services case with all mandatory fields
    Then User saves the case
    And User verifies that the case is editable
    #Then User logout from the portal

  #User cannot create a new parts and technical record type without mandatory field
  @SSEC_1227_CaseCreation_5
  Scenario: User cannot create a new parts and technical record type without mandatory field
    Given Access to SVO Portal with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User navigates to cases tab
    And User creates new Parts and Technical Record type case
    Then User saves the case without filling mandatory field like VIN
    And User verifies that it is unable to save
    And User logout from the portal

  #User cannot create new case if case page is not available
  @SSEC_1227_CaseCreation_6
  Scenario: User cannot create new case if case page is not available
    Given Access to SVO Classic Operations portal with Username "LoginUsers,B,9" and password "LoginUsers,C,9"
    When User searches the Case page in searchbox
    Then User logout from the portal

  #User is not able to delete the created case for Customer services
  @SSEC_1227_CaseCreation_7
  Scenario: User is not able to delete the created case for Customer services
    #Given User login to SVO Portal by Classic Customer Service user with Username "LoginUsers, B, 20" and Password "LoginUsers, C, 20"
    Given One Time Login to SVO Portal as Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,B,20"
    When User navigates to cases tab
    And User creates new Customer services case with all mandatory fields
    Then User saves the case
    And User verifies that the case cannot be deleted
    #Then User logout from the portal

  #User is able to close the Parts & Technical Case created in Salesforce
  @SSEC_1238_Case_Lifecycle_01 @SSEC-1632
  Scenario: User is able to close the Parts & Technical Case created in Salesforce
    Given Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    And User create new case with record type "Case_Lifecycle,B,2"
    Then User close the case with status "Case_Lifecycle,C,2"
    And Verify that case closure status set to be closed under case closure details
    Then Logout from SVO Portal

  #User is able to edit case closure details for newly created Customer Service case
  @SSEC_1238_Case_Lifecycle_02 @SSEC-1660
  Scenario: User is able to edit case closure details for newly created Customer Service case
    #Given Access to SVO Portal as Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    Given One Time Login to SVO Portal as Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,B,20"
    And User create new case with record type "Case_Lifecycle,B,3"
    Then User edits case closure status "Case_Lifecycle,D,2"
    And Verify that updated case closure status details
    #Then Logout from SVO Portal

  #User is able to edit case closure details of case which is in open status
  @SSEC_1238_Case_Lifecycle_03 @SSEC-1661
  Scenario: User is able to edit case closure details of case which is in open status
    Given Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    And User create new case with record type "Case_Lifecycle,B,2"
    Then User move the case to open status
    Then User edits case closure status "Case_Lifecycle,D,3"
    And Verify that updated case closure status details
    Then Logout from SVO Portal

  #User is able to edit case closure details of case which is in 'In Progress' status
  @SSEC_1238_Case_Lifecycle_04 @SSEC-1662
  Scenario: User is able to edit case closure details of case which is in 'In Progress' status
    #Given Access to SVO Portal as Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    Given One Time Login to SVO Portal as Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,B,20"
    And User create new case with record type "Case_Lifecycle,B,3"
    Then User move the case to inprogres status
    Then User edits case closure status "Case_Lifecycle,D,3"
    And Verify that updated case closure status details
    #Then Logout from SVO Portal

  #User is not able to edit case closure status once the case has been closed
  @SSEC_1238_Case_Lifecycle_05 @SSEC-1663
  Scenario: User is not able to edit case closure status once the case has been closed
    Given Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    And User create new case with record type "Case_Lifecycle,B,2"
    Then User close the case with status "Case_Lifecycle,C,2"
    And Verify that case closure status set to be closed under case closure details
    Then User is not able to edit case closure status once the case has been closed status "Case_Lifecycle,D,3"
    Then Logout from SVO Portal

  #User is able to move closed Customer Service case to new status, which is created via Web
  @SSEC_1238_Case_Lifecycle_06 @SSEC-1664
  Scenario: User is able to move closed Customer Service case to new status, which is created via Web
    Given Access to the Classic Parts portal
    Then Submit Contact Us form with Email "Case_Lifecycle,F,2" First name "Case_Lifecycle,G,2" Last name "Case_Lifecycle,H,2" Country "Case_Lifecycle,I,2" Phone number "Case_Lifecycle,J,2" VIN number "Case_Lifecycle,K,2"
    #Given User Access to SVO Portal as Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    Given One Time Login to SVO Portal as Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,B,20"
    And User select case under respective queue "Case_Lifecycle,L,3"
    Then User close the case with status "Case_Lifecycle,C,2"
    And Verify that case closure status set to be closed under case closure details
    Then User is able to move the case back to new lifecycle after closed successfully "Case_Lifecycle,E,2"
    #Then Logout from SVO Portal

  #User is able to edit Parts & Techinical case closure reason after case has been closed, which is created via Email
  @SSEC_1238_Case_Lifecycle_07 @SSEC-1665
  Scenario: User is able to edit Parts & Techinical case closure reason after case has been closed, which is created via Email
    Given Access to mail account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    When Customer send an enquiry mail to user "Case_Lifecycle,F,2"
    Given User Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    And User select customer service case under respective queue "Case_Lifecycle,L,2"
    And Enter VIN number
    Then User close the case with status "Case_Lifecycle,C,4"
    And Verify that case closure status set to be closed under case closure details
    Then User is able to edit case closure reason after case has been closed "Case_Lifecycle,C,2"
    Then Logout from SVO Portal

  #User is not able to create new case without VIN number on salesforce
  @SSEC_1238_Case_Lifecycle_08 @SSEC-1666
  Scenario: User is not able to create new case without VIN number on salesforce
    Given Access to Salesforce Portal as Classic Parts user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    And User not able to create new case without VIN number record type "Case_Lifecycle,B,2"
    Then Logout from SVO Portal

  #User is not able to close the case which is created through email
  @SSEC_1238_Case_Lifecycle_09 @SSEC-1667
  Scenario: User ia not able to close the case which is created through email
    Given Access to mail account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    When Customer send an enquiry mail to user "Case_Lifecycle,F,2"
    Given User Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    And User select case under respective queue "Case_Lifecycle,L,2"
    And Verify that error message while closing the case without VIN number
    Then Logout from SVO Portal

  #User is able to close the case without case closure reason on salesforce
  @SSEC_1238_Case_Lifecycle_10 @SSEC-1668
  Scenario: User is able to close the case without case closure reason on salesforce
    Given Access to Salesforce Portal as Classic Parts user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    And User create new case with record type "Case_Lifecycle,B,2"
    Then User close the case without case closure reason status "Case_Lifecycle,C,4"
    Then Logout from SVO Portal

  #User is able to verify all fields added on 'All cases' List view from Cases page
  @SSEC_1234_CaseListViews_01 @Automation_SSEC-1717
  Scenario: User is able to verify all fields added on 'All cases' List view from Cases page
    #Given Login to the Salesforce Portal by Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    Given One Time Login to SVO Portal as Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,B,20"
    When User Navigate to the Cases Tab
    Then Verify "CaseCreation,M,2" cases list view on cases tab
    And Verify fields like CaseNumber Subject Status CaseOwner Priority added to the list view
    #Then Logouts from the Salesforce Portal

  #User is able to verify all fields added on 'All Open cases' List view from Cases page
  @SSEC_1234_CaseListView_02 @Automation_SSEC-1718
  Scenario: User is able to verify all fields added on 'All Open cases' List view from Cases page
    Given Login to Salesforce Portal by Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User Navigate to the Cases Tab
    Then Verify "CaseCreation,M,3" cases list view on cases tab
    And Verify fields like CaseNumber Subject Status CaseOwner Priority added to the list view
    Then Logouts from the Salesforce Portal

  #User is able to verify all fields added on 'All Closed cases' List view from Cases page
  @SSEC_1234_CaseListView_03 @Automation_SSEC-1719
  Scenario: User is able to verify all fields added on 'All Closed cases' List view from Cases page
    Given Login to Salesforce Portal by Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User Navigate to the Cases Tab
    Then Verify "CaseCreation,M,4" cases list view on cases tab
    And Verify fields like CaseNumber Subject Status CaseOwner Priority added to the list view
    Then Logouts from the Salesforce Portal

  #User is able to verify all fields added on 'Cases Due Closure' List view from Cases page
  @SSEC_1234_CaseListView_04 @Automation_SSEC-1720
  Scenario: User is able to verify all fields added on 'Cases Due Closure' List view from Cases page
    #Given Login to the Salesforce Portal by Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    Given One Time Login to SVO Portal as Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,B,20"
    When User Navigate to the Cases Tab
    Then Verify "CaseCreation,M,5" cases list view on cases tab
    And Verify fields like CaseNumber Subject Status CaseOwner Priority added to the list view
    #Then Logouts from the Salesforce Portal

  #User is able to verify all fields added on 'My Cases' List view from Cases page
  @SSEC_1234_CaseListView_05 @Automation_SSEC-1721
  Scenario: User is able to verify all fields added on 'My Cases' List view from Cases page
    Given Login to Salesforce Portal by Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User Navigate to the Cases Tab
    Then Verify "CaseCreation,M,6" cases list view on cases tab
    And Verify fields like CaseNumber Subject Status CaseOwner Priority added to the list view
    Then Logouts from the Salesforce Portal

  #User is able to verify all fields added on 'Customer Service - All Cases' List view from Cases page
  @SSEC_1234_CaseListView_06 @Automation_SSEC-1722
  Scenario: User is able to verify all fields added on 'Customer Service - All Cases' List view from Cases page
    #Given Login to the Salesforce Portal by Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    Given One Time Login to SVO Portal as Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,B,20"
    When User Navigate to the Cases Tab
    Then Verify "CaseCreation,M,7" cases list view on cases tab
    And Verify fields like CaseNumber Subject Status CaseOwner Priority added to the list view
    #Then Logouts from the Salesforce Portal

  #User is able to verify all fields added on 'Customer Service  My Cases' List view from Cases page
  @SSEC_1234_CaseListView_07 @Automation_SSEC-1723
  Scenario: User is able to verify all fields added on 'Customer Service  My Cases' List view from Cases page
    #Given Login to the Salesforce Portal by Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    Given One Time Login to SVO Portal as Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,B,20"
    When User Navigate to the Cases Tab
    Then Verify "CaseCreation,M,8" cases list view on cases tab
    And Verify fields like CaseNumber Subject Status CaseOwner Priority added to the list view
    #Then Logouts from the Salesforce Portal

  #User is able to verify all fields added on 'Parts & Technical - All Cases' List view from Cases page
  @SSEC_1234_CaseListView_08 @Automation_SSEC-1724
  Scenario: User is able to verify all fields added on 'Parts & Technical - All Cases' List view from Cases page
    Given Login to Salesforce Portal by Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User Navigate to the Cases Tab
    Then Verify "CaseCreation,M,9" cases list view on cases tab
    And Verify fields like CaseNumber Subject Status CaseOwner Priority added to the list view
    Then Logouts from the Salesforce Portal

  #User is able to verify all fields added on 'Parts & Technical -  Cases Due Closure' List view from Cases page
  @SSEC_1234_CaseListView_09 @Automation_SSEC-1725
  Scenario: User is able to verify all fields added on 'Parts & Technical -  Cases Due Closure' List view from Cases page
    Given Login to Salesforce Portal by Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User Navigate to the Cases Tab
    Then Verify "CaseCreation,M,10" cases list view on cases tab
    And Verify fields like CaseNumber Subject Status CaseOwner Priority added to the list view
    Then Logouts from the Salesforce Portal

  #User is able to verify all fields added on 'Parts & Technical  My Cases' List view from Cases page
  @SSEC_1234_CaseListView_10 @Automation_SSEC-1726
  Scenario: User is able to verify all fields added on 'Parts & Technical  My Cases' List view from Cases page
    Given Login to Salesforce Portal by Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User Navigate to the Cases Tab
    Then Verify "CaseCreation,M,11" cases list view on cases tab
    And Verify fields like CaseNumber Subject Status CaseOwner Priority added to the list view
    Then Logouts from the Salesforce Portal

  #User is not able to view All Parts and Technical Closed Cases
  @SSEC_1234_CaseListView_11 @Automation_SSEC-1727
  Scenario: User is not able to view All Parts and Technical Closed Cases .
    #Given Login to the Salesforce Portal by Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    Given One Time Login to SVO Portal as Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,B,20"
    When User Navigate to the Cases Tab
    Then Verify that user is not able to view All Parts and Technical Closed Cases
    #Then Logouts from the Salesforce Portal

  #User is not able to view Cases List view if Cases Page not available.
  @SSEC_1234_CaseListView_12 @Automation_SSEC-1728
  Scenario: User is not able to link Contact to Customer Service case if Cases page is not available
    Given Login to Salesforce Portal with Classic Sales user with the Username "LoginUsers,B,5" and Password "LoginUsers,C,5"
    And Verify that cases tab is not available so that user is not able to view Cases List view
    Then Logouts from the Salesforce Portal

  #User is able to submit a case for Parts & Technical Jaguar/Landrover via sending an email
  @SSEC_1226_CaseCreationEmail_01 @SSEC-1791
  Scenario: User is able to submit a case for Parts & Technical Jaguar/Landrover via sending an email
    Given Access to email account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    When Customer sends case creation mail to user "CaseCreation,A,2"
    And Logout from user email account
    Then Verify User receives an outlook email notification on case creation update
    Given Login to SVO Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User Navigates to Cases Tab
    And Verify automatically created Parts and Technical case on SVO Portal
    Then Verify the Case Origin of created case
    Then User Logouts from the SVO Portal

  #User is not able to submit a case for Customer Service via sending an email
  @SSEC_1226_CaseCreationEmail_02 @SSEC-1792
  Scenario: User is not able to submit a case for Customer Service via sending an email
    Given Access to email account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    When Customer sends case creation mail to user "CaseCreation,A,4"
    And Logout from user email account
    Given Access to SVO Portal as admin with Username "LoginUsers,B,6" and Password "LoginUsers,C,6"
    Then login to SVO portal as customer service user "CaseCreation,B,2" via setup login
    When User Navigates to Cases Tab
    And Verify that Classic Customer Service case is not created on SVO Portal
    Then logout from set up login
    Then User Logouts from the SVO Portal

  #A new Parts & Technical case is not created if invalid email address enters via sending an email
  @SSEC_1226_CaseCreationEmail_03 @SSEC-1794
  Scenario: A new Parts & Technical case is not created if invalid email address enters via sending an email
    Given Access to email account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    When Customer sends case creation mail to invalid user "CaseCreation,A,3"
    And Logout from user email account
    Given Login to SVO Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User Navigates to Cases Tab
    And Verify that parts and technical case is not created on SVO Portal
    Then User Logouts from the SVO Portal

  #User is able to create and view Customer Service case on Salesforce.
  @SSEC_1235_CaseVisibilityInSF_001 @Automation_SSEC-1827
  Scenario: User is able to create and view Customer Service case on Salesforce.
    #Given Login to Salesforce Portal using Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    Given One Time Login to SVO Portal as Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,B,20"
    When User Navigate to Cases Tab on SVO
    And User creates a new case with record type "CaseCreation,C,2" by linking case with existing Account "Opportunity,I,2" and Contact "Opportunity,J,2" details
    Then Verify the Case Origin of created case on SF
    #And User Logouts from the SF SVO Portal

  #User is able to create and view Classic Parts and Technical case on Salesforce
  @SSEC_1235_CaseVisibilityInSF_002 @Automation_SSEC-1828
  Scenario: User is able to create and view Classic Parts and Technical case on Salesforce
    Given User logins to SVO Portal by Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User Navigate to Cases Tab on SVO
    And User creates a new case with record type "CaseCreation,C,3" by linking case with existing Account "Opportunity,I,2" and Contact "Opportunity,J,2" details
    Then Verify the Case Origin of created case on SF
    And User Logouts from the SF SVO Portal

  #User is able to view Classic Parts and Technical case created through email
  @SSEC_1235_CaseVisibilityInSF_003 @Automation_SSEC-1829
  Scenario: User is able to view Classic Parts and Technical case created through email
    Given User logins to email account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    When Customer send an case creation mail to user "CaseCreation,A,2"
    And User logouts from the user email account
    Then Verify User receives an outlook email notification for Parts and Technical case creation
    Given User access to SVO Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User Navigate to Cases Tab on SVO
    And User verifies that Parts and Technical case is automatically created on SVO Portal
    Then Verify the Case Origin of created case on SF
    And User Logouts from the SF SVO Portal

  #User is able to view Classic Customer service case created through Web form
  @SSEC_1235_CaseVisibilityInSF_004 @Automation_SSEC-1830
  Scenario: User is able to view Classic Customer service case created through Web form
    Given User logins to an Classic Parts portal
    Then User Navigates to the Contact Us section
    Then User creates Customer Service Case from web form with Email "CaseCreation,A,2" First name "CaseCreation,G,2" Last name "CaseCreation,H,2" Country "CaseCreation,I,2" Phone number "CaseCreation,K,2" VIN number "CaseCreation,J,2"
    Then Verify that user receives an outlook email notification on Classic Service case creation
    Given User logins to the SVO Portal by Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User Navigate to Cases Tab on SVO
    And User verifies that Classic Customer Service case is automatically created on SVO Portal
    Then Verify the Case Origin of created case on SF
    And User Logouts from the SF SVO Portal

  #User is able to change the record type of an Classic customer service case on Salesforce
  @SSEC_1235_CaseVisibilityInSF_005 @Automation_SSEC-1831
  Scenario: User is able to change the record type of an Classic customer service case on Salesforce
    #Given Login to Salesforce Portal using Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    Given One Time Login to SVO Portal as Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,B,20"
    When User Navigate to Cases Tab on SVO
    And User creates a new case with record type "CaseCreation,C,2" by linking case with existing Account "Opportunity,I,2" and Contact "Opportunity,J,2" details
    Then Verify the Case Origin of created case on SF
    When User change the Record type of created case to "CaseCreation,C,3"
    Then User verifies the case record type on case details page
    #And User Logouts from the SF SVO Portal

  #User is able to edit Parts and Technical case created on Salesforce
  @SSEC_1235_CaseVisibilityInSF_006 @Automation_SSEC-1832
  Scenario: User is able to edit Parts and Technical case created on Salesforce
    Given User logins to SVO Portal by Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User Navigate to Cases Tab on SVO
    And User creates a new case with record type "CaseCreation,C,3" by linking case with existing Account "Opportunity,I,2" and Contact "Opportunity,J,2" details
    Then Verify the Case Origin of created case on SF
    And User edit the created parts and technical details
    Then Verify the updated case details on Salesforce
    And User Logouts from the SF SVO Portal

  #User is not able to able to change the owner of an Classic customer service case to Bespoke user
  @SSEC_1235_CaseVisibilityInSF_007 @Automation_SSEC-1833
  Scenario: User is not able to able to change the owner of an Classic customer service case to Bespoke user
    #Given Login to Salesforce Portal using Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    Given One Time Login to SVO Portal as Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,B,20"
    When User Navigate to Cases Tab on SVO
    And User creates a new case with record type "CaseCreation,C,2" by linking case with existing Account "Opportunity,I,2" and Contact "Opportunity,J,2" details
    Then Verify the Case Origin of created case on SF
    When User tries to change the Owner of created case to "ItemsToApprove,A,3"
    Then Verify an error message caught for updating owner for the case record
    #And User Logouts from the SF SVO Portal

  #User is able to submit Parts and Technical Jaguar Enquiry through web form.
  @SSEC_1230_Case_Assignment_Rules_01 @SSEC-1802
  Scenario: User is able to submit Parts and Technical Jaguar Enquiry through web form.
    Given Access to the Classic Parts portal
    Then Navigate to Contact Us section
    Then Submit Contact Us form with classic parts and technical jaguar enquiry Email "Case_Lifecycle,F,2" First name "Case_Lifecycle,G,2" Last name "Case_Lifecycle,H,2" Country "Case_Lifecycle,I,2" Phone number "Case_Lifecycle,J,2" VIN number "Case_Lifecycle,K,2"
    Given User Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    And Verifies Classic parts and technical case under parts and technical queue
    Then Logout from SVO Portal

  #User is able to submit Parts and Technical Land Rover Enquiry through web form.
  @SSEC_1230_Case_Assignment_Rules_02 @SSEC-1803
  Scenario: User is able to submit Parts and Technical Land Rover Enquiry through web form.
    Given Access to the Classic Parts portal
    Then Navigate to Contact Us section
    Then Submit Contact Us form with classic parts and technical landrover enquiry Email "Case_Lifecycle,F,2" First name "Case_Lifecycle,G,2" Last name "Case_Lifecycle,H,2" Country "Case_Lifecycle,I,2" Phone number "Case_Lifecycle,J,2" VIN number "Case_Lifecycle,K,2"
    Given User Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    And Verifies Classic parts and technical case under parts and technical queue
    Then Logout from SVO Portal

  #User is able to submit Customer Service Enquiry through web form.
  @SSEC_1230_Case_Assignment_Rules_03 @SSEC-1804
  Scenario: User is able to submit Customer Service Enquiry through web form.
    Given Access to the Classic Parts portal
    Then Navigate to Contact Us section
    Then Submit Contact Us form with classic customer service enquiry Email "Case_Lifecycle,F,2" First name "Case_Lifecycle,G,2" Last name "Case_Lifecycle,H,2" Country "Case_Lifecycle,I,2" Phone number "Case_Lifecycle,J,2" VIN number "Case_Lifecycle,K,2"
    Given User Access to SVO Portal as Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    And Verifies Classic customer service case under customer service queue
    Then Logout from SVO Portal

  #User is able to create case through web form by selecting Other.
  @SSEC_1230_Case_Assignment_Rules_04 @SSEC-1805
  Scenario: User is able to create case through web form by selecting Other.
    Given Access to the Classic Parts portal
    Then Navigate to Contact Us section
    Then Submit Contact Us form with Other Email "Case_Lifecycle,F,2" First name "Case_Lifecycle,G,2" Last name "Case_Lifecycle,H,2" Country "Case_Lifecycle,I,2" Phone number "Case_Lifecycle,J,2" VIN number "Case_Lifecycle,K,2"
    Given User Access to SVO Portal as Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    And Verifies Classic customer service case under customer service queue
    Then Logout from SVO Portal

  #User is not able to view case under Parts and Technical queue if Customer Service option is selected while creating case through web form
  @SSEC_1230_Case_Assignment_Rules_05 @SSEC-1806
  Scenario: User is not able to view case under Parts and Technical queue if Customer Service option is selected while creating case through web form
    Given Access to the Classic Parts portal
    Then Navigate to Contact Us section
    Then Submit Contact Us form with classic customer service enquiry Email "Case_Lifecycle,F,2" First name "Case_Lifecycle,G,2" Last name "Case_Lifecycle,H,2" Country "Case_Lifecycle,I,2" Phone number "Case_Lifecycle,J,2" VIN number "Case_Lifecycle,K,2"
    Given User Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    And Verifies Classic customer service case is not present under parts and technical queue
    Then Logout from SVO Portal

  #User is able to create Parts and Technical case through email.
  @SSEC_1230_Case_Assignment_Rules_06 @SSEC-1807
  Scenario: User is able to create Parts and Technical case through email.
    Given Access to mail account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    When Customer send an enquiry mail to user "Case_Lifecycle,F,2"
    Given User Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    And Verifies Classic parts and technical case under parts and technical queue
    Then Logout from SVO Portal

  #User is able to create new Parts and Technical case on salesforce.
  @SSEC_1230_Case_Assignment_Rules_07 @SSEC-1808
  Scenario: User is able to create new Parts and Technical case on salesforce.
    Given Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    And User create new case with record type "Case_Lifecycle,B,2"
    And Verifies Classic parts and technical case under parts and technical queue
    Then Logout from SVO Portal

  #User is able to create new Customer Service case on salesforce.
  @SSEC_1230_Case_Assignment_Rules_08 @SSEC-1809
  Scenario: User is able to create new Customer Service case on salesforce.
    #Given Access to SVO Portal as Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    Given One Time Login to SVO Portal as Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,B,20"
    And User create new case with record type "Case_Lifecycle,B,3"
    And Verifies Classic customer service case under customer service queue
    #Then Logout from SVO Portal

  #Verify that newly created Parts and Technical case is not available under Customer Service queue which is through email
  @SSEC_1230_Case_Assignment_Rules_09 @SSEC-1810
  Scenario: Verify that newly created Parts and Technical case is not available under Customer Service queue which is through email
    Given Access to mail account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    When Customer send an enquiry mail to user "Case_Lifecycle,F,2"
    Given User Access to SVO Portal as Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    And Verifies Classic parts and technical case is not present under customer service queue
    Then Logout from SVO Portal

  #User is not able to view case under Parts and Technical queue if Customer Service option is selected while creating case through web form
  @SSEC_1230_Case_Assignment_Rules_10 @SSEC-1811
  Scenario: User is not able to view case under Parts and Technical queue if Customer Service option is selected while creating case through web form
    Given Access to the Classic Parts portal
    Then Navigate to Contact Us section
    Then Submit Contact Us form with classic customer service enquiry Email "Case_Lifecycle,F,2" First name "Case_Lifecycle,G,2" Last name "Case_Lifecycle,H,2" Country "Case_Lifecycle,I,2" Phone number "Case_Lifecycle,J,2" VIN number "Case_Lifecycle,K,2"
    Given User Access to SVO Portal as Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    And Verifies Classic customer service case is not present under parts and technical queue
    Then Logout from SVO Portal

  #User is able to create Parts and Technical case through email.
  @SSEC_1230_Case_Assignment_Rules_11 @SSEC-1812
  Scenario: User is able to create Parts and Technical case through email.
    Given Access to mail account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    When Customer send an enquiry mail to user "LoginUsers,B,14"
    Given User Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    And Verifies Classic customer service case is not present under parts and technical queue
    Then Logout from SVO Portal

  #User is able to create Classic Parts and Technical Queue Case
  @SSEC_1229_CaseQueue_01 @SSEC-1858
  Scenario: User is able to create Classic Parts and Technical Queue Case
    Given Access to SVO Portal as Classic Parts User with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User Navigates to cases tab
    And User creates Parts and Technical Record type case with mandatory fields like VIN
    Then User saves the Case
    And Verify that Case Owner has value as Parts and Technical Queue
    And Verify that user is able to close case for Parts and Technical Queue
    And User logouts from portal

  #User is able to create Classic Customer Service Queue Case
  @SSEC_1229_CaseQueue_02 @SSEC-1860
  Scenario: User is able to create Classic Customer Service Queue Case
    #Given Access to SVO portal as Customer Services User with UserName "LoginUsers,B,20" and Password "LoginUsers,C,20"
    Given One Time Login to SVO Portal as Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,B,20"
    When User Navigates to cases tab
    And User creates Customer services case with all mandatory fields
    Then User saves the Case
    And Verify that Case Owner has value as Customer Service Queue
    And Verify that user is able to close case for Customer Service Queue
    #And User logouts from portal

  #User is able to view all Customer Service Queue cases
  @SSEC_1229_CaseQueue_03 @SSEC-1862
  Scenario: User is able to view all Customer Service Queue cases
    #Given Access to SVO portal as Customer Services User with UserName "LoginUsers, B, 20" and Password "LoginUsers, C, 20"
    Given One Time Login to SVO Portal as Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,B,20"
    When User Navigates to cases tab
    And User creates Customer services case with all mandatory fields
    Then User saves the Case
    And Verify that user is able to view all the Customer Service Queues in Cases tab
    #And User logouts from portal

  #User is able to view all Parts and Technical cases
  @SSEC_1229_CaseQueue_04 @SSEC-1867
  Scenario: User is able to view all Parts and Technical cases
    Given Access to SVO Portal as Classic Parts User with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User Navigates to cases tab
    And User creates Parts and Technical Record type case with mandatory fields like VIN
    Then User saves the Case
    And Verify that user is able to view all the Parts and Technical Queues in Cases tab
    And User logouts from portal

  #User is able to switch queues for Customer Service Queue case
  @SSEC_1229_CaseQueue_05 @SSEC-1875
  Scenario: User is able to switch queues for Customer Service Queue case
    #Given Access to SVO portal as Customer Services User with UserName "LoginUsers,B,20" and Password "LoginUsers,C,20"
    Given One Time Login to SVO Portal as Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,B,20"
    When User Navigates to cases tab
    And User creates Customer services case with all mandatory fields
    Then User saves the Case
    And User edits the case from Customer Service Queue to Parts and Technical Queue
    And User verifies that Case Owner is Parts and Technical Queue
    #And User logouts from portal

  #User is unable to remove Case Owner from Classic Parts and Technical Case
  @SSEC_1229_CaseQueue_06 @SSEC-1877
  Scenario: User is unable to remove Case Owner from Classic Parts and Technical Case
    Given Access to SVO Portal as Classic Parts User with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User Navigates to cases tab
    And User creates Parts and Technical Record type case with mandatory fields like VIN
    Then User saves the Case
    Then User clicks on edit button of Case Owner of Parts and Technical Queue and verifies that remove Case owner is not present
    And User logouts from portal

  #Submit a case for Customer service enquiry
  @SSEC_1225_CaseCreationWebToCase_01
  Scenario: Submit a case for Customer service enquiry
    Given Access to the Classic Parts portal
    Then Navigate to contact us section
    Then Submit Contact Us form with Email "ClassicPartsContactDet,B,2" First name "ClassicPartsContactDet,C,2" Last name "ClassicPartsContactDet,D,2" Country "CPData,I,2" Phone number "ClassicPartsContactDet,H,3" Enquiry Type "CPData,S,4" VIN number "CPData,J,2"
    And close the browser
    Then User Access to SVO Portal as Classic Parts and technical user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    Then Search the Case number in the Search box
    And Verify the Case Origin Case Owner and Web Email
    And user logout from the portal

  #Submit a case for Parts and Technical enquiry
  @SSEC_1225_CaseCreationWebToCase_02
  Scenario: Submit a case for Parts and Technical enquiry
    Given Access to the Classic Parts portal
    Then Navigate to contact us section
    Then Submit Contact Us form with Email "ClassicPartsContactDet,B,2" First name "ClassicPartsContactDet,C,2" Last name "ClassicPartsContactDet,D,2" Country "CPData,I,2" Phone number "ClassicPartsContactDet,H,3" Enquiry Type "CPData,S,2" VIN number "CPData,J,2"
    And close the browser
    Then User Access to SVO Portal as Classic Parts and technical user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    Then Search the Case number in the Search box
    And Verify the Case Origin Case Owner and Web Email
    And user logout from the portal

  #User is not able to submit a case, if any mandatory field value is missing
  @SSEC_1225_CaseCreationWebToCase_03
  Scenario: User is not able to submit a case, if any mandatory field value is missing
    Given Access to the Classic Parts portal
    Then Navigate to contact us section
    Then Submit Contact Us form with Email "ClassicPartsContactDet,B,2" First name "ClassicPartsContactDet,C,2" Last name "ClassicPartsContactDet,D,2" Country "CPData,I,2" Enquiry Type "CPData,S,2" VIN number "CPData,J,2"
    And Verify that error message while submitting contact us form
    And user logout from the portal

  #User is not able to view created web case in salesforce, if case is not submitted from Classic Parts web portal
  @SSEC_1225_CaseCreationWebToCase_04
  Scenario: User is not able to view created web case in salesforce, if case is not submitted from Classic Parts web portal
    Given Access to the Classic Parts portal
    Then Navigate to contact us section
    Then Submit Contact Us form with Email "ClassicPartsContactDet,B,2" First name "ClassicPartsContactDet,C,2" Last name "ClassicPartsContactDet,D,2" Country "CPData,I,2" Phone number "ClassicPartsContactDet,H,3" Enquiry Type "CPData,S,4" VIN number "CPData,J,2"
    Then User Access to SVO Portal as Classic Parts and technical user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    And User navigates to the Enquiry tab and validate that no new enquiry has been created
    And user logout from the portal

  #User is able to verify '3D Secure' Email Template for Customer Service cases created on Salesforce
  @SSEC_1233_CaseEmailTemplate_001 @Automation_SSEC-1765
  Scenario: User is able to verify '3D Secure' Email Template for Customer Service cases created on Salesforce
    #Given Access to salesforce Portal as an Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    Given One Time Login to SVO Portal as Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,B,20"
    When Navigate to Cases Tab
    And Create a new case on SVO of record type "CaseCreation,C,2" by linking case with existing Account "Opportunity,I,2" and Contact "Opportunity,J,2" details
    Then User Navigate to Activity tab
    And Verify and send an email to customer "LoginUsers,B,17" with template "CaseCreation,N,2" on compose email section
    Then User Logout from an SVO Portal
    When Login to the email account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    And Verify that customer receives an email with the applied template "CaseCreation,N,2"
    #Then User logout from the user email account

  #User is able to verify 'Parts Enquiry' Email Template for Classic Parts and Technical case created through web form
  @SSEC_1233_CaseEmailTemplate_002 @Automation_SSEC-1766
  Scenario: User is able to verify 'Parts Enquiry' Email Template for Classic Parts and Technical case created through web form
    Given Login to the Classic Parts "CaseCreation,L,2" portal
    Then User Navigates to Contact Us section
    Then User creates an Parts and Technical Case from web form with Email "CaseCreation,A,2" First name "CaseCreation,G,2" Last name "CaseCreation,H,2" Country "CaseCreation,I,2" Phone number "CaseCreation,K,2" VIN number "CaseCreation,J,2"
    Then Verify that user receives an outlook email notification on Parts and Technical case creation
    Given Login to the salesforce Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When Navigate to Cases Tab
    And User Verify that Classic Parts and Technical case is automatically created on SVO Portal
    Then User Navigate to Activity tab
    And Verify and send an email to customer "LoginUsers,B,17" with template "CaseCreation,N,4" on compose email section
    Then User Logout from an SVO Portal
    When Login to the email account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    And Verify that customer receives an email with the applied template "CaseCreation,N,2"
    Then User logout from the user email account

  #User is able to verify 'Returns' Email Template for Customer Service case created on Salesforce
  @SSEC_1233_CaseEmailTemplate_003 @Automation_SSEC-1767
  Scenario: User is able to verify 'Returns' Email Template for Customer Service case created on Salesforce
    #Given Access to salesforce Portal as an Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    Given One Time Login to SVO Portal as Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,B,20"
    When Navigate to Cases Tab
    And Create a new case on SVO of record type "CaseCreation,C,2" by linking case with existing Account "Opportunity,I,2" and Contact "Opportunity,J,2" details
    Then User Navigate to Activity tab
    And Verify and send an email to customer "LoginUsers,B,17" with template "CaseCreation,N,3" on compose email section
    Then User Logout from an SVO Portal
    When Login to the email account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    And Verify that customer receives an email with the applied template "CaseCreation,N,3"
    Then User logout from the user email account

  #User is able to verify 'Returns 2' Email Template for Customer Service case created on Salesforce
  @SSEC_1233_CaseEmailTemplate_004 @Automation_SSEC-1768
  Scenario: User is able to verify 'Returns 2' Email Template for Customer Service case created on Salesforce
    #Given Access to salesforce Portal as an Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    Given One Time Login to SVO Portal as Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,B,20"
    When Navigate to Cases Tab
    And Create a new case on SVO of record type "CaseCreation,C,2" by linking case with existing Account "Opportunity,I,2" and Contact "Opportunity,J,2" details
    Then User Navigate to Activity tab
    And Verify and send an email to customer "LoginUsers,B,17" with template "CaseCreation,N,5" on compose email section
    Then User Logout from an SVO Portal
    When Login to the email account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    And Verify that customer receives an email with the applied template "CaseCreation,N,5"
    Then User logout from the user email account

  #User is able to edit the 'Parts Enquiry' Email template for Classic Parts and Technical Case created through email
  @SSEC_1233_CaseEmailTemplate_005 @Automation_SSEC-1769
  Scenario: User is able to edit the 'Parts Enquiry' Email template for Classic Parts and Technical Case created through email
    Given Login to the Classic Parts "CaseCreation,L,2" portal
    Then User Navigates to Contact Us section
    Then User creates an Parts and Technical Case from web form with Email "CaseCreation,A,2" First name "CaseCreation,G,2" Last name "CaseCreation,H,2" Country "CaseCreation,I,2" Phone number "CaseCreation,K,2" VIN number "CaseCreation,J,2"
    Then Verify that user receives an outlook email notification on Parts and Technical case creation
    Given Login to the salesforce Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When Navigate to Cases Tab
    And User Verify that Classic Parts and Technical case is automatically created on SVO Portal
    Then User Navigate to Activity tab
    And Verify and send an email to customer "LoginUsers,B,17" by editing template "CaseCreation,N,4" on compose email section
    Then User Logout from an SVO Portal
    When Login to the email account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    And Verify that customer receives an email with the applied template "CaseCreation,N,2"
    Then User logout from the user email account

  #User is able to remove inserted Email template for Classic customer service case
  @SSEC_1233_CaseEmailTemplate_006 @Automation_SSEC-1770
  Scenario: User is able to remove inserted Email template for Classic customer service case
    #Given Access to salesforce Portal as an Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    Given One Time Login to SVO Portal as Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,B,20"
    When Navigate to Cases Tab
    And Create a new case on SVO of record type "CaseCreation,C,2" by linking case with existing Account "Opportunity,I,2" and Contact "Opportunity,J,2" details
    Then User Navigate to Activity tab
    And Send an email to customer "LoginUsers,B,17" after removing email template "CaseCreation,N,5" on compose email section
    #Then User Logout from an SVO Portal

  #User is not able to send an Email with 'Parts Enquiry' template without mandatory details of email
  @SSEC_1233_CaseEmailTemplate_007 @Automation_SSEC-1771
  Scenario: User is not able to send an Email with 'Parts Enquiry' template without mandatory details of email
    Given Login to the Classic Parts "CaseCreation,L,2" portal
    Then User Navigates to Contact Us section
    Then User creates an Parts and Technical Case from web form with Email "CaseCreation,A,2" First name "CaseCreation,G,2" Last name "CaseCreation,H,2" Country "CaseCreation,I,2" Phone number "CaseCreation,K,2" VIN number "CaseCreation,J,2"
    Then Verify that user receives an outlook email notification on Parts and Technical case creation
    Given Login to the salesforce Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When Navigate to Cases Tab
    And User Verify that Classic Parts and Technical case is automatically created on SVO Portal
    Then User Navigate to Activity tab
    And Verify user is not able to send an email without madatory fields of email to customer "LoginUsers,B,17" with template "CaseCreation,N,4" on compose email section
    Then User Logout from an SVO Portal

  #User is able to link existing Contact and Account to Customer Service Case created on Salesforce
  @SSEC_1228_LinkContactAndAccountTocase_01 @Automation_SSEC-1650
  Scenario: User is able to link existing Contact and Account to Customer Service Case created on Salesforce
    Given Access to SVO Portal as admin by using Username "LoginUsers,B,6" and Password "LoginUsers,C,6"
    Then login to SVO portal by customer service user "CaseCreation,B,2" via setup login
    When User Navigate to Cases Tab
    And Create a new case of record type "CaseCreation,C,2" by linking case with existing Account "Opportunity,I,2" and Contact "Opportunity,J,2" details
    Then Verify that newly created case is linked with existing Account and Contact
    And logout from user login via set up
    Then Logouts from the SVO Portal

  #User is able to link existing Contact and Account to Parts and Technical Case created on Salesforce
  @SSEC_1228_LinkContactAndAccountTocase_02 @Automation_SSEC-1651
  Scenario: User is able to link existing Contact and Account to Parts and Technical Case created on Salesforce
    Given Login to SVO Portal by Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User Navigate to Cases Tab
    And Create a new case of record type "CaseCreation,C,3" by linking case with existing Account "Opportunity,I,2" and Contact "Opportunity,J,2" details
    Then Verify that newly created case is linked with existing Account and Contact
    Then Logouts from the SVO Portal

  #User is able to link new contact to the Customer Service case created on Salesforce
  @SSEC_1228_LinkContactAndAccountTocase_03 @Automation_SSEC-1652
  Scenario: User is able to link new contact to the Customer Service case created on Salesforce
    #Given Login to SVO Portal by Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    Given One Time Login to SVO Portal as Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,B,20"
    And User Navigate to Contacts Tab
    Then Create a new contact with contact name "CaseCreation,D,2" and AccountName "CaseCreation,E,2"
    And Verify new contact is created on salesforce
    When User Navigate to Cases Tab
    And Create a new case of record type "CaseCreation,C,2" by linking case with newly created Contact
    Then Verify that case is linked with newly created contact along with autopopulated Account field
    #Then Logouts from the SVO Portal

  #User is able to link existing Contact and Account to Parts and Technical Case created through Email
  @SSEC_1228_LinkContactAndAccountTocase_04 @Automation_SSEC-1653
  Scenario: User is able to link existing Contact and Account to Parts and Technical Case created through Email
    Given Login to email account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    When Customer sends a case creation mail to user "CaseCreation,A,2"
    And Logout from the user email account
    Then Verify User receives an outlook email notification on Parts and Technical case creation
    Given Access to SVO Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User Navigate to Cases Tab
    And Verify that Parts and Technical case is automatically created on SVO Portal
    Then Verify that newly created case is linked with existing Account and Contact
    Then Logouts from the SVO Portal

  #User is not able to link existing Contact for Customer Service Case as it is not available to create through Email
  @SSEC_1228_LinkContactAndAccountTocase_05 @Automation_SSEC-1715
  Scenario: User is not able to link existing Contact for Customer Service Case as it is not available to create through Email
    Given Login to email account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    When Customer sends a case creation mail to user "CaseCreation,A,2"
    And Logout from the user email account
    Given Login to the SVO Portal by Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User Navigate to Cases Tab
    And Verify that Classic Customer Service case is not created on SVO Portal
    Then Logouts from the SVO Portal

  #User is able to link existing Contact and Account to Parts and Technical Case created through Web
  @SSEC_1228_LinkContactAndAccountTocase_06 @Automation_SSEC-1654
  Scenario: User is able to link existing Contact and Account to Parts and Technical Case created through Web
    Given Login to an Classic Parts portal
    Then Navigate to Contact Us section
    Then Create Parts and Technical Case from web form with Email "CaseCreation,A,2" First name "CaseCreation,G,2" Last name "CaseCreation,H,2" Country "CaseCreation,I,2" Phone number "CaseCreation,K,2" VIN number "CaseCreation,J,2"
    Then Verify User receives an outlook email notification on Parts and Technical case creation
    Given Access to SVO Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User Navigate to Cases Tab
    And Verify that Parts and Technical case is automatically created on SVO Portal
    Then Verify that newly created case through web is linked with existing Account and Contact
    Then Logouts from the SVO Portal

  #User is able to link existing Contact and Account to Customer Service Case created through Web
  @SSEC_1228_LinkContactAndAccountTocase_07 @Automation_SSEC-1655
  Scenario: User is able to link existing Contact and Account to Customer Service Case created through Web.
    Given Login to an Classic Parts portal
    Then Navigate to Contact Us section
    Then Create Customer Service Case from web form with Email "CaseCreation,A,2" First name "CaseCreation,G,2" Last name "CaseCreation,H,2" Country "CaseCreation,I,2" Phone number "CaseCreation,K,2" VIN number "CaseCreation,J,2"
    Then Verify User receives an outlook email notification on Classic Service case creation
    Given Login to the SVO Portal by Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User Navigate to Cases Tab
    And Verify that Classic Customer Service case is automatically created on SVO Portal
    Then Verify that newly created case through web is linked with existing Account and Contact
    Then Logouts from the SVO Portal

  #User is able to link new contact to Parts and Technical case created through Email
  @SSEC_1228_LinkContactAndAccountTocase_08 @Automation_SSEC-1656
  Scenario: User is able to link new contact to Parts and Technical case created through Email
    Given Login to email account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    When Customer sends a case creation mail to user "CaseCreation,A,2"
    And Logout from the user email account
    Then Verify User receives an outlook email notification on Parts and Technical case creation
    Given Access to SVO Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    And User Navigate to Contacts Tab
    Then Create a new contact with contact name "CaseCreation,D,2" and AccountName "CaseCreation,E,2"
    And Verify new contact is created on salesforce
    When User Navigate to Cases Tab
    And Verify that Parts and Technical case is automatically created on SVO Portal
    When User link the case with newly created contact
    And Verify that case is linked with newly created contact along with autopopulated Account field
    Then Logouts from the SVO Portal

  #User is able to link new contact to Parts and Technical case created through web
  @SSEC_1228_LinkContactAndAccountTocase_09 @Automation_SSEC-1657
  Scenario: User is able to link new contact to Parts and Technical case created through web
    Given Login to an Classic Parts portal
    Then Navigate to Contact Us section
    Then Create Parts and Technical Case from web form with Email "CaseCreation,A,2" First name "CaseCreation,G,2" Last name "CaseCreation,H,2" Country "CaseCreation,I,2" Phone number "CaseCreation,K,2" VIN number "CaseCreation,J,2"
    Then Verify User receives an outlook email notification on Parts and Technical case creation
    Given Access to SVO Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    And User Navigate to Contacts Tab
    Then Create a new contact with contact name "CaseCreation,D,2" and AccountName "CaseCreation,E,2"
    And Verify new contact is created on salesforce
    When User Navigate to Cases Tab
    And Verify that Parts and Technical case is automatically created on SVO Portal
    When User link the case with newly created contact
    And Verify that case is linked with newly created contact along with autopopulated Account field
    Then Logouts from the SVO Portal

  #User is able to link new contact to Customer Service case created through web
  @SSEC_1228_LinkContactAndAccountTocase_10 @Automation_SSEC-1658
  Scenario: User is able to link new contact to Customer Service case created through web
    Given Login to an Classic Parts portal
    Then Navigate to Contact Us section
    Then Create Customer Service Case from web form with Email "CaseCreation,A,2" First name "CaseCreation,G,2" Last name "CaseCreation,H,2" Country "CaseCreation,I,2" Phone number "CaseCreation,K,2" VIN number "CaseCreation,J,2"
    Then Verify User receives an outlook email notification on Classic Service case creation
    Given Login to the SVO Portal by Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    And User Navigate to Contacts Tab
    Then Create a new contact with contact name "CaseCreation,D,2" and AccountName "CaseCreation,E,2"
    And Verify new contact is created on salesforce
    When User Navigate to Cases Tab
    And Verify that Classic Customer Service case is automatically created on SVO Portal
    When User link the case with newly created contact
    Then Verify that case is linked with newly created contact along with autopopulated Account field
    Then Logouts from the SVO Portal

  #User is not able to link Contact to Customer Service case if Cases page is not available
  @SSEC_1228_LinkContactAndAccountTocase_11 @Automation_SSEC-1659
  Scenario: User is not able to link Contact to Customer Service case if Cases page is not available
    Given Login to SVO Portal with Classic Sales user with the Username "LoginUsers,B,5" and Password "LoginUsers,C,5"
    And Verify that cases tab is not available so that user is not able to link Contact to Customer Service case
    Then Logouts from the SVO Portal

  #User is able to create a Classic Customer Service Case on Salesforce
  @SSEC_1239_CaseVisibilityInSF_01
  Scenario: User is able to create a Classic Customer Service Case on Salesforce
    #Given Access to SVO portal as Customer Services user with UserName "LoginUsers,B,20" and Password "LoginUsers,C,20"
    Given One Time Login to SVO Portal as Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,B,20"
    When User navigates to Cases tab on SVO portal
    And User creates new Customer Services case with all mandatory fields
    Then User Saves the case
    Then User Validates new cust case
    #And User Logout from the portal

  #User is able to create Parts and Technical Case through Web
  @SSEC_1239_CaseVisibilityInSF_02
  Scenario: User is able to create Parts and Technical Case through Web
    Given Access to the Classic Parts "CPData,G,2" web portal
    Then Navigate to Contact Us Section
    Then Submit Contact Us form with details like Email "BGData,B,2" First name "BGData,C,2" Last name "BGData,D,2" Country "CPData,I,2" Phone number "BGData,H,3" Enquiry Type "CPData,S,2" VIN number "CPData,J,2"
    And Logout from the browser
    Then User Access to SVO Portal as Classic Parts and technical user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    Then User navigates to Cases tab
    And Filters the Cases as All Cases and Searches the CaseNumber copied
    And Verify the Case origin Case owner and Web Email
    And User Logout from the portal

  #User is able to create Parts and Technical Case through Email
  @SSEC_1239_CaseVisibilityInSF_03
  Scenario: User is able to create Parts and Technical Case through Email
    Given Login to gmail account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    When Customer sends a Case creation mail to user "CaseCreation,A,2"
    And Logout from the user Email account
    Then Verify User receives an outlook email Notification on Parts and Technical case creation
    Given User Access to SVO Portal as Classic Parts and technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User navigates to Cases tab
    And Verify that Parts and Technical case is automatically created on SVO Portal of Classic Parts
    And Verify that Case Origin is Email
    And User Logout from the portal

  #User is not able to create Classic Customer Case on SVO portal
  @SSEC_1239_CaseVisibilityInSF_04
  Scenario: User is not able to create Classic Customer Case on SVO portal
    Given Access to Classic Operations with Username "LoginUsers,B,9" and password "LoginUsers,C,9"
    And User Verifies that no Cases tab is present
    Then User Logout from the portal

  #User is not able to create Classic Parts and Technical Case on SVO portal
  @SSEC_1239_CaseVisibilityInSF_05
  Scenario: User is not able to create Classic Parts and Technical Case on SVO portal
    Given Access to Classic Finance with Username "LoginUsers,B,10" and password "LoginUsers,C,10"
    And User Verifies that no Cases tab is present
    Then User Logout from the portal

  #User is not able to view Classic Customer Case on SVO Portal
  @SSEC_1239_CaseVisibilityInSF_06
  Scenario: User is not able to view Classic Customer Case on SVO Portal
    Given Access to Classic Sales with Username "LoginUsers,B,5" and password "LoginUsers,C,5"
    And User Verifies that no Cases tab is present
    Then User Logout from the portal

  #User is able to close newly created Customer Service Case which is created via Salesforce
  @SSEC_1237_CloseCase_01
  Scenario: User is able to close newly created Customer Service Case which is created via Salesforce
    #Given Access to SVO Portal as Classic Customer Service user with Username "LoginUsers,B,21" and Password "LoginUsers,C,21"
    Given One Time Login to SVO Portal as Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,B,20"
    When User navigates to Cases tab
    And User creates new Customer Service record type with Case Origin as Salesforce
    And User saves the customer service case
    Then user closes the case with reason as Question Answered
    Then User logouts from the SF SVO

  #User is able to close newly created Parts & Technical Case which is created via Email
  @SSEC_1237_CloseCase_02
  Scenario: User is able to close newly created Parts & Technical Case which is created via Email
    Given Access to SVO Portal as Classic Parts and technical user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User navigates to Cases tab
    And User creates new Parts and technical record type with Case Origin as Email
    And User saves the Parts and Technical case
    Then user closes the case with reason as Vin not Eligible
    Then User logouts from the SF SVO

  #User is able to close newly created Customer Service Case which is created via Website
  @SSEC_1237_CloseCase_03
  Scenario: User is able to close newly created Customer Service Case which is created via Website
    #Given Access to SVO Portal as Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    Given One Time Login to SVO Portal as Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,B,20"
    When User navigates to Cases tab
    And User creates new Customer Service record type with Case Origin as Website
    And User saves the customer service case
    Then user closes the case with reason as Transferred to Another Department
    Then User logouts from the SF SVO

  #User is able to close newly created Parts & Technical Case which is created via Web
  @SSEC_1237_CloseCase_04
  Scenario: User is able to close newly created Parts & Technical Case which is created via Web
    Given Access to SVO Portal as Classic Parts and technical user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User navigates to Cases tab
    And User creates new Parts and technical record type with first name, last name and Case Origin as Website and VIN
    And User saves the Parts and Technical case
    Then user closes the case with reason as Part not available
    Then User logouts from the SF SVO

  #User is able to close newly created Parts & Technical Case which is created via Salesforce
  @SSEC_1237_CloseCase_05
  Scenario: User is able to close newly created Parts & Technical Case which is created via Salesforce
    Given Access to SVO Portal as Classic Parts and technical user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User navigates to Cases tab
    And User creates new Parts and technical record type with first name, last name and Case Origin as Salesforce and VIN
    And User saves the Parts and Technical case
    Then user closes the case with reason as Other
    Then User logouts from the SF SVO

  #User is not able to Close Parts & Technical Case without filling the mandatory details
  @SSEC_1237_CloseCase_06
  Scenario: User is not able to Close Parts & Technical Case without filling the mandatory details
    Given Access to SVO Portal as Classic Parts and technical user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User navigates to Cases tab
    And User creates new Parts and technical record type with first name, last name and Case Origin as Salesforce and VIN
    And User saves the Parts and Technical case
    Then User removes the mandatory field details and saves the case
    Then user closes the case
    And Verifies that closed case has all the mandatory fields
    Then User logouts from the SF SVO

  #User is able to close newly created Customer Service Case which is created via Salesforce without any reason
  @SSEC_1237_CloseCase_07
  Scenario: User is able to close newly created Customer Service Case which is created via Salesforce without any reason
    #Given Access to SVO Portal as Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    Given One Time Login to SVO Portal as Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,B,20"
    When User navigates to Cases tab
    And User creates new Customer Service record type with Case Origin as Salesforce
    And User saves the customer service case
    Then user closes the case without selecting any reason
    Then User logouts from the SF SVO

  #User is able to receive an automatic email on Monday if Customer service enquiry raised on Friday(Before 5pm)
  @SSEC_1231_ClassicParts_DefineSLA_01
  Scenario: User is able to receive an automatic email on Monday if Customer service enquiry raised on Friday(Before 5pm)
    #Given Access to SVO Portal as Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    Given One Time Login to SVO Portal as Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,B,20"
    And User create new case to define SLA with record type "Case_Lifecycle,B,2"
    Then Collect case number on Friday before five PM
    Then Verify that SLA is breached on salesforce
    #Then User Logout from SVO Portal

  #User is able to receive an automatic email on Tuesday if Parts and Technical enquiry raised on Friday(After 5pm)
  @SSEC_1231_ClassicParts_DefineSLA_02
  Scenario: User is able to receive an automatic email on Tuesday if Parts and Technical enquiry raised on Friday(After 5pm)
    Given Access to SVO Portal as Classic Parts and technical user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    And User create new case with record type "Case_Lifecycle,B,2"
    Then Collect case number on Friday after five PM
    Then Verify that SLA is breached on salesforce
    Then User Logout from SVO Portal

  #User is able to receive an automatic email on Tuesday if Parts and Technical enquiry raised on Saturday(Anytime)
  @SSEC_1231_ClassicParts_DefineSLA_03
  Scenario: User is able to receive an automatic email on Tuesday if Parts and Technical enquiry raised on Saturday(Anytime)
    Given Access to mail account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    When Customer send an enquiry mail to user "Case_Lifecycle,F,2"
    Given User Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    And Verifies Classic parts and technical case under parts and technical queue
    And Collect case number on Saturday anytime
    Then Verify that SLA is breached on salesforce
    Then User Logout from SVO Portal

  #User is able to receive an automatic email on Tuesday if Customer Service enquiry raised on Sunday(Anytime)
  @SSEC_1231_ClassicParts_DefineSLA_04
  Scenario: User is able to receive an automatic email on Tuesday if Customer Service enquiry raised on Sunday(Anytime)
    #Given Access to SVO Portal as Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    Given One Time Login to SVO Portal as Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,B,20"
    And User create new case with record type "Case_Lifecycle,B,3"
    And Collect case number on Sunday anytime
    Then Verify that SLA is breached on salesforce
    #Then User Logout from SVO Portal

  #User is able to receive an automatic email within 24 hours if Parts and Technical enquiry raised on Weekday(Before 5PM)
  @SSEC_1231_ClassicParts_DefineSLA_05
  Scenario: User is able to receive an automatic email within 24 hours if Parts and Technical enquiry raised on Weekday(Before 5PM)
    Given Access to SVO Portal as Classic Parts and technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    And User create new case to define SLA with record type "Case_Lifecycle,B,2"
    Then Collect case number on Weekday before five PM
    Then Verify that SLA is breached on salesforce
    Then User Logout from SVO Portal

  #User is able to receive an automatic email if Customer Service enquiry raised on Weekday(After 5PM)
  @SSEC_1231_ClassicParts_DefineSLA_06
  Scenario: User is able to receive an automatic email if Customer Service enquiry raised on Weekday(After 5PM)
    Given Access to SVO Portal as Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    And User create new case with record type "Case_Lifecycle,B,2"
    Then Collect case number on Weekday after five PM
    Then Verify that SLA is breached on salesforce
    Then User Logout from SVO Portal

  #User is not able to receive an automatic email if Parts and Technical case is in closed state.
  @SSEC_1231_ClassicParts_DefineSLA_07
  Scenario: User is not able to receive an automatic email if Parts and Technical case is in closed state.
    Given Access to SVO Portal as Classic Parts and technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    And User create new case with record type "Case_Lifecycle,B,2"
    Then User close the case with status "Case_Lifecycle,C,2"
    And Collect case number which is in closed status
    Then Verify that SLA is breached on salesforce
    Then User Logout from SVO Portal

  #User is able to receive an automatic email if Parts and Technical enquiry raised through web on Friday(Before 5pm)
  @SSEC_1231_ClassicParts_DefineSLA_08
  Scenario: User is able to receive an automatic email if Parts and Technical enquiry raised through web on Friday(Before 5pm)
    Given Access to the Classic Parts portal
    Then Submit Contact Us form with classic customer service enquiry Email "Case_Lifecycle,F,2" First name "Case_Lifecycle,G,2" Last name "Case_Lifecycle,H,2" Country "Case_Lifecycle,I,2" Phone number "Case_Lifecycle,J,2" VIN number "Case_Lifecycle,K,2"
    Given User Access to SVO Portal as Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    And Verifies Classic customer service case under customer service queue
    And Collect case number created via web on Friday before five PM
    Then Verify that SLA is breached on salesforce
    Then User Logout from SVO Portal

  #User is able to receive an automatic email on Tuesday if Parts and Technical enquiry raised on Friday(After 5pm)
  @SSEC_1231_ClassicParts_DefineSLA_09
  Scenario: User is able to receive an automatic email on Tuesday if Parts and Technical enquiry raised on Friday(After 5pm)
    Given Access to the Classic Parts portal
    Then Submit Contact Us form with classic parts and technical landrover enquiry Email "Case_Lifecycle,F,2" First name "Case_Lifecycle,G,2" Last name "Case_Lifecycle,H,2" Country "Case_Lifecycle,I,2" Phone number "Case_Lifecycle,J,2" VIN number "Case_Lifecycle,K,2"
    Given User Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    And Verifies Classic parts and technical case under parts and technical queue
    And Collect case number created via web on Friday after five PM
    Then Verify that SLA is breached on salesforce
    Then User Logout from SVO Portal

  #User is able to verify '3D Secure' Email Template for Customer Service cases created on Salesforce
  @SSEC_1557_ClassicPartsCaseEmailTemplate_001 @SSEC-1936
  Scenario: User is able to verify '3D Secure' Email Template for Customer Service cases created on Salesforce
    Given User Access to the salesforce Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User navigate to Cases Tab
    And User creates a new case on SVO of record type "CaseCreation,C,2" by linking case with existing Account "Opportunity,I,2" and Contact "Opportunity,J,2" details
    Then User Navigates to Activity tab
    And User verify and send an email to customer "LoginUsers,B,17" with template "CaseCreation,N,2" on compose email section
    Then User Logouts from an SVO Portal

  #User is able to verify 'CLASSIC WORKS CUST SVC CONTACT' Email Template for Customer Service case created on Salesforce
  @SSEC_1557_ClassicPartsCaseEmailTemplate_002 @SSEC-1937
  Scenario: User is able to verify 'CLASSIC WORKS CUST SVC CONTACT' Email Template for Customer Service case created on Salesforce
    Given User Access to the salesforce Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User navigate to Cases Tab
    And User creates a new case on SVO of record type "CaseCreation,C,3" by linking case with existing Account "Opportunity,I,2" and Contact "Opportunity,J,2" details
    Then User Navigates to Activity tab
    And User verify and send an email to customer "LoginUsers,B,17" with template "CaseCreation,N,21" on compose email section
    Then User Logouts from an SVO Portal

  #User is able to verify 'DEALER CUST SVC NOT CLASSIC' Email Template for Classic Parts and Technical case created through web form
  @SSEC_1557_ClassicPartsCaseEmailTemplate_003 @SSEC-1938
  Scenario: User is able to verify 'DEALER CUST SVC NOT CLASSIC' Email Template for Classic Parts and Technical case created through web form
    Given Login to the Classic Parts portal
    Then User Navigated to Contact Us section
    Then User create an Parts and Technical Case from web form with Email "CaseCreation,A,2" First name "CaseCreation,G,2" Last name "CaseCreation,H,2" Country "CaseCreation,I,2" Phone number "CaseCreation,K,2" VIN number "CaseCreation,J,2"
    Then Verify that an user receives an outlook email notification on Parts and Technical case creation
    Given User login to the salesforce Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User navigate to Cases Tab
    Then User Verifies that Parts and Technical case is automatically created on SVO Portal
    Then User Navigates to Activity tab
    And User verify and send an email to customer "LoginUsers,B,17" with template "CaseCreation,N,6" on compose email section
    Then User Logouts from an SVO Portal

  #User is able to verify 'DELIVERY COSTS' Email Template for Classic Parts and Technical case created through web form
  @SSEC_1557_ClassicPartsCaseEmailTemplate_004 @SSEC-1939
  Scenario: User is able to verify 'DELIVERY COSTS' Email Template for Classic Parts and Technical case created through web form
    Given Login to the Classic Parts portal
    Then User Navigated to Contact Us section
    Then User create an Parts and Technical Case from web form with Email "CaseCreation,A,2" First name "CaseCreation,G,2" Last name "CaseCreation,H,2" Country "CaseCreation,I,2" Phone number "CaseCreation,K,2" VIN number "CaseCreation,J,2"
    Then Verify that an user receives an outlook email notification on Parts and Technical case creation
    Given User login to the salesforce Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User navigate to Cases Tab
    Then User Verifies that Parts and Technical case is automatically created on SVO Portal
    Then User Navigates to Activity tab
    And User verify and send an email to customer "LoginUsers,B,17" with template "CaseCreation,N,22" on compose email section
    Then User Logouts from an SVO Portal

  #User is able to verify 'JDHT ARCHIVE BUILD SHEET' Email Template for Customer Service case created on Salesforce
  @SSEC_1557_ClassicPartsCaseEmailTemplate_005 @SSEC-1940
  Scenario: User is able to verify 'JDHT ARCHIVE BUILD SHEET' Email Template for Customer Service case created on Salesforce
    Given User Access to the salesforce Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User navigate to Cases Tab
    And User creates a new case on SVO of record type "CaseCreation,C,3" by linking case with existing Account "Opportunity,I,2" and Contact "Opportunity,J,2" details
    Then User Navigates to Activity tab
    And User verify and send an email to customer "LoginUsers,B,17" with template "CaseCreation,N,7" on compose email section
    Then User Logouts from an SVO Portal

  #User is able to verify 'JDHT KEY NUMBERS' Email Template for Customer Service case created on Salesforce
  @SSEC_1557_ClassicPartsCaseEmailTemplate_006 @SSEC-1941
  Scenario: User is able to verify 'JDHT KEY NUMBERS' Email Template for Customer Service case created on Salesforce
    Given User Access to the salesforce Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User navigate to Cases Tab
    And User creates a new case on SVO of record type "CaseCreation,C,3" by linking case with existing Account "Opportunity,I,2" and Contact "Opportunity,J,2" details
    Then User Navigates to Activity tab
    And User verify and send an email to customer "LoginUsers,B,17" with template "CaseCreation,N,23" on compose email section
    Then User Logouts from an SVO Portal

  #User is able to verify 'KEYS AND CODES' Email Template for Customer Service case created on Salesforce
  @SSEC_1557_ClassicPartsCaseEmailTemplate_007 @SSEC-1942
  Scenario: User is able to verify 'KEYS AND CODES' Email Template for Customer Service case created on Salesforce
    Given User Access to the salesforce Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User navigate to Cases Tab
    And User creates a new case on SVO of record type "CaseCreation,C,3" by linking case with existing Account "Opportunity,I,2" and Contact "Opportunity,J,2" details
    Then User Navigates to Activity tab
    And User verify and send an email to customer "LoginUsers,B,17" with template "CaseCreation,N,8" on compose email section
    Then User Logouts from an SVO Portal

  #User is able to verify 'L322' Email Template for Customer Service cases created on Salesforce
  @SSEC_1557_ClassicPartsCaseEmailTemplate_008 @SSEC-1943
  Scenario: User is able to verify 'L322' Email Template for Customer Service cases created on Salesforce
    Given User Access to the salesforce Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User navigate to Cases Tab
    And User creates a new case on SVO of record type "CaseCreation,C,2" by linking case with existing Account "Opportunity,I,2" and Contact "Opportunity,J,2" details
    Then User Navigates to Activity tab
    And User verify and send an email to customer "LoginUsers,B,17" with template "CaseCreation,N,9" on compose email section
    Then User Logouts from an SVO Portal

  #User is able to verify 'NLA' Email Template for Classic Parts and Technical case created through web form
  @SSEC_1557_ClassicPartsCaseEmailTemplate_009 @SSEC-1944
  Scenario: User is able to verify 'NLA' Email Template for Classic Parts and Technical case created through web form
    Given Login to the Classic Parts portal
    Then User Navigated to Contact Us section
    Then User create an Parts and Technical Case from web form with Email "CaseCreation,A,2" First name "CaseCreation,G,2" Last name "CaseCreation,H,2" Country "CaseCreation,I,2" Phone number "CaseCreation,K,2" VIN number "CaseCreation,J,2"
    Then Verify that an user receives an outlook email notification on Parts and Technical case creation
    Given User login to the salesforce Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User navigate to Cases Tab
    Then User Verifies that Parts and Technical case is automatically created on SVO Portal
    Then User Navigates to Activity tab
    And User verify and send an email to customer "LoginUsers,B,17" with template "CaseCreation,N,10" on compose email section
    Then User Logouts from an SVO Portal

  #User is able to verify 'NOT IN RANGE' Email Template for Classic Parts and Technical case created through web form
  @SSEC_1557_ClassicPartsCaseEmailTemplate_010 @SSEC-1945
  Scenario: User is able to verify 'NOT IN RANGE' Email Template for Classic Parts and Technical case created through web form
    Given Login to the Classic Parts portal
    Then User Navigated to Contact Us section
    Then User create an Parts and Technical Case from web form with Email "CaseCreation,A,2" First name "CaseCreation,G,2" Last name "CaseCreation,H,2" Country "CaseCreation,I,2" Phone number "CaseCreation,K,2" VIN number "CaseCreation,J,2"
    Then Verify that an user receives an outlook email notification on Parts and Technical case creation
    Given User login to the salesforce Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User navigate to Cases Tab
    Then User Verifies that Parts and Technical case is automatically created on SVO Portal
    Then User Navigates to Activity tab
    And User verify and send an email to customer "LoginUsers,B,17" with template "CaseCreation,N,24" on compose email section
    Then User Logouts from an SVO Portal

  #User is able to verify 'PAINT CODES JAGUAR NON US' Email Template for Customer Service case created on Salesforce
  @SSEC_1557_ClassicPartsCaseEmailTemplate_011 @SSEC-1946
  Scenario: User is able to verify 'PAINT CODES JAGUAR NON US' Email Template for Customer Service case created on Salesforce
    Given User Access to the salesforce Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User navigate to Cases Tab
    And User creates a new case on SVO of record type "CaseCreation,C,3" by linking case with existing Account "Opportunity,I,2" and Contact "Opportunity,J,2" details
    Then User Navigates to Activity tab
    And User verify and send an email to customer "LoginUsers,B,17" with template "CaseCreation,N,12" on compose email section
    Then User Logouts from an SVO Portal

  #User is able to verify 'PAINT CODE LAND ROVER' Email Template for Customer Service cases created on Salesforce
  @SSEC_1557_ClassicPartsCaseEmailTemplate_012 @SSEC-1947
  Scenario: User is able to verify 'PAINT CODE LAND ROVER' Email Template for Customer Service cases created on Salesforce
    Given User Access to the salesforce Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User navigate to Cases Tab
    And User creates a new case on SVO of record type "CaseCreation,C,2" by linking case with existing Account "Opportunity,I,2" and Contact "Opportunity,J,2" details
    Then User Navigates to Activity tab
    And User verify and send an email to customer "LoginUsers,B,17" with template "CaseCreation,N,13" on compose email section
    Then User Logouts from an SVO Portal

  #User is able to verify 'PLEASE REMEMBER' Email Template for Classic Parts and Technical case created through web form
  @SSEC_1557_ClassicPartsCaseEmailTemplate_013 @SSEC-1948
  Scenario: User is able to verify 'PLEASE REMEMBER' Email Template for Classic Parts and Technical case created through web form
    Given Login to the Classic Parts portal
    Then User Navigated to Contact Us section
    Then User create an Parts and Technical Case from web form with Email "CaseCreation,A,2" First name "CaseCreation,G,2" Last name "CaseCreation,H,2" Country "CaseCreation,I,2" Phone number "CaseCreation,K,2" VIN number "CaseCreation,J,2"
    Then Verify that an user receives an outlook email notification on Parts and Technical case creation
    Given User login to the salesforce Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User navigate to Cases Tab
    Then User Verifies that Parts and Technical case is automatically created on SVO Portal
    Then User Navigates to Activity tab
    And User verify and send an email to customer "LoginUsers,B,17" with template "CaseCreation,N,14" on compose email section
    Then User Logouts from an SVO Portal

  #User is able to verify 'RADIO CODES' Email Template for Customer Service case created on Salesforce
  @SSEC_1557_ClassicPartsCaseEmailTemplate_014 @SSEC-1949
  Scenario: User is able to verify 'RADIO CODES' Email Template for Customer Service case created on Salesforce
    Given User Access to the salesforce Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User navigate to Cases Tab
    And User creates a new case on SVO of record type "CaseCreation,C,3" by linking case with existing Account "Opportunity,I,2" and Contact "Opportunity,J,2" details
    Then User Navigates to Activity tab
    And User verify and send an email to customer "LoginUsers,B,17" with template "CaseCreation,N,15" on compose email section
    Then User Logouts from an SVO Portal

  #User is able to verify 'VIN REQUEST' Email Template for Customer Service case created on Salesforce
  @SSEC_1557_ClassicPartsCaseEmailTemplate_015 @SSEC-1950
  Scenario: User is able to verify 'VIN REQUEST' Email Template for Customer Service case created on Salesforce
    Given User Access to the salesforce Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User navigate to Cases Tab
    And User creates a new case on SVO of record type "CaseCreation,C,3" by linking case with existing Account "Opportunity,I,2" and Contact "Opportunity,J,2" details
    Then User Navigates to Activity tab
    And User verify and send an email to customer "LoginUsers,B,17" with template "CaseCreation,N,16" on compose email section
    Then User Logouts from an SVO Portal

  #User is able to verify 'WEB LINK ORDER INSTRUCTIONS' Email Template for Customer Service cases created on Salesforce
  @SSEC_1557_ClassicPartsCaseEmailTemplate_016 @SSEC-1954
  Scenario: User is able to verify 'WEB LINK ORDER INSTRUCTIONS' Email Template for Customer Service cases created on Salesforce
    Given User Access to the salesforce Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User navigate to Cases Tab
    And User creates a new case on SVO of record type "CaseCreation,C,2" by linking case with existing Account "Opportunity,I,2" and Contact "Opportunity,J,2" details
    Then User Navigates to Activity tab
    And User verify and send an email to customer "LoginUsers,B,17" with template "CaseCreation,N,17" on compose email section
    Then User Logouts from an SVO Portal

  #User is able to verify 'WRONG WEB CONTACT' Email Template for Classic Parts and Technical case created through web form
  @SSEC_1557_ClassicPartsCaseEmailTemplate_017 @SSEC-1955
  Scenario: User is able to verify 'WRONG WEB CONTACT' Email Template for Classic Parts and Technical case created through web form
    Given Login to the Classic Parts portal
    Then User Navigated to Contact Us section
    Then User create an Parts and Technical Case from web form with Email "CaseCreation,A,2" First name "CaseCreation,G,2" Last name "CaseCreation,H,2" Country "CaseCreation,I,2" Phone number "CaseCreation,K,2" VIN number "CaseCreation,J,2"
    Then Verify that an user receives an outlook email notification on Parts and Technical case creation
    Given User login to the salesforce Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User navigate to Cases Tab
    Then User Verifies that Parts and Technical case is automatically created on SVO Portal
    Then User Navigates to Activity tab
    And User verify and send an email to customer "LoginUsers,B,17" with template "CaseCreation,N,18" on compose email section
    Then User Logouts from an SVO Portal

  #User is able to verify 'ARCHIVE AT BRITISH MOTOR MUSEUM' Email Template for Customer Service case created on Salesforce
  @SSEC_1557_ClassicPartsCaseEmailTemplate_018 @SSEC-1956
  Scenario: User is able to verify 'ARCHIVE AT BRITISH MOTOR MUSEUM' Email Template for Customer Service case created on Salesforce
    Given User Access to the salesforce Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User navigate to Cases Tab
    And User creates a new case on SVO of record type "CaseCreation,C,3" by linking case with existing Account "Opportunity,I,2" and Contact "Opportunity,J,2" details
    Then User Navigates to Activity tab
    And User verify and send an email to customer "LoginUsers,B,17" with template "CaseCreation,N,19" on compose email section
    Then User Logouts from an SVO Portal

  #User is able to verify 'HISTORIC BUILD SHEET' Email Template for Customer Service case created on Salesforce
  @SSEC_1557_ClassicPartsCaseEmailTemplate_019 @SSEC-1957
  Scenario: User is able to verify 'HISTORIC BUILD SHEET' Email Template for Customer Service case created on Salesforce
    Given User Access to the salesforce Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User navigate to Cases Tab
    And User creates a new case on SVO of record type "CaseCreation,C,3" by linking case with existing Account "Opportunity,I,2" and Contact "Opportunity,J,2" details
    Then User Navigates to Activity tab
    And User verify and send an email to customer "LoginUsers,B,17" with template "CaseCreation,N,20" on compose email section
    Then User Logouts from an SVO Portal

  #User is not able to send an Email with 'WRONG WEB CONTACT' template without mandatory details of email
  @SSEC_1557_ClassicPartsCaseEmailTemplate_020 @SSEC-1958
  Scenario: User is not able to send an Email with 'WRONG WEB CONTACT' template without mandatory details of email
    Given Login to the Classic Parts portal
    Then User Navigated to the Contact Us section
    Then User creates an Classic Parts and Technical Case from web form with Email "CaseCreation,A,2" First name "CaseCreation,G,2" Last name "CaseCreation,H,2" Country "CaseCreation,I,2" Phone number "CaseCreation,K,2" VIN number "CaseCreation,J,2"
    Then Verify that an user receives an outlook mail notification on Parts and Technical case creation
    Given User login to the salesforce Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User navigate to Cases Tab
    Then User Verifies that Parts and Technical case is automatically created on SVO Portal
    Then User Navigates to Activity tab
    And Verify that user is not able to send an email without madatory fields of email to customer "LoginUsers,B,17" with template "CaseCreation,N,18" on compose email section
    Then User Logouts from an SVO Portal
