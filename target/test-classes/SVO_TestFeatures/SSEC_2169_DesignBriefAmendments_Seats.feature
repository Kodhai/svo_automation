@SSEC_2169
Feature: Validate Design Brief Amendments- Seats

  @SSEC_2169_01 @sonica
  Scenario: Bespoke user is able to fill seat details like Monotone/Dutone, Hand of Drive, Front and rear seat materials
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User creates a Bespoke Opportunity with check cleared like Stage "Opportunity,B,3" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,I,3" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,3"
    Then User verifies that new Opportunity is created
    Then User navigates to Quotes from Related Links
    And User creates new Bespoke Design Brief quotes "Opportunity,J,4" with mandatory details
    And Click on the new Bespoke Design Brief quote created successfully
    Then Click on Edit button of Monotone/Duotone "DesginBrief,A,13" from Seats
    And Select the Hand of Drive "DesginBrief,B,13" and front "DesginBrief,C,13" and rear seat materials "DesginBrief,D,13" from drop down
    Then Click on save button of Design Brief
    Then User Logout from SVO Portal

  @SSEC_2169_02 @sonica
  Scenario: Bespoke User is able to select single contrast color if Monotone is selected from Contrast Stitch Type from Seats Commodity
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User creates a Bespoke Opportunity with check cleared like Stage "Opportunity,B,3" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,I,3" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,3"
    Then User verifies that new Opportunity is created
    Then User navigates to Quotes from Related Links
    And User creates new Bespoke Design Brief quotes "Opportunity,J,4" with mandatory details
    And Click on the new Bespoke Design Brief quote created successfully
    Then Click on Edit button of Contrast Stitch Type and select Monotone "DesginBrief,E,13" from Contrast Stitch Type Brief drop down
    Then Select Contrast Color1 "DesginBrief,F,13" from drop down
    Then Click on save button of Design Brief
    Then User Logout from SVO Portal

  @SSEC_2169_03 @sonica
  Scenario: Bespoke User is able to select double contrast colors if Duotone is selected from Contrast Stitch Type from Seats Commodity
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User creates a Bespoke Opportunity with check cleared like Stage "Opportunity,B,3" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,I,3" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,3"
    Then User verifies that new Opportunity is created
    Then User navigates to Quotes from Related Links
    And User creates new Bespoke Design Brief quotes "Opportunity,J,4" with mandatory details
    And Click on the new Bespoke Design Brief quote created successfully
    Then Click on Edit button of Contrast Stitch Type and select Duotone "DesginBrief,E,14" from Contrast Stitch Type drop down
    Then Select Contrast Color1 "DesginBrief,F,13" and Color2 "DesginBrief,G,13" from drop down
    Then Click on save button of Design Brief
    Then User Logout from SVO Portal

  @SSEC_2169_04 @sonica
  Scenario: Bespoke User is able to select 3 contrast colors if Duotone full contract and a third color is selected from Contrast Stitch Type from Seats Commodity
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User creates a Bespoke Opportunity with check cleared like Stage "Opportunity,B,3" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,I,3" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,3"
    Then User verifies that new Opportunity is created
    Then User navigates to Quotes from Related Links
    And User creates new Bespoke Design Brief quotes "Opportunity,J,4" with mandatory details
    And Click on the new Bespoke Design Brief quote created successfully
    Then Click on Edit button of Contrast Stitch Type and select the Duotone Full contrast and Third Color option "DesignBrief,E,15" from drop down
    Then Select Contrast Color1 "DesignBrief,F,13" and Color2 "DesignBrief,G,13" and Color3 "DesignBrief,H,13" from drop down
    Then Click on save button of Design Brief
    Then User Logout from SVO Portal

  @SSEC_2169_05 @sonica
  Scenario: Seat Embroidery is selected as Personalized a text box is displayed for adding details
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User creates a Bespoke Opportunity with check cleared like Stage "Opportunity,B,3" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,I,3" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,3"
    Then User verifies that new Opportunity is created
    Then User navigates to Quotes from Related Links
    And User creates new Bespoke Design Brief quotes "Opportunity,J,4" with mandatory details
    And Click on the new Bespoke Design Brief quote created successfully
    Then Click on Edit button of Seat Embroidery and select Personalized option "DesignBrief,I,13" from the drop down
    Then Add the additional details in the text box
    Then Click on save button of Design Brief
    Then User Logout from SVO Portal

  @SSEC_2169_06 @sonica
  Scenario: Bespoke User is able to select cushions colors of the seats
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User creates a Bespoke Opportunity with check cleared like Stage "Opportunity,B,3" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,I,3" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,3"
    Then User verifies that new Opportunity is created
    Then User navigates to Quotes from Related Links
    And User creates new Bespoke Design Brief quotes "Opportunity,J,4" with mandatory details
    And Click on the new Bespoke Design Brief quote created successfully
    Then Click on Edit button of Rear Seat Cushion color "DesignBrief,J,13"
    And Select Rear seat cushion color backboard color and bulkhead colors from the drop down
    Then Click on save button of Design Brief
    Then User Logout from SVO Portal
