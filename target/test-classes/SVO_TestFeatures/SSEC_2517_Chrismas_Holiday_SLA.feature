Feature: Classic & SVO - Enquiry SLA and Auto-Response Updates during Christmas Holiday

  @SSEC_2517_01
  Scenario: Verify that Classic Sales user is able to view Paused SLA for new enquiry
    Given Access to Outlook account with Username "LoginUsers,B,14" and password "LoginUsers,C,14"
    Then Customer sends an email to create an enquiry
    Given Access to SVO Portal as Classic Sales user with Username "LoginUsers,B,5" and password "LoginUsers,C,5"
    When User Navigate to Enquiries
    And User verify that SLA has paused for created enquiry
    Then Logout from the SVO Portal

  @SSEC_2517_02
  Scenario: Verify that SVO Bespoke user is able to view Paused SLA for new enquiry
    Given Access to Outlook account with Username "LoginUsers,B,14" and password "LoginUsers,C,14"
    Then Customer sends an email to create a bespoke enquiry
    Given Access to SVO Portal as Classic Sales user with Username "LoginUsers,B,3" and password "LoginUsers,C,3"
    When User Navigate to Enquiries
    And User verify that SLA has paused for created enquiry
    Then Logout from the SVO Portal

  @SSEC_2517_03
  Scenario: User is able to verify the Start Date - 23rd December , End Date - 3rd January of created new enquiry
    Given Access to Outlook account with Username "LoginUsers,B,14" and password "LoginUsers,C,14"
    Then Customer sends an email to create an enquiry
    Given Access to SVO Portal as Classic Sales user with Username "LoginUsers,B,5" and password "LoginUsers,C,5"
    When User Navigate to Enquiries
    And User verify the start and end date of created new enquiry during Christmas holiday
    Then Logout from the SVO Portal
    
    @SSEC_2517_04
    Scenario: Verify that User is able to get Thank you for your enquiry message after create new enquiry in Classic Sales User.
    Given Access to Outlook account with Username "LoginUsers,B,14" and password "LoginUsers,C,14"
    Then Customer sends an email to create an enquiry
    And User verify the thank you message after enquiry got created
    
    @SSEC_2517_05
    Scenario: Verify that User is able to get Thank you for your enquiry message after create new enquiry in SVO Bespoke User
    Given Access to Outlook account with Username "LoginUsers,B,14" and password "LoginUsers,C,14"
    Then Customer sends an email to create a bespoke enquiry
    And User verify the thank you message after enquiry got created
    
     @SSEC_2517_06
    Scenario: Verify that User is not able to get Thank you for your enquiry message after christmas holiday ends.
    Given Access to Outlook account with Username "LoginUsers,B,14" and password "LoginUsers,C,14"
    Then Customer sends an email to create an enquiry
    And User verify the thank you message after enquiry got created
   