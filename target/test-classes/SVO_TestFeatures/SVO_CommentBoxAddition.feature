Feature: Addition of a Comment Box for New and Old Work Order fields

  @SVO_CommentBoxAddition1 @SSEC-836
  Scenario: User is able to view all required Comments field on existing Work Order
    Given Access to SVO Portal as Classic Operations user with Username "LoginUsers,B,9" and Password "LoginUsers,C,9"
    Then User Navigates to Work Orders page from SVO Menu bar
    Then User selects All Work orders list from Select List view
    And User opens first Work Order from the All Work Orders List
    Then User verifies that Comments fields are present on an existing Work Order
    Then User Logout from the SVO Portal

  @SVO_CommentBoxAddition2 @SSEC-890
  Scenario: User enters comments in all applicable sections while creating a new Work Order
    Given Access to SVO Portal as Admin user with Username "LoginUsers,B,6" and Password "LoginUsers,C,6"
    Then User Navigates to Work Orders page from SVO Menu bar
    And User creates a new Work Order and enter comments in all the required section
    Then User verifies that the comments are added in all required section
    Then User Logout from the SVO Portal

  @SVO_CommentBoxAddition3 @SSEC-891
  Scenario: User is able to edit added comments under any section of an existing work order
    Given Access to SVO Portal as Classic Admin user with Username "LoginUsers,B,13" and Password "LoginUsers,C,13"
    Then User Navigates to Work Orders page from SVO Menu bar
    Then User selects All Work orders list from Select List view
    And User opens first Work Order from the All Work Orders List
    Then User adds Comment under any section of an opened Work Order
    And User edits added comment under any section of an opened Work Order
    Then User verify that the comments are updated for an opened Work Order
    Then User Logout from the SVO Portal

  @SVO_CommentBoxAddition4 @SSEC-896
  Scenario: User is able to add comments under any section to an existing work order
    Given Access to SVO Portal as Classic Operations user with Username "LoginUsers,B,9" and Password "LoginUsers,C,9"
    Then User Navigates to Work Orders page from SVO Menu bar
    Then User selects New work orders list from Select List view
    And User opens first Work Order from the All Work Orders List after sorting it
    Then User adds Comment with Bold, Italic, Underline, Different Font Style, Font Color and Font Size for any section
    And User verifies that the comments are added with formatting under any section
    Then User Logout from the SVO Portal

  @SVO_CommentBoxAddition5 @SSEC-897
  Scenario: User is not able to attach link into comments field under any section of an existing Work Order
    Given Access to SVO Portal as classic service from Add Username user with Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    Then User Navigates to Work Orders page from SVO Menu bar
    Then User selects All Work orders list from Select List view
    And User opens first Work Order from the All Work Orders List after sorting it
    Then Verify that user is not able to attach link into comments field under any section of an opened work order
    Then User Logout from the SVO Portal

  @SVO_CommentBoxAddition6 @SSEC-897
  Scenario: User is able to attach images into comments field under any section of an existing Work Order
    Given Access to SVO Portal as Classic Operations user with Username "LoginUsers,B,9" and Password "LoginUsers,C,9"
    Then User Navigates to Work Orders page from SVO Menu bar
    And User selects New work orders list from Select List view
    And User opens first Work Order from the All Work Orders List
    Then User attach image into Comment box for any Section of an opened work order
    And Verify that user is able to view attach image into Comment box for an opened work order
    Then User Logout from the SVO Portal

  @SVO_CommentBoxAddition7 @SSEC-897
  Scenario: User is able to remove formatting added in any section's comments field of an existing Work Order
    Given Access to SVO Portal as Classic Admin user with Username "LoginUsers,B,13" and Password "LoginUsers,C,13"
    Then User Navigates to Work Orders page from SVO Menu bar
    Then User selects All Work orders list from Select List view
    And User opens first Work Order from the All Work Orders List after sorting it
    Then User adds Comment with Bold, Italic, Underline, Different Font Style, Font Color and Font Size for any section
    And User verifies that the comments are added with formatting under any section
    Then User removes formatting added in comment box of an opened work order
    Then User Logout from the SVO Portal
			
  @SVO_CommentBoxAddition8 @SSEC-897
  Scenario: User is able to add multiple images into comments field under any section of an existing Work Order
    Given Access to SVO Portal as Classic Admin user with Username "LoginUsers,B,13" and Password "LoginUsers,C,13"
    Then User Navigates to Work Orders page from SVO Menu bar
    Then User selects New work orders list from Select List view
    And User opens first Work Order from the All Work Orders List after sorting it
    Then User attach multiple images into single comment box of an opened work order
    And Verify that user is able to view attach image into Comment box for an opened work order
    Then User Logout from the SVO Portal

  @SVO_CommentBoxAddition11 @SSEC-872
  Scenario: User is not able to view comments field under any section of an existing Work Order
    Given Access to SVO Portal as Classic Finance user with Username "LoginUsers,B,10" and Password "LoginUsers,C,10"
    Then Verify that user is not able to view Work orders page from SVO Menu bar
    Then User Logout from the SVO Portal

  @SVO_CommentBoxAddition12 @SSEC-873
  Scenario: User is not able to edit comments field under any section of an existing Work Order
    Given Access to SVO Portal as classic service Username user with Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    Then User Navigates to Work Orders page from SVO Menu bar
    Then User selects All Work orders list from Select List view
    And User opens first Work Order from the All Work Orders List after sorting it
    Then Verify that user is not able to edit comments field under any section of an opened work order
    Then User Logout from the SVO Portal

  @SVO_CommentBoxAddition13 @SSEC-874
  Scenario: User is not able to view added comments under any section of an existing Work Order
    Given Access to SVO Portal as Classic Operations user with Username "LoginUsers,B,9" and Password "LoginUsers,C,9"
    Then User Navigates to Work Orders page from SVO Menu bar
    And User selects New work orders list from Select List view
    And User opens first Work Order from the All Work Orders List
    Then User cancels the comments window after adding Comment under any section of an opened Work Order
    And Verify that user is not able to view added comments under any section of an opened work order
    Then User Logout from the SVO Portal
