@SSEC_2119
Feature: Validate Additional Vehicles Name Auto Number

  @SSEC_2119_01 @sonica
  Scenario: User is able to create new additional vehicle for Bespoke User
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User create a SVO Bespoke Opportunity with check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,4" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User navigates to Additional Vehicles from Quick Links
    And Creates a new Additional Vehicle
    Then Verify new additional vehicle is created
    Then User logouts from the portal

  @SSEC_2119_02 @sonica
  Scenario: User is able to verify that new additional vehicle created would have a seqential form starting with SVO
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User create a SVO Bespoke Opportunity with check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,4" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User navigates to Additional Vehicles from Quick Links
    And Creates a new Additional Vehicle
    Then Verify that new additional vehicle created is in the sequential form starting with SVO
    Then User logouts from the portal

  @SSEC_2119_03 @sonica
  Scenario: User is able to verify that each additional Vehicle is related to an Opportunity
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User create a SVO Bespoke Opportunity with check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,4" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User navigates to Additional Vehicles from Quick Links
    And Creates a new Additional Vehicle
    Then Verify that the opportunity field has the opportunity name
    Then User logouts from the portal

  @SSEC_2119_04 @sonica
  Scenario: User is not able to edit Vehicle Auto Number field
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User create a SVO Bespoke Opportunity with check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,4" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User navigates to Additional Vehicles from Quick Links
    And Creates a new Additional Vehicle
    Then User clicks on Edit button
    And Verify that Additional Vehicle Auto Number is uneditable
    Then User logouts from the portal