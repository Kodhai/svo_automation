Feature: SVO Work Order Additional owner Validations

  @SVO_WorkOrder_Additional_Owner_04 @SSEC-950 @AutoTestSet-SSEC-953
  Scenario: User is able remove the additional owner user from an existing work order on Work orders page
    Given Access to SVO Portal as Classic Operations user with Username "LoginUsers,B,9" and Password "LoginUsers,C,9"
    Then User Navigates to Work Orders page from SVO Menu bar
    Then User selects an existing Work Order from All Work orders List view
    And Verify the Additional Owner field on Work Orders page
    And Verify that user is able to remove 'Additional Owner' field on work orders page Additional Owner Name "Additional Owner,A,2"
    Then User Logout from the SVO Portal
    Then User Logout from the SVO Portal

  @SVO_WorkOrder_Additional_Owner_05 @SSEC-951 @AutoTestSet-SSEC-953
  Scenario: An Additional User is not able to view work order details, if the created work order is deleted
    Given Access to SVO Portal as Classic Admin user with Username "LoginUsers,B,13" and Password "LoginUsers,C,13"
    Then User navigates to the Opportunity tab
    And User create a Classic Reborn Opportunity with check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User moves an opportunity to order placed stage
    And User Navigates to work order of an created order
    And Verify the Additional Owner field on Work Orders page
    Then Add Classic User "AdditionalOwner,A,4" as new Additional Owner for an existing Work order
    Then User Navigates back to Work Orders page from SVO Menu bar
    And user is able to view newly created work order on 'My work orders' list view on work order page
    And Verify that user is able to delete work order on work orders page
    Then Access to SVO Portal as classic service user from Add Username user with Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    Then User Navigates to Work Orders page from SVO Menu bar
    Then User selects My Work orders from Select List view
    And User is not able to view the work order details on my work orders page
    Then User Logout from the SVO Portal

  @SVO_WorkOrder_AdditionalOwner_06 @SSEC-945 @AutoTestSet-SSEC-953
  Scenario: User is not able to add any user other than classic user as an Additional Owner for any work order
    Given Access to SVO Portal as Classic Admin user with Username "LoginUsers,B,13" and Password "LoginUsers,C,13"
    Then User navigates to the Opportunity tab
    And User create a Classic Reborn Opportunity with check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User moves an opportunity to order placed stage
    And User Navigates to work order of an created order
    And Verify the Additional Owner field on Work Orders page
    Then Add non classic user "AdditionalOwner,A,3" as new Additional owner for the selected work order
    Then Verify that user is not able to add non classic user as additional owner for an work order
    Then User Logout from the SVO Portal

  @SVO_WorkOrder_Additional_Owner_07 @SSEC-948 @AutoTestSet-SSEC-953
  Scenario: User is not able to view additional owner field on work orders page
    Given Access to SVO bespoke operation Portal with Username "LoginUsers,B,8" and Password "LoginUsers,C,8"
    Then Verify that user is not able to view Work orders page from SVO Menu bar
    Then User Logout from the SVO Portal

  @SVO_WorkOrder_AdditionalOwner_08 @SSEC-946 @AutoTestSet-SSEC-953
  Scenario: User is not able to view 'Additional Owner' field on work orders page
    Given Access to SVO Portal as Admin user with Username "LoginUsers,B,6" and Password "LoginUsers,C,6"
    Then User Navigates to Work Orders page from SVO Menu bar
    Then User selects an existing Work Order from All Work orders List view
    And Verify that user is not able to view Additional Owner field on work orders page
    Then User Logout from the SVO Portal

  @SVO_WorkOrder_AdditionalOwner_09 @SSEC-947 @AutoTestSet-SSEC-953
  Scenario: User is not able to add or edit 'Additional owner' for any work order on work orders page
    Given Access to SVO Portal with Classic Service user with the Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    Then User Navigates to Work Orders page from SVO Menu bar
    Then User selects an existing Work Order from All Work orders List view
    And Verify that user is not able to view Edit Additional Owner Icon on work orders page
    Then User Logout from the SVO Portal

  @SVO_WorkOrder_Additional_Owner_10 @SSEC-952 @AutoTestSet-SSEC-953
  Scenario: User is not able to update work order with same Owner and Additional owner on work orders page
    Given Access to SVO Portal as Classic Admin user with Username "LoginUsers,B,13" and Password "LoginUsers,C,13"
    Then User Navigates to Work Orders page from SVO Menu bar
    Then User selects All from Select List view
    And Verify the error message while entering same name in both owner and additional owner field
    Then User Logout from the SVO Portal
