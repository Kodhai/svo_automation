@SSEC_2371
Feature: Validate Desgn Bried Amendments scripts

  #Manage Out of Office Responses & Milestone- Analysis & Development
  @SSEC_2371_01 @Prajna
  Scenario: Manage Out of Office Responses & Milestone- Analysis & Development
    Given Access to mail account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    When Customer send an enquiry mail to user "CustomerResponse,A,5"
    Then User Verify the mail test
    And Logout from users mail account
    #Given Access to outlook account with Username "LoginUsers,B,14" and password "LoginUsers,C,14"
    #When Customer send an enquiry outlook mail to user "LoginUsers,B,22"
    Given User login to lateral access SVO Portal by Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User navigate to Cases Tab
    When User Verify the cases created
    Then User Logouts from an SVO Portal

  #Manage Out of Office Responses & Milestone- Analysis & Development
  @SSEC_2371_02 @Prajna
  Scenario: Manage Out of Office Responses & Milestone- Analysis & Development
    #Given Login to SVO Portal by private office user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    Given Access to contact as form
    Then User Fill the form with mandatory details
    Given Laterally Access to the SVO Portal as Classic Parts user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User navigate to Cases Tab
    When User Verify the cases created
    Then User Logouts from an SVO Portal

  #Manage Out of Office Responses & Milestone- Analysis & Development
  @SSEC_2371_03 @Prajna
  Scenario: Manage Out of Office Responses & Milestone- Analysis & Development
    #Given Login to SVO Portal by private office user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    Given Access to contact as form
    Then User Fill the form with mandatory details
    Given Laterally Access to the SVO Portal as Classic Parts user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User navigate to Cases Tab
    When User Verify the cases created
    Then User Logouts from an SVO Portal

  #User is able to verify that milestone has not started
  @SSEC_2371_04 @Prajna
  Scenario: User is able to verify that milestone has not started
    #Given Login to SVO Portal by private office user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    Given Access to contact as form
    Then User Fill the form with mandatory details
    Given Laterally Access to the SVO Portal as Classic Parts user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User navigate to Cases Tab
    When User Verify the cases created
    And User Verify that milestone has not started
    Then User Logouts from an SVO Portal

  #User able to verify that the email template should contain From Date and To Date for response
  @SSEC_2371_05 @Prajna
  Scenario: User able to verify that the email template should contain From Date and To Date for response
    #Given Access to mail account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    #When Customer send an enquiry mail to user "CustomerResponse,A,5"
    Given Access to contact as form
    Then User Fill the form with mandatory details
    #Given Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    Given Laterally Access to the SVO Portal as Classic Parts user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User navigate to Cases Tab
    When User Verify the cases created
    Then User Logouts from an SVO Portal

  #User not able to send automatic response for Classic customer service case created via email
  @SSEC_2371_06 @Prajna
  Scenario: User not able to send automatic response for Classic customer service case created via email
    Given Access to contact as form
    Then User Fill the form with mandatory details
    #Given Access to mail account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    #When Customer send an enquiry mail to user "CustomerResponse,A,5"
    Given Laterally Access to the SVO Portal as Classic Parts user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    #Given Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User navigate to Cases Tab
    Then User Verify in customer service case havent created via email
    Then User Logouts from an SVO Portal
