Feature: Validate Scenario for Multiple User Role

  #User is able to create a new order with any record type for an individual account
  @SVO @SVO_044_otherRole @SSEC-315
  Scenario: User is able to create a new order with any record type for an individual account
    Given Access SVO Portal as a user having "Scripts_UserRole,B,45"
    When User Navigate to Accounts
    And User searches for a User Account "New Enquiry,AA,2"
    Then User Navigates to the User Account "New Enquiry,AA,2"
    And User Navigates to Orders Section
    And User Creates a New Order
    And User Saves the New order
    And User Deletes the Order
    Then Logout from the SF SVO Portal

  #In this scenario user tries to create classic opportunity for corporate account
  @SVO @SVO_071_otherRole @SSEC-342
  Scenario: User is able to create Opportunities (except SVO Bespoke and SVO Private Office) for a Corporate account
    Given Access SVO Portal as a user having "Scripts_UserRole,B,72"
    When User Navigate to Accounts
    And User searches for a User Account "Accounts,E,9" with AccountType "Accounts,U,2"
    Then User navigate to Opportunity Section
    And User creates a new Opportunity with Record Type "Opportunity,A,3" for Corporate Account
    Then Enter Required details in New Opportunity like Product Offering "Opportunity,B,2" Source "Opportunity,C,2" Account Name "Opportunity,D,5" Region "Opportunity,E,2" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,2" and Model "Opportunity,J,2"
    Then Logout from the SF SVO Portal

  #In this Scenario User tries to delete Corporate Account
  @SVO @SVO_072_otherRole @SSEC-343
  Scenario: User can delete the Corporate account created
    Given Access SVO Portal as a user having "Scripts_UserRole,B,73"
    When User Navigate to Accounts
    Then Create new Corporate account with Record type "Accounts,A,9" Account name "Accounts,E,9" Email "Accounts,K,10" Region "Accounts,N,10"
    And User tries to delete created Corporate Account
    Then Logout from the SF SVO Portal

  #User creates a new Programme while creaing Classic Limited Edition opportunity
  @SVO @SVO_093_otherRole @SSEC-364
  Scenario: User creates a new Programme while creaing Classic Limited Edition opportunity
    Given Access SVO Portal as a user having "Scripts_UserRole,B,94"
    When User Navigate to Opportunity
    Then User Creates a New opportunity with Record Type as "Opportunity,A,5"
    Then user navigated to Programme Field and clicks on new
    And enter the required information like name "Opportunity,U,5" product offering "Opportunity,B,5" build capacity "Opportunity,V,5" brand "Opportunity,I,5" and model "Opportunity,J,5"
    And save the programme
    Then cancel the opportunity creation
    Then Logout from the SF SVO Portal

  #User tries to delete Closed Won Opportunity
  @SVO @SVO-123_otherRole @SSEC-394
  Scenario: User tries to delete Closed Won Opportunity
    Given Access SVO Portal as a user having "Scripts_UserRole,B,124"
    Then User Navigate to Opportunity tab
    And Search for Opportunity with stage "Closed Won" and open relevant Opportunity
    Then Click on Delete button
    Then Logout from SF_SVO Portal

  #User tries to edit From section of an email in compose email section
  @SVO @SVO-127_otherRole @SSEC-398
  Scenario: User tries to edit From section of an email in compose email section
    Given Access SVO Portal as a user having "Scripts_UserRole,B,128"
    Then User Navigate to Opportunity tab
    And Search for Opportunity with stage "Estimate" and open relevant Opportunity
    Then Click on Compose email
    Then Verify that the user cannot edit From and Business unit fields in Compose email page
    Then Logout from SF_SVO Portal

  #User tries to change the Build Start details of an Work order in Opportunity section
  @SVO @SVO-132_otherRole @SSEC-403
  Scenario: User tries to change the Build Start details of an Work order in Opportunity section
    Given Access SVO Portal as a user having "Scripts_UserRole,B,133"
    Then User Navigate to Opportunity tab
    And Search for Opportunity with stage "Order Placed" and open relevant Opportunity
    And User navigates to Orders section from Related links of Opportunity
    Then Navigate to Work Order from Order window
    Then Edit Build start dates and Validate start slippage days
    Then Logout from SF_SVO Portal

  #User tries to retrieve the mail from drafts after entering email body and subject
  @SVO @SVO-140_otherRole @SSEC-411
  Scenario: User tries to retrieve the mail from drafts after entering email body and subject
    Given Access SVO Portal as a user having "Scripts_UserRole,B,141"
    Then User Navigate to Opportunity tab
    And Search for Opportunity with stage "Qualified" and open relevant Opportunity
    Then Click on Compose email
    Then edit the required information as Additional to "New Enquiry,U,2" Subject "New Enquiry,X,2" Body "New Enquiry,Y,2"
    And Click on save mail
    Then Click on Compose email
    Then edit the Additional to "New Enquiry,U,2"
    Then Click on drafts and retrive draft with Subject "New Enquiry,X,2"
    Then Logout from SF_SVO Portal

  #User Creates a new Quote
  @SVO @SVO-144_otherRole @SSEC-415
  Scenario: User Creates a new Quote
    Given Access SVO Portal as a user having "Scripts_UserRole,B,145"
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,5"
    And Enter details of New Opportunity like Opportunity name "Opportunity,L,5" Product Offering "Opportunity,B,5" Account Name "Opportunity,D,5" Region "Opportunity,E,5" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,5" and Model "Opportunity,J,5"
    Then Save the created Opportunity
    Then Navigate to Quotes
    Then Create a new Quote "Test Quote" and save
    Then Logout from SF_SVO Portal

  #User is able to Delete the Quotes
  @SVO @SVO-146_otherRole @SSEC-417
  Scenario: User is able to Delete the Quotes
    Given Access SVO Portal as a user having "Scripts_UserRole,B,147"
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,5"
    And Enter details of New Opportunity like Opportunity name "Opportunity,L,5" Product Offering "Opportunity,B,5" Account Name "Opportunity,D,5" Region "Opportunity,E,5" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,5" and Model "Opportunity,J,5"
    Then Save the created Opportunity
    Then Navigate to Quotes
    Then Create a new Quote "Test Quote" and save
    Then Delete the Quote created
    Then Logout from SF_SVO Portal

  #User is not able to change the ops Approval Status
  @SVO @SVO-149_otherRole @SSEC-420
  Scenario: User is not able to change the ops Approval Status
    Given Access SVO Portal as a user having "Scripts_UserRole,B,150"
    Then User Navigate to Opportunity tab
    Then Click on New button
    And Enter details of New SVO Bespoke Opportunity like Opportunity name "Opportunity,L,5" Product Offering "Opportunity,B,5" Account Name "Opportunity,D,5" Region "Opportunity,E,5" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,5" and Model "Opportunity,J,5"
    Then Save the created Opportunity
    Then Navigate to Quotes
    Then Create a new SVO Quote "Test Quote" and save
    Then Verify the OPS Approval Status field is not editable
    Then Logout from SF_SVO Portal

  #User can update/Change the Outcome Details
  #SVO Private office user
  @SVO @SVO_159_otherRole
  Scenario: User can update/Change the Outcome Details
    Given Access SVO Portal as a user having "Scripts_UserRole,B,154"
    Then User Navigate to Opportunity tab
    Then Create New Opportunity for SVO Private Office
    And Enter details of New Opportunity for Private Office Opportunity name "Opportunity,L,2" Product Offering "Opportunity,B,6" Account Name "Opportunity,D,5" Region "Opportunity,E,2" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,2" and Model "Opportunity,J,2"
    Then Save the created Opportunity
    Then Navigate to Quotes
    Then Create a new Quote "Test Quote" for Private Office and save
    Then Mark Status as Complete
    Then Mark Status as Complete
    Then Mark the Quote status as Outcome accepted
    Then Click on Edit and verify the outcome details cannot be changed
    Then Logout from SF_SVO Portal

  #User is able to Edit and Delete the orders in Quote section
  @SVO @SVO_162_otherRole
  Scenario: User is able to Edit and Delete the orders in Quote section
    Given Access SVO Portal as a user having "Scripts_UserRole,B,163"
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,8"
    And Enter details of New Opportunity like Opportunity name "Opportunity,L,2" Product Offering "Opportunity,B,2" Account Name "Opportunity,D,5" Region "Opportunity,E,2" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,2" and Model "Opportunity,J,2"
    Then Save the created Opportunity
    Then Navigate to Quotes
    Then Create a new Quote "Test Quote" and save
    Then Delete the Quote created
    Then Logout from SF_SVO Portal

  #User should be able to delete an Enquiry from KMI Record Section
  @SVO @SVO_176_otherRole @SSEC-447
  Scenario: User should be able to delete an Enquiry from KMI Record Section
    Given Access SVO Portal as a user having "Scripts_UserRole,B,177"
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "New Enquiry,A,2"
    And Enter the required details for the Enquiry creation
    Then Save the new Enquiry
    Then Navigate to KMI section
    Then User created New KMI with fields like Contact "New Enquiry,AA,2" ProductOffering "New Enquiry,B,4" Brand "New Enquiry,J,2" Model "New Enquiry,K,2"
    Then User click on newly created KMI
    Then User delete the KMI Record
    Then Logout from the SVO Portal

  #User tries to select template of e2a mail after adding text into email body
  @SVO @SVO_182_otherRole
  Scenario: User tries to select template of e2a mail after adding text into email body
    Given Access SVO Portal as a user having "Scripts_UserRole,B,183"
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,8"
    And fill all mandatory fields of SVOBespoke opportunity like OpportunityName "Opportunity,A,8" Closedate "Opportunity,AI,2" Stage "Opportunity,R,4" ProductOffering "Opportunity,B,2" Region "Opportunity,E,2" AccountName "Opportunity,D,9" PreferredContact "Opportunity,H,9" Brand "Opportunity,I,2" Model "Opportunity,J,2"
    Then Click on Compose email
    And User selects the template of an email
    Then User save the email
    Then Logout from SF_SVO Portal

  #User tries to create a new Order for a Corporate account
  @SVO @SVO_205_otherRole
  Scenario: User tries to create a new Order for a Corporate account
    Given Access SVO Portal as a user having "Scripts_UserRole,B,206"
    When User Navigate to Accounts
    Then User Searches for account "Accounts,E,10" and selects it
    And User Navigates to Orders Section
    Then Verify new button is not available to create a new order
    Then Logout from the SF SVO Portal

  #An order can automatically created after completing quote
  #SVO Bespoke access
  @SVO @SVO_206_otherRole
  Scenario: An order can automatically created after completing quote
    Given Access SVO Portal as a user having "Scripts_UserRole,B,207"
    Then User Navigate to Opportunity tab
    And Search for Opportunity with stage "Qualified" and open relevant Opportunity
    Then Navigate to Quotes
    Then Create a new SVO Quote "Test Quote" and save
    Then Navigate and open Design briefs
    Then Click on Submit and navigate back to Quote
    Then Mark the Quote status as Outcome accepted
    Then Navigate back to Opportunity
    Then navigate to Orders link and open the order
    Then Logout from SF_SVO Portal

  #User tries to create a new Opportunity with SVO Bespoke Record Type
  #Classic Bespoke access
  @SVO @SVO_207_otherRole
  Scenario: User tries to create a new Opportunity with SVO Bespoke Record Type
    Given Access SVO Portal as a user having "Scripts_UserRole,B,208"
    Then User Navigate to Opportunity tab
    Then Click on New opportunity button and verify user cannot create SVO Bespoke Record Type opportunity
    Then Logout from SF_SVO Portal

  #User tries to move the opportunity stage from  contract signed to order placed without an order creation
  #Users : Classic Finance
  @SVO @SVO_208_otherRole
  Scenario: User tries to move the opportunity stage from  contract signed to order placed without an order creation
    Given Access SVO Portal as a user having "Scripts_UserRole,B,209"
    Then User Navigate to Opportunity tab
    Then User searches for "Opportunity,AC,5" opportunities from select list view
    And Open an opportunity with Record Type "Opportunity,R,11"
    And User click on the mark stage as complete button to complete the process
    Then Verify the error message to close the opportunity
    Then Logout from SF_SVO Portal

  #User tries to create a new Programme while creating Classic Limited Edition opportunity
  #User Role : Classic Sales User : Michael Bishop
  @SVO @SVO_209_otherRole
  Scenario: User tries to create a new Programme while creating Classic Limited Edition opportunity
    Given Access SVO Portal as a user having "Scripts_UserRole,B,210"
    Then set up login to the user role "User Role,B,18"
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,9"
    When click on Programme text box
    Then Verify that New Record button is not available
    Then Logout from SF_SVO Portal

  #User tries to generate document through S-DOCS for any open opportunity
  #Users : SVO Private Office
  @SVO @SVO_210_otherRole
  Scenario: User tries to generate document through S-DOCS for any open opportunity
    Given Access SVO Portal as a user having "Scripts_UserRole,B,211"
    Then User Navigate to Opportunity tab
    Then User searches for "Opportunity,AC,5" opportunities from select list view
    And Open an opportunity with Record Type "Opportunity,R,11"
    When User click on S-Docs button
    Then Verify the error message insufficient Privileges
    Then Logout from SF_SVO Portal

  #User tries to delete Closed Won Opportunity
  #Users : Classic Finance
  @SVO @SVO_211_otherRole
  Scenario: User tries to delete Closed Won Opportunity
    Given Access SVO Portal as a user having "Scripts_UserRole,B,212"
    Then User Navigate to Opportunity tab
    Then User searches for "Opportunity,AC,5" opportunities from select list view
    And Open an opportunity with Record Type "Opportunity,R,13"
    Then Verify the Delete button
    Then Logout from SF_SVO Portal

  #User tries to create a new SAP Account for an individual account
  #Users: Special Operations Data Manager
  @SVO @SVO_212_otherRole
  Scenario: User tries to create a new SAP Account for an individual account
    Given Access SVO Portal as a user having "Scripts_UserRole,B,213"
    When User Navigate to Accounts
    Then User searches for "Accounts,O,2" Account from select list view
    When User click on first Account in the list
    And Verify the SAP Accounts link
    Then Logout from the SF SVO Portal

  #User tries to add new vehicle ownership as a driver to an individual account
  #Users : Bespoke user
  @SVO @SVO_213_otherRole
  Scenario: User tries to add new vehicle ownership as a driver to an individual account
    Given Access SVO Portal as a user having "Scripts_UserRole,B,214"
    When User Navigate to Accounts
    Then User searches for "Accounts,O,2" Account from select list view
    When User click on first Account in the list
    Then Navigate to the Vehicles Owned section
    And Verify the New button to add relationship as a driver
    Then Logout from the SF SVO Portal

  #User tries to create a new order with any record type for an individual account
  #Classic sales admin login
  @SVO @SVO_215_otherRole
  Scenario: User tries to create a new order with any record type for an individual account
    Given Access SVO Portal as a user having "Scripts_UserRole,B,216"
    When User Navigate to Accounts
    And User searches for a User Account "New Enquiry,AA,2"
    Then User Navigates to the User Account "New Enquiry,AA,2"
    And User Navigates to Orders Section
    Then Verify new button is not available to create a new order
    Then Logout from the SF SVO Portal

  #User tries to update consent for marketing communication for an individual account
  #Special Operations Data Manager/SVO Bespoke Data Manager login
  @SVO @SVO_216_otherRole
  Scenario: User tries to update consent for marketing communication for an individual account
    Given Access SVO Portal as a user having "Scripts_UserRole,B,217"
    When User Navigate to Accounts
    And User searches for a User Account "New Enquiry,AA,2"
    Then User Navigates to the User Account "New Enquiry,AA,2"
    Then Verify Update Consent button is not available for the user
    Then Logout from the SF SVO Portal

  #User tries to change the Person Account type or record type of an individual account
  #User Role : Special Operations Data Manager
  @SVO @SVO_217_otherRole
  Scenario: User tries to change the Person Account type or record type of an individual account
    Given Access SVO Portal as a user having "Scripts_UserRole,B,218"
    Then set up the user role login "User Role,B,7"
    When User Navigate to Accounts
    Then User searches for "Accounts,O,2" Account from select list view
    When User click on first Account in the list
    And User Navigate to Change Record Type section
    Then Verify the access denied error message
    Then Logout from the SF SVO Portal

  #User tries to create a new SAP Account and add New Vehicle Ownership for an Corporate account
  # User Role : SVO Bespoke
  @SVO @SVO_218_otherRole
  Scenario: User tries to create a new SAP Account and add New Vehicle Ownership for an Corporate account
    Given Access SVO Portal as a user having "Scripts_UserRole,B,219"
    Then set up the user role login "User Role,B,8"
    When User Navigate to Accounts
    Then User searches for "Accounts,O,3" Account from select list view
    When User click on first Account in the list
    And Verify the SAP Accounts link
    Then Logout from the SF SVO Portal

  #User tries to create new SVO client Opportunities for a Corporate account
  #Classic service role
  @SVO @SVO_220_otherRole @SSEC-491
  Scenario: User tries to create new SVO client Opportunities for a Corporate account
    Given Access SVO Portal as a user having "Scripts_UserRole,B,221"
    When User Navigate to Accounts
    Then User Searches for account "Accounts,E,10" and selects it
    Then Navigate to the SVO Client Opportunities section and click on New and verify SVO Bespoke record type is not available
    Then Logout from the SF SVO Portal

  #User tries to create a new opportunity with Classic Record type for a corporate account
  #SVO Bespoke role
  @SVO @SVO_221_otherRole @SSEC-492
  Scenario: User tries to create a new opportunity with Classic Record type for a corporate account
    Given Access SVO Portal as a user having "Scripts_UserRole,B,222"
    When User Navigate to Accounts
    Then User Searches for account "Accounts,E,10" and selects it
    Then Navigate to the Opportunities section and click on New and verify Classic record type is not available
    Then Logout from the SF SVO Portal

  #User cannot update consent for an Individual Account
  #Private Office role
  @SVO @SVO_230_otherRole
  Scenario: User cannot update consent for an Individual Account
    Given Access SVO Portal as a user having "Scripts_UserRole,B,231"
    When User Navigate to Accounts
    Then User Searches for account "Accounts,E,5" and selects it
    Then Verify Update Consent button is not available for the user
    Then Logout from the SF SVO Portal

  #User tries to access Vehicle Manufacturer section
  #Classic Sales role
  @SVO @SVO_232_otherRole
  Scenario: User tries to access Vehicle Manufacturer section
    Given Access SVO Portal as a user having "Scripts_UserRole,B,233"
    Then Search for "Vehicle manufacturer" in search box
    Then Verify no search results are found for Vehicle manufacturer
    Then Logout from SF-SVO Portal

  #User tries to Edit the Task details under Tasks Section
  #User Role : SVO Bespoke Data Manager
  @SVO @SVO_242_otherRole
  Scenario: User tries to Edit the Task details under Tasks Section
    Given Access SVO Portal as a user having "Scripts_UserRole,B,243"
    Then set up login to the user role "User Role,B,10"
    Then Navigate to Tasks tab
    And Select "Opportunity,AC,7" Tasks from select list view
    When User select first task from the list
    Then User Edit the selected task
    And Verify the Access denied error message
    Then Logout from SF_SVO Portal

  #User tries to Delete completed Task  under Tasks section
  #User Role: Special Operations Data Manager
  @SVO @SVO_243_otherRole
  Scenario: User tries to Delete completed Task  under Tasks section
    Given Access SVO Portal as a user having "Scripts_UserRole,B,244"
    Then set up login to the user role "User Role,B,7"
    Then Navigate to Tasks tab
    And Select "Opportunity,AC,7" Tasks from select list view
    When User select first task from the list
    And User assign the Task to "Opportunity,AE,2"
    Then User Mark task as completed
    When User delete the selected Task
    And Verify the Access denied error message to delete the Task
    Then Logout from SF_SVO Portal
