Feature: SVO Opportunity Invoice Validations

  @SVO_Opportunity_Invoice_01 @SSEC-1106 @SVO_Opportunity_Invoice_Batch
  Scenario: User is able to view auto-populated opportunity fields on invoice page whenever invoice is created
    Given Access to SVO Portal as classic service Username user with Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    When User navigates to an Opportunity tab
    And User creates Classic Opportunity with mandatory fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,7" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" SAP Account "Opportunity,Q,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then Move the Opportunity to Order placed stage Vehicle name "Opportunity_Invoice,B,2"
    And Verify that Account and Order details are displayed for the selected classic opportunity
    Then User create new invoice under invoice section with deposit Record Type "Opportunity_Invoice,A,2"
    And Verify the invoice details such as Account, Sap, Order and payment details of deposit invoice is displayed on Invoice page
    Then Logout from the Special vehicle operations Portal

  @SVO_Opportunity_Invoice_02 @SSEC-1107 @SVO_Opportunity_Invoice_Batch
  Scenario: User is able to create an invoice with 'Credit Note' Record type for any Classic Opportunity
    Given Access to SVO Portal as classic service Username user with Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    When User navigates to an Opportunity tab
    And User creates Classic Opportunity with mandatory fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,7" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" SAP Account "Opportunity,Q,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then Move the Opportunity to Order placed stage Vehicle name "Opportunity_Invoice,B,2"
    When User moves the opportunity to closed won stage
    And Verify that Account and Order details are displayed for the selected classic opportunity
    Then User create new invoice under invoice section with Credit note Record Type "Opportunity_Invoice,A,3"
    And Verify the invoice details such as Account, Sap, Order and payment details of credit note invoice is displayed on Invoice page
    Then Logout from the Special vehicle operations Portal

  @SVO_Opportunity_Invoice_03 @SSEC-1108 @SVO_Opportunity_Invoice_Batch
  Scenario: User is able to create an invoice with 'Deposit' Record type for any Classic Opportunity
    Given Access to SVO Portal as classic service Username user with Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    When User navigates to an Opportunity tab
    And User creates Classic Opportunity with mandatory fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,7" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" SAP Account "Opportunity,Q,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then Move the Opportunity to Order placed stage Vehicle name "Opportunity_Invoice,B,2"
    When User moves the opportunity to closed won stage
    And Verify that Account and Order details are displayed for the selected classic opportunity
    Then User create new invoice under invoice section with deposit Record Type "Opportunity_Invoice,A,2"
    And Verify the invoice details such as Account, Sap, Order and payment details of deposit invoice is displayed on Invoice page
    Then Logout from the Special vehicle operations Portal

  @SVO_Opportunity_Invoice_04 @SSEC-1110 @SVO_Opportunity_Invoice_Batch
  Scenario: User is able to create an invoice with 'Invoice' Record type for any Classic Opportunity
    Given Access to SVO Portal as classic service Username user with Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    When User navigates to an Opportunity tab
    And User creates Classic Opportunity with mandatory fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,7" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" SAP Account "Opportunity,Q,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then Move the Opportunity to Order placed stage Vehicle name "Opportunity_Invoice,B,2"
    When User moves the opportunity to closed Lost stage
    And Verify that Account and Order details are displayed for the selected classic opportunity
    Then User create new invoice under invoice section with invoice Record Type "Opportunity_Invoice,A,4"
    And Verify the invoice details such as Account, Sap, Order and payment details of invoice is displayed on Invoice page
    Then Logout from the Special vehicle operations Portal

  @SVO_Opportunity_Invoice_05 @SSEC-1112 @SVO_Opportunity_Invoice_Batch
  Scenario: User is able to create multiple invoices for single Opportunity
    Given Access to SVO Portal as classic service Username user with Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    When User navigates to an Opportunity tab
    And User creates Classic Opportunity without SAP Account field like Stage "Opportunity,B,2" Product Offering "Opportunity,C,7" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" SAP Account "Opportunity,Q,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then Move the Opportunity to Order placed stage Vehicle name "Opportunity_Invoice,B,2"
    And Verify that Account name and Order details are displayed for the selected classic opportunity
    Then User create new invoice under invoice section with invoice Record Type "Opportunity_Invoice,A,4"
    And Verify the invoice details such as Account, Order and payment details of invoice is displayed on Invoice page
    Then User adds SAP account name on opportunity page SAP Account "Opportunity,Q,2"
    Then User create new invoice under invoice section with deposit Record Type "Opportunity_Invoice,A,2"
    And Verify the second invoice details such as Account, Sap, Order and payment details of an invoice is displayed on Invoice page
    Then Logout from the Special vehicle operations Portal

  @SVO_Opportunity_Invoice_06 @SSEC-1113 @SVO_Opportunity_Invoice_Batch
  Scenario: User is able to view opportunity's invoice under 'Agreement & Invoices' section of an Opportunity
    Given Access to SVO Portal as classic service Username user with Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    When User navigates to an Opportunity tab
    And User creates Classic Opportunity with mandatory fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,7" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" SAP Account "Opportunity,Q,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then Move the Opportunity to Order placed stage Vehicle name "Opportunity_Invoice,B,2"
    And Verify that Account and Order details are displayed for the selected classic opportunity
    Then User create new invoice under invoice section with deposit Record Type "Opportunity_Invoice,A,2"
    And Verify the invoice details such as Account, Sap, Order and payment details of deposit invoice is displayed on Invoice page
    And User navigates to invoice from agreement and invoice section
    And Verify the invoice details such as Account, Sap, Order and payment details from agreements and invoice section
    Then Logout from the Special vehicle operations Portal

  @SVO_Opportunity_Invoice_07 @SSEC-1109 @SVO_Opportunity_Invoice_Batch
  Scenario: User is not able to create an invoice for an nonexisting opportunity
    Given Access to SVO Portal as Classic Admin user with Username "LoginUsers,B,13" and Password "LoginUsers,C,13"
    When User navigates to an Opportunity tab
    And User creates Classic Opportunity with mandatory fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,7" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" SAP Account "Opportunity,Q,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then Move Opportunity to Order placed stage Vehicle name "Opportunity_Invoice,B,2"
    And Navigate to Invoice section of an Opportunity
    Then User tries to save the invoice after removing the Opportunity field
    And Verify the error message displayed to select the opportunity field
    Then Logout from the Special vehicle operations Portal

  @SVO_Opportunity_Invoice_08 @SSEC-1111 @SVO_Opportunity_Invoice_Batch
  Scenario: User is not able to view Account, SAP Account and Order details for opportunity which is in 'Qualified' stage
    Given Access to SVO Portal as Classic Admin user with Username "LoginUsers,B,13" and Password "LoginUsers,C,13"
    When User navigates to an Opportunity tab
    And User creates Classic Opportunity with mandatory fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,7" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" SAP Account "Opportunity,Q,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User create new invoice under invoice section with deposit Record Type "Opportunity_Invoice,A,2"
    And Verify the invoice details such as Account, Sap, Order and payment details of deposit invoice are not displayed on Invoice page
    Then Logout from the Special vehicle operations Portal

  @SVO_Opportunity_Invoice_09 @SSEC-1114 @SVO_Opportunity_Invoice_Batch
  Scenario: User is able to move the Opportunity's invoice stage from 'Planned request' to 'SAP Invoice paid'
    Given Access to SVO Portal with Classic Service user with the Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    When User navigates to an Opportunity tab
    And User creates Classic Opportunity with mandatory fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,7" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" SAP Account "Opportunity,Q,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then Move the Opportunity to Order placed stage Vehicle name "Opportunity_Invoice,B,2"
    And Navigate to Invoice section of an Opportunity
    Then User creates a Deposit type of invoice
    And Verify that the invoice stage is in Planned Request stage
    Then Update the created Invoice with all pricing details and sap pricing details of an Invoice
    Then Verify that the invoice stage is moved to SAP invoice paid stage
    Then Logout from the Special vehicle operations Portal

  @SVO_Opportunity_Invoice_10 @SSEC-1115 @SVO_Opportunity_Invoice_Batch
  Scenario: User is able to Edit the invoice of an Order placed classic Opportunity
    Given Access to SVO Portal with Classic Service user with the Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    When User navigates to an Opportunity tab
    And User creates Classic Opportunity with mandatory fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,7" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" SAP Account "Opportunity,Q,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then Move the Opportunity to Order placed stage Vehicle name "Opportunity_Invoice,B,2"
    And Navigate to Invoice section of an Opportunity
    Then User creates a Deposit type of invoice with all pricing details
    Then Update the created Invoice with all pricing details and sap pricing details of an Invoice
    Then Logout from the Special vehicle operations Portal

  @SVO_Opportunity_Invoice_11 @SSEC-1116 @SVO_Opportunity_Invoice_Batch
  Scenario: User is not able to create an invoice for SVO Bespoke Opportunity
    Given Access to SVO Portal by bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    When User navigates to an Opportunity tab
    Then Verify that user is not able to view Bespoke Opportunities from select list view
    Then Logout from the Special vehicle operations Portal

  @SVO_Opportunity_Invoice_12 @SSEC-1117 @SVO_Opportunity_Invoice_Batch
  Scenario: User is not able to create single invoice for multiple Opportunities
    Given Access to SVO Portal as Classic Admin user with Username "LoginUsers,B,13" and Password "LoginUsers,C,13"
    When User navigates to an Opportunity tab
    And User creates Classic Opportunity with mandatory fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,7" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" SAP Account "Opportunity,Q,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then Move Opportunity to Order placed stage Vehicle name "Opportunity_Invoice,B,2"
    And Navigate to Invoice section of an Opportunity
    Then Verify that user is not able to link multiple opportunities for single Invoice
    Then Logout from the Special vehicle operations Portal
