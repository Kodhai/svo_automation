Feature: Validations on Classic Sales and Services Case record types

  #User is able to create new case object with Classic Sales record type on Salesforce.
  @SSEC_1565_CaseRecordTypes_001
  Scenario: User is able to create new case object with Classic Sales record type on Salesforce.
    Given Access to SVO Portal as Classic Sales user with Username "LoginUsers,B,5" and password "LoginUsers,C,5"
    And User Navigate to cases tab
    Then User creates classic sales case with details like Record Type "CaseCreation,C,4" Product Offering "CaseCreation,O,3" Rating "CaseCreation,P,2" and Case Origin "CaseCreation,Q,3"
    And Verify the case origin of newly created case
    And Verify the case record type of newly created case
    Then User Logouts from SVO Portal

  #User is able to create new case object with Classic Service record type on Salesforce
  @SSEC_1565_CaseRecordTypes_002
  Scenario: User is able to create new case object with Classic Service record type on Salesforce
    Given Access to SF SVO Portal as Classic Service user with Username "LoginUsers,B,12" and Password "LoginUsers,B,12"
    And User Navigate to cases tab
    Then User creates classic Service case with details like Record Type "CaseCreation,C,5" Product Offering "CaseCreation,O,3" Rating "CaseCreation,P,2" and Case Origin "CaseCreation,Q,3"
    And Verify the case origin of newly created case
    And Verify the case record type of newly created case
    Then User Logouts from SVO Portal

  #User is able to move newly created Classic Service case to Closed status on Salesforce.
  @SSEC_1565_CaseRecordTypes_003
  Scenario: User is able to move newly created Classic Service case to Closed status on Salesforce.
    Given Access to SF SVO Portal as Classic Service user with Username "LoginUsers,B,12" and Password "LoginUsers,B,12"
    And User Navigate to cases tab
    Then User creates classic Service case with details like Record Type "CaseCreation,C,5" Product Offering "CaseCreation,O,3" Rating "CaseCreation,P,2" and Case Origin "CaseCreation,Q,3"
    Then User Navigate to Activity tab and logs a call from Activity tab
    And Verify the status of newly created case on case details page
    And User Moves the newly created Case status to Closed
    And Verify the status of case on case details page
    Then User Logouts from SVO Portal

  #User is able to change the owner of Closed Classic sales case to Parts and Technical user on Salesforce
  @SSEC_1565_CaseRecordTypes_004
  Scenario: User is able to change the owner of Closed Classic sales case to Parts and Technical user on Salesforce
    Given Access to SVO Portal as Classic Sales user with Username "LoginUsers,B,5" and password "LoginUsers,C,5"
    And User Navigate to cases tab
    Then User creates classic sales case with details like Record Type "CaseCreation,C,5" Product Offering "CaseCreation,O,3" Rating "CaseCreation,P,2" and Case Origin "CaseCreation,Q,3"
    And Verify the case origin of newly created case
    Then User Navigate to Activity tab and logs a call from Activity tab
    And Verify the status of newly created case on case details page
    And User Moves the newly created Case status to Closed
    And Verify the status of case on case details page
    Then Change the owner of case to "AdditionalOwner,A,6"
    Then User Logouts from SVO Portal

  #User is able to move Closed classic service case to New status on Salesforce
  @SSEC_1565_CaseRecordTypes_005
  Scenario: User is able to move Closed classic service case to New status on Salesforce
    Given Access to SVO Portal as Classic Sales user with Username "LoginUsers,B,5" and password "LoginUsers,C,5"
    And User Navigate to cases tab
    Then User creates classic Service case with details like Record Type "CaseCreation,C,5" Product Offering "CaseCreation,O,3" Rating "CaseCreation,P,2" and Case Origin "CaseCreation,Q,3"
    And Verify the case origin of newly created case
    Then User Navigate to Activity tab and logs a call from Activity tab
    And Verify the status of newly created case on case details page
    And User Moves the newly created Case status to Closed
    And Verify the status of case on case details page
    When User reopen the closed classic service case	
    And Verify that user is able to move closed classic service case to New status
    Then User Logouts from SVO Portal

  #User is not able to change the owner of Classic service case to non-classic user on salesforce
  @SSEC_1565_CaseRecordTypes_006
  Scenario: User is not able to change the owner of Classic service case to non-classic user on salesforce
    Given Access to SF SVO Portal as Classic Service user with Username "LoginUsers,B,12" and Password "LoginUsers,B,12"
    And User Navigate to cases tab
    Then User creates classic Service case with details like Record Type "CaseCreation,C,5" Product Offering "CaseCreation,O,3" Rating "CaseCreation,P,2" and Case Origin "CaseCreation,Q,3"
    And Verify the status of newly created case on case details page
    Then Verify that user is not able to Change the owner of case to "AdditionalOwner,A,3"
    Then User Logouts from SVO Portal

  #User is able to change record type of closed classic sales case on Salesforce
  @SSEC_1565_CaseRecordTypes_007
  Scenario: User is able to change record type of closed classic sales case on Salesforce
    Given Access to SVO Portal as Classic Sales user with Username "LoginUsers,B,5" and password "LoginUsers,C,5"
    And User Navigate to cases tab
    Then User creates classic sales case with details like Record Type "CaseCreation,C,4" Product Offering "CaseCreation,O,3" Rating "CaseCreation,P,2" and Case Origin "CaseCreation,Q,3"
    Then Verify the case record type of newly created case
    Then User Navigate to Activity tab and logs a call from Activity tab
    And Verify the status of newly created case on case details page
    And User Moves the newly created Case status to Closed
    And Verify the status of case on case details page
    And Change the record type of closed classic sales case
    Then Verify the case record type of closed case on case details page
    Then User Logouts from SVO Portal

  #User is able to delete Classic Service case which is in 'Contacted' status on Salesforce.
  @SSEC_1565_CaseRecordTypes_008
  Scenario: User is able to delete Classic Service case which is in 'Contacted' status on Salesforce.
    Given Access to SF SVO Portal as Classic Service user with Username "LoginUsers,B,12" and Password "LoginUsers,B,12"
    And User Navigate to cases tab
    Then User creates classic Service case with details like Record Type "CaseCreation,C,5" Product Offering "CaseCreation,O,3" Rating "CaseCreation,P,2" and Case Origin "CaseCreation,Q,3"
    And Verify the case origin of newly created case
    Then User Navigate to Activity tab and logs a call from Activity tab
    And Verify the status of newly created case on case details page
    Then Move newly created case to Contacted status
    And Verify the status of case on case details page
    Then Verify that user is able to delete the Classic service case which is in Contacted status
    Then User Logouts from SVO Portal

  #User is not able to create Classic sales case on Salesforce
  @SSEC_1565_CaseRecordTypes_009
  Scenario: User is not able to create Classic sales case on Salesforce
    Given User logins to SVO portal as SVO Bespoke User UserName "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User Verify that there is no Cases Tab available
    Then User Logouts from SVO Portal
	