@PRO
Feature: Script to add Provisional Order Form in Classic Sales User only for Enquiry and KMI

  @SSEC_1533_01
  Scenario: User is able to add Provisional Order Form Signed information in Classic Sales KMI & Enquiry
    Given Access to SVO portal for Classic Sales using Username "LoginUsers,B,5" and password "LoginUsers,C,5"
    And User navigates to Enquiry tab
    Then User creates new Classic Sales enquiry
    And Enter the Enquiry Title for the new Enquiry creation
    And User fills Preferred Contact as "Enquiry,I,2"
    Then User fills required details such as Product Offering "Enquiry,B,4" Brand "Enquiry,J,4" Model "Enquiry,K,4"
    And Verify that user is able to add Provisional Order Form Signed information while creating Classic Sales enquiry
    And User Saves the enquiry
    Then User logs a call in the activity tab
    Then User closes the enquiry with status as Qualified KMI
    Then User verifies new KMI record gets created and navigates to it
    And Verify that Provisonal Order Form is dispalyed
    Then User logout from portal

  @PF2
  Scenario: User not able to add Provisional Order Form Signed information in Classic Service Enquiry
    Given Access to SVO portal for Classic Service user using Username "LoginUsers,B,12" and password "LoginUsers,C,12"
    And User navigates to Enquiry tab
    Then User creates new Classic Service enquiry with record type "Enquiry,A,2"
    And Verify that user is unable to add Provisional Order Form Signed Information on the Classic Service enquiry page
    Then User logout from portal
    
  @PF3
  Scenario: User is able to edit added Provisional Order Form Signed information for Classic Sales enquiry
    Given Access to SVO portal for Classic Sales using Username "LoginUsers,B,5" and password "LoginUsers,C,5"
    And User navigates to Enquiry tab
    Then User creates new Classic Sales enquiry
     And Enter the Enquiry Title for the new Enquiry creation
    And User fills Preferred Contact as "Enquiry,I,2"
    Then User fills required details such as Product Offering "Enquiry,B,4" Brand "Enquiry,J,4" Model "Enquiry,K,4"
    And Verify that user is able to add Provisional Order Form Signed information while creating Classic Sales enquiry 
    And User Saves the enquiry
    Then User logs a call in the activity tab
    Then User closes the enquiry with status as Qualified KMI
    Then User verifies new KMI record gets created in Quick Links
    Then Verify that user is able to edit added Provisional Order Form Signed information on enquiry page
    Then User logout from portal
    
     @PF4
  Scenario: User is not able to add Provisional Order Form Signed information implemented for Classic Customer Service user
    Given Access to SVO portal for Customer Services User with UserName "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User navigates to the enquiry tab
    And User creates new Customer Services record type
    And User verifies that no Provisional Order Form Signed checkbox is available
    Then User cancels the case creation
    And User logout from portal
    
  @PF5
  Scenario: User is able to edit Provisional Order Form Signed information added on KMI Page
    Given Access to SVO portal for Classic Sales using Username "LoginUsers,B,5" and password "LoginUsers,C,5"
    And User navigates to Enquiry tab
    Then User creates new Classic Sales enquiry
    And Enter the Enquiry Title for the new Enquiry creation
    And User fills Preferred Contact as "Enquiry,I,2"
    Then User fills required details such as Product Offering "Enquiry,B,4" Brand "Enquiry,J,4" Model "Enquiry,K,4"
    And Verify that user is able to add Provisional Order Form Signed information while creating Classic Sales enquiry
    And User Saves the enquiry
    Then User logs a call in the activity tab
    Then User closes the enquiry with status as Qualified KMI
    Then User verifies new KMI record gets created and navigates to it
    And Verify that user is able to edit added Provisional Order Form Signed information in KMI section
    Then User logout from portal
    
  @PF6
  Scenario: User is able to remove added Provisional Order Form Signed information for Classic Sales enquiry
    Given Access to SVO portal for Classic Sales using Username "LoginUsers,B,5" and password "LoginUsers,C,5"
    And User navigates to Enquiry tab
    Then User creates new Classic Sales enquiry
    And Enter the Enquiry Title for the new Enquiry creation
    And User fills Preferred Contact as "Enquiry,I,2"
    Then User fills required details such as Product Offering "Enquiry,B,4" Brand "Enquiry,J,4" Model "Enquiry,K,4"
    And Verify that user is able to add Provisional Order Form Signed information while creating Classic Sales enquiry
    And User Saves the enquiry
    Then Verify that user is able to remove added Provisional Order Form Signed information for that enquiry
    Then User logout from portal


  @PF7
  Scenario: User is not able to view edited Provisional Order Form Signed information from Enquiry page 
    Given Access to SVO portal for Classic Sales using Username "LoginUsers,B,5" and password "LoginUsers,C,5"
    And User navigates to Enquiry tab
    Then User creates new Classic Sales enquiry
    And Enter the Enquiry Title for the new Enquiry creation
    And User fills Preferred Contact as "Enquiry,I,2"
    Then User fills required details such as Product Offering "Enquiry,B,4" Brand "Enquiry,J,4" Model "Enquiry,K,4"
    And Verify that user is able to add Provisional Order Form Signed information while creating Classic Sales enquiry
    And User Saves the enquiry
    Then User logs a call in the activity tab
    Then User closes the enquiry with status as Qualified KMI
    Then User verifies new KMI record gets created and navigates to it
    And Verifies that user is not able to view edited Provisional Order Form Signed information from Enquiry page
    Then User logout from portal
      
  @PF8
  Scenario: User is not able to view edited Provisional Order Form Signed information from KMI section  
    Given Access to SVO portal for Classic Sales using Username "LoginUsers,B,5" and password "LoginUsers,C,5"
    And User navigates to Enquiry tab
    Then User creates new Classic Sales enquiry
    And Enter the Enquiry Title for the new Enquiry creation
    And User fills Preferred Contact as "Enquiry,I,2"
    Then User fills required details such as Product Offering "Enquiry,B,4" Brand "Enquiry,J,4" Model "Enquiry,K,4"
    And Verify that user is able to add Provisional Order Form Signed information while creating Classic Sales enquiry
    And User Saves the enquiry
    Then User logs a call in the activity tab
    Then User closes the enquiry with status as Qualified KMI
    Then User verifies new KMI record gets created and navigates to it
    And Verify that user is able to edit added Provisional Order Form Signed information in KMI section
    Then Verify that user is not able to view edited Provisional Order Form Signed information from KMI page
    Then User logout from portal









  







    
    
  