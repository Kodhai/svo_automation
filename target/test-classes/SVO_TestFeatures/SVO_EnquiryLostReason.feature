Feature: SVO Enquiry loss Reason validations

  @SVO_EnquiryLostReason_01 @SSEC-1134
  Scenario: User is able to close the Classic sales Enquiry with closed reason 'Enquiry transferred to Essen' on Enquiry page
    Given Access to SVO Portal as Classic Admin user with Username "LoginUsers,B,13" and Password "LoginUsers,C,13"
    Then User navigates to the Enquiries tab
    And User creates an Enquiry with record type "EnquiryLostReason,A,2"
    Then User verifies the SLA status of an created Enquiry
    And Log a call for an enquiry activity
    Then User verifies the SLA status of an created Enquiry
    Then User moves an enquiry to discovery stage by adding contact details "Opportunity,J,2"
    Then User close the enquiry with closed state "EnquiryLostReason,B,2" and closed reason "EnquiryLostReason,C,2" on Enquiry page
    And Verify the auto-populated fields of enqiry closed reason and closed state on Enquiry page
    Then User Logout from the SVO Portal

  @SVO_EnquiryLostReason_02 @SSEC-1135
  Scenario: User is able to edit closed reason for a Closed lost classic service Enquiry on Enquiry page
    Given Access to SVO Portal with Classic Service user with the Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    Then User navigates to the Enquiries tab
    And User creates an Enquiry with record type "EnquiryLostReason,A,3"
    Then User verifies the SLA status of an created Enquiry
    Then User close the enquiry with closed state "EnquiryLostReason,B,2" and closed reason "EnquiryLostReason,C,2" on Enquiry page
    And Verify the auto-populated fields of enqiry closed reason and closed state on Enquiry page
    Then User edit the closed reason of an enquiry to "EnquiryLostReason,C,2"
    And Verify the auto-populated fields of enqiry closed reason and closed state on Enquiry page
    Then User Logout from the SVO Portal

  @SVO_EnquiryLostReason_03 @SSEC-1136
  Scenario: User is able to close the Classic sales Enquiry from Discovery stage with closed reason 'Enquiry transferred to Essen' on Enquiry page
    Given Access to SVO Portal by private office user with Username "LoginUsers,B,4" and Password "LoginUsers,C,4"
    Then User navigates to the Enquiries tab
    And User creates an Enquiry with record type "EnquiryLostReason,A,2"
    Then User verifies the SLA status of an created Enquiry
    And Log a call for an enquiry activity
    Then User moves an enquiry to discovery stage by adding contact details "Opportunity,J,2"
    Then User close the enquiry with closed state "EnquiryLostReason,B,2" and closed reason "EnquiryLostReason,C,2" on Enquiry page
    And Verify the auto-populated fields of enqiry closed reason and closed state on Enquiry page
    Then User Logout from the SVO Portal

  @SVO_EnquiryLostReason_04 @SSEC-1137
  Scenario: User is able to close the Classic service Enquiry with closed reason 'Enquiry transferred to Essen' from 'New' stage of an Enquiry
    Given Access to SVO Portal with Classic Service user with the Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    Then User navigates to the Enquiries tab
    And User creates an Enquiry with record type "EnquiryLostReason,A,3"
    Then User verifies the SLA status of an created Enquiry
    Then User close the enquiry with closed state "EnquiryLostReason,B,2" and closed reason "EnquiryLostReason,C,2" on Enquiry page
    And Verify the auto-populated fields of enqiry closed reason and closed state on Enquiry page
    Then User Logout from the SVO Portal

  @SVO_EnquiryLostReason_05 @SSEC-1138
  Scenario: User is not able to move the classic service Enquiry status from 'Closed lost' to 'New' status on Enquiry page
    Given Access to SVO Portal as Classic Admin user with Username "LoginUsers,B,13" and Password "LoginUsers,C,13"
    Then User navigates to the Enquiries tab
    And User creates an Enquiry with record type "EnquiryLostReason,A,3"
    Then User verifies the SLA status of an created Enquiry
    Then User close the enquiry with closed state "EnquiryLostReason,B,2" and closed reason "EnquiryLostReason,C,2" on Enquiry page
    And Verify the auto-populated fields of enqiry closed reason and closed state on Enquiry page
    Then User tries to re-open the enquiry status "EnquiryLostReason,D,2"
    And Verify the error message displayed on Enquiry page
    Then User Logout from the SVO Portal

  @SVO_EnquiryLostReason_06 @SSEC-1139
  Scenario: User is able to edit the details of an closed lost classic sales enquiry on Enquiry page
    Given Access to SVO Portal with Classic Service user with the Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    Then User navigates to the Enquiries tab
    And User creates an Enquiry with record type "EnquiryLostReason,A,2"
    Then User verifies the SLA status of an created Enquiry
    Then User close the enquiry with closed state "EnquiryLostReason,B,2" and closed reason "EnquiryLostReason,C,2" on Enquiry page
    And Verify the auto-populated fields of enqiry closed reason and closed state on Enquiry page
    Then User Edit the Enquiry title for an closed lost enquiry
    And Verify that the enquiry title is updated with the edited value on enquiry page
    Then User Logout from the SVO Portal

  @SVO_EnquiryLostReason_07 @SSEC-1140
  Scenario: User is not able to view updated lost reason when record type of classic service enquiry is changed on Enquiry page
    Given Access to SVO Portal as Classic Admin user with Username "LoginUsers,B,13" and Password "LoginUsers,C,13"
    Then User navigates to the Enquiries tab
    And User creates an Enquiry with record type "EnquiryLostReason,A,2"
    Then User verifies the SLA status of an created Enquiry
    Then User close the enquiry with closed state "EnquiryLostReason,B,2" and closed reason "EnquiryLostReason,C,2" on Enquiry page
    And Verify the auto-populated fields of enqiry closed reason and closed state on Enquiry page
    Then Verify that existing closed reason picklist is not updated when record type changed on Enquiry page
    And Verify the auto-populated fields of enqiry closed reason and closed state on Enquiry page
    Then User Logout from the SVO Portal

  @SVO_EnquiryLostReason_08 @SSEC-1141
  Scenario: User is able to delete the closed lost classic sales enquiry record from enquiry page
    Given Access to SVO Portal as Classic Admin user with Username "LoginUsers,B,13" and Password "LoginUsers,C,13"
    Then User navigates to the Enquiries tab
    And User creates an Enquiry with record type "EnquiryLostReason,A,2"
    Then User verifies the SLA status of an created Enquiry
    Then User close the enquiry with closed state "EnquiryLostReason,B,2" and closed reason "EnquiryLostReason,C,2" on Enquiry page
    And Verify the auto-populated fields of enqiry closed reason and closed state on Enquiry page
    Then User delete the closed lost Enquiry from Enquiry page
    Then User Logout from the SVO Portal

  @SVO_EnquiryLostReason_09 @SSEC-1142
  Scenario: User is not able to delete the closed lost classic sales enquiry record from enquiry page
    Given Access to SVO Portal with Classic Service user with the Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    Then User navigates to the Enquiries tab
    And User creates an Enquiry with record type "EnquiryLostReason,A,2"
    Then User verifies the SLA status of an created Enquiry
    Then User close the enquiry with closed state "EnquiryLostReason,B,2" and closed reason "EnquiryLostReason,C,2" on Enquiry page
    And Verify the auto-populated fields of enqiry closed reason and closed state on Enquiry page
    Then Verify that user is not able to delete the closed lost Enquiry from Enquiry page
    Then User Logout from the SVO Portal

  @SVO_EnquiryLostReason_10 @SSEC-1143
  Scenario: User is not able to close an SVO Bespoke Enquiry with closed reason 'Enquiry transferred to Essen'
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Enquiries tab
    And User creates an Enquiry with record type "EnquiryLostReason,A,4"
    Then User verifies the SLA status of an created Enquiry
    And Verify that user is not able to close the enquiry with closed state "EnquiryLostReason,B,2" and closed reason "EnquiryLostReason,C,2"
    Then User Logout from the SVO Portal

  @SVO_EnquiryLostReason_11 @SSEC-1144
  Scenario: User is able to close the SLA breached classic sales Enquiry with closed reason 'Enquiry transferred to Essen'
    Given Access to SVO Portal as Classic Admin user with Username "LoginUsers,B,13" and Password "LoginUsers,C,13"
    Then User navigates to the Enquiries tab
    And User select an SLA breached classic sales enquiry which is in stage New
    Then User close the enquiry with closed state "EnquiryLostReason,B,2" and closed reason "EnquiryLostReason,C,2" on Enquiry page
    And Verify the auto-populated fields of enqiry closed reason and closed state on Enquiry page
    Then User Logout from the SVO Portal

  @SVO_EnquiryLostReason_12 @SSEC-1145
  Scenario: User is not able to close an SVO Private office Enquiry with closed reason 'Enquiry transferred to Essen'
    Given Access to SVO Portal by private office user with Username "LoginUsers,B,4" and Password "LoginUsers,C,4"
    Then User navigates to the Enquiries tab
    And User creates an Enquiry with record type "EnquiryLostReason,A,5"
    Then User verifies the SLA status of an created Enquiry
    And Verify that user is not able to close the enquiry with closed state "EnquiryLostReason,B,2" and closed reason "EnquiryLostReason,C,2"
    Then User Logout from the SVO Portal
