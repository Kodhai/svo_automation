Feature: Remove Items To Approve Section validations

  #Verify that 'Item to Approve' section is not available on SVO portal
  @RemoveItemsToApproveSection_01 @Batch @SSEC-
  Scenario: Verify that 'Item to Approve' section is not available on SVO portal
    Given Access to SVO Portal by bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then Verify that Items to approve section is not available on home page
    And Logout from the Special vehicle operations Portal

  #User is not able to view 'Item to Approve' section on Home Page of SVO portal
  @RemoveItemsToApproveSection_02 @Batch @SSEC-
  Scenario: User is not able to view 'Item to Approve' section on Home Page of SVO portal
    Given Access to SVO bespoke operation Portal with Username "LoginUsers,B,8" and Password "LoginUsers,C,8"
    Then Verify that Items to approve section is not available on home page
    And Logout from the Special vehicle operations Portal

  #Verify that 'Item to Approve' section is not present on SVO portal
  @RemoveItemsToApproveSection_03 @Batch @SSEC-
  Scenario: Verify that 'Item to Approve' section is not present on SVO portal
    Given Access to SVO Portal as Classic Service user with Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    Then Verify that Items to approve section is not available on home page
    And Logout from the Special vehicle operations Portal

  #User is not able to view 'Item to Approve' section on Home Page of SVO portal
  @RemoveItemsToApproveSection_04 @Batch @SSEC-
  Scenario: User is not able to view 'Item to Approve' section on Home Page of SVO portal
    Given Access to SVO Portal by private office user with Username "LoginUsers,B,4" and Password "LoginUsers,C,4"
    Then Verify that Items to approve section is not available on home page
    And Logout from the Special vehicle operations Portal

  #Verify that 'Item to Approve' section is not available on SVO portal
  @RemoveItemsToApproveSection_05 @Batch @SSEC-
  Scenario: Verify that 'Item to Approve' section is not available on SVO portal
    Given Access to SO data management Portal with Username "LoginUsers,B,11" and Password "LoginUsers,C,11"
    Then Verify that Items to approve section is not available on home page
    And Logout from the Special vehicle operations Portal
