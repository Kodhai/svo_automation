@SSEC_2092
Feature: Validate Bespoke Order Form-design brief page based on the Vehicle Line

  @SSEC_2092_01
  Scenario: User is able to display the design brief record type created for L460 model whenever user selects 460AD model in the Opportunity page
   Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
   Then User navigates to the Opportunity tab
   And User creates a new Bespoke Opportunity where it selects model as 460AD
   Then User verifies that new Opportunity is created
   Then User navigates to Quotes from Related Links
   And User creates new Bespoke Design Brief quotes with mandatory details
   Then User verifies that new quote is created
   Then Navigate to newly created Bespoke Design Brief quote then navigate to Design Brief in the related quick links
   And Click on Design Brief quote name and click on Submit button
   Then Click on return render pack button
   Then User navigates to the Opportunity tab
   And Click on first record and verify that model is 460AD
   Then User Logout from SVO Portal
   
   @SSEC_2092_02
   Scenario: User is able to display the design brief record type created for L460 model whenever user selects 460AH model in the Opportunity page
   Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
   Then User navigates to the Opportunity tab
   And User creates a new Bespoke Opportunity where it selects model as 460AH
   Then User verifies that new Opportunity is created
   Then User navigates to Quotes from Related Links
   And User creates new Bespoke Design Brief quotes with mandatory details
   Then User verifies that new quote is created
   Then Navigate to newly created Bespoke Design Brief quote then navigate to Design Brief in the related quick links
   And Click on Design Brief quote name and click on Submit button
   Then Click on return render pack button
   Then User navigates to the Opportunity tab
   And Click on first record and verify that model is 460AH
   Then User Logout from SVO Portal
   
   @SSEC_2092_03
   Scenario: User is able to display the design brief record type created for L460 model whenever user selects 460AH model in the Opportunity page
   Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
   Then User navigates to the Opportunity tab
   And User creates a new Bespoke Opportunity where it selects model as 460AM
   Then User verifies that new Opportunity is created
   Then User navigates to Quotes from Related Links
   And User creates new Bespoke Design Brief quotes with mandatory details
   Then User verifies that new quote is created
   Then Navigate to newly created Bespoke Design Brief quote then navigate to Design Brief in the related quick links
   And Click on Design Brief quote name and click on Submit button
   Then Click on return render pack button
   Then User navigates to the Opportunity tab
   And Click on first record and verify that model is 460AM
   Then User Logout from SVO Portal
   
   @SSEC_2092_04
   Scenario: User is able to display the design brief record type created for L460 model whenever user selects 460AH model in the Opportunity page
   Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
   Then User navigates to the Opportunity tab
   And User creates a new Bespoke Opportunity where it selects model as 460AS
   Then User verifies that new Opportunity is created
   Then User navigates to Quotes from Related Links
   And User creates new Bespoke Design Brief quotes with mandatory details
   Then User verifies that new quote is created
   Then Navigate to newly created Bespoke Design Brief quote then navigate to Design Brief in the related quick links
   And Click on Design Brief quote name and click on Submit button
   Then Click on return render pack button
   Then User navigates to the Opportunity tab
   And Click on first record and verify that model is 460AS
   Then User Logout from SVO Portal
   
   @SSEC_2092_05
   Scenario: User is able to display the design brief record type created for L460 model whenever user selects 460AH model in the Opportunity page
   Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
   Then User navigates to the Opportunity tab
   And User creates a new Bespoke Opportunity where it selects model as 460AW
   Then User verifies that new Opportunity is created
   Then User navigates to Quotes from Related Links
   And User creates new Bespoke Design Brief quotes with mandatory details
   Then User verifies that new quote is created
   Then Navigate to newly created Bespoke Design Brief quote then navigate to Design Brief in the related quick links
   And Click on Design Brief quote name and click on Submit button
   Then Click on return render pack button
   Then User navigates to the Opportunity tab
   And Click on first record and verify that model is 460AW
   Then User Logout from SVO Portal
   
   @SSEC_2092_06
   Scenario: User is able to display the design brief record type created for L460 model whenever user selects 460AH model in the Opportunity page
   Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
   Then User navigates to the Opportunity tab
   And User creates a new Bespoke Opportunity where it selects model as 460BB
   Then User verifies that new Opportunity is created
   Then User navigates to Quotes from Related Links
   And User creates new Bespoke Design Brief quotes with mandatory details
   Then User verifies that new quote is created
   Then Navigate to newly created Bespoke Design Brief quote then navigate to Design Brief in the related quick links
   And Click on Design Brief quote name and click on Submit button
   Then Click on return render pack button
   Then User navigates to the Opportunity tab
   And Click on first record and verify that model is 460BB
   Then User Logout from SVO Portal
   
   @SSEC_2092_07
   Scenario: User is able to display the design brief record type created for L460 model whenever user selects 460BV model in the Opportunity page
   Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
   Then User navigates to the Opportunity tab
   And User creates a new Bespoke Opportunity where it selects model as 460BV
   Then User verifies that new Opportunity is created
   Then User navigates to Quotes from Related Links
   And User creates new Bespoke Design Brief quotes with mandatory details
   Then User verifies that new quote is created
   Then Navigate to newly created Bespoke Design Brief quote then navigate to Design Brief in the related quick links
   And Click on Design Brief quote name and click on Submit button
   Then Click on return render pack button
   Then User navigates to the Opportunity tab
   And Click on first record and verify that model is 460BV
   Then User Logout from SVO Portal
   
   @SSEC_2092_08
   Scenario: User is able to display the design brief record type created for L460 model whenever user selects 460BZ model in the Opportunity page
   Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
   Then User navigates to the Opportunity tab
   And User creates a new Bespoke Opportunity where it selects model as 460BZ
   Then User verifies that new Opportunity is created
   Then User navigates to Quotes from Related Links
   And User creates new Bespoke Design Brief quotes with mandatory details
   Then User verifies that new quote is created
   Then Navigate to newly created Bespoke Design Brief quote then navigate to Design Brief in the related quick links
   And Click on Design Brief quote name and click on Submit button
   Then Click on return render pack button
   Then User navigates to the Opportunity tab
   And Click on first record and verify that model is 460BZ
   Then User Logout from SVO Portal
   
   @SSEC_2092_09
   Scenario: User is able to display the design brief record type created for L460 model whenever user selects 460CD model in the Opportunity page
   Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
   Then User navigates to the Opportunity tab
   And User creates a new Bespoke Opportunity where it selects model as 460CD
   Then User verifies that new Opportunity is created
   Then User navigates to Quotes from Related Links
   And User creates new Bespoke Design Brief quotes with mandatory details
   Then User verifies that new quote is created
   Then Navigate to newly created Bespoke Design Brief quote then navigate to Design Brief in the related quick links
   And Click on Design Brief quote name and click on Submit button
   Then Click on return render pack button
   Then User navigates to the Opportunity tab
   And Click on first record and verify that model is 460CD
   Then User Logout from SVO Portal
   
   @SSEC_2092_10
   Scenario: User is able to display the design brief record type created for L460 model whenever user selects 460CH model in the Opportunity page
   Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
   Then User navigates to the Opportunity tab
   And User creates a new Bespoke Opportunity where it selects model as 460CH
   Then User verifies that new Opportunity is created
   Then User navigates to Quotes from Related Links
   And User creates new Bespoke Design Brief quotes with mandatory details
   Then User verifies that new quote is created
   Then Navigate to newly created Bespoke Design Brief quote then navigate to Design Brief in the related quick links
   And Click on Design Brief quote name and click on Submit button
   Then Click on return render pack button
   Then User navigates to the Opportunity tab
   And Click on first record and verify that model is 460CH
   Then User Logout from SVO Portal
   
   @SSEC_2092_11
   Scenario: User is able to display the design brief record type created for L460 model whenever user selects 460CX model in the Opportunity page
   Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
   Then User navigates to the Opportunity tab
   And User creates a new Bespoke Opportunity where it selects model as 460CX
   Then User verifies that new Opportunity is created
   Then User navigates to Quotes from Related Links
   And User creates new Bespoke Design Brief quotes with mandatory details
   Then User verifies that new quote is created
   Then Navigate to newly created Bespoke Design Brief quote then navigate to Design Brief in the related quick links
   And Click on Design Brief quote name and click on Submit button
   Then Click on return render pack button
   Then User navigates to the Opportunity tab
   And Click on first record and verify that model is 460CX
   Then User Logout from SVO Portal
   
   @SSEC_2092_12
   Scenario: User is able to display the design brief record type created for L460 model whenever user selects 460CZ model in the Opportunity page
   Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
   Then User navigates to the Opportunity tab
   And User creates a new Bespoke Opportunity where it selects model as 460CZ
   Then User verifies that new Opportunity is created
   Then User navigates to Quotes from Related Links
   And User creates new Bespoke Design Brief quotes with mandatory details
   Then User verifies that new quote is created
   Then Navigate to newly created Bespoke Design Brief quote then navigate to Design Brief in the related quick links
   And Click on Design Brief quote name and click on Submit button
   Then Click on return render pack button
   Then User navigates to the Opportunity tab
   And Click on first record and verify that model is 460CZ
   Then User Logout from SVO Portal
 