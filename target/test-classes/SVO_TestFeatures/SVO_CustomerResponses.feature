Feature: SVO Customer Responses Validations

  @SVO_CustomerResponses_01 @SSEC-1195 @SVO_CustomerResponses_Batch
  Scenario: User is able to receive customer response notification for private office enquiry on SVO portal
    Given Access to mail account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    When Customer send an enquiry mail to user "CustomerResponse,A,2"
    And Logout from users mail account
    Given Login to SVO Portal by private office user with Username "LoginUsers,B,4" and Password "LoginUsers,C,4"
    And Verify automatically created private office enquiry on SVO Portal
    Then Verify that Created date, Enquiry title and Enquiry status on Enquiries page
    And Verify that subject name is auto-populated on emails page
    Then User send an email to customer "LoginUsers,B,17" with Business unit "CustomerResponse,I,3" and subject
    And Verify that enquiry status on enquiries page
    Then User update owner as UAT test for an private office enquiry Owner Name "LoginUsers,B,16"
    And Verify that the enquiry is removed from the list view for which the owner has been changed
    Then User Logout from SVO Portal
    Given Access to user mail account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    Then Verify that Customer receives an enquiry update email from user
    And Customer reply to received enquiry update request email
    And Logout from users mail account
    Given Access to SVO Portal by Owner user with Username "LoginUsers,B,16" and Password "LoginUsers,C,16"
    Then Verify the email details of an enquiry from custom notification received
    And Verify that customer response email subject is displayed under emails section of an enquiry
    Then User Logout from SVO Portal
    Given Access to owner mail account with Username "LoginUsers,B,16" and password "LoginUsers,C,16"
    Then User navigated to enquiry link from the mail received from customer
    And Logout from users mail account

  @SVO_CustomerResponses_02 @SSEC-1196 @SVO_CustomerResponses_Batch
  Scenario: User is able to receive customer response notification for classic service enquiry on SVO portal
    Given Access to mail account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    When Customer send an enquiry mail to user "CustomerResponse,A,4"
    Then Verify that Customer receives an acknowledgement email
    And Logout from users mail account
    Given Login to the SVO Portal by classic service uat user with Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    And Verify automatically created classic service enquiry on SVO Portal
    Then Verify that Created date, Enquiry title and Enquiry status on Enquiries page
    And Verify that subject name is auto-populated on emails page
    Then User send an email to customer "LoginUsers,B,17" with Business unit "CustomerResponse,I,4" and subject
    And Verify that enquiry status on enquiries page
    Then User update owner as UAT test for an enquiry Owner Name "LoginUsers,B,16"
    And Verify that the enquiry is removed from the list view for which the owner has been changed
    Then User Logout from SVO Portal
    Given Access to user mail account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    Then Verify that Customer receives an enquiry update email from user
    And Customer reply to received enquiry update request email
    And Logout from users mail account
    Given Access to SVO Portal by Owner user with Username "LoginUsers,B,16" and Password "LoginUsers,C,16"
    Then Verify the email details of an enquiry from custom notification received
    And Verify that customer response email subject is displayed under emails section of an enquiry
    Then User Logout from SVO Portal
    Given Access to owner mail account with Username "LoginUsers,B,16" and password "LoginUsers,C,16"
    Then User navigated to enquiry link from the mail received from customer
    And Logout from users mail account

  @SVO_CustomerResponses_03 @SSEC-1190 @SVO_CustomerResponses_Batch
  Scenario: User is able to receive customer response notification for classic sales enquiry on SVO portal
    Given Access to mail account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    When Customer send an enquiry mail to user "CustomerResponse,A,5"
    Then Verify that Customer receives an acknowledgement email
    And Logout from users mail account
    Given Login to SVO Portal as Classic Admin user with Username "LoginUsers,B,13" and Password "LoginUsers,C,13"
    And Verify that the New Classic Sales enquiry is created automatically on SVO Portal
    Then Verify that Created date, Enquiry title and Enquiry status on Enquiries page
    And Verify that subject name is auto-populated on emails page
    Then User send an email to customer "LoginUsers,B,17" with Business unit "CustomerResponse,I,3" and subject
    And Verify that enquiry status on enquiries page
    Then User update owner as UAT test for an enquiry Owner Name "LoginUsers,B,16"
    And Verify that the enquiry is removed from the list view for which the owner has been changed
    Then User Logout from the SVO Portal
    Given Access to gmail account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    Then Verify that Customer receives an enquiry update email from user
    And Customer reply to received enquiry update request email
    And Logout from users mail account
    Given Access to SVO Portal by Owner user with Username "LoginUsers,B,16" and Password "LoginUsers,C,16"
    Then Verify the email details of an enquiry from custom notification received
    And Verify that customer response email subject is displayed under emails section of an enquiry
    Then User Logout from the SVO Portal
    Given Access to gmail account with Username "LoginUsers,B,16" and password "LoginUsers,C,16"
    Then User navigates to enquiry link from the mail received from customer
    And Logout from users mail account

  @SVO_CustomerResponses_04 @SSEC-1197 @SVO_CustomerResponses_Batch
  Scenario: User is able to receive customer response notification for bespoke enquiry on SVO portal
    Given Access to mail account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    When Customer send an enquiry mail to user "CustomerResponse,A,3"
    Then Verify that Customer receives an acknowledgement email
    And Logout from users mail account
    Given Login to the SVO Portal by bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    And Verify that the New Bespoke enquiry is created automatically on SVO Portal
    Then Verify that Created date, Enquiry title and Enquiry status on Enquiries page
    And Verify that subject name is auto-populated on emails page
    Then User send an email to customer "LoginUsers,B,17" with Business unit "CustomerResponse,I,2" and subject
    And Verify that enquiry status on enquiries page
    Then User update owner as UAT test for an enquiry Owner Name "LoginUsers,B,16"
    And Verify that the enquiry is removed from the list view for which the owner has been changed
    Then User Logout from the SVO Portal
    Given Access to gmail account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    Then Verify that Customer receives an enquiry update email from user
    And Customer reply to received enquiry update request email
    And Logout from users mail account
    Given Access to SVO Portal by Owner user with Username "LoginUsers,B,16" and Password "LoginUsers,C,16"
    Then Verify the email details of an enquiry from custom notification received
    And Verify that customer response email subject is displayed under emails section of an enquiry
    Then User Logout from the SVO Portal
    Given Access to gmail account with Username "LoginUsers,B,16" and password "LoginUsers,C,16"
    Then User navigates to enquiry link from the mail received from customer
    And Logout from users mail account

  @SVO_CustomerResponses_05 @SSEC-1191 @SVO_CustomerResponses_Batch
  Scenario: User is able to receive multiple customer response notifications for single enquiry on SVO portal
    Given Access to mail account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    When Customer send an enquiry mail to user "CustomerResponse,A,3"
    Then Verify that Customer receives an acknowledgement email
    And Logout from users mail account
    Given Login to the SVO Portal by bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    And Verify that the New Bespoke enquiry is created automatically on SVO Portal
    Then Verify that Created date, Enquiry title and Enquiry status on Enquiries page
    And Verify that subject name is auto-populated on emails page
    Then User sends multiple emails to customer "LoginUsers,B,17" with Business unit "CustomerResponse,I,2" and subject
    And Verify that enquiry status on enquiries page
    Then User update owner as UAT test for an enquiry Owner Name "LoginUsers,B,16"
    And Verify that the enquiry is removed from the list view for which the owner has been changed
    Then User Logout from the SVO Portal
    Given Access to gmail account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    Then Verify that Customer receives multple response emails from user on a single enquiry
    And Customer reply to received enquiry update request email
    And Logout from users mail account
    Given Access to SVO Portal by Owner user with Username "LoginUsers,B,16" and Password "LoginUsers,C,16"
    Then Verify the email details of an enquiry from custom notification received
    And Verify that customer response email subject is displayed under emails section of an enquiry
    Then User Logout from the SVO Portal
    Given Access to gmail account with Username "LoginUsers,B,16" and password "LoginUsers,C,16"
    Then User navigates to enquiry link from the mail received from customer
    And Logout from users mail account

  @SVO_CustomerResponses_06 @SSEC-1198 @SVO_CustomerResponses_Batch
  Scenario: User is not  able to receive customer response notification for data manager enquiry on SVO portal
    Given Access to mail account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    When Customer send an enquiry mail to user "CustomerResponse,A,6"
    Then Verify that Customer is not receives an acknowledgement email
    And Logout from users mail account
    Given Login to the SVO Portal by bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    And Verify that no enquiry is created automatically on enquiries page
    Then User Logout from SVO Portal

  @SVO_CustomerResponses_07 @SSEC-1192 @SVO_CustomerResponses_Batch
  Scenario: User is able to edit enquiry details which is created automatically on Enquiries page
    Given Access to mail account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    When Customer send an enquiry mail to user "CustomerResponse,A,4"
    Then Verify that Customer receives an acknowledgement email
    And Logout from users mail account
    Given Login to SVO Portal with Classic Service user with the Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    And Verify that the New Classic Service enquiry is created automatically on SVO Portal
    Then Verify that Created date, Enquiry title and Enquiry status on Enquiries page
    Then User Edit the Enquiry details of an created enquiry on Enquiry page
    And Verify that subject name is auto-populated on emails page
    Then User send an email to customer "LoginUsers,B,17" with Business unit "CustomerResponse,I,4" and subject
    And Verify that enquiry status on enquiries page
    Then User update owner as UAT test for an enquiry Owner Name "LoginUsers,B,16"
    And Verify that the enquiry is removed from the list view for which the owner has been changed
    Then User Logout from the SVO Portal
    Given Access to gmail account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    Then Verify that Customer receives an enquiry update email from user
    And Customer reply to received enquiry update request email
    And Logout from users mail account
    Given Access to SVO Portal by Owner user with Username "LoginUsers,B,16" and Password "LoginUsers,C,16"
    Then Verify the email details of an enquiry from custom notification received
    And Verify that customer response email subject is displayed under emails section of an enquiry
    Then User Logout from the SVO Portal
    Given Access to gmail account with Username "LoginUsers,B,16" and password "LoginUsers,C,16"
    Then User navigates to enquiry link from the mail received from customer
    And Logout from users mail account

  @SVO_CustomerResponses_08 @SSEC-1199 @SVO_CustomerResponses_Batch
  Scenario: User is not able to receive customer response notification without sending an e2a mail to the customer
    Given Access to mail account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    When Customer send an enquiry mail to user "CustomerResponse,A,3"
    Then Verify that Customer receives an acknowledgement email
    And Logout from users mail account
    Given Login to the SVO Portal by bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    And Verify automatically created bespoke enquiry on SVO Portal
    Then Verify that Created date, Enquiry title and Enquiry status on Enquiries page
    And Verify that subject name is auto-populated on emails page
    Then User update owner as UAT test for an enquiry Owner Name "LoginUsers,B,16"
    And Verify that the enquiry is removed from the list view for which the owner has been changed
    Then User Logout from SVO Portal
    Given Access to SVO Portal by Owner user with Username "LoginUsers,B,16" and Password "LoginUsers,C,16"
    Then Verify the not able to receive email of an enquiry from custom notification
    Then User Logout from SVO Portal

  @SVO_CustomerResponses_09 @SSEC-1193 @SVO_CustomerResponses_Batch
  Scenario: User is not able to receive customer response notification if owner name has not been changed on SVO portal
    Given Access to mail account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    When Customer send an enquiry mail to user "CustomerResponse,A,4"
    And Logout from users mail account
    Given Login to SVO Portal by private office user with Username "LoginUsers,B,4" and Password "LoginUsers,C,4"
    And Verify that the New Private Office enquiry is created automatically on SVO Portal
    Then Verify that Created date, Enquiry title and Enquiry status on Enquiries page
    And Verify that subject name is auto-populated on emails page
    Then User send an email to customer "LoginUsers,B,17" with Business unit "CustomerResponse,I,5" and subject
    And Verify that enquiry status on enquiries page
    Then User Logout from the SVO Portal
    Given Access to gmail account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    Then Verify that Customer receives an enquiry update email from user
    And Customer reply to received enquiry update request email
    And Logout from users mail account
    Given Login to SVO Portal by private office user with Username "LoginUsers,B,4" and Password "LoginUsers,C,4"
    Then Verify that the user is not able to view customer response notification on SVO Portal
    Then User Logout from the SVO Portal

  @SVO_CustomerResponses_10 @SSEC-1200 @SVO_CustomerResponses_Batch
  Scenario: User is not able to receive customer response notification if the Owner has changed before sending an e2a mail on SVO portal
    Given Access to mail account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    When Customer send an enquiry mail to user "CustomerResponse,A,4"
    Then Verify that Customer receives an acknowledgement email
    And Logout from users mail account
    Given Login to the SVO Portal by classic service uat user with Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    And Verify automatically created classic service enquiry on SVO Portal
    Then Verify that Created date, Enquiry title and Enquiry status on Enquiries page
    And Verify that subject name is auto-populated on emails page
    Then User update owner as UAT test for an enquiry Owner Name "LoginUsers,B,16"
    And Verify that the enquiry is removed from the list view for which the owner has been changed
    Then User Logout from SVO Portal
    Given Access to SVO Portal by Owner user with Username "LoginUsers,B,16" and Password "LoginUsers,C,16"
    Then Verify the not able to receive email of an enquiry from custom notification
    Then User Logout from SVO Portal

  @SVO_CustomerResponses_11 @SSEC-1194 @SVO_CustomerResponses_Batch
  Scenario: User is not able to receive customer responses on SVO portal if customer is not reply to the users mail
    Given Access to mail account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    When Customer send an enquiry mail to user "CustomerResponse,A,3"
    Then Verify that Customer receives an acknowledgement email
    And Logout from users mail account
    Given Login to the SVO Portal by bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    And Verify that the New Bespoke enquiry is created automatically on SVO Portal
    Then Verify that Created date, Enquiry title and Enquiry status on Enquiries page
    And Verify that subject name is auto-populated on emails page
    Then User send an email to customer "LoginUsers,B,17" with Business unit "CustomerResponse,I,2" and subject
    And Verify that enquiry status on enquiries page
    Then User update owner as UAT test for an enquiry Owner Name "LoginUsers,B,16"
    And Verify that the enquiry is removed from the list view for which the owner has been changed
    Then User Logout from the SVO Portal
    Given Access to gmail account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    Then Verify that Customer receives an enquiry update email from user
    And Logout from users mail account
    Given Access to SVO Portal by Owner user with Username "LoginUsers,B,16" and Password "LoginUsers,C,16"
    Then Verify that the user is not able to view customer response notification on SVO Portal
    Then User Logout from the SVO Portal
