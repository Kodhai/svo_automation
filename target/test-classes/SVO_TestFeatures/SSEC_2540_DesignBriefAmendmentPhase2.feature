@SSEC_2540
Feature: Validate Design Brief Aendments for Phase 2

  @SSEC_2540_01
  Scenario: User is able to select the Headrest cover from the picklist
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User creates a Bespoke Opportunity with check cleared like Stage "Opportunity,B,3" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,I,3" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,3"
    Then User verifies that new Opportunity is created
    Then User navigates to Quotes from Related Links
    And User creates new Bespoke Design Brief quotes "Opportunity,J,4" with mandatory details
    Then Navigate to the new Bespoke Design Brief record created
    And Click on the Headrest cover to select from the picklist
    Then User Logout from SVO Portal

  @SSEC_2540_02
  Scenario: User is able to select the Headrest cover from the picklist
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User creates a Bespoke Opportunity with check cleared like Stage "Opportunity,B,3" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,I,3" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,3"
    Then User verifies that new Opportunity is created
    Then User navigates to Quotes from Related Links
    And User creates new Bespoke Design Brief quotes "Opportunity,J,4" with mandatory details
    Then Navigate to the new Bespoke Design Brief record created
    And Click on the Calfrest cover to select from the picklist
    Then User Logout from SVO Portal

  @SSEC_2540_03
  Scenario: User is able to select the Headrest cover from the picklist
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User creates a Bespoke Opportunity with check cleared like Stage "Opportunity,B,3" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,I,3" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,3"
    Then User verifies that new Opportunity is created
    Then User navigates to Quotes from Related Links
    And User creates new Bespoke Design Brief quotes "Opportunity,J,4" with mandatory details
    Then Navigate to the new Bespoke Design Brief record created
    And Click on the Parcel Shelf Interior to choose it from the picklist
    And Click on Parcel Shelf Color to choose depending on its interior material
    Then User Logout from SVO Portal

  @SSEC_2540_04
  Scenario: User is able to select the M8E2 as prefix for donor part number generation for Sun roof blind
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User creates a Bespoke Opportunity with check cleared like Stage "Opportunity,B,3" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,I,3" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,3"
    Then User verifies that new Opportunity is created
    Then User navigates to Quotes from Related Links
    And User creates new Bespoke Design Brief quotes "Opportunity,J,4" with mandatory details
    Then Navigate to the new Bespoke Design Brief record created
    And Click on the Sun Roof Blind dropdown and select with prefix as M8E2
    Then User Logout from SVO Portal

  @SSEC_2540_05
  Scenario: User is able to select the Radiator Grille finish and  Radiator Grille color from the picklist
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User creates a Bespoke Opportunity with check cleared like Stage "Opportunity,B,3" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,I,3" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,3"
    Then User verifies that new Opportunity is created
    Then User navigates to Quotes from Related Links
    And User creates new Bespoke Design Brief quotes "Opportunity,J,4" with mandatory details
    Then Navigate to the new Bespoke Design Brief record created
    And User selects the Radiator Grille finish and Radiator Grille color from the picklist
    Then User Logout from SVO Portal

  @SSEC_2540_06
  Scenario: User is able to select the Front Tow hook Cover and Rear Tow hook Cover from the front tow eye cover  and Rear tow eye cover picklistsUser is able to select the Front Tow hook Cover and Rear Tow hook Cover from the front tow eye cover  and Rear tow eye cover picklists
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User creates a Bespoke Opportunity with check cleared like Stage "Opportunity,B,3" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,I,3" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,3"
    Then User verifies that new Opportunity is created
    Then User navigates to Quotes from Related Links
    And User creates new Bespoke Design Brief quotes "Opportunity,J,4" with mandatory details
    Then Navigate to the new Bespoke Design Brief record created
    And User selects the Front Tow hook Cover from the front tow eye cover picklist
    And User selects the Rear Tow hook cover from the Rear tow eye cover picklist
    Then User Logout from SVO Portal

  @SSEC_2540_07
  Scenario: User is able to select the Script Badge from the picklist
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User creates a Bespoke Opportunity with check cleared like Stage "Opportunity,B,3" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,I,3" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,3"
    Then User verifies that new Opportunity is created
    Then User navigates to Quotes from Related Links
    And User creates new Bespoke Design Brief quotes "Opportunity,J,4" with mandatory details
    Then Navigate to the new Bespoke Design Brief record created
    And User selects the Script Badge from the picklist
    Then User Logout from SVO Portal

  @SSEC_2540_08
  Scenario: User is able to fill the Content Commodity fields
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User creates a Bespoke Opportunity with check cleared like Stage "Opportunity,B,3" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,I,3" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,3"
    Then User verifies that new Opportunity is created
    Then User navigates to Quotes from Related Links
    And User creates new Bespoke Design Brief quotes "Opportunity,J,4" with mandatory details
    Then Navigate to the new Bespoke Design Brief record created
    And User fills the Content Commodity fields
    Then User Logout from SVO Portal
