@SSEC_2158
Feature: Validate Design Brief Amendments Wheels, Treadplates, Headrest and Rear Console sides

  @SSEC_2158_01 @sonica
  Scenario: Bespoke User is able to select Wheel color and Inserts for Wheels
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User creates a Bespoke Opportunity with check cleared like Stage "Opportunity,B,3" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,I,3" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,3"
    Then User verifies that new Opportunity is created
    Then User navigates to Quotes from Related Links
    And User creates new Bespoke Design Brief quotes "Opportunity,J,4" with mandatory details
    And Click on the new Bespoke Design Brief quote created successfully
    Then Click on Edit button of Wheel Inserts and select inserts "DesignBrief,I,2" and color "DesignBrief,J,2" from the drop down
    Then Click on save button of Design Brief
    Then User Logout from SVO Portal

  @SSEC_2158_02 @sonica
  Scenario: Bespoke User is able to select Treadplates personalization, upper and lower trim color
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User creates a Bespoke Opportunity with check cleared like Stage "Opportunity,B,3" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,I,3" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,3"
    Then User verifies that new Opportunity is created
    Then User navigates to Quotes from Related Links
    And User creates new Bespoke Design Brief quotes "Opportunity,J,4" with mandatory details
    And Click on the new Bespoke Design Brief quote created successfully
    Then Click on Edit button of Upper trim and select lower "DesignBrief,K,2" upper trim "DesignBrief,L,2" and treadplates personalisation "DesignBrief,M,2"
    Then Click on save button of Design Brief
    Then User Logout from SVO Portal

  @SSEC_2158_03 @sonica
  Scenario: Bespoke User is able to select Head rest material, Perforation, Front seat front squab, Front seat rear squab, Rear seat front squab, Rear seat Front squab
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User creates a Bespoke Opportunity with check cleared like Stage "Opportunity,B,3" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,I,3" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,3"
    Then User verifies that new Opportunity is created
    Then User navigates to Quotes from Related Links
    And User creates new Bespoke Design Brief quotes "Opportunity,J,4" with mandatory details
    And Click on the new Bespoke Design Brief quote created successfully
    Then Click on Edit button of Headrest material and select all the fields as HeadRestMaterial "DesignBrief,N,2" FrontSeatFrontSquab "DesignBrief,O,2" FrontSeatRearSquab "DesignBrief,P,2" RearSeatFrontSquab "DesignBrief,Q,2" RearSeatRearSquab "DesignBrief,R,2" Perforation "DesignBrief,S,2"
    Then Click on save button of Design Brief
    Then User Logout from SVO Portal

  @SSEC_2158_04 @sonica
  Scenario: Bespoke User is able to select Rear center console material, finisher color, arm rest, and console sides
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User creates a Bespoke Opportunity with check cleared like Stage "Opportunity,B,3" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,I,3" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,3"
    Then User verifies that new Opportunity is created
    Then User navigates to Quotes from Related Links
    And User creates new Bespoke Design Brief quotes "Opportunity,J,4" with mandatory details
    And Click on the new Bespoke Design Brief quote created successfully
    Then Click on Edit button of Rear center console material and select the fields as RearConsoleMaterial "DesignBrief,T,2" RearConsoleArmRest "DesignBrief,U,2" RearConsoleFinisher "DesignBrief,V,2" RearConsoleSides "DesignBrief,W,2"
    Then Click on save button of Design Brief
    Then User Logout from SVO Portal

  @SSEC_2158_05 @sonica
  Scenario: Bespoke User is able to edit Wheels color once saved
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User creates a Bespoke Opportunity with check cleared like Stage "Opportunity,B,3" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,I,3" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,3"
    Then User verifies that new Opportunity is created
    Then User navigates to Quotes from Related Links
    And User creates new Bespoke Design Brief quotes "Opportunity,J,4" with mandatory details
    And Click on the new Bespoke Design Brief quote created successfully
    Then Click on Edit button of Wheels inserts "DesignBrief,I,3" color and select the color "DesignBrief,J,3"
    Then Click on save button of Design Brief
    Then User Logout from SVO Portal

  @SSEC_2158_06 @sonica
  Scenario: User is able to verify that when Headrest material is changed to PU, then the color list also changes accordingly
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User creates a Bespoke Opportunity with check cleared like Stage "Opportunity,B,3" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,I,3" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,3"
    Then User verifies that new Opportunity is created
    Then User navigates to Quotes from Related Links
    And User creates new Bespoke Design Brief quotes "Opportunity,J,4" with mandatory details
    And Click on the new Bespoke Design Brief quote created successfully
    Then Click on Edit button of Headrest matrial and select as PU "DesignBrief,N,3"
    Then Verify the color list for frontseatfrontsquab "DesignBrief,0,3" and frontseatrearsquab "DesignBrief,P,3" Headrest is changed according to the material
    Then Click on save button of Design Brief
    Then User Logout from SVO Portal
