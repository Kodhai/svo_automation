Feature: Script to add Classic Sales and Classic Service Case Object Fields

  #User is able to view classic sales case details after creating classic sales case on salesforce.
  @SSEC_1566_CaseObjectfields_01 @SSEC_1566_Batch
  Scenario: User is able to view classic sales case details after creating classic sales case on salesforce.
    Given Access to SVO portal for Classic Sales User using Username "LoginUsers,B,5" and password "LoginUsers,C,5"
    And User navigates to the Cases tab
    Then User creates classic sales case with case details like Record Type "CaseCreation,C,4" Product Offering "CaseCreation,O,3" Rating "CaseCreation,P,2" and Case Origin "CaseCreation,Q,3"
    Then Verify all the details of Case Information Section for newly created case
    Then User logouts from SVO portal

  #User is able to view client details of classic service case on case details page
  @SSEC_1566_CaseObjectfields_02 @SSEC_1566_Batch
  Scenario: User is able to view client details of classic service case on case details page
    #Given Access to SVO portal for Classic Services User with UserName "LoginUsers,B,12" and Password "LoginUsers,C,12"
    Given Login to the SF SVO portal for Classic Services User with UserName "LoginUsers,B,12" and Password "LoginUsers,C,12"
    And User navigates to the Cases tab
    Then User creates Classic Service case with client details like record type "CaseCreation,C,5" Account Name "CaseCreation,E,3" Contact Name "CaseCreation,D,3" Region "Opportunity,D,2" Sales Area "Opportunity,E,2" Country "Opportunity,F,2" FirstName "CaseCreation,G,2" LastName "CaseCreation,H,2" WebEmail "CaseCreation,A,3" Customer Contact "CaseCreation,R,2" WebPhone "CaseCreation,K,2"
    And Verify all Client Details of newly created Classic service case on case details page
    Then User logouts from SVO portal

  #User is able to view Vehicle/Programme details of Classic Sales case on Salesforce.
  @SSEC_1566_CaseObjectfields_03 @SSEC_1566_Batch
  Scenario: User is able to view Vehicle/Programme details of Classic Sales case on Salesforce.
    #Given Access to SVO portal for Classic Sales User using Username "LoginUsers,B,5" and password "LoginUsers,C,5"
    Given laterally Access to SVO Portal by user name "LoginUsers,B,5" and Password "LoginUsers,C,5"
    And User navigates to the Cases tab
    Then User created Classic Sales case with Vehicle details like RecordType "CaseCreation,C,4" Program "BillingAddress,B,2" Brand "Opportunity,L,2" and Model "Opportunity,M,2" Series name "Opportunity,O,7" Body Style "Opportunity,P,7" Production Year "CaseCreation,S,2" VIN "CaseCreation,J,2"
    And Verify all the Vehicle Details of newly created Classic sales case on case details page
    Then User logouts from SVO portal

  #User is able to view Enquiry Source and Retailer details of Classic Service case on Salesforce.
  @SSEC_1566_CaseObjectfields_04 @SSEC_1566_Batch
  Scenario: User is able to view Enquiry Source and Retailer details of Classic Service case on Salesforce.
    #Given Access to SVO portal for Classic Services User with UserName "LoginUsers,B,12" and Password "LoginUsers,C,12"
    Given laterally Access to SVO Portal by user name "LoginUsers,B,12" and Password "LoginUsers,C,12"
    And User navigates to the Cases tab
    And User creates new Classic Service case with Enquiry Source and Retailer details like Recordtype "CaseCreation,C,5" Source Campaign "CaseCreation,T,2" Source KMI "CaseCreation,U,2" Preferred Retailer "CaseCreation,E,3"
    Then Verify all the details of Enquiry Source section of newly created Classic service case on case details page
    And Verify all the details of Retailer Details section of newly created Classic service case on case details page
    Then User logouts from SVO portal

  #User is able to verify System Information on Classic Sales enquiry page User is able to verify System Information of Classic Sales case on Salesforce.
  @SSEC_1566_CaseObjectfields_05 @SSEC_1566_Batch
  Scenario: User is able to verify System Information of Classic Sales case on Salesforce.
    #Given Access to SVO portal for Classic Sales User using Username "LoginUsers,B,5" and password "LoginUsers,C,5"
    Given laterally Access to SVO Portal by user name "LoginUsers,B,5" and Password "LoginUsers,C,5"
    And User navigates to the Cases tab
    Then User creates classic sales case with case details like Record Type "CaseCreation,C,4" Product Offering "CaseCreation,O,3" Rating "CaseCreation,P,2" and Case Origin "CaseCreation,Q,3"
    And Verify all the details of System Information section of newly created Classic Sales case on case details page
    Then User logouts from SVO portal

  #User is able to verify SLA Status of Classic Service case on salesforce.
  @SSEC_1566_CaseObjectfields_06 @SSEC_1566_Batch
  Scenario: User is able to verify SLA Status of Classic Service case on salesforce.
    #Given Access to SVO portal for Classic Services User with UserName "LoginUsers,B,12" and Password "LoginUsers,C,12"
    Given laterally Access to SVO Portal by user name "LoginUsers,B,12" and Password "LoginUsers,C,12"
    Then User navigates to the Cases tab
    And User creates new Classic Service case with Enquiry Source and Retailer details like Recordtype "CaseCreation,C,5" Source Campaign "CaseCreation,T,2" Source KMI "CaseCreation,U,2" Preferred Retailer "CaseCreation,E,3"
    When Verify all the details of SLA section of newly created Classic service case on case details page
    Then User logouts from SVO portal

  #User is able to view Enquiry Source and Retailer details of Classic Sales case on Salesforce.
  @SSEC_1566_CaseObjectfields_07 @SSEC_1566_Batch
  Scenario: User is able to view Enquiry Source and Retailer details of Classic Sales case on Salesforce.
    #Given Access to SVO portal for Classic Sales User using Username "LoginUsers,B,5" and password "LoginUsers,C,5"
    Given laterally Access to SVO Portal by user name "LoginUsers,B,5" and Password "LoginUsers,C,5"
    And User navigates to the Cases tab
    And User creates new Classic Sales case with Enquiry Source and Retailer details like Recordtype "CaseCreation,C,4" Source Campaign "CaseCreation,T,2" Source KMI "CaseCreation,U,2" Preferred Retailer "CaseCreation,E,3"
    Then Verify all the details of Enquiry Source and Retailer section of newly created Classic Sales case on case details page
    Then User logouts from SVO portal

  #User is able to view Vehicle details of Closed Classic Service case on Salesforce.
  @SSEC_1566_CaseObjectfields_08 @SSEC_1566_Batch
  Scenario: User is able to view Vehicle details of Closed Classic Service case on Salesforce.
    #Given Access to SVO portal for Classic Services User with UserName "LoginUsers,B,12" and Password "LoginUsers,C,12"
    Given laterally Access to SVO Portal by user name "LoginUsers,B,12" and Password "LoginUsers,C,12"
    And User navigates to the Cases tab
    Then User created Classic Service case with Vehicle details like RecordType "CaseCreation,C,4" Program "BillingAddress,B,2" Brand "Opportunity,L,2" and Model "Opportunity,M,2" Series name "Opportunity,O,7" Body Style "Opportunity,P,7" Production Year "CaseCreation,S,2" VIN "CaseCreation,J,2"
    Then User Navigates to Activity tab and log a call
    Then User Move the Case stage to Closed state
    And Verify all the Vehicle Details of newly created Classic Service case on case details page
    Then User logouts from SVO portal

  #User is not able to view Classic Service case details on Salesforce.
  @SSEC_1566_CaseObjectfields_09 @SSEC_1566_Batch
  Scenario: User is not able to view Classic Service case details on Salesforce
    Given User logins to SVO portal as a SVO Bespoke User UserName "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User Verifies that there is no Cases Tab available
    Then User logouts from SVO portal
