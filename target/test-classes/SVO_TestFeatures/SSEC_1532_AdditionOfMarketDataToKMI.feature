Feature: User is able to view Region, Sales Area and Market values into newly created KMI for Classic Sales Enquiry

  #User is able to validate newly created KMI with details like Region, Sale and Market values
  @Sl1
  Scenario: User is able to validate newly created KMI with details like Region, Sale and Market values
    Given User Access SVO Portal
    When User navigates to Enquiries tab
    And User Create New Enquiry with Record Type "Enquiry,A,4"
    And Enter the Enquiry Title for the Enquiry creation
    Then User fills required details such as_ Product Offering "Enquiry,B,4" Enquiry Source "Enquiry,C,4" Originating Division "Enquiry,D,4" Region "Enquiry,E,4" Sales Area "Enquiry,F,4" Market "Enquiry,G,4" Brand "Enquiry,J,4" Model "Enquiry,K,4" and Retailer "Enquiry,L,4"
    And Save the enquiry
    Then User logs a call to an Activity tab
    Then From Closed Tab, click on Mark Current Enquiry Status as Close and State as Qualified-KMI
    Then User verifies that new KMI record gets created
    Then User verifies that Region, Sales Area and Market values are added to newly created KMI

  #User is able to validate newly created KMI with details like Region, Sale and Market values
  @Sl1
  Scenario: User is able to validate newly created KMI with details like Region, Sale and Market values
    Given User Access SVO Portal
    When User navigates to Enquiries tab
    And User Create New Enquiry with Record Type "Enquiry,A,5"
    And Enter the Enquiry Title for the Enquiry creation
    Then User fills required details such as_ Product Offering "Enquiry,B,5" Enquiry Source "Enquiry,C,5" Originating Division "Enquiry,D,5" Region "Enquiry,E,5" Sales Area "Enquiry,F,5" Market "Enquiry,G,5" Brand "Enquiry,J,5" Model "Enquiry,K,5" and Retailer "Enquiry,L,5"
    And Save the enquiry
    Then User logs a call to an Activity tab
    Then From Closed Tab, click on Mark Current Enquiry Status as Close and State as Qualified-KMI
    Then User verifies that new KMI record gets created
    Then User verifies that Region, Sales Area and Market values are added to newly created KMI

  #User is not able to view Region, Sales Area and Market values in active KMI record for SVO Bespoke Enquiry
  @Sl3
  Scenario: User is not able to view Region, Sales Area and Market values in active KMI record for SVO Bespoke Enquiry
    Given User Access SVO Portal
    When User navigates to Enquiries tab
    And User Create New Enquiry with Record Type "Enquiry,A,2"
    And Enter the Enquiry Title for the Enquiry creation
    Then User fills required details such as Region "Enquiry,E,2" Sales Area "Enquiry,F,2" Market "Enquiry,G,2"
    And Save the enquiry
    Then User marks the current enquiry state
    And User verifies that Qualified-KMI is not available in Closed State
    And User verifies that no KMI is created

  #User is not able to view Region, Sales Area and Market values in active KMI record for Classic Service Enquiry
  @Sl4
  Scenario: User is not able to view Region, Sales Area and Market values in active KMI record for Classic Service Enquiry
    Given User Access SVO Portal
    When User navigates to Enquiries tab
    And User Create New Enquiry with Record Type "Enquiry,A,5"
    And Enter the Enquiry Title for the Enquiry creation
    Then User fills required details such as_ Product Offering "Enquiry,B,5" Enquiry Source "Enquiry,C,5" Originating Division "Enquiry,D,5" Region "Enquiry,E,5" Sales Area "Enquiry,F,5" Market "Enquiry,G,5" Brand "Enquiry,J,5" Model "Enquiry,K,5" and Retailer "Enquiry,L,5"
    And Save the enquiry
    Then User logs a call to an Activity tab
    Then User verifies that no new KMI record gets created

  #Region, Sales Area and Market values should not displayed in active KMI for Classic Sales enquiry closed with Qualified-Opportunity
  @Sl5
  Scenario: Region, Sales Area and Market values should not displayed in active KMI for Classic Sales enquiry closed with Qualified-Opportunity
    Given User Access SVO Portal
    When User navigates to Enquiries tab
    And User Create New Enquiry with Record Type "Enquiry,A,5"
    Then User fills required details such as_ Product Offering "Enquiry,B,5" Enquiry Source "Enquiry,C,5" Originating Division "Enquiry,D,5" Region "Enquiry,E,5" Sales Area "Enquiry,F,5" Market "Enquiry,G,5" Brand "Enquiry,J,5" Model "Enquiry,K,5" and Retailer "Enquiry,L,5"
    And User fills Preferred Contact "Enquiry,I,2"
    And Save the enquiry
    Then User logs a call to an Activity tab
    Then From Closed Tab click on Mark Current Enquiry Status as Close and State as Qualified - Opportunity
    Then User verifies that no new KMI record gets created and Region, Sales Area and Market values are not added

  #User is able to view updated Region, Sales Area and Market values in active KMI record for Classic Sales Enquiry
  @Sl6
  Scenario: User is able to view updated Region, Sales Area and Market values in active KMI record for Classic Sales Enquiry
    Given Access SVO Portal
    When User navigates to Enquiries tab
    And User Create New Enquiry with Record Type "Enquiry,A,4"
    And Enter the Enquiry Title for the Enquiry creation
    Then User fills required details such as_ Product Offering "Enquiry,B,4" Enquiry Source "Enquiry,C,4" Originating Division "Enquiry,D,4" Region "Enquiry,E,4" Sales Area "Enquiry,F,4" Market "Enquiry,G,4" Brand "Enquiry,J,4" Model "Enquiry,K,4" and Retailer "Enquiry,L,4"
    And Save the enquiry
    Then User logs a call to an Activity tab
    Then From Closed Tab, click on Mark Current Enquiry Status as Close and State as Qualified-KMI
    Then User verifies that new KMI record gets created
    Then User verifies that Region, Sales Area and Market values are added to newly created KMI
    Then User updates Region, Sales Area and Market values in closed Classic Sales Enquiry
    Then User validates that Region, Sales Area and Market values are updated in KMI record

  #User is able to view Region Sales Area and Market values in active KMI record for deletec Classic Service Enquiry in Classic Admin
  @Scenario_7
  Scenario: User is able to view Region Sales Area and Market values in active KMI record for deletec Classic Service Enquiry in Classic Admin
    Given Access to SVO Portal as Classic Admin user with Username "LoginUsers,B,13" and Password "LoginUsers,C,13"
    When User navigates to Enquiries tab
    And User Creates New Enquiry with Record Type "Enquiry,A,5"
    And Enter the Enquiry Title for the Enquiry creation
    And Save the enquiry
    And user fills the required details like Product Offering "Enquiry,B,2" Enquiry Source "Enquiry,C,2" Originating Division "Enquiry,D,2" Region "Enquiry,E,2" Sales Area "Enquiry,F,2" Market "Enquiry,G,2" Brand "Enquiry,J,2" Model "Enquiry,K,2" and Retailer "Enquiry,L,2"
    And User fills Preferred Contact "Enquiry,I,2"
    Then User logs a call to an Activity tab
    Then From Closed Tab, click on Mark Current Enquiry Status as Close and State as Qualified-KMI
    Then User verifies that new KMI record gets created
    Then User verifies that Region, Sales Area and Market values are added to newly created KMI
    Then User deletes the enquiry created
    And User checks Region, Sales Area and Market values into KMI record
