Feature: Validations for Case creation through email

  #User is able to submit a case for Parts & Technical Jaguar/Landrover via sending an email
  @SSEC_1226_CaseCreationEmail_01 @SSEC-1791
  Scenario: User is able to submit a case for Parts & Technical Jaguar/Landrover via sending an email
    Given Access to email account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    When Customer sends case creation mail to user "CaseCreation,A,2"
    And Logout from user email account	
    Then Verify User receives an outlook email notification on case creation update	
    Given Login to SVO Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User Navigates to Cases Tab
    And Verify automatically created Parts and Technical case on SVO Portal
    Then Verify the Case Origin of created case
    Then User Logouts from the SVO Portal		
    
  #User is not able to submit a case for Customer Service via sending an email
  @SSEC_1226_CaseCreationEmail_02 @SSEC-1792
  Scenario: User is not able to submit a case for Customer Service via sending an email
   Given Access to email account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    When Customer sends case creation mail to user "CaseCreation,A,4"
    And Logout from user email account	
    Given Access to SVO Portal as admin with Username "LoginUsers,B,6" and Password "LoginUsers,C,6"
    Then login to SVO portal as customer service user "CaseCreation,B,2" via setup login
    When User Navigates to Cases Tab
    And Verify that Classic Customer Service case is not created on SVO Portal
    Then logout from set up login
    Then User Logouts from the SVO Portal			

  #A new Parts & Technical case is not created if invalid email address enters via sending an email
  @SSEC_1226_CaseCreationEmail_03	@SSEC-1794
  Scenario: A new Parts & Technical case is not created if invalid email address enters via sending an email
    Given Access to email account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    When Customer sends case creation mail to invalid user "CaseCreation,A,3"
    And Logout from user email account	
    Given Login to SVO Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User Navigates to Cases Tab
    And Verify that parts and technical case is not created on SVO Portal			
    Then User Logouts from the SVO Portal
