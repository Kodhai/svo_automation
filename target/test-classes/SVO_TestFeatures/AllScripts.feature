Feature: Validate SF-SVO Test Scripts

  #User creates a new Enquiry with SVO Bespoke Record Type
  @SVO @SVO_001 @SSEC-270 @firstBatch @SVOBatch @Recon001
  Scenario: User creates a new Enquiry with SVO Bespoke Record Type
    #And mark test script status in "ScriptResults,C,1"
    #Given Access SVO Portal as a user having "Scripts_UserRole,B,2"
    Given Access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,2"
    And Enter the required details for the Enquiry creation
    Then Save the new Enquiry
    Then User selects Preferred Contact details from the Potential Matches coming after entering the Email Address "Enquiry,N,2"
    And Log a call to the Enquiry with client "Enquiry,H,2"
    And Log a email to the Enquiry
    Then edit the required information as to "Enquiry,S,2" Related to "Enquiry,T,2" Additional to "Enquiry,U,2" CC "Enquiry,V,2" BCC "Enquiry,W,2" Subject "Enquiry,X,2" Body "Enquiry,Y,2" and Importance "Enquiry,Z,2"
    And Click on sent mail button
    Then from Discovery Tab click on Mark Current Enquiry Status
    And user fills the required details like Product Offering "Enquiry,B,2" Enquiry Source "Enquiry,C,2" Originating Division "Enquiry,D,2" Region "Enquiry,E,2" Sales Area "Enquiry,F,2" Market "Enquiry,G,2" Brand "Enquiry,J,2" Model "Enquiry,K,2" and Retailer "Enquiry,L,2"
    Then From Discovery Tab click on Mark Current Enquiry Status as Complete
    And Verify that a new opportunity is created after closing the Enquiry
    Then Verify that user can view Recently Viewed Enquiries as default
    And mark this script status in "ScriptResults,C,1"

  #Then Logout from the SVO Portal
  #User creates a new Enquiry with SVO Private Office Record Type
  @SVO @SVO_002 @SSEC-273 @firstBatch @SVOBatch @Sanity
  Scenario: User creates a new Enquiry with SVO Private Office Record Type
    And mark test script status in "ScriptResults,C,2"
    Given Access SVO Portal
    #Given User Access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,3"
    And Enter the required details for the Enquiry creation
    Then Save the new Enquiry
    Then User selects Preferred Contact details from the Potential Matches coming after entering the Email Address "Enquiry,N,2"
    And Log a call to the Enquiry with client "Enquiry,H,3"
    And Log a email to the Enquiry
    Then edit the required information as to "Enquiry,S,2" Related to "Enquiry,T,2" Additional to "Enquiry,U,2" CC "Enquiry,V,2" BCC "Enquiry,W,2" Subject "Enquiry,X,2" Body "Enquiry,Y,2" and Importance "Enquiry,Z,2"
    And Click on sent mail button
    Then from Discovery Tab click on Mark Current Enquiry Status
    And user fills the required details like Product Offering "Enquiry,B,3" Enquiry Source "Enquiry,C,3" Originating Division "Enquiry,D,3" Region "Enquiry,E,3" Sales Area "Enquiry,F,3" Market "Enquiry,G,3" Brand "Enquiry,J,3" Model "Enquiry,K,3" and Retailer "Enquiry,L,3"
    Then Verify the SLA status and change the Enquiry Owner information to "Enquiry,O,3"
    Then From Discovery Tab click on Mark Current Enquiry Status as Complete
    And Verify that a new opportunity is created after closing the Enquiry
    Then Verify that user can view Recently Viewed Enquiries as default
    And mark this script status in "ScriptResults,C,2"

  #Then Logout from the SVO Portal
  #User creates a new Enquiry with Classic Sales Record Type
  @SVO @SVO_003 @SSEC-274 @firstBatch @SVOBatch
  Scenario: User creates a new Enquiry with Classic Sales Record Type
    And mark test script status in "ScriptResults,C,3"
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,4"
    And Enter the required details for the Enquiry creation
    Then Save the new Enquiry
    Then User selects Preferred Contact details from the Potential Matches coming after entering the Email Address "Enquiry,N,2"
    And Log a call to the Enquiry with client "Enquiry,H,4"
    And Log a email to the Enquiry
    Then edit the required information as to "Enquiry,S,2" Related to "Enquiry,T,2" Additional to "Enquiry,U,2" CC "Enquiry,V,2" BCC "Enquiry,W,2" Subject "Enquiry,X,2" Body "Enquiry,Y,2" and Importance "Enquiry,Z,2"
    And Click on sent mail button
    Then from Discovery Tab click on Mark Current Enquiry Status
    And user fills the required details like Product Offering "Enquiry,B,4" Enquiry Source "Enquiry,C,4" Originating Division "Enquiry,D,4" Region "Enquiry,E,4" Sales Area "Enquiry,F,4" Market "Enquiry,G,4" Brand "Enquiry,J,4" Model "Enquiry,K,4" and Retailer "Enquiry,L,4"
    Then Close enquiry with close type as "Enquiry,P,4" and reason as "Enquiry,Q,4"
    Then Verify that user can view Recently Viewed Enquiries as default
    And mark this script status in "ScriptResults,C,3"

  #Then Logout from the SVO Portal
  #User creates a new Enquiry with Classic Service Record Type
  @SVO @SVO_004 @SSEC-275 @firstBatch @SVOBatch
  Scenario: User creates a new Enquiry with Classic Service Record Type
    And mark test script status in "ScriptResults,C,4"
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,5"
    And Enter the required details for the Enquiry creation
    Then Save the new Enquiry
    Then User selects Preferred Contact details from the Potential Matches coming after entering the Email Address "Enquiry,N,2"
    And Log a call to the Enquiry with client "Enquiry,H,5"
    And Log a email to the Enquiry
    Then edit the required information as to "Enquiry,S,2" Related to "Enquiry,T,2" Additional to "Enquiry,U,2" CC "Enquiry,V,2" BCC "Enquiry,W,2" Subject "Enquiry,X,2" Body "Enquiry,Y,2" and Importance "Enquiry,Z,2"
    And Click on sent mail button
    Then from Discovery Tab click on Mark Current Enquiry Status
    And user fills the required details like Product Offering "Enquiry,B,5" Enquiry Source "Enquiry,C,5" Originating Division "Enquiry,D,5" Region "Enquiry,E,5" Sales Area "Enquiry,F,5" Market "Enquiry,G,5" Brand "Enquiry,J,5" Model "Enquiry,K,5" and Retailer "Enquiry,L,5"
    Then Close enquiry with close type as "Enquiry,P,5" and reason as "Enquiry,Q,5"
    When change the Enquiry Owner information to "Enquiry,O,3" without a mail
    Then Verify that user can view Recently Viewed Enquiries as default
    And mark this script status in "ScriptResults,C,4"

  #Then Logout from the SVO Portal
  #User does not log a call to an enquiry which is in New stage
  @SVO @SVO_005 @SSEC-276 @firstBatch @SVOBatch
  Scenario: User does not log a call to an enquiry which is in New stage
    And mark test script status in "ScriptResults,C,5"
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,2"
    And Enter the required details for the Enquiry creation
    Then Save the new Enquiry
    Then from Contacted Tab Mark Current Enquiry Status
    Then Verify Current status cannot be marked as Contacted
    And mark this script status in "ScriptResults,C,5"

  #Then Logout from the SVO Portal
  #User enters Incorrect Enquiry name in the Enquiry search box
  @SVO @SVO_006 @SSEC-277 @firstBatch @SVOBatch @JIRADemo
  Scenario: User enters Incorrect Enquiry name in the Enquiry search box
    And mark test script status in "ScriptResults,C,6"
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    And Enter Incorrect Enquiry name as "ENQ-001" in the Enquiry search box
    Then Validate error message displayed
    And mark this script status in "ScriptResults,C,6"

  #Then Logout from the SVO Portal
  #User navigates to All Enquiries list from Recently Viewed Enquiry list
  @SVO @SVO_007 @SSEC-278 @firstBatch @SVOBatch @JIRADemo
  Scenario: User navigates to All Enquiries list from Recently Viewed Enquiry list
    And mark test script status in "ScriptResults,C,7"
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    And User navigates to All Enquiries list from Recently Viewed Enquiry list
    Then Validate user can view all enquiries with status and enquiry owner
    And mark this script status in "ScriptResults,C,7"

  #Then Logout from the SVO Portal
  #User can select client and preferred conatct details without entering an email address
  @SVO @SVO_008 @SSEC-279 @firstBatch @SVOBatch @Sanity
  Scenario: User can select client and preferred conatct details without entering an email address
    And mark test script status in "ScriptResults,C,8"
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,2"
    And Enter the required details for the Enquiry creation
    Then Save the new Enquiry
    And User Edits an enquiry and Enter Client "Enquiry,H,6" and Preferred Contact "Enquiry,H,6"
    And Save the enquiry
    And mark this script status in "ScriptResults,C,8"

  #Then Logout from the SVO Portal
  #User skips Client/Vehicle Details/Source field at the contacted stage of an enquiry
  @SVO @SVO_009 @SSEC-280 @firstBatch @SVOBatch
  Scenario: User skips Client/Vehicle Details/Source field at the contacted stage of an enquiry
    And mark test script status in "ScriptResults,C,9"
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    And User Create New Enquiry with Record Type "Enquiry,A,4"
    And Enter the Enquiry Title for the Enquiry creation
    And Save the enquiry
    And Log a call to the Enquiry with client "Enquiry,H,4"
    And From the Contacted Stage Mark Enquiry Status as Complete
    Then Validate the Error Message

  #Then Logout from the SVO Portal
  #User enters Client, Preferred Contact and Source and closes an enquiry as Qualified-Opportunity
  @SVO @SVO_010 @SSEC-281 @firstBatch @SVOBatch
  Scenario: User enters Client, Preferred Contact and Source and closes an enquiry as Qualified-Opportunity
    And mark test script status in "ScriptResults,C,10"
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    And User Create New Enquiry with Record Type "Enquiry,A,4"
    And Enter the Enquiry Title for the Enquiry creation
    And Save the enquiry
    Then User selects Preferred Contact details from the Potential Matches coming after entering the Email Address "Enquiry,N,2"
    And Log a call to the Enquiry with client "Enquiry,H,4"
    Then from Discovery Tab click on Mark Current Enquiry Status
    Then Mark the Enquiry Status as Complete with Qualified-Opportunity state
    Then Validate the Error Message	Displayed

  #Then Logout from the SVO Portal
  #User skips Client and Source fields to close an enquiry as Closed[Lost] status
  @SVO @SVO_011 @SSEC-282 @firstBatch @SVOBatch
  Scenario: User skips Client and Source fields to close an enquiry as Closed[Lost] status
    And mark test script status in "ScriptResults,C,11"
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    And User Create New Enquiry with Record Type "Enquiry,A,2"
    And Enter the Enquiry Title for the Enquiry creation
    And Save the enquiry
    And User closes an enquiry with "Closed[Lost]" status

  #Then Logout from the SVO Portal
  #User tries to re-open [Move to New from Closed] an enquiry
  @SVO @SVO_012 @SSEC-283 @firstBatch @SVOBatch
  Scenario: User tries to re-open [Move to New from Closed] an enquiry
    And mark test script status in "ScriptResults,C,12"
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    And User search an enquiry having closed reason type as "Qualified-Opportunity"
    And User tries to reopen Closed Enquiry

  #Then Logout from the SVO Portal
  #User can change owner of a closed enquiry
  @SVO @SVO_013 @SSEC-284 @firstBatch @SVOBatch
  Scenario: User can change owner of a closed enquiry
    And mark test script status in "ScriptResults,C,13"
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,2"
    And Enter the required details for the Enquiry creation
    Then Save the new Enquiry
    Then User selects Preferred Contact details from the Potential Matches coming after entering the Email Address "Enquiry,N,2"
    And Log a call to the Enquiry with client "Enquiry,H,2"
    Then from Discovery Tab click on Mark Current Enquiry Status
    And user fills the required details like Product Offering "Enquiry,B,2" Enquiry Source "Enquiry,C,2" Originating Division "Enquiry,D,2" Region "Enquiry,E,2" Sales Area "Enquiry,F,2" Market "Enquiry,G,2" Brand "Enquiry,J,2" Model "Enquiry,K,2" and Retailer "Enquiry,L,2"
    Then From Discovery Tab click on Mark Current Enquiry Status as Complete
    When User clicks on the change Owner and enter Owner as "Enquiry,S,2"

  #Then Logout from the SVO Portal
  #User should be able view Enquiry History
  @SVO @SVO_014 @SSEC-285 @firstBatch @SVOBatch
  Scenario: User should be able view Enquiry History
    And mark test script status in "ScriptResults,C,14"
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    And User Create New Enquiry with Record Type "Enquiry,A,4"
    And Enter the Enquiry Title for the Enquiry creation
    And Save the enquiry
    Then User selects Preferred Contact details from the Potential Matches coming after entering the Email Address "Enquiry,N,2"
    And Log a call to the Enquiry with client "Enquiry,H,4"
    Then from Discovery Tab click on Mark Current Enquiry Status
    And User View Enquiry History

  #Then Logout from the SVO Portal
  #User changes the enquiry status in the backward direction [Discovery to Contacted]
  @SVO @SVO_015 @SSEC-286 @firstBatch @SVOBatch @Sanity
  Scenario: User changes the enquiry status in the backward direction [Discovery to Contacted]
    And mark test script status in "ScriptResults,C,15"
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    And User Create New Enquiry with Record Type "Enquiry,A,2"
    And Enter the Enquiry Title for the Enquiry creation
    And Save the enquiry
    Then User selects Preferred Contact details from the Potential Matches coming after entering the Email Address "Enquiry,N,2"
    And Log a call to the Enquiry with client "Enquiry,H,2"
    Then from Discovery Tab click on Mark Current Enquiry Status
    And User changes the enquiry status from Discovery to Contacted stage

  #Then Logout from the SVO Portal
  #User cannot change the enquiry status from Contacted to New
  @SVO @SVO_016 @SSEC-287 @firstBatch @SVOBatch
  Scenario: User cannot change the enquiry status from Contacted to New
    And mark test script status in "ScriptResults,C,16"
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    And User Create New Enquiry with Record Type "Enquiry,A,2"
    And Enter the Enquiry Title for the Enquiry creation
    And Save the enquiry
    Then User selects Preferred Contact details from the Potential Matches coming after entering the Email Address "Enquiry,N,2"
    And Log a call to the Enquiry with client "Enquiry,H,2"
    And User changes enquiry status from Contacted to New stage
    And Verify that an error occurred when moving enquiry from Contacted to New

  #Then Logout from the SVO Portal
  #User should be able to change the record type of a non-closed enquiry
  @SVO @SVO_017 @SSEC-288 @firstBatch @SVOBatch
  Scenario: User should be able to change the record type of a non-closed enquiry
    And mark test script status in "ScriptResults,C,17"
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    And User Create New Enquiry with Record Type "Enquiry,A,2"
    And Enter the Enquiry Title for the Enquiry creation
    And Save the enquiry
    And User changes the record type of a non-closed enquiry

  #Then Logout from the SVO Portal
  #User should not be able to change the record type of a closed enquiry
  @SVO @SVO_018 @SSEC-289 @firstBatch @SVOBatch
  Scenario: User should not be able to change the record type of a closed enquiry
    And mark test script status in "ScriptResults,C,18"
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,2"
    And Enter the required details for the Enquiry creation
    Then Save the new Enquiry
    Then User selects Preferred Contact details from the Potential Matches coming after entering the Email Address "Enquiry,N,2"
    And Log a call to the Enquiry with client "Enquiry,H,2"
    Then from Discovery Tab click on Mark Current Enquiry Status
    And user fills the required details like Product Offering "Enquiry,B,2" Enquiry Source "Enquiry,C,2" Originating Division "Enquiry,D,2" Region "Enquiry,E,2" Sales Area "Enquiry,F,2" Market "Enquiry,G,2" Brand "Enquiry,J,2" Model "Enquiry,K,2" and Retailer "Enquiry,L,2"
    Then From Discovery Tab click on Mark Current Enquiry Status as Complete
    And User changes the record type of a closed enquiry
    Then Verify that an error occurred when user changes record type of a closed enquiry

  #Then Logout from the SVO Portal
  #User can get vehicle information from Vehicle Search Option for any non-closed enquiry
  @SVO @SVO_019 @SSEC-290 @firstBatch @SVOBatch @Sanity
  Scenario: User can get vehicle information from Vehicle Search Option for any non-closed enquiry
    And mark test script status in "ScriptResults,C,19"
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    And User Create New Enquiry with Record Type "Enquiry,A,2"
    And Enter the Enquiry Title for the Enquiry creation
    And Save the enquiry
    And User searches a vehicle from Vehicle Search Option using Brand "Vehicle,A,2",Model "Vehicle,B,2",Full VIN "Vehicle,C,2" and Record Type "Vehicle,D,2"
    Then Verify that Vehicle information get auto populated

  #Then Logout from the SVO Portal
  #User should be able to close an enquiry with Qualified-KMI status [For Classic Sales and Classic Service Record Type
  @SVO @SVO_020 @SSEC-291 @firstBatch @SVOBatch @Sanity
  Scenario: User should be able to close enquiry with Qualified-KMI status [For Classic Sales and Classic Service Record Type
    And mark test script status in "ScriptResults,C,20"
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    And User Create New Enquiry with Record Type "Enquiry,A,5"
    And Enter the Enquiry Title for the Enquiry creation
    And Save the enquiry
    Then User selects Preferred Contact details from the Potential Matches coming after entering the Email Address "Enquiry,N,2"
    And Log a call to the Enquiry with client "Enquiry,H,2"
    Then from Discovery Tab click on Mark Current Enquiry Status
    And user fills the required details like Product Offering "Enquiry,B,5" Enquiry Source "Enquiry,C,5" Originating Division "Enquiry,D,5" Region "Enquiry,E,5" Sales Area "Enquiry,F,5" Market "Enquiry,G,5" Brand "Enquiry,J,5" Model "Enquiry,K,5" and Retailer "Enquiry,L,5"
    Then User Creates a new Programme with Status "Programme,A,2", Product offering "Programme,B,2", Brand "Programme,C,2", Model "Programme,D,2"
    Then From Discovery Tab click on Mark Current Enquiry Status as Complete

  #Then Logout from the SVO Portal
  #User searches an opportunity for non-closed enquiry
  @SVO @SVO_021 @SSEC-292 @firstBatch @SVOBatch
  Scenario: User searches an opportunity for non-closed enquiry
    And mark test script status in "ScriptResults,C,21"
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    And User Create New Enquiry with Record Type "Enquiry,A,5"
    And Enter the Enquiry Title for the Enquiry creation
    And Save the enquiry
    Then Verify that no opportunity is linked for a non-closed enquiry

  #Then Logout from the SVO Portal
  #User is able to change closed enquiry reason type from Closed[Lost] to Qualified-Opportunity
  @SVO @SVO_022 @SSEC-293 @firstBatch @SVOBatch
  Scenario: User is able to change closed enquiry reason type from Closed[Lost] to Qualified-Opportunity
    And mark test script status in "ScriptResults,C,22"
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    And User search an enquiry having Closed Lost reason type as "Closed[Lost]"
    Then User changes the closed reason type from "Closed[Lost]" to "Qualified-Opportunity"

  #Then Logout from the SVO Portal
  #User cannot change closed enquiry reason type from Qualified-Opportunity to Closed[Lost]
  @SVO @SVO_023 @SSEC-294 @firstBatch @SVOBatch
  Scenario: User cannot change closed enquiry reason type from Qualified-Opportunity to Closed[Lost]
    And mark test script status in "ScriptResults,C,23"
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    And User search an enquiry having closed reason type as "Qualified-Opportunity"
    Then User changes the closed reason type from "Qualified-Opportunity" to "Closed[Lost]"

  #Then Logout from the SVO Portal
  #User should be able receive an email if no action is taken for an open enquiry within 48 hours
  @SVO @SVO_024 @SSEC-295 @firstBatch @SVOBatch
  Scenario: User should be able receive an email if no action is taken for an open enquiry within 48 hours
    And mark test script status in "ScriptResults,C,24"
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    Then User opens an enquiry which is in new status since 48hrs
    And Verify that User receives a Custom notification for the same enquiry

  #Then Logout from the SVO Portal
  #User should not fail to complete the first contact target within 48 hours of Kick-off date for an open enquiry
  @SVO @SVO_025 @SSEC-296 @firstBatch @SVOBatch
  Scenario: User should not fail to complete the first contact target within 48 hours of Kick-off date for an open enquiry
    And mark test script status in "ScriptResults,C,25"
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    Then User opens an enquiry which is in new status since 48hrs
    And Verify that the open enquiry SLA status is failed

  #Then Logout from the SVO Portal
  #User is able to complete first contact target within 48 hours of Kick-off date for an open enquiry
  @SVO @SVO_026 @SSEC-297 @secondBatch @SVOBatch
  Scenario: User is able to complete first contact target within 48 hours of Kick-off date for an open enquiry
    And mark test script status in "ScriptResults,C,26"
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    And User Create New Enquiry with Record Type "Enquiry,A,5"
    And Enter the Enquiry Title for the Enquiry creation
    And Save the enquiry
    And Verify that the open enquiry SLA status is in progress

  #Then Logout from the SVO Portal
  #User should not be able to close an enquiry without SLA Success status
  @SVO @SVO_027 @SSEC-298 @secondBatch @SVOBatch
  Scenario: User should not be able to close an enquiry without SLA Success status
    And mark test script status in "ScriptResults,C,27"
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    And User Create New Enquiry with Record Type "Enquiry,A,5"
    And Enter the Enquiry Title for the Enquiry creation
    And Save the enquiry
    And User closes an enquiry for which SLA status is in progress
    Then Verify that an error occurred when user closes an enquiry without SLA Success status

  #Then Logout from the SVO Portal
  #User is able to receive an email reminder till final Escalation Reminder target date
  @SVO @SVO_028 @SSEC-299 @secondBatch @SVOBatch
  Scenario: User is able to receive an email reminder till final Escalation Reminder target date
    And mark test script status in "ScriptResults,C,28"
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    Then Open an enquiry for which SLA is failed and reach till Final Escalation Reminder Target Date
    And Verify that User receives a Custom notification for the same enquiry

  #Then Logout from the SVO Portal
  #User is not able to receive any email, if an enquiry is contacted within 48 hours
  @SVO @SVO_029 @SSEC-300 @secondBatch @SVOBatch
  Scenario: User is not able to receive any email, if an enquiry is contacted within 48 hours
    And mark test script status in "ScriptResults,C,29"
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    And User Create New Enquiry with Record Type "Enquiry,A,5"
    And Enter the Enquiry Title for the Enquiry creation
    And Save the enquiry
    Then User selects Preferred Contact details from the Potential Matches coming after entering the Email Address "Enquiry,N,2"
    And Log a call to the Enquiry with client "Enquiry,H,2"
    And Verify that user is not received any email when an enquiry is contacted within 48 hours

  #Then Logout from the SVO Portal
  #User is able to create multiple enquiries with the same enquiry title
  @SVO @SVO_030 @SSEC-301 @secondBatch @SVOBatch
  Scenario: User is able to create multiple enquiries with the same enquiry title
    And mark test script status in "ScriptResults,C,30"
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    And User Create New Enquiry with Record Type "Enquiry,A,2"
    And Enter Enquiry title "Enquiry,R,2"  for the Enquiry creation
    Then Save the new Enquiry
    And Verify the title of the created Enquiry as "Enquiry,R,2"
    When User Navigate to Enquiries
    And User Create New Enquiry with Record Type "Enquiry,A,2"
    And Enter Enquiry title "Enquiry,R,2"  for the Enquiry creation
    Then Save the new Enquiry
    And Verify the title of the created Enquiry as "Enquiry,R,2"

  #Then Logout from the SVO Portal
  #User is able create a new Individual account from Accounts section
  @SVO @SVO_031 @secondBatch @secondBatch @Try31 @SVOBatch
  Scenario: User is able create a new Individual account from Accounts section
    And mark test script status in "ScriptResults,C,31"
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Individual account with Record type "Account,A,2" First name "Account,L,2" Last name "Account,M,3" Region "Account,N,3" Email "Account,K,3"

  #Then Logout from the SF SVO Portal
  #User creates a new SAP Account for an individual account
  @SVO @SVO_032 @secondBatch @SVOBatch
  Scenario: User creates a new SAP Account for an individual account
    And mark test script status in "ScriptResults,C,32"
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Individual account with Record type "Account,A,2" First name "Account,L,2" Last name "Account,M,3" Region "Account,N,3" Email "Account,K,3"
    #Then User Searches for account "Account,E,3" and selects it
    Then Navigate to the SAP Accounts section and click on New
    Then Enter details in new SAP Account and Save it

  #Then create a new SAP account with Requested Status
  #Then Logout from the SF SVO Portal
  #User can be able to create multiple SAP Account for the same individual account
  @SVO @SVO_033 @secondBatch @SVOBatch
  Scenario: User can be able to create multiple SAP Account for the same individual account
    And mark test script status in "ScriptResults,C,33"
    #Given Access SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Individual account with Record type "Account,A,2" First name "Account,L,2" Last name "Account,M,3" Region "Account,N,3" Email "Account,K,3"
    And Navigates to Users SAP Account
    Then User creates an SAP Account with SAP ID "SAP,A,2"
    And Enter all mandatory fields for SAP Account like Status "SAP,B,2" and Location "SAP,C,2"
    And Click on Save and New
    Then User creates an SAP Account with SAP ID "SAP,A,3"
    And Enter all mandatory fields for SAP Account like Status "SAP,B,3" and Location "SAP,C,3"
    And Save the details

  #Then Filter on SAP Accounts using SAP	ID "SAP,A,2"
  #And Delete the Selected SAP Account
  #Then Logout from the SVO Portal
  #User can be able to add relationships to an individual account from Related Contacts section
  @SVO @SVO_034 @secondBatch @SVOBatch
  Scenario: User can be able to add relationships to an individual account from Related Contacts section
    And mark test script status in "ScriptResults,C,34"
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Individual account with Record type "Account,A,2" First name "Account,L,2" Last name "Account,M,4" Region "Account,N,3" Email "Account,K,3"
    Then Navigate to the Related contacts section and click on add Relationship
    And Enter details for New Account Relationship like Contact "Account,B,5",Roles "Account,I,2",Relationship Strength "Account,J,2" and Start Date
    And Save the details for New Account Relationship

  #Then Filter on Related Contacts using Contact "SAP,A,2"
  #And Remove Account Relationship
  #Given User access SVO Portal with Non-Admin User
  #When User Navigate to Accounts
  #And User opens any individual account having name "Account,M,4"
  #Then Navigate to the Related contacts section and click on add Relationship
  #Then Verify that relation field is not present
  #Then Logout from the SF SVO Portal
  #User can view added relationship's contact name into Contacts Menu
  @SVO @SVO_035 @SSEC-306 @secondBatch @SVOBatch
  Scenario: User can view added relationship's contact name into Contacts Menu
    And mark test script status in "ScriptResults,C,35"
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Individual account with Record type "Account,A,2" First name "Account,L,2" Last name "Account,M,3" Region "Account,N,3" Email "Account,K,3"
    #Then User Searches for account "Account,E,2" and selects it
    Then Navigate to the Related contacts section and click on add Relationship
    And Enter details for New Account Relationship like Contact "Account,B,5",Roles "Account,I,2",Relationship Strength "Account,J,2" and Start Date
    And Save the details for New Account Relationship
    #And Enter the Relationship contact information "Account,B,2" and save it
    Then Verify the contact name "Account,B,5" and details

  #Then Logout from the SF SVO Portal
  #User adds new vehicle ownership as a driver to an individual account [Excluding end date]
  @SVO @SVO_036 @secondBatch @SVOBatch
  Scenario: User adds new vehicle ownership as a driver to an individual account [Excluding end date]
    And mark test script status in "ScriptResults,C,36"
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Individual account with Record type "Account,A,2" First name "Account,L,2" Last name "Account,M,3" Region "Account,N,3" Email "Account,K,3"
    Then Navigate to the Vehicles Owned section and click on new
    And select the ownership as driver and enter the Contact "Account,E,2" Brand "Account,C,2" and Model "Account,D,2" information and save it
    Then Verify the added new vehicle

  #Then Logout from the SF SVO Portal
  #User adds multiple vehicle ownership/vehicle driven to an individual account [As a Owner]
  @SVO @SVO_037 @secondBatch @SVOBatch
  Scenario: User adds multiple vehicle ownership/vehicle driven to an individual account [As a Owner]
    And mark test script status in "ScriptResults,C,37"
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Individual account with Record type "Account,A,2" First name "Account,L,2" Last name "Account,M,3" Region "Account,N,3" Email "Account,K,3"
    Then Navigate to the Vehicles Owned section and click on new
    And select the ownership as Owner and enter the Brand "Account,C,2" Model "Account,D,2" Start date "Account,F,2" and End date "Account,G,2" information
    Then click on save and new vehicle button
    And select the ownership as Owner and enter the Brand "Account,C,2" Model "Account,D,2" Start date "Account,F,3" and End date "Account,G,3" information
    Then click on save vehicle button
    #Given User access SVO Portal with SO Data Manager User
    #When User Navigate to Accounts
    #And User opens any individual account having name "Account,M,3"
    #Then Navigate to the Vehicles Owned section
    Then Verify and delete the added vehicle

  #Then Logout from the SF SVO Portal
  #User adds Vehicle Driven as a owner to an individual account with end date
  @SVO @SVO_038 @secondBatch @SVOBatch
  Scenario: User adds Vehicle Driven as a owner to an individual account with end date
    And mark test script status in "ScriptResults,C,38"
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Individual account with Record type "Account,A,2" First name "Account,L,2" Last name "Account,M,3" Region "Account,N,3" Email "Account,K,3"
    Then Navigate to the Vehicles Driven section and click on New owner record type
    Then Enter details in new Vehicles driven and Save it

  #Then Logout from the SF SVO Portal
  #User cannot be able to select end date prior to start date while creating a new Vehicle Ownership/Vehicle Driven for an individual account
  @SVO @SVO_039 @SSEC-310 @secondBatch @SVOBatch
  Scenario: User cannot be able to select end date prior to start date while creating a new Vehicle Ownership/Vehicle Driven for an individual account
    And mark test script status in "ScriptResults,C,39"
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Individual account with Record type "Account,A,2" First name "Account,L,2" Last name "Account,M,3" Region "Account,N,3" Email "Account,K,3"
    Then Navigate to the Vehicles Driven section and click on New owner record type
    Then Enter details and past end date in new Vehicles driven and Save it to verify the error

  #Then Logout from the SF SVO Portal
  #User is able to view list of Enquiries, Opportunities and SVO Client Opportunities
  @SVO @SVO_040 @SSEC-311 @secondBatch @SVOBatch
  Scenario: User is able to view list of Enquiries, Opportunities and SVO Client Opportunities
    And mark test script status in "ScriptResults,C,40"
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Individual account with Record type "Account,A,2" First name "Account,L,2" Last name "Account,M,3" Region "Account,N,3" Email "Account,K,3"
    And User is able to view list of Enquiries, Opportunities and SVO Client Opportunities

  #Then Logout from the SF SVO Portal
  #User is able to create a new KMI record for an individual account
  @SVO @SVO_041 @SSEC-312 @secondBatch @SVOBatch
  Scenario: User is able to create a new KMI record for an individual account
    And mark test script status in "ScriptResults,C,41"
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Individual account with Record type "Account,A,2" First name "Account,L,2" Last name "Account,M,3" Region "Account,N,3" Email "Account,K,3"
    Then Navigate to the KMI section and click on new
    And Enter all the mandatory information like Contact "Account,B,4" Product Offering "Account,H,4" Brand "Account,C,4" and Model "Account,D,4" information and save it
    And Verify the added new KMI record

  #Then Logout from the SF SVO Portal
  #An entry of newly created KMI account with any other contact details should not be shown under KMI section
  @SVO @SVO_042 @SSEC-313 @secondBatch @SVOBatch
  Scenario: An entry of newly created KMI account with any other contact details should not be shown under KMI section
    And mark test script status in "ScriptResults,C,42"
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Individual account with Record type "Account,A,2" First name "Account,L,2" Last name "Account,M,3" Region "Account,N,3" Email "Account,K,3"
    Then Navigate to the KMI section and click on new
    And Enter all the mandatory information like Contact "Account,B,3" Product Offering "Account,H,3" Brand "Account,C,3" and Model "Account,D,3" information and save it
    And Verify the no new KMI record is added

  #Then Logout from the SF SVO Portal
  #User is able to view details of an order into Orders Section, which is placed by an Opportunity using an individual account
  @SVO @SVO_043 @SSEC-314 @secondBatch @SVOBatch
  Scenario: User is able to view details of an order into Orders Section, which is placed by an Opportunity using an individual account
    And mark test script status in "ScriptResults,C,43"
    #Given Access SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Individual account with Record type "Account,A,2" First name "Account,L,2" Last name "Account,M,3" Region "Account,N,3" Email "Account,K,3"
    And User Navigates to Orders Section
    And User Creates a New Order
    And User Saves the New order
    Then Verify Order Details under orders section

  #Then Logout from the SVO Portal
  #User is able to create a new order with any record type for an individual account
  @SVO @SVO_044 @SSEC-315 @secondBatch @SVOBatch @Sanity
  Scenario: User is able to create a new order with any record type for an individual account
    And mark test script status in "ScriptResults,C,44"
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Individual account with Record type "Account,A,2" First name "Account,L,2" Last name "Account,M,3" Region "Account,N,3" Email "Account,K,3"
    And User Navigates to Orders Section
    And User Creates a New Order
    And User Saves the New order

  #And User Deletes the Order
  #Then Logout from the SF SVO Portal
  #User is able to add a note to an individual account into Notes section
  @SVO @SVO_045 @SSEC-316 @secondBatch @SVOBatch
  Scenario: User is able to add a note to an individual account into Notes section
    And mark test script status in "ScriptResults,C,45"
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Individual account with Record type "Account,A,2" First name "Account,L,2" Last name "Account,M,3" Region "Account,N,3" Email "Account,K,3"
    Then Navigate to the Notes section and click on New
    Then Create a new Note and Save it

  #Then Logout from the SF SVO Portal
  #User is able to view Person Account History for an individual account
  @SVO @SVO_046 @SSEC-317 @secondBatch @SVOBatch
  Scenario: User is able to view Person Account History for an individual account
    And mark test script status in "ScriptResults,C,46"
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Individual account with Record type "Account,A,2" First name "Account,L,2" Last name "Account,M,3" Region "Account,N,3" Email "Account,K,3"
    Then Navigate to the Person Account History section and views Person Account History for an individual account

  #Then Logout from the SF SVO Portal
  #User is able to update consent for marketing communication for an individual account
  @SVO @SVO_047 @SSEC-318 @secondBatch @SVOBatch
  Scenario: User is able to update consent for marketing communication for an individual account
    And mark test script status in "ScriptResults,C,47"
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Individual account with Record type "Account,A,2" First name "Account,L,2" Last name "Account,M,3" Region "Account,N,3" Email "Account,K,3"
    Then Click on Update Consent
    Then Update the consent as required and Save

  #Then Logout from the SF SVO Portal
  #User is able to search recently created individual account in Contacts Menu
  @SVO @SVO_048 @SSEC-319 @secondBatch @SVOBatch
  Scenario: User is able to search recently created individual account in Contacts Menu
    And mark test script status in "ScriptResults,C,48"
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Individual account with Record type "Account,A,2" First name "Account,L,2" Last name "Account,M,3" Region "Account,N,3" Email "Account,K,3"
    When User Navigate to Contacts tab
    Then User Searches for Contacts "Account,M,3" and selects it
    And Verify the contact information Account name "Account,E,5" and email ID "Account,K,5"

  #Then Logout from the SF SVO Portal
  #User is able to change an owner of an individual account
  @SVO @SVO_049 @SSEC-320 @secondBatch @SVOBatch
  Scenario: User is able to change an owner of an individual account
    And mark test script status in "ScriptResults,C,49"
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Individual account with Record type "Account,A,2" First name "Account,L,2" Last name "Account,M,3" Region "Account,N,3" Email "Account,K,3"
    When User clicks on the change Owner and change Owner as "Account,B,6"
    #And User selects all the check boxes
    Then User clicks on submit button for Owner change

  #Then Logout from the SF SVO Portal
  #User changes the Person Account type or record type of an individual account
  @SVO @SVO_050 @SSEC-321 @secondBatch @SVOBatch @Sanity
  Scenario: User changes the Person Account type or record type of an individual account
    And mark test script status in "ScriptResults,C,50"
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Individual account with Record type "Account,A,2" First name "Account,L,2" Last name "Account,M,3" Region "Account,N,3" Email "Account,K,3"
    When User clicks on change Record type and selects record type as "Account,A,7"
    Then User clicks save to proceed with the selected record type

  #Then Logout from the SF SVO Portal
  #User can be able to give an access to any user from an individual account
  @SVO @SVO_051 @SSEC-322 @thirdBatch @SVOBatch @51Failed
  Scenario: User can be able to give an access to any user from an individual account
    And mark test script status in "ScriptResults,C,51"
    Given Access to SVO Portal
    #Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Individual account with Record type "Account,A,10" First name "Account,L,10" Last name "Account,M,10" Region "Account,N,10" Email "Account,K,10"
    Then Click on Sharing from the dropdown
    Then Update the access to any user and save it

  #Then Logout from the SF SVO Portal
  #User should not be able to search any account with invalid account name
  @SVO @SVO_052 @SSEC-323 @thirdBatch @SVOBatch
  Scenario: User should not be able to search any account with invalid account name
    And mark test script status in "ScriptResults,C,52"
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then User Searches for invalid account "testabc" and verifies the error

  #Then Logout from the SF SVO Portal
  #User is able to search and view details of an account by account name
  @SVO @SVO_053 @SSEC-324 @thirdBatch @SVOBatch
  Scenario: User is able to search and view details of an account by account name
    And mark test script status in "ScriptResults,C,53"
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Individual account with Record type "Account,A,10" First name "Account,L,10" Last name "Account,M,10" Region "Account,N,10" Email "Account,K,10"
    Then Click on Edit
    Then Update the required details of the account

  #Then Logout from the SF SVO Portal
  #User is able to create a new corporate account
  @SVO @SVO_054 @SSEC-325 @thirdBatch @SVOBatch
  Scenario: User is able to create a new corporate account
    And mark test script status in "ScriptResults,C,54"
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Corporate account with Record type "Account,A,9" Account name "Account,E,9" Email "Account,K,10" Region "Account,N,10"

  #Then Logout from the SF SVO Portal
  #User does not enter phone number or email id while creating a new corporate account
  @SVO @SVO_055 @SSEC-326 @thirdBatch @SVOBatch
  Scenario: User does not enter phone number or email id while creating a new corporate account
    And mark test script status in "ScriptResults,C,55"
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Corporate account with Record type "Account,A,9" Account name "Account,E,9" Region "Account,I,9" and without Phone or Email and verify the error

  #Then Logout from the SF SVO Portal
  #User clicks on View Account Hierarchy before adding parent account
  @SVO @SVO_056 @SSEC-327 @thirdBatch @SVOBatch
  Scenario: User clicks on View Account Hierarchy before adding parent account
    And mark test script status in "ScriptResults,C,56"
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Corporate account with Record type "Account,A,9" Account name "Account,E,9" Email "Account,K,10" Region "Account,N,10"
    Then Click on View Account Hierarchy and verify only current account is displayed

  #Then Logout from the SF SVO Portal
  #User clicks on View Account Hierarchy after adding parent account
  @SVO @SVO_057 @SSEC-328 @thirdBatch @SVOBatch @Sanity
  Scenario: User clicks on View Account Hierarchy after adding parent account
    And mark test script status in "ScriptResults,C,57"
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Corporate account with Record type "Account,A,9" Account name "Account,E,9" Email "Account,K,10" Region "Account,N,10"
    Then User adds parent account in details section
    Then Click on View Account Hierarchy and verify both parent and current account is displayed

  #Then Logout from the SF SVO Portal
  #User should be able to change account owner using Change owner option
  @SVO @SVO_058 @SSEC-329 @thirdBatch @ThirdBatch @SVOBatch @Sanity
  Scenario: User should be able to change account owner using Change owner option
    And mark test script status in "ScriptResults,C,58"
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Corporate account with Record type "Account,A,9" Account name "Account,E,9" Email "Account,K,10" Region "Account,N,10"
    When User clicks on the change Owner and change Owner as "Account,B,5"
    #And User selects all the check boxes
    Then User clicks on submit button for Owner change

  #Then Logout from the SF SVO Portal
  #User creates a new SAP Account for an Corporate account
  @SVO @SVO_059 @SSEC-330 @thirdBatch @SVOBatch
  Scenario: User creates a new SAP Account for an Corporate account
    And mark test script status in "ScriptResults,C,59"
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Corporate account with Record type "Account,A,9" Account name "Account,E,9" Email "Account,H,9" Region "Account,I,9"
    Then Navigate to the SAP Accounts section and click on New
    Then Enter details in new SAP Account and Save it

  #Then Logout from the SF SVO Portal
  #User can be able to create multiple SAP Account for the same Corporate account
  @SVO @SVO_060 @SSEC-331 @thirdBatch @SVOBatch @Sanity
  Scenario: User can be able to create multiple SAP Account for the same Corporate account
    And mark test script status in "ScriptResults,C,60"
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Corporate account with Record type "Account,A,9" Account name "Account,E,9" Email "Account,K,10" Region "Account,N,10"
    Then Navigate to the SAP Accounts section and click on New
    Then Enter details in new SAP Account and Save it
    Then Click on New SAP Account
    Then Enter details in new SAP Account and Save it

  #Then Logout from the SF SVO Portal
  #User can add a New Contact from Related Contacts link
  @SVO @SVO_061 @SSEC-332 @thirdBatch @ThirdBatch @SVOBatch
  Scenario: User can add a New Contact from Related Contacts link
    And mark test script status in "ScriptResults,C,61"
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Corporate account with Record type "Account,A,9" Account name "Account,E,9" Email "Account,K,10" Region "Account,N,10"
    Then Navigate to the Related contacts section and click on New contact
    Then Enter details for New "Corporate Contact" like first name "Account,L,6" last name "Account,M,6" email "Account,K,6" and Save it

  #Then Logout from the SF SVO Portal
  #User can add a Relationship from Related Contacts link
  @SVO @SVO_062 @SSEC-333 @thirdBatch @SVOBatch
  Scenario: User can add a Relationship from Related Contacts link
    And mark test script status in "ScriptResults,C,62"
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Corporate account with Record type "Account,A,9" Account name "Account,E,9" Email "Account,K,10" Region "Account,N,10"
    Then Navigate to the Related contacts section and click on add Relationship
    Then Enter the Relationship contact information "Account,B,2" and save it

  #Then Logout from the SF SVO Portal
  #User can add New vehicle ownership using Vehicles owned link
  @SVO @SVO_063 @SSEC-334 @thirdBatch @SVOBatch
  Scenario: User can add New vehicle ownership using Vehicles owned link
    And mark test script status in "ScriptResults,C,63"
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Corporate account with Record type "Account,A,9" Account name "Account,E,9" Email "Account,H,9" Region "Account,I,9"
    Then Navigate to the Vehicles Owned section and click on New owner record type
    Then Enter details in new Vehicles owned and Save it

  #Then Logout from the SF SVO Portal
  #User can create an enquiry using Enquiries link
  @SVO @SVO_064 @SSEC-335 @thirdBatch @SVOBatch
  Scenario: User can create an enquiry using Enquiries link
    And mark test script status in "ScriptResults,C,64"
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Corporate account with Record type "Account,A,9" Account name "Account,E,9" Email "Account,K,10" Region "Account,N,10"
    Then Navigate to Enquiries section and click on New enquiry
    Then Enter the details "Enquiry title" in new enquiry and save it

  #Then Logout from the SF SVO Portal
  #User can create new SVO client Opportunities for a Corporate account
  @SVO @SVO_065 @SSEC-336 @thirdBatch @SVOBatch
  Scenario: User can create new SVO client Opportunities for a Corporate account
    And mark test script status in "ScriptResults,C,65"
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Corporate account with Record type "Account,A,9" Account name "Account,E,9" Email "Account,H,9" Region "Account,I,9"
    Then Navigate to the SVO Client Opportunities section and click on New SVO "SVO Bespoke" opportunity
    Then Enter Required details in New Opportunity like Product Offering "Opportunity,B,2" Source "Opportunity,C,2" Account Name "Opportunity,D,2" Region "Opportunity,E,2" Preferred Contact "Opportunity,H,2" Brand "Opportunity,I,2" and Model "Opportunity,J,2"

  #Then Logout from the SF SVO Portal
  #User can create a new opportunity with Classic Record type for a corporate account
  @SVO @SVO_066 @SSEC-337 @thirdBatch @SVOBatch
  Scenario: User can create a new opportunity with Classic Record type for a corporate account
    And mark test script status in "ScriptResults,C,66"
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Corporate account with Record type "Account,A,9" Account name "Account,E,9" Email "Account,H,9" Region "Account,I,9"
    Then Navigate to the SVO Client Opportunities section and click on New Classic "Classic service" opportunity
    Then Enter Required details in New Opportunity like Product Offering "Opportunity,B,2" Source "Opportunity,C,2" Account Name "Opportunity,D,2" Region "Opportunity,E,2" Preferred Contact "Opportunity,H,2" Brand "Opportunity,I,2" and Model "Opportunity,J,2"

  #Then Logout from the SF SVO Portal
  #User can create a new Order for a Corporate account
  @SVO @SVO_67 @SSEC-338 @thirdBatch @SVOBatch
  Scenario: User can create a new Order for a Corporate account
    And mark test script status in "ScriptResults,C,67"
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Corporate account with Record type "Account,A,9" Account name "Account,E,9" Email "Account,K,10" Region "Account,N,10"
    And User Navigates to Orders Section
    And User Creates a New Order
    And User Saves the New order
    And User Deletes the Order

  #Then Logout from the SF SVO Portal
  #User is able to add a note to a corporate account into Notes section
  @SVO @SVO_068 @SSEC-339 @thirdBatch @SVOBatch
  Scenario: User is able to add a note to a corporate account into Notes section
    And mark test script status in "ScriptResults,C,68"
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Corporate account with Record type "Account,A,9" Account name "Account,E,9" Email "Account,K,9" Region "Account,N,9"
    Then Navigate to the Notes section and click on New
    Then Create a new Note with title "New title" and description "New desc" and Save it

  #Then Logout from the SF SVO Portal
  #User is able to view Account History for a corporate account
  @SVO @SVO_069 @SSEC-340 @thirdBatch @SVOBatch
  Scenario: User is able to view Account History for a corporate account
    And mark test script status in "ScriptResults,C,69"
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Corporate account with Record type "Account,A,9" Account name "Account,E,9" Email "Account,K,9" Region "Account,N,9"
    Then Navigate to the Account History section and view Account History for an Corporate account

  #Then Logout from the SF SVO Portal
  #User should not be able to edit the Account History
  @SVO @SVO_070 @SSEC-341 @thirdBatch @SVOBatch
  Scenario: User should not be able to edit the Account History
    And mark test script status in "ScriptResults,C,70"
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Corporate account with Record type "Account,A,9" Account name "Account,E,9" Email "Account,K,9" Region "Account,N,9"
    Then Navigate to the Account History section and verify user cannot edit the Account History for an Corporate account

  #Then Logout from the SF SVO Portal
  #In this scenario user tries to create classic opportunity for corporate account
  @SVO @SVO_071 @SSEC-342 @thirdBatch @SVOBatch
  Scenario: User is able to create Opportunities (except SVO Bespoke and SVO Private Office) for a Corporate account
    And mark test script status in "ScriptResults,C,71"
    #Given Access to SVO Portal with username "LoginUsers,B,5" and password "LoginUsers,C,5"
    Given User access SVO Portal
    When User Navigate to Accounts
    And User searches for a User Account "Account,E,9" with AccountType "Account,U,2"
    Then User navigate to Opportunity Section
    And User creates a new Opportunity with Record Type "Opportunity,A,3" for Corporate Account
    Then Enter Required details in New Opportunity like Product Offering "Opportunity,B,2" Source "Opportunity,C,2" Account Name "Opportunity,D,5" Region "Opportunity,E,2" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,2" and Model "Opportunity,J,2"

  #Then Logout from the SF SVO Portal
  #In this Scenario User tries to delete Corporate Account
  @SVO @SVO_072 @SSEC-343 @thirdBatch @SVOBatch
  Scenario: User can delete the Corporate account created
    And mark test script status in "ScriptResults,C,72"
    #Given Access to SVO Portal with username "LoginUsers,B,11" and password "LoginUsers,C,11"
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Corporate account with Record type "Account,A,9" Account name "Account,E,9" Email "Account,K,10" Region "Account,N,10"
    And User tries to delete created Corporate Account

  #Then Logout from the SF SVO Portal
  #User should be able to spot difference between Individual and Corporate accounts
  @SVO @SVO_073 @SSEC-344 @thirdBatch @SVOBatch
  Scenario: User should be able to spot difference between Individual and Corporate accounts
    And mark test script status in "ScriptResults,C,73"
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Individual account with Record type "Account,A,10" First name "Account,L,10" Last name "Account,M,10" Region "Account,N,10" Email "Account,K,10"
    Then Verify that Vehicles Driven link is available in Related List Quick links section for Individual account
    When User Navigate to Accounts
    Then Create new Corporate account with Record type "Account,A,9" Account name "Account,E,9" Email "Account,K,9" Region "Account,N,9"
    Then Verify that Vehicles Driven link is not available in Related List Quick links section for Corporate account

  #Then Logout from the SF SVO Portal
  #User creates SVO Bespoke Opportunity
  @SVO @SVO_74 @SSEC-345 @thirdBatch @SecondDemo @SVOBatch @Recon74
  Scenario: User creates a new Opportunity with SVO Bespoke Record Type
    And mark test script status in "ScriptResults,C,74"
    Given Access SVO Portal
    #Given User access SVO Portal
    When User Navigate to Opportunity tab
    And User Creates New Opportunity with "Opportunity,A,8" Record Type
    Then User enters mandatory fields for an Opportunity like Stage "Opportunity,R,4" Product Offering "Opportunity,B,8" Region "Opportunity,E,8" Sales Area "Opportunity,F,8" Market "Opportunity,G,8" Preferred Contact "Opportunity,H,8" Account Name "Opportunity,D,5" Source "Opportunity,C,8" Brand "Opportunity,I,8" Model "Opportunity,J,8"
    And User verifies Reference Number is generated
    And User navigates to Commissioning Request
    And User Creates a new Commissioning Request with "CommissioningRequest,A,2" Record type
    And User enters all mandatory fields and save commissioning Request
    Then User verifies that linked opportunity is displayed under Bespoke Opportunity Details

  #Then Verify that User can create New Reservation from Calendar with Primary Contact "CommissioningRequest,B,2"
  #Then Logout from SF_SVO Portal
  #User creates Classic Bespoke Opportunity
  @SVO @SVO_75 @SSEC-346 @thirdBatch @SVOBatch @Sanity
  Scenario: User creates a new Opportunity with Classic Bespoke Record Type
    And mark test script status in "ScriptResults,C,75"
    #Given Access to SF-SVO Portal with username "LoginUsers,B,6" and password "LoginUsers,C,6"
    Given User access SVO Portal
    When User Navigate to Opportunity tab
    #And User Creates New Opportunity with "Opportunity,A,3" Record Type
    And User Creates New Opportunity with "Classic Bespoke" Record Type
    Then User enters mandatory fields for an Opportunity like Stage "Opportunity,R,4" Product Offering "Opportunity,B,3" Region "Opportunity,E,3" Sales Area "Opportunity,F,3" Market "Opportunity,G,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,D,5" Source "Opportunity,C,3" Brand "Opportunity,I,3" Model "Opportunity,J,3"
    And User verifies Reference Number is generated
    And User click on the mark stage as complete button to complete the process
    And User click on the mark stage as complete button to complete the process
    Then Click on Send POF button
    Then Click on Merge & Adobe sign button
    Then Click on Send button
    Then User access to User Email address to sign the contract
    And User navigate to Agreement section of an opportunity and verify the status of contract is signed

  #And mark this script status in "ScriptResults,C,75"
  #Then Logout from SF_SVO Portal
  #User creates Classic Continuation Opportunity
  @SVO @SVO_76 @SSEC-347 @forthBatch @SVOBatch
  Scenario: User creates a new Opportunity with Classic Continuation Record Type
    And mark test script status in "ScriptResults,C,76"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    When User Navigate to Opportunity tab
    And User Creates New Opportunity with "Opportunity,A,7" Record Type
    Then User enters mandatory fields for an Opportunity like Stage "Opportunity,R,9" Product Offering "Opportunity,B,3" Region "Opportunity,E,3" Sales Area "Opportunity,F,3" Market "Opportunity,G,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,D,5" Source "Opportunity,C,3" Brand "Opportunity,I,3" Model "Opportunity,J,3"
    And User verifies Reference Number is generated

  #Then Logout from SF_SVO Portal
  #User creates Classic Limited Edition Opportunity
  @SVO @SVO_77 @SSEC-348 @forthBatch @SVOBatch
  Scenario: User creates a new Opportunity with Classic Limited Edition Record Type
    And mark test script status in "ScriptResults,C,77"
    #Given Access to SF-SVO Portal with username "LoginUsers,B,6" and password "LoginUsers,C,6"
    Given User access SF-SVO Portal
    When User Navigate to Opportunity tab
    And User Creates New Opportunity with "Opportunity,A,9" Record Type
    Then User enters mandatory fields for an Opportunity like Stage "Opportunity,R,9" Product Offering "Opportunity,B,3" Region "Opportunity,E,3" Sales Area "Opportunity,F,3" Market "Opportunity,G,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,D,5" Source "Opportunity,C,3" Brand "Opportunity,I,3" Model "Opportunity,J,3"
    And User verifies Reference Number is generated

  #Then Logout from SF_SVO Portal
  #User creates Classic Reborn Opportunity
  @SVO @SVO_78 @SSEC-349 @forthBatch @SVOBatch @Sanity
  Scenario: User creates a new Opportunity with Classic Reborn Record Type
    And mark test script status in "ScriptResults,C,78"
    #Given Access to SF-SVO Portal with username "LoginUsers,B,6" and password "LoginUsers,C,6"
    Given User access SF-SVO Portal
    When User Navigate to Opportunity tab
    And User Creates New Opportunity with "Opportunity,A,4" Record Type
    Then User enters mandatory fields for an Opportunity like Stage "Opportunity,R,4" Product Offering "Opportunity,B,3" Region "Opportunity,E,3" Sales Area "Opportunity,F,3" Market "Opportunity,G,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,D,5" Source "Opportunity,C,3" Brand "Opportunity,I,3" Model "Opportunity,J,3"
    And User verifies Reference Number is generated

  #Then Logout from SF_SVO Portal
  #User Creates Classic Service Opportunity
  @SVO @SVO_79 @SSEC-350 @forthBatch @SVOBatch
  Scenario: User creates a new Opportunity with Classic Service Record Type
    And mark test script status in "ScriptResults,C,79"
    #Given Access to SF-SVO Portal with username "LoginUsers,B,6" and password "LoginUsers,C,6"
    Given User access SF-SVO Portal
    When User Navigate to Opportunity tab
    And User Creates New Opportunity with "Opportunity,A,2" Record Type
    Then User enters mandatory fields for an Opportunity like Stage "Opportunity,R,4" Product Offering "Opportunity,B,3" Region "Opportunity,E,3" Sales Area "Opportunity,F,3" Market "Opportunity,G,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,D,5" Source "Opportunity,C,3" Brand "Opportunity,I,3" Model "Opportunity,J,3"
    And User verifies Reference Number is generated

  #Then Logout from SF_SVO Portal
  #User creates Classic Works Legend Opportunity
  @SVO @SVO_80 @SSEC-351 @forthBatch @SVOBatch @Sanity
  Scenario: User creates a new Opportunity with Classic Works Legend Type
    And mark test script status in "ScriptResults,C,80"
    #Given Access to SF-SVO Portal with username "LoginUsers,B,6" and password "LoginUsers,C,6"
    Given User access SF-SVO Portal
    When User Navigate to Opportunity tab
    And User Creates New Opportunity with "Opportunity,A,6" Record Type
    Then User enters mandatory fields for an Opportunity like Stage "Opportunity,R,4" Product Offering "Opportunity,B,3" Region "Opportunity,E,3" Sales Area "Opportunity,F,3" Market "Opportunity,G,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,D,5" Source "Opportunity,C,3" Brand "Opportunity,I,3" Model "Opportunity,J,3"
    And User verifies Reference Number is generated

  #Then Logout from SF_SVO Portal
  #User creates SVO Private Office Opportunity
  @SVO @SVO_81 @SSEC-352 @forthBatch @SVOBatch @Sanity
  Scenario: User creates a new Opportunity with SVO Private Office Record Type
    And mark test script status in "ScriptResults,C,81"
    #Given Access to SF-SVO Portal with username "LoginUsers,B,6" and password "LoginUsers,C,6"
    Given User access SF-SVO Portal
    When User Navigate to Opportunity tab
    And User Creates New Opportunity with "Opportunity,A,10" Record Type
    Then User enters mandatory fields for an Opportunity like Stage "Opportunity,R,4" Product Offering "Opportunity,B,3" Region "Opportunity,E,3" Sales Area "Opportunity,F,3" Market "Opportunity,G,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,D,5" Source "Opportunity,C,3" Brand "Opportunity,I,3" Model "Opportunity,J,3"
    And User verifies Reference Number is generated
    And User navigates to Commissioning Request
    And User Creates a new Commissioning Request with "CommissioningRequest,A,2" Record type
    And User enters all mandatory fields and save commissioning Request
    Then User verifies that linked opportunity is displayed under Bespoke Opportunity Details

  #Then Logout from SF_SVO Portal
  #User tries to re-open the closed opportunity[Move stage from closed to Qualified]
  @SVO @SVO_82 @SSEC-353 @forthBatch @SVOBatch
  Scenario: User tries to re-open the closed opportunity[Move stage from closed to Qualified]
    And mark test script status in "ScriptResults,C,82"
    #Given Access SF_SVO Portal
    Given User access SF-SVO Portal
    Then User Navigate to Opportunity tab
    And User searched for Closed opportunity
    Then User tries to reopen closed Opportunity to "Qualified" Stage

  #Then Logout from SF_SVO Portal
  #User tries to move the opportunity stage from design brief & quote to contract signed without adding quote for SVO Private Office
  @SVO @SVO_83 @SSEC-354 @forthBatch @SecondDemo @SVOBatch
  Scenario: User tries to move the opportunity stage from design brief & quote to contract signed without adding quote for SVO Private Office
    And mark test script status in "ScriptResults,C,83"
    #Given Access SVO Portal
    Given User access SF-SVO Portal
    Then User Navigate to Opportunity tab
    And User Creates New Opportunity with "Opportunity,A,8" Record Type
    Then User enters mandatory fields for an Opportunity like Stage "Opportunity,R,4" Product Offering "Opportunity,B,8" Region "Opportunity,E,8" Sales Area "Opportunity,F,8" Market "Opportunity,G,8" Preferred Contact "Opportunity,H,8" Account Name "Opportunity,D,5" Source "Opportunity,C,8" Brand "Opportunity,I,8" Model "Opportunity,J,8"
    And User click on the mark stage as complete button to complete the process
    And User click on the mark stage as complete button to complete the process
    And Verify that the opportunity stage moves to Design Brief & Quote
    And User click on the mark stage as complete button to complete the process
    Then Verify that user can move to "Contract Signed" stage

  #Then Logout from SF_SVO Portal
  #User tries to move the opportunity stage from  contract signed to order placed without an order creation
  @SVO @SVO_84 @SSEC-355 @forthBatch @SVOBatch
  Scenario: User tries to move the opportunity stage from  contract signed to order placed without an order creation
    And mark test script status in "ScriptResults,C,84"
    #Given Access SF_SVO Portal
    Given User access SF-SVO Portal
    Then User Navigate to Opportunity tab
    #Then User searches for "Opportunity,AC,5" opportunities from select list view
    And Open an opportunity with Record Type "Opportunity,R,11"
    And User click on the mark stage as complete button to complete the process
    Then Verify the error message to close the opportunity

  #Then Logout from SF_SVO Portal
  #User can get vehicle information from Vehicle Search Option for any non-closed Opportunity
  @SVO @SVO_085 @SSEC-356 @forthBatch @SVOBatch
  Scenario: User can get vehicle information from Vehicle Search Option for any non-closed Opportunity
    And mark test script status in "ScriptResults,C,85"
    #Given Access the SVO Portal
    Given User access SF-SVO Portal
    When User Navigate to Opportunity
    And Open an opportunity with Record Type "Opportunity,R,11"
    Then User clicks on search Vehicle
    Then select brand as test brand and click search
    When click on new and enter Record name "New Enquiry,S,5" Full VIN "New Enquiry,K,5" Brand "New Enquiry,I,5" and Model "New Enquiry,J,5"
    And save the vehicle information

  #Then Logout from SF_SVO Portal
  #User tries to move the stage of an opportunity backward[Move from Contract signed to design & brief]
  @SVO @SVO_086 @SSEC-357 @forthBatch @SVOBatch
  Scenario: User tries to move the stage of an opportunity backward[Move from Contract signed to design & brief]
    And mark test script status in "ScriptResults,C,86"
    #Given Access the SVO Portal
    Given User access SF-SVO Portal
    When User Navigate to Opportunity
    And User searches for Contract signed opportunity and selects it
    Then User selects Design Brief and Quote and clicks on mark as current stage
    And verify the displayed flow error message

  #Then Logout from SF_SVO Portal
  #User clone the closed opportunity
  @SVO @SVO_087 @SSEC-358 @forthBatch @SVOBatch
  Scenario: User clone the closed opportunity
    And mark test script status in "ScriptResults,C,87"
    #Given Access the SVO Portal
    Given User access SF-SVO Portal
    When User Navigate to Opportunity
    And User searches for Closed won opportunity "Opportunity,L,5" and selects it
    Then user clicks on dropdown and clicks on the clone button
    And user saves the clone opportunity
    When User Navigate to Opportunity
    And User searches for Closed won opportunity "Opportunity,L,5" and selects it

  #Then Logout from SF_SVO Portal
  #User Edits the stage of an opportunity from Waitlist to Closed lost
  @SVO @SVO_088 @SSEC-359 @forthBatch @SecondDemo @SVOBatch @Sanity
  Scenario: User Edits the stage of an opportunity from Waitlist to Closed lost
    And mark test script status in "ScriptResults,C,88"
    #Given Access SVO Portal
    Given User access SF-SVO Portal
    When User Navigate to Opportunity
    And User Creates New Opportunity with "Opportunity,A,9" Record Type
    Then User enters mandatory fields for an Opportunity like Stage "Opportunity,R,9" Product Offering "Opportunity,B,3" Region "Opportunity,E,3" Sales Area "Opportunity,F,3" Market "Opportunity,G,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,D,5" Source "Opportunity,C,3" Brand "Opportunity,I,3" Model "Opportunity,J,3"
    Then user selects the closed tab and selects stage as "Opportunity,R,8" and Reason "Opportunity,T,8"

  #Then Logout from SF_SVO Portal
  #User tries to change the Record type of an closed opportunity
  @SVO @SVO_089 @SSEC-360 @forthBatch @SVOBatch
  Scenario: User tries to change the Record type of an closed opportunity
    And mark test script status in "ScriptResults,C,89"
    #Given Access the SVO Portal
    Given User access SF-SVO Portal
    When User Navigate to Opportunity
    And User searches for Closed won opportunity "Opportunity,L,5" and selects it
    Then user changes the record type of the opportunity
    And Verify the displayed record error message

  #Then Logout from SF_SVO Portal
  #User selects Unchecked for Restricted Party Screening field for an open opportunity
  @SVO @SVO_090 @SSEC-361 @forthBatch @SecondDemo @SVOBatch
  Scenario: User selects Unchecked for Restricted Party Screening field for an open opportunity
    And mark test script status in "ScriptResults,C,90"
    #Given Access the SVO Portal
    Given User access SF-SVO Portal
    When User Navigate to Opportunity
    And User Creates New Opportunity with "Opportunity,A,3" Record Type
    Then User enters mandatory fields for an Opportunity like Stage "Opportunity,R,4" Product Offering "Opportunity,B,3" Region "Opportunity,E,3" Sales Area "Opportunity,F,3" Market "Opportunity,G,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,D,5" Source "Opportunity,C,3" Brand "Opportunity,I,3" Model "Opportunity,J,3"
    Then user clicks on contracting and clicks on Mark as current stage
    And verify the displayed flow error message

  #Then Logout from SF_SVO Portal
  #User get build capacity information by clicking on blue link of Programme
  #Classic Limited Edition opportunity
  @SVO @SVO_091 @SSEC-362 @forthBatch @SVOBatch
  Scenario: User get build capacity information by clicking on blue link of Programme
    And mark test script status in "ScriptResults,C,91"
    #Given Access SF_SVO Portal
    Given User access SF-SVO Portal
    Then User Navigate to SVO
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,5"
    And Enter details of Classic Limited Edition Opportunity like Opportunity name "Opportunity,L,2" Stage "Opportunity,R,9" Region "Opportunity,E,4" Account Name "Opportunity,D,9" Preferred Contact "Opportunity,H,9" Programme "Opportunity,U,6" and CloseDate "Opportunity,AI,2"
    Then Save the created Opportunity
    And Verify the Build Capacity of the Programme

  #Then Logout from SF_SVO Portal
  #User does not enter value for Programme field, while creating Classic Limited Edition and Classic Continuation Opportunity
  #Classic Limited Edition and Classic Continuation opportunities
  @SVO @SVO_092 @SSEC-363 @forthBatch @SVOBatch
  Scenario: User creates Classic Limited Edition and Classic Continuation Opportunity without entering Programme field
    And mark test script status in "ScriptResults,C,92"
    #Given Access SF_SVO Portal
    Given User access SF-SVO Portal
    Then User Navigate to SVO
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,5"
    And User enters fields like OpportunityName "Opportunity,L,9" CloseDate "Opportunity,AG,2" Stage "Opportunity,R,4" Region "Opportunity,E,4" AccountName "Opportunity,D,9" PreferredContact "Opportunity,H,9" Source "Opportunity,C,2"
    Then Verify the error message to fill the Programme
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,7"
    And User enters fields like OpportunityName "Opportunity,L,10" CloseDate "Opportunity,AG,2" Stage "Opportunity,R,4" Region "Opportunity,E,4" AccountName "Opportunity,D,9" PreferredContact "Opportunity,H,9" Source "Opportunity,C,2"
    Then Verify the error message to fill the Programme

  #Then Logout from SF_SVO Portal
  #User creates a new Programme while creaing Classic Limited Edition opportunity
  @SVO @SVO_093 @SSEC-364 @forthBatch @SVOBatch @Sanity
  Scenario: User creates a new Programme while creaing Classic Limited Edition opportunity
    And mark test script status in "ScriptResults,C,93"
    #Given Access to SVO Portal
    Given User access SF-SVO Portal
    When User Navigate to Opportunity
    Then User Creates a New opportunity with Record Type as "Opportunity,A,5"
    Then user navigated to Programme Field and clicks on new
    And enter the required information like name "Opportunity,U,5" product offering "Opportunity,B,5" build capacity "Opportunity,V,5" brand "Opportunity,I,5" and model "Opportunity,J,5"
    And save the programme
    Then cancel the opportunity creation

  #Then Logout from the SF SVO Portal
  #User generates document through S-DOCS for any open opportunity
  @SVO @SVO_094 @SSEC-365 @forthBatch @SVOBatch
  Scenario: User do E-Sign an agreement using Send Sales Agreement for any open opportunity
    #And mark test script status in "ScriptResults,C,94"
    Given Access the SVO Portal
    #Given User access SF-SVO Portal
    When User Navigate to Opportunity
    And User Creates New Opportunity with "Opportunity,A,8" Record Type
    Then User enters mandatory fields for an Opportunity like Stage "Opportunity,R,4" Product Offering "Opportunity,B,8" Region "Opportunity,E,8" Sales Area "Opportunity,F,8" Market "Opportunity,G,8" Preferred Contact "Opportunity,H,10" Account Name "Opportunity,D,10" Source "Opportunity,C,3" Brand "Opportunity,I,2" Model "Opportunity,J,2"
    And User click on the mark stage as complete button to complete the process
    And User click on the mark stage as complete button to complete the process
    Then Navigate to Quotes
    Then Create a new Quote "Opportunity,H,10" and save
    Then Navigate and open Design briefs
    Then Move the design brief stage from Draft to Submitted
    Then Return Render pack of design brief
    Then Navigate back to Quote section
    Then fill the price details of Quote like CostPrice "Opportunity,X,2" ListPrice "Opportunity,Y,2" Contribution "Opportunity,Z,2"  RetailPrice "Opportunity,AA,2" Revenue"Opportunity,AB,2"
    And User click on the mark stage as complete button to complete the process
    Then User clicks on Send Sales Agreement button
    And User verifies all the details and send the email to sign the agreement
    Then User access to User Email address to sign the contract
    And User verifies that the contract saved in agreement section of an opportunity and status is signed

  #When user selects a file and clicks on next step
  #Then user clicks on the E-sign button to send an email of the document
  #Then Logout from SF_SVO Portal
  #User is able to upload and view any document into Files Section for any open opportunity
  @SVO @SVO_095 @SSEC-366 @forthBatch @SVOBatch
  Scenario: User is able to upload and view any document into Files Section for any open opportunity
    And mark test script status in "ScriptResults,C,95"
    #Given Access the SVO Portal
    Given User access SF-SVO Portal
    When User Navigate to Opportunity
    And User Creates New Opportunity with "Opportunity,A,2" Record Type
    Then User enters mandatory fields for an Opportunity like Stage "Opportunity,R,4" Product Offering "Opportunity,B,3" Region "Opportunity,E,3" Sales Area "Opportunity,F,3" Market "Opportunity,G,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,D,5" Source "Opportunity,C,3" Brand "Opportunity,I,3" Model "Opportunity,J,3"
    Then user navigates to file section and clicks on add files
    And select a file and click on upload Files
    Then navigate to the file and delete it

  #Then Logout from SF_SVO Portal
  #User does not enter pricing details for any open SVO Bespoke opportunity which should not be in Order Placed or Closed stage
  @SVO @SVO_096 @SSEC-367 @forthBatch @SVOBatch
  Scenario: User does not enter pricing details for any open SVO Bespoke opportunity which should not be in Order Placed or Closed stage
    And mark test script status in "ScriptResults,C,96"
    #Given Access SF_SVO Portal
    Given User access SF-SVO Portal
    Then User Navigate to SVO
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,8"
    And fill all mandatory fields of SVOBespoke opportunity like OpportunityName "Opportunity,A,8" Closedate "Opportunity,AI,2" Stage "Opportunity,R,4" ProductOffering "Opportunity,B,2" Region "Opportunity,E,2" AccountName "Opportunity,D,9" PreferredContact "Opportunity,H,9" Brand "Opportunity,I,2" Model "Opportunity,J,2"
    Then User remove the Pricing details "Opportunity,X,2"
    Then User Move the stage to Order placed
    Then Verify the error message to fill the pricing details

  #Then Logout from SF_SVO Portal
  #User enters Production Year corresponds to Model for any open opportunity
  @SVO @SVO_097 @SSEC-368 @forthBatch @SVOBatch
  Scenario: User enters Production Year corresponds to Model for any open opportunity
    And mark test script status in "ScriptResults,C,97"
    #Given Access SF_SVO Portal
    Given User access SF-SVO Portal
    Then User Navigate to SVO
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,3"
    And fill all mandatory fields of SVOBespoke opportunity like OpportunityName "Opportunity,A,3" Closedate "Opportunity,AI,2" Stage "Opportunity,R,4" ProductOffering "Opportunity,B,3" Region "Opportunity,E,2" AccountName "Opportunity,D,9" PreferredContact "Opportunity,H,9" Brand "Opportunity,I,2" Model "Opportunity,J,2"
    Then User enters production year of Vehicle "Opportunity,AQ,2"

  #Then Logout from SF_SVO Portal
  #User tries to move to Quotation stage for any open opportunity [Other than SVO Opportunities]
  @Opportunity @SVO_098 @SSEC-369 @forthBatch @SVOBatch
  Scenario: User tries to move to Quotation stage for any open opportunity [Other than SVO Opportunities]
    And mark test script status in "ScriptResults,C,98"
    #Given Access to SVO Portal
    Given User access SF-SVO Portal
    When User Navigate to Opportunity
    Then User Create a New Opportunity with Record Type "Opportunity,A,9"
    Then User enters mandatory fields for an Opportunity like Stage "Opportunity,R,4" Product Offering "Opportunity,B,3" Region "Opportunity,E,3" Sales Area "Opportunity,F,3" Market "Opportunity,G,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,D,5" Source "Opportunity,C,3" Brand "Opportunity,I,3" Model "Opportunity,J,3"
    And User click on the mark stage as complete button to complete the process
    And User click on the mark stage as complete button to complete the process
    And User click on the mark stage as complete button to complete the process

  #Then Logout from SF_SVO Portal
  #User is not able to see S-DOCS button for an opportunity with specific record type
  @SVO @SVO_099 @SSEC-370 @forthBatch @SVOBatch @099
  Scenario: User is not able to see Send Sales Agreement and Send POF button for an opportunity with specific record type
    #And mark test script status in "ScriptResults,C,99"
    Given Access to SVO Portal
    #Given User access SF-SVO Portal
    When User Navigate to Opportunity
    And User searches for Classic Service opportunity and selects it
    Then Validate that user should not be able to see Send Sales Agreement and Send POF button

  #Then Logout from the SF SVO Portal
  #User searches document for different category for any closed opportunity
  @SVO @SVO_100 @SSEC-371 @forthBatch @SVOBatch
  Scenario: User searches document for different category for any closed opportunity
    And mark test script status in "ScriptResults,C,100"
    #Given Access to SVO Portal
    Given User access SF-SVO Portal
    When User Navigate to Opportunity
    And User searches for SVO Bespoke Closed opportunity and selects it
    Then Validate that user should not be able to see Send Sales Agreement button
    Then Validate that user should not be able to see Send POF button

  #Then Logout from the SF SVO Portal
  #User creates a new Compose Email for an existing Enquiry
  @SVO @SVO_101 @SSEC-372 @FifthBatch @SVOBatch
  Scenario: User creates a new Compose Email for an existing Enquiry
    And mark test script status in "ScriptResults,C,101"
    #Given Access SVO Portal
    Given User access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,2"
    And Enter the required details for the Enquiry creation
    Then Save the new Enquiry
    When user clicks on Compose Email button
    Then edit the required information as to "Enquiry,S,2" Related to "Enquiry,T,2" Additional to "Enquiry,U,2" CC "Enquiry,V,2" BCC "Enquiry,W,2" Subject "Enquiry,X,2" Body "Enquiry,Y,2" and Importance "Enquiry,Z,2"
    And Click on sent mail button

  #Then Logout from the SVO Portal
  #User selects template from Select Template button
  @SVO @SVO_102 @SSEC-373 @SVOBatch
  Scenario: User selects template from Select Template button
    And mark test script status in "ScriptResults,C,102"
    #Given Access SVO Portal
    Given User access SF-SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,2"
    And Enter the required details for the Enquiry creation
    Then Save the new Enquiry
    When user clicks on Compose Email button
    Then user clicks on select template button and selects a template and saves it

  #Then Logout from the SVO Portal
  #User Saves the email after entering all the details
  @SVO @SVO_103 @SSEC-374 @SVOBatch
  Scenario: User Saves the email after entering all the details
    And mark test script status in "ScriptResults,C,103"
    #Given Access SVO Portal
    Given User access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,2"
    And Enter the required details for the Enquiry creation
    Then Save the new Enquiry
    When user clicks on Compose Email button
    Then edit the required information as to "Enquiry,S,2" Related to "Enquiry,T,2" Additional to "Enquiry,U,2" CC "Enquiry,V,2" BCC "Enquiry,W,2" Subject "Enquiry,X,2" Body "Enquiry,Y,2" and Importance "Enquiry,Z,2"
    And Click on save mail button

  #Then Logout from the SVO Portal
  #User should be able to view saved emails in Save Draft
  @SVO @SVO_104 @SSEC-375 @SVOBatch
  Scenario: User should be able to view saved emails in Save Draft
    And mark test script status in "ScriptResults,C,104"
    #Given Access SVO Portal
    Given User access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,2"
    And Enter the required details for the Enquiry creation
    Then Save the new Enquiry
    When user clicks on Compose Email button
    And Click on save mail button
    Then Click on Drafts button

  #Then Logout from the SVO Portal
  #User is able to Attach file to the email
  @SVO @SVO_105 @SSEC-376 @SVOBatch
  Scenario: User is able to Attach file to the email
    And mark test script status in "ScriptResults,C,105"
    #Given Access SVO Portal
    Given User access SVO Portal
    When User Navigate to Enquiries
    Then User opens any open enquiry
    And User compose an email and attach some file from different location

  #Then Logout from the SVO Portal
  #User can Cancel email if required before sending
  @SVO @SVO_106 @SSEC-377 @SVOBatch
  Scenario: User can Cancel email if required before sending
    And mark test script status in "ScriptResults,C,106"
    #Given Access SVO Portal
    Given User access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,2"
    And Enter the required details for the Enquiry creation
    Then Save the new Enquiry
    When user clicks on Compose Email button
    Then edit the required information as to "Enquiry,S,2" Related to "Enquiry,T,2" Additional to "Enquiry,U,2" CC "Enquiry,V,2" BCC "Enquiry,W,2" Subject "Enquiry,X,2" Body "Enquiry,Y,2" and Importance "Enquiry,Z,2"
    Then Click on the Cancel button

  #Then Logout from the SVO Portal
  #An error message is displayed, when user tries to send an email without entering all the required fields
  @SVO @SVO_107 @SSEC-378 @SVOBatch
  Scenario: An error message is displayed, when user tries to send an email without entering all the required fields
    And mark test script status in "ScriptResults,C,107"
    #Given Access SVO Portal
    Given User access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,2"
    And Enter the required details for the Enquiry creation
    Then Save the new Enquiry
    Then Validate that preferred contact details is blank
    When user clicks on Compose Email button
    Then Click on the Send button and verify the error

  #Then Logout from the SVO Portal
  #User can able to view all the buttons on the header section of compose email and same buttons on the footer section
  @SVO @SVO_108 @SSEC-379 @SVOBatch
  Scenario: User can able to view all the buttons on the header section of compose email and same buttons on the footer section
    And mark test script status in "ScriptResults,C,108"
    #Given Access SVO Portal
    Given User access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,2"
    And Enter the required details for the Enquiry creation
    Then Save the new Enquiry
    When user clicks on Compose Email button
    Then Verify buttons on header and footer section

  #Then Logout from the SVO Portal
  #User is able to retrive saved emails from Drafts
  @SVO @SVO_109 @SSEC-380 @SVOBatch
  Scenario: User is able to retrive saved emails from Drafts
    And mark test script status in "ScriptResults,C,109"
    #Given Access SVO Portal
    Given User access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,2"
    And Enter the required details for the Enquiry creation
    Then Save the new Enquiry
    When user clicks on Compose Email button
    Then edit the required information as to "Enquiry,S,2" Related to "Enquiry,T,2" Additional to "Enquiry,U,2" CC "Enquiry,V,2" BCC "Enquiry,W,2" Subject "Enquiry,X,2" Body "Enquiry,Y,2" and Importance "Enquiry,Z,2"
    And Click on save mail button
    When user clicks on Compose Email button
    Then user clicks on drafts and retrives draft with Subject "Enquiry,X,2"
    And Click on sent mail button

  #Then Logout from the SVO Portal
  #User is able to view auto populated fields after opening Compose email
  @SVO @SVO_110 @SSEC-381 @SVOBatch
  Scenario: User is able to view auto populated fields after opening Compose email
    And mark test script status in "ScriptResults,C,110"
    #Given Access SVO Portal
    Given User access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,2"
    And Enter the required details for the Enquiry creation
    Then Save the new Enquiry
    When user clicks on Compose Email button
    And Verify that user is able to view auto populated fields after opening Compose email

  #Then Logout from the SVO Portal
  #User can edit auto populated field details in Compose email list
  @SVO @SVO_111 @SSEC-382 @SVOBatch
  Scenario: User can edit auto populated field details in Compose email list
    And mark test script status in "ScriptResults,C,111"
    #Given Access SVO Portal
    Given User access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,2"
    And Enter the required details for the Enquiry creation
    Then Save the new Enquiry
    When user clicks on Compose Email button
    Then Edit auto populated fields Related to "Enquiry,T,2" and Importance "Enquiry,Z,2" in Compose email page
    And Click on save mail button

  #Then Logout from the SVO Portal
  #User can add Related To on Compose Email page before sending
  @SVO @SVO_112 @SSEC-383 @SVOBatch
  Scenario: User can add Related To on Compose Email page before sending
    And mark test script status in "ScriptResults,C,112"
    #Given Access SVO Portal
    Given User access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,2"
    And Enter the required details for the Enquiry creation
    Then Save the new Enquiry
    Then user clicks on Compose Email button
    And User adds Additional Related to "Enquiry,T,3" as "Enquiry,H,4"
    And Click on save mail button

  #Then Logout from the SVO Portal
  #User should be able to view Reference number for an Enquiry with Qualified Opprotunity closed state
  @SVO @SVO_113 @SSEC-384 @SVOBatch
  Scenario: User should be able to view Reference number for an Enquiry with Qualified Opprotunity closed state
    And mark test script status in "ScriptResults,C,113"
    #Given Access SVO Portal
    Given User access SVO Portal
    When User Navigate to Enquiries
    And User Create New Enquiry with Record Type "Enquiry,A,4"
    And Enter Enquiry title "Enquiry,R,2"  for the Enquiry creation
    And Save the enquiry
    Then User selects Preferred Contact details from the Potential Matches coming after entering the Email Address "Enquiry,N,4"
    Then Log a call to that Enquiry with Client "Enquiry,H,4"
    Then From the Contacted Stage Mark Enquiry Status as Complete
    And user fills the required details like Product Offering "Enquiry,B,4" Enquiry Source "Enquiry,C,4" Originating Division "Enquiry,D,4" Region "Enquiry,E,4" Sales Area "Enquiry,F,4" Market "Enquiry,G,4" Brand "Enquiry,J,4" Model "Enquiry,K,4" and Retailer "Enquiry,L,4"
    Then Mark the Enquiry Status as Complete with Qualified-Opportunity state
    Then User Navigates to Opportunities link	with Enquiry Title "Enquiry,R,2"
    Then User Validates the Reference Number in the Details Section

  #Then Logout from the SVO Portal
  #User tries to Move the stage of an Opportunity from Estimate to Contracting without clearing Restricted party screening for Classic Bespoke Opportunity
  #Classic Bespoke Opportunity with Estimate Stage
  @SVO @SVO_114 @SSEC-385 @SVOBatch
  Scenario: User tries to Move the stage of an Opportunity from Estimate to Contracting without clearing Restricted party screening for Classic Bespoke Opportunity
    And mark test script status in "ScriptResults,C,114"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to SVO
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,3"
    And fill all mandatory fields of SVOBespoke opportunity like OpportunityName "Opportunity,A,3" Closedate "Opportunity,AI,2" Stage "Opportunity,R,12" ProductOffering "Opportunity,B,3" Region "Opportunity,E,2" AccountName "Opportunity,D,9" PreferredContact "Opportunity,H,9" Brand "Opportunity,I,2" Model "Opportunity,J,2"
    Then User unchecked the Restricted party screening "Opportunity,AR,2"
    And User click on the mark stage as complete button to complete the process
    Then Verify the error message to clear the Restricted party screening

  #Then Logout from SF_SVO Portal
  #User skips SAP Account details and try to close an opportunity
  #Classic Bespoke Opportunity with Order Placed Stage
  @SVO @SVO_115 @SSEC-386 @SVOBatch
  Scenario: User skips SAP Account details and try to close an opportunity
    And mark test script status in "ScriptResults,C,115"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to SVO
    Then User Navigate to Opportunity tab
    Then User searched and select "Opportunity,AC,2" opportunities from select list view
    And Open an opportunity with Record Type "Opportunity,R,7"
    Then User clears SAP Account details
    And User click on the mark stage as complete button to complete the process
    Then User selects stage as ClosedWon
    Then Verify the error message to close the opportunity

  #Then Logout from SF_SVO Portal
  #User skips the Opportunity currency and VAT Qualifying and tries to Move the stage from Estimate to Contracting
  #Classic Bespoke with Estimate stage
  @SVO @SVO_116 @SSEC-387 @SVOBatch
  Scenario: User skips the Opportunity currency and VAT Qualifying and tries to Move the stage from Estimate to Contracting
    And mark test script status in "ScriptResults,C,116"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,3"
    And fill all mandatory fields of SVOBespoke opportunity like OpportunityName "Opportunity,A,3" Closedate "Opportunity,AI,2" Stage "Opportunity,R,12" ProductOffering "Opportunity,B,3" Region "Opportunity,E,2" AccountName "Opportunity,D,9" PreferredContact "Opportunity,H,9" Brand "Opportunity,I,2" Model "Opportunity,J,2"
    Then User clears opportunity currency
    Then Verify the error message to fill Opportunity Currency
    Then User click on cancel button

  #Then Logout from SF_SVO Portal
  #User can creates a new Campaign and view Campaign details under Campaigns Menu
  #Super User
  @SVO @SVO_117 @SVOBatch
  Scenario: User can creates a new Campaign and view Campaign details under Campaigns Menu
    And mark test script status in "ScriptResults,C,117"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,3"
    And fill all mandatory fields of SVOBespoke opportunity like OpportunityName "Opportunity,A,3" Closedate "Opportunity,AI,2" Stage "Opportunity,R,12" ProductOffering "Opportunity,B,3" Region "Opportunity,E,2" AccountName "Opportunity,D,9" PreferredContact "Opportunity,H,9" Brand "Opportunity,I,2" Model "Opportunity,J,2"
    #Then User uncheck VAT Qualifying and try to move stage from Estimate to Contracting
    Then Verify the error message to fill Opportunity Currency
    Then User click on cancel button

  #Then Logout from SF_SVO Portal
  #User tries enter two digit number in production year section of an Opportunity
  @SVO @SVO_118 @SSEC-389 @SVOBatch
  Scenario: User tries enter two digit number in production year section of an Opportunity
    And mark test script status in "ScriptResults,C,118"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to SVO
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,3"
    And fill all mandatory fields of SVOBespoke opportunity like OpportunityName "Opportunity,A,3" Closedate "Opportunity,AI,2" Stage "Opportunity,R,12" ProductOffering "Opportunity,B,3" Region "Opportunity,E,2" AccountName "Opportunity,D,9" PreferredContact "Opportunity,H,9" Brand "Opportunity,I,2" Model "Opportunity,J,2"
    When User tries to enter invalid production year as "Opportunity,N,5"
    Then Verify the error message to fill Opportunity Currency
    And User click on cancel button

  #Then Logout from SF_SVO Portal
  #User able to Attach a file in compose email section of an Opportunity
  #User Role : Mike King
  #SVO Bespoke Opportunity with stage Design Brief & Quote
  @SVO @SVO_119 @SSEC-390 @SVOBatch
  Scenario: User able to Attach a file in compose email section of an Opportunity
    And mark test script status in "ScriptResults,C,119"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then set up login to the user role "User Role,B,17"
    Then User Navigate to Opportunity tab
    And User click on New button to create an opportunity
    And fill all mandatory fields of SVOBespoke Mike opportunity like OpportunityName "Opportunity,A,8" Closedate "Opportunity,AI,2" Stage "Opportunity,R,6" ProductOffering "Opportunity,B,2" Region "Opportunity,E,2" AccountName "Opportunity,D,9" PreferredContact "Opportunity,H,9" Brand "Opportunity,I,2" Model "Opportunity,J,2"
    Then Click on Compose email
    Then User attach a file in Compose email section

  #Then Logout from SF_SVO Portal
  #User able to Remove a file in compose email section of an Opportunity
  @SVO @SVO-120 @SSEC-391 @SVOBatch
  Scenario: User able to Remove a file in compose email section of an Opportunity
    And mark test script status in "ScriptResults,C,120"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    And Search for Opportunity with stage "Estimate" and open relevant Opportunity
    Then Click on Compose email
    Then User attaches file from different location

  #Then Logout from SF_SVO Portal
  #User able to retrieve the saved mail from draft of an Opportunity
  @SVO @SVO-121 @SSEC-392 @SVOBatch
  Scenario: User able to retrieve the saved mail from draft of an Opportunity
    And mark test script status in "ScriptResults,C,121"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    And Search for Opportunity with stage "Qualified" and open relevant Opportunity
    Then Click on Compose email
    Then edit the required information as Additional to "New Enquiry,U,2" Subject "New Enquiry,X,2" Body "New Enquiry,Y,2"
    And Click on save mail
    Then Click on Compose email
    Then edit the Additional to "New Enquiry,U,2"
    Then Click on drafts and retrive draft with Subject "New Enquiry,X,2"
    And Click on sent mail

  #Then Logout from SF_SVO Portal
  #User generates an S-Doc for closed-lost Opportunity
  @SVO @SVO-122 @SSEC-393 @SVOBatch
  Scenario: User generates an S-Doc for closed-lost Opportunity
    And mark test script status in "ScriptResults,C,122"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    And Search for Opportunity with stage "Closed Lost" and open relevant Opportunity
    Then Click on S-Docs button
    When user selects a required file and clicks on next step

  #Then Logout from SF_SVO Portal
  #User tries to delete Closed Won Opportunity
  @SVO @SVO-123 @SSEC-394 @SVOBatch @JIRADemo @123
  Scenario: User tries to delete Closed Won Opportunity
    And mark test script status in "ScriptResults,C,123"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    #Then User Navigate to Opportunity tab
    And Search for Opportunity with stage "Closed Won" and open relevant Opportunity
    Then Click on Delete button
    And mark this script status in "ScriptResults,C,123"

  #Then Logout from SF_SVO Portal
  #User selects Order status of an Opportunity other than retailed
  @SVO @SVO-124 @SSEC-395 @SVOBatch @124 @ForBatch
  Scenario: User selects Order status of an Opportunity other than retailed
    And mark test script status in "ScriptResults,C,124"
    #Given Access SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,4"
    Then User enters mandatory fields for an Opportunity like Stage "Opportunity,R,4" Product Offering "Opportunity,B,3" Region "Opportunity,E,3" Sales Area "Opportunity,F,3" Market "Opportunity,G,3" Preferred Contact "Opportunity,H,6" Account Name "Opportunity,D,5" Source "Opportunity,C,3" Brand "Opportunity,I,3" Model "Opportunity,J,3"
    And User click on the mark stage as complete button to complete the process
    And User enters Pricing Details
    And User click on the mark stage as complete button to complete the process
    And User click on the mark stage as complete button to complete the process
    #Then User enters Vehicle details
    And User click on the mark stage as complete button to complete the process
    Then navigate to Orders link and open the order
    Then Open the work order
    #Then Enter all the required details in work order
    Then Navigate to the order and mark status as Cancelled by customer
    Then Navigate to Opportunity and verify that the status is changed to Closed Lost

  #Then Logout from SF_SVO Portal
  #User tries to send an email without entering mandatory fields of an Opportunity
  @SVO @SVO-125 @SSEC-396 @SVOBatch
  Scenario: User tries to send an email without entering mandatory fields of an Opportunity
    And mark test script status in "ScriptResults,C,125"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    And Search for Opportunity with stage "Estimate" and open relevant Opportunity
    Then Click on Compose email
    Then Click on Send button and verify the error

  #Then Logout from SF_SVO Portal
  #User can edit auto populated field details in Compose email list of an Opportunity
  @SVO @SVO-126 @SSEC-397 @SVOBatch
  Scenario: User can edit auto populated field details in Compose email list of an Opportunity
    And mark test script status in "ScriptResults,C,126"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    And Search for Opportunity with stage "Estimate" and open relevant Opportunity
    Then Click on Compose email
    Then Edit auto populated fields like Related to "New Enquiry,T,2" and Importance "New Enquiry,Z,2" in Compose email page
    And Click on save mail

  #Then Logout from SF_SVO Portal
  #User tries to edit From section of an email in compose email section
  @SVO @SVO-127 @SSEC-398 @SVOBatch
  Scenario: User tries to edit From section of an email in compose email section
    And mark test script status in "ScriptResults,C,127"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    And Search for Opportunity with stage "Estimate" and open relevant Opportunity
    Then Click on Compose email
    Then Verify that the user cannot edit From and Business unit fields in Compose email page

  #Then Logout from SF_SVO Portal
  #User selects To, CC and BCC fields of mail from contact list search in compose email section of an opportunity
  @SVO @SVO-128 @SSEC-399 @SVOBatch
  Scenario: User selects To, CC and BCC fields of mail from contact list search in compose email section of an opportunity
    And mark test script status in "ScriptResults,C,128"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to SVO
    Then User Navigate to Opportunity tab
    And Open an opportunity which is in Estimate stage
    Then User selects TO BCC and CC section of an email

  #Then Logout from SF_SVO Portal
  #User creates an invoice for an order in Opportunity section
  @SVO @SVO-129 @SSEC-400 @SVOBatch @Sanity
  Scenario: User creates an invoice for an order in Opportunity section
    And mark test script status in "ScriptResults,C,129"
    #Given Access SVO Portal
    Given User access SVO Portal
    Then User Navigate to SVO
    Then User Navigate to Opportunity tab
    Then Open an opportunity which is in OrderPlaced stage
    Then User selects an order which is linked to selected Opportunity
    Then User selects deposit type invoice
    Then User creates new invoice by entering mandatory fields

  #Then Logout from SF_SVO Portal
  #User tries to change the Record type of an invoice
  @SVO @SVO-130 @SSEC-401 @SVOBatch
  Scenario: User tries to change the Record type of an invoice
    And mark test script status in "ScriptResults,C,130"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to SVO
    Then User Navigate to Opportunity tab
    Then Open an opportunity which is in OrderPlaced stage
    Then User selects an order which is linked to selected Opportunity
    Then User selects an Invoice linked to Order
    Then User Change the Record type of an Invoice

  #Then Logout from SF_SVO Portal
  #User created an S-sign Envelop for Order placed Opportunity
  @SVO @SVO-131 @Error @SSEC-402 @SVOBatch @Sanity
  Scenario: User created an S-sign Envelop for Order placed Opportunity
    And mark test script status in "ScriptResults,C,131"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,5"
    And Enter details of New Opportunity like Opportunity name "Opportunity,L,5" Product Offering "Opportunity,B,5" Account Name "Opportunity,D,5" Region "Opportunity,E,5" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,5" and Model "Opportunity,J,5"

  #Then Save the created Opportunity
  #User tries to change the Build Start details of an Work order in Opportunity section
  @SVO @SVO-132 @Admin @SSEC-403 @SVOBatch
  Scenario: User tries to change the Build Start details of an Work order in Opportunity section
    And mark test script status in "ScriptResults,C,132"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    And Search for Opportunity with stage "Order Placed" and open relevant Opportunity
    And User navigates to Orders section from Related links of Opportunity
    Then Navigate to Work Order from Order window
    Then Edit Build start dates and Validate start slippage days

  #Then Logout from SF_SVO Portal
  #User tries to add ABS-Forecast date of an order before Build start date
  @SVO @SVO-133 @Admin @SSEC-404 @SVOBatch
  Scenario: User tries to add ABS-Forecast date of an order before Build start date
    And mark test script status in "ScriptResults,C,133"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    And Search for Opportunity with stage "Order Placed" and open relevant Opportunity
    And User navigates to Orders section from Related links of Opportunity
    Then Navigate to Work Order from Order window
    Then Set ABS-forecast date prior to Build Start date and validate error

  #Then Logout from SF_SVO Portal
  #User tries to enter non-sequencial forecast dates for sequential build stages
  @SVO @SVO-134 @SSEC-405 @Admin @SVOBatch
  Scenario: User tries to enter non-sequencial forecast dates for sequential build stages
    And mark test script status in "ScriptResults,C,134"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    And Search for Opportunity with stage "Order Placed" and open relevant Opportunity
    And User navigates to Orders section from Related links of Opportunity
    Then Navigate to Work Order from Order window
    Then Set Strip_Parts Forecast and Paint forecast date in a non sequential manner and validate error

  #Then Logout from SF_SVO Portal
  #User tries to Move the status of invoice payment from Planned request to Payment request Paid
  @SVO @SVO-135 @SSEC-406 @SVOBatch
  Scenario: User tries to Move the status of invoice payment from Planned request to Payment request sent
    And mark test script status in "ScriptResults,C,135"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    And Search for Opportunity with stage "Order Placed" and open relevant Opportunity
    Then User selects an order which is linked to selected Opportunity
    Then User selects deposit type invoice
    Then User creates new invoice by entering mandatory fields
    Then User navigates to Invoice which is in Planned Request Status
    And User edits payment date and value and validates the invoice status is moved to Payment request sent

  #Then Logout from SF_SVO Portal
  #User can view and edit details of calendar under Booker25 Menu
  @SVO @SVO-136 @SSEC-407 @SVOBatch
  Scenario: User tries to Move the status of invoice payment from Payment request sent  to Payment request Paid
    And mark test script status in "ScriptResults,C,136"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    And Search for Opportunity with stage "Order Placed" and open relevant Opportunity
    Then User selects an order which is linked to selected Opportunity
    Then User selects deposit type invoice
    Then User creates new invoice by entering mandatory fields
    Then User navigates to Invoice which is in Planned Request Status
    And User edits paid ammount details and validates the status is changed to Payment Request Paid

  #Then Logout from SF_SVO Portal
  #User tries to mark Strip & parts /BIW build stage as complete for an work order
  @SVO @SVO-137 @SSEC-408 @SVOBatch
  Scenario: User tries to mark Strip & parts /BIW build stage as complete for an work order
    And mark test script status in "ScriptResults,C,137"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,4"
    And Enter Required details of New Opportunity like Product Offering "Opportunity,B,4" Source "Opportunity,C,4" Account Name "Opportunity,D,4" Region "Opportunity,E,4" Preferred Contact "Opportunity,H,4" Brand "Opportunity,I,4" and Model "Opportunity,J,4"
    Then Save the created Opportunity
    Then Change the opportunity status to Order Placed
    Then navigate to Orders link and open the order
    Then Open the work order
    Then Mark Strip & parts /BIW build stage as complete in work order and verify the other details

  #Then Logout from SF_SVO Portal
  #User fills the Handover details of vehicle in build status of an Order without selecting vehicle
  @SVO @SVO-138 @SSEC-409 @SVOBatch
  Scenario: User fills the Handover details of vehicle in build status of an Order without selecting vehicle
    And mark test script status in "ScriptResults,C,138"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,4"
    And Enter Required details of New Opportunity like Product Offering "Opportunity,B,4" Source "Opportunity,C,4" Account Name "Opportunity,D,4" Region "Opportunity,E,4" Preferred Contact "Opportunity,H,4" Brand "Opportunity,I,4" and Model "Opportunity,J,4"
    Then Save the created Opportunity
    Then Change the opportunity status to Order Placed
    Then navigate to Orders link and open the order
    Then Navigate to Handover details section and enter the required details
    Then User should be able to Mark Status as complete for the order

  #Then Logout from SF_SVO Portal
  #User tries to move the order status from Accepted sales to Outcome Retailed without filling Handover details
  @SVO @SVO-139 @SSEC-410 @SVOBatch
  Scenario: User tries to move the order status from Accepted sales to Outcome Retailed without filling Handover details
    And mark test script status in "ScriptResults,C,139"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to SVO
    Then User Navigate to Opportunity tab
    Then Open an opportunity which is in OrderPlaced stage
    Then User selects an order which is linked to selected Opportunity
    Then User clears HandOver details
    And User click on the mark status as complete button of an Order
    Then Verify the error message to clear the Restricted party screening

  #Then Logout from SF_SVO Portal
  #User tries to retrieve the mail from drafts after entering email body and subject
  @SVO @SVO-140 @SSEC-411 @SVOBatch
  Scenario: User tries to retrieve the mail from drafts after entering email body and subject
    And mark test script status in "ScriptResults,C,140"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    And Search for Opportunity with stage "Qualified" and open relevant Opportunity
    Then Click on Compose email
    Then edit the required information as Additional to "New Enquiry,U,2" Subject "New Enquiry,X,2" Body "New Enquiry,Y,2"
    And Click on save mail
    Then Click on Compose email
    Then edit the Additional to "New Enquiry,U,2"
    Then Click on drafts and retrive draft with Subject "New Enquiry,X,2"

  #Then Logout from SF_SVO Portal
  #User Fills the Payment details for an Invoice of an Order
  @SVO @SVO-141 @SSEC-412 @SVOBatch
  Scenario: User Fills the Payment details for an Invoice of an Order
    And mark test script status in "ScriptResults,C,141"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to SVO
    Then User Navigate to Opportunity tab
    Then Open an opportunity which is in OrderPlaced stage
    Then User selects an order which is linked to selected Opportunity
    Then User selects an Invoice linked to Order
    Then User fills Payment details of an Invoice
    Then Verify the payment due date of an Invoice

  #Then Logout from SF_SVO Portal
  #User tries to Edit the close date of closed won Opportunity
  @SVO @SVO-142 @SSEC-413 @SVOBatch
  Scenario: User tries to Edit the close date of closed won Opportunity
    And mark test script status in "ScriptResults,C,142"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to SVO
    Then User Navigate to Opportunity tab
    Then Open an opportunity which is in OrderPlaced stage
    Then User edits the Close date of an Opportunity
    And User click on the mark stage as complete button to complete the process
    Then User mark status as closed
    Then Verify the error message to clear the Restricted party screening

  #Then Logout from SF_SVO Portal
  #User tries to add pinewood job card under reference numbers of an order
  @SVO @SVO-143 @SSEC-414 @SVOBatch
  Scenario: User tries to add pinewood job card under reference numbers of an order
    And mark test script status in "ScriptResults,C,143"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to SVO
    Then User Navigate to Opportunity tab
    Then Open an opportunity which is in OrderPlaced stage
    Then User selects an order which is linked to selected Opportunity
    Then User enters pinewood job card number

  #Then Logout from SF_SVO Portal
  #User Creates a new Quote
  @SVO @SVO-144 @SSEC-415 @SVOBatch
  Scenario: User Creates a new Quote
    And mark test script status in "ScriptResults,C,144"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,5"
    And Enter details of New Opportunity like Opportunity name "Opportunity,L,5" Product Offering "Opportunity,B,5" Account Name "Opportunity,D,5" Region "Opportunity,E,5" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,5" and Model "Opportunity,J,5"
    Then Save the created Opportunity
    Then Navigate to Quotes
    Then Create a new Quote "Test Quote" and save

  #Then Logout from SF_SVO Portal
  #User is able to edit the Quote
  @SVO @SVO-145 @SSEC-416 @SVOBatch
  Scenario: User is able to edit the Quote
    And mark test script status in "ScriptResults,C,145"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,5"
    And Enter details of New Opportunity like Opportunity name "Opportunity,L,5" Product Offering "Opportunity,B,5" Account Name "Opportunity,D,5" Region "Opportunity,E,5" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,5" and Model "Opportunity,J,5"
    Then Save the created Opportunity
    Then Navigate to Quotes
    Then Create a new Quote "Test Quote" and save

  #Then Logout from SF_SVO Portal
  #User is able to Delete the Quotes
  @SVO @SVO-146 @SSEC-417 @SVOBatch
  Scenario: User is able to Delete the Quotes
    And mark test script status in "ScriptResults,C,146"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,5"
    And Enter details of New Opportunity like Opportunity name "Opportunity,L,5" Product Offering "Opportunity,B,5" Account Name "Opportunity,D,5" Region "Opportunity,E,5" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,5" and Model "Opportunity,J,5"
    Then Save the created Opportunity
    Then Navigate to Quotes
    Then Create a new Quote "Test Quote" and save
    Then Delete the Quote created

  #Then Logout from SF_SVO Portal
  #At design brief stage user skips updating the design brief in related quick links
  @SVO @SVO-147 @SSEC-418 @SVOBatch @ForthBatch
  Scenario: At design brief stage user skips updating the design brief in related quick links
    And mark test script status in "ScriptResults,C,147"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to SVO
    Then User Navigate to Opportunity tab
    Then User selects All Bespoke opportunities from select list view
    Then Open an opportunity which is in OrderPlaced stage
    Then Navigate to Quote section
    Then User created new Bespoke Design Brief Quote
    Then User clears Design brief section
    And User click on the mark status as complete button of an Order
    Then Verify the error message to clear the Restricted party screening

  #Then Logout from SF_SVO Portal
  #User is able to check and un-check the Required design team
  @SVO @SVO_148 @SSEC-419 @SVOBatch
  Scenario: User is able to check and un-check the Required design team
    And mark test script status in "ScriptResults,C,148"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to SVO
    Then User Navigate to Opportunity tab
    Then User searched and select "Opportunity,AC,3" opportunities from select list view
    And Open an opportunity with Record Type "Opportunity,R,7"
    Then Navigate to Quote section
    Then Create a new Quote "Opportunity,A,5" and save
    Then User navigate to Design brief section
    Then User uncheck the design team

  #Then Logout from SF_SVO Portal
  #User is not able to change the ops Approval Status
  @SVO @SVO-149 @SSEC-420 @SVOBatch
  Scenario: User is not able to change the ops Approval Status
    And mark test script status in "ScriptResults,C,149"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    Then Click on New button
    And Enter details of New SVO Bespoke Opportunity like Opportunity name "Opportunity,L,5" Product Offering "Opportunity,B,5" Account Name "Opportunity,D,5" Region "Opportunity,E,5" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,5" and Model "Opportunity,J,5"
    Then Save the created Opportunity
    Then Navigate to Quotes
    Then Create a new SVO Quote "Test Quote" and save
    Then Verify the OPS Approval Status field is not editable

  #Then Logout from SF_SVO Portal
  #User is able to fill the Exterior design with some Random text
  @SVO @SVO-150 @SSEC-421 @SVOBatch
  Scenario: User is able to fill the Exterior design with some Random text
    And mark test script status in "ScriptResults,C,150"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,5"
    And Enter details of New Opportunity like Opportunity name "Opportunity,L,5" Product Offering "Opportunity,B,5" Account Name "Opportunity,D,5" Region "Opportunity,E,5" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,5" and Model "Opportunity,J,5"
    Then Save the created Opportunity
    Then Navigate to Quotes
    Then Create a new Quote "Test Quote" and save
    Then Navigate and open Design briefs
    Then Enter the Exterior design details and save

  #Then Logout from SF_SVO Portal
  #User is able to update the design brief in related Quick links and displays a second pop up for submit
  @SVO @SVO-151 @SSEC-422 @SVOBatch
  Scenario: User is able to update the design brief in related Quick links and displays a second pop up for submit
    And mark test script status in "ScriptResults,C,151"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,5"
    And Enter details of New Opportunity like Opportunity name "Opportunity,L,5" Product Offering "Opportunity,B,5" Account Name "Opportunity,D,5" Region "Opportunity,E,5" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,5" and Model "Opportunity,J,5"
    Then Save the created Opportunity
    Then Navigate to Quotes
    Then Create a new Quote "Test Quote" and save
    Then Navigate and open Design briefs
    Then Click on Submit and verify the error

  #Then Logout from SF_SVO Portal
  #User is able to see Approval history for the quote
  @SVO @SVO_152 @SVOBatch
  Scenario: User is able to see Approval history for the quote
    And mark test script status in "ScriptResults,C,152"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,5"
    And Enter details of New SVO Bespoke Opportunity like Opportunity name "Opportunity,L,5" Product Offering "Opportunity,B,5" Account Name "Opportunity,D,5" Region "Opportunity,E,5" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,5" and Model "Opportunity,J,5"
    Then Save the created Opportunity
    Then Navigate to Quotes
    Then Create a new Quote "Test Quote" and save
    Then Navigate and open Design briefs
    Then Move the design brief stage from Draft to Submitted
    Then Verfify the Approval History of Design brief

  #Then Logout from SF_SVO Portal
  #User cannot delete Closed Won Opportunity
  @SVO @SVO_153 @SVOBatch
  Scenario: User should verify the Approval Status as Approved
    And mark test script status in "ScriptResults,C,153"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,5"
    And Enter details of New SVO Bespoke Opportunity like Opportunity name "Opportunity,L,5" Product Offering "Opportunity,B,5" Account Name "Opportunity,D,5" Region "Opportunity,E,5" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,5" and Model "Opportunity,J,5"
    Then Save the created Opportunity
    Then Navigate to Quotes
    Then Create a new Quote "Test Quote" and save
    Then Navigate and open Design briefs
    Then Move the design brief stage from Draft to Submitted
    Then Verfify the Approval History of Design brief
    And Approve recently created design brief
    Then Validate that the approval status as Approved

  #Then Logout from SF_SVO Portal
  #User Skips the pricing Details and price stage
  @SVO @SVO_154 @SVOBatch
  Scenario: User Skips the pricing Details and price stage
    And mark test script status in "ScriptResults,C,154"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,5"
    And Enter details of New Opportunity like Opportunity name "Opportunity,L,2" Product Offering "Opportunity,B,6" Account Name "Opportunity,D,2" Region "Opportunity,E,2" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,4" and Model "Opportunity,J,4"
    Then Save the created Opportunity
    Then Navigate to Quotes
    Then Create a new Quote "Test Quote" and save
    Then Navigate and open Design briefs
    Then Move the design brief stage from Draft to Submitted
    Then Return Render pack of design brief
    Then Navigate back to Quote section
    Then User should be able to Mark Status as complete for the order
    Then Verify the error message to clear the Restricted party screening

  #Then Logout from SF_SVO Portal
  #User able to fill the price details and proceed further
  @SVO @SVO_155 @SVOBatch @ForthBatch @155 @ForBatch
  Scenario: User able to fill the price details and proceed further
    And mark test script status in "ScriptResults,C,155"
    #Given Access SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    And User Creates New Opportunity with "Opportunity,A,8" Record Type
    Then User enters mandatory fields for an Opportunity like Stage "Opportunity,R,4" Product Offering "Opportunity,B,8" Region "Opportunity,E,8" Sales Area "Opportunity,F,8" Market "Opportunity,G,8" Preferred Contact "Opportunity,H,8" Account Name "Opportunity,D,5" Source "Opportunity,C,8" Brand "Opportunity,I,8" Model "Opportunity,J,8"
    Then Navigate to Quotes
    Then Create a new Quote "Opportunity,D,6" and save
    Then Navigate and open Design briefs
    Then Move the design brief stage from Draft to Submitted
    Then Return Render pack of design brief
    Then Navigate back to Quote section
    Then fill the price details of Quote like CostPrice "Opportunity,X,2" ListPrice "Opportunity,Y,2" Contribution "Opportunity,Z,2"  RetailPrice "Opportunity,AA,2" Revenue"Opportunity,AB,2"
    Then User should be able to Mark Status as complete for the order
    Then User should be able to Mark Status as complete for the order
    Then User should be able to Mark Status as complete for the order
    Then User enter Outcome fields like Outcome "Opportunity,W,2" OutcomeStage "Opportunity,W,2"

  #Then Logout from SF_SVO Portal
  #User able to update the Proposal sent stage
  @SVO @SVO_156 @SVOBatch
  Scenario: User able to update the Proposal sent stage
    And mark test script status in "ScriptResults,C,156"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,5"
    And Enter details of New SVO Bespoke Opportunity like Opportunity name "Opportunity,L,5" Product Offering "Opportunity,B,5" Account Name "Opportunity,D,5" Region "Opportunity,E,5" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,5" and Model "Opportunity,J,5"
    Then Save the created Opportunity
    Then Navigate to Quotes
    Then Create a new Quote "Opportunity,A,5" and save
    Then Navigate and open Design briefs
    Then Move the design brief stage from Draft to Submitted
    Then Return Render pack of design brief
    Then Navigate back to Quote section
    Then fill the price details of Quote like CostPrice "Opportunity,X,2" ListPrice "Opportunity,X,2" Contribution "Opportunity,X,2"  RetailPrice "Opportunity,X,2" Revenue"Opportunity,X,2"
    Then User should be able to Mark Status as complete for the order
    Then User should be able to Mark Status as complete for the order
    Then User should be able to Mark Status as complete for the order
    Then User enter Outcome fields like Outcome "Opportunity,W,2" OutcomeStage "Opportunity,W,2"

  #Then Logout from SF_SVO Portal
  #User able to send the SVO retailer proposal
  @SVO @SVO_157 @SVOBatch
  Scenario: User able to send the SVO retailer proposal
    And mark test script status in "ScriptResults,C,157"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,5"
    And Enter details of New SVO Bespoke Opportunity like Opportunity name "Opportunity,L,5" Product Offering "Opportunity,B,5" Account Name "Opportunity,D,5" Region "Opportunity,E,5" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,5" and Model "Opportunity,J,5"
    Then Save the created Opportunity
    Then Navigate to Quotes
    Then Create a new Quote "Opportunity,A,5" and save
    Then Navigate and open Design briefs
    Then Move the design brief stage from Draft to Submitted
    Then Return Render pack of design brief
    Then Navigate back to Quote section
    Then fill the price details of Quote like CostPrice "Opportunity,X,2" ListPrice "Opportunity,X,2" Contribution "Opportunity,X,2"  RetailPrice "Opportunity,X,2" Revenue"Opportunity,X,2"
    Then User should be able to Mark Status as complete for the order
    Then User send Quote pack
    Then Verify the error message displayed

  #Then Logout from SF_SVO Portal
  #User tries to change the Status of the Quote while editing the details
  @SVO @SVO-158 @SSEC-429 @SVOBatch @Sanity
  Scenario: User tries to change the Status of the Quote while editing the details
    And mark test script status in "ScriptResults,C,158"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,5"
    And Enter details of New Opportunity like Opportunity name "Opportunity,L,5" Product Offering "Opportunity,B,5" Account Name "Opportunity,D,5" Region "Opportunity,E,5" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,5" and Model "Opportunity,J,5"
    Then Save the created Opportunity
    Then Navigate to Quotes
    Then Create a new Quote "Test Quote" and save
    Then User edit the status as "Opportunity,S,8" of saved Quote
    Then Verify the error message displayed to save quote

  #Then Logout from SF_SVO Portal
  #User can update/Change the Outcome Details
  #SVO Private office user
  @SVO @SVO_159 @SVOBatch
  Scenario: User can update/Change the Outcome Details
    And mark test script status in "ScriptResults,C,159"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity for SVO Private Office
    And Enter details of New Opportunity for Private Office Opportunity name "Opportunity,L,2" Product Offering "Opportunity,B,6" Account Name "Opportunity,D,5" Region "Opportunity,E,2" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,2" and Model "Opportunity,J,2"
    Then Save the created Opportunity
    Then Navigate to Quotes
    Then Create a new Quote "Test Quote" for Private Office and save
    Then Mark Status as Complete
    Then Mark Status as Complete
    Then Mark the Quote status as Outcome accepted
    Then Click on Edit and verify the outcome details cannot be changed

  #Then Logout from SF_SVO Portal
  #User who don't have licence, cannot complete 'E-sign documents in person' Activity
  @SVO @SVO_160 @SVOBatch
  Scenario: User who don't have licence, cannot complete 'E-sign documents in person' Activity
    bAnd mark test script status in "ScriptResults,C,160"

    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,8"
    And Enter details of New Opportunity like Opportunity name "Opportunity,L,2" Product Offering "Opportunity,B,2" Account Name "Opportunity,D,5" Region "Opportunity,E,2" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,2" and Model "Opportunity,J,2"
    Then Save the created Opportunity
    Then Navigate to Quotes
    Then Create a new Quote "Test Quote" and save
    Then Navigate and open Design briefs
    Then Click on Submit
    Then Click on Return Render pack and navigate back to Quote
    Then Mark the Quote stage as Price
    Then Click on Send Quote Pack
    Then Select a document and move to next step to E-Sign documents in person and verify the error

  #Then Logout from SF_SVO Portal
  #User is able to add Orders to the Quotes
  @SVO @SVO-161 @SVOBatch
  Scenario: User is able to add Orders to the Quotes
    And mark test script status in "ScriptResults,C,161"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,6"
    And Enter details of New Opportunity like Opportunity name "Opportunity,L,6" Product Offering "Opportunity,B,6" Account Name "Opportunity,D,6" Region "Opportunity,E,6" Preferred Contact "Opportunity,H,6" Brand "Opportunity,I,6" and Model "Opportunity,J,6"
    Then Save the created Opportunity
    Then Navigate to Quotes
    Then Create a new Quote "Test Quote" and save
    Then Navigate to Orders under Quotes and click open
    Then Create a new order with Opportunity "Opportunity,W,2"
    Then Save the order created

  #Then Logout from SF_SVO Portal
  #User is able to Edit and Delete the orders in Quote section
  @SVO @SVO_162 @SVOBatch @Sanity
  Scenario: User is able to Edit and Delete the orders in Quote section
    And mark test script status in "ScriptResults,C,162"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,8"
    And Enter details of New Opportunity like Opportunity name "Opportunity,L,2" Product Offering "Opportunity,B,2" Account Name "Opportunity,D,5" Region "Opportunity,E,2" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,2" and Model "Opportunity,J,2"
    Then Save the created Opportunity
    Then Navigate to Quotes
    Then Create a new Quote "Test Quote" and save
    Then Delete the Quote created

  #Then Logout from SF_SVO Portal
  #Check in files section for approval file
  @SVO @SVO_163 @SVOBatch
  Scenario: Check in files section for approval file
    And mark test script status in "ScriptResults,C,163"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,8"
    And Enter details of New Opportunity like Opportunity name "Opportunity,L,2" Product Offering "Opportunity,B,2" Account Name "Opportunity,D,5" Region "Opportunity,E,2" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,2" and Model "Opportunity,J,2"
    Then Save the created Opportunity
    Then Navigate to Quotes
    Then Create a new Quote "Test Quote" and save
    Then Navigate to Files under Quotes and click open
    Then Add a new file and save

  #Then Logout from SF_SVO Portal
  #User tries to navigate back to the completed stages at outcome stage
  @SVO @SVO-164 @SVOBatch
  Scenario: User tries to navigate back to the completed stages at outcome stage
    And mark test script status in "ScriptResults,C,164"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,6"
    And Enter details of New Opportunity like Opportunity name "Opportunity,L,6" Product Offering "Opportunity,B,6" Account Name "Opportunity,D,6" Region "Opportunity,E,6" Preferred Contact "Opportunity,H,6" Brand "Opportunity,I,6" and Model "Opportunity,J,6"
    Then Save the created Opportunity
    Then Navigate to Quotes
    Then Create a new Quote "Test Quote" and save
    Then Navigate and open Design briefs
    Then Click on Submit and navigate back to Quote
    Then Mark the Quote status as Outcome accepted
    Then Mark the Quote status as Proposal Sent and verify user cannot navigate back stages

  #Then Logout from SF_SVO Portal
  #User skip to select the SVO proposal Document at 'Create S-Docs' stage
  @SVO @SVO_165 @SVOBatch
  Scenario: Validate the error message when user clicks on 'Add Files' without selecting any document in 'Send Sales Agreement' page
    And mark test script status in "ScriptResults,C,165"
    Given Access the SVO Portal
    #Given User access SF-SVO Portal
    When User Navigate to Opportunity
    And User Creates New Opportunity with "Opportunity,A,8" Record Type
    Then User enters mandatory fields for an Opportunity like Stage "Opportunity,R,4" Product Offering "Opportunity,B,8" Region "Opportunity,E,8" Sales Area "Opportunity,F,8" Market "Opportunity,G,8" Preferred Contact "Opportunity,H,10" Account Name "Opportunity,D,10" Source "Opportunity,C,3" Brand "Opportunity,I,2" Model "Opportunity,J,2"
    And User click on the mark stage as complete button to complete the process
    And User click on the mark stage as complete button to complete the process
    Then Navigate to Quotes
    Then Create a new Quote "Opportunity,H,10" and save
    Then Navigate and open Design briefs
    Then Move the design brief stage from Draft to Submitted
    Then Return Render pack of design brief
    Then Navigate back to Quote section
    Then fill the price details of Quote like CostPrice "Opportunity,X,2" ListPrice "Opportunity,Y,2" Contribution "Opportunity,Z,2"  RetailPrice "Opportunity,AA,2" Revenue"Opportunity,AB,2"
    And User click on the mark stage as complete button to complete the process
    Then User clicks on Send Sales Agreement button
    And User verifies that an error occurred when user removes added file

  #Then Logout from SF_SVO Portal
  #User cannot see any KMI created for non-closed enquiry
  @SVO @SVO_166 @SSEC-437 @SVOBatch
  Scenario: User cannot see any KMI created for non-closed enquiry
    And mark test script status in "ScriptResults,C,166"
    #Given Access SVO Portal
    Given User access SVO Portal
    When User Navigate to Enquiries
    And User opens an enquiry with "Contacted" stage
    And Open KMI from related links and verify there are no records available

  #Then Logout from the SVO Portal
  #User cannot see any KMI if an enquiry is closed with Quailified-Opportunity
  @SVO @SVO_167 @SSEC-438 @SVOBatch @Sanity
  Scenario: User cannot see any KMI if an enquiry is closed with Quailified-Opportunity
    And mark test script status in "ScriptResults,C,167"
    #Given Access SVO Portal
    Given User access SVO Portal
    When User Navigate to Enquiries
    And User opens an enquiry with "Closed" stage
    And Open KMI from related links and verify there are no records available

  #Then Logout from the SVO Portal
  #User can enter Source KMI details while creating new enquiry
  @SVO @SVO_168 @SSEC-439 @SVOBatch
  Scenario: User can enter Source KMI details while creating new enquiry
    And mark test script status in "ScriptResults,C,168"
    #Given Access SVO Portal
    Given User access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,2"
    Then Create a new enquiry with title "Enquiry,R,2" source KMI "Enquiry,AB,2"
    Then Save the new Enquiry

  #Then Logout from the SVO Portal
  #User is able to create a new KMI under KMI section before closing an enquiry
  @SVO @SVO_169 @SSEC-440 @SVOBatch
  Scenario: User is able to create a new KMI under KMI section before closing an enquiry
    And mark test script status in "ScriptResults,C,169"
    #Given Access SVO Portal
    Given User access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,12"
    And Enter the required details for the Enquiry creation
    Then Save the new Enquiry
    When user clicks on Compose Email button
    Then edit the required information as to "Enquiry,S,2" Related to "Enquiry,T,2" Additional to "Enquiry,U,2" CC "Enquiry,V,2" BCC "Enquiry,W,2" Subject "Enquiry,X,2" Body "Enquiry,Y,2" and Importance "Enquiry,Z,2"
    And Click on save mail button

  #Then Logout from the SVO Portal
  #User is able to edit the created KMI from KMI Record Section
  @SVO @SVO_170 @SSEC-441 @SVOBatch
  Scenario: User is able to edit the created KMI from KMI Record Section
    And mark test script status in "ScriptResults,C,170"
    #Given Access SVO Portal
    Given User access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,15"
    And Enter the required details for the Enquiry creation
    Then Save the new Enquiry
    Then Navigate to KMI section
    Then User created New KMI with fields like Contact "Enquiry,AA,2" ProductOffering "Enquiry,B,4" Brand "Enquiry,J,2" Model "Enquiry,K,2"
    Then User changes the Product offering of KMI to "Enquiry,B,5"
    Then User save the KMI Record

  #Then Logout from the SVO Portal
  #User is able to change Owner of the KMI record from KMI Record Section
  @SVO @SVO_171 @SSEC-442 @SVOBatch
  Scenario: User is able to change Owner of the KMI record from KMI Record Section
    And mark test script status in "ScriptResults,C,171"
    #Given Access SVO Portal
    Given User access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,15"
    And Enter the required details for the Enquiry creation
    Then Save the new Enquiry
    Then Navigate to KMI section
    Then User created New KMI with fields like Contact "Enquiry,AA,2" ProductOffering "Enquiry,B,4" Brand "Enquiry,J,2" Model "Enquiry,K,2"
    Then User change the owner of KMI to "Enquiry,AA,2"

  #Then Logout from the SVO Portal
  #User is able to delete the KMI records from any enquiry
  @SVO @SVO_172 @SSEC-443 @SVOBatch @Sanity
  Scenario: User is able to delete the KMI records from any enquiry
    And mark test script status in "ScriptResults,C,172"
    #Given Access SVO Portal
    Given User access SVO Portal
    When User Navigate to Enquiries
    And User opens an enquiry with "Closed" stage
    And Open KMI from related links and verify there are no records available
    Then Click on New button to create a new KMI
    Then Enter the fields contact "Enquiry,I,2" product offering "Enquiry,B,2" brand "Enquiry,J,2" model "Enquiry,K,2"
    Then Save the KMI created
    Then Delete the KMI from the list

  #Then Logout from the SVO Portal
  #User is able to create an Enquiries from KMI Record Section
  @SVO @SVO_173 @SVOBatch
  Scenario: User is able to create an Enquiries from KMI Record Section
    And mark test script status in "ScriptResults,C,173"
    #Given Access SVO Portal
    Given User access SVO Portal
    When User Navigate to Enquiries
    And User opens an enquiry with "Closed" stage
    And Open KMI from related links and verify there are no records available
    Then Click on New button to create a new KMI
    Then Enter the fields contact "Enquiry,I,2" product offering "Enquiry,B,2" brand "Enquiry,J,2" model "Enquiry,K,2"
    Then Save the KMI created
    Then Open the KMI created
    Then Navigate to Related tab
    Then Create a new enquiry of type "Enquiry,A,2" and title "Enquiry,R,2"
    Then Save the new Enquiry

  #Then Logout from the SVO Portal
  #User cannot be able to see Source KMI field for SVO Bespoke and SVO Private Office Enquiry
  @SVO @SVO_174 @SVOBatch
  Scenario: User cannot be able to see Source KMI field for SVO Bespoke and SVO Private Office Enquiry
    And mark test script status in "ScriptResults,C,174"
    #Given Access SVO Portal
    Given User access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,2"
    Then Create a new enquiry with title "Enquiry,R,2" source KMI "Enquiry,AB,2"
    Then Save the new Enquiry
    Then Click on Closed state and mark as current enquiry status
    Then Click on Closed state dropdown and verify Qualified - KMI option is not available for the user
    Then Click on Cancel

  #Then Logout from the SVO Portal
  #User can Open, Send reply, Edit fields and forward to any email from Emails tab of an Enquiry
  @SVO @SVO_175 @SVOBatch
  Scenario: User is able to create SVO Enquiries from KMI Record Section
    And mark test script status in "ScriptResults,C,175"
    #Given Access SVO Portal
    Given User access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,2"
    And Enter the required details for the Enquiry creation
    Then Save the new Enquiry
    Then Navigate to KMI section
    Then User created New KMI with fields like Contact "Enquiry,AA,2" ProductOffering "Enquiry,B,4" Brand "Enquiry,J,2" Model "Enquiry,K,2"
    Then User click on newly created KMI

  #Then Logout from the SVO Portal
  #User should be able to delete an Enquiry from KMI Record Section
  @SVO @SVO_176 @SSEC-447 @SVOBatch
  Scenario: User should be able to delete an Enquiry from KMI Record Section
    And mark test script status in "ScriptResults,C,176"
    #Given Access SVO Portal
    Given User access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,2"
    And Enter the required details for the Enquiry creation
    Then Save the new Enquiry
    Then Navigate to KMI section
    Then User created New KMI with fields like Contact "Enquiry,AA,2" ProductOffering "Enquiry,B,4" Brand "Enquiry,J,2" Model "Enquiry,K,2"
    Then User click on newly created KMI
    Then User delete the KMI Record

  #Then Logout from the SVO Portal
  #User should be able to Change owner of the Enquiry from KMI Record Section
  @SVO @SVO_177 @SSEC-448 @SVOBatch
  Scenario: User should be able to Change owner of the Enquiry from KMI Record Section
    And mark test script status in "ScriptResults,C,177"
    #Given Access SVO Portal
    Given User access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,2"
    And Enter the required details for the Enquiry creation
    Then Save the new Enquiry
    Then Navigate to KMI section
    Then User created New KMI with fields like Contact "Enquiry,AA,2" ProductOffering "Enquiry,B,4" Brand "Enquiry,J,2" Model "Enquiry,K,2"
    Then User click on newly created KMI
    Then User navigate to Related tab of KMI
    Then User creates new enquiry and link to KMI
    Then User navigate to Enquiry
    Then Change the owner of Enquiry to "Enquiry,AA,2"

  #Then Logout from the SVO Portal
  #User tries to Edit comment section of an work order of an Opportunity
  #User Role: Classic Service
  @SVO @SVO_178 @SVOBatch @Sanity
  Scenario: User tries to Edit comment section of an work order of an Opportunity
    And mark test script status in "ScriptResults,C,178"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then set up login to the user role "User Role,B,6"
    Then User Navigate to Opportunity tab
    Then User searched and select as "Opportunity,AC,2" opportunities from select list view
    And Open an opportunity with Record Type "Opportunity,R,13"
    Then navigate to Orders link and open the order
    Then Open the work order
    Then Verify Edit comment button is not available

  #Then Logout from SF_SVO Portal
  #User tries to edit the Stage history and Opportunity field history of an Opportunity
  @SVO @SVO_179 @SVOBatch
  Scenario: User tries to edit the Stage history and Opportunity field history of an Opportunity
    And mark test script status in "ScriptResults,C,179"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    And Search for Opportunity with stage "Design Brief & Quote" and open relevant Opportunity
    Then Navigate to Stage History and verify user cannot edit the stage history
    Then Navigate to Opportunity field history and verify user cannot edit the Opportunity field history

  #Then Logout from SF_SVO Portal
  #User create new Filter for searching an Opportunities
  @SVO @SVO_180 @SVOBatch
  Scenario: User create new Filter for searching an Opportunities
    And mark test script status in "ScriptResults,C,180"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to SVO
    Then User Navigate to Opportunity tab
    Then User selects New option to create New filter
    Then User fill the details like ListName "Opportunity,AL,2" and ListAPIname "Opportunity,AM,2" then click on save
    Then User Add filter by entering details like Field "Opportunity,AN,2" Operator "Opportunity,AO,2" Value "Opportunity,I,2"
    Then User click on Save filter
    Then Verify that New filter is created

  #Then Logout from SF_SVO Portal
  #User should be able to do E-Sign the document in person if the user is having Licence
  @SVO @SVO_181 @SVOBatch
  Scenario: User should be able to do E-Sign the document in person if the user is having Licence
    And mark test script status in "ScriptResults,C,181"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then set up login to the user role "User Role,B,17"
    Then User Navigate to Opportunity tab
    And User click on New button to create an opportunity
    And fill all mandatory fields of SVOBespoke Mike opportunity like OpportunityName "Opportunity,A,8" Closedate "Opportunity,AI,2" Stage "Opportunity,R,4" ProductOffering "Opportunity,B,2" Region "Opportunity,E,2" AccountName "Opportunity,D,9" PreferredContact "Opportunity,H,9" Brand "Opportunity,I,2" Model "Opportunity,J,2"
    Then Navigate to Quotes
    Then Create a new Bespoke design brief Quote "Opportunity,D,9"
    Then User save the new Bespoke design brief Mike Quote
    Then User selects the created Quote
    Then Navigate and open Design briefs
    Then Move the design brief stage from Draft to Submitted
    Then Return Render pack of design brief
    Then Navigate back to Quote section
    Then fill the price details of Quote like CostPrice "Opportunity,X,2" ListPrice "Opportunity,X,2" Contribution "Opportunity,X,2"  RetailPrice "Opportunity,X,2" Revenue"Opportunity,X,2"
    Then User should be able to Mark Status as complete for the order
    When User selects documents and click on Next step
    And Generated documents are sent for Electronic Signature through mail

  #Then Logout from SF_SVO Portal
  #User tries to select template of e2a mail after adding text into email body
  @SVO @SVO_182 @SVOBatch
  Scenario: User tries to select template of e2a mail after adding text into email body
    And mark test script status in "ScriptResults,C,182"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,8"
    And fill all mandatory fields of SVOBespoke opportunity like OpportunityName "Opportunity,A,8" Closedate "Opportunity,AI,2" Stage "Opportunity,R,4" ProductOffering "Opportunity,B,2" Region "Opportunity,E,2" AccountName "Opportunity,D,9" PreferredContact "Opportunity,H,9" Brand "Opportunity,I,2" Model "Opportunity,J,2"
    Then Click on Compose email
    And User selects the template of an email
    Then User save the email

  #Then Logout from SF_SVO Portal
  #User tries to Re-open the closed quote of an Opportunity
  @SVO @SVO_183 @SVOBatch @Sanity
  Scenario: User tries to Re-open the closed quote of an Opportunity
    And mark test script status in "ScriptResults,C,183"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,8"
    And Enter details of New Opportunity like Opportunity name "Opportunity,L,2" Product Offering "Opportunity,B,2" Account Name "Opportunity,D,5" Region "Opportunity,E,2" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,2" and Model "Opportunity,J,2"
    Then Save the created Opportunity
    Then Navigate to Quotes
    Then Create a new Quote "Test Quote" and save
    Then Navigate and open Design briefs
    Then Click on Submit and navigate back to Quote
    Then Mark the Quote status as Outcome rejected
    Then Mark the Quote status as Design brief draft and verify the error message

  #Then Logout from SF_SVO Portal
  #User tries to Edit the Priority position of an Opportunity
  @SVO @SVO_184 @SVOBatch
  Scenario: User tries to Edit the Priority position of an Opportunity
    And mark test script status in "ScriptResults,C,184"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,7"
    And Enter details of New Opportunity like Opportunity name "Opportunity,L,7" Stage "Opportunity,R,9" Region "Opportunity,E,2" Account Name "Opportunity,D,5" Preferred Contact "Opportunity,H,9" Programme "Opportunity,U,5" and Source "Opportunity,C,2"
    Then Save the created Opportunity
    Then Edit the priority position of the opportunity as "5" and Save

  #Then Logout from SF_SVO Portal
  #User tries to Move the opportunity stage from Waitlist to contracting(without navigating through intermediate stages)
  @SVO @SVO_185 @SVOBatch
  Scenario: User tries to Move the opportunity stage from Waitlist to contracting(without navigating through intermediate stages)
    And mark test script status in "ScriptResults,C,185"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,7"
    And Enter details of New Opportunity like Opportunity name "Opportunity,L,7" Stage "Opportunity,R,9" Region "Opportunity,E,2" Account Name "Opportunity,D,5" Preferred Contact "Opportunity,H,9" Programme "Opportunity,U,5" and Source "Opportunity,C,2"
    Then Save the created Opportunity
    Then Click on Contracting stage and mark as current stage

  #Then Logout from SF_SVO Portal
  #User sends an contract agreement for electronic signature
  @SVO @SVO_186 @SVOBatch
  Scenario: User sends an contract agreement for electronic signature
    And mark test script status in "ScriptResults,C,186"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    And Search for Opportunity with stage "Contracting" and open relevant Opportunity
    Then Click on S-Docs button
    Then Select a document and move to next step to send documents for electronic signature

  #Then Logout from SF_SVO Portal
  #User tries to Edit the status of an Order in backword direction
  @SVO @SVO_187 @SVOBatch
  Scenario: User tries to Edit the status of an Order in backword direction
    And mark test script status in "ScriptResults,C,187"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    And Open an opportunity with Record Type "Opportunity,R,7"
    Then navigate to Orders link and open the order
    When User changes order stage to "Opportunity,AP,2"
    Then Verify the error message displayed to save quote

  #Then Logout from SF_SVO Portal
  #User tries to Edit Donor details of an Classic Bespoke Opportunity
  @SVO @SVO_188 @SVOBatch
  Scenario: User tries to Edit Donor details of an Classic Bespoke Opportunity
    And mark test script status in "ScriptResults,C,188"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    Then User searched and select "Opportunity,AC,2" opportunities from select list view
    And Open an opportunity with Record Type "Opportunity,R,10"
    Then User edit the donor details of vehicle
    Then Verify the error message to fill the Programme

  #Then Logout from SF_SVO Portal
  #User tries to add New Task for an Opportunity
  @SVO @SVO_189 @SVOBatch @Sanity
  Scenario: User tries to add New Task for an Opportunity
    And mark test script status in "ScriptResults,C,189"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    And Open an opportunity with Record Type "Opportunity,R,7"
    When User navigate to Activity tab
    And User navigate to New task tab
    Then User created new task with subject field as "Opportunity,B,3"
    Then User save the Task

  #Then Logout from SF_SVO Portal
  #User Skips the Common order number Field in Orders Section and mark Status as complete and try to Mark Status as complete
  @SVO @SVO_190 @SVOBatch @Sanity
  Scenario: User Skips the Common order number Field in Orders Section and mark Status as complete and try to Mark Status as complete
    And mark test script status in "ScriptResults,C,190"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    Then User searched and select "Opportunity,AC,4" opportunities from select list view
    And Open an opportunity with Record Type "Opportunity,R,7"
    Then navigate to Orders link and open the order
    When User clears common order number
    And User mark status as complete
    Then Verify the error message to clear the Restricted party screening

  #Then Logout from SF_SVO Portal
  #User Able to upload file to the Orders at Outcome Stage
  @SVO @SVO_191 @SVOBatch
  Scenario: User Able to upload file to the Orders at Outcome Stage
    And mark test script status in "ScriptResults,C,191"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    Then User searched and select "Opportunity,AC,4" opportunities from select list view
    And Open an opportunity with Record Type "Opportunity,R,7"
    Then navigate to Orders link and open the order
    And User navigate to Files section
    When User add files to the selected order

  #Then Logout from SF_SVO Portal
  #User Able to share Files in Order section
  @SVO @SVO_192 @SVOBatch
  Scenario: User Able to share Files in Order section
    And mark test script status in "ScriptResults,C,192"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    And Search for Opportunity with stage "Order Placed" and open relevant Opportunity
    Then Navigate to Orders section and open the order
    Then Navigate to Files section
    Then Add a new file and save

  #Then Logout from SF_SVO Portal
  #User moves to Proposed status of created Commissioning Request from any non-closed Bespoke Opportunity
  @SVO @SVO_193 @SVOBatch
  Scenario: User moves to Proposed status of created Commissioning Request from any non-closed Bespoke Opportunity
    And mark test script status in "ScriptResults,C,193"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,8"
    And Enter details of New Opportunity like Opportunity name "Opportunity,L,2" Product Offering "Opportunity,B,2" Account Name "Opportunity,D,5" Region "Opportunity,E,2" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,2" and Model "Opportunity,J,2"
    Then Save the created Opportunity
    Then Navigate to Commissioning Request
    Then Create a new Commissioning Request from "Opportunity,AH,3" to "Opportunity,AI,3" and save
    Then Navigate to Reservations
    Then Create a new "Bespoke" reservation from "Opportunity,AH,3" to "Opportunity,AI,3" with contact "Opportunity,H,5" and save
    Then Navigate back to Commissioning request and verify the status is moved to Proposed

  #Then Logout from SF_SVO Portal
  #User verifies that Enquiry Owner receives an email when Commissioning Request status changed to Booked for any non-closed Bespoke Opportunity
  @SVO @SVO_194 @SVOBatch
  Scenario: User verifies that Enquiry Owner receives an email when Commissioning Request status changed to Booked for any non-closed Bespoke Opportunity
    And mark test script status in "ScriptResults,C,194"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,8"
    And Enter details of New Opportunity like Opportunity name "Opportunity,L,2" Product Offering "Opportunity,B,2" Account Name "Opportunity,D,5" Region "Opportunity,E,2" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,2" and Model "Opportunity,J,2"
    Then Save the created Opportunity
    Then Navigate to Commissioning Request
    Then Create a new Commissioning Request from "Opportunity,AH,3" to "Opportunity,AI,3" and save
    Then Navigate to Reservations
    Then Create a new "Bespoke" reservation from "Opportunity,AH,3" to "Opportunity,AI,3" with contact "Opportunity,H,5" and save
    Then Open the reservation and change the status to Booked and host "Opportunity,H,5"
    Then Navigate back to Commissioning request and verify the status is moved to "Booked"

  #Then Logout from SF_SVO Portal
  #User can be able to change the Closed stage  for opportunities Section
  @SVO @SVO_195 @SVOBatch
  Scenario: User can be able to change the Closed stage  for opportunities Section
    And mark test script status in "ScriptResults,C,195"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    And Search for Opportunity with stage "Closed Lost" and open relevant Opportunity
    Then Click on Change closed stage
    Then Edit the stage to Contract signed and save to verify the error

  #Then Logout from SF_SVO Portal
  #User skips the mandatory fields at contract signed and makes Mark status as complete
  @SVO @SVO_196 @SVOBatch
  Scenario: User skips the mandatory fields at contract signed and makes Mark status as complete
    And mark test script status in "ScriptResults,C,196"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    Then User searched and select "Opportunity,AC,2" opportunities from select list view
    And Open an opportunity with Record Type "Opportunity,R,11"
    When User clears Body style details of vehicle
    And User click on the mark stage as complete button to complete the process
    Then Verify the error message to clear the Restricted party screening

  #Then Logout from SF_SVO Portal
  #In orders at classic bespoke User skips the Handover date at Accepted sales stage and Mark status as complete
  @SVO @SVO_197 @SVOBatch
  Scenario: In orders at classic bespoke User skips the Handover date at Accepted sales stage and Mark status as complete
    And mark test script status in "ScriptResults,C,197"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    And Search for Opportunity with stage "Order Placed" and open relevant Opportunity
    And User navigates to Orders section from Related links of Opportunity
    Then Validate that Handover details are blank and try to move to the next stage

  #Then Logout from SF_SVO Portal
  #User fills the Handover date and check on Fully paid and mark status as complete
  @SVO @SVO_198 @SVOBatch
  Scenario: User fills the Handover date and check on Fully paid and mark status as complete
    And mark test script status in "ScriptResults,C,198"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,4"
    And Enter Required details of New Opportunity like Product Offering "Opportunity,B,4" Source "Opportunity,C,4" Account Name "Opportunity,D,5" Region "Opportunity,E,4" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,2" and Model "Opportunity,J,2"
    Then Save the created Opportunity
    Then Change the opportunity status to Order Placed
    Then navigate to Orders link and open the order
    Then Navigate to Handover details section and enter the required details
    And User click on the mark status as complete button of an Order

  #Then Logout from SF_SVO Portal
  #User can be able to delete the Invoice in Orders
  @SVO @SVO_199 @SVOBatch
  Scenario: User can be able to delete the Invoice in Orders
    And mark test script status in "ScriptResults,C,199"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,4"
    And Enter Required details of New Opportunity like Product Offering "Opportunity,B,4" Source "Opportunity,C,4" Account Name "Opportunity,D,5" Region "Opportunity,E,4" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,2" and Model "Opportunity,J,2"
    Then Save the created Opportunity
    Then Change the opportunity status to Order Placed
    Then navigate to Orders link and open the order
    Then Navigate to invoices
    Then Create a new invoice with Opportunity "Classic bespoke" Account "Opportunity,AE,2" and SAP account "Opportunity,AF,2" and save
    Then Click on Delete button

  #Then Logout from SF_SVO Portal
  #User cannot edit/change the auto populate fields for Invoice
  @SVO @SVO_200 @SVOBatch
  Scenario: User cannot edit/change the auto populate fields for Invoice
    And mark test script status in "ScriptResults,C,200"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,4"
    And Enter Required details of New Opportunity like Product Offering "Opportunity,B,4" Source "Opportunity,C,4" Account Name "Opportunity,D,5" Region "Opportunity,E,4" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,2" and Model "Opportunity,J,2"
    Then Save the created Opportunity
    Then Change the opportunity status to Order Placed
    Then navigate to Orders link and open the order
    Then Navigate to invoices
    Then Create a new invoice with Opportunity "Classic bespoke" Account "Opportunity,AE,2" and SAP account "Opportunity,AF,2" and save
    Then Verify that auto populated fields like Invoice name and Status are not editable

  #Then Logout from SF_SVO Portal
  #User can submit Commissioning Request which is Draft status for any non-closed Private Office Opportunity
  #SVO Private Office Opportunity
  @SVO @SVO_201 @SVOBatch
  Scenario: User is able to click on Checkbox under the Is void  for Invoice in edit option
    And mark test script status in "ScriptResults,C,201"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,4"
    And Enter Required details of New Opportunity like Product Offering "Opportunity,B,4" Source "Opportunity,C,4" Account Name "Opportunity,D,5" Region "Opportunity,E,4" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,2" and Model "Opportunity,J,2"
    Then Save the created Opportunity
    Then Change the opportunity status to Order Placed
    Then navigate to Orders link and open the order
    Then Navigate to invoices
    Then Create a new invoice with Opportunity "Classic bespoke" Account "Opportunity,AE,2" and SAP account "Opportunity,AF,2" and save
    Then Verify that auto populated fields like Invoice name and Status are not editable

  #Then Logout from SF_SVO Portal
  #User is able to add Note to the Opportunity
  #SVO Private Office Opportunity
  @SVO @SVO_202 @SVOBatch
  Scenario: User is able to add Note to the Opportunity
    And mark test script status in "ScriptResults,C,202"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,10"
    And fill all mandatory fields of SVO Private Office opportunity like OpportunityName "Opportunity,A,10" Closedate "Opportunity,AI,2" Stage "Opportunity,R,4" ProductOffering "Opportunity,B,10" Region "Opportunity,E,2" AccountName "Opportunity,D,9" PreferredContact "Opportunity,H,9" Brand "Opportunity,I,2" Model "Opportunity,J,2"
    When User navigate to Notes Section
    And User Create New Quote with title "Opportunity,A,10"
    Then User Save the Note

  #Then Logout from SF_SVO Portal
  #User is able to Share and Delete the Note
  #SVO Private Office Opportunity
  @SVO @SVO_203 @SVOBatch
  Scenario: User is able to Share and Delete the Note
    And mark test script status in "ScriptResults,C,203"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,10"
    And fill all mandatory fields of SVO Private Office opportunity like OpportunityName "Opportunity,A,10" Closedate "Opportunity,AI,2" Stage "Opportunity,R,4" ProductOffering "Opportunity,B,10" Region "Opportunity,E,2" AccountName "Opportunity,D,9" PreferredContact "Opportunity,H,9" Brand "Opportunity,I,2" Model "Opportunity,J,2"
    When User navigate to Notes Section
    And User Create New Quote with title "Opportunity,A,10"
    Then User share the created note to user "User Role,B,17"
    And User delete the created note

  #Then Logout from SF_SVO Portal
  #User is able to delete the order which is at outcome stage in classic bespoke opportunity
  @SVO @SVO_204 @SVOBatch @Sanity
  Scenario: User is able to delete the order which is at outcome stage in classic bespoke opportunity
    And mark test script status in "ScriptResults,C,204"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    Then User searched and select "Opportunity,AC,2" opportunities from select list view
    And Open an opportunity with Record Type "Opportunity,R,13"
    Then navigate to Orders link and open the order
    Then User deletes the order

  #Then Logout from SF_SVO Portal
  #User tries to create a new Order for a Corporate account
  @SVO @SVO_205 @SVOBatch
  Scenario: User tries to create a new Order for a Corporate account
    And mark test script status in "ScriptResults,C,205"
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then User Searches for account "Account,E,10" and selects it
    And User Navigates to Orders Section
    Then Verify new button is not available to create a new order

  #Then Logout from the SF SVO Portal
  #An order can automatically created after completing quote
  #SVO Bespoke access
  @SVO @SVO_206 @SVOBatch
  Scenario: User tries to create Opportunities (except SVO Bespoke and SVO Private Office) for a Corporate account
    And mark test script status in "ScriptResults,C,206"
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Corporate account with Record type "Account,A,9" Account name "Account,E,9" Email "Account,H,9" Region "Account,I,9"
    Then Navigate to the SVO Client Opportunities section and click on New Classic "Classic service" opportunity
    Then Enter Required details in New Opportunity like Product Offering "Opportunity,B,2" Source "Opportunity,C,2" Account Name "Opportunity,D,2" Region "Opportunity,E,2" Preferred Contact "Opportunity,H,2" Brand "Opportunity,I,2" and Model "Opportunity,J,2"

  #Then Logout from the SF SVO Portal
  #User tries to create a new Opportunity with SVO Bespoke Record Type
  #Classic Bespoke access
  @SVO @SVO_207 @SVOBatch
  Scenario: User tries to create a new Opportunity with SVO Bespoke Record Type
    And mark test script status in "ScriptResults,C,207"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    Then Click on New opportunity button and verify user cannot create SVO Bespoke Record Type opportunity

  #Then Logout from SF_SVO Portal
  #User tries to move the opportunity stage from  contract signed to order placed without an order creation
  #Users : Classic Finance
  @SVO @SVO_208 @SVOBatch @Sanity
  Scenario: User tries to move the opportunity stage from  contract signed to order placed without an order creation
    And mark test script status in "ScriptResults,C,208"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    Then User searches for "Opportunity,AC,5" opportunities from select list view
    And Open an opportunity with Record Type "Opportunity,R,11"
    And User click on the mark stage as complete button to complete the process
    Then Verify the error message to close the opportunity

  #Then Logout from SF_SVO Portal
  #User tries to create a new Programme while creating Classic Limited Edition opportunity
  #User Role : Classic Sales User : Michael Bishop
  @SVO @SVO_209 @SVOBatch
  Scenario: User tries to create a new Programme while creating Classic Limited Edition opportunity
    And mark test script status in "ScriptResults,C,209"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then set up login to the user role "User Role,B,18"
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,9"
    When click on Programme text box
    Then Verify that New Record button is not available

  #Then Logout from SF_SVO Portal
  #User tries to generate document through S-DOCS for any open opportunity
  #Users : SVO Private Office
  @SVO @SVO_210 @SVOBatch
  Scenario: User tries to generate document through S-DOCS for any open opportunity
    And mark test script status in "ScriptResults,C,210"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    And Open opportunity with Record Type "Opportunity,R,7"
    Then Navigate to Quotes section under Related lists quick links section
    Then Select a Quote which is at the Design Brief Draft stage
    Then Verify the button Send Sales Agreement

  #Then Logout from SF_SVO Portal
  #User tries to delete Closed Won Opportunity
  #Users : Classic Finance
  @SVO @SVO_211 @SVOBatch
  Scenario: User tries to delete Closed Won Opportunity
    And mark test script status in "ScriptResults,C,211"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    Then User searches for "Opportunity,AC,5" opportunities from select list view
    And Open an opportunity with Record Type "Opportunity,R,13"
    Then Verify the Delete button

  #Then Logout from SF_SVO Portal
  #User tries to create a new SAP Account for an individual account
  #Users: Special Operations Data Manager
  @SVO @SVO_212 @SVOBatch
  Scenario: User tries to create a new SAP Account for an individual account
    And mark test script status in "ScriptResults,C,212"
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then User searches for "Account,O,2" Account from select list view
    When User click on first Account in the list
    And Verify the SAP Accounts link

  #Then Logout from the SF SVO Portal
  #User tries to add new vehicle ownership as a driver to an individual account
  #Users : Bespoke user
  @SVO @SVO_213 @SVOBatch
  Scenario: User tries to add new vehicle ownership as a driver to an individual account
    And mark test script status in "ScriptResults,C,213"
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then User searches for "Account,O,2" Account from select list view
    When User click on first Account in the list
    Then Navigate to the Vehicles Owned section
    And Verify the New button to add relationship as a driver

  #Then Logout from the SF SVO Portal
  #User can save any type of Reservation from Commissioning Suite menu
  @SVO @SVO_214 @SVOBatch
  Scenario: User can save any type of Reservation from Commissioning Suite menu
    And mark test script status in "ScriptResults,C,214"
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Commissioning Suite
    And User selects time slot "Account,P,3" for Reservation
    Then fill all mandatory fields of Reservation like Status "Account,Q,2" CommissioningRequest "Account,R,2" PrimaryContact "Account,B,7" MeetingType "Account,S,2" MaxAttendees "Account,T,2"
    Then User save the Reservation

  #Then Logout from the SF SVO Portal
  #User tries to create a new order with any record type for an individual account
  #Classic sales admin login
  @SVO @SVO_215 @SVOBatch
  Scenario: User tries to create a new order with any record type for an individual account
    And mark test script status in "ScriptResults,C,215"
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    And User searches for a User Account "New Enquiry,AA,2"
    Then User Navigates to the User Account "New Enquiry,AA,2"
    And User Navigates to Orders Section
    Then Verify new button is not available to create a new order

  #Then Logout from the SF SVO Portal
  #User tries to update consent for marketing communication for an individual account
  #Special Operations Data Manager/SVO Bespoke Data Manager login
  @SVO @SVO_216 @SVOBatch @Sanity
  Scenario: User tries to update consent for marketing communication for an individual account
    And mark test script status in "ScriptResults,C,216"
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    And User searches for a User Account "New Enquiry,AA,2"
    Then User Navigates to the User Account "New Enquiry,AA,2"
    Then Verify Update Consent button is not available for the user

  #Then Logout from the SF SVO Portal
  #User tries to change the Person Account type or record type of an individual account
  #User Role : Special Operations Data Manager
  @SVO @SVO_217 @SVOBatch
  Scenario: User tries to change the Person Account type or record type of an individual account
    And mark test script status in "ScriptResults,C,217"
    #Given Access to SVO Portal
    Given User access SVO Portal
    Then set up the user role login "User Role,B,7"
    When User Navigate to Accounts
    Then User searches for "Account,O,2" Account from select list view
    When User click on first Account in the list
    And User Navigate to Change Record Type section
    Then Verify the access denied error message

  #Then Logout from the SF SVO Portal
  #User tries to create a new SAP Account and add New Vehicle Ownership for an Corporate account
  # User Role : SVO Bespoke
  @SVO @SVO_218 @SVOBatch
  Scenario: User tries to create a new SAP Account and add New Vehicle Ownership for an Corporate account
    And mark test script status in "ScriptResults,C,218"
    #Given Access to SVO Portal
    Given User access SVO Portal
    Then set up the user role login "User Role,B,8"
    When User Navigate to Accounts
    Then User searches for "Account,O,3" Account from select list view
    When User click on first Account in the list
    And Verify the SAP Accounts link

  #Then Logout from the SF SVO Portal
  #User sends group email from any open enquiry
  #user : admin
  @SVO @SVO_219 @SVOBatch
  Scenario: User tries to add New vehicle ownership using Vehicles owned link
    And mark test script status in "ScriptResults,C,219"
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Corporate account with Record type "Account,A,9" Account name "Account,E,9" Email "Account,H,9" Region "Account,I,9"
    Then Navigate to the Vehicles Owned section and click on New owner record type
    Then Enter details in new Vehicles owned and Save it

  #Then Logout from the SF SVO Portal
  #User tries to create new SVO client Opportunities for a Corporate account
  #Classic service role
  @SVO @SVO_220 @SSEC-491 @SVOBatch
  Scenario: User tries to create new SVO client Opportunities for a Corporate account
    And mark test script status in "ScriptResults,C,220"
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then User Searches for account "Account,E,10" and selects it
    Then Navigate to the SVO Client Opportunities section and click on New and verify SVO Bespoke record type is not available

  #Then Logout from the SF SVO Portal
  #User tries to create a new opportunity with Classic Record type for a corporate account
  #SVO Bespoke role
  @SVO @SVO_221 @SSEC-492 @SVOBatch
  Scenario: User tries to create a new opportunity with Classic Record type for a corporate account
    And mark test script status in "ScriptResults,C,221"
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then User Searches for account "Account,E,10" and selects it
    Then Navigate to the Opportunities section and click on New and verify Classic record type is not available

  #Then Logout from the SF SVO Portal
  #User tries to link an Enquiry for Classic Bespoke opportunity
  #user : admin
  @SVO @SVO_222 @SVOBatch
  Scenario: User tries to link an Enquiry for Classic Bespoke opportunity
    And mark test script status in "ScriptResults,C,222"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then User Navigate to Opportunity tab
    Then User searched and select "Opportunity,AC,2" opportunities from select list view
    And Open an opportunity with Record Type "Opportunity,R,7"
    When User click on Veicle number under donor details
    And User navigate to Related Tab
    Then create an new enquiry with Record type "Opportunity,A,8"
    And User enters Enquiry title "Opportunity,A,8" and click on save
    Then Verify that new enquiry is linked to Opportunity

  #Then Logout from SF_SVO Portal
  #User tries to Save vehicle record of Classic Bespoke Opportunity without adding Full VIN
  @SVO @SVO_223 @SVOBatch
  Scenario: User tries to Save vehicle record of Classic Bespoke Opportunity without adding Full VIN
    And mark test script status in "ScriptResults,C,223"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    When User Navigate to Opportunity
    And User searches for Classic bespoke opportunity and selects it
    Then Navigate to Vehicle field and click on edit
    Then User adds new "Classic Vehicle" record
    Then Enter fields Record name "Opportunity,S,5" Brand "Opportunity,I,2" model "Opportunity,J,2" and save leaving the VIN field blank
    Then Verify the error message displayed for VIN field

  #Then Logout from SF_SVO Portal
  #User tries to Change Owner for multiple Vehicle manufacturers at a time
  @SVO @SVO_224 @SVOBatch
  Scenario: User tries to Change Owner for multiple Vehicle manufacturers at a time
    And mark test script status in "ScriptResults,C,224"
    #Given Access SF-SVO Portal
    Given User access SVO Portal
    Then Navigate to Vehicle manufacturers tab
    Then Select multiple Vehicle manufacturers and click on Change Owner
    Then Change the owner and save

  #Then Logout from SF-SVO Portal
  #User tries to make a Brand as 'Is JLR Owned Brand' in Vehicle manufacturers section
  @SVO @SVO_225 @SVOBatch
  Scenario: User tries to make a Brand as 'Is JLR Owned Brand' in Vehicle manufacturers section
    And mark test script status in "ScriptResults,C,225"
    #Given Access SF-SVO Portal
    Given User access SVO Portal
    Then Navigate to Vehicle manufacturers tab
    Then Edit for Is JLR owned brand field and select the checkbox
    Then Save the changes and verify user is able to make the brand as Is JLR Owned Brand

  #Then Logout from SF-SVO Portal
  #User tries to add New Vehicle series for a Model of any Vehicle Manufacturer
  @SVO @SVO_226 @SVOBatch @Sanity
  Scenario: User tries to add New Vehicle series for a Model of any Vehicle Manufacturer
    And mark test script status in "ScriptResults,C,226"
    #Given Access SF-SVO Portal
    Given User access SVO Portal
    Then Navigate to Vehicle manufacturers tab
    Then Select a manufacturer
    Then Select a model
    Then Navigate to Vehicle series and click on New button
    Then Enter details Name "Test Vehicle" model year start "2020" model year end "2030" and save

  #Then Logout from SF-SVO Portal
  #User tries to add New Body style for a Model of any Vehicle Manufacturer
  @SVO @SVO_227 @SVOBatch
  Scenario: User tries to add New Body style for a Model of any Vehicle Manufacturer
    And mark test script status in "ScriptResults,C,227"
    #Given Access SF-SVO Portal
    Given User access SVO Portal
    Then Navigate to Vehicle manufacturers tab
    Then Select a manufacturer
    Then Select a model
    Then Navigate to Body Styles and click on New button
    Then Enter Name "Test Body Style" and save

  #Then Logout from SF-SVO Portal
  #User tries to add New Vehicle derivatives for a Model of any Vehicle Manufacturer
  @SVO @SVO_228 @SVOBatch
  Scenario: User tries to add New Vehicle derivatives for a Model of any Vehicle Manufacturer
    And mark test script status in "ScriptResults,C,228"
    #Given Access SF-SVO Portal
    Given User access SVO Portal
    Then Navigate to Vehicle manufacturers tab
    Then Select a manufacturer
    Then Select a model
    Then Navigate to Vehicle Derivatives and click on New button
    Then Enter derivative Name "Test derivative" model "Opportunity,J,2" series Name "Test Vehicle" and save

  #Then Logout from SF-SVO Portal
  #User creates Record share data for any Model of Vehicle Manufacturer [with Read only Vehicle Model access level]
  @SVO @SVO_229 @SVOBatch
  Scenario: User creates Record share data for any Model of Vehicle Manufacturer [with Read only Vehicle Model access level]
    And mark test script status in "ScriptResults,C,229"
    #Given Access SF-SVO Portal
    Given User access SVO Portal
    Then Navigate to Vehicle manufacturers tab
    Then Select a manufacturer
    Then Select a model
    Then Click on Sharing button
    Then Enter the user name "Opportunity,H,3" and save

  #Then Logout from SF-SVO Portal
  #User creates Record share data for any Model of Vehicle Manufacturer [with Read/Write Vehicle Model access level]
  @SVO @SVO_230 @SVOBatch
  Scenario: User creates Record share data for any Model of Vehicle Manufacturer [with Read/Write Vehicle Model access level]
    And mark test script status in "ScriptResults,C,230"
    #Given Access SF-SVO Portal
    Given User access SVO Portal
    Then Navigate to Vehicle manufacturers tab
    Then Select a manufacturer
    Then Select a model
    Then Click on Sharing button
    Then Enter the user name "Opportunity,H,3" and save

  #Then Logout from SF-SVO Portal
  #User tries to Enter Model start date greater than the Model end date of Vehicle manufacturer
  @SVO @SVO_231 @SVOBatch
  Scenario: User tries to Enter Model start date greater than the Model end date of Vehicle manufacturer
    And mark test script status in "ScriptResults,C,231"
    #Given Access SF-SVO Portal
    Given User access SVO Portal
    Then Navigate to Vehicle manufacturers tab
    Then Select a manufacturer
    Then Select a model
    Then Enter model start year as "2022" model end year as "2021" and save
    Then Verify the error message displayed for model end year entered before model start year

  #Then Logout from SF-SVO Portal
  #User tries to access Vehicle Manufacturer section
  #Classic Sales role
  @SVO @SVO_232 @SVOBatch
  Scenario: User tries to access Vehicle Manufacturer section
    And mark test script status in "ScriptResults,C,232"
    #Given Access SF-SVO Portal
    Given User access SVO Portal
    Then Search for "Vehicle manufacturer" in search box
    Then Verify no search results are found for Vehicle manufacturer

  #Then Logout from SF-SVO Portal
  #User tries to Edit Vehicle manufacturer details
  @SVO @SVO_233 @SVOBatch
  Scenario: User tries to Edit Vehicle manufacturer details
    And mark test script status in "ScriptResults,C,233"
    #Given Access SF-SVO Portal
    Given User access SVO Portal
    Then Navigate to Vehicle manufacturers tab
    Then Select a manufacturer
    Then Edit vehicle manufacturer name as "New Vehicle" and save

  #Then Logout from SF-SVO Portal
  #User tries to delete the Vehicle manufacturer
  @SVO @SVO_234 @SVOBatch
  Scenario: User tries to delete the Vehicle manufacturer
    And mark test script status in "ScriptResults,C,234"
    #Given Access SF-SVO Portal
    Given User access SVO Portal
    Then Navigate to Vehicle manufacturers tab
    Then Create a new vehicle manufacturer with name "New Vehicle" and save
    Then Click on Delete button to delete vehicle manufacturer

  #Then Logout from SF-SVO Portal
  #User tries to Change Owner for one of the Vehicle manufacturer
  @SVO @SVO_235 @SVOBatch @FifBatch
  Scenario: User tries to Change Owner for one of the Vehicle manufacturer
    And mark test script status in "ScriptResults,C,235"
    #Given Access SVO Portal
    Given User access SVO Portal
    Then Navigate to Vehicle manufacturers tab
    Then Select only one Vehicle manufacturer and click on Change Owner
    Then Change the owner and save

  #Then Logout from SF-SVO Portal
  #User tries to change owner of Vehicle manufacturer without selecting any Vehicle manufacturer from the Manufacturers list
  @SVO @SVO_236 @SVOBatch
  Scenario: User tries to change owner of Vehicle manufacturer without selecting any Vehicle manufacturer from the Manufacturers list
    And mark test script status in "ScriptResults,C,236"
    #Given Access SF-SVO Portal
    Given User access SVO Portal
    Then Navigate to Vehicle manufacturers tab
    And User tries to change owner of Vehicle Manufacturer without selecting any Vehicle Manufacturer from list

  #Then Logout from SF-SVO Portal
  #User tries to create New Manufacturer of Vehicles
  @SVO @SVO_237 @SVOBatch @FifBatch
  Scenario: User tries to create New Manufacturer of Vehicles
    And mark test script status in "ScriptResults,C,237"
    #Given Access SVO Portal
    Given User access SVO Portal
    Then Navigate to Vehicle manufacturers tab
    Then Create a new vehicle manufacturer with name "New Vehicle Manufacturer" and save

  #Then Logout from SF-SVO Portal
  #User tries add New Model for any Brand
  @SVO @SVO_238 @SVOBatch
  Scenario: User tries add New Model for any Brand
    And mark test script status in "ScriptResults,C,238"
    #Given Access SF-SVO Portal
    Given User access SVO Portal
    Then Navigate to Vehicle manufacturers tab
    Then Select a manufacturer
    And User tries to add New Model for any Brand

  #Then Logout from SF-SVO Portal
  #User tries to delete a Model of any Brand under Vehicle Manufacturer section
  @SVO @SVO_239 @SVOBatch @FifBatch @Sanity
  Scenario: User tries to delete a Model of any Brand under Vehicle Manufacturer section
    And mark test script status in "ScriptResults,C,239"
    #Given Access SVO Portal
    Given User access SVO Portal
    Then Navigate to Vehicle manufacturers tab
    Then Create a new vehicle manufacturer with name "New Vehicle Manufacturer" and save
    And User tries to add New Model for any Brand
    Then User tries to delete a Model of any Brand under Vehicle Manufacturer section

  #Then Logout from SF-SVO Portal
  #User tries to Change the Priority of an completed Task
  @SVO @SVO_240 @SVOBatch @FifBatch
  Scenario: User tries to Change the Priority of an completed Task
    And mark test script status in "ScriptResults,C,240"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then Navigate to Tasks tab
    And User selects New option to create new task
    Then fill all mandatory fields of Task with subject "Opportunity,AL,2" and click on Save
    Then User Mark task as completed
    And User tries to change the priority of completed task

  #Then Logout from SF_SVO Portal
  #User tries to change the Due date of an completed Task
  #Tasks
  @SVO @SVO_241 @SVOBatch @FifBatch
  Scenario: User tries to change the Due date of an completed Task
    And mark test script status in "ScriptResults,C,241"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then Navigate to Tasks tab
    And Select "Opportunity,AC,6" Tasks from select list view
    When User select first task from the list
    And User change the Due Date of task to "Opportunity,AG,2"

  #Then Logout from SF_SVO Portal
  #User tries to Edit the Task details under Tasks Section
  #User Role : SVO Bespoke Data Manager
  @SVO @SVO_242 @SVOBatch
  Scenario: User tries to Edit the Task details under Tasks Section
    And mark test script status in "ScriptResults,C,242"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then set up login to the user role "User Role,B,10"
    Then Navigate to Tasks tab
    And Select "Opportunity,AC,7" Tasks from select list view
    When User select first task from the list
    Then User Edit the selected task
    And Verify the Access denied error message

  #Then Logout from SF_SVO Portal
  #User tries to Delete completed Task  under Tasks section
  #User Role: Special Operations Data Manager
  @SVO @SVO_243 @SVOBatch @FifBatch @Sanity
  Scenario: User tries to Delete completed Task  under Tasks section
    And mark test script status in "ScriptResults,C,243"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    #Then set up login to the user role "User Role,B,7"
    Then Navigate to Tasks tab
    And Select "Opportunity,AC,7" Tasks from select list view
    When User select first task from the list
    And User assign the Task to "Opportunity,AE,2"
    Then User Mark task as completed
    When User delete the selected Task

  #And Verify the Access denied error message to delete the Task
  #Then Logout from SF_SVO Portal
  #User tries to create New Task
  #Tasks
  @SVO @SVO_244 @SVOBatch
  Scenario: User tries to create New Task
    And mark test script status in "ScriptResults,C,244"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then Navigate to Tasks tab
    And User selects New option to create new task
    Then fill all mandatory fields of Task with subject "Opportunity,AL,2" and click on Save

  #Then Logout from SF_SVO Portal
  #User tries to complete the Task
  #Tasks
  @SVO @SVO_245 @SVOBatch
  Scenario: User tries to complete the Task
    And mark test script status in "ScriptResults,C,245"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then Navigate to Tasks tab
    And Select "Opportunity,AC,7" Tasks from select list view
    When User select first task from the list
    Then User Mark task as completed

  #Then Logout from SF_SVO Portal
  #User tries to Re-Open the Completed task
  @SVO @SVO_246 @SVOBatch @Sanity
  Scenario: User tries to Re-Open the Completed task
    And mark test script status in "ScriptResults,C,246"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
    Then Navigate to Tasks tab
    And User selects New option to create new task
    Then fill all mandatory fields of Task with subject "Opportunity,AL,2" and click on Save
    When User select first task from the list
    Then User Mark task as completed

  #Then Logout from SF_SVO Portal
  #User tries to access Sections other than Home and Design Briefs sections
  @SVO @SVO_247 @SVOBatch
  Scenario: User tries to access Sections other than Home and Design Briefs sections
    And mark test script status in "ScriptResults,C,247"
    #Given Access SF_SVO Portal
    Given User access SVO Portal

  #Then Logout from SF_SVO Portal
  #User tries to Update Consent of any Contact
  @SVO @SVO_248 @SVOBatch
  Scenario: User tries to Update Consent of any Contact
    And mark test script status in "ScriptResults,C,248"
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Individual account with Record type "Account,A,2" First name "Account,L,2" Last name "Account,M,3" Region "Account,N,3" Email "Account,K,3"
    Then Click on Update Consent
    Then Update the consent as required and Save

  #Then Logout from the SF SVO Portal
  #User tries to create New programme
  @SVO @SVO_249 @SVOBatch
  Scenario: User tries to create New programme
    And mark test script status in "ScriptResults,C,249"
    #Given Access SF_SVO Portal
    Given User access SVO Portal

  #Then Logout from SF_SVO Portal
  #User tries to Change Owner of Programme in Classic Opportunity
  @SVO @SVO_250 @SVOBatch @Sanity
  Scenario: User tries to Change Owner of Programme in Classic Opportunity
    And mark test script status in "ScriptResults,C,250"
    #Given Access SF_SVO Portal
    Given User access SVO Portal
