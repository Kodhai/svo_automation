Feature: SVO Customer Responses Validations

  #User is able to receive an email remainder for Classic sales enquiry created through email
  @SVO_1039_emailremainder_01
  Scenario: User is able to receive an email remainder for Classic sales enquiry created through email
    Given Access to mail account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    When Customer send an enquiry mail to user "CustomerResponse,A,5"
    And Logout from users mail account
    Given Login to SVO Portal as classic Sales user with Username "LoginUsers,B,5" and Password "LoginUsers,C,5"
    And Verify that the New Classic Sales enquiry is created automatically on SVO Portal
    Then Verify that Created date, Enquiry title and Enquiry status on Enquiries page
    And Verify SLA first contact time remaining details on Enquiry page
    Then User update owner as UAT test for an private office enquiry Owner Name "LoginUsers,B,16"
    And Verify that the enquiry is removed from the list view for which the owner has been changed
    Then User Logout from SVO Portal
    Given Access to owner mail account with Username "LoginUsers,B,16" and password "LoginUsers,C,16"
    And Verify that user receives and email remainder for the created enquiry after SLA Breach
    And Logout from users mail account
	
  #User is able to receive an email remainder for Private Office enquiry created through email
  @SVO_1039_emailremainder_02
  Scenario: User is able to receive an email remainder for Private Office enquiry created through email
    Given Access to mail account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    When Customer send an enquiry mail to user "CustomerResponse,A,2"
    And Logout from users mail account
    Given Login to SVO Portal by private office user with Username "LoginUsers,B,4" and Password "LoginUsers,C,4"
    And Verify automatically created private office enquiry on SVO Portal
    Then Verify that Created date, Enquiry title and Enquiry status on Enquiries page
    And Verify SLA first contact time remaining details on Enquiry page
    Then User update owner as UAT test for an private office enquiry Owner Name "LoginUsers,B,16"
    And Verify that the enquiry is removed from the list view for which the owner has been changed
    Then User Logout from SVO Portal
    Given Access to owner mail account with Username "LoginUsers,B,16" and password "LoginUsers,C,16"
    And Verify that user receives and email remainder for the created enquiry after SLA Breach
    And Logout from users mail account

  #User is able to receive an email remainder for SVO Bespoke enquiry created on SVO Portal
  @SVO_1039_emailremainder_03
  Scenario: User is able to receive an email remainder for SVO Bespoke enquiry created on SVO Portal
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Enquiries tab
    And User creates an Enquiry with record type "EnquiryLostReason,A,4"
    And Verify SLA first contact time remaining details on Enquiry page
    Then User update owner as UAT test for an private office enquiry Owner Name "LoginUsers,B,16"
    And Verify that the enquiry is removed from the list view for which the owner has been changed
    Then User Logout from SVO Portal
    Given Access to owner mail account with Username "LoginUsers,B,16" and password "LoginUsers,C,16"
    And Verify that user receives and email remainder for the created enquiry after SLA Breach
    And Logout from users mail account

  #User is able to receive an email remainder for Classic Service enquiry created on SVO Portal
  @SVO_1039_emailremainder_04
  Scenario: User is able to receive an email remainder for Classic Service enquiry created on SVO Portal
    Given Login to the SVO Portal by classic service uat user with Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    Then User navigates to the Enquiries tab
    And User creates an Enquiry with record type "EnquiryLostReason,A,3"
    And Verify SLA first contact time remaining details on Enquiry page
    Then User update owner as UAT test for an private office enquiry Owner Name "LoginUsers,B,16"
    And Verify that the enquiry is removed from the list view for which the owner has been changed
    Then User Logout from SVO Portal
    Given Access to owner mail account with Username "LoginUsers,B,16" and password "LoginUsers,C,16"
    And Verify that user receives and email remainder for the created enquiry after SLA Breach
    And Logout from users mail account

  #An Enquiry is not created on SVO portal if the customer sends an email to invalid user
  @SVO_1039_emailremainder_05
  Scenario: An Enquiry is not created on SVO portal if the customer sends an email to invalid user
    Given Access to mail account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    When Customer send an enquiry mail to user "CustomerResponse,A,6"
    And Logout from users mail account
    Given Access to SVO Portal as classic Sales user with Username "LoginUsers,B,5" and Password "LoginUsers,C,5"
    And Verify that the New Classic Sales enquiry is not created automatically on SVO Portal
    Then User Logout from SVO Portal

  #User is not able to receive any email remainder for Classic sales enquiry if the enquiry is contacted within SLA breach
  @SVO_1039_emailremainder_06
  Scenario: User is not able to receive any email remainder for Classic sales enquiry if the enquiry is contacted within SLA breach
    Given Access to mail account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    When Customer send an enquiry mail to user "CustomerResponse,A,3"
    Then Verify that Customer receives an acknowledgement email
    And Logout from users mail account
    Given Login to the SVO Portal by bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    And Verify that the New Bespoke enquiry is created automatically on SVO Portal
    Then Verify that Created date, Enquiry title and Enquiry status on Enquiries page
    And Verify SLA first contact time remaining details on Enquiry page
    And Verify that subject name is auto-populated on emails page
    Then User send an email to customer "LoginUsers,B,17" with Business unit "CustomerResponse,I,2" and subject
    And Verify that enquiry status on enquiries page
    Then User update owner as UAT test for an enquiry Owner Name "LoginUsers,B,16"
    And Verify that the enquiry is removed from the list view for which the owner has been changed
    Then User Logout from the SVO Portal
    Given Access to owner mail account with Username "LoginUsers,B,16" and password "LoginUsers,C,16"
    And Verify that user not received and email remainder for the created enquiry after SLA Breach
    And Logout from users mail account
