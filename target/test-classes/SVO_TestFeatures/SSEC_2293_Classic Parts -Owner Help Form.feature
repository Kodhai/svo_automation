Feature: Validations on SSEC_2293 User stories

  #User able to create a web form and submit it
  @SSEC_2393_CaseCreation_1 @ClassicParts
  Scenario: User able to create a web form and submit it
    Given User open the Ownerhelp portal
    Then User fill all the mandatory details
    And User close the browser

  #User unable to submit the web form
  @SSEC_2393_CaseCreation_2 @ClassicParts
  Scenario: User unable to submit the web form
    Given User open the Ownerhelp portal
    Then User is getting error to not fill all the mandatory details
    And User close the browser

  #Verify that after submitting the web form, Parts & Technical record type case should be created in Salesforce and assigned to Parts & technical queue
  @SSEC_2393_CaseCreation_3 @ClassicParts
  Scenario: Verify that after submitting the web form, Parts & Technical record type case should be created in Salesforce and assigned to Parts & technical queue
    Given User open the Ownerhelp portal
    Then User fill all the mandatory details
    Given Laterally Access to SVO Portal as Classic Parts and technical user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    And User navigate to cases tab

  #Verify that user should able to modify/edit the record details in Salesforce
  @SSEC_2293_ClassicPartsOwnerHelpForm_04 @SSEC-2293 @backlog
  Scenario: Verify that user should able to modify/edit the record details in Salesforce
    #Given Login to the Classic Parts portal
    Then User launches the owner help form
    Then User create an Parts and Technical Case from owner Help form with Email "CaseCreation,A,2" First name "CaseCreation,G,2" Last name "CaseCreation,H,2" Country "CaseCreation,I,2" Phone number "CaseCreation,K,2" VIN number "CaseCreation,J,2"
    #Then Verify that an user receives an outlook email notification on Parts and Technical case creation
    Given User login to the salesforce Portal as Classic Parts and Technical user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User navigate to Cases Tab
    Then User Verifies that Parts and Technical case is automatically created on SVO Portal
    And User Edit the created case
    Then User Logouts from an SVO Portal

  #User able to delete the record from the cases
  @SSEC_2293_ClassicPartsOwnerHelpForm_05 @SSEC-2293 @backlog
  Scenario: User able to delete the record from the cases
    #Given Login to the Classic Parts portal
    Then User launches the owner help form
    Then User create an Parts and Technical Case from owner Help form with Email "CaseCreation,A,2" First name "CaseCreation,G,2" Last name "CaseCreation,H,2" Country "CaseCreation,I,2" Phone number "CaseCreation,K,2" VIN number "CaseCreation,J,2"
    #Then Verify that an user receives an outlook email notification on Parts and Technical case creation
    Given User login to the salesforce Portal as Classic Parts and Technical user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User navigate to Cases Tab
    Then User Verifies that Parts and Technical case is automatically created on SVO Portal
    And User Delete the created case
    Then User Logouts from an SVO Portal
