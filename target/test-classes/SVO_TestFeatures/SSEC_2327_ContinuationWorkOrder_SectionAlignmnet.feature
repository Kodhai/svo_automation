@SSEC_2327
Feature: Validate Continuation WOrk Orders Section Alignment scripts

  @SSEC_2327_01 @Sonica
  Scenario: User is able to create a new Classic Continuation Opportunity record
    Given Access to SVO Portal as Classic Sales user with Username "LoginUsers,B,5" and password "LoginUsers,C,5"
    And User navigates to Opportunities tab
    And Create a new Continuation Opportunity record type
    Then Fill all the mandatory details Stage "Opportunity,B,3" Region "Opportunity,D,3" Market "Opportunity,F,3" AccName "Opportunity,I,3" PreferredContact "Opportunity,H,3" Programme "Opportunity,S,2" VAT "Opportunity,T,2" Source "Opportunity,K,2"
    And Click on Save button
    Then User Logout from SVO Portal
    
    @SSEC_2327_02 @Sonica
  Scenario: User is able to verify Opportunity record type and Opportunity Owner for Classic user
  Given Access to SVO Portal as Classic Sales user with Username "LoginUsers,B,5" and password "LoginUsers,C,5"
    And User navigates to Opportunities tab
    And Create a new Continuation Opportunity record type
    Then Fill all the mandatory details Stage "Opportunity,B,3" Region "Opportunity,D,3" Market "Opportunity,F,3" AccName "Opportunity,I,3" PreferredContact "Opportunity,H,3" Programme "Opportunity,S,2" VAT "Opportunity,T,2" Source "Opportunity,K,2"
    And Click on Save button
    Then Verify Opportunity Record Type and Owner
    Then User Logout from SVO Portal
    
     @SSEC_2327_03 @Sonica
  Scenario: User is able to edit Vehicle Details sections in the Classic Continuation Opportunity record
Given Access to SVO Portal as Classic Sales user with Username "LoginUsers,B,5" and password "LoginUsers,C,5"
    And User navigates to Opportunities tab
    And Create a new Continuation Opportunity record type
    Then Fill all the mandatory details Stage "Opportunity,B,3" Region "Opportunity,D,3" Market "Opportunity,F,3" AccName "Opportunity,I,3" PreferredContact "Opportunity,H,3" Programme "Opportunity,S,2" VAT "Opportunity,T,2" Source "Opportunity,K,2"
    And Click on Save button
    Then Verify user is able to edit Vehicle section
    Then User Logout from SVO Portal  
    
    @SSEC_2327_04 @Sonica
  Scenario: User is able to edit Donor Details sections in the Classic Continuation Opportunity record
Given Access to SVO Portal as Classic Sales user with Username "LoginUsers,B,5" and password "LoginUsers,C,5"
    And User navigates to Opportunities tab
    And Create a new Continuation Opportunity record type
    Then Fill all the mandatory details Stage "Opportunity,B,3" Region "Opportunity,D,3" Market "Opportunity,F,3" AccName "Opportunity,I,3" PreferredContact "Opportunity,H,3" Programme "Opportunity,S,2" VAT "Opportunity,T,2" Source "Opportunity,K,2"
    And Click on Save button
    Then Verify user is able to edit Donor section
    Then User Logout from SVO Portal  
    
    @SSEC_2327_05 @Sonica
  Scenario: User is able to verify that Donor section is present after Vehicle section
Given Access to SVO Portal as Classic Sales user with Username "LoginUsers,B,5" and password "LoginUsers,C,5"
    And User navigates to Opportunities tab
    And Create a new Continuation Opportunity record type
    Then Fill all the mandatory details Stage "Opportunity,B,3" Region "Opportunity,D,3" Market "Opportunity,F,3" AccName "Opportunity,I,3" PreferredContact "Opportunity,H,3" Programme "Opportunity,S,2" VAT "Opportunity,T,2" Source "Opportunity,K,2"
    And Click on Save button
    Then Verify that Donor section is present after Vehicle section
    Then User Logout from SVO Portal  
    
     @SSEC_2327_06 @Sonica
  Scenario: User is able to Mark stage as Complete for Classic Opportunity record
Given Access to SVO Portal as Classic Sales user with Username "LoginUsers,B,5" and password "LoginUsers,C,5"
    And User navigates to Opportunities tab
    And Create a new Continuation Opportunity record type
    Then Fill all the mandatory details Stage "Opportunity,B,3" Region "Opportunity,D,3" Market "Opportunity,F,3" AccName "Opportunity,I,3" PreferredContact "Opportunity,H,3" Programme "Opportunity,S,2" VAT "Opportunity,T,2" Source "Opportunity,K,2"
    And Click on Save button
    Then Verify that user is able to Mark Stage as Complete
    Then User Logout from SVO Portal  
