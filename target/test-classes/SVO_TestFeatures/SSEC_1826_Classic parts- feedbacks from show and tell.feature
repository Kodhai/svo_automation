Feature: SVO Classic parts - Feedbacks from show and tell

  @SSEC_1826_ClassicParts_FeedbacksFromShowAndTell_01
  Scenario: User is able to add values to 'What's on your mind' text field under Additional details for Classic parts and technical case
    Given Access to SVO Portal as Classic Parts and technical user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    And User create new case with record type "Case_Lifecycle,B,2"
    Then User add values to what's on your mind field on Cases page
    Then User Logout from SVO Portal

  @SSEC_1826_ClassicParts_FeedbacksFromShowAndTell_02
  Scenario: User is able to add values to 'What's on your mind' text field under Additional details for Classic parts and technical case
    Given Access to SVO Portal as Classic Customer Service user with Username "LoginUsers,B,21" and Password "LoginUsers,C,21"
    And User create new case with record type "Case_Lifecycle,B,3"
    Then User edit values of what's on your mind field on Cases page
    Then User Logout from SVO Portal

  @SSEC_1826_ClassicParts_FeedbacksFromShowAndTell_03
  Scenario: User is able to add values to 'What's on your mind' text field under Additional details for Classic parts and technical case
    #Given Access to SVO Portal as Classic Customer Service user with Username "LoginUsers,B,21" and Password "LoginUsers,C,21"
    And User create new case with record type "Case_Lifecycle,B,3"
    Then User remove the values of what's on your mind field on Cases page
    Then User Logout from SVO Portal

  @SSEC_1826_ClassicParts_FeedbacksFromShowAndTell_04
  Scenario: Verify that the SLA timer is running between 8:00 and 16:00 for the Classic part and technical case created through web
    Given Access to the Classic Parts portal
    Then Navigate to Contact Us section
    Then Submit Contact Us form with classic parts and technical landrover enquiry Email "Case_Lifecycle,F,2" First name "Case_Lifecycle,G,2" Last name "Case_Lifecycle,H,2" Country "Case_Lifecycle,I,2" Phone number "Case_Lifecycle,J,2" VIN number "Case_Lifecycle,K,2"
    Given User Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    And Verifies Classic parts and technical case under parts and technical queue
    Then Verify that the SLA timer is running between 8:00 and 16:00 under milestones details on cases page
    Then User Logout from SVO Portal

  @SSEC_1826_ClassicParts_FeedbacksFromShowAndTell_05
  Scenario: Verify that the SLA timer is running between 8:00 and 16:00 for the Classic part and technical case created through web
    Given Access to the Classic Parts portal
    Then Navigate to Contact Us section
    Then Submit Contact Us form with classic customer service enquiry Email "Case_Lifecycle,F,2" First name "Case_Lifecycle,G,2" Last name "Case_Lifecycle,H,2" Country "Case_Lifecycle,I,2" Phone number "Case_Lifecycle,J,2" VIN number "Case_Lifecycle,K,2"
    Given User Access to SVO Portal as Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    And Verifies Classic customer service case under customer service queue
    Then Verify that the SLA timer is running between 8:00 and 16:00 under milestones details on cases page
    Then User Logout from SVO Portal

  @SSEC_1826_ClassicParts_FeedbacksFromShowAndTell_06
  Scenario: Verify that the case owner is set to 'Glen Parkes' by default after creating Classic parts and technical case on salesforce
    Given Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    And User create new case with record type "Case_Lifecycle,B,2"
    Then Verify that default case owner as 'Glen Parkes' on cases page
    Then User Logout from SVO Portal

  @SSEC_1826_ClassicParts_FeedbacksFromShowAndTell_07
  Scenario: Verify that the case owner is set to 'Glen Parkes' by default after creating Classic parts and technical case on salesforce
    Given Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    And User create new case without VIN number record type "Case_Lifecycle,B,2"
    Then Verify that VIN number is empty after new parts and technical case created
    Then User Logout from SVO Portal

  @SSEC_1826_ClassicParts_FeedbacksFromShowAndTell_08
  Scenario: Verify that email signature is present on emails page for Classic parts and technical case while sending an email to customer
    Given Access to mail account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    When Customer send an enquiry mail to user "Case_Lifecycle,F,2"
    Given User Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    And Verifies Classic parts and technical case under parts and technical queue
    Then User verifies that email signatute while sending an email to customer "LoginUsers,B,17" with Business unit "CustomerResponse,I,3" and subject and verify works legend stock link in email signature
    Then User Logout from SVO Portal

  @SSEC_1826_ClassicParts_FeedbacksFromShowAndTell_09
  Scenario: Verify that the case owner is not set to 'Glen Parkes' by default after creating Classic customer service case on salesforce
    Given Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    And User create new case with record type "Case_Lifecycle,B,2"
    Then Verify that default case owner is not updated to 'Glen Parkes' on cases page
    Then User Logout from SVO Portal

  @SSEC_1826_ClassicParts_FeedbacksFromShowAndTell_10
  Scenario: Verify that the SLA timer will not start if Classic customer service case created before 8:00 AM through web
    Given Access to the Classic Parts portal
    Then Navigate to Contact Us section
    Then Submit Contact Us form with classic customer service enquiry Email "Case_Lifecycle,F,2" First name "Case_Lifecycle,G,2" Last name "Case_Lifecycle,H,2" Country "Case_Lifecycle,I,2" Phone number "Case_Lifecycle,J,2" VIN number "Case_Lifecycle,K,2"
    Given User Access to SVO Portal as Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    And Verifies Classic customer service case under customer service queue
    Then Verify that the SLA timer will not start if Classic customer service case created before 8:00 AM
    Then User Logout from SVO Portal

  @SSEC_1826_ClassicParts_FeedbacksFromShowAndTell_11
  Scenario: Verify that email signature is present on emails page for Classic parts and technical case while sending an email to customer
    Given Access to mail account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    When Customer send an enquiry mail to user "Case_Lifecycle,F,2"
    Given User Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    And Verifies Classic parts and technical case under parts and technical queue
    Then Verify that the SLA timer will not start if Classic parts and technical case created after 16:00 PM
    Then User Logout from SVO Portal
