Feature: Script to edit details for created customer services case

  #User is able to create new case and logout
  @SSEC_1227_CaseCreation_1
  Scenario: User is able to create new case and logout
    Given Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User navigates to cases tab
    And User creates new Parts and Technical Record type case with mandatory fields like VIN
    Then User saves the case
    And User validates the new case created with the VIN
    And User logout from the portal

  #User is able to create new customer services case
  @SSEC_1227_CaseCreation_2
  Scenario: User is able to create new customer services case
    Given User login to SVO Portal by Classic Customer Service user with Username "LoginUsers, B, 21" and Password "LoginUsers, C, 21"
    When User navigates to cases tab
    And User creates new Customer services case with all mandatory fields
    Then User saves the case
    Then User validates new cust case
    And User logout from the portal

  #User is able to create multiple cases for same record type
  @SSEC_1227_CaseCreation_3
  Scenario: User is able to create multiple cases for same record type
    Given Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User navigates to cases tab
    And User creates new Parts and Technical Record type case with mandatory fields like VIN
    Then User saves and creates a new case with the same record type
    Then User validates the new parts and technical case
    And User logout from the portal

  #User is able to edit details of the created customer services case
  @SSEC_1227_CaseCreation_4
  Scenario: User is able to edit details of the created customer services case
   Given User login to SVO Portal by Classic Customer Service user with Username "LoginUsers, B, 21" and Password "LoginUsers, C, 21"
    When User navigates to cases tab
    And User creates new Customer services case with all mandatory fields
    Then User saves the case
    And User verifies that the case is editable
    Then User logout from the portal

  #User cannot create a new parts and technical record type without mandatory field
  @SSEC_1227_CaseCreation_5
  Scenario: User cannot create a new parts and technical record type without mandatory field
    Given Access to SVO Portal with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User navigates to cases tab
    And User creates new Parts and Technical Record type case
    Then User saves the case without filling mandatory field like VIN
    And User verifies that it is unable to save
    And User logout from the portal

  #User cannot create new case if case page is not available
  @SSEC_1227_CaseCreation_6
  Scenario: User cannot create new case if case page is not available
    Given Access to SVO Classic Operations portal with Username "LoginUsers,B,9" and password "LoginUsers,C,9"
    When User searches the Case page in searchbox
    Then User logout from the portal
	
  #User is not able to delete the created case for Customer services
  @SSEC_1227_CaseCreation_7
  Scenario: User is not able to delete the created case for Customer services
   Given User login to SVO Portal by Classic Customer Service user with Username "LoginUsers, B, 21" and Password "LoginUsers, C, 21"
    When User navigates to cases tab
    And User creates new Customer services case with all mandatory fields
    Then User saves the case
    And User verifies that the case cannot be deleted
    Then User logout from the portal
