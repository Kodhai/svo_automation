@SSEC_2150
Feature: Validate Booker 25 Amendments scripts

  @SSEC_2150_01 @Sonica
  Scenario: User is able to create Bespoke Commissioning request form for managing the reservation
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    And Navigate to Commissioning Requests
    When User clicks on New button
    And Select Bespoke record type
    And Fills the Start Date and End Date
    Then Click on Save button of Commissioning request
    Then User Logout from SVO Portal

  @SSEC_2150_02 @Sonica
  Scenario: User is able to create Bespoke Commissioning request form for managing the reservation from Opportunity tab
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User creates a Bespoke Opportunity with check cleared like Stage "Opportunity,B,3" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,I,3" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,3"
    When User navigates to Commissioning Request Tab
    When User clicks on New button
    And Select Bespoke record type
    And Fills the Start Date and End Date
    Then Click on Save button of Commissioning request
    Then User Logout from SVO Portal

  @SSEC_2150_03 @Sonica
  Scenario: User is able to create Private Commissioning request form for managing the reservation
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    And Navigate to Commissioning Requests
    When User clicks on New button
    And Select Private Office record type
    And Fills the Start Date and End Date Duration TourType "CommissioningRequests,A,2"
    Then Click on Save button of Commissioning request
    Then User Logout from SVO Portal

  @SSEC_2150_04 @Sonica
  Scenario: User is able to tick on all the checkboxes for Private Office commissioning request
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    And Navigate to Commissioning Requests
    When User clicks on New button
    And Select Private Office record type
    And Fills the Start Date and End Date Duration TourType "CommissioningRequests,A,2"
    And Check all the checkboxes of Commissioning Request form
    Then Click on Save button of Commissioning request
    Then User Logout from SVO Portal

  @SSEC_2150_05 @Sonica
  Scenario: User is able to submit the commissioning request form
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    And Navigate to Commissioning Requests
    When User clicks on New button
    And Select Bespoke record type
    And Fills the Start Date and End Date
    Then Click on Save button of Commissioning request
    And Clicks on Submit button
    Then User Logout from SVO Portal

  @SSEC_2150_06 @Sonica
  Scenario: User is able to create Reservation for Bespoke Commissioning request
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    And Navigate to Commissioning Requests
    When User clicks on New button
    And Select Bespoke record type
    And Fills the Start Date and End Date
    Then Click on Save button of Commissioning request
    And User creates a new Reservation
    Then Fills Reservation details such as PrimaryContact "CommissioningRequests,B,2" MeetingType "CommissioningRequests,C,2"
    Then User Logout from SVO Portal

  @SSEC_2150_07 @Sonica
  Scenario: User is able to edit the Reservation Status as Booked and save it
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    And Navigate to Commissioning Requests
    When User clicks on New button
    And Select Bespoke record type
    And Fills the Start Date and End Date
    Then Click on Save button of Commissioning request
    And User creates a new Reservation
    Then Fills Reservation details such as PrimaryContact "CommissioningRequests,B,2" MeetingType "CommissioningRequests,C,2"
    And User edits Status as booked "CommissioningRequests,D,2" and Host "CommissioningRequests,E,2" for Reservaton and Save it
    Then User Logout from SVO Portal
    
     @SSEC_2150_08 @Sonica
  Scenario: Verify that once the Reservation status is changed to Booked, Commissioning Status is also changed to Booked
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    And Navigate to Commissioning Requests
    When User clicks on New button
    And Select Bespoke record type
    And Fills the Start Date and End Date
    Then Click on Save button of Commissioning request
    And User creates a new Reservation
    Then Fills Reservation details such as PrimaryContact "CommissioningRequests,B,2" MeetingType "CommissioningRequests,C,2"
    And User edits Status as booked "CommissioningRequests,D,2" and Host "CommissioningRequests,E,2" for Reservaton and Save it
    Then Verify that once the Reservation status is changed to Booked, Commissioning Status is also changed to Booked
    Then User Logout from SVO Portal
    
