Feature: Validations for Email template for cases

  #User is able to verify '3D Secure' Email Template for Customer Service cases created on Salesforce
  @SSEC_1233_CaseEmailTemplate_001 @Automation_SSEC-1765
  Scenario: User is able to verify '3D Secure' Email Template for Customer Service cases created on Salesforce
    Given Access to salesforce Portal as an Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When Navigate to Cases Tab
    And Create a new case on SVO of record type "CaseCreation,C,2" by linking case with existing Account "Opportunity,I,2" and Contact "Opportunity,J,2" details
    Then User Navigate to Activity tab
    And Verify and send an email to customer "LoginUsers,B,17" with template "CaseCreation,N,2" on compose email section
    Then User Logout from an SVO Portal
    When Login to the email account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    And Verify that customer receives an email with the applied template "CaseCreation,N,2"
    Then User logout from the user email account

  #User is able to verify 'Parts Enquiry' Email Template for Classic Parts and Technical case created through web form
  @SSEC_1233_CaseEmailTemplate_002 @Automation_SSEC-1766
  Scenario: User is able to verify 'Parts Enquiry' Email Template for Classic Parts and Technical case created through web form
    Given Login to the Classic Parts "CaseCreation,L,2" portal
    Then User Navigates to Contact Us section	
    Then User creates an Parts and Technical Case from web form with Email "CaseCreation,A,2" First name "CaseCreation,G,2" Last name "CaseCreation,H,2" Country "CaseCreation,I,2" Phone number "CaseCreation,K,2" VIN number "CaseCreation,J,2"
    Then Verify that user receives an outlook email notification on Parts and Technical case creation
    Given Login to the salesforce Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When Navigate to Cases Tab
    And User Verify that Classic Parts and Technical case is automatically created on SVO Portal
    Then User Navigate to Activity tab
    And Verify and send an email to customer "LoginUsers,B,17" with template "CaseCreation,N,4" on compose email section
    Then User Logout from an SVO Portal
    When Login to the email account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    And Verify that customer receives an email with the applied template "CaseCreation,N,2"
    Then User logout from the user email account

  #User is able to verify 'Returns' Email Template for Customer Service case created on Salesforce
  @SSEC_1233_CaseEmailTemplate_003 @Automation_SSEC-1767
  Scenario: User is able to verify 'Returns' Email Template for Customer Service case created on Salesforce
    Given Access to salesforce Portal as an Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When Navigate to Cases Tab
    And Create a new case on SVO of record type "CaseCreation,C,2" by linking case with existing Account "Opportunity,I,2" and Contact "Opportunity,J,2" details
    Then User Navigate to Activity tab
    And Verify and send an email to customer "LoginUsers,B,17" with template "CaseCreation,N,3" on compose email section
    Then User Logout from an SVO Portal
    When Login to the email account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    And Verify that customer receives an email with the applied template "CaseCreation,N,3"
    Then User logout from the user email account
    
  #User is able to verify 'Returns 2' Email Template for Customer Service case created on Salesforce
  @SSEC_1233_CaseEmailTemplate_004 @Automation_SSEC-1768
  Scenario: User is able to verify 'Returns 2' Email Template for Customer Service case created on Salesforce
    Given Access to salesforce Portal as an Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When Navigate to Cases Tab
    And Create a new case on SVO of record type "CaseCreation,C,2" by linking case with existing Account "Opportunity,I,2" and Contact "Opportunity,J,2" details
    Then User Navigate to Activity tab
    And Verify and send an email to customer "LoginUsers,B,17" with template "CaseCreation,N,5" on compose email section
    Then User Logout from an SVO Portal
    When Login to the email account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    And Verify that customer receives an email with the applied template "CaseCreation,N,5"
    Then User logout from the user email account
    
  #User is able to edit the 'Parts Enquiry' Email template for Classic Parts and Technical Case created through email
  @SSEC_1233_CaseEmailTemplate_005 @Automation_SSEC-1769
  Scenario: User is able to edit the 'Parts Enquiry' Email template for Classic Parts and Technical Case created through email
    Given Login to the Classic Parts "CaseCreation,L,2" portal
    Then User Navigates to Contact Us section	
    Then User creates an Parts and Technical Case from web form with Email "CaseCreation,A,2" First name "CaseCreation,G,2" Last name "CaseCreation,H,2" Country "CaseCreation,I,2" Phone number "CaseCreation,K,2" VIN number "CaseCreation,J,2"
    Then Verify that user receives an outlook email notification on Parts and Technical case creation
    Given Login to the salesforce Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When Navigate to Cases Tab
    And User Verify that Classic Parts and Technical case is automatically created on SVO Portal
    Then User Navigate to Activity tab
    And Verify and send an email to customer "LoginUsers,B,17" by editing template "CaseCreation,N,4" on compose email section
    Then User Logout from an SVO Portal	
    When Login to the email account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    And Verify that customer receives an email with the applied template "CaseCreation,N,2"
    Then User logout from the user email account
    
  #User is able to remove inserted Email template for Classic customer service case
  @SSEC_1233_CaseEmailTemplate_006 @Automation_SSEC-1770
  Scenario: User is able to remove inserted Email template for Classic customer service case
    Given Access to salesforce Portal as an Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When Navigate to Cases Tab
    And Create a new case on SVO of record type "CaseCreation,C,2" by linking case with existing Account "Opportunity,I,2" and Contact "Opportunity,J,2" details
    Then User Navigate to Activity tab
    And Send an email to customer "LoginUsers,B,17" after removing email template "CaseCreation,N,5" on compose email section
    Then User Logout from an SVO Portal
    	
  #User is not able to send an Email with 'Parts Enquiry' template without mandatory details of email
  @SSEC_1233_CaseEmailTemplate_007 @Automation_SSEC-1771
  Scenario: User is not able to send an Email with 'Parts Enquiry' template without mandatory details of email
    Given Login to the Classic Parts "CaseCreation,L,2" portal
    Then User Navigates to Contact Us section	
    Then User creates an Parts and Technical Case from web form with Email "CaseCreation,A,2" First name "CaseCreation,G,2" Last name "CaseCreation,H,2" Country "CaseCreation,I,2" Phone number "CaseCreation,K,2" VIN number "CaseCreation,J,2"
    Then Verify that user receives an outlook email notification on Parts and Technical case creation
    Given Login to the salesforce Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When Navigate to Cases Tab
    And User Verify that Classic Parts and Technical case is automatically created on SVO Portal
    Then User Navigate to Activity tab
    And Verify user is not able to send an email without madatory fields of email to customer "LoginUsers,B,17" with template "CaseCreation,N,4" on compose email section
    Then User Logout from an SVO Portal	
   
 
