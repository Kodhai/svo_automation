Feature: SVO Classic parts - Define SLA

  #User is able to receive an automatic email on Monday if Customer service enquiry raised on Friday(Before 5pm)
  @SSEC_1231_ClassicParts_DefineSLA_01
  Scenario: User is able to receive an automatic email on Monday if Customer service enquiry raised on Friday(Before 5pm)
    Given Access to SVO Portal as Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    And User create new case to define SLA with record type "Case_Lifecycle,B,2"
    Then Collect case number on Friday before five PM
    Then Verify that SLA is breached on salesforce
    Then User Logout from SVO Portal

  #User is able to receive an automatic email on Tuesday if Parts and Technical enquiry raised on Friday(After 5pm)
  @SSEC_1231_ClassicParts_DefineSLA_02
  Scenario: User is able to receive an automatic email on Tuesday if Parts and Technical enquiry raised on Friday(After 5pm)
    Given Access to SVO Portal as Classic Parts and technical user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    And User create new case with record type "Case_Lifecycle,B,2"
    Then Collect case number on Friday after five PM
    Then Verify that SLA is breached on salesforce
    Then User Logout from SVO Portal

  #User is able to receive an automatic email on Tuesday if Parts and Technical enquiry raised on Saturday(Anytime)
  @SSEC_1231_ClassicParts_DefineSLA_03
  Scenario: User is able to receive an automatic email on Tuesday if Parts and Technical enquiry raised on Saturday(Anytime)
    Given Access to mail account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    When Customer send an enquiry mail to user "Case_Lifecycle,F,2"
    Given User Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    And Verifies Classic parts and technical case under parts and technical queue
    And Collect case number on Saturday anytime
    Then Verify that SLA is breached on salesforce
    Then User Logout from SVO Portal

  #User is able to receive an automatic email on Tuesday if Customer Service enquiry raised on Sunday(Anytime)
  @SSEC_1231_ClassicParts_DefineSLA_04
  Scenario: User is able to receive an automatic email on Tuesday if Customer Service enquiry raised on Sunday(Anytime)
    Given Access to SVO Portal as Classic Customer Service user with Username "LoginUsers,B,21" and Password "LoginUsers,C,21"
    And User create new case with record type "Case_Lifecycle,B,3"
    And Collect case number on Sunday anytime
    Then Verify that SLA is breached on salesforce
    Then User Logout from SVO Portal

  #User is able to receive an automatic email within 24 hours if Parts and Technical enquiry raised on Weekday(Before 5PM)
  @SSEC_1231_ClassicParts_DefineSLA_05
  Scenario: User is able to receive an automatic email within 24 hours if Parts and Technical enquiry raised on Weekday(Before 5PM)
    Given Access to SVO Portal as Classic Parts and technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    And User create new case to define SLA with record type "Case_Lifecycle,B,2"
    Then Collect case number on Weekday before five PM
    Then Verify that SLA is breached on salesforce
    Then User Logout from SVO Portal

  #User is able to receive an automatic email if Customer Service enquiry raised on Weekday(After 5PM)
  @SSEC_1231_ClassicParts_DefineSLA_06
  Scenario: User is able to receive an automatic email if Customer Service enquiry raised on Weekday(After 5PM)
    Given Access to SVO Portal as Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    And User create new case with record type "Case_Lifecycle,B,2"
    Then Collect case number on Weekday after five PM
    Then Verify that SLA is breached on salesforce
    Then User Logout from SVO Portal

  #User is not able to receive an automatic email if Parts and Technical case is in closed state.
  @SSEC_1231_ClassicParts_DefineSLA_07
  Scenario: User is not able to receive an automatic email if Parts and Technical case is in closed state.
    Given Access to SVO Portal as Classic Parts and technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    And User create new case with record type "Case_Lifecycle,B,2"
    Then User close the case with status "Case_Lifecycle,C,2"
    And Collect case number which is in closed status
    Then Verify that SLA is breached on salesforce
    Then User Logout from SVO Portal

  #User is able to receive an automatic email if Parts and Technical enquiry raised through web on Friday(Before 5pm)
  @SSEC_1231_ClassicParts_DefineSLA_08
  Scenario: User is able to receive an automatic email if Parts and Technical enquiry raised through web on Friday(Before 5pm)
    Given Access to the Classic Parts portal
    Then Submit Contact Us form with classic customer service enquiry Email "Case_Lifecycle,F,2" First name "Case_Lifecycle,G,2" Last name "Case_Lifecycle,H,2" Country "Case_Lifecycle,I,2" Phone number "Case_Lifecycle,J,2" VIN number "Case_Lifecycle,K,2"
    Given User Access to SVO Portal as Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    And Verifies Classic customer service case under customer service queue
    And Collect case number created via web on Friday before five PM
    Then Verify that SLA is breached on salesforce
    Then User Logout from SVO Portal

  #User is able to receive an automatic email on Tuesday if Parts and Technical enquiry raised on Friday(After 5pm)
  @SSEC_1231_ClassicParts_DefineSLA_09
  Scenario: User is able to receive an automatic email on Tuesday if Parts and Technical enquiry raised on Friday(After 5pm)
    Given Access to the Classic Parts portal
    Then Submit Contact Us form with classic parts and technical landrover enquiry Email "Case_Lifecycle,F,2" First name "Case_Lifecycle,G,2" Last name "Case_Lifecycle,H,2" Country "Case_Lifecycle,I,2" Phone number "Case_Lifecycle,J,2" VIN number "Case_Lifecycle,K,2"
    Given User Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    And Verifies Classic parts and technical case under parts and technical queue
    And Collect case number created via web on Friday after five PM
    Then Verify that SLA is breached on salesforce
    Then User Logout from SVO Portal
