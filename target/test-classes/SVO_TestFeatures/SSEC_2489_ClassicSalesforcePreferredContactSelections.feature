@SSEC_2489
Feature: Validate Classic Salesforce Preferred Contact Selections
  I want to use this template for my feature file

  @SSEC_2489_01
  Scenario: User is able create a new Enquiry for the Salesforce user
    Given Access to SVO Portal as Classic Sales user with Username "LoginUsers,B,5" and password "LoginUsers,C,5"
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,2"
    And Enter the required details for the Enquiry creation
    Then Save the new Enquiry
    Then Logout from the SVO Portal

  @SSEC_2489_02
  Scenario: User is able to add Preferred Contact and Email in the Enquiry form
    Given Access to SVO Portal as Classic Sales user with Username "LoginUsers,B,5" and password "LoginUsers,C,5"
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,2"
    And Enter the required details for the Enquiry creation
    Then User selects Preferred Contact details from the Potential Matches coming after entering the Email Address "Enquiry,N,2"
    Then Save the new Enquiry
    Then Logout from the SVO Portal

  @SSEC_2489_03
  Scenario: User is able to edit the Preferred Contact and Email in the Enquiry form
    Given Access to SVO Portal as Classic Sales user with Username "LoginUsers,B,5" and password "LoginUsers,C,5"
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,2"
    And Enter the required details for the Enquiry creation
    Then User selects Preferred Contact details from the Potential Matches coming after entering the Email Address "Enquiry,N,2"
    Then Save the new Enquiry
    Then Click on Edit button of Preferred Contact and select the option from drop down
    And Click on Edit button of Email and fill it
    Then Save the new Enquiry
    Then Logout from the SVO Portal

  @SSEC_2489_04
  Scenario: User is able to create a new Classic Service Enquiry for the Salesforce user
    Given Access to SVO Portal as classic service Username user with Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    Then User navigates to the Enquiries tab
    Then User Creates a New Enquiry with Record Type "Enquiry,A,5"
    Then Create a new enquiry with title "Enquiry,R,2" source KMI "Enquiry,AB,2"
    Then Save the new Enquiry
    Then User Logout from SVO Portal

  @SSEC_2489_05
  Scenario: User is able to mark Enquiry Status as Complete
    Given Access to SVO Portal as classic service Username user with Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    Then User navigates to the Enquiries tab
    Then User Creates a New Enquiry with Record Type "Enquiry,A,5"
    Then Create a new enquiry with title "Enquiry,R,2" source KMI "Enquiry,AB,2"
    And User adds the preferred contact details
    Then Save the new Enquiry
    And Log a call to the Enquiry with client "Enquiry,H,2"
    Then User mark Enquiry status as complete
    Then User Logout from SVO Portal

  @SSEC_2489_06
  Scenario: User is able to verify the Person Account in the Preferred Contact drop down
    Given Access to SVO Portal as classic service Username user with Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    Then User navigates to the Enquiries tab
    Then User Creates a New Enquiry with Record Type "Enquiry,A,5"
    Then Create a new enquiry with title "Enquiry,R,2" source KMI "Enquiry,AB,2"
    And User adds the preferred contact details
    Then Save the new Enquiry
    Then User Logout from SVO Portal
