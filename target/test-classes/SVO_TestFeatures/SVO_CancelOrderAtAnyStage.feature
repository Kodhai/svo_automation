Feature: SVO Cancel Order at any stage Validations

  @SVO_Cancel_Order_01 @SSEC-1044 @SVO_CancelOrder_Batch
  Scenario: User is able to cancel the order from 'Committed to Build stage' on Orders page of an opportunity
    Given Access to SVO bespoke operation Portal with Username "LoginUsers,B,8" and Password "LoginUsers,C,8"
    Then User navigates to the Opportunity tab
    And User create a SVO Bespoke Opportunity with check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User moves an opportunity to Design Brief and Quote stage
    Then User moves an opportunity to Contract Signed stage from Design Brief and Quote
    And User moves an Oppotunity to Order Placed stage after accepting the Quote "Opportunity,J,3"
    And User move the order to committed to build stage
    Then User cancel the order with outcome stage "CancelOrder,A,3" on Orders page
    Then User navigate back to Opportunity page
    And User verifies the opportunity stage and lost reason on opportunity page
    Then Logout from the Special vehicle operations Portal

  @SVO_Cancel_Order_02 @SSEC-1045 @SVO_CancelOrder_Batch
  Scenario: User is able to cancel the order from 'Built stage' on Orders page of an opportunity
    Given Access to SVO Portal by bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    #Given laterally Access SVO Portal by user name "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User create a SVO Bespoke Opportunity with check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User is able to verify the Restricted Party Screening under details section of an opportunity
    Then User moves an opportunity to Design Brief and Quote stage
    Then User moves an opportunity to Contract Signed stage from Design Brief and Quote
    And User moves an Oppotunity to Order Placed stage after accepting the Quote "Opportunity,J,3"
    And User move the order to Built stage
    Then User cancel the order with outcome stage "CancelOrder,A,4" on Orders page
    Then User navigate back to Opportunity page
    And User verifies the opportunity stage and lost reason on opportunity page
    Then Logout from the Special vehicle operations Portal

  @SVO_Cancel_Order_03 @SSEC-1046 @SVO_CancelOrder_Batch
  Scenario: User is able to cancel the order at 'Accepted by Sales stage' on Orders page of an opportunity
    Given Access to SVO bespoke operation Portal with Username "LoginUsers,B,8" and Password "LoginUsers,C,8"
    #Given laterally Access SVO Portal by user name "LoginUsers,B,8" and Password "LoginUsers,C,8"
    Then User navigates to the Opportunity tab
    And User create a SVO Bespoke Opportunity with check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User is able to verify the Restricted Party Screening under details section of an opportunity
    Then User moves an opportunity to Design Brief and Quote stage
    Then User moves an opportunity to Contract Signed stage from Design Brief and Quote
    And User moves an Oppotunity to Order Placed stage after accepting the Quote "Opportunity,J,2"
    And User move the order to Accepted by sales stage
    Then User cancel the order with outcome stage "CancelOrder,A,2" on Orders page
    And Verify that order status is displayed with outcome as cancelled by customer
    Then User navigate back to Opportunity page
    And User verifies the opportunity stage and lost reason on opportunity page
    Then Logout from the Special vehicle operations Portal

  @SVO_Cancel_Order_04 @SSEC-1047 @SVO_CancelOrder_Batch
  Scenario: User is able to cancel the order at 'Retailer stage' on Orders page of an opportunity
    Given Access to SVO Portal by bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    #Given laterally Access SVO Portal by user name "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User create a SVO Bespoke Opportunity with check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User is able to verify the Restricted Party Screening under details section of an opportunity
    Then User moves an opportunity to Design Brief and Quote stage
    Then User moves an opportunity to Contract Signed stage from Design Brief and Quote
    And User moves an Oppotunity to Order Placed stage after accepting the Quote "Opportunity,J,2"
    And User move the order to Retailed stage
    Then User cancel the order with outcome stage "CancelOrder,A,2" on Orders page
    And Verify that order status is displayed with outcome as cancelled by customer
    Then User navigate back to Opportunity page
    And User verifies the opportunity stage and lost reason on opportunity page
    Then Logout from the Special vehicle operations Portal

  @SVO_Cancel_Order_05 @SSEC-1070 @SVO_CancelOrder_Batch
  Scenario: User is able to retail the order at 'Committed to Build stage' on Orders page of an opportunity
    Given Access to SVO Portal by bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    #Given laterally Access SVO Portal by user name "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User create a SVO Bespoke Opportunity with check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User moves an opportunity to Design Brief and Quote stage
    Then User moves an opportunity to Contract Signed stage from Design Brief and Quote
    And User moves an Oppotunity to Order Placed stage after accepting the Quote "Opportunity,J,3"
    And User move the order to committed to build stage
    Then Edit the order status to "Outcome" and outcome as "CancelOrder,A,5"
    Then User navigate back to Opportunity page
    And User verifies the opportunity stage and lost reason on opportunity page
    Then Logout from the Special vehicle operations Portal

  @SVO_Cancel_Order_06 @SSEC-1075 @SVO_CancelOrder_Batch @SVO_CancelOrder_Batch_ReExecution
  Scenario: User is able to retail the order at 'Built stage' on Orders page of an opportunity
    Given Access to SVO bespoke operation Portal with Username "LoginUsers,B,8" and Password "LoginUsers,C,8"
    #Given laterally Access SVO Portal by user name "LoginUsers,B,8" and Password "LoginUsers,C,8"
    Then User navigates to the Opportunity tab
    And User create a SVO Bespoke Opportunity with check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User moves an opportunity to Design Brief and Quote stage
    Then User moves an opportunity to Contract Signed stage from Design Brief and Quote
    And User moves an Oppotunity to Order Placed stage after accepting the Quote "Opportunity,J,3"
    And User move the order to Built stage
    Then Edit the order status to "Outcome" and outcome as "CancelOrder,A,5"
    Then User navigate back to Opportunity page
    And User verifies the opportunity stage and lost reason on opportunity page
    Then Logout from the Special vehicle operations Portal

  @SVO_Cancel_Order_07 @SSEC-1076 @SVO_CancelOrder_Batch
  Scenario: User is able to retail the order at 'Accepted by Sales stage' on Orders page of an opportunity
    Given Access to SVO Portal by bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    #Given laterally Access SVO Portal by user name "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User create a SVO Bespoke Opportunity with check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User moves an opportunity to Design Brief and Quote stage
    Then User moves an opportunity to Contract Signed stage from Design Brief and Quote
    And User moves an Oppotunity to Order Placed stage after accepting the Quote "Opportunity,J,3"
    And User move the order to Accepted by sales stage
    Then Edit the order status to "Outcome" and outcome as "CancelOrder,A,5"
    Then User navigate back to Opportunity page
    And User verifies the opportunity stage and lost reason on opportunity page
    Then Logout from the Special vehicle operations Portal

  @SVO_Cancel_Order_08 @SSEC-1072 @SVO_CancelOrder_Batch
  Scenario: User is able to retail the order at 'Retailer stage' on Orders page of an opportunity
    Given Access to SVO bespoke operation Portal with Username "LoginUsers,B,8" and Password "LoginUsers,C,8"
    #Given laterally Access SVO Portal by user name "LoginUsers,B,8" and Password "LoginUsers,C,8"
    Then User navigates to the Opportunity tab
    And User create a SVO Bespoke Opportunity with check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User is able to verify the Restricted Party Screening under details section of an opportunity
    Then User moves an opportunity to Design Brief and Quote stage
    Then User moves an opportunity to Contract Signed stage from Design Brief and Quote
    And User moves an Oppotunity to Order Placed stage after accepting the Quote "Opportunity,J,3"
    And User move the order to Retailed stage
    Then User Retail the order with outcome stage "CancelOrder,A,5" on Orders page
    Then User navigate back to Opportunity page
    And User verifies the opportunity stage and lost reason on opportunity page
    Then Logout from the Special vehicle operations Portal

  @SVO_Cancel_Order_09 @SSEC-1073 @SVO_CancelOrder_Batch
  Scenario: User is not able to cancel the order once the order is retailed on Orders page
    Given Access to SVO Portal by bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    #Given laterally Access SVO Portal by user name "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User create a SVO Bespoke Opportunity with check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User is able to verify the Restricted Party Screening under details section of an opportunity
    Then User moves an opportunity to Design Brief and Quote stage
    Then User moves an opportunity to Contract Signed stage from Design Brief and Quote
    And User moves an Oppotunity to Order Placed stage after accepting the Quote "Opportunity,J,3"
    And User move the order to Retailed stage
    Then User Retail the order with outcome stage "CancelOrder,A,5" on Orders page
    And User is not able to cancel the order once the order is retailed on Orders page with outcome stage "CancelOrder,A,3"
    Then Logout from the Special vehicle operations Portal

  @SVO_Cancel_Order_10 @SSEC-1074 @SVO_CancelOrder_Batch @SVO_CancelOrder_Batch_ReExecution
  Scenario: The Outcome field on Orders page is disabled, if the Order Status is selected other than ‘Outcome’ for any order
    Given Access to SVO bespoke operation Portal with Username "LoginUsers,B,8" and Password "LoginUsers,C,8"
    #Given laterally Access SVO Portal by user name "LoginUsers,B,8" and Password "LoginUsers,C,8"
    Then User navigates to the Opportunity tab
    And User create a SVO Bespoke Opportunity with check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User is able to verify the Restricted Party Screening under details section of an opportunity
    Then User moves an opportunity to Design Brief and Quote stage
    Then User moves an opportunity to Contract Signed stage from Design Brief and Quote
    And User moves an Oppotunity to Order Placed stage after accepting the Quote "Opportunity,J,2"
    Then Verify that user is not able to retail the order without moving an order to outcome stage
    Then Logout from the Special vehicle operations Portal

  @SVO_Cancel_Order_11 @SSEC-1050 @SVO_CancelOrder_Batch
  Scenario: User is not able to cancel the order if design brief quote is not accepted
    Given Access to SVO bespoke operation Portal with Username "LoginUsers,B,8" and Password "LoginUsers,C,8"
    #Given laterally Access SVO Portal by user name "LoginUsers,B,8" and Password "LoginUsers,C,8"
    Then User navigates to the Opportunity tab
    And User create a SVO Bespoke Opportunity with check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User is able to verify the Restricted Party Screening under details section of an opportunity
    Then User moves an opportunity to Design Brief and Quote stage
    And User create new design brief quote for an opportunity "Opportunity,J,2"
    Then Verify that no order is placed without accepting quote
    Then Logout from the Special vehicle operations Portal

  @SVO_Cancel_Order_12 @SSEC-1051 @SVO_CancelOrder_Batch
  Scenario: User is not able to move an order to Created stage from Outcome stage on order page
    Given Access to SVO Portal by bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    #Given laterally Access SVO Portal by user name "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User create a SVO Bespoke Opportunity with check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User is able to verify the Restricted Party Screening under details section of an opportunity
    Then User moves an opportunity to Design Brief and Quote stage
    Then User moves an opportunity to Contract Signed stage from Design Brief and Quote
    And User moves an Oppotunity to Order Placed stage after accepting the Quote "Opportunity,J,2"
    Then User cancel the order with outcome stage "CancelOrder,A,2" on Orders page
    And Verify that user is not able to move an order to Created stage from Outcome stage
    Then Logout from the Special vehicle operations Portal

  @SVO_Cancel_Order_13 @SSEC-1079 @SVO_CancelOrder_Batch @SVO_CancelOrder_Batch_ReExecution
  Scenario: User is not able to Cancelled or Retailed an Order from orders page without having Common Order Number
    Given Access to SVO bespoke operation Portal with Username "LoginUsers,B,8" and Password "LoginUsers,C,8"
    #Given laterally Access SVO Portal by user name "LoginUsers,B,8" and Password "LoginUsers,C,8"
    Then User navigates to the Opportunity tab
    And User create a SVO Bespoke Opportunity with check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User is able to verify the Restricted Party Screening under details section of an opportunity
    Then User moves an opportunity to Design Brief and Quote stage
    Then Create a quote with Retailer Contact "Opportunity,J,2" and accept the created quote
    Then Edit the order status to "Outcome" and outcome as "CancelOrder,A,2"
    Then Verify the error message when user is not able to cancel the order
    Then Logout from the Special vehicle operations Portal

  @SVO_Cancel_Order_14 @SSEC-1080 @SVO_CancelOrder_Batch @SVO_CancelOrder_Batch_ReExecution
  Scenario: User is not able to retailed the order once the order is cancelled on Orders page
    Given Access to SVO Portal by bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    #Given laterally Access SVO Portal by user name "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User create a SVO Bespoke Opportunity with check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User is able to verify the Restricted Party Screening under details section of an opportunity
    Then User moves an opportunity to Design Brief and Quote stage
    Then User moves an opportunity to Contract Signed stage from Design Brief and Quote
    And User moves an Oppotunity to Order Placed stage after accepting the Quote "Opportunity,J,2"
    And User move the order to Retailed stage
    Then User cancel the order with outcome stage "CancelOrder,A,2" on Orders page
    And User is not able to retail the order once the order is cancelled on Orders page with outcome stage "CancelOrder,A,5"
    Then Logout from the Special vehicle operations Portal

  @SVO_Cancel_Order_15 @SSEC-1078 @SVO_CancelOrder_Batch
  Scenario: User is not able to cancel the order at 'Committed to Build stage' on Orders page of an classic opportunity
    Given Access to SVO Portal as Classic Admin user with Username "LoginUsers,B,13" and Password "LoginUsers,C,13"
    #Given laterally Access SVO Portal by user name "LoginUsers,B,13" and Password "LoginUsers,C,13"
    When User navigates to an Opportunity tab
    And User creates new Classic Service Opportunity with mandatory fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,7" Region "Opportunity,D,2" Account "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then Move the opportunity from Qualified to Order Placed stage
    Then Navigate to Orders and verify user is not able to view Committed to Build stage
    Then Logout from the Special vehicle operations Portal
