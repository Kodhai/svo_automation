Feature: update hand of drive details from enquiry page validations

  @UpdateHandofDriveFieldsFromEnquiryPage_01 @Batch @SSEC-
  Scenario: User is able to update hand of drive details of active KMI which is created from enquiry page
    Given Access to SVO Portal as classic Sales user with Username "LoginUsers,B,5" and Password "LoginUsers,C,5"
    Then User navigates to the Enquiries tab
    And User creates an Enquiry with record type "EnquiryLostReason,A,2"
    Then User edit enquiry to add new KMI
    Then User create new active KMI with all fields from enquiry page Contact details "Opportunity,H,2" Hand of Drive "KMIs,C,2" Product Offering "KMIs,B,7" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    And User views Hand of Drive details from All Active KMIs list view
    Then User updates hand of drive details on KMI page Hand of drive "KMIs,C,3"
    Then Logout from SVO Portal

  @UpdateHandofDriveFieldsFromEnquiryPage_02 @Batch @SSEC-
  Scenario: User is able to update hand of drive details of Inactive KMI which is created from enquiry page
    Given Access to SVO Portal as classic Sales user with Username "LoginUsers,B,5" and Password "LoginUsers,C,5"
    Then User navigates to the Enquiries tab
    And User creates an Enquiry with record type "EnquiryLostReason,A,2"
    Then User edit enquiry to add new KMI
    Then User create new Inactive KMI with all fields from enquiry page Contact details "Opportunity,H,2" Hand of Drive "KMIs,C,2" Product Offering "KMIs,B,7" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    And User views Hand of Drive details from All Inactive KMIs list view
    Then User updates hand of drive details on KMI page Hand of drive "KMIs,C,3"
    Then Logout from SVO Portal

  @UpdateHandofDriveFieldsFromEnquiryPage_03 @Batch @SSEC-
  Scenario: User is able to delete hand of drive details of KMI which is created from enquiry page
    Given Access to SVO Portal as Classic Service user with Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    Then User navigates to the Enquiries tab
    And User creates an Enquiry with record type "EnquiryLostReason,A,3"
    Then User edit enquiry to add new KMI
    Then User create new active KMI with all fields from enquiry page Contact details "Opportunity,H,2" Hand of Drive "KMIs,C,2" Product Offering "KMIs,B,7" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    And User views Hand of Drive details from All Active KMIs list view
    Then User removes hand of drive details on KMI page Hand of drive "KMIs,C,3"
    Then Logout from SVO Portal

  @UpdateHandofDriveFieldsFromEnquiryPage_04 @Batch @SSEC-
  Scenario: User is not able to create new KMI from enquiry page
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Enquiries tab
    And User creates an Enquiry with record type "EnquiryLostReason,A,4"
    Then User not able to create new KMI from enquiry page
    Then Logout from SVO Portal

  @UpdateHandofDriveFieldsFromEnquiryPage_05 @Batch @SSEC-
  Scenario: User is not able to view enquiry tab to create new KMI from enquiry page
    Given Access to SVO Portal as Classic Operations user with Username "LoginUsers,B,10" and Password "LoginUsers,C,10"
    Then Verify that enquiries tab is not available on Homepage
    Then Logout from SVO Portal

  @UpdateHandofDriveFieldsFromEnquiryPage_06 @Batch @SSEC-
  Scenario: User is not able view KMI tab on SVO Home page
    Given Access to SVO Portal by private office user with Username "LoginUsers,B,4" and Password "LoginUsers,C,4"
    Then Verify that KMI tab is not available on Homepage
    Then Logout from SVO Portal
    
    @UpdateHandofDriveFieldsFromEnquiryPage_07 @Batch @SSEC-
  Scenario: User is able to select hand of drive details on KMI page
    Given Access to SVO Portal as Classic Service user with Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    Then User navigates to the Enquiries tab
    And User creates an Enquiry with record type "EnquiryLostReason,A,3"
    Then User edit enquiry to add new KMI
    Then User create new KMI without hand of drive all fields from enquiry page Contact details "Opportunity,H,2" Hand of Drive "KMIs,C,2" Product Offering "KMIs,B,7" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    And User views Hand of Drive details from All Active KMIs list view
    Then User updates hand of drive details on KMI page Hand of drive "KMIs,C,3"
    Then Logout from SVO Portal
    
    @UpdateHandofDriveFieldsFromEnquiryPage_08 @Batch @SSEC-
  Scenario: User is able to create new KMI from closed lost enquiry
    Given Access to SVO Portal as classic Sales user with Username "LoginUsers,B,5" and Password "LoginUsers,C,5"
    Then User navigates to the Enquiries tab
    And Search and select enquiry "EnquiryLostReason,B,2" 
    Then User edit enquiry to add new KMI
    Then User create new active KMI with all fields from enquiry page Contact details "Opportunity,H,2" Hand of Drive "KMIs,C,2" Product Offering "KMIs,B,7" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    When User navigates to KMI section
    And User views Hand of Drive details from All Active KMIs list view
    Then User updates hand of drive details on KMI page Hand of drive "KMIs,C,3"
    Then Logout from SVO Portal
    
    @UpdateHandofDriveFieldsFromEnquiryPage_09 @Batch @SSEC-
  Scenario: User is able to create new KMI from closed Opportunity enquiry
    Given Access to SVO Portal as Classic Service user with Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    Then User navigates to the Enquiries tab
    And Search and select enquiry "EnquiryLostReason,B,3" 
    Then User edit enquiry to add new KMI
    Then User create new active KMI with all fields from enquiry page Contact details "Opportunity,H,2" Hand of Drive "KMIs,C,2" Product Offering "KMIs,B,7" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    When User navigates to KMI section
    And User views Hand of Drive details from All Active KMIs list view
    Then User updates hand of drive details on KMI page Hand of drive "KMIs,C,3"
    Then Logout from SVO Portal
    
