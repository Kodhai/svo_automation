@SSEC_1568
Feature: Validate 1568 user story

  @SSEC_1568_01 @sonica
  Scenario: User is able to view the case automatically created  for Classic Sales when an email is sent by JLR Retailers / Global Support teams
    Given Access to Gmail account with "LoginUsers,B,17" and password "LoginUsers,C,17"
    And Customer sends email to Classic Sales user "LoginUsers,B,25"
    Then user logouts from gmail account
    Given Access to SVO Classic Sales Portal with Username "LoginUsers,B,5" and password "LoginUsers,C,5"
    And User navigates to Cases tab
    Then User verify that Classic Parts and Technical case is automatically created
    Then User logouts from the portal

  @SSEC_1568_02 @sonica
  Scenario: User is able to view the case automatically created  for Classic Sales when an email is sent by JLR Retailers / Global Support teams
    Given Access to Gmail account with "LoginUsers,B,17" and password "LoginUsers,C,17"
    And Customer sends email to Classic Sales user "LoginUsers,B,25"
    Then user logouts from gmail account
    Given Access to SVO Classic Sales Portal with Username "LoginUsers,B,5" and password "LoginUsers,C,5"
    And User navigates to Cases tab
    Then User verify that Classic Service case is automatically created
    Then User logouts from the portal

  @SSEC_1568_03 @sonica
  Scenario: User is able to view the case automatically created  for Classic Sales when an email is sent by JLR Retailers / Global Support teams
    Given Access to SVO Classic Sales Portal with Username "LoginUsers,B,5" and password "LoginUsers,C,5"
    And User navigates to Cases tab
    Then User verify that Classic Service case is automatically created
    Then Verify that Case Origin is Email
    Then User logouts from the portal

  @SSEC_1568_04 @sonica
  Scenario: User is able to view the case automatically created  for Classic Sales when an email is sent by JLR Retailers / Global Support teams
    Given Access to SVO Classic Sales Portal with Username "LoginUsers,B,5" and password "LoginUsers,C,5"
    And User navigates to Cases tab
    Then User verify that Classic Service case is automatically created
    Then Verify that Case Origin is Email
    And Verify that WebEmail Subject and Description is filled
    Then User logouts from the portal

  @SSEC_1568_05 @sonica
  Scenario: User is able to view the case automatically created  for Classic Sales when an email is sent by JLR Retailers / Global Support teams
    Given Access to SVO Classic Sales Portal with Username "LoginUsers,B,5" and password "LoginUsers,C,5"
    And User navigates to Cases tab
    Then User verify that Classic Service case is automatically created
    Then Verify that Case Origin is Email
    And Verify that user is able to close the case on Cases page
    Then User logouts from the portal

  @SSEC_1568_06 @sonica
  Scenario: User is able to view the case automatically created  for Classic Sales when an email is sent by JLR Retailers / Global Support teams
    Given Access to SVO Classic Sales Portal with Username "LoginUsers,B,5" and password "LoginUsers,C,5"
    And User navigates to Cases tab
    Then User verify that Classic Service case is automatically created
    Then Click on drop down button for the case created and select delete option
    And Click on Ok button
    Then User logouts from the portal
