Feature: SVO Enquiry validation

  #User creates a new Enquiry with SVO Bespoke Record Type
  @SVO @SVO_001 @SSEC-270 
  Scenario: User creates a new Enquiry with SVO Bespoke Record Type
    #Given Access SVO Portal as a user having "Scripts_UserRole,B,2" 
    Given Access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,2"
    And Enter the required details for the Enquiry creation
    Then Save the new Enquiry
	    Then User selects Preferred Contact details from the Potential Matches coming after entering the Email Address "Enquiry,N,2"
    And Log a call to the Enquiry with client "Enquiry,H,2"
    And Log a email to the Enquiry
    Then edit the required information as to "Enquiry,S,2" Related to "Enquiry,T,2" Additional to "Enquiry,U,2" CC "Enquiry,V,2" BCC "Enquiry,W,2" Subject "Enquiry,X,2" Body "Enquiry,Y,2" and Importance "Enquiry,Z,2"
    And Click on sent mail button
    Then from Discovery Tab click on Mark Current Enquiry Status
    And user fills the required details like Product Offering "Enquiry,B,2" Enquiry Source "Enquiry,C,2" Originating Division "Enquiry,D,2" Region "Enquiry,E,2" Sales Area "Enquiry,F,2" Market "Enquiry,G,2" Brand "Enquiry,J,2" Model "Enquiry,K,2" and Retailer "Enquiry,L,2"
    Then From Discovery Tab click on Mark Current Enquiry Status as Complete
    And Verify that a new opportunity is created after closing the Enquiry
    Then Verify that user can view Recently Viewed Enquiries as default
    #Then Logout from the SVO Portal

  #User creates a new Enquiry with SVO Private Office Record Type
  @SVO @SVO_002 @SSEC-273 
  Scenario: User creates a new Enquiry with SVO Private Office Record Type
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,3"
    And Enter the required details for the Enquiry creation
    Then Save the new Enquiry
    Then User selects Preferred Contact details from the Potential Matches coming after entering the Email Address "Enquiry,N,2"
    And Log a call to the Enquiry with client "Enquiry,H,3"
    And Log a email to the Enquiry
    Then edit the required information as to "Enquiry,S,2" Related to "Enquiry,T,2" Additional to "Enquiry,U,2" CC "Enquiry,V,2" BCC "Enquiry,W,2" Subject "Enquiry,X,2" Body "Enquiry,Y,2" and Importance "Enquiry,Z,2"
    And Click on sent mail button
    Then from Discovery Tab click on Mark Current Enquiry Status
    And user fills the required details like Product Offering "Enquiry,B,3" Enquiry Source "Enquiry,C,3" Originating Division "Enquiry,D,3" Region "Enquiry,E,3" Sales Area "Enquiry,F,3" Market "Enquiry,G,3" Brand "Enquiry,J,3" Model "Enquiry,K,3" and Retailer "Enquiry,L,3"
    Then Verify the SLA status and change the Enquiry Owner information to "Enquiry,O,3"
    Then From Discovery Tab click on Mark Current Enquiry Status as Complete
    And Verify that a new opportunity is created after closing the Enquiry
    Then Verify that user can view Recently Viewed Enquiries as default
    #Then Logout from the SVO Portal

    #User creates a new Enquiry with Classic Sales Record Type
  @SVO @SVO_003 @SSEC-274 
  Scenario: User creates a new Enquiry with Classic Sales Record Type
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,4"
    And Enter the required details for the Enquiry creation
    Then Save the new Enquiry
    Then User selects Preferred Contact details from the Potential Matches coming after entering the Email Address "Enquiry,N,2"
    And Log a call to the Enquiry with client "Enquiry,H,4"
    And Log a email to the Enquiry
    Then edit the required information as to "Enquiry,S,2" Related to "Enquiry,T,2" Additional to "Enquiry,U,2" CC "Enquiry,V,2" BCC "Enquiry,W,2" Subject "Enquiry,X,2" Body "Enquiry,Y,2" and Importance "Enquiry,Z,2"
    And Click on sent mail button
    Then from Discovery Tab click on Mark Current Enquiry Status
   	And user fills the required details like Product Offering "Enquiry,B,4" Enquiry Source "Enquiry,C,4" Originating Division "Enquiry,D,4" Region "Enquiry,E,4" Sales Area "Enquiry,F,4" Market "Enquiry,G,4" Brand "Enquiry,J,4" Model "Enquiry,K,4" and Retailer "Enquiry,L,4"
    Then Close enquiry with close type as "Enquiry,P,4" and reason as "Enquiry,Q,4"
    Then Verify that user can view Recently Viewed Enquiries as default
   	#Then Logout from the SVO Portal

  #User creates a new Enquiry with Classic Service Record Type
  @SVO @SVO_004 @SSEC-275 
  Scenario: User creates a new Enquiry with Classic Service Record Type
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,5"
    And Enter the required details for the Enquiry creation
    Then Save the new Enquiry
    Then User selects Preferred Contact details from the Potential Matches coming after entering the Email Address "Enquiry,N,2"
    And Log a call to the Enquiry with client "Enquiry,H,5"
    And Log a email to the Enquiry
    Then edit the required information as to "Enquiry,S,2" Related to "Enquiry,T,2" Additional to "Enquiry,U,2" CC "Enquiry,V,2" BCC "Enquiry,W,2" Subject "Enquiry,X,2" Body "Enquiry,Y,2" and Importance "Enquiry,Z,2"
    And Click on sent mail button
    Then from Discovery Tab click on Mark Current Enquiry Status
    And user fills the required details like Product Offering "Enquiry,B,5" Enquiry Source "Enquiry,C,5" Originating Division "Enquiry,D,5" Region "Enquiry,E,5" Sales Area "Enquiry,F,5" Market "Enquiry,G,5" Brand "Enquiry,J,5" Model "Enquiry,K,5" and Retailer "Enquiry,L,5"
    Then Close enquiry with close type as "Enquiry,P,5" and reason as "Enquiry,Q,5"
    When change the Enquiry Owner information to "Enquiry,O,3" without a mail
    Then Verify that user can view Recently Viewed Enquiries as default
    #Then Logout from the SVO Portal

  #User does not log a call to an enquiry which is in New stage
  @SVO @SVO_005 @SSEC-276 
  Scenario: User does not log a call to an enquiry which is in New stage
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,2"
    And Enter the required details for the Enquiry creation
    Then Save the new Enquiry
    Then from Contacted Tab Mark Current Enquiry Status
    Then Verify Current status cannot be marked as Contacted
    #Then Logout from the SVO Portal

  #User enters Incorrect Enquiry name in the Enquiry search box
  @SVO @SVO_006 @SSEC-277 
  Scenario: User enters Incorrect Enquiry name in the Enquiry search box
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    And Enter Incorrect Enquiry name as "ENQ-001" in the Enquiry search box
    Then Validate error message displayed
    #Then Logout from the SVO Portal

  #User navigates to All Enquiries list from Recently Viewed Enquiry list
  @SVO @SVO_007 @SSEC-278  
  Scenario: User navigates to All Enquiries list from Recently Viewed Enquiry list
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    And User navigates to All Enquiries list from Recently Viewed Enquiry list
    Then Validate user can view all enquiries with status and enquiry owner
    #Then Logout from the SVO Portal

  #User can select client and preferred conatct details without entering an email address
  @SVO @SVO_008 @SSEC-279 
  Scenario: User can select client and preferred conatct details without entering an email address
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,2"
    And Enter the required details for the Enquiry creation
    Then Save the new Enquiry
    And User Edits an enquiry and Enter Client "Enquiry,H,6" and Preferred Contact "Enquiry,H,6"
    And Save the enquiry
    #Then Logout from the SVO Portal

  #User skips Client/Vehicle Details/Source field at the contacted stage of an enquiry
  @SVO @SVO_009 @SSEC-280 
  Scenario: User skips Client/Vehicle Details/Source field at the contacted stage of an enquiry
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    And User Create New Enquiry with Record Type "Enquiry,A,4"
    And Enter the Enquiry Title for the Enquiry creation
    And Save the enquiry
    And Log a call to the Enquiry with client "Enquiry,H,4"
    And From the Contacted Stage Mark Enquiry Status as Complete
    Then Validate the Error Message
    #Then Logout from the SVO Portal

  #User enters Client, Preferred Contact and Source and closes an enquiry as Qualified-Opportunity
  @SVO @SVO_010 @SSEC-281 
  Scenario: User enters Client, Preferred Contact and Source and closes an enquiry as Qualified-Opportunity
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    And User Create New Enquiry with Record Type "Enquiry,A,4"
    And Enter the Enquiry Title for the Enquiry creation
    And Save the enquiry
    Then User selects Preferred Contact details from the Potential Matches coming after entering the Email Address "Enquiry,N,2"
    And Log a call to the Enquiry with client "Enquiry,H,4"
    Then from Discovery Tab click on Mark Current Enquiry Status
    Then Mark the Enquiry Status as Complete with Qualified-Opportunity state
    Then Validate the Error Message	Displayed
    #Then Logout from the SVO Portal

  #User skips Client and Source fields to close an enquiry as Closed[Lost] status
  @SVO @SVO_011 @SSEC-282 
  Scenario: User skips Client and Source fields to close an enquiry as Closed[Lost] status
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    And User Create New Enquiry with Record Type "Enquiry,A,2"
    And Enter the Enquiry Title for the Enquiry creation
    And Save the enquiry
    And User closes an enquiry with "Closed[Lost]" status
    #Then Logout from the SVO Portal

  #User tries to re-open [Move to New from Closed] an enquiry
  @SVO @SVO_012 @SSEC-283 
  Scenario: User tries to re-open [Move to New from Closed] an enquiry
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    And User search an enquiry having closed reason type as "Qualified-Opportunity"
    And User tries to reopen Closed Enquiry
    #Then Logout from the SVO Portal

  #User can change owner of a closed enquiry
  @SVO @SVO_013 @SSEC-284 
  Scenario: User can change owner of a closed enquiry
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,2"
    And Enter the required details for the Enquiry creation
    Then Save the new Enquiry
    Then User selects Preferred Contact details from the Potential Matches coming after entering the Email Address "Enquiry,N,2"
    And Log a call to the Enquiry with client "Enquiry,H,2"
    Then from Discovery Tab click on Mark Current Enquiry Status
    And user fills the required details like Product Offering "Enquiry,B,2" Enquiry Source "Enquiry,C,2" Originating Division "Enquiry,D,2" Region "Enquiry,E,2" Sales Area "Enquiry,F,2" Market "Enquiry,G,2" Brand "Enquiry,J,2" Model "Enquiry,K,2" and Retailer "Enquiry,L,2"
    Then From Discovery Tab click on Mark Current Enquiry Status as Complete
    When User clicks on the change Owner and enter Owner as "Enquiry,S,2"
    #Then Logout from the SVO Portal

  #User should be able view Enquiry History
  @SVO @SVO_014 @SSEC-285  
  Scenario: User should be able view Enquiry History
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    And User Create New Enquiry with Record Type "Enquiry,A,4"
    And Enter the Enquiry Title for the Enquiry creation
    And Save the enquiry
    Then User selects Preferred Contact details from the Potential Matches coming after entering the Email Address "Enquiry,N,2"
    And Log a call to the Enquiry with client "Enquiry,H,4"
    Then from Discovery Tab click on Mark Current Enquiry Status
    And User View Enquiry History
    #Then Logout from the SVO Portal

  #User changes the enquiry status in the backward direction [Discovery to Contacted]
  @SVO @SVO_015 @SSEC-286 
  Scenario: User changes the enquiry status in the backward direction [Discovery to Contacted]
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    And User Create New Enquiry with Record Type "Enquiry,A,2"
    And Enter the Enquiry Title for the Enquiry creation
    And Save the enquiry
    Then User selects Preferred Contact details from the Potential Matches coming after entering the Email Address "Enquiry,N,2"
    And Log a call to the Enquiry with client "Enquiry,H,2"
    Then from Discovery Tab click on Mark Current Enquiry Status
    And User changes the enquiry status from Discovery to Contacted stage
    #Then Logout from the SVO Portal

  #User cannot change the enquiry status from Contacted to New
  @SVO @SVO_016 @SSEC-287 
  Scenario: User cannot change the enquiry status from Contacted to New
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    And User Create New Enquiry with Record Type "Enquiry,A,2"
    And Enter the Enquiry Title for the Enquiry creation
    And Save the enquiry
    Then User selects Preferred Contact details from the Potential Matches coming after entering the Email Address "Enquiry,N,2"
    And Log a call to the Enquiry with client "Enquiry,H,2"
    And User changes enquiry status from Contacted to New stage
    And Verify that an error occurred when moving enquiry from Contacted to New
    #Then Logout from the SVO Portal

  #User should be able to change the record type of a non-closed enquiry
  @SVO @SVO_017 @SSEC-288
  Scenario: User should be able to change the record type of a non-closed enquiry
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    And User Create New Enquiry with Record Type "Enquiry,A,2"
    And Enter the Enquiry Title for the Enquiry creation
    And Save the enquiry
    And User changes the record type of a non-closed enquiry
    #Then Logout from the SVO Portal

  #User should not be able to change the record type of a closed enquiry
  @SVO @SVO_018 @SSEC-289
  Scenario: User should not be able to change the record type of a closed enquiry
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,2"
    And Enter the required details for the Enquiry creation
    Then Save the new Enquiry
    Then User selects Preferred Contact details from the Potential Matches coming after entering the Email Address "Enquiry,N,2"
    And Log a call to the Enquiry with client "Enquiry,H,2"
    Then from Discovery Tab click on Mark Current Enquiry Status
    And user fills the required details like Product Offering "Enquiry,B,2" Enquiry Source "Enquiry,C,2" Originating Division "Enquiry,D,2" Region "Enquiry,E,2" Sales Area "Enquiry,F,2" Market "Enquiry,G,2" Brand "Enquiry,J,2" Model "Enquiry,K,2" and Retailer "Enquiry,L,2"
    Then From Discovery Tab click on Mark Current Enquiry Status as Complete
    And User changes the record type of a closed enquiry
    Then Verify that an error occurred when user changes record type of a closed enquiry
    #Then Logout from the SVO Portal

  #User can get vehicle information from Vehicle Search Option for any non-closed enquiry
  @SVO @SVO_019 @SSEC-290
  Scenario: User can get vehicle information from Vehicle Search Option for any non-closed enquiry
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    And User Create New Enquiry with Record Type "Enquiry,A,2"
    And Enter the Enquiry Title for the Enquiry creation
    And Save the enquiry
    And User searches a vehicle from Vehicle Search Option using Brand "Vehicle,A,2",Model "Vehicle,B,2",Full VIN "Vehicle,C,2" and Record Type "Vehicle,D,2"
    Then Verify that Vehicle information get auto populated
    #Then Logout from the SVO Portal

  #User should be able to close an enquiry with Qualified-KMI status [For Classic Sales and Classic Service Record Type
  @SVO @SVO_020 @SSEC-291
  Scenario: User should be able to close an enquiry with Qualified-KMI status [For Classic Sales and Classic Service Record Type
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    And User Create New Enquiry with Record Type "Enquiry,A,5"
    And Enter the Enquiry Title for the Enquiry creation
    And Save the enquiry
    Then User selects Preferred Contact details from the Potential Matches coming after entering the Email Address "Enquiry,N,2"
    And Log a call to the Enquiry with client "Enquiry,H,2"
    Then from Discovery Tab click on Mark Current Enquiry Status
    And user fills the required details like Product Offering "Enquiry,B,5" Enquiry Source "Enquiry,C,5" Originating Division "Enquiry,D,5" Region "Enquiry,E,5" Sales Area "Enquiry,F,5" Market "Enquiry,G,5" Brand "Enquiry,J,5" Model "Enquiry,K,5" and Retailer "Enquiry,L,5"
    Then User Creates a new Programme with Status "Programme,A,2", Product offering "Programme,B,2", Brand "Programme,C,2", Model "Programme,D,2"
    Then From Discovery Tab click on Mark Current Enquiry Status as Complete
    #Then Logout from the SVO Portal

  #User searches an opportunity for non-closed enquiry
  @SVO @SVO_021 @SSEC-292
  Scenario: User searches an opportunity for non-closed enquiry
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    And User Create New Enquiry with Record Type "Enquiry,A,5"
    And Enter the Enquiry Title for the Enquiry creation
    And Save the enquiry
    Then Verify that no opportunity is linked for a non-closed enquiry
    #Then Logout from the SVO Portal

  #User is able to change closed enquiry reason type from Closed[Lost] to Qualified-Opportunity
  @SVO @SVO_022 @SSEC-293
  Scenario: User is able to change closed enquiry reason type from Closed[Lost] to Qualified-Opportunity
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    And User search an enquiry having Closed Lost reason type as "Closed[Lost]"
    Then User changes the closed reason type from "Closed[Lost]" to "Qualified-Opportunity"
    #Then Logout from the SVO Portal

  #User cannot change closed enquiry reason type from Qualified-Opportunity to Closed[Lost]
  @SVO @SVO_023 @SSEC-294 
  Scenario: User cannot change closed enquiry reason type from Qualified-Opportunity to Closed[Lost]
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    And User search an enquiry having closed reason type as "Qualified-Opportunity"
    Then User changes the closed reason type from "Qualified-Opportunity" to "Closed[Lost]"
    #Then Logout from the SVO Portal

  #User should be able receive an email if no action is taken for an open enquiry within 48 hours
  @SVO @SVO_024 @SSEC-295
  Scenario: User should be able receive an email if no action is taken for an open enquiry within 48 hours
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    Then User opens an enquiry which is in new status since 48hrs
    And Verify that User receives a Custom notification for the same enquiry
    #Then Logout from the SVO Portal

  #User should not fail to complete the first contact target within 48 hours of Kick-off date for an open enquiry
  @SVO @SVO_025 @SSEC-296 
  Scenario: User should not fail to complete the first contact target within 48 hours of Kick-off date for an open enquiry
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    Then User opens an enquiry which is in new status since 48hrs
    And Verify that the open enquiry SLA status is failed
    Then Logout from the SVO Portal

  #User is able to complete first contact target within 48 hours of Kick-off date for an open enquiry
  @SVO @SVO_026 @SSEC-297 
  Scenario: User is able to complete first contact target within 48 hours of Kick-off date for an open enquiry
    Given Access SVO Portal
    #Given User Access SVO Portal
    When User Navigate to Enquiries
    Then User opens an enquiry which is in new status
    And Verify that the open enquiry SLA status is in progress
    #Then Logout from the SVO Portal

  #User should not be able to close an enquiry without SLA Success status
  @SVO @SVO_027 @SSEC-298 
  Scenario: User should not be able to close an enquiry without SLA Success status
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    Then User opens an enquiry which is in new status
    And User closes an enquiry for which SLA status is in progress
    Then Verify that an error occurred when user closes an enquiry without SLA Success status
    #Then Logout from the SVO Portal

  #User is able to receive an email reminder till final Escalation Reminder target date
  @SVO @SVO_028 @SSEC-299 
  Scenario: User is able to receive an email reminder till final Escalation Reminder target date
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    Then Open an enquiry for which SLA is failed and reach till Final Escalation Reminder Target Date
    And Verify that User receives a Custom notification for the same enquiry
    #Then Logout from the SVO Portal

  #User is not able to receive any email, if an enquiry is contacted within 48 hours
  @SVO @SVO_029 @SSEC-300 
  Scenario: User is not able to receive any email, if an enquiry is contacted within 48 hours
    #Given Access SVO Portal
    Given User Access SVO Portal
    When User Navigate to Enquiries
    Then Open an enquiry which is in Contacted status
    And Verify that user is not received any email when an enquiry is contacted within 48 hours
    #Then Logout from the SVO Portal

  #User is able to create multiple enquiries with the same enquiry title
  @SVO @SVO_030 @SSEC-301
  Scenario: User is able to create multiple enquiries with the same enquiry title
    Given Access SVO Portal
    #Given User Access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,2"
    And Enter Enquiry title "Enquiry,R,2"  for the Enquiry creation
    Then Save the new Enquiry
    And Verify the title of the created Enquiry as "Enquiry,R,2"
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,2"
    And Enter Enquiry title "Enquiry,R,2"  for the Enquiry creation
    Then Save the new Enquiry
    And Verify the title of the created Enquiry as "Enquiry,R,2"
    #Then Logout from the SVO Portal
    
  #User creates a new Compose Email for an existing Enquiry
  @SVO @SVO_101 @SSEC-372
  Scenario: User creates a new Compose Email for an existing Enquiry
    Given Access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,2"
    And Enter the required details for the Enquiry creation
    Then Save the new Enquiry
    When user clicks on Compose Email button
    Then edit the required information as to "Enquiry,S,2" Related to "Enquiry,T,2" Additional to "Enquiry,U,2" CC "Enquiry,V,2" BCC "Enquiry,W,2" Subject "Enquiry,X,2" Body "Enquiry,Y,2" and Importance "Enquiry,Z,2"
    And Click on sent mail button
    Then Logout from the SVO Portal

  #User selects template from Select Template button
  @SVO @SVO_102 @SSEC-373
  Scenario: User selects template from Select Template button
    Given Access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,2"
    And Enter the required details for the Enquiry creation
    Then Save the new Enquiry
    When user clicks on Compose Email button
    Then user clicks on select template button and selects a template and saves it
    Then Logout from the SVO Portal

  #User Saves the email after entering all the details
  @SVO @SVO_103 @SSEC-374
  Scenario: User Saves the email after entering all the details
    Given Access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,2"
    And Enter the required details for the Enquiry creation
    Then Save the new Enquiry
    When user clicks on Compose Email button
    Then edit the required information as to "Enquiry,S,2" Related to "Enquiry,T,2" Additional to "Enquiry,U,2" CC "Enquiry,V,2" BCC "Enquiry,W,2" Subject "Enquiry,X,2" Body "Enquiry,Y,2" and Importance "Enquiry,Z,2"
    And Click on save mail button
    Then Logout from the SVO Portal

  #User should be able to view saved emails in Save Draft
  @SVO @SVO_104 @SSEC-375
  Scenario: User should be able to view saved emails in Save Draft
    Given Access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,2"
    And Enter the required details for the Enquiry creation
    Then Save the new Enquiry
    When user clicks on Compose Email button
    And Click on save mail button
    Then Click on Drafts button
    Then Logout from the SVO Portal

  #User is able to Attach file to the email
  @SVO @SVO_105 @SSEC-376
  Scenario: User is able to Attach file to the email
    Given Access SVO Portal
    When User Navigate to Enquiries
    Then User opens any open enquiry
    And User compose an email and attach some file from different location
    Then Logout from the SVO Portal

  #User can Cancel email if required before sending
  @SVO @SVO_106 @SSEC-377
  Scenario: User can Cancel email if required before sending
    Given Access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,2"
    And Enter the required details for the Enquiry creation
    Then Save the new Enquiry
    When user clicks on Compose Email button
    Then edit the required information as to "Enquiry,S,2" Related to "Enquiry,T,2" Additional to "Enquiry,U,2" CC "Enquiry,V,2" BCC "Enquiry,W,2" Subject "Enquiry,X,2" Body "Enquiry,Y,2" and Importance "Enquiry,Z,2"
    Then Click on the Cancel button
    Then Logout from the SVO Portal

  #An error message is displayed, when user tries to send an email without entering all the required fields
  @SVO @SVO_107 @SSEC-378
  Scenario: An error message is displayed, when user tries to send an email without entering all the required fields
    Given Access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,2"
    And Enter the required details for the Enquiry creation
    Then Save the new Enquiry
    Then Validate that preferred contact details is blank
    When user clicks on Compose Email button
    Then Click on the Send button and verify the error
    Then Logout from the SVO Portal

  #User can able to view all the buttons on the header section of compose email and same buttons on the footer section
  @SVO @SVO_108 @SSEC-379
  Scenario: User can able to view all the buttons on the header section of compose email and same buttons on the footer section
    Given Access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,2"
    And Enter the required details for the Enquiry creation
    Then Save the new Enquiry
    When user clicks on Compose Email button
    Then Verify buttons on header and footer section
    Then Logout from the SVO Portal
  
  #User is able to retrive saved emails from Drafts
  @SVO @SVO_109 @SSEC-380
  Scenario: User is able to retrive saved emails from Drafts
    Given Access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,2"
    And Enter the required details for the Enquiry creation
    Then Save the new Enquiry
    When user clicks on Compose Email button
    Then edit the required information as to "Enquiry,S,2" Related to "Enquiry,T,2" Additional to "Enquiry,U,2" CC "Enquiry,V,2" BCC "Enquiry,W,2" Subject "Enquiry,X,2" Body "Enquiry,Y,2" and Importance "Enquiry,Z,2"
    And Click on save mail button
    When user clicks on Compose Email button
    Then user clicks on drafts and retrives draft with Subject "Enquiry,X,2"
    And Click on sent mail button
    Then Logout from the SVO Portal

  #User is able to view auto populated fields after opening Compose email
  @SVO @SVO_110 @SSEC-381
  Scenario: User is able to view auto populated fields after opening Compose email
    Given Access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,2"
    And Enter the required details for the Enquiry creation
    Then Save the new Enquiry
    When user clicks on Compose Email button
    And Verify that user is able to view auto populated fields after opening Compose email
    Then Logout from the SVO Portal

  #User can edit auto populated field details in Compose email list
  @SVO @SVO_111 @SSEC-382
  Scenario: User can edit auto populated field details in Compose email list
    Given Access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,2"
    And Enter the required details for the Enquiry creation
    Then Save the new Enquiry
    When user clicks on Compose Email button
    Then Edit auto populated fields Related to "Enquiry,T,2" and Importance "Enquiry,Z,2" in Compose email page
    And Click on save mail button
    Then Logout from the SVO Portal

  #User can add Related To on Compose Email page before sending
  @SVO @SVO_112 @SSEC-383
  Scenario: User can add Related To on Compose Email page before sending
    Given Access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,2"
    And Enter the required details for the Enquiry creation
    Then Save the new Enquiry
    Then user clicks on Compose Email button
    And User adds Additional Related to "Enquiry,T,3" as "Enquiry,H,4"
    And Click on save mail button
    Then Logout from the SVO Portal

  #User should be able to view Reference number for an Enquiry with Qualified Opprotunity closed state
  @SVO @SVO_113 @SSEC-384
  Scenario: User should be able to view Reference number for an Enquiry with Qualified Opprotunity closed state
    Given Access SVO Portal
    When User Navigate to Enquiries
    And User Create New Enquiry with Record Type "Enquiry,A,4"
    And Enter Enquiry title "Enquiry,R,2"  for the Enquiry creation
    And Save the enquiry
    Then User selects Preferred Contact details from the Potential Matches coming after entering the Email Address "Enquiry,N,4"
    Then Log a call to that Enquiry with Client "Enquiry,H,4"
    Then From the Contacted Stage Mark Enquiry Status as Complete
    And user fills the required details like Product Offering "Enquiry,B,4" Enquiry Source "Enquiry,C,4" Originating Division "Enquiry,D,4" Region "Enquiry,E,4" Sales Area "Enquiry,F,4" Market "Enquiry,G,4" Brand "Enquiry,J,4" Model "Enquiry,K,4" and Retailer "Enquiry,L,4"
    Then Mark the Enquiry Status as Complete with Qualified-Opportunity state
    Then User Navigates to Opportunities link	with Enquiry Title "Enquiry,R,2"
    Then User Validates the Reference Number in the Details Section
    Then Logout from the SVO Portal

  #User cannot see any KMI created for non-closed enquiry
  @SVO @SVO_166 @SSEC-437
  Scenario: User cannot see any KMI created for non-closed enquiry
    Given Access SVO Portal
    When User Navigate to Enquiries
    And User opens an enquiry with "Contacted" stage
    And Open KMI from related links and verify there are no records available
    Then Logout from the SVO Portal

  #User cannot see any KMI if an enquiry is closed with Quailified-Opportunity
  @SVO @SVO_167 @SSEC-438
  Scenario: User cannot see any KMI if an enquiry is closed with Quailified-Opportunity
    Given Access SVO Portal
    When User Navigate to Enquiries
    And User opens an enquiry with "Closed" stage
    And Open KMI from related links and verify there are no records available
    Then Logout from the SVO Portal

  #User can enter Source KMI details while creating new enquiry
  @SVO @SVO_168 @SSEC-439
  Scenario: User can enter Source KMI details while creating new enquiry
    Given Access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,2"
    Then Create a new enquiry with title "Enquiry,R,2" source KMI "Enquiry,AB,2"
    Then Save the new Enquiry
    Then Logout from the SVO Portal

  #User is able to create a new KMI under KMI section before closing an enquiry
  @SVO @SVO_169 @SSEC-440
  Scenario: User is able to create a new KMI under KMI section before closing an enquiry
    Given Access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,12"
    And Enter the required details for the Enquiry creation
    Then Save the new Enquiry
    When user clicks on Compose Email button
    Then edit the required information as to "Enquiry,S,2" Related to "Enquiry,T,2" Additional to "Enquiry,U,2" CC "Enquiry,V,2" BCC "Enquiry,W,2" Subject "Enquiry,X,2" Body "Enquiry,Y,2" and Importance "Enquiry,Z,2"
    And Click on save mail button
    Then Logout from the SVO Portal

  #User is able to edit the created KMI from KMI Record Section
  @SVO @SVO_170 @SSEC-441
  Scenario: User is able to edit the created KMI from KMI Record Section
    Given Access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,15"
    And Enter the required details for the Enquiry creation
    Then Save the new Enquiry
    Then Navigate to KMI section
    Then User created New KMI with fields like Contact "Enquiry,AA,2" ProductOffering "Enquiry,B,4" Brand "Enquiry,J,2" Model "Enquiry,K,2"
    Then User changes the Product offering of KMI to "Enquiry,B,5"
    Then User save the KMI Record
    Then Logout from the SVO Portal

  #User is able to change Owner of the KMI record from KMI Record Section
  @SVO @SVO_171 @SSEC-442
  Scenario: User is able to change Owner of the KMI record from KMI Record Section
    Given Access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,15"
    And Enter the required details for the Enquiry creation
    Then Save the new Enquiry
    Then Navigate to KMI section
    Then User created New KMI with fields like Contact "Enquiry,AA,2" ProductOffering "Enquiry,B,4" Brand "Enquiry,J,2" Model "Enquiry,K,2"
    Then User change the owner of KMI to "Enquiry,AA,2"
    Then Logout from the SVO Portal
    
  #User is able to delete the KMI records from any enquiry
  @SVO @SVO_172 @SSEC-443
  Scenario: User is able to delete the KMI records from any enquiry
    Given Access SVO Portal
    When User Navigate to Enquiries
    And User opens an enquiry with "Closed" stage
    And Open KMI from related links and verify there are no records available
    Then Click on New button to create a new KMI
    Then Enter the fields contact "Enquiry,I,2" product offering "Enquiry,B,2" brand "Enquiry,J,2" model "Enquiry,K,2"
    Then Save the KMI created
    Then Delete the KMI from the list
    Then Logout from the SVO Portal
    
  #User is able to create an Enquiries from KMI Record Section
  @SVO @SVO_173
  Scenario: User is able to create an Enquiries from KMI Record Section
    Given Access SVO Portal
    When User Navigate to Enquiries
    And User opens an enquiry with "Closed" stage
    And Open KMI from related links and verify there are no records available
    Then Click on New button to create a new KMI
    Then Enter the fields contact "Enquiry,I,2" product offering "Enquiry,B,2" brand "Enquiry,J,2" model "Enquiry,K,2"
    Then Save the KMI created
    Then Open the KMI created
    Then Navigate to Related tab
    Then Create a new enquiry of type "Enquiry,A,2" and title "Enquiry,R,2"
    Then Save the new Enquiry
    Then Logout from the SVO Portal

  #User cannot be able to see Source KMI field for SVO Bespoke and SVO Private Office Enquiry
  @SVO @SVO_174 
  Scenario: User cannot be able to see Source KMI field for SVO Bespoke and SVO Private Office Enquiry
    Given Access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,2"
    Then Create a new enquiry with title "Enquiry,R,2" source KMI "Enquiry,AB,2"
    Then Save the new Enquiry
    Then Click on Closed state and mark as current enquiry status
    Then Click on Closed state dropdown and verify Qualified - KMI option is not available for the user
    Then Click on Cancel
    Then Logout from the SVO Portal

  #User can Open, Send reply, Edit fields and forward to any email from Emails tab of an Enquiry
  @SVO @SVO_175 
  Scenario: User can Open, Send reply, Edit fields and forward to any email from Emails tab of an Enquiry
    Given Access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,2"
    Then Create a new enquiry with title "Enquiry,R,2" source KMI "Enquiry,AB,2"
    Then Save the new Enquiry
    Then Navigate to Emails tab
    Then Click on e2a send an email
    Then edit the required information as to "Enquiry,U,2" Subject "Enquiry,X,2" and click on Send
    Then Navigate to Emails tab
    Then Click on e2a email and open the sent email
    Then Click on Reply button
    Then Edit the to field as "Enquiry,W,2" and click on send
    Then Click on Forward button
    Then Edit the to field as "Enquiry,U,2" and click on send
    Then Logout from the SVO Portal

  #User should be able to delete an Enquiry from KMI Record Section
  @SVO @SVO_176 @SSEC-447
  Scenario: User should be able to delete an Enquiry from KMI Record Section
    Given Access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,2"
    And Enter the required details for the Enquiry creation
    Then Save the new Enquiry
    Then Navigate to KMI section
    Then User created New KMI with fields like Contact "Enquiry,AA,2" ProductOffering "Enquiry,B,4" Brand "Enquiry,J,2" Model "Enquiry,K,2"
    Then User click on newly created KMI
    Then User delete the KMI Record
    Then Logout from the SVO Portal

  #User should be able to Change owner of the Enquiry from KMI Record Section
  @SVO @SVO_177 @SSEC-448
  Scenario: User should be able to Change owner of the Enquiry from KMI Record Section
    Given Access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,2"
    And Enter the required details for the Enquiry creation
    Then Save the new Enquiry
    Then Navigate to KMI section
    Then User created New KMI with fields like Contact "Enquiry,AA,2" ProductOffering "Enquiry,B,4" Brand "Enquiry,J,2" Model "Enquiry,K,2"
    Then User click on newly created KMI
    Then User navigate to Related tab of KMI
    Then User creates new enquiry and link to KMI
    Then User navigate to Enquiry
    Then Change the owner of Enquiry to "Enquiry,AA,2"
    Then Logout from the SVO Portal

  #User sends group email from any open enquiry
  #user : admin
  @SVO @SVO_219
  Scenario: User sends group email from any open enquiry
    Given Access SVO Portal
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,2"
    Then Create a new enquiry with title "Enquiry,R,2" source KMI "Enquiry,AB,2"
    Then Save the new Enquiry
    Then Navigate to Emails tab
    Then Click on e2a send an email
    And Add multiple users in To section as "Enquiry,S,2" and "Enquiry,S,3" and "Enquiry,S,4" subject "Enquiry,X,2" and click on send
    Then Logout from the SVO Portal
