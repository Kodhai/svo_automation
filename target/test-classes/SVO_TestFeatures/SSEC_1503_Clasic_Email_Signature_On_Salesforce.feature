Feature: SVO Classic email signature on salesforce validations

  #User is able to click on the link attached in the email signature of an email
  @SVO_1503_Clasic_Email_Signature_01 @SSEC-1643 @SVO_1503_Batch
  Scenario: User is able to click on the link attached in the email signature of an email
    Given Access to SVO Portal as classic Sales user with Username "LoginUsers,B,5" and Password "LoginUsers,C,5"
    Then User navigates to the Enquiries tab
    And User creates an Enquiry with record type "EnquiryLostReason,A,2"
    Then User send an email to customer "LoginUsers,B,17" with Business unit "CustomerResponse,I,3" and subject and verify works legend stock link in email signature
    Then User Logout from SVO Portal
    Given Access to user mail account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    Then Verify that Customer receives an enquiry update email from user
    And User views works legend stock after clicking on the link from email signature
    Then Logout from Outlook account

  #User is able to view works legend stock from attached link in the email signature for Classic Service enquiry
  @SVO_1503_Clasic_Email_Signature_02 @SSEC-1644 @SVO_1503_Batch
  Scenario: User is able to view works legend stock from attached link in the email signature for Classic Service enquiry
    Given Access to SVO Portal as classic service Username user with Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    Then User navigates to the Enquiries tab
    And User creates an Enquiry with record type "EnquiryLostReason,A,3"
    Then User send an email to customer "LoginUsers,B,17" with Business unit "CustomerResponse,I,4" and subject and verify works legend stock link in email signature
    Then User Logout from SVO Portal
    Given Access to user mail account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    Then Verify that Customer receives an enquiry update email from user
    And User views works legend stock after clicking on the link from email signature
    Then Logout from Outlook account

  #User is able to view works legend stock from attached link in the email signature for Closed Classic Sales enquiry
  @SVO_1503_Clasic_Email_Signature_03 @SSEC-1646 @SVO_1503_Batch
  Scenario: User is able to view works legend stock from attached link in the email signature for Closed Classic Sales enquiry
    Given Access to SVO Portal as Classic Admin user with Username "LoginUsers,B,13" and Password "LoginUsers,C,13"
    Then User navigates to the Enquiries tab
    And User creates an Enquiry with record type "EnquiryLostReason,A,2"
    And Log a call for an enquiry activity
    Then User moves an enquiry to discovery stage by adding contact details "Opportunity,J,7"
    Then User close the enquiry with closed state "EnquiryLostReason,B,2" and closed reason "EnquiryLostReason,C,2" on Enquiry page
    And Verify the auto-populated fields of enqiry closed reason and closed state on Enquiry page
    Then User send an email to customer "LoginUsers,B,17" with Business unit "CustomerResponse,I,3" and subject and verify works legend stock link in email signature
    Then User Logout from the SVO Portal
    Given Access to user mail account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    Then Verify that Customer receives an enquiry update email from user
    And User views works legend stock after clicking on the link from email signature
    Then Logout from Outlook account

  #User is not able to view works legend link in the email signature for SVO Bespoke enquiry
  @SVO_1503_Clasic_Email_Signature_04 @SSEC-1647 @SVO_1503_Batch
  Scenario: User is not able to view works legend link in the email signature for SVO Bespoke enquiry
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Enquiries tab
    And User creates an Enquiry with record type "EnquiryLostReason,A,4"
    Then User send an email to customer "LoginUsers,B,17" with Business unit "CustomerResponse,I,2" and subject and verify works legend stock link is not present on email signature
    Then User Logout from SVO Portal
    Given Access to user mail account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    Then Verify that Customer receives an enquiry update email from user
    And works legend stock link is not available in received email
    Then Logout from Outlook account

  #User is not able to view works legend link in the email signature if business unit is not selected for Private Office enquiry
  @SVO_1503_Clasic_Email_Signature_05 @SSEC-1648 @SVO_1503_Batch
  Scenario: User is not able to view works legend link in the email signature if business unit is not selected for Private Office enquiry
    Given Access to SVO Portal by private office user with Username "LoginUsers,B,4" and Password "LoginUsers,C,4"
    Then User navigates to the Enquiries tab
    And User creates an Enquiry with record type "EnquiryLostReason,A,5"
    Then User send an email to customer "LoginUsers,B,17" without Business unit and subject and verify works legend stock link is not present on email signature
    Then User Logout from SVO Portal
    Given Access to user mail account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    Then Verify that Customer receives an enquiry update email from user
    And works legend stock link is not available in received email
    Then Logout from Outlook account
