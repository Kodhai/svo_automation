Feature: SVO_Case_Assignments_rules

  #User is able to submit Parts and Technical Jaguar Enquiry through web form.
  @SSEC_1230_Case_Assignment_Rules_01 @SSEC-1802
  Scenario: User is able to submit Parts and Technical Jaguar Enquiry through web form.
    Given Access to the Classic Parts "CaseCreation,L,2" portal
    Then Navigate to Contact Us section
    Then Submit Contact Us form with classic parts and technical jaguar enquiry Email "Case_Lifecycle,F,2" First name "Case_Lifecycle,G,2" Last name "Case_Lifecycle,H,2" Country "Case_Lifecycle,I,2" Phone number "Case_Lifecycle,J,2" VIN number "Case_Lifecycle,K,2"
    Given User Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    And Verifies Classic parts and technical case under parts and technical queue
    Then Logout from SVO Portal

  #User is able to submit Parts and Technical Land Rover Enquiry through web form.
  @SSEC_1230_Case_Assignment_Rules_02 @SSEC-1803
  Scenario: User is able to submit Parts and Technical Land Rover Enquiry through web form.
    Given Access to the Classic Parts "CaseCreation,L,2" portal
    Then Navigate to Contact Us section
    Then Submit Contact Us form with classic parts and technical landrover enquiry Email "Case_Lifecycle,F,2" First name "Case_Lifecycle,G,2" Last name "Case_Lifecycle,H,2" Country "Case_Lifecycle,I,2" Phone number "Case_Lifecycle,J,2" VIN number "Case_Lifecycle,K,2"
    Given User Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    And Verifies Classic parts and technical case under parts and technical queue
    Then Logout from SVO Portal

  #User is able to submit Customer Service Enquiry through web form.
  @SSEC_1230_Case_Assignment_Rules_03 @SSEC-1804
  Scenario: User is able to submit Customer Service Enquiry through web form.
    Given Access to the Classic Parts "CaseCreation,L,2" portal
    Then Navigate to Contact Us section
    Then Submit Contact Us form with classic customer service enquiry Email "Case_Lifecycle,F,2" First name "Case_Lifecycle,G,2" Last name "Case_Lifecycle,H,2" Country "Case_Lifecycle,I,2" Phone number "Case_Lifecycle,J,2" VIN number "Case_Lifecycle,K,2"
    Given User Access to SVO Portal as Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    And Verifies Classic customer service case under customer service queue
    Then Logout from SVO Portal

  #User is able to create case through web form by selecting Other.
  @SSEC_1230_Case_Assignment_Rules_04 @SSEC-1805
  Scenario: User is able to create case through web form by selecting Other.
    Given Access to the Classic Parts "CaseCreation,L,2" portal
    Then Navigate to Contact Us section
    Then Submit Contact Us form with Other Email "Case_Lifecycle,F,2" First name "Case_Lifecycle,G,2" Last name "Case_Lifecycle,H,2" Country "Case_Lifecycle,I,2" Phone number "Case_Lifecycle,J,2" VIN number "Case_Lifecycle,K,2"
    Given User Access to SVO Portal as Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    And Verifies Classic customer service case under customer service queue
    Then Logout from SVO Portal

  #User is not able to view case under Parts and Technical queue if Customer Service option is selected while creating case through web form
  @SSEC_1230_Case_Assignment_Rules_05 @SSEC-1806
  Scenario: User is not able to view case under Parts and Technical queue if Customer Service option is selected while creating case through web form
    Given Access to the Classic Parts "CaseCreation,L,2" portal
    Then Navigate to Contact Us section
    Then Submit Contact Us form with classic customer service enquiry Email "Case_Lifecycle,F,2" First name "Case_Lifecycle,G,2" Last name "Case_Lifecycle,H,2" Country "Case_Lifecycle,I,2" Phone number "Case_Lifecycle,J,2" VIN number "Case_Lifecycle,K,2"
    Given User Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    And Verifies Classic customer service case is not present under parts and technical queue
    Then Logout from SVO Portal

  #User is able to create Parts and Technical case through email.
  @SSEC_1230_Case_Assignment_Rules_06 @SSEC-1807
  Scenario: User is able to create Parts and Technical case through email.
    Given Access to mail account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    When Customer send an enquiry mail to user "Case_Lifecycle,F,2"
    Given User Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    And Verifies Classic parts and technical case under parts and technical queue
    Then Logout from SVO Portal

  #User is able to create new Parts and Technical case on salesforce.
  @SSEC_1230_Case_Assignment_Rules_07 @SSEC-1808
  Scenario: User is able to create new Parts and Technical case on salesforce.
    Given Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    And User create new case with record type "Case_Lifecycle,B,2"
    And Verifies Classic parts and technical case under parts and technical queue
    Then Logout from SVO Portal

  #User is able to create new Customer Service case on salesforce.
  @SSEC_1230_Case_Assignment_Rules_08 @SSEC-1809
  Scenario: User is able to create new Customer Service case on salesforce.
    Given Access to SVO Portal as Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    And User create new case with record type "Case_Lifecycle,B,3"
    And Verifies Classic customer service case under customer service queue
    Then Logout from SVO Portal

  #Verify that newly created Parts and Technical case is not available under Customer Service queue which is through email
  @SSEC_1230_Case_Assignment_Rules_09 @SSEC-1810
  Scenario: Verify that newly created Parts and Technical case is not available under Customer Service queue which is through email
    Given Access to mail account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    When Customer send an enquiry mail to user "Case_Lifecycle,F,2"
    Given User Access to SVO Portal as Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    And Verifies Classic parts and technical case is not present under customer service queue
    Then Logout from SVO Portal

  #User is not able to view case under Parts and Technical queue if Customer Service option is selected while creating case through web form
  @SSEC_1230_Case_Assignment_Rules_10 @SSEC-1811
  Scenario: User is not able to view case under Parts and Technical queue if Customer Service option is selected while creating case through web form
    Given Access to the Classic Parts "CaseCreation,L,2" portal
    Then Navigate to Contact Us section
    Then Submit Contact Us form with classic customer service enquiry Email "Case_Lifecycle,F,2" First name "Case_Lifecycle,G,2" Last name "Case_Lifecycle,H,2" Country "Case_Lifecycle,I,2" Phone number "Case_Lifecycle,J,2" VIN number "Case_Lifecycle,K,2"
    Given User Access to SVO Portal as Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    And Verifies Classic customer service case is not present under parts and technical queue
    Then Logout from SVO Portal

  #User is able to create Parts and Technical case through email.
  @SSEC_1230_Case_Assignment_Rules_11 @SSEC-1812
  Scenario: User is able to create Parts and Technical case through email.
    Given Access to mail account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    When Customer send an enquiry mail to user "LoginUsers,B,14"
    Given User Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    And Verifies Classic customer service case is not present under parts and technical queue
    Then Logout from SVO Portal
