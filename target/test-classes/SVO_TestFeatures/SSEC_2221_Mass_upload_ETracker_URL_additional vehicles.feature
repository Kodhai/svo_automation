@SSEC_2221
Feature: Validate Mass upload for E-Tracker and URL for additional vehicles - Etracker Field Datatype Change

  @SSEC_2221_01 @sonica @SSEC_2295
  Scenario: Bespoke User is able to create a new Additional vehicle in Opportunity tab
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User create a SVO Bespoke Opportunity with check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,4" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User navigates to Additional Vehicles from Quick Links
    And Creates a new Additional Vehicle in Opportunity tab
    Then Verify new additional vehicle is created
    Then User logouts from the portal

  @SSEC_2221_02 @sonica @SSEC_2296
  Scenario: Bespoke User is able to fill ETacker URL in the Etracker section
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User create a SVO Bespoke Opportunity with check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,4" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User navigates to Additional Vehicles from Quick Links
    And Creates a new Additional Vehicle in Opportunity tab
    Then Fill Brand Model and ETracker URL
    Then Verify new additional vehicle is created
    Then User logouts from the portal

  @SSEC_2221_03 @sonica @SSEC_2297
  Scenario: Bespoke User is able to fill all the Etracker details in their respective fields
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User create a SVO Bespoke Opportunity with check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,4" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User navigates to Additional Vehicles from Quick Links
    And Creates a new Additional Vehicle in Opportunity tab
    Then Fill Brand Model and ETracker URL
    And Fill ETracker ID into the respective field
    Then Verify new additional vehicle is created
    Then User logouts from the portal

  @SSEC_2221_04 @sonica @SSEC_2298
  Scenario: Bespoke User is able to attach csv file from data import wizard
    Given Access to SVO Portal as Admin user with Username "LoginUsers,B,6" and Password "LoginUsers,C,6"
    And User navigates to Setup window and upload Mass upload csv file
    Then User logouts from the portal

  @SSEC_2221_05 @sonica @SSEC_2299
  Scenario: Bespoke User is able to edit he Etracker URL once saved
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User create a SVO Bespoke Opportunity with check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,4" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User navigates to Additional Vehicles from Quick Links
    And Creates a new Additional Vehicle in Opportunity tab
    Then Edit the ETracker URL
    Then User logouts from the portal
