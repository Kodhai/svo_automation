Feature: SVO JIRA Integration

  #Convert JIRA Status
  @convertJIRA
  Scenario: Convert JIRA status
    Given User convert JIRA Status as per JUNIT Test Status

  #Update the JIRA execution status
  @JIRA
  Scenario: User tries to complete the Task
    Given log into jira with issueKey "JIRA_IssueKey,B,24"
    Then update the execution status in JIRA for issueKey "JIRA_IssueKey,B,24"
