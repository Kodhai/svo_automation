Feature: SVO_Item_To_Approve

  @SVO_Item_To_Approve_01 @SSEC-723 @SVO_Bespoke_Ops_User
  Scenario: User is able to view design brief requests on Design Brief Pending Approvals page
    Given Access to SVO bespoke operation Portal with Username "LoginUsers,B,8" and Password "LoginUsers,C,8"
    And User navigate to Design Brief Pending Approvals section
    Then User verifies that all different fields are available under Design Brief Pending Approvals section
    Then User Verify that Approve, Reject and Reassign buttons are present on Design Brief Pending Approvals page
    And User is able to navigate to respective Opportunity Related To and Submitted By on Design Brief pending approvals page
    And User selects multiple design brief request from the list in order to approve/reject/reassign from Design Brief Pending Approvals page
    Then Logout from an SVO main Portal

  @SVO_Item_To_Approve_02 @SSEC-724 @SVO_Bespoke_User
  Scenario: User is able to view details of an approval request and design brief from Design Brief Pending Approvals page
    Given Access to SVO bespoke Portal with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    And User navigate to Design Brief Pending Approvals section
    Then User views design brief approval requests details from Design Brief Pending Approvals page
    Then Logout from an SVO main Portal

  @SVO_Item_To_Approve_03 @SSEC-725 @SVO_Bespoke_Ops_User
  Scenario: User is not able to view selected design brief's status on Items To Approve page
    Given Access to SVO bespoke operation Portal with Username "LoginUsers,B,8" and Password "LoginUsers,C,8"
    Given laterally Access SVO Portal with user name "LoginUsers,B,8" and Password "LoginUsers,C,8"
    And User navigate to Design Brief Pending Approvals section
    And User is not able to view selected design brief's status on Design Brief pending Approvals page
    Then Logout from an SVO main Portal

  @SVO_Item_To_Approve_04 @SSEC-726 @SVO_Bespoke_User
  Scenario: design brief request is not present on Design Brief Pending Approvals page, if it is not submitted for an approval
    Given Access to SVO bespoke Portal with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Given laterally Access SVO Portal with user name "LoginUsers,B,3" and Password "LoginUsers,C,3"
    When User navigates to an Opportunity tab
    And User create a new SVO Bespoke Opportunity with mandatory fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User create a new Quote with mandatory fields like Retailer Contact "Opportunity,J,2"
    And User not be able to view created Bespoke Design Brief request on Design Brief Pending Approvals page
    Then Logout from an SVO main Portal

  @SVO_Item_To_Approve_05 @SSEC-727 @SVO_Admin_User
  Scenario: User is not able to view Design Brief requests on Design Brief Pending Approvals page, if the record type of the Quote is not Bespoke Design Brief
    Given Access to SVO admin Portal with Username "LoginUsers,B,6" and Password "LoginUsers,C,6"
    When User navigates to an Opportunity tab
    And User create new SVO Private Office Opportunity with mandatory fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,3" Region "Opportunity,D,2" Client "Opportunity,G,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User create a new Quote with mandatory fields like Retailer Contact "Opportunity,J,2"
    And User not able to submit Private Office Design Brief for an approval
    Then Logout from an SVO main Portal

  @SVO_Item_To_Approve_06 @SSEC-728 @SVO_Bespoke_Ops_User
  Scenario: User is not able to view deleted Design Brief request from Design Brief Pending Approvals page
    Given Access to SVO bespoke operation Portal with Username "LoginUsers,B,8" and Password "LoginUsers,C,8"
    #Given laterally Access SVO Portal with user name "LoginUsers,B,8" and Password "LoginUsers,C,8"
    When User navigates to an Opportunity tab
    And User create a new SVO Bespoke Opportunity with mandatory fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User create a new Quote with mandatory fields like Retailer Contact "Opportunity,J,2"
    And User not be able to view deleted Bespoke Design Brief Request on Design Brief Pending Approvals page
    Then Logout from an SVO main Portal

  @SVO_Item_To_Approve_07 @SSEC-729 @SVO_Admin_User
  Scenario: An error occurred when user clicks on Approve/Reject/Reassign button without selecting any design brief from Design Brief Pending Approvals page
    Given Access to SVO admin Portal with Username "LoginUsers,B,6" and Password "LoginUsers,C,6"
    #Given laterally Access SVO Portal with user name "LoginUsers,B,6" and Password "LoginUsers,C,6"
    When User navigates to an Opportunity tab
    And User create a SVO Bespoke Opportunity with fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User create a new Design Brief Quote with fields like Retailer Contact "Opportunity,J,2"
    And User submit Design Brief Request for an approval
    Then login to SVO portal as bespoke user "ItemsToApprove,B,3" via setup login
    Then User clicks on Approve/Reject/Reassign button without selecting any Design Brief request and validate error messege
    Then logout from SVO Portal as bespoke user
    Then Logout from an SVO main Portal

  @SVO_Item_To_Approve_08 @SSEC-730 @Classic_Ops_User
  Scenario: User verify that Items to Approve section is not present on the home page
    Given Access to SVO classic operation Portal with Username "LoginUsers,B,9" and Password "LoginUsers,C,9"
    When User verify that Design brief pending approval section is not present on the Home page
    Then Logout from an SVO main Portal

  @SVO_Item_To_Approve_09 @SSEC-731 @SO_Data_manager
  Scenario: User cannot approve/reject/reassign any design brief request if there is no any approval request on Items to Approve page
    Given Access to SO data management Portal with Username "LoginUsers,B,11" and Password "LoginUsers,C,11"
    And User navigate to Design Brief Pending Approvals section
    When User verify that there is no any Design Brief request present on Design Brief Pending Approvals page
    Then User verify that Approve/Reject/Reassign button is disabled
    Then Logout from an SVO main Portal

  @SVO_Item_To_Approve_010 @SSEC-722 @SVO_Admin_User
  Scenario: User approves selected design brief request from Items to Approve page for Full Bespoke Design Brief
    Given Access to SVO admin Portal with Username "LoginUsers,B,6" and Password "LoginUsers,C,6"
    # Given laterally Access SVO Portal with user name "LoginUsers,B,6" and Password "LoginUsers,C,6"
    When User navigates to an Opportunity tab
    And User create a SVO Bespoke Opportunity with fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User create a new Design Brief Quote with fields like Retailer Contact "Opportunity,J,2"
    And User submit Design Brief Request for an approval
    Then login to SVO portal as bespoke user "ItemsToApprove,B,2" via setup login
    Then Verify that user receives notification for Design brief request
    Then First Approver approves the bespoke design brief approval request
    Then Verify that user receives notification for bespoke Design brief request
    Then logout from SVO Portal as bespoke user
    Then login to SVO portal as bespoke user "ItemsToApprove,B,3" via setup login
    Then Verify that user receives notification for Design brief request
    Then Second Approver approves the bespoke design brief approval request
    Then Verify that user receives notification for bespoke Design brief request
    Then logout from SVO Portal as bespoke user
    Then login to SVO portal as bespoke user "ItemsToApprove,B,4" via setup login
    Then Verify that user receives notification for Design brief request
    Then Third Approver approves the bespoke design brief approval request
    Then Verify that user receives notification for bespoke Design brief request
    Then User navigates to the design brief request details section
    And Verify that Design brief approval request status is Approved from all three Approvers
    Then logout from SVO Portal as bespoke user
    Then Logout from an SVO main Portal

  @SVO_Item_To_Approve_12 @SSEC-720 @SVO_Admin_User
  Scenario: User approves single design brief request from Items to Approve page for Interior Only Design Brief
    Given Access to SVO admin Portal with Username "LoginUsers,B,6" and Password "LoginUsers,C,6"
    #Given laterally Access SVO Portal with user name "LoginUsers,B,6" and Password "LoginUsers,C,6"
    When User navigates to an Opportunity tab
    And User create a SVO Bespoke Opportunity with fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,4" Region "Opportunity,D,2" Client "Opportunity,G,2" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User create a new Design Brief Quote with fields like Retailer Contact "Opportunity,J,2"
    And User submit Design Brief Request for an approval
    Then login to SVO portal as bespoke user "ItemsToApprove,B,2" via setup login
    Then Verify that user receives notification for Design brief request
    Then First Approver approves the bespoke design brief approval request
    Then Verify that user receives notification for bespoke Design brief request
    Then logout from SVO Portal as bespoke user
    Then login to SVO portal as bespoke user "ItemsToApprove,B,3" via setup login
    Then Verify that user receives notification for Design brief request
    Then Second Approver approves the bespoke design brief approval request
    Then Verify that user receives notification for bespoke Design brief request
    Then logout from SVO Portal as bespoke user
    Then login to SVO portal as bespoke user "ItemsToApprove,B,4" via setup login
    Then Verify that user receives notification for Design brief request
    Then Third Approver approves the bespoke design brief approval request
    Then Verify that user receives notification for bespoke Design brief request
    Then User navigates to the design brief request details section
    And Verify that Design brief approval request status is Approved from all three Approvers
    Then logout from SVO Portal as bespoke user
    Then Logout from an SVO main Portal

  @SVO_Item_To_Approve_13 @SSEC-721 @SVO_Admin_User
  Scenario: User approves multiple design brief requests from Items to Approve page for Exterior Only Design Brief
    Given Access to SVO admin Portal with Username "LoginUsers,B,6" and Password "LoginUsers,C,6"
    #Given laterally Access SVO Portal with user name "LoginUsers,B,6" and Password "LoginUsers,C,6"
    When User navigates to an Opportunity tab
    And User create a SVO Bespoke Opportunity with fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,5" Region "Opportunity,D,2" Client "Opportunity,G,2" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User create a new Design Brief Quote with fields like Retailer Contact "Opportunity,J,2"
    And User submit Design Brief Request for an approval
    Then User navigates to approval history on design brief page
    Then User navigate back to the Opportunity page
    Then User create second Design Brief Quote with fields like Retailer Contact "Opportunity,J,2"
    Then User submit the second Design Brief Request for approval
    Then login to SVO portal as bespoke user "ItemsToApprove,B,2" via setup login
    Then Verify that user receives notification for Design brief request
    Then First Approver approves multiple bespoke design brief approval request
    Then Verify that user receives notification for bespoke Design brief request
    Then logout from SVO Portal as bespoke user
    Then login to SVO portal as bespoke user "ItemsToApprove,B,3" via setup login
    Then Verify that user receives notification for Design brief request
    Then Second Approver approves multiple bespoke design brief approval request
    Then Verify that user receives notification for bespoke Design brief request
    Then logout from SVO Portal as bespoke user
    Then login to SVO portal as bespoke user "ItemsToApprove,B,4" via setup login
    Then Verify that user receives notification for Design brief request
    Then Third Approver approves First bespoke design brief approval request
    Then Verify that user receives notification for bespoke Design brief request
    Then User navigates to the design brief request details section
    And Verify that Design brief approval request status is Approved from all three Approvers
    Then Third Approver approves second bespoke design brief approval request
    Then Verify that user receives notification for bespoke Design brief request
    Then User navigates to the design brief request details section
    And Verify that Design brief approval request status is Approved from all three Approvers
    Then logout from SVO Portal as bespoke user
    Then Logout from an SVO main Portal

  @SVO_Item_To_Approve_14 @SSEC-722 @SVO_Admin_User
  Scenario: User is able to cancel approval of any selected design brief request from Items to Approve page
    Given Access to SVO admin Portal with Username "LoginUsers,B,6" and Password "LoginUsers,C,6"
    #Given laterally Access SVO Portal with user name "LoginUsers,B,6" and Password "LoginUsers,C,6"
    When User navigates to an Opportunity tab
    And User create a SVO Private Office Opportunity with fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,3" Region "Opportunity,D,2" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User create a new Design Brief Quote with fields like Retailer Contact "Opportunity,J,2"
    And User submit Design Brief Request for an approval
    Then User navigates to approval history on design brief page
    Then login to SVO portal as bespoke user "ItemsToApprove,B,2" via setup login
    Then Verify that user receives notification for Design brief request
    And User navigate to Design Brief pending approvals section
    Then User select design brief request for an approval
    And User cancel the reassign after entering reassignto "ItemsToApprove,A,3" user
    When User navigates to an Opportunity tab
    Then User navigates to the design brief section of an opportunity
    Then User navigates to approval history on design brief page
    And Verify that Design brief request approver is not updated to reassigned user "ItemsToApprove,A,3"
    Then Verify that Approval status is set to be pending Approval on Approval history page
    Then logout from SVO Portal as bespoke user
    Then Logout from an SVO main Portal

  @SVO_Item_To_Approve_16 @SSEC-732 @SVO_Admin_User
  Scenario: User rejects selected design brief request from Items to Approve page
    Given Access to SVO admin Portal with Username "LoginUsers,B,6" and Password "LoginUsers,C,6"
    #Given laterally Access SVO Portal with user name "LoginUsers,B,6" and Password "LoginUsers,C,6"
    When User navigates to an Opportunity tab
    And User create a SVO Bespoke Opportunity with fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User create a new Design Brief Quote with fields like Retailer Contact "Opportunity,J,2"
    And User submit Design Brief Request for an approval
    Then login to SVO portal as bespoke user "ItemsToApprove,B,2" via setup login
    Then Verify that user receives notification for Design brief request
    And User navigate to Design Brief pending approvals section
    Then User select design brief request for an approval
    Then User Reject the bespoke design brief approval request from design brief pending Approvals section
    Then Verify that user receives notification for bespoke Design brief request
    When User navigates to an Opportunity tab
    Then User navigates to the design brief section of an opportunity
    Then User navigates to approval history on design brief page
    And Verify that Design brief approval request status is updated to Rejected status
    Then logout from SVO Portal as bespoke user
    Then Logout from an SVO main Portal

  @SVO_Item_To_Approve_17 @SSEC-733 @SVO_Admin_User
  Scenario: User is able to cancel rejection of any selected design brief request from Items to Approve page
    Given Access to SVO admin Portal with Username "LoginUsers,B,6" and Password "LoginUsers,C,6"
    #Given laterally Access SVO Portal with user name "LoginUsers,B,6" and Password "LoginUsers,C,6"
    When User navigates to an Opportunity tab
    And User create a SVO Bespoke Opportunity with fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User create a new Design Brief Quote with fields like Retailer Contact "Opportunity,J,2"
    And User submit Design Brief Request for an approval
    Then User navigates to approval history on design brief page
    Then login to SVO portal as bespoke user "ItemsToApprove,B,2" via setup login
    Then Verify that user receives notification for Design brief request
    And User navigate to Design Brief pending approvals section
    Then User select design brief request for an approval
    And User cancel the reassign after entering reassignto "ItemsToApprove,A,3" user
    When User navigates to an Opportunity tab
    Then User navigates to the design brief section of an opportunity
    Then User navigates to approval history on design brief page
    And Verify that Design brief request approver is not updated to reassigned user "ItemsToApprove,A,3"
    Then logout from SVO Portal as bespoke user
    Then Logout from an SVO main Portal

  @SVO_Item_To_Approve_18 @SSEC-734 @SVO_Admin_User
  Scenario: User rejects selected design brief request from Items to Approve page, if the quote outcome is accepted
    Given Access to SVO admin Portal with Username "LoginUsers,B,6" and Password "LoginUsers,C,6"
    #Given laterally Access SVO Portal with user name "LoginUsers,B,6" and Password "LoginUsers,C,6"
    When User navigates to an Opportunity tab
    And User create a SVO Bespoke Opportunity with fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User moves the selected opportunity to Design Brief & Quote stage
    Then User create and accept the Design brief Quote of an opportunity "Opportunity,J,2"
    And User submit Design Brief Request for an approval
    Then login to SVO portal as bespoke user "ItemsToApprove,B,2" via setup login
    Then Verify that user receives notification for Design brief request
    And User navigate to Design Brief pending approvals section
    Then User select design brief request for an approval
    Then User Reject the bespoke design brief approval request from design brief pending Approvals section
    Then Verify that user receives notification for bespoke Design brief request
    When User navigates to an Opportunity tab
    Then User navigates to the design brief section of an opportunity
    Then User navigates to approval history on design brief page
    And Verify that Design brief approval request status is updated to Rejected status
    Then logout from SVO Portal as bespoke user
    Then Logout from an SVO main Portal

  @SVO_Item_To_Approve_19 @SSEC-735 @SVO_Admin_User
  Scenario: User is able to reject multiple design brief requests from Items to Approve page
    Given Access to SVO admin Portal with Username "LoginUsers,B,6" and Password "LoginUsers,C,6"
    #Given laterally Access SVO Portal with user name "LoginUsers,B,6" and Password "LoginUsers,C,6"
    When User navigates to an Opportunity tab
    And User create a SVO Bespoke Opportunity with fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User create a new Design Brief Quote with fields like Retailer Contact "Opportunity,J,2"
    And User submit Design Brief Request for an approval
    Then User navigates to approval history on design brief page
    Then User navigate back to the Opportunity page
    Then User create second Design Brief Quote with fields like Retailer Contact "Opportunity,J,2"
    Then User submit the second Design Brief Request for approval
    Then User navigates to approval history on design brief page
    Then login to SVO portal as bespoke user "ItemsToApprove,B,2" via setup login
    Then Verify that user receives notification for Design brief request
    And User navigate to Design Brief pending approvals section
    Then User select multiple design brief request for reject
    Then User Reject the bespoke design brief approval request from design brief pending Approvals section
    Then Verify that user receives notification for bespoke Design brief request
    When User navigates to an Opportunity tab
    Then User navigates to the design brief section of an opportunity
    Then User navigates to approval history on design brief page
    And Verify that Design brief approval request status is updated to Rejected status
    Then logout from SVO Portal as bespoke user
    Then Logout from an SVO main Portal

  @SVO_Item_To_Approve_v2_21 @SSEC-737 @SVO_Admin_User
  Scenario: User is able to approve/reject selected design brief request from Items to Approve section placed on Home page
    Given Access to SVO admin Portal with Username "LoginUsers,B,6" and Password "LoginUsers,C,6"
    #Given laterally Access SVO Portal with user name "LoginUsers,B,6" and Password "LoginUsers,C,6"
    When User navigates to an Opportunity tab
    And User create a SVO Bespoke Opportunity with fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User create a new Design Brief Quote with fields like Retailer Contact "Opportunity,J,2"
    And User submit Design Brief Request for an approval
    Then login to SVO portal as bespoke user "ItemsToApprove,B,2" via setup login
    Then Verify that user receives notification for Design brief request
    And User navigate to Design Brief pending approvals section
    Then User select design brief request for an approval
    Then User Reject the bespoke design brief approval request from design brief pending Approvals section
    Then Verify that user receives notification for bespoke Design brief request
    When User navigates to an Opportunity tab
    Then User navigates to the design brief section of an opportunity
    Then User navigates to approval history on design brief page
    And Verify that Design brief approval request status is updated to Rejected status
    Then logout from SVO Portal as bespoke user
    Then Logout from an SVO main Portal

  @SVO_Item_To_Approve_22 @SSEC-738 @SVO_Admin_User
  Scenario: User reassigns selected design brief request from Items to Approve page
    Given Access to SVO admin Portal with Username "LoginUsers,B,6" and Password "LoginUsers,C,6"
    #Given laterally Access SVO Portal with user name "LoginUsers,B,6" and Password "LoginUsers,C,6"
    When User navigates to an Opportunity tab
    And User create a SVO Bespoke Opportunity with fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User create a new Design Brief Quote with fields like Retailer Contact "Opportunity,J,2"
    And User submit Design Brief Request for an approval
    Then User navigates to approval history on design brief page
    Then login to SVO portal as bespoke user "ItemsToApprove,B,2" via setup login
    Then Verify that user receives notification for Design brief request
    And User navigate to Design Brief pending approvals section
    Then User select design brief request for an approval
    And User reassign the design brief to the "ItemsToApprove,A,2" user
    Then Verify that user receives notification for bespoke Design brief request
    When User navigates to an Opportunity tab
    Then User navigates to the design brief section of an opportunity
    Then User navigates to approval history on design brief page
    And Verify that Design brief approver is updated to reassigned user "ItemsToApprove,A,2"
    Then logout from SVO Portal as bespoke user
    Then Logout from an SVO main Portal

  @SVO_Item_To_Approve_23 @SSEC-739 @SVO_Admin_User
  Scenario: User cancels the reassignments of any selected design brief request from Items to Approve page
    Given Access to SVO admin Portal with Username "LoginUsers,B,6" and Password "LoginUsers,C,6"
    #Given laterally Access SVO Portal with user name "LoginUsers,B,6" and Password "LoginUsers,C,6"
    When User navigates to an Opportunity tab
    And User create a SVO Bespoke Opportunity with fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User create a new Design Brief Quote with fields like Retailer Contact "Opportunity,J,2"
    And User submit Design Brief Request for an approval
    Then User navigates to approval history on design brief page
    Then login to SVO portal as bespoke user "ItemsToApprove,B,2" via setup login
    Then Verify that user receives notification for Design brief request
    And User navigate to Design Brief pending approvals section
    Then User select design brief request for an approval
    And User cancel the reassign after entering reassignto "ItemsToApprove,A,3" user
    When User navigates to an Opportunity tab
    Then User navigates to the design brief section of an opportunity
    Then User navigates to approval history on design brief page
    And Verify that Design brief approver is still assigned to user "ItemsToApprove,B,2"
    Then logout from SVO Portal as bespoke user
    Then Logout from an SVO main Portal

  @SVO_Item_To_Approve_24 @SSEC-740 @SVO_Admin_User
  Scenario: User is not able to reassign design brief request to the invalid/inactive user from Items to Approve page
    Given Access to SVO admin Portal with Username "LoginUsers,B,6" and Password "LoginUsers,C,6"
    #Given laterally Access SVO Portal with user name "LoginUsers,B,6" and Password "LoginUsers,C,6"
    When User navigates to an Opportunity tab
    And User create a SVO Bespoke Opportunity with fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User create a new Design Brief Quote with fields like Retailer Contact "Opportunity,J,2"
    And User submit Design Brief Request for an approval
    Then User navigates to approval history on design brief page
    Then login to SVO portal as bespoke user "ItemsToApprove,B,2" via setup login
    Then Verify that user receives notification for Design brief request
    And User navigate to Design Brief pending approvals section
    Then User select design brief request for an approval
    And User reassign the design brief to the invalid "ItemsToApprove,A,4" user
    And Verify that user is not able to reassign the design brief approval to invalid user
    Then logout from SVO Portal as bespoke user
    Then Logout from an SVO main Portal

  @SVO_Item_To_Approve_25 @SSEC-837 @SVO_Admin_User
  Scenario: User is not able to reassign design brief request without adding  user name in reassign to user field on Design brief pending approvals page
    Given Access to SVO admin Portal with Username "LoginUsers,B,6" and Password "LoginUsers,C,6"
    #Given laterally Access SVO Portal with user name "LoginUsers,B,6" and Password "LoginUsers,C,6"
    When User navigates to an Opportunity tab
    And User create a SVO Bespoke Opportunity with fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User create a new Design Brief Quote with fields like Retailer Contact "Opportunity,J,2"
    And User submit Design Brief Request for an approval
    Then User navigates to approval history on design brief page
    Then login to SVO portal as bespoke user "ItemsToApprove,B,2" via setup login
    Then Verify that user receives notification for Design brief request
    And User navigate to Design Brief pending approvals section
    Then User select design brief request for an approval
    Then User hits on reassign without adding reassign user on design brief pending approvals page
    And Verify that user is not able to reassign the design brief approval without adding any user
    Then logout from SVO Portal as bespoke user
    Then Logout from an SVO main Portal
