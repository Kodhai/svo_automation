 Feature: SVO 
  #User is able to automatically send holding response to customer's Classic Sales enquiry 
  @SVO @SVO_2497_1
  Scenario: User is able to automatically send holding response to customers Classic Sales enquiry 
    Given Access to SVO Portal as Classic Sales user with Username "LoginUsers,B,5" and password "LoginUsers,C,5"
     Then User navigates to the Enquiries tab
    Then User Creates a New Enquiry with Record Type "Enquiry,A,4"
    Then Create a new enquiry with title "Enquiry,R,2" source KMI "Enquiry,AB,2"
    Then Save the new Enquiry
    Then User send Holding Response email to customer "LoginUsers,B,17" with Business unit "CustomerResponse,I,3"
    Then Logout from the SVO Portal
    
    #Verify that created Classic Service enquiry status set to New stage 
  @SVO @SVO_2497_2 @Shifa16
  Scenario: Verify that created Classic Service enquiry status set to New stage
    Given Access to SVO Portal as Classic Service user with Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
     Then User navigates to the Enquiries tab
    Then User Creates a New Enquiry with Record Type "Enquiry,A,4"
    Then Create a new enquiry with title "Enquiry,R,2" source KMI "Enquiry,AB,2"
    Then Save the new Enquiry
    Then User send Holding Response email to customer "LoginUsers,B,17" with Business unit "CustomerResponse,I,3"
    And user Verify that created Classic Service enquiry status set to New stage
    Then Logout from the SVO Portal
    
     #Verify that created Classic sales enquiry stage not moved to contacted stage 
  @SVO @SVO_2497_3 @Shifa16
  Scenario: Verify that created Classic sales enquiry stage not moved to contacted stage  
    Given Access to SVO Portal as Classic Sales user with Username "LoginUsers,B,5" and password "LoginUsers,C,5"
     Then User navigates to the Enquiries tab
    Then User Creates a New Enquiry with Record Type "Enquiry,A,4"
    Then Create a new enquiry with title "Enquiry,R,2" source KMI "Enquiry,AB,2"
    Then Save the new Enquiry
    Then User send Holding Response email to customer "LoginUsers,B,17" with Business unit "CustomerResponse,I,3"
    And Verify that created Classic sales enquiry stage not moved to contacted stage
    Then Logout from the SVO Portal
    
    #Verify that created Classic Service enquiry stage not moved to contacted stage 
  @SVO @SVO_2497_4
  Scenario: Verify that created Classic Service enquiry stage not moved to contacted stage   
    Given Access to SF SVO Portal as Classic Service user with Username "LoginUsers,B,5" and Password "LoginUsers,C,5"
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,4"
    Then Create a new enquiry with title "Enquiry,R,2" source KMI "Enquiry,AB,2"
    Then Save the new Enquiry
    Then User send Holding Response email to customer "LoginUsers,B,17" with Business unit "CustomerResponse,I,3"
    Then Logout from the SVO Portal
    
     #User is not able to send a holding response to customer If enquiry state is already contacted
  @SVO @SVO_2497_6
  Scenario: User is not able to send a holding response to customer If enquiry state is already contacted
    Given Access to SVO Portal as Classic Service user with Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    When User Navigate to Enquiries
    Then User Creates a New Enquiry with Record Type "Enquiry,A,4"
    Then Create a new enquiry with title "Enquiry,R,2" source KMI "Enquiry,AB,2"
    Then Save the new Enquiry
     And Log a call to the Enquiry
    #Verify that status is already contacted
    Then Logout from the SVO Portal
