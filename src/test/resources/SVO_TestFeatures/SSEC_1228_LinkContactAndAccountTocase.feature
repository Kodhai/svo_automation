Feature: Validations for Link contact and account to Case

  #User is able to link existing Contact and Account to Customer Service Case created on Salesforce
  @SSEC_1228_LinkContactAndAccountTocase_01 @Automation_SSEC-1650
  Scenario: User is able to link existing Contact and Account to Customer Service Case created on Salesforce
    Given Login to SVO Portal by Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User Navigate to Cases Tab
    And Create a new case of record type "CaseCreation,C,2" by linking case with existing Account "Opportunity,I,2" and Contact "Opportunity,J,2" details
    Then Verify that newly created case is linked with existing Account and Contact
    And logout from user login via set up
    Then Logouts from the SVO Portal

  #User is able to link existing Contact and Account to Parts and Technical Case created on Salesforce
  @SSEC_1228_LinkContactAndAccountTocase_02 @Automation_SSEC-1651
  Scenario: User is able to link existing Contact and Account to Parts and Technical Case created on Salesforce
    Given Login to SVO Portal by Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User Navigate to Cases Tab
    And Create a new case of record type "CaseCreation,C,3" by linking case with existing Account "Opportunity,I,2" and Contact "Opportunity,J,2" details
    Then Verify that newly created case is linked with existing Account and Contact
    Then Logouts from the SVO Portal

  #User is able to link new contact to the Customer Service case created on Salesforce
  @SSEC_1228_LinkContactAndAccountTocase_03 @Automation_SSEC-1652
  Scenario: User is able to link new contact to the Customer Service case created on Salesforce
    Given Login to SVO Portal by Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    And User Navigate to Contacts Tab
    Then Create a new contact with contact name "CaseCreation,D,2" and AccountName "CaseCreation,E,2"
    And Verify new contact is created on salesforce
    When User Navigate to Cases Tab
    And Create a new case of record type "CaseCreation,C,2" by linking case with newly created Contact
    Then Verify that case is linked with newly created contact along with autopopulated Account field
    Then Logouts from the SVO Portal

  #User is able to link existing Contact and Account to Parts and Technical Case created through Email
  @SSEC_1228_LinkContactAndAccountTocase_04 @Automation_SSEC-1653
  Scenario: User is able to link existing Contact and Account to Parts and Technical Case created through Email
    Given Login to email account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    When Customer sends a case creation mail to user "CaseCreation,A,2"
    And Logout from the user email account
    Then Verify User receives an outlook email notification on Parts and Technical case creation
    Given Access to SVO Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User Navigate to Cases Tab
    And Verify that Parts and Technical case is automatically created on SVO Portal
    Then Verify that newly created case is linked with existing Account and Contact
    Then Logouts from the SVO Portal

  #User is not able to link existing Contact for Customer Service Case as it is not available to create through Email
  @SSEC_1228_LinkContactAndAccountTocase_05 @Automation_SSEC-1715
  Scenario: User is not able to link existing Contact for Customer Service Case as it is not available to create through Email
    Given Login to email account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    When Customer sends a case creation mail to user "CaseCreation,A,2"
    And Logout from the user email account
    Given Login to the SVO Portal by Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User Navigate to Cases Tab
    And Verify that Classic Customer Service case is not created on SVO Portal
    Then Logouts from the SVO Portal

  #User is able to link existing Contact and Account to Parts and Technical Case created through Web
  @SSEC_1228_LinkContactAndAccountTocase_06 @Automation_SSEC-1654
  Scenario: User is able to link existing Contact and Account to Parts and Technical Case created through Web
    Given Login to an Classic Parts portal
    Then Navigate to Contact Us section
    Then Create Parts and Technical Case from web form with Email "CaseCreation,A,2" First name "CaseCreation,G,2" Last name "CaseCreation,H,2" Country "CaseCreation,I,2" Phone number "CaseCreation,K,2" VIN number "CaseCreation,J,2"
    Then Verify User receives an outlook email notification on Parts and Technical case creation
    Given Access to SVO Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User Navigate to Cases Tab
    And Verify that Parts and Technical case is automatically created on SVO Portal
    Then Verify that newly created case through web is linked with existing Account and Contact
    Then Logouts from the SVO Portal

  #User is able to link existing Contact and Account to Customer Service Case created through Web
  @SSEC_1228_LinkContactAndAccountTocase_07 @Automation_SSEC-1655
  Scenario: User is able to link existing Contact and Account to Customer Service Case created through Web.
    Given Login to an Classic Parts portal
    Then Navigate to Contact Us section
    Then Create Customer Service Case from web form with Email "CaseCreation,A,2" First name "CaseCreation,G,2" Last name "CaseCreation,H,2" Country "CaseCreation,I,2" Phone number "CaseCreation,K,2" VIN number "CaseCreation,J,2"
    Then Verify User receives an outlook email notification on Classic Service case creation
    Given Login to the SVO Portal by Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User Navigate to Cases Tab
    And Verify that Classic Customer Service case is automatically created on SVO Portal
    Then Verify that newly created case through web is linked with existing Account and Contact
    Then Logouts from the SVO Portal

  #User is able to link new contact to Parts and Technical case created through Email
  @SSEC_1228_LinkContactAndAccountTocase_08 @Automation_SSEC-1656
  Scenario: User is able to link new contact to Parts and Technical case created through Email
    Given Login to email account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    When Customer sends a case creation mail to user "CaseCreation,A,2"
    And Logout from the user email account
    Then Verify User receives an outlook email notification on Parts and Technical case creation
    Given Access to SVO Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    And User Navigate to Contacts Tab
    Then Create a new contact with contact name "CaseCreation,D,2" and AccountName "CaseCreation,E,2"
    And Verify new contact is created on salesforce
    When User Navigate to Cases Tab
    And Verify that Parts and Technical case is automatically created on SVO Portal
    When User link the case with newly created contact
    And Verify that case is linked with newly created contact along with autopopulated Account field
    Then Logouts from the SVO Portal

  #User is able to link new contact to Parts and Technical case created through web
  @SSEC_1228_LinkContactAndAccountTocase_09 @Automation_SSEC-1657
  Scenario: User is able to link new contact to Parts and Technical case created through web
    Given Login to an Classic Parts portal
    Then Navigate to Contact Us section
    Then Create Parts and Technical Case from web form with Email "CaseCreation,A,2" First name "CaseCreation,G,2" Last name "CaseCreation,H,2" Country "CaseCreation,I,2" Phone number "CaseCreation,K,2" VIN number "CaseCreation,J,2"
    Then Verify User receives an outlook email notification on Parts and Technical case creation
    Given Access to SVO Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    And User Navigate to Contacts Tab
    Then Create a new contact with contact name "CaseCreation,D,2" and AccountName "CaseCreation,E,2"
    And Verify new contact is created on salesforce
    When User Navigate to Cases Tab
    And Verify that Parts and Technical case is automatically created on SVO Portal
    When User link the case with newly created contact
    And Verify that case is linked with newly created contact along with autopopulated Account field
    Then Logouts from the SVO Portal

  #User is able to link new contact to Customer Service case created through web
  @SSEC_1228_LinkContactAndAccountTocase_10 @Automation_SSEC-1658
  Scenario: User is able to link new contact to Customer Service case created through web
    Given Login to an Classic Parts portal	
    Then Navigate to Contact Us section
    Then Create Customer Service Case from web form with Email "CaseCreation,A,2" First name "CaseCreation,G,2" Last name "CaseCreation,H,2" Country "CaseCreation,I,2" Phone number "CaseCreation,K,2" VIN number "CaseCreation,J,2"
    Then Verify User receives an outlook email notification on Classic Service case creation
    Given Login to the SVO Portal by Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    And User Navigate to Contacts Tab
    Then Create a new contact with contact name "CaseCreation,D,2" and AccountName "CaseCreation,E,2"
    And Verify new contact is created on salesforce
    When User Navigate to Cases Tab
    And Verify that Classic Customer Service case is automatically created on SVO Portal
    When User link the case with newly created contact
    Then Verify that case is linked with newly created contact along with autopopulated Account field
    Then Logouts from the SVO Portal

  #User is not able to link Contact to Customer Service case if Cases page is not available
  @SSEC_1228_LinkContactAndAccountTocase_11 @Automation_SSEC-1659
  Scenario: User is not able to link Contact to Customer Service case if Cases page is not available
    Given Login to SVO Portal with Classic Sales user with the Username "LoginUsers,B,5" and Password "LoginUsers,C,5"
    And Verify that cases tab is not available so that user is not able to link Contact to Customer Service case
    Then Logouts from the SVO Portal
