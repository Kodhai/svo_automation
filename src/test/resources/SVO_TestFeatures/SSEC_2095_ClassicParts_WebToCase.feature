@SSEC_2095
Feature: Validate Classic Parts Web To Case Scenarios

  #User is able to submit and view the case for Classic Parts and Technical enquiry
  @SSEC_2095_01 @Sonica
  Scenario: User is able to create new case and logout
    Given Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User navigates to cases tab
    And User creates new Parts and Technical Record type case with mandatory fields like VIN
    Then User saves the case
    And User validates the new case created with the VIN
    And User logout from the portal

  #User is able to submit and view the case for Classic Customer Service enquiry
  @SSEC_2095_02 @Sonica
  Scenario: User is able to submit and view the case for Classic Customer Service enquiry
    Given User login to SVO Portal by Classic Customer Service user with Username "LoginUsers, B, 21" and Password "LoginUsers, C, 21"
    When User navigates to cases tab
    And User creates new Customer services case with all mandatory fields
    Then User saves the case
    Then User validates new cust case
    And User logout from the portal

  @SSEC_2095_03 @Sonica
  Scenario: User is able to verify all the fields for cases filled in the Salesforce portal
    Given Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User navigates to cases tab
    And Open an existing case record
    And Verify all the fields in the Cases record
    And User logout from the portal

  @SSEC_2095_04 @Sonica
  Scenario: User is able to edit the Parts and Technical cases fields in the salesforce
    Given Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User navigates to cases tab
    And Open an existing case record
    Then Edit First name of the case
    And User logout from the portal

  @SSEC_2095_05 @Sonica
  Scenario: User is able to verify that the case created for Classic Parts and Technical goes to Parts and Technical queue
    Given Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User navigates to cases tab
    And Open an existing case record
    And Verify that the case has Parts and Technical Queue as Case Owner
    And User logout from the portal

  @SSEC_2095_06  @Sonica
  Scenario: User is able to verify that the case created for Classic Customer Service goes to Customer Service queue
    Given User login to SVO Portal by Classic Customer Service user with Username "LoginUsers, B, 21" and Password "LoginUsers, C, 21"
    When User navigates to cases tab
    And Open an existing case record
    And Verify that the case has Parts and Technical Queue as Case Owner
    And User logout from the portal
    
    
  @SSEC_2095_07  @Sonica
  Scenario: User creates a new case
  Given Access to SVO portal
