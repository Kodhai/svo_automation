@SSEC_2157
Feature: Validate Bespoke Order form Design Brief Amendments

  @SSEC_2157_01 @sonica
  Scenario: Bespoke User is able to select seatbelts for front and rear seats
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User creates a Bespoke Opportunity with check cleared like Stage "Opportunity,B,3" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,I,3" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,3"
    Then User verifies that new Opportunity is created
    Then User navigates to Quotes from Related Links
    And User creates new Bespoke Design Brief quotes "Opportunity,J,4" with mandatory details
    And Click on the new Bespoke Design Brief quote created successfully
    Then Click on Edit button of front seat belt color and select the color of frontseatbelt "DesignBrief,A,2" and rearseatbelt "DesignBrief,B,2"
    Then Click on save button of Design Brief
    Then User Logout from SVO Portal

  @SSEC_2157_02 @sonica
  Scenario: Bespoke User is able to select cabin carpet color on Design Brief tab
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User creates a Bespoke Opportunity with check cleared like Stage "Opportunity,B,3" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,I,3" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,3"
    Then User verifies that new Opportunity is created
    Then User navigates to Quotes from Related Links
    And User creates new Bespoke Design Brief quotes "Opportunity,J,4" with mandatory details
    And Click on the new Bespoke Design Brief quote created successfully
    Then Click on Edit button of cabin carpet color and select the color "DesignBrief,C,2"
    Then Click on save button of Design Brief
    Then User Logout from SVO Portal

  @SSEC_2157_03 @sonica
  Scenario: Bespoke User is able to select fridge details like fridge color, material and Veneer color
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User creates a Bespoke Opportunity with check cleared like Stage "Opportunity,B,3" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,I,3" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,3"
    Then User verifies that new Opportunity is created
    Then User navigates to Quotes from Related Links
    And User creates new Bespoke Design Brief quotes "Opportunity,J,4" with mandatory details
    And Click on the new Bespoke Design Brief quote created successfully
    Then Click on Edit button for fridge material and select the material "DesignBrief,D,2" color "DesignBrief,E,2" and veneer color "DesignBrief,F,2"
    Then Click on save button of Design Brief
    Then User Logout from SVO Portal

  @SSEC_2157_04 @sonica
  Scenario: Bespoke User is able to select Headliner details like headliner color, and material
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User creates a Bespoke Opportunity with check cleared like Stage "Opportunity,B,3" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,I,3" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,3"
    Then User verifies that new Opportunity is created
    Then User navigates to Quotes from Related Links
    And User creates new Bespoke Design Brief quotes "Opportunity,J,4" with mandatory details
    And Click on the new Bespoke Design Brief quote created successfully
    Then Click on Edit button for Headliner material and select the material "DesignBrief,G,2" and color "DesignBrief,H,2"
    Then Click on save button of Design Brief
    Then User Logout from SVO Portal

  @SSEC_2157_05 @sonica
  Scenario: Bespoke User is able to edit sealbelts color once saved
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User creates a Bespoke Opportunity with check cleared like Stage "Opportunity,B,3" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,I,3" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,3"
    Then User verifies that new Opportunity is created
    Then User navigates to Quotes from Related Links
    And User creates new Bespoke Design Brief quotes "Opportunity,J,4" with mandatory details
    And Click on the new Bespoke Design Brief quote created successfully
    Then Click on Edit button for front seat belt color and select the seat color as Ebony "DesignBrief,A,2"
    Then Click on save button of Design Brief
    Then User Logout from SVO Portal

  @SSEC_2157_06 @sonica
  Scenario: User is able to verify that when fridge material is changed to PU, then the color list also changes accordingly
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User creates a Bespoke Opportunity with check cleared like Stage "Opportunity,B,3" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,I,3" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,3"
    Then User verifies that new Opportunity is created
    Then User navigates to Quotes from Related Links
    And User creates new Bespoke Design Brief quotes "Opportunity,J,4" with mandatory details
    And Click on the new Bespoke Design Brief quote created successfully
    Then Click on Edit button for fridge material and select the material as PU "DesignBrief,D,3"
    Then Verify the color list for fridge is changed according to the material
    Then Click on save button of Design Brief
    Then User Logout from SVO Portal
    