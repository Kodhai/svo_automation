Feature: SVO Opportunity Secondary Owner Validations

  @SVO_OpportunitySecondaryOwner_01 @SSEC-1265 @SVO_OpportunitySecondaryOwner_Batch
  Scenario: User is able to add only Classic User as an Secondary Owner for any Classic Opportunity
    Given Access to an SVO Portal as Classic Admin user with Username "LoginUsers,B,13" and Password "LoginUsers,C,13"
    When User navigates to an Opportunity tab
    And User creates Classic Opportunity with all mandatory fields like Stage "Opportunity,B,2" Record Type "OpportunitySecondaryOwner,B,3" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" SAP Account "Opportunity,Q,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then Verify that opportunity is in Qualified stage
    And User adds classic user "OpportunitySecondaryOwner,A,2" as secondary owner for an classic opportunity
    Then User Logout from SVO Portal
    And Access to SVO Portal as classic service user by add username with Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    Then User navigates to an Opportunity tab
    And User views classic opportunity under secondary opportunities list
    Then Verify that the details of classic opportunity
    And User send an email to customer "LoginUsers,B,17" with Business unit "CustomerResponse,I,3" and subject
    Then Logout from the Special vehicle operations Portal

  @SVO_OpportunitySecondaryOwner_02 @SSEC-1266 @SVO_OpportunitySecondaryOwner_Batch
  Scenario: User is able to add & edit Classic user as an Secondary Owner for any existing Classic Opportunity
    Given Access to SVO Portal as classic Sales user with Username "LoginUsers,B,5" and Password "LoginUsers,C,5"
    When User navigates to an Opportunity tab
    Then User select any existing classic opportunity
    And User adds classic user "OpportunitySecondaryOwner,A,2" as secondary owner for an classic opportunity
    Then Verify that updated secondary owner field on opportunity page
    And User edit classic user "OpportunitySecondaryOwner,A,4" as secondary owner for an classic opportunity
    Then Verify that updated secondary owner field on opportunity page
    Then Logout from the Special vehicle operations Portal

  @SVO_OpportunitySecondaryOwner_03 @SSEC-1267 @SVO_OpportunitySecondaryOwner_Batch
  Scenario: An Secondary Owner is able to edit any classic opportunity
    Given Access to SVO Portal as Classic Admin user with Username "LoginUsers,B,13" and Password "LoginUsers,C,13"
    When User navigates to an Opportunity tab
    And User creates Classic Limitted Edition Opportunity with all mandatory fields like Stage "Opportunity,B,2" Record Type "OpportunitySecondaryOwner,B,5" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" SAP Account "Opportunity,Q,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Programme "BillingAddress,B,3" Model "Opportunity,M,2"
    Then Verify that opportunity is in Qualified stage
    And User adds classic user "OpportunitySecondaryOwner,A,2" as secondary owner for an classic opportunity
    Then User Logout from SVO Portal
    And Access to SVO Portal as classic service user by add username with Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    Then User navigates to an Opportunity tab
    And User views classic opportunity under secondary opportunities list
    Then Verify that the details of classic opportunity
    Then User send an email to customer "LoginUsers,B,17" with Business unit "CustomerResponse,I,3" and subject
    And User edit opportunity details for classic opportunity
    Then Verify that updated details on opportunity page
    Then Logout from the Special vehicle operations Portal

  @SVO_OpportunitySecondaryOwner_04 @SSEC-1268 @SVO_OpportunitySecondaryOwner_Batch
  Scenario: An Secondary Owner is able to close the assigned opportunity
    Given Access to SVO Portal as classic Sales user with Username "LoginUsers,B,5" and Password "LoginUsers,C,5"
    When User navigates to an Opportunity tab
    And User create an Classic Works Legend Opportunity with fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then Verify that opportunity is in Qualified stage
    And User adds classic user "OpportunitySecondaryOwner,A,2" as secondary owner for an classic opportunity
    Then User Logout from SVO Portal
    And Access to SVO Portal as classic service user by add username with Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    Then User navigates to an Opportunity tab
    And User views classic opportunity under secondary opportunities list
    Then Verify that the details of classic opportunity
    Then User send an email to customer "LoginUsers,B,17" with Business unit "CustomerResponse,I,3" and subject
    Then Move the Opportunity to closed won stage with Outcome as "CancelOrder,A,5"
    And User navigate back to Opportunities page
    And Verify that opportunity is in Closed Won stage
    Then Logout from the Special vehicle operations Portal

  @SVO_OpportunitySecondaryOwner_05 @SSEC-1269 @SVO_OpportunitySecondaryOwner_Batch
  Scenario: User is able to remove Secondary Owner from an existing Classic Opportunity
    Given Access to SVO Portal as classic Sales user with Username "LoginUsers,B,5" and Password "LoginUsers,C,5"
    When User navigates to an Opportunity tab
    And User creates an Classic Opportunity with mandatory fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,7" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" SAP Account "Opportunity,Q,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then Verify that opportunity is in Qualified stage
    And User adds classic user "OpportunitySecondaryOwner,A,4" as secondary owner for an classic opportunity
    Then User remove classic user under secondary owner field for an opportunity
    Then Logout from the Special vehicle operations Portal

  @SVO_OpportunitySecondaryOwner_06 @SSEC-1270 @SVO_OpportunitySecondaryOwner_Batch
  Scenario: An Secondary Owner is able to change the Secondary Owner for any classic opportunity
    Given Access to SVO Portal as Classic Admin user with Username "LoginUsers,B,13" and Password "LoginUsers,C,13"
    When User navigates to an Opportunity tab
    And User creates Classic Opportunity with all mandatory fields like Stage "Opportunity,B,2" Record Type "OpportunitySecondaryOwner,B,6" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" SAP Account "Opportunity,Q,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then Verify that opportunity is in Qualified stage
    And User adds classic user "OpportunitySecondaryOwner,A,3" as secondary owner for an classic opportunity
    Then User Logout from SVO Portal
    And Access to SVO Portal as classic Sales user by using add username with Username "LoginUsers,B,5" and Password "LoginUsers,C,5"
    Then User navigates to an Opportunity tab
    And User views classic opportunity under secondary opportunities list
    Then Verify that the details of selected classic opportunity
    And User adds classic user "OpportunitySecondaryOwner,A,4" as secondary owner for an classic opportunity
    Then Verify that the opportunity is not present on opportunity page for logged in user
    Then User Logout from SVO Portal
    And Access to SVO Portal as Classic Finance user by using add username with Username "LoginUsers,B,10" and Password "LoginUsers,C,10"
    Then User navigates to an Opportunity tab
    And User views classic opportunity under secondary opportunities list
    Then Verify that the details of selected classic opportunity
    Then Logout from the Special vehicle operations Portal

  @SVO_OpportunitySecondaryOwner_07 @SSEC-1271 @SVO_OpportunitySecondaryOwner_Batch
  Scenario: An Secondary Owner should not be able to view an opportunity which is deleted
    Given Access to SVO Portal as Classic Admin user with Username "LoginUsers,B,13" and Password "LoginUsers,C,13"
    When User navigates to an Opportunity tab
    And User creates an Classic Opportunity with mandatory fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,7" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" SAP Account "Opportunity,Q,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then Verify that opportunity is in Qualified stage
    And User adds classic user "OpportunitySecondaryOwner,A,4" as secondary owner for an classic opportunity
    Then User Logout from SVO Portal
    And Access to SVO Portal as Classic Finance user by using add username with Username "LoginUsers,B,10" and Password "LoginUsers,C,10"
    Then User navigates to an Opportunity tab
    And User views classic opportunity under secondary opportunities list
    Then Verify that the details of selected classic opportunity
    Then User Logout from SVO Portal
    Given laterally Access SVO Portal by user name "LoginUsers,B,13" and Password "LoginUsers,C,13"
    Then User search and select the same classic opportunity which is created recently
    And User delete the selected classic opportunity
    Then User Logout from SVO Portal
    Given laterally Access SVO Portal by user name "LoginUsers,B,10" and Password "LoginUsers,C,10"
    Then User is not able to view deleted classic opportunity
    Then Logout from the Special vehicle operations Portal

  @SVO_OpportunitySecondaryOwner_08 @SSEC-1272 @SVO_OpportunitySecondaryOwner_Batch
  Scenario: User is able to add an Secondary Owner for Closed classic opportunity
    Given Access to SVO Portal as Classic Admin user with Username "LoginUsers,B,13" and Password "LoginUsers,C,13"
    When User navigates to an Opportunity tab
    And User create an Classic Works Legend Opportunity with fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then Move the Opportunity to closed won stage with Outcome as "CancelOrder,A,5"
    And User navigate back to Opportunities page
    And Verify that opportunity is in Closed Won stage
    And User adds classic user "OpportunitySecondaryOwner,A,2" as secondary owner for an classic opportunity
    Then User Logout from SVO Portal
    And Access to SVO Portal as classic service user by add username with Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    Then User navigates to an Opportunity tab
    And User views classic opportunity under secondary opportunities list
    Then Verify that the details of selected classic opportunity
    Then Logout from the Special vehicle operations Portal

  @SVO_OpportunitySecondaryOwner_09 @SSEC-1273 @SVO_OpportunitySecondaryOwner_Batch
  Scenario: User is not able to view Secondary Owner field for an opportunity.
    Given Access to SVO bespoke Portal with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    When User navigates to an Opportunity tab
    And User creates new SVO Bespoke Opportunity with mandatory fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then Verify that secondary owner field is not present for bespoke opportunities
    Then Logout from the Special vehicle operations Portal

  @SVO_OpportunitySecondaryOwner_10 @SSEC-1274 @SVO_OpportunitySecondaryOwner_Batch
  Scenario: User is not able to view Opportunity tab to add Secondary Owner
    Given Access to SVO Portal as Classic Operations user with Username "LoginUsers,B,9" and Password "LoginUsers,C,9"
    Then Verify that opportunity tab is not present on home page
    Then Logout from the Special vehicle operations Portal

  @SVO_OpportunitySecondaryOwner_11 @SSEC-1275 @SVO_OpportunitySecondaryOwner_Batch
  Scenario: User is not able to add any user other than classic user as an Secondary Owner for any classic opportunity
    Given Access to SVO Portal as Classic Admin user with Username "LoginUsers,B,13" and Password "LoginUsers,C,13"
    When User navigates to an Opportunity tab
    And User creates Classic Opportunity with all mandatory fields like Stage "Opportunity,B,2" Record Type "OpportunitySecondaryOwner,B,6" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" SAP Account "Opportunity,Q,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then Verify that opportunity is in Qualified stage
    And User not able to add non classic user "OpportunitySecondaryOwner,A,6" as secondary owner for an classic opportunity
    Then Logout from the Special vehicle operations Portal

  @SVO_OpportunitySecondaryOwner_12 @SSEC-1276 @SVO_OpportunitySecondaryOwner_Batch
  Scenario: User is not able to view 'Secondary Owner' field for any classic opportunity
    Given Access to SVO Portal as Classic Finance user with Username "LoginUsers,B,10" and Password "LoginUsers,C,10"
    When User navigates to an Opportunity tab
    Then User select any existing classic opportunity
    And User is not able to view secondary owner for an classic opportunity
    Then Logout from the Special vehicle operations Portal

  @SVO_OpportunitySecondaryOwner_13 @SSEC-1277 @SVO_OpportunitySecondaryOwner_Batch
  Scenario: User is able to add an Secondary Owner for Closed Lost classic opportunity
    Given Access to SVO Portal as classic Sales user with Username "LoginUsers,B,5" and Password "LoginUsers,C,5"
    When User navigates to an Opportunity tab
    And User create an Classic Works Legend Opportunity with fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then Verify that opportunity is in Qualified stage
    Then Move the Opportunity to closed won stage with Outcome as "CancelOrder,A,2"
    And Verify that opportunity is in Closed Lost stage
    And User adds classic user "OpportunitySecondaryOwner,A,2" as secondary owner for an classic opportunity
    Then User Logout from SVO Portal
    And Access to SVO Portal as classic service user by add username with Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    Then User navigates to an Opportunity tab
    And User views classic opportunity under secondary opportunities list
    Then Verify that the details of selected classic opportunity
    Then Logout from the Special vehicle operations Portal

  @SVO_OpportunitySecondaryOwner_14 @SSEC-1278 @SVO_OpportunitySecondaryOwner_Batch
  Scenario: User is able to add or edit an Secondary Owner, when the main owner of the opportunity is not logged in user
    Given Access to SVO Portal as classic service Username user with Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    When User navigates to an Opportunity tab
    And User creates Classic Continuation Opportunity with all mandatory fields like Stage "Opportunity,B,2" Record Type "OpportunitySecondaryOwner,B,4" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" SAP Account "Opportunity,Q,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Programme "BillingAddress,B,3" Model "Opportunity,M,2"
    Then Verify that opportunity is in Qualified stage
    Then User Logout from SVO Portal
    And Access to SVO Portal as Classic Admin user by using add username with Username "LoginUsers,B,13" and Password "LoginUsers,C,13"
    And User navigates to an Opportunity tab
    Then User search and select the same classic opportunity which is created recently
    Then Verify that the details of selected classic opportunity
    And User adds classic user "OpportunitySecondaryOwner,A,2" as secondary owner for an classic opportunity
    Then Logout from the Special vehicle operations Portal

  @SVO_OpportunitySecondaryOwner_15 @SSEC-1279 @SVO_OpportunitySecondaryOwner_Batch
  Scenario: User is not able to add classic operation user as secondary owner for any classic opportunity
    Given Access to SVO Portal as classic Sales user with Username "LoginUsers,B,5" and Password "LoginUsers,C,5"
    When User navigates to an Opportunity tab
    And User creates Classic Continuation Opportunity with all mandatory fields like Stage "Opportunity,B,2" Record Type "OpportunitySecondaryOwner,B,4" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" SAP Account "Opportunity,Q,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Programme "BillingAddress,B,3" Model "Opportunity,M,2"
    Then Verify that opportunity is in Qualified stage
    And User not able to add non classic user "OpportunitySecondaryOwner,A,7" as secondary owner for an classic opportunity
    Then Logout from the Special vehicle operations Portal
