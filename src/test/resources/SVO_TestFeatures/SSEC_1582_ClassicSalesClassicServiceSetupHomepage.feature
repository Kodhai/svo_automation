
@CSSH
Feature: Script to Setup Homepage for Classic Sales and Classic Service User

  @SH1
  Scenario: User is able to verify Classic Sales My Cases section from SVO Home page
    Given Access to SVO Classic Sales Portal with Username "LoginUsers,B,5" and password "LoginUsers,C,5"
    When User navigates to Classic Sales Hompage
    Then User verifies My Cases section from Classic Sales Homepage
    Then User logouts from the portal
    
  @SH2
  Scenario: User is able to verify Classic Sales All Cases section from SVO Home page
    Given Access to SVO Classic Sales Portal with Username "LoginUsers,B,5" and password "LoginUsers,C,5"
    When User navigates to Classic Sales Hompage
    Then User verifies All Cases section from Classic Sales Homepage
    Then User logouts from the portal
    
  @SH3
  Scenario: User is able to verify Classic Sales All Cases Created Today section from SVO Home page
    Given Access to SVO Classic Sales Portal with Username "LoginUsers,B,5" and password "LoginUsers,C,5"
    When User navigates to Classic Sales Hompage
    Then User verifies All Cases Created Today section from Classic Sales Homepage
    Then User logouts from the portal
    
  @SH4
  Scenario: User is able to verify Classic Service My Cases section from SVO Home page
    Given Access to SVO Classic Service Portal with Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    When User navigates to Classic Service Hompage
    Then User verifies My Cases Created Today section from Classic Service Homepage
    Then User logouts from the portal
    
  @SH5
  Scenario: User is able to verify Classic Service All Cases section from SVO Home page
    Given Access to SVO Classic Service Portal with Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    When User navigates to Classic Service Hompage
    Then User verifies All Cases section from Classic Service Homepage
    Then User logouts from the portal
    
  @SH6
  Scenario: User is able to verify Classic Service All Cases Created Today section from SVO Home page
    Given Access to SVO Classic Service Portal with Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    When User navigates to Classic Service Hompage
    Then User verifies All Cases Created Today section from Classic Service Homepage
    Then User logouts from the portal
    
  @SH7
  Scenario: User is not able to verify Classic Sales My Cases section from SVO Home page.
    Given Access to SVO Classic Service Portal with Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    When User navigates to Classic Service Hompage
    Then Verify user is not able to verify Classic Sales My Cases section from SVO Homepage
    Then User logouts from the portal
    
  @SH8
  Scenario: User is not able to verify Classic Sales My Cases section from SVO Home page.
    Given Access to SVO Classic Service Portal with Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    When User navigates to Classic Service Hompage
    Then Verify user is not able to verify Classic Service All Cases section from SVO Homepage
    Then User logouts from the portal
    
  @SH9
  Scenario: User is able to verify Classic Sales My Cases section link under recent records on home page
    Given Access to SVO Classic Service Portal with Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    When User navigates to Classic Service Hompage
    Then User is able to verify Classic Sales My Cases section link under recent records on homepage
    Then Verify that user is able to navigate to Classic Sales My Cases section by clicking on link
    Then User logouts from the portal
    
   @SH10
  Scenario: User is able to verify Classic Service All Cases Created Today section link under recent records on home page
    Given Access to SVO Classic Service Portal with Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    When User navigates to Classic Service Hompage
    Then User is able to verify Classic Service All Cases Created Today section link under recent records on homepage
    Then Verify that user is able to navigate to Classic Service All Cases Created Today section by clicking on link
    Then User logouts from the portal
    




    
  
  