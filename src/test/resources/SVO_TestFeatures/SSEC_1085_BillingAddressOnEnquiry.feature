Feature: SVO Billing Address on Enquiry Validations

  @SVO_BillingAddress_01
  Scenario: User is able to view Address details of the customer in contact details section after creating a New Classic sales Enquiry
    Given Access to SVO Portal as classic Sales user with Username "LoginUsers,B,5" and Password "LoginUsers,C,5"
    Then User navigates to the Enquiries tab
    Then User creates an Enquiry with record type "EnquiryLostReason,A,2" productOffering "BillingAddress,A,2" Region "Opportunity,D,2" Client "Opportunity,G,2" PreferredContact "Opportunity,H,2" Programme "BillingAddress,B,3" Brand "Opportunity,L,2" Model "Opportunity,M,2" BodyStyle "Opportunity,P,7" OrginatingDivision "RestrictedPartyScreening,B,4"
    And Verify that the enquiry is in New stage
    Then Verify that billing address is autopopulated under contact details section
    Then User Logout from the SVO Portal

  @SVO_BillingAddress_02
  Scenario: User is able to view Address details of the customer in contact details section after creating a New Classic Service Enquiry
    Given Access to SVO Portal with Classic Service user with the Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    Then User navigates to the Enquiries tab
    Then User creates an Enquiry with record type "EnquiryLostReason,A,3" productOffering "BillingAddress,A,6" Region "Opportunity,D,2" Client "Opportunity,G,2" PreferredContact "Opportunity,H,2" Programme "BillingAddress,B,3" Brand "Opportunity,L,2" Model "Opportunity,M,2" BodyStyle "Opportunity,P,7" OrginatingDivision "RestrictedPartyScreening,B,2"
    And Verify that the enquiry is in New stage
    Then Verify that billing address is autopopulated under contact details section
    Then User Logout from the SVO Portal

  @SVO_BillingAddress_03
  Scenario: User is able to view Address details of the customer in contact details section for an Existing Closed Lost Classic Sales Enquiry
    Given Access to SVO Portal as Classic Admin user with Username "LoginUsers,B,13" and Password "LoginUsers,C,13"
    Then User navigates to the Enquiries tab
    Then User select an existing closed lost classic sales enquiry from list view
    And Verify that the enquiry is in Closed Lost stage
    Then Verify that billing address is autopopulated under contact details section
    Then User Logout from the SVO Portal

  @SVO_BillingAddress_04
  Scenario: User is not able to view billing address details of the customer in contact details section for an Closed Lost SVO Private office Enquiry
    Given Access to SVO Portal by private office user with Username "LoginUsers,B,4" and Password "LoginUsers,C,4"
    Then User navigates to the Enquiries tab
    Then User creates an Private Office Enquiry with record type "EnquiryLostReason,A,5" productOffering "Opportunity,C,3" Region "Opportunity,D,2" Client "Opportunity,G,2" PreferredContact "Opportunity,H,2" Programme "BillingAddress,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2" BodyStyle "Opportunity,P,7" OrginatingDivision "RestrictedPartyScreening,B,4"
    And Log a call for an enquiry activity
    Then User moves an enquiry to discovery stage
    Then User close the enquiry with closed state "EnquiryLostReason,B,2" and closed reason "EnquiryLostReason,C,4" on Enquiry page
    And Verify the auto-populated fields of enqiry closed reason and closed state on Enquiry page
    Then Verify that user is not able to view customer billing address details on enquiry page
    Then User Logout from the SVO Portal

  @SVO_BillingAddress_05
  Scenario: User is not able to view billing address details of the customer in contact details section for an New SVO Bespoke Enquiry
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Enquiries tab
    Then User creates an Private Office Enquiry with record type "EnquiryLostReason,A,4" productOffering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" PreferredContact "Opportunity,H,2" Programme "BillingAddress,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2" BodyStyle "Opportunity,P,7" OrginatingDivision "RestrictedPartyScreening,B,4"
    And Log a call for an enquiry activity
    Then User moves an enquiry to discovery stage
    Then User close the enquiry with closed state "EnquiryLostReason,B,2" and closed reason "EnquiryLostReason,C,4" on Enquiry page
    And Verify the auto-populated fields of enqiry closed reason and closed state on Enquiry page
    Then Verify that user is not able to view customer billing address details on enquiry page
    Then User Logout from the SVO Portal

  @SVO_BillingAddress_06
  Scenario: User is able to close the Classic sales enquiry with closed state Qualified KMI state after adding customer address in contact details section of an enquiry
    Given Access to SVO Portal with Classic Sales user with the Username "LoginUsers,B,5" and Password "LoginUsers,C,5"
    Then User navigates to the Enquiries tab
    Then User creates an Enquiry with record type "EnquiryLostReason,A,2" productOffering "BillingAddress,A,2" Region "Opportunity,D,2" Client "Opportunity,G,2" PreferredContact "Opportunity,H,2" Programme "BillingAddress,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2" BodyStyle "Opportunity,P,7" OrginatingDivision "RestrictedPartyScreening,B,4"
    And Log a call for an enquiry activity
    When User enters customer contact details "Opportunity,J,2" and Billing Address details on enquiry page
    Then User moves an enquiry to discovery stage
    Then User close the enquiry with closed state "EnquiryLostReason,B,4" on Enquiry page
    And Verify the auto-populated fields of enqiry closed state on Enquiry page
    Then User Logout from the SVO Portal

  @SVO_BillingAddress_07
  Scenario: User is able to edit the customer billing address details for an Closed Qualified Classic sales enquiry
    Given Access to SVO Portal by private office user with Username "LoginUsers,B,4" and Password "LoginUsers,C,4"
    Then User navigates to the Enquiries tab
    Then User creates an Enquiry with record type "EnquiryLostReason,A,2" productOffering "BillingAddress,A,2" Region "Opportunity,D,2" Client "Opportunity,G,2" PreferredContact "Opportunity,H,2" Programme "BillingAddress,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2" BodyStyle "Opportunity,P,7" OrginatingDivision "RestrictedPartyScreening,B,4"
    And Log a call for an enquiry activity
    When User enters customer contact details "Opportunity,J,2" and Billing Address details on enquiry page
    Then User moves an enquiry to discovery stage
    Then User close the enquiry with closed state "EnquiryLostReason,B,4" on Enquiry page
    And Verify the auto-populated fields of enqiry closed state on Enquiry page
    And User edits customer contact details "Opportunity,J,3" and Billing Address details on enquiry page
    Then Verify that updated billing address details of the customer on enquiry page
    Then User Logout from the SVO Portal

  @SVO_BillingAddress_08
  Scenario: User is able to update the customer billing address in contact details section of an Classic service enquiry which is in Discovery status
    Given Access to SVO Portal with Classic Service user with the Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    Then User navigates to the Enquiries tab
    Then User creates an Enquiry with record type "EnquiryLostReason,A,3" productOffering "BillingAddress,A,2" Region "Opportunity,D,2" Client "Opportunity,G,2" PreferredContact "Opportunity,H,2" Programme "BillingAddress,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2" BodyStyle "Opportunity,P,7" OrginatingDivision "RestrictedPartyScreening,B,4"
    And Log a call for an enquiry activity
    When User enters customer contact details "Opportunity,J,2" and Billing Address details on enquiry page
    Then User moves an enquiry to discovery stage
    And User edits customer contact details "Opportunity,J,3" and Billing Address details on enquiry page
    Then Verify that updated billing address details of the customer on enquiry page
    Then User close the enquiry with closed state "EnquiryLostReason,B,3" on Enquiry page
    And Verify the auto-populated fields of enqiry closed state on Enquiry page
    Then User Logout from the SVO Portal

  @SVO_BillingAddress_09
  Scenario: User is able to remove the customer billing address in contact details section of an Closed Lost Classic sales enquiry
    Given Access to SVO Portal with Classic Sales user with the Username "LoginUsers,B,5" and Password "LoginUsers,C,5"
    Then User navigates to the Enquiries tab
    Then User creates an Enquiry with record type "EnquiryLostReason,A,2" productOffering "BillingAddress,A,2" Region "Opportunity,D,2" Client "Opportunity,G,2" PreferredContact "Opportunity,H,2" Programme "BillingAddress,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2" BodyStyle "Opportunity,P,7" OrginatingDivision "RestrictedPartyScreening,B,4"
    And Log a call for an enquiry activity
    When User enters customer contact details "Opportunity,J,2" and Billing Address details on enquiry page
    Then User moves an enquiry to discovery stage
    Then User close the enquiry with closed state "EnquiryLostReason,B,3" closed reason "EnquiryLostReason,C,2" on Enquiry page
    And Verify the auto-populated fields of enqiry closed state on Enquiry page
    And User remove customer contact details from contact details section of an enquiry
    Then User Logout from the SVO Portal

  @SVO_BillingAddress_10
  Scenario: User is able to Update contact details for an Classic sales enquiry so that respective billing address of the customer is reflected on Enquiry page
    Given Access to SVO Portal with Classic Sales user with the Username "LoginUsers,B,5" and Password "LoginUsers,C,5"
    Then User navigates to the Enquiries tab
    Then User creates an Enquiry with record type "EnquiryLostReason,A,2" productOffering "BillingAddress,A,2" Region "Opportunity,D,2" Client "Opportunity,G,2" PreferredContact "Opportunity,H,2" Programme "BillingAddress,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2" BodyStyle "Opportunity,P,7" OrginatingDivision "RestrictedPartyScreening,B,4"
    And Log a call for an enquiry activity
    When User enters customer contact details "Opportunity,J,2" and Billing Address details on enquiry page
    Then User moves an enquiry to discovery stage
    And Update the contact details under contact details section of an enquiry
    Then Verify that billing address details is updated with respect to contact details
    Then User close the enquiry with closed state "EnquiryLostReason,B,3" on Enquiry page
    And Verify the auto-populated fields of enqiry closed state on Enquiry page
    Then User Logout from the SVO Portal

  @SVO_BillingAddress_11
  Scenario: User is able to remove the customer billing address in contact details section of an Closed Lost Classic sales enquiry
    Given Access to SVO Portal with Classic Service user with the Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    Then User navigates to the Enquiries tab
    Then User creates an Enquiry with record type "EnquiryLostReason,A,2" productOffering "BillingAddress,A,2" Region "Opportunity,D,2" Client "Opportunity,G,2" PreferredContact "Opportunity,H,2" Programme "BillingAddress,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2" BodyStyle "Opportunity,P,7" OrginatingDivision "RestrictedPartyScreening,B,4"
    And Log a call for an enquiry activity
    When User enters customer contact details "Opportunity,J,2" and Billing Address details on enquiry page
    Then User moves an enquiry to discovery stage
    Then User close the enquiry with closed state "EnquiryLostReason,B,3" closed reason "EnquiryLostReason,C,2" on Enquiry page
    And User remove customer contact details from contact details section of an enquiry
    Then Verify that billing address details of the customer is removed within customer contact details section of an enquiry
    And Verify the auto-populated fields of enqiry closed state on Enquiry page
    Then User Logout from the SVO Portal

  @SVO_BillingAddress_12
  Scenario: User is not able to Close the Classic sales Enquiry with qualified state without adding contact details and billing address details on Enquiry page
    Given Access to SVO Portal with Classic Sales user with the Username "LoginUsers,B,5" and Password "LoginUsers,C,5"
    Then User navigates to the Enquiries tab
    Then User creates an Enquiry with record type "EnquiryLostReason,A,2" productOffering "BillingAddress,A,2" Region "Opportunity,D,2" Client "Opportunity,G,2" PreferredContact "Opportunity,H,2" Programme "BillingAddress,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2" BodyStyle "Opportunity,P,7" OrginatingDivision "RestrictedPartyScreening,B,4"
    Then User close the enquiry with closed state "EnquiryLostReason,B,3" on Enquiry page
    And Verify that error message while Close the Classic sales Enquiry with qualified state without adding contact details and billing address details with closed state on Enquiry page
    Then User Logout from the SVO Portal

  @SVO_BillingAddress_13
  Scenario: User is not able to remove contact details of an Classic sales enquiry which is closed with qualified opportunity state on Enquiry page
    Given Access to SVO Portal with Classic Sales user with the Username "LoginUsers,B,5" and Password "LoginUsers,C,5"
    Then User navigates to the Enquiries tab
    Then User creates an Enquiry with record type "EnquiryLostReason,A,3" productOffering "BillingAddress,A,2" Region "Opportunity,D,2" Client "Opportunity,G,2" PreferredContact "Opportunity,H,2" Programme "BillingAddress,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2" BodyStyle "Opportunity,P,7" OrginatingDivision "RestrictedPartyScreening,B,4"
    And Log a call for an enquiry activity
    When User enters customer contact details "Opportunity,J,2" and Billing Address details on enquiry page
    Then User moves an enquiry to discovery stage
    Then User close the enquiry with closed state "EnquiryLostReason,B,3" on Enquiry page
    And Verify the auto-populated fields of enqiry closed state on Enquiry page
    Then Verify that error message while removing the contact details after enquiry is closed
    Then User Logout from the SVO Portal

  @SVO_BillingAddress_14
  Scenario: User is not able to remove contact details of an Classic sales enquiry which is closed with qualified opportunity state on Enquiry page
    Given Access to SVO Portal with Classic Sales user with the Username "LoginUsers,B,5" and Password "LoginUsers,C,5"
    Then User navigates to the Enquiries tab
    Then User creates an Enquiry with address details record type "EnquiryLostReason,A,3" productOffering "BillingAddress,A,2" Region "Opportunity,D,2" Client "Opportunity,G,2" PreferredContact "Opportunity,H,2" Programme "BillingAddress,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2" BodyStyle "Opportunity,P,7" OrginatingDivision "RestrictedPartyScreening,B,4"
    And Log a call for an enquiry activity
    Then User close the enquiry with closed state "EnquiryLostReason,B,3" on Enquiry page
    Then User Logout from the SVO Portal
