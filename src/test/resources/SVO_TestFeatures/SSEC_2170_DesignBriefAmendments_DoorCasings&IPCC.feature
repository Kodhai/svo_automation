@SSEC_2170
Feature: Validate Design Brief Amendments- IP & CC

  @SSEC_2170_01 @sonica
  Scenario: Bespoke user is able to fill door casings like front door armrest material, rear door armrest material, top roll and main carrier front color
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User creates a Bespoke Opportunity with check cleared like Stage "Opportunity,B,3" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,I,3" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,3"
    Then User verifies that new Opportunity is created
    Then User navigates to Quotes from Related Links
    And User creates new Bespoke Design Brief quotes "Opportunity,J,4" with mandatory details
    And Click on the new Bespoke Design Brief quote created successfully
    Then Click on Edit button of Front Door Armrest Material "DesignBrief,R,13"
    And Select the Rear Door Armrest Material "DesignBrief,S,13" Top Roll Front Color "DesignBrief,T,13" and Main Carrier Front Color "DesignBrief,U,13"
    Then Click on save button of Design Brief
    Then User Logout from SVO Portal

  @SSEC_2170_02 @sonica
  Scenario: Bespoke user is able to fill door casings like front door armrest material, rear door armrest material, top roll and main carrier front color
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User creates a Bespoke Opportunity with check cleared like Stage "Opportunity,B,3" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,I,3" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,3"
    Then User verifies that new Opportunity is created
    Then User navigates to Quotes from Related Links
    And User creates new Bespoke Design Brief quotes "Opportunity,J,4" with mandatory details
    And Click on the new Bespoke Design Brief quote created successfully
    Then Click on Edit button of IP and CC Material "DesignBrief,L,13"
    And Select the Column Cowl Color "DesignBrief,M,13" and Topper Pad Color "DesignBrief,N,13" from drop down
    Then Click on save button of Design Brief
    Then User Logout from SVO Portal

  @SSEC_2170_03 @sonica
  Scenario: Bespoke user is able to fill door casings like front door armrest material, rear door armrest material, top roll and main carrier front color
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User creates a Bespoke Opportunity with check cleared like Stage "Opportunity,B,3" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,I,3" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,3"
    Then User verifies that new Opportunity is created
    Then User navigates to Quotes from Related Links
    And User creates new Bespoke Design Brief quotes "Opportunity,J,4" with mandatory details
    And Click on the new Bespoke Design Brief quote created successfully
    Then Click on Edit button of of Mid Roll Color "DesignBrief,O,13"
    And Select the IP Lower Color "DesignBrief,P,13" and Console color "DesignBrief,Q,13"
    Then Click on save button of Design Brief
    Then User Logout from SVO Portal

  @SSEC_2170_04 @sonica
  Scenario: Bespoke user is able to fill door casings like armrest front color, armrest rear color, top roll rear color and main carrier rear color
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User creates a Bespoke Opportunity with check cleared like Stage "Opportunity,B,3" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,I,3" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,3"
    Then User verifies that new Opportunity is created
    Then User navigates to Quotes from Related Links
    And User creates new Bespoke Design Brief quotes "Opportunity,J,4" with mandatory details
    And Click on the new Bespoke Design Brief quote created successfully
    Then Click on Edit button of ArmRest FrontColorMaterial "DesignBrief,V,13"
    And Select the ArmRestRearColor "DesignBrief,W,13" TopRollRearColor "DesignBrief,X,13" MainCarrierRearColor "DesignBrief,Y,13"
    Then Click on save button of Design Brief
    Then User Logout from SVO Portal

  @SSEC_2170_05 @sonica
  Scenario: Bespoke user is able to verify that when front door armrest material is changed, the arm rest front color is also changed accordingly
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User creates a Bespoke Opportunity with check cleared like Stage "Opportunity,B,3" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,I,3" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,3"
    Then User verifies that new Opportunity is created
    Then User navigates to Quotes from Related Links
    And User creates new Bespoke Design Brief quotes "Opportunity,J,4" with mandatory details
    And Click on the new Bespoke Design Brief quote created successfully
    Then Click on Edit button of ArmRestFrontColorMaterial "DesignBrief,V,14"
    And Verify that the ArmRestFrontColor is changed when ArmRestFrontColor Material is changed
    Then Click on save button of Design Brief
    Then User Logout from SVO Portal
