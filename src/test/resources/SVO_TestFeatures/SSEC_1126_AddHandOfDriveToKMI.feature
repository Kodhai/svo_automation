Feature: Addition of 'Hand of Drive' field to KMI validations

  @AddHandOfDriveToKMI_01 @Batch @SSEC-
  Scenario: User is able to view 'Hand of Drive' field on 'All Active KMIs' list view after creating new KMI
    Given Access to SVO Portal as Classic Service user with Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    When User navigates to KMI section
    Then User create new active KMI with all fields like Contact details "KMIs,A,2" Hand of Drive "KMIs,C,2" Product Offering "KMIs,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    And User views Hand of Drive details under All Active KMIs list view
    Then User sort All KMIs based on Hand of Drive Field
    And Logout from the Special vehicle operations Portal

  @AddHandOfDriveToKMI_02 @Batch @SSEC-
  Scenario: User is able to view 'Hand of Drive' field on 'All Inactive KMIs' list view after creating new KMI
    Given Access to SVO Portal as classic Sales user with Username "LoginUsers,B,5" and Password "LoginUsers,C,5"
    When User navigates to KMI section
    Then User create new inactive KMI with all fields like Contact details "KMIs,A,2" Hand of Drive "KMIs,C,3" Product Offering "KMIs,B,3" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    And User views Hand of Drive details under All Inactive KMIs list view
    Then User sort All KMIs based on Hand of Drive Field
    And Logout from the Special vehicle operations Portal

  @AddHandOfDriveToKMI_03 @Batch @SSEC-
  Scenario: User is able to edit Active field of KMI record after creating new KMI
    Given Access to SVO Portal as Classic Service user with Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    When User navigates to KMI section
    Then User create new active KMI with all fields like Contact details "KMIs,A,2" Hand of Drive "KMIs,C,2" Product Offering "KMIs,C,3" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    And User select newly created KMI under All Active KMIs list view
    And User edits KMI details like Product Offering "KMIs,C,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then Verify that KMI is updated under All Inactive KMIs list view
    And Logout from the Special vehicle operations Portal

  @AddHandOfDriveToKMI_04 @Batch @SSEC-
  Scenario: User is not able to view KMIs section from SVO menu bar on SVO portal
    Given Access to SVO Portal as Classic Finance user with Username "LoginUsers,B,10" and Password "LoginUsers,C,10"
    When Verify that user is not able to view KMIs section on home page
    And Logout from the Special vehicle operations Portal

  @AddHandOfDriveToKMI_05 @Batch @SSEC-
  Scenario: User is able to delete inactive KMI from inactive KMIs list view
    Given Access to SVO Portal as Classic Admin user with Username "LoginUsers,B,13" and Password "LoginUsers,C,13"
    When User navigates to KMI section
    Then User create new inactive KMI with all fields like Contact details "KMIs,A,2" Hand of Drive "KMIs,B,3" Product Offering "KMIs,C,4" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    And User delete created KMI from KMIs page
    Then Verify that KMI is not present on KMIs page
    And Logout from the Special vehicle operations Portal

  @AddHandOfDriveToKMI_06 @Batch @SSEC-
  Scenario: User is able to view details of selected source enquiry from KMIs page
    Given Access to SVO Portal as Classic Admin user with Username "LoginUsers,B,13" and Password "LoginUsers,C,13"
    When User navigates to KMI section
    Then User create new active KMI with all fields like Contact details "KMIs,A,2" Hand of Drive "KMIs,C,2" Product Offering "KMIs,C,5" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    And User select newly created KMI under All Active KMIs list view
    Then User chooses source enquiry for an KMI Record Type "Enquiry,A,2"
    And Verify the details of enquiry from KMIs page
    And Logout from the Special vehicle operations Portal

  @AddHandOfDriveToKMI_07 @Batch @SSEC-
  Scenario: User is not able to delete active KMI from active KMIs list view
    Given Access to SVO Portal as classic Sales user with Username "LoginUsers,B,5" and Password "LoginUsers,C,5"
    When User navigates to KMI section
    Then User create new active KMI with all fields like Contact details "KMIs,A,2" Hand of Drive "KMIs,B,3" Product Offering "KMIs,C,6" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    And User select newly created KMI under All Active KMIs list view
    Then Verify that delete button is not available on KMIs page
    And Logout from the Special vehicle operations Portal

  @AddHandOfDriveToKMI_08 @Batch @SSEC-
  Scenario: User is not able to choose source enquiry if the contact details of enquiry is not matched with KMIs contact details
    Given Access to SVO Portal as Classic Service user with Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    When User navigates to KMI section
    Then Select an existing KMI from Active KMIs list
    Then Verify that user is not able to choose source enquiry if contact details of enquiry and KMI is not matched
    And Logout from the Special vehicle operations Portal

  @AddHandOfDriveToKMI_09 @Batch @SSEC-
  Scenario: User is not able to update KMI name after creating new KMI
    Given Access to SVO Portal as classic Sales user with Username "LoginUsers,B,5" and Password "LoginUsers,C,5"
    When User navigates to KMI section
    Then User create new inactive KMI with all fields like Contact details "KMIs,A,2" Hand of Drive "KMIs,B,3" Product Offering "KMIs,C,3" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    And Verify that edit option is not availabe for KMI name field on KMIs page
    And Logout from the Special vehicle operations Portal

  @AddHandOfDriveToKMI_10 @Batch @SSEC-
  Scenario: User is not able to view 'Hand of Drive' details on 'All Active KMIs' list view if Hand of Drive is not selected while creating new KMI
    Given Access to SVO Portal as classic Sales user with Username "LoginUsers,B,5" and Password "LoginUsers,C,5"
    When User navigates to KMI section
    Then User create new active KMI without selecting Hand of drive field like Contact details "KMIs,A,2" Product Offering "KMIs,C,5" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    And Verify that Hand o Drive details are not present on KMis page
    And Logout from the Special vehicle operations Portal

  @AddHandOfDriveToKMI_11 @Batch @SSEC- @tentative
  Scenario: User is able to change the owner as classic user for existing KMI on KMIs page
    Given Access to SVO Portal as Classic Service user with Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    When User navigates to KMI section
    Then Select an existing KMI from KMIs list
    And User change the KMI Onwer as classic user "KMIs,A,5"
    And Logout from the Special vehicle operations Portal

  @AddHandOfDriveToKMI_12 @Batch @SSEC- @tentative
  Scenario: User is not able to change owner other than classic user for existing KMI on KMIs page
    Given Access to SVO Portal as Classic Service user with Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    When User navigates to KMI section
    Then Select an existing KMI from KMIs list
    And User not able to change the KMI Onwer to other than classic user as "KMIs,A,4"
    And Logout from the Special vehicle operations Portal
