@SSEC_1229
Feature: Script to add Case Queues in Classic Parts

  #User is able to create Classic Parts and Technical Queue Case
  @SSEC_1229_CaseQueue_01 @SSEC-1858 @CQ01
  Scenario: User is able to create Classic Parts and Technical Queue Case
    Given Access to SVO Portal as Classic Parts User with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User Navigates to cases tab
    And User creates Parts and Technical Record type case with mandatory fields like VIN
    Then User saves the Case
    And Verify that Case Owner has value as Parts and Technical Queue
    And Verify that user is able to close case for Parts and Technical Queue
    And User logouts from portal

  #User is able to create Classic Customer Service Queue Case
  @SSEC_1229_CaseQueue_02 @SSEC-1860 @CQ02
  Scenario: User is able to create Classic Customer Service Queue Case
    Given Access to SVO portal as Customer Services User with UserName "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User Navigates to cases tab
    And User creates Customer services case with all mandatory fields
    Then User saves the Case
    And Verify that Case Owner has value as Customer Service Queue
    And Verify that user is able to close case for Customer Service Queue
    #And User logouts from portal

  #User is able to view all Customer Service Queue cases
  @SSEC_1229_CaseQueue_03 @SSEC-1862 @CQ03
  Scenario: User is able to view all Customer Service Queue cases
    Given Access to SVO portal as Customer Services User with UserName "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User Navigates to cases tab
    And User creates Customer services case with all mandatory fields
    Then User saves the Case
    And Verify that user is able to view all the Customer Service Queues in Cases tab
    And User logouts from portal

  #User is able to view all Parts and Technical cases
  @SSEC_1229_CaseQueue_04 @SSEC-1867 @CQ04
  Scenario: User is able to view all Parts and Technical cases
    Given Access to SVO Portal as Classic Parts Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User Navigates to cases tab
    And User creates Parts and Technical Record type case with mandatory fields like VIN
    Then User saves the Case
    And Verify that user is able to view all the Parts and Technical Queues in Cases tab
    And User logouts from portal

  #User is able to switch queues for Customer Service Queue case
  @SSEC_1229_CaseQueue_05 @SSEC-1875 @CQ05
  Scenario: User is able to switch queues for Customer Service Queue case
    Given Access to SVO portal as Customer Services User with UserName "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User Navigates to cases tab
    And User creates Customer services case with all mandatory fields
    Then User saves the Case
    And User edits the case from Customer Service Queue to Parts and Technical Queue
    And User verifies that Case Owner is Parts and Technical Queue
    And User logouts from portal

  #User is unable to remove Case Owner from Classic Parts and Technical Case
  @SSEC_1229_CaseQueue_06 @SSEC-1877 @CQ06
  Scenario: User is unable to remove Case Owner from Classic Parts and Technical Case
    Given Access to SVO portal as Customer Services User with UserName "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User Navigates to cases tab
    And User creates Parts and Technical Record type case with mandatory fields like VIN
    Then User saves the Case
    Then User clicks on edit button of Case Owner of Parts and Technical Queue and verifies that remove Case owner is not present
    And User logouts from portal
    
 
