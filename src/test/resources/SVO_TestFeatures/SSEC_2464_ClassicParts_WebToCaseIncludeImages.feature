@SSEC_2464
Feature: Validate Web To Case Include Images (Contact Form) scenarios.

  @SSEC_2464_01
  Scenario: User is able to fill the contact form and click on Submit button on the Contact form
    Given Access to the Classic Parts portal
    Then Submit Contact Us form with Email "ClassicPartsContactDet,B,2" First name "ClassicPartsContactDet,C,2" Last name "ClassicPartsContactDet,D,2" Country "ClassicPartsContactForm,I,3" Phone number "ClassicPartsContactDet,H,3" Enquiry Type "ClassicPartsContactForm,T,2" VIN number "ClassicPartsContactForm,J,2"
    And close the browser

  @SSEC_2464_02
  Scenario: User is able to fill the contact form and click on Submit button on the Contact form
    Given Access to the Classic Parts portal
    Then Submit Contact Us form with Email "ClassicPartsContactDet,B,2" First name "ClassicPartsContactDet,C,2" Last name "ClassicPartsContactDet,D,2" Country "ClassicPartsContactForm,I,3" Phone number "ClassicPartsContactDet,H,3" Enquiry Type "ClassicPartsContactForm,T,2" VIN number "ClassicPartsContactForm,J,2"
    And close the browser
    Then User Access to SVO Portal as Classic Parts and technical user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    And Filters the Cases as All Cases and Searches the CaseNumber copied
    #Then Search the Case number in the Search box
    And user logout from the portal

  @SSEC_2464_03
  Scenario: User is able to verify the 4 options available after the Submit button on the Contact form
    Given Access to the Classic Parts portal
    Then Submit Contact Us form with Email "ClassicPartsContactDet,B,2" First name "ClassicPartsContactDet,C,2" Last name "ClassicPartsContactDet,D,2" Country "ClassicPartsContactForm,I,3" Phone number "ClassicPartsContactDet,H,3" Enquiry Type "ClassicPartsContactForm,T,2" VIN number "ClassicPartsContactForm,J,2"
    And User verify all four options available after the submit button on the contact form
    And close the browser

  @SSEC_2464_04
  Scenario: User is able to verify Jaguar Land Rover Classic Description
    Given Access to the Classic Parts Contact Us portal
    Then User is able to upload images on contact us form
    Then select a file and click on upload button

  @SSEC_2464_05
  Scenario: User is able to delete images after uploading on contact us form
    Given Access to the Classic Parts Contact Us portal
    Then User is able to upload images on contact us form
    Then select a file and click on upload Files
    And Click on Delete Image in the Contact Us form

  @SSEC_2464_06
  Scenario: User verify the captcha in contact us form
    Given Access to the Classic Parts Contact Us portal
    Then User is able to upload images on contact us form
    Then select a file and click on upload Files
    And Verify the captcha in the contact us form
