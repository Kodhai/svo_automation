@SSEC_2432
Feature: Validate Desgn Bried Amendments scripts

  #User able to submit the cases in the website, the corresponding case should be created in Salesforce and assigned to the Queue
  @SSEC_2432_02 @Prajna @Backlog
  Scenario: User able to submit the cases in the website, the corresponding case should be created in Salesforce and assigned to the Queue
   Given User open the Ownerhelp portal
   Then User fill all the mandatory details
   Given Laterally Access to SVO Portal as Classic Parts and technical user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
   When User navigate to Cases Tab
   When User Verify the cases created
   And User Verify the Salesforce and assigned to the Queue
   Then User logouts from the SF SVO
 
  #User unable to submit the cases in the website without mandatory details
  @SSEC_2432_03 @Prajna @Backlog
  Scenario: User unable to submit the cases in the website without mandatory details
   Given User open the Ownerhelp portal
   Then User is getting error to not fill all the mandatory details
   And User close the browser
   
   #User able to delete the record from the cases
  @SSEC_2432_04 @Prajna @Backlog
  Scenario: User able to delete the record from the cases
   Given User open the Ownerhelp portal
   Then User fill all the mandatory details
   Given Laterally Access to SVO Portal as Classic Parts and technical user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
   When User navigate to Cases Tab
   When User Verify the cases created
   And User Verify that the record successfully deleted
   Then User logouts from the SF SVO
 
   #Verify that user is able to modify/edit the record details on Salesforce 
   @SSEC_2432_01 @Prajna @Backlog
   Scenario: Verify that user is able to modify/edit the record details on Salesforce
   Given User open the Ownerhelp portal
   Then User fill all the mandatory details
   Given Laterally Access to SVO Portal as Classic Parts and technical user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
   When User navigate to Cases Tab
   When User Verify the cases created
   Then User logouts from the SF SVO
 
  #Verify that user is able to modify/edit the record details on Salesforce 
   @SSEC_2432_05 @Prajna @Backlog
   Scenario: Verify that user is able to modify/edit the record details on Salesforce
   Given User open the Ownerhelp portal
   Then User fill all the mandatory details
   Given Laterally Access to SVO Portal as Classic Parts and technical user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
   When User navigate to Cases Tab
   When User Verify the cases created
   And User is able to edit the record details
   #Then User logouts from the SF SVO
 
 #Verify that user is able to upload photo within 5MB 
   @SSEC_2432_06 @Prajna @Backlog
   Scenario: Verify that user is able to upload photo within 5MB
   Given User open the Ownerhelp portal
   Then User fill all the mandatory details with upload picture
   And User close the browser
   
 
 
 