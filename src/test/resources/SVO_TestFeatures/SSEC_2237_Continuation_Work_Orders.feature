@SSEC_2237
Feature: Validate Continuation Work Orders Issue

  @SSEC_2237_01 @sonica
  Scenario: Classic Sales user is able to create Continuation Opportunity record type and add donor details
    Given Access to SVO Portal as Classic Sales user with Username "LoginUsers,B,5" and password "LoginUsers,C,5"
    And User navigates to Opportunities tab
    And Create a new Continuation Opportunity record type
    Then Fill all the mandatory details Stage "Opportunity,B,3" Region "Opportunity,D,3" Market "Opportunity,F,3" AccName "Opportunity,I,3" PreferredContact "Opportunity,H,3" Programme "Opportunity,S,2" VAT "Opportunity,T,2" Source "Opportunity,K,2"
    And Click on Save button
    And Adds the Donor field
    Then User Logout from SVO Portal

  @SSEC_2237_02 @sonica
  Scenario: Classic Sales user is able to create Continuation Opportunity record type and add donor details
    Given Login to the SF SVO portal for Classic Services User with UserName "LoginUsers,B,12" and Password "LoginUsers,C,12"
    And User navigates to Opportunities tab
    And Create a new Continuation Opportunity record type
    Then Fill all the mandatory details Stage "Opportunity,B,3" Region "Opportunity,D,3" Market "Opportunity,F,3" AccName "Opportunity,I,3" PreferredContact "Opportunity,H,3" Programme "Opportunity,S,2" VAT "Opportunity,T,2" Source "Opportunity,K,2"
    And Click on Save button
    And Adds the Donor field
    Then User Logout from SVO Portal

  @SSEC_2237_03 @sonica
  Scenario: Classic Sales user is able to mark the Continuation Opportunity with Order Placed
    Given Access to SVO Portal as Classic Sales user with Username "LoginUsers,B,5" and password "LoginUsers,C,5"
    And User navigates to Opportunities tab
    And Create a new Continuation Opportunity record type
    Then Fill all the mandatory details Stage "Opportunity,B,3" Region "Opportunity,D,3" Market "Opportunity,F,3" AccName "Opportunity,I,3" PreferredContact "Opportunity,H,3" Programme "Opportunity,S,2" VAT "Opportunity,T,2" Source "Opportunity,K,2"
    And Click on Save button
    And Adds the Donor field
    And Fill BodyStyle
    Then Fills Specifications and Options section HandOfDrive "Opportunity,U,2" Speedometer "Opportunity,V,2"
    And Click on Mark Stage as Complete

  @SSEC_2237_04 @sonica
  Scenario: Classic Sales user is able to verify VIN number from Continuation Opportunity is auto-populated in the Work Orders page
    Given Access to SVO Portal as Classic Sales user with Username "LoginUsers,B,5" and password "LoginUsers,C,5"
    And User navigates to Opportunities tab
    And Create a new Continuation Opportunity record type
    Then Fill all the mandatory details Stage "Opportunity,B,3" Region "Opportunity,D,3" Market "Opportunity,F,3" AccName "Opportunity,I,3" PreferredContact "Opportunity,H,3" Programme "Opportunity,S,2" VAT "Opportunity,T,2" Source "Opportunity,K,2"
    And Click on Save button
    And Adds the Donor field
    And Fill BodyStyle
    Then Fills Specifications and Options section HandOfDrive "Opportunity,U,2" Speedometer "Opportunity,V,2"
    And Click on Mark Stage as Complete
    Then User navigates to WorkOrder from Orders tab
    Then User Logout from SVO Portal

  @SSEC_2237_05 @sonica
  Scenario: Verify that if VIN(Donor details) is not added in the Continuation Opportunity record type then its not present in the Work Orders page too
    Given Access to SVO Portal as Classic Sales user with Username "LoginUsers,B,5" and password "LoginUsers,C,5"
    And User navigates to Opportunities tab
    And Create a new Continuation Opportunity record type
    Then Fill all the mandatory details Stage "Opportunity,B,3" Region "Opportunity,D,3" Market "Opportunity,F,3" AccName "Opportunity,I,3" PreferredContact "Opportunity,H,3" Programme "Opportunity,S,2" VAT "Opportunity,T,2" Source "Opportunity,K,2"
    And Click on Save button
    And Adds the Donor field
    And Fill BodyStyle
    Then Fills Specifications and Options section HandOfDrive "Opportunity,U,2" Speedometer "Opportunity,V,2"
    Then Verify that Orders tab is blank
    Then User Logout from SVO Portal

  @SSEC_2237_05 @sonica
  Scenario: Verify that if VIN(Donor details) is not added in the Continuation Opportunity record type then its not present in the Work Orders page too
    Given Access to SVO Portal as Classic Sales user with Username "LoginUsers,B,5" and password "LoginUsers,C,5"
    And User navigates to Opportunities tab
    And Create a new Continuation Opportunity record type
    Then Fill all the mandatory details Stage "Opportunity,B,3" Region "Opportunity,D,3" Market "Opportunity,F,3" AccName "Opportunity,I,3" PreferredContact "Opportunity,H,3" Programme "Opportunity,S,2" VAT "Opportunity,T,2" Source "Opportunity,K,2"
    And Click on Save button
    And Fill BodyStyle
    Then Fills Specifications and Options section HandOfDrive "Opportunity,U,2" Speedometer "Opportunity,V,2"
    And Click on Mark Stage as Complete
    Then User navigates to WorkOrder from Orders tab
    Then User Logout from SVO Portal
