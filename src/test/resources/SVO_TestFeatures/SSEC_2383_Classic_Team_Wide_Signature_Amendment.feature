Feature: Validations on Classic Parts User stories

  #User is able to add SO e2a Classic Reply Email Signature template for Classic Service
  @SSEC_2383_CaseCreation_1 @CustomerService
  Scenario: User is able to add SO e2a Classic Reply Email Signature template for Classic Service
    Given User access to SVO Portal as classic service Username user with Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    When User Navigate to Opportunity tab
    #And User Creates New Opportunity with "Opportunity,A,8" Record Type
    And User creates Classic Opportunity with all mandatory fields like Stage "Opportunity,B,2" Record Type "OpportunitySecondaryOwner,B,7" Region "Opportunity,D,2" Client "Opportunity,G,3" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" SAP Account "Opportunity,Q,3" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User click on the Emails to verify e2a emails ClassicEmail "Opportunity,W,2",eo2email "Opportunity,X,2"

  #User is able to add SO e2a Classic New Email Signature template for Classic Sales
  @SSEC_2383_CaseCreation_2 @CustomerSales
  Scenario: User is able to add SO e2a Classic New Email Signature template for Classic Sales
    Given User access to SVO Portal as classic service Username user with Username "LoginUsers,B,5" and Password "LoginUsers,C,5"
    When User Navigate to Opportunity tab
    #And User Creates New Opportunity with "Opportunity,A,8" Record Type
    And User creates Classic Opportunity with all mandatory fields like Stage "Opportunity,B,2" Record Type "OpportunitySecondaryOwner,B,7" Region "Opportunity,D,2" Client "Opportunity,G,3" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" SAP Account "Opportunity,Q,3" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User click on the Emails to verify e2a emails ClassicEmail "Opportunity,W,2",eo2email "Opportunity,X,2"

  #Verify in the Signature, the word 'Stock' is replaced with 'Vehicles' for Classic Sales
  @SSEC_2383_CaseCreation_3 @CustomerService
  Scenario: Verify in the Signature, the word 'Stock' is replaced with 'Vehicles' for Classic Sales
    Given User access to SVO Portal as classic service Username user with Username "LoginUsers,B,5" and Password "LoginUsers,C,5"
    When User Navigate to Opportunity tab
    #And User Creates New Opportunity with "Opportunity,A,8" Record Type
    And User creates Classic Opportunity with all mandatory fields like Stage "Opportunity,B,2" Record Type "OpportunitySecondaryOwner,B,7" Region "Opportunity,D,2" Client "Opportunity,G,3" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" SAP Account "Opportunity,Q,3" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User click on the Emails to verify e2a emails ClassicEmail "Opportunity,W,2",eo2email "Opportunity,X,2"

  #User is able to create a new Case for Classic Parts record type
  @SSEC_2383_Classic_Team_Wide_Signature_Amendment_01 @SSEC-2383
  Scenario: User is able to create a new Case for Classic Sales record type
    Given User access to SF SVO Portal as classic sales Username user with Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    When User navigates to an Opportunity tab
    And User creates Classic Opportunity with all mandatory fields like Stage "Opportunity,B,2" Record Type "OpportunitySecondaryOwner,B,3" Region "Opportunity,D,2" Client "Opportunity,G,8" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" SAP Account "Opportunity,Q,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    And User Verify the e2a Send an email button
    Then User Logout from an SVO Portal

  #User is able to create a new Case for Customer Service record type
  @SSEC_2383_Classic_Team_Wide_Signature_Amendment_02 @SSEC-2383
  Scenario: User is able to create a new Case for Customer Service record type
    Given User access to SF SVO Portal as classic service Username user with Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    When User navigates to an Opportunity tab
    And User creates Classic Opportunity with all mandatory fields like Stage "Opportunity,B,2" Record Type "OpportunitySecondaryOwner,B,3" Region "Opportunity,D,2" Client "Opportunity,G,8" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" SAP Account "Opportunity,Q,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    And User Verify the e2a Send an email button
    Then User Logout from an SVO Portal

  #User is able to add SO e2a Classic New Email Signature template for Classic Sales
  @SSEC_2383_Classic_Team_Wide_Signature_Amendment_03 @SSEC-2383
  Scenario: User is able to add SO e2a Classic New Email Signature template for Classic Sales
    Given User access to SF SVO Portal as classic sales Username user with Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    When User navigates to an Opportunity tab
    And User creates Classic Opportunity with all mandatory fields like Stage "Opportunity,B,2" Record Type "OpportunitySecondaryOwner,B,3" Region "Opportunity,D,2" Client "Opportunity,G,8" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" SAP Account "Opportunity,Q,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    And User Verify the e2a Send an email button
    And verify that user is able to add New Email Signature Template for classic Sales
    Then User Logouts from an SVO Portal
