Feature: SVO Work Order Validations

  @SVO_WorkOrder_01 @SVO_WorkOrder_ProgrammeField @SSEC-840
  Scenario: User is able to view 'Programme' field on 'My work orders' and 'All work orders' list view from work orders page
    Given Access to SVO Portal as Admin user with Username "LoginUsers,B,6" and Password "LoginUsers,C,6"
    Then User Navigates to Work Orders page from SVO Menu bar
    Then User selects My Work orders list from Select List view
    And Verify that programme field is displayed on My work orders list view
    Then User sort the My work orders list view based on programme field
    Then User selects All Work orders list from Select List view
    And Verify that programme field is displayed on All work orders list view
    Then User sort the All work orders list view based on programme field
    Then User Logout from the SVO Portal

  @SVO_WorkOrder_02 @SVO_WorkOrder_ProgrammeField @SSEC-841
  Scenario: User is not able to view 'Programme' field on 'New work orders' and 'Orders in Build' list view from work orders page
    Given Access to SVO Portal as Classic Operations user with Username "LoginUsers,B,9" and Password "LoginUsers,C,9"
    #Given laterally Access SVO Portal by user name "LoginUsers,B,9" and Password "LoginUsers,C,9"
    Then User Navigates to Work Orders page from SVO Menu bar
    And User selects New work orders list from Select List view
    Then User verify that 'Programme' field is not displayed on 'New work orders' list view from work orders page
    And User selects Orders in Build list from Select List view
    Then User verify that 'Programme' field is not displayed on 'Orders in Build' list view from work orders page
    Then User Logout from the SVO Portal

  @SVO_WorkOrder_03 @SVO_WorkOrder_ProgrammeField @SSEC-842
  Scenario: User is not able to view 'Programme' field on 'My work orders' and 'All work orders' list view from work orders page
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    #Given laterally Access SVO Portal by user name "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then Verify that user is not able to view Work orders page from SVO Menu bar
    Then User Logout from the SVO Portal

  @SVO_WorkOrder_04 @SVO_WorkOrder_ProgrammeField @SSEC-843
  Scenario: User is able to view 'Programme' field on 'My work orders' and 'All work orders' list view from work orders page
    Given Access to SVO Portal as Classic Admin user with Username "LoginUsers,B,13" and Password "LoginUsers,C,13"
    #Given laterally Access SVO Portal by user name "LoginUsers,B,13" and Password "LoginUsers,C,13"
    Then User Navigates to Work Orders page from SVO Menu bar
    Then User selects My Work orders list from Select List view
    And Verify that programme field is displayed on My work orders list view
    Then User sort the My work orders list view based on programme field
    Then User selects All Work orders list from Select List view
    And Verify that programme field is displayed on All work orders list view
    Then User sort the All work orders list view based on programme field
    Then User Logout from the SVO Portal

  @SVO_WorkOrder_05 @SVO_WorkOrder_ProgrammeField @SSEC-844
  Scenario: User is not able to view 'New work orders' and 'Orders in Build' list view from work orders page
    Given Access to SVO Portal as Classic Admin user with Username "LoginUsers,B,13" and Password "LoginUsers,C,13"
    #Given laterally Access SVO Portal by user name "LoginUsers,B,13" and Password "LoginUsers,C,13"
    Then User Navigates to Work Orders page from SVO Menu bar
    Then Verify that user is not be able to view 'New Work orders' from list view drop down on work orders page
    And Verify that user is not be able to view 'Orders in Build' from list view drop down on work orders page
    Then User Logout from the SVO Portal

  @SVO_WorkOrder_06 @SVO_WorkOrder_ProgrammeField @SSEC-845
  Scenario: User is not able to view 'Programme' field on 'My work orders' and 'All work orders' list view from work orders page
    Given Access to SVO Portal as bespoke operation user with Username "LoginUsers,B,8" and Password "LoginUsers,C,8"
    #Given laterally Access SVO Portal by user name "LoginUsers,B,8" and Password "LoginUsers,C,8"
    Then Verify that user is not able to view Work orders page from SVO Menu bar
    Then User Logout from the SVO Portal

  @SVO_WorkOrder_07 @SVO_WorkOrder_ProgrammeField @SSEC-846
  Scenario: User is not able to view 'Programme' field on 'My work orders' and 'All work orders' list view from work orders page
    Given Access to SVO Portal as Classic Finance user with Username "LoginUsers,B,10" and Password "LoginUsers,C,10"
    #Given laterally Access SVO Portal by user name "LoginUsers,B,10" and Password "LoginUsers,C,10"
    Then Verify that user is not able to view Work orders page from SVO Menu bar
    Then User Logout from the SVO Portal

  @SVO_WorkOrder_08 @SVO_WorkOrder_ProgrammeField @SSEC-847
  Scenario: User is able to view newly created work order in 'My work orders' and 'All work orders' list view from work orders page
    Given Access to SVO Portal as Admin user with Username "LoginUsers,B,6" and Password "LoginUsers,C,6"
    #Given laterally Access SVO Portal by user name "LoginUsers,B,6" and Password "LoginUsers,C,6"
    Then User Navigates to Work Orders page from SVO Menu bar
    Then User creates a new work order
    Then User Navigates back to Work Orders page from SVO Menu bar
    And user is able to view newly created work order on 'My work orders' list view
    Then user is able to view newly created work order on 'All work orders' list view
    Then User Logout from the SVO Portal

  @SVO_WorkOrder_09 @SVO_WorkOrder_ProgrammeField @SSEC-848
  Scenario: User is not able to view admin created work order in 'My work orders' list view from work orders page
    Given Access to SVO Portal as Admin user with Username "LoginUsers,B,6" and Password "LoginUsers,C,6"
    #Given laterally Access SVO Portal by user name "LoginUsers,B,6" and Password "LoginUsers,C,6"
    Then User Navigates to Work Orders page from SVO Menu bar
    Then User creates a new work order
    Then User Navigates back to Work Orders page from SVO Menu bar
    And user is able to view newly created work order on 'My work orders' list view
    Then Access to SVO Portal as classic service from Add Username user with Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    Then User Navigates to Work Orders page from SVO Menu bar
    And user is not able to view newly created work order on 'My work orders' list view
    Then User Logout from the SVO Portal
