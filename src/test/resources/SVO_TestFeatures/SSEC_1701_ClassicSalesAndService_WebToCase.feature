Feature: Validations for Classic sales and Service web to case link contact to case

  #User is able to verify classic sales case created in salesforce through classic sales web form
  @ClassicSalesAndService_WebToCase_01
  Scenario: User is able to verify classic sales case created in salesforce through classic sales web form
    Given User logins to the Ecommerce "CaseCreation,L,2" portal
    Then User Navigate to the Contact Us section
    And User creates Classic sales Case from web form with Email "CaseCreation,A,2" First name "CaseCreation,G,2" Last name "CaseCreation,H,2" Country "CaseCreation,I,2" Phone number "CaseCreation,K,2" VIN number "CaseCreation,J,2"
    Then Verify that the user receives an outlook email notification on case creation
    And User logout from an Ecommerce Portal	
    Given User Access to SVO Portal with Classic Sales user with the Username "LoginUsers,B,5" and Password "LoginUsers,C,5"
    And User Navigated to Contacts Tab
    Then User creates a new contact with contact name "CaseCreation,D,2" and AccountName "CaseCreation,E,2"
    And Verify that new contact is created on salesforce
    When User Navigates to the Cases Tab	
    And Verify that Classic Sales case is automatically created on SVO Portal
    When User link the created case with newly created contact
    And User verifies that case is linked with newly created contact along with autopopulated Account field
    Then User logout from an SF SVO Portal
	
  #User is able to verify classic sales case created in salesforce through web form and link existing contact
  @ClassicSalesAndService_WebToCase_02
  Scenario: User is able to verify classic sales case created in salesforce through web form and link existing contact
    Given User logins to the Ecommerce "CaseCreation,L,2" portal
    Then User Navigate to the Contact Us section
    And User creates Classic sales Case from web form with Email "CaseCreation,A,2" First name "CaseCreation,G,2" Last name "CaseCreation,H,2" Country "CaseCreation,I,2" Phone number "CaseCreation,K,2" VIN number "CaseCreation,J,2"
    Then Verify that the user receives an outlook email notification on case creation
    And User logout from an Ecommerce Portal
    Given User Access to SVO Portal with Classic Sales user with the Username "LoginUsers,B,5" and Password "LoginUsers,C,5"
    When User Navigates to the Cases Tab
    And Verify that Classic Sales case is automatically created on SVO Portal
    Then User verifies that newly created case through web is linked with existing Account and Contact
    Then User logout from an SF SVO Portal

  #User is able to verify classic service case created in salesforce through classic service web form
  @ClassicSalesAndService_WebToCase_03
  Scenario: User is able to verify classic service case created in salesforce through classic service web form
    Given User logins to the Ecommerce "CaseCreation,L,2" portal
    Then User Navigate to the Contact Us section
    And User creates Classic service Case from web form with Email "CaseCreation,A,2" First name "CaseCreation,G,2" Last name "CaseCreation,H,2" Country "CaseCreation,I,2" Phone number "CaseCreation,K,2" VIN number "CaseCreation,J,2"
    Then Verify that the user receives an outlook email notification on case creation
    And User logout from an Ecommerce Portal
    Given User access to SF SVO Portal as classic service Username user with Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    And User Navigated to Contacts Tab
    Then User creates a new contact with contact name "CaseCreation,D,2" and AccountName "CaseCreation,E,2"
    And Verify that new contact is created on salesforce
    When User Navigates to the Cases Tab
    And Verify that Classic Service case is automatically created on SVO Portal
    When User link the created case with newly created contact
    And User verifies that case is linked with newly created contact along with autopopulated Account field
    Then User logout from an SF SVO Portal

  #User is able to verify classic service case created in salesforce through web form and link existing contact
  @ClassicSalesAndService_WebToCase_04
  Scenario: User is able to verify classic service case created in salesforce through web form and link existing contact
    Given User logins to the Ecommerce "CaseCreation,L,2" portal
    Then User Navigate to the Contact Us section
    And User creates Classic service Case from web form with Email "CaseCreation,A,2" First name "CaseCreation,G,2" Last name "CaseCreation,H,2" Country "CaseCreation,I,2" Phone number "CaseCreation,K,2" VIN number "CaseCreation,J,2"
    Then Verify that the user receives an outlook email notification on case creation
    And User logout from an Ecommerce Portal
    Given User access to SF SVO Portal as classic service Username user with Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    When User Navigates to the Cases Tab
    And Verify that Classic Service case is automatically created on SVO Portal
    When User link the created case with newly created contact
    And User verifies that case is linked with newly created contact along with autopopulated Account field
    Then User logout from an SF SVO Portal

  #User is not able to view classic service case created on salesforce through web form
  @ClassicSalesAndService_WebToCase_05
  Scenario: User is not able to view classic service case created on salesforce through web form
    Given User logins to the Ecommerce "CaseCreation,L,2" portal
    Then User Navigate to the Contact Us section
    And User creates Classic service Case from web form with Email "CaseCreation,A,2" First name "CaseCreation,G,2" Last name "CaseCreation,H,2" Country "CaseCreation,I,2" Phone number "CaseCreation,K,2" VIN number "CaseCreation,J,2"
    Then Verify that the user receives an outlook email notification on case creation
    And User logout from an Ecommerce Portal
    Given User access to the SVO Portal as Classic Finance user with Username "LoginUsers,B,10" and Password "LoginUsers,C,10"
    And Verify that user is not able to view classic service case created on salesforce through web form
    Then User logout from an SF SVO Portal

  #User is not able to view classic sales case created on salesforce through classic sales web form
  @ClassicSalesAndService_WebToCase_06
  Scenario: User is not able to view classic sales case created on salesforce through classic sales web form
    Given User logins to the Ecommerce "CaseCreation,L,2" portal
    Then User Navigate to the Contact Us section
    And User creates Classic sales Case from web form with Email "CaseCreation,A,2" First name "CaseCreation,G,2" Last name "CaseCreation,H,2" Country "CaseCreation,I,2" Phone number "CaseCreation,K,2" VIN number "CaseCreation,J,2"
    Then Verify that the user receives an outlook email notification on case creation
    And User logout from an Ecommerce Portal
    Given User access to SF SVO Portal as classic service Username user with Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    When User Navigates to the Cases Tab
    And Verify that user is not able to view classic sales case created on salesforce through classic sales web form
    Then User logout from an SF SVO Portal

  #User is not able to view classic service case created on salesforce through classic service web form
  @ClassicSalesAndService_WebToCase_07
  Scenario: User is not able to view classic service case created on salesforce through classic service web form
    Given User logins to the Ecommerce "CaseCreation,L,2" portal
    Then User Navigate to the Contact Us section
    And User creates Classic service Case from web form with Email "CaseCreation,A,2" First name "CaseCreation,G,2" Last name "CaseCreation,H,2" Country "CaseCreation,I,2" Phone number "CaseCreation,K,2" VIN number "CaseCreation,J,2"
    Then Verify that the user receives an outlook email notification on case creation
    And User logout from an Ecommerce Portal
    Given User Access to SVO Portal with Classic Sales user with the Username "LoginUsers,B,5" and Password "LoginUsers,C,5"
    When User Navigates to the Cases Tab
    And Verify that user is not able to view classic service case created on salesforce through classic service web form
    Then User logout from an SF SVO Portal

  #User is not able to view created case in salesforce if the case is not submitted from web
  @ClassicSalesAndService_WebToCase_08
  Scenario: User is not able to view created case in salesforce if the case is not submitted from web
    Given User logins to the Ecommerce "CaseCreation,L,2" portal
    Then User Navigate to the Contact Us section
    And User creates Classic service Case from web form with Email "CaseCreation,A,2" First name "CaseCreation,G,2" Last name "CaseCreation,H,2" Country "CaseCreation,I,2" Phone number "CaseCreation,K,2" VIN number "CaseCreation,J,2"
    And User logout from an Ecommerce Portal
    Given User Access to SVO Portal with Classic Sales user with the Username "LoginUsers,B,5" and Password "LoginUsers,C,5"
    When User Navigates to the Cases Tab
    And Verify that user is not able to view created case in salesforce if the case is not submitted from web
    Then User logout from an SF SVO Portal

  #User is not able to submit web form to create a  new case, if any mandaroty fields are missing in web form
  @ClassicSalesAndService_WebToCase_09
  Scenario: User is not able to submit web form to create a  new case, if any mandaroty fields are missing in web form
    Given User logins to the Ecommerce "CaseCreation,L,2" portal
    Then User Navigate to the Contact Us section
    And User tries to create Classic sales Case without mandatory fields from web form with Email "CaseCreation,A,2" First name "CaseCreation,G,2" Last name "CaseCreation,H,2" Country "CaseCreation,I,2" Phone number "CaseCreation,K,2" VIN number "CaseCreation,J,2"
    Then Verify that user is not able to submit web form to create a  new case, if any mandaroty fields are missing in web form
    And User logout from an Ecommerce Portal
