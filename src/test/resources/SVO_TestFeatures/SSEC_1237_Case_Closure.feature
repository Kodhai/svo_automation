Feature: Use is able to close the Case for Classic Parts and Technical and Customer Services

  #User is able to close newly created Customer Service Case which is created via Salesforce
  @SSEC_1237_CloseCase_01 @SSEC-1994
  Scenario: User is able to close newly created Customer Service Case which is created via Salesforce
    Given Access to SVO Portal as Classic Customer Service user with Username "LoginUsers,B,21" and Password "LoginUsers,C,21"
    When User navigates to Cases tab
    And User creates new Customer Service record type with Case Origin as Salesforce
    And User saves the customer service case
    Then user closes the case with reason as Question Answered
    Then User logouts from the SF SVO

  #User is able to close newly created Parts & Technical Case which is created via Email
  @SSEC_1237_CloseCase_02 @SSEC-1995
  Scenario: User is able to close newly created Parts & Technical Case which is created via Email
    Given Access to SVO Portal as Classic Parts and technical user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User navigates to Cases tab
    And User creates new Parts and technical record type with Case Origin as Email
    And User saves the Parts and Technical case
    Then user closes the case with reason as Vin not Eligible
    Then User logouts from the SF SVO

  #User is able to close newly created Customer Service Case which is created via Website
  @SSEC_1237_CloseCase_03 @SSEC-1996
  Scenario: User is able to close newly created Customer Service Case which is created via Website
    Given Access to SVO Portal as Classic Customer Service user with Username "LoginUsers,B,21" and Password "LoginUsers,C,21"
    When User navigates to Cases tab
    And User creates new Customer Service record type with Case Origin as Website
    And User saves the customer service case
    Then user closes the case with reason as Transferred to Another Department
    Then User logouts from the SF SVO

  #User is able to close newly created Parts & Technical Case which is created via Web
  @SSEC_1237_CloseCase_04 @SSEC-1997
  Scenario: User is able to close newly created Parts & Technical Case which is created via Web
    Given Access to SVO Portal as Classic Parts and technical user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User navigates to Cases tab
    And User creates new Parts and technical record type with first name, last name and Case Origin as Website and VIN
    And User saves the Parts and Technical case
    Then user closes the case with reason as Part not available
    Then User logouts from the SF SVO

  #User is able to close newly created Parts & Technical Case which is created via Salesforce
  @SSEC_1237_CloseCase_05 @SSEC-1998
  Scenario: User is able to close newly created Parts & Technical Case which is created via Salesforce
    Given Access to SVO Portal as Classic Parts and technical user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User navigates to Cases tab
    And User creates new Parts and technical record type with first name, last name and Case Origin as Salesforce and VIN
    And User saves the Parts and Technical case
    Then user closes the case with reason as Other
    Then User logouts from the SF SVO

  #User is not able to Close Parts & Technical Case without filling the mandatory details
  @SSEC_1237_CloseCase_06 @SSEC-1999
  Scenario: User is not able to Close Parts & Technical Case without filling the mandatory details
    Given Access to SVO Portal as Classic Parts and technical user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User navigates to Cases tab
    And User creates new Parts and technical record type with first name, last name and Case Origin as Salesforce and VIN
    And User saves the Parts and Technical case
    Then User removes the mandatory field details and saves the case
    Then user closes the case
    And Verifies that closed case has all the mandatory fields
    Then User logouts from the SF SVO

  #User is able to close newly created Customer Service Case which is created via Salesforce without any reason
  @SSEC_1237_CloseCase_07 @SSEC-2000
  Scenario: User is able to close newly created Customer Service Case which is created via Salesforce without any reason
    Given Access to SVO Portal as Classic Customer Service user with Username "LoginUsers,B,21" and Password "LoginUsers,C,21"
    When User navigates to Cases tab
    And User creates new Customer Service record type with Case Origin as Salesforce
    And User saves the customer service case
    Then user closes the case without selecting any reason
    Then User logouts from the SF SVO
