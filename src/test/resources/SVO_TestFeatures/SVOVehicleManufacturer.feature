Feature: Validate SVO Vehicle Manufacturer

 	#User tries to Change Owner for multiple Vehicle manufacturers at a time
  @SVO @SVO_224
  Scenario: User tries to Change Owner for multiple Vehicle manufacturers at a time
    Given Access SF-SVO Portal
    Then Navigate to Vehicle manufacturers tab
    Then Select multiple Vehicle manufacturers and click on Change Owner
    Then Change the owner and save
    Then Logout from SF-SVO Portal

  #User tries to make a Brand as 'Is JLR Owned Brand' in Vehicle manufacturers section
  @SVO @SVO_225
  Scenario: User tries to make a Brand as 'Is JLR Owned Brand' in Vehicle manufacturers section
    Given Access SF-SVO Portal
    Then Navigate to Vehicle manufacturers tab
    Then Edit for Is JLR owned brand field and select the checkbox
    Then Save the changes and verify user is able to make the brand as Is JLR Owned Brand
    Then Logout from SF-SVO Portal

  #User tries to add New Vehicle series for a Model of any Vehicle Manufacturer
  @SVO @SVO_226
  Scenario: User tries to add New Vehicle series for a Model of any Vehicle Manufacturer
    Given Access SF-SVO Portal
    Then Navigate to Vehicle manufacturers tab
    Then Select a manufacturer
    Then Select a model
    Then Navigate to Vehicle series and click on New button
    Then Enter details Name "Test Vehicle" model year start "2020" model year end "2030" and save
    Then Logout from SF-SVO Portal

  #User tries to add New Body style for a Model of any Vehicle Manufacturer
  @SVO @SVO_227
  Scenario: User tries to add New Body style for a Model of any Vehicle Manufacturer
    Given Access SF-SVO Portal
    Then Navigate to Vehicle manufacturers tab
    Then Select a manufacturer
    Then Select a model
    Then Navigate to Body Styles and click on New button
    Then Enter Name "Test Body Style" and save
    Then Logout from SF-SVO Portal

  #User tries to add New Vehicle derivatives for a Model of any Vehicle Manufacturer
  @SVO @SVO_228
  Scenario: User tries to add New Vehicle derivatives for a Model of any Vehicle Manufacturer
    Given Access SF-SVO Portal
    Then Navigate to Vehicle manufacturers tab
    Then Select a manufacturer
    Then Select a model
    Then Navigate to Vehicle Derivatives and click on New button
    Then Enter derivative Name "Test derivative" model "Opportunity,J,2" series Name "Test Vehicle" and save
    Then Logout from SF-SVO Portal

  #User creates Record share data for any Model of Vehicle Manufacturer
  @SVO @SVO_229
  Scenario: User creates Record share data for any Model of Vehicle Manufacturer
    Given Access SF-SVO Portal
    Then Navigate to Vehicle manufacturers tab
    Then Select a manufacturer
    Then Select a model
    Then Click on Sharing button
    Then Enter the user name "Opportunity,H,3" and save
    Then Logout from SF-SVO Portal
    
    #User tries to Enter Model start date greater than the Model end date of Vehicle manufacturer 
		@SVO @SVO_231
		Scenario: User tries to Enter Model start date greater than the Model end date of Vehicle manufacturer 
			Given Access SF-SVO Portal
			Then Navigate to Vehicle manufacturers tab
			Then Select a manufacturer
			Then Select a model
			Then Enter model start year as "2022" model end year as "2021" and save
			Then Verify the error message displayed for model end year entered before model start year
			Then Logout from SF-SVO Portal

		#User tries to access Vehicle Manufacturer section
		#Classic Sales role 
		@SVO @SVO_232
		Scenario: User tries to access Vehicle Manufacturer section 
			Given Access SF-SVO Portal
			Then Search for "Vehicle manufacturer" in search box
			Then Verify no search results are found for Vehicle manufacturer
			Then Logout from SF-SVO Portal

		#User tries to Edit Vehicle manufacturer details 
		@SVO @SVO_233
		Scenario: User tries to Edit Vehicle manufacturer details 
			Given Access SF-SVO Portal
			Then Navigate to Vehicle manufacturers tab
			Then Select a manufacturer
			Then Edit vehicle manufacturer name as "New Vehicle" and save
			Then Logout from SF-SVO Portal

#User tries to delete the Vehicle manufacturer 
@SVO @SVO_234
Scenario: User tries to delete the Vehicle manufacturer   
	Given Access SF-SVO Portal
	Then Navigate to Vehicle manufacturers tab
	Then Create a new vehicle manufacturer with name "New Vehicle" and save
	Then Click on Delete button to delete vehicle manufacturer
	Then Logout from SF-SVO Portal

#User tries to Change Owner for one of the Vehicle manufacturer 
@SVO @SVO_235
Scenario: User tries to Change Owner for one of the Vehicle manufacturer   
	Given Access SF-SVO Portal
	Then Navigate to Vehicle manufacturers tab
	Then Select only one Vehicle manufacturer and click on Change Owner
	Then Change the owner and save
	Then Logout from SF-SVO Portal
	
#User tries to change owner of Vehicle manufacturer without selecting any Vehicle manufacturer from the Manufacturers list
@SVO @SVO_236
Scenario: User tries to change owner of Vehicle manufacturer without selecting any Vehicle manufacturer from the Manufacturers list
	Given Access SF-SVO Portal
	Then Navigate to Vehicle manufacturers tab
	And User tries to change owner of Vehicle Manufacturer without selecting any Vehicle Manufacturer from list
	Then Logout from SF-SVO Portal
	
#User tries to create New Manufacturer of Vehicles
@SVO @SVO_237
Scenario: User tries to create New Manufacturer of Vehicles
	Given Access SF-SVO Portal
	Then Navigate to Vehicle manufacturers tab
	Then Create a new vehicle manufacturer with name "New Vehicle Manufacturer" and save
	Then Logout from SF-SVO Portal
	
#User tries add New Model for any Brand
@SVO @SVO_238
Scenario: User tries add New Model for any Brand
	Given Access SF-SVO Portal
	Then Navigate to Vehicle manufacturers tab
	Then Select a manufacturer
	And User tries to add New Model for any Brand
	Then Logout from SF-SVO Portal
	
#User tries to delete a Model of any Brand under Vehicle Manufacturer section
@SVO @SVO_239
Scenario: User tries to delete a Model of any Brand under Vehicle Manufacturer section
	Given Access SF-SVO Portal
	Then Navigate to Vehicle manufacturers tab
	Then Select a manufacturer
	And User tries to add New Model for any Brand
	Then User tries to delete a Model of any Brand under Vehicle Manufacturer section 
	Then Logout from SF-SVO Portal





    
    