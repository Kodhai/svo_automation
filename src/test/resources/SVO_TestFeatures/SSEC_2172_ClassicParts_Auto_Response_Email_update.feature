@SSEC-2172
Feature: Validate Auto Response Email Update Scenarios

  @SSEC_2172_01 @sonica
  Scenario: User is able to edit auto response template for Classic Parts
    Given Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User navigates to Cases tab
    And User creates new Parts and technical record type with Case Origin as Email
    And User saves the Parts and Technical case
    Then User goes to Activity tab
    And Clicks on Compose button and then clicks on insert template and selects the template
    Then User clicks on Send button and verifies that the email is sent
    #Then User logouts from the portal

  @SSEC_2172_02 @sonica
  Scenario: User is able to edit auto response template for Classic Parts web to case default response
    Given User Access to the salesforce Portal as Classic Parts and Technical user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User navigates to Cases tab
    And User creates new Parts and technical record type with Case Origin as Email
    And User saves the Parts and Technical case
    Then User goes to Activity tab
    And Clicks on Compose button and then clicks on insert template and selects the template
    Then edit the auto response template
    Then User clicks on Send button and verifies that the email is sent
    Then User logouts from the portal

  @SSEC_2172_03 @sonica
  Scenario: User is able to select the auto response template for Email
    Given Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User navigates to Cases tab
    And User creates new Parts and technical record type with Case Origin as Email
    And User saves the Parts and Technical case
    Then User goes to Activity tab
    And Clicks on Compose button and then clicks on insert template and selects the template
    Then User clicks on Send button
    Then User logouts from the portal

  @SSEC_2172_04 @sonica
  Scenario: User is able to view the auto response email sent for Classic parts case in Emails tab
    Given User Access to the salesforce Portal as Classic Parts and Technical user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User navigates to Cases tab
    And User creates new Parts and technical record type with Case Origin as Email
    And User saves the Parts and Technical case
    Then User goes to Activity tab
    And Clicks on Compose button and then clicks on insert template and selects the template
    Then User clicks on Send button
    Then User navigates to Emails tab from Quick Links and click on Email record
    Then User logouts from the portal
