Feature: SVO Pricing Logic Validations

  @SVO_Pricing_Logic_01 @SSEC-1002 @SalesAgreement @PricingLogicBatch
  Scenario: User is able to view sum of Retail Prices for multiple vehicles on sales agreement proposal document of an opportunity
    Given Access to SVO bespoke Portal with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    When User navigates to an Opportunity tab
    And User create a SVO Bespoke Opportunity with check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User navigates to Additional Vehicle Link from Quick Links
    And User adds one Vehicle Record with all details Brand "Additional_Vehicles,A,3", Model "Additional_Vehicles,B,3", Series Name "Additional_Vehicles,C,3", Derivative "Additional_Vehicles,D,3" and Model Year "Additional_Vehicles,E,3"
    And User adds second Vehicle Record with all details like Brand "Additional_Vehicles,A,3" Model "Additional_Vehicles,B,3" Series Name "Additional_Vehicles,C,3" Derivative "Additional_Vehicles,D,3" and Model Year "Additional_Vehicles,E,3"
    Then User navigate back to Opportunity page
    Then User create a Design Brief Quote with all fields like Retailer Contact "Opportunity,J,2"
    Then User submit Design Brief Request and return the render pack
    Then User navigate back to Opportunity page
    And User moves the design brief quote stage to proposal sent stage
    Then User send sales agreement document of design brief quote
    And User verifies the pricing details of main vehicle and Additional vehicles on sales agreement document
    #Then User Logout from the SVO Portal
	
  @SVO_Pricing_Logic_02 @SSEC-1030 @PricingLogicBatch
  Scenario: User is not able to view total pricing details on sales agreement proposal document without sending design brief quote to sales agreement
    #Given Access to SVO bespoke Portal with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Given laterally Access SVO Portal by user name "LoginUsers,B,3" and Password "LoginUsers,C,3"
    When User Navigate to Opportunity tab
    And User create a SVO Bespoke Opportunity with check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User navigates to Additional Vehicle Link from Quick Links
    And User adds one Vehicle Record with fields like Brand "Additional_Vehicles,A,3", Model "Additional_Vehicles,B,3", Series Name "Additional_Vehicles,C,3", Derivative "Additional_Vehicles,D,3" and Model Year "Additional_Vehicles,E,3"
    Then User navigate back to Opportunity page
    Then User create a Design Brief Quote with all fields like Retailer Contact "Opportunity,J,2"
    Then User submit Design Brief Request and return the render pack
    Then User navigate back to Opportunity page
    And User moves the design brief quote stage to proposal sent stage
    Then Verify the Agreement section is not present under related quick links
    #Then Logout from an SVO main Portal

  @SVO_Pricing_Logic_03 @SSEC-1032 @PricingLogicBatch
  Scenario: User is not able to view Agreement section under related quick links for any opportunity
    Given Access to SVO bespoke operation Portal with Username "LoginUsers,B,8" and Password "LoginUsers,C,8"
    #Given laterally Access SVO Portal by user name "LoginUsers,B,8" and Password "LoginUsers,C,8"
    When User navigates to an Opportunity tab
    And User create a SVO Bespoke Opportunity with check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User navigates to Additional Vehicle Link from Quick Links
    And User adds one Vehicle Record with fields like Brand "Additional_Vehicles,A,3", Model "Additional_Vehicles,B,3", Series Name "Additional_Vehicles,C,3", Derivative "Additional_Vehicles,D,3" and Model Year "Additional_Vehicles,E,3"
    Then User navigate back to Opportunity page
    Then User create a Design Brief Quote with all fields like Retailer Contact "Opportunity,J,2"
    Then User submit Design Brief Request and return the render pack
    Then User navigate back to Opportunity page
    And User moves the design brief quote stage to proposal sent stage
    Then Verify the Agreement section is not present under related quick links
    #Then Logout from an SVO main Portal

  @SVO_Pricing_Logic_04 @SSEC-989 @PricingLogicBatch
  Scenario: User is not able to send private office quote to sales agreement for an opportunity
    Given Access to SVO admin Portal with Username "LoginUsers,B,6" and Password "LoginUsers,C,6"
    #Given laterally Access SVO Portal by user name "LoginUsers,B,6" and Password "LoginUsers,C,6"
    When User navigates to an Opportunity tab
    And User create new SVO Private Office Opportunity with mandatory fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,3" Region "Opportunity,D,2" Client "Opportunity,G,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User is not able to view Additional vehicles section/Additional vehicles hyper link for Private office opportunity
    Then User create a new Quote with mandatory fields like Retailer Contact "Opportunity,J,2" 
    And User is not able to view send sales agreement button on quote page
    #Then User Logout from the SVO Portal

  @SVO_Pricing_Logic_05 @SSEC-1003 @SalesAgreement_01 @PricingLogicBatch
  Scenario: User is able to view only Retail Price of the main vehicle as total price on sales agreement proposal document of an opportunity.
    #Given Access to SVO bespoke Portal with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Given laterally Access SVO Portal by user name "LoginUsers,B,3" and Password "LoginUsers,C,3"
    When User navigates to an Opportunity tab
    And User create a SVO Bespoke Opportunity with check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2" 
    Then User create a Design Brief Quote with all fields like Retailer Contact "Opportunity,J,2"
    Then User submit Design Brief Request and return the render pack
    Then User navigate back to Opportunity page
    And User moves the design brief quote stage to proposal sent stage
    Then User send sales agreement document of design brief quote
    And User verifies the pricing details of only main vehicle on sales agreement document
    #Then User Logout from the SVO Portal

  @SVO_Pricing_Logic_06 @SSEC-990 @PricingLogicBatch
  Scenario: User is able to view single additional vehicle pricing details on Additional vehicles page of an Opportunity
    #Given Access to SVO Portal by bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Given laterally Access SVO Portal by user name "LoginUsers,B,3" and Password "LoginUsers,C,3"
    When User navigates to an Opportunity tab
    And User create a SVO Bespoke Opportunity with check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User navigates to Additional Vehicle Link from Quick Links
    And User adds one Vehicle Record with fields like Brand "Additional_Vehicles,A,3", Model "Additional_Vehicles,B,3", Series Name "Additional_Vehicles,C,3", Derivative "Additional_Vehicles,D,3" and Model Year "Additional_Vehicles,E,3"
    Then User navigate back to Opportunity page
    Then User create and accept a Design Brief Quote with all fields like Retailer Contact "Opportunity,J,2"
    Then User navigates to Additional Vehicle Link from Quick Links
    And Verify that updated pricing details for additional vehicle record on additional vehicle page
    #Then User Logout from the SVO Portal

  @SVO_Pricing_Logic_07 @SSEC-991 @PricingLogicBatch
  Scenario: User is not able to view vehicle pricing details on Additional vehicles page for Private office Opportunity
    #Given Access to SVO Portal by private office user with Username "LoginUsers,B,4" and Password "LoginUsers,C,4"
    Given laterally Access SVO Portal by user name "LoginUsers,B,4" and Password "LoginUsers,C,4"
    When User navigates to an Opportunity tab
    And User create a SVO Private Office Opportunity with Restricted fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,3" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,3" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User is not able to view Additional vehicles section/Additional vehicles hyper link for Private office opportunity
    #Then User Logout from the SVO Portal

  @SVO_Pricing_Logic_08 @SSEC-992 @PricingLogicBatch
  Scenario: User is not able to view the vehicle pricing details from Quote on Additional vehicles page, if quote is not accepted
    #Given Access to SVO Portal by bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Given laterally Access SVO Portal by user name "LoginUsers,B,3" and Password "LoginUsers,C,3"
    When User navigates to an Opportunity tab
    And User create a SVO Bespoke Opportunity with check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User navigates to Additional Vehicle Link from Quick Links
    And User adds one Vehicle Record with fields like Brand "Additional_Vehicles,A,3", Model "Additional_Vehicles,B,3", Series Name "Additional_Vehicles,C,3", Derivative "Additional_Vehicles,D,3" and Model Year "Additional_Vehicles,E,3"
    Then User create a Design Brief Quote with fields like Retailer Contact "Opportunity,J,2"
    And User is able to verify that additional vehicle pricing details are not dispayed on additional vehicle page
    #Then User Logout from the SVO Portal

  @SVO_Pricing_Logic_09 @SSEC-1004 @PricingLogicBatch
  Scenario: User is able to add multiple additional vehicles to an opportunity on Additional vehicles page
    #Given Access to SVO Portal by bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Given laterally Access SVO Portal by user name "LoginUsers,B,3" and Password "LoginUsers,C,3"
    When User navigates to an Opportunity tab
    And User create a SVO Bespoke Opportunity with check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User navigates to Additional Vehicle Link from Quick Links
    And User adds one Vehicle Record with fields like Brand "Additional_Vehicles,A,3", Model "Additional_Vehicles,B,3", Series Name "Additional_Vehicles,C,3", Derivative "Additional_Vehicles,D,3" and Model Year "Additional_Vehicles,E,3"
    And User adds multiple Vehicle Record with fields like Brand "Additional_Vehicles,A,3", Model "Additional_Vehicles,B,3", Series Name "Additional_Vehicles,C,3", Derivative "Additional_Vehicles,D,3" and Model Year "Additional_Vehicles,E,3"
    Then User navigate back to Opportunity page
    Then User create and accept a Design Brief Quote with all fields like Retailer Contact "Opportunity,J,2"
    Then User navigates to Additional Vehicle Link from Quick Links
    Then User is able to view pricing details of first additional vehicle
    Then User is able to view pricing details of second additional vehicle
    #Then User Logout from the SVO Portal

  @SVO_Pricing_Logic_10 @SSEC-1028 @PricingLogicBatch
  Scenario: User is able to view total pricing details of both main vehicle and additional vehicle on opportunity page
    #Given Access to SVO bespoke operation Portal with Username "LoginUsers,B,8" and Password "LoginUsers,C,8"
    Given laterally Access SVO Portal by user name "LoginUsers,B,8" and Password "LoginUsers,C,8"
    When User navigates to an Opportunity tab
    And User create a SVO Bespoke Opportunity with check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User moves opportunity to Contract Signed stage
    And User create and accept a Design Brief Quote with all fields like Retailer Contact "Opportunity,J,2"
    Then User navigates to Additional Vehicle Link from Quick Links
    And User adds one Vehicle Record with fields like Brand "Additional_Vehicles,A,3", Model "Additional_Vehicles,B,3", Series Name "Additional_Vehicles,C,3", Derivative "Additional_Vehicles,D,3" and Model Year "Additional_Vehicles,E,3"
    And User is able to view total pricing details of both main vehicle and additional vehicle on opportunity page
    #Then Logout from an SVO main Portal

  @SVO_Pricing_Logic_11 @SSEC-1033 @PricingLogicBatch
  Scenario: User is able to view total pricing details for multiple additional vehicles on opportunity layout page
    #Given Access to SVO bespoke Portal with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Given laterally Access SVO Portal by user name "Log1inUsers,B,3" and Password "LoginUsers,C,3"
    When User Navigate to Opportunity tab
    And User create a new SVO Bespoke Opportunity with mandatory fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User create and accept a Design Brief Quote with all fields like Retailer Contact "Opportunity,J,2"
    Then User navigates to Additional Vehicle Link from Quick Links
    And User adds one Vehicle Record with all details Brand "Additional_Vehicles,A,3", Model "Additional_Vehicles,B,3", Series Name "Additional_Vehicles,C,3", Derivative "Additional_Vehicles,D,3" and Model Year "Additional_Vehicles,E,3"
    Then User clicks on Save & New button to add one more vehicle record
    And User adds one Vehicle Record with fields like Brand "Additional_Vehicles,A,3", Model "Additional_Vehicles,B,3", Series Name "Additional_Vehicles,C,3", Derivative "Additional_Vehicles,D,3" and Model Year "Additional_Vehicles,E,3"
    Then User is able to view total pricing details of main vehicle and additional vehicle on opportunity page
    #Then Logout from an SVO main Portal

  @SVO_Pricing_Logic_12 @SSEC-1034 @Edit_01 @PricingLogicBatch
  Scenario: User is not able to update Retail Price of the main vehicle from an Opportunity page
    #Given Access to SVO bespoke Portal with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Given laterally Access SVO Portal by user name "LoginUsers,B,3" and Password "LoginUsers,C,3"
    When User Navigate to Opportunity tab
    And User creates new SVO Bespoke Opportunity with mandatory fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User create and accept a Design Brief Quote with all fields like Retailer Contact "Opportunity,J,2"
    #Then User adds pricing details on Opportunity page
    #Then User navigates to Additional Vehicle Link from Quick Links
    #And User adds one Vehicle Record with fields like Brand "Additional_Vehicles,A,3", Model "Additional_Vehicles,B,3", Series Name "Additional_Vehicles,C,3", Derivative "Additional_Vehicles,D,3" and Model Year "Additional_Vehicles,E,3"
    #Then User navigate back to Opportunity page
    Then User is able to view total pricing details of main vehicle on the opportunity page
    And User edit main vehicle pricing details on opportunity page
    Then User verifies total pricing details of both main and additional vehicle on opportunity page
    Then Verify that pricing details are not updated on quotes page
    #Then Logout from an SVO main Portal

  @SVO_Pricing_Logic_13 @SSEC-1035 @Edit_02 @PricingLogicBatch
  Scenario: User is not able to update Pricing details of an Additional Vehicle added for an Opportunity
    #Given Access to SVO bespoke operation Portal with Username "LoginUsers,B,8" and Password "LoginUsers,C,8"
    Given laterally Access SVO Portal by user name "LoginUsers,B,8" and Password "LoginUsers,C,8"
    When User navigates to an Opportunity tab
    And User create a SVO Bespoke Opportunity with check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User create and accept a Design Brief Quote with all fields like Retailer Contact "Opportunity,J,2"
    Then User navigates to Additional Vehicle Link from Quick Links
    And User adds one Vehicle Record with fields like Brand "Additional_Vehicles,A,3", Model "Additional_Vehicles,B,3", Series Name "Additional_Vehicles,C,3", Derivative "Additional_Vehicles,D,3" and Model Year "Additional_Vehicles,E,3"
    Then User is able to view total pricing details of both main vehicle and additional vehicle on opportunity page
    Then Verify that user is not able to Edit pricing details of Additional vehicle on opportunity page
    #Then Logout from an SVO main Portal

  @SVO_Pricing_Logic_14 @SSEC-1036 @SalesAgreement @PricingLogicBatch
  Scenario: User is able to view the total pricing details of main and additional vehicles which is created by other user on opportunity
    Given Access to SVO Portal by super uat user with Username "LoginUsers,B,2" and Password "LoginUsers,C,2"
    #Given laterally Access SVO Portal by user name "LoginUsers,B,2" and Password "LoginUsers,C,2"
    When User navigates to an Opportunity tab
    And User creates Bespoke Opportunity with mandatory fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2" 
    Then User adds pricing details on Opportunity page
    Then User navigates to Additional Vehicle Link from Quick Links
    And User adds one Vehicle Record with all details Brand "Additional_Vehicles,A,3", Model "Additional_Vehicles,B,3", Series Name "Additional_Vehicles,C,3", Derivative "Additional_Vehicles,D,3" and Model Year "Additional_Vehicles,E,3"
    Then User verifies total pricing details of both main and additional vehicles on opportunity page
    Given Access to SVO Portal by bespoke username with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    And User views total pricing details of same opportunity
    Then Verify that user is not able to Edit pricing details of Additional vehicle on opportunity page
    #Then Logout from an SVO main Portal

  @SVO_Pricing_Logic_15 @SSEC-993 @SalesAgreement @Edit_03 @PricingLogicBatch1
  Scenario: User is not able to edit the pricing details of main vehicle on opportunity page which is created by other user
    Given Access to SVO Portal by super uat user with Username "LoginUsers,B,2" and Password "LoginUsers,C,2"
    #Given laterally Access SVO Portal by user name "LoginUsers,B,2" and Password "LoginUsers,C,2"
    When User navigates to an Opportunity tab
    And User creates Bespoke Opportunity with mandatory fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2" 
    Then User navigates to Additional Vehicle Link from Quick Links
    And User adds one Vehicle Record with all details Brand "Additional_Vehicles,A,3", Model "Additional_Vehicles,B,3", Series Name "Additional_Vehicles,C,3", Derivative "Additional_Vehicles,D,3" and Model Year "Additional_Vehicles,E,3"
    Then User navigate back to an Opportunity page
    Then User create and accept a Design Brief Quote with all fields like Retailer Contact "Opportunity,J,2"
    #Then User is able to view total pricing details of main vehicle on the opportunity page
    Then User verifies total pricing details of both main and additional vehicle on opportunity page
    Given Access to SVO Portal by bespoke username with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    And User views total pricing details of same opportunity
    And User edit main vehicle pricing details on opportunity page
    Then Verify that pricing details are not updated on quotes page
    #Then User Logout from the SVO Portal

  @SVO_Pricing_Logic_16 @SSEC-994 @PricingLogicBatch1
  Scenario: User is not able to edit the pricing details of additional vehicle which is created by other user on additional vehicle page
    #Given Access to SVO Portal by super uat user with Username "LoginUsers,B,2" and Password "LoginUsers,C,2"
    Given laterally Access SVO Portal by user name "LoginUsers,B,2" and Password "LoginUsers,C,2"
    #Given Access to SVO bespoke operation Portal with Username "LoginUsers,B,8" and Password "LoginUsers,C,8"
    When User navigates to an Opportunity tab
    And User creates Bespoke Opportunity with mandatory fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2" 
    Then User navigates to Additional Vehicle Link from Quick Links
    And User adds one Vehicle Record with all details Brand "Additional_Vehicles,A,3", Model "Additional_Vehicles,B,3", Series Name "Additional_Vehicles,C,3", Derivative "Additional_Vehicles,D,3" and Model Year "Additional_Vehicles,E,3"
    Then User navigate back to an Opportunity page
    Then User create and accept a Design Brief Quote with all fields like Retailer Contact "Opportunity,J,2"
    Then User verifies total pricing details of both main and additional vehicle on opportunity page
    Given Access to SVO Portal by bespoke username with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    And User views total pricing details of same opportunity
    Then Verify that user is not able to Edit pricing details of Additional vehicle on opportunity page
    #Then User Logout from the SVO Portal

  @SVO_Pricing_Logic_17 @SSEC-997 @PricingLogicBatch1
  Scenario: User is able to view only Retail Price of the main vehicle as total price on Opportunity page
    #Given Access to SVO Portal by bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Given laterally Access SVO Portal by user name "LoginUsers,B,3" and Password "LoginUsers,C,3"
    When User navigates to an Opportunity tab
    And User creates Bespoke Opportunity with mandatory fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2" 
    Then User adds pricing details on Opportunity page
    And User views total pricing details of same opportunity
    #Then User Logout from the SVO Portal

  @SVO_Pricing_Logic_18 @SSEC-998 @PricingLogicBatch1
  Scenario: User is able to view 'Total Retail Price' field on orders page layout of an Opportunity
    #Given Access to SVO Portal by bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Given laterally Access SVO Portal by user name "LoginUsers,B,3" and Password "LoginUsers,C,3"
    When User navigates to an Opportunity tab
    And User creates Bespoke Opportunity with mandatory fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2" 
    Then User navigates to Additional Vehicle Link from Quick Links
    And User adds one Vehicle Record with all details Brand "Additional_Vehicles,A,3", Model "Additional_Vehicles,B,3", Series Name "Additional_Vehicles,C,3", Derivative "Additional_Vehicles,D,3" and Model Year "Additional_Vehicles,E,3" 
   Then User navigate back to an Opportunity page
    And User verifies total retail price on order page after accepting the Quote for Retailer Contact "Opportunity,J,2"
    #Then User Logout from the SVO Portal

  @SVO_Pricing_Logic_19 @SSEC-1029 @PricingLogicBatch1
  Scenario: User is able to view sum of Retail Prices for multiple vehicles on sales agreement proposal document of an opportunity
    #Given Access to SVO admin Portal with Username "LoginUsers,B,6" and Password "LoginUsers,C,6"
    Given laterally Access SVO Portal by user name "LoginUsers,B,6" and Password "LoginUsers,C,6"
    When User Navigate to Opportunity tab
    And User create a SVO Bespoke Opportunity with check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User navigates to Additional Vehicle Link from Quick Links
    And User adds one Vehicle Record with all details Brand "Additional_Vehicles,A,3", Model "Additional_Vehicles,B,3", Series Name "Additional_Vehicles,C,3", Derivative "Additional_Vehicles,D,3" and Model Year "Additional_Vehicles,E,3"
    And User adds one Vehicle Record with fields like Brand "Additional_Vehicles,A,3", Model "Additional_Vehicles,B,3", Series Name "Additional_Vehicles,C,3", Derivative "Additional_Vehicles,D,3" and Model Year "Additional_Vehicles,E,3"
    Then User navigate back to Opportunity page
    Then User create a Design Brief Quote with all fields like Retailer Contact "Opportunity,J,2"
    Then User submit Design Brief Request and return the render pack
    Then User navigate back to Opportunity page
    And User moves the design brief quote stage to proposal sent stage
    Then User send sales agreement document of design brief quote
    And User verifies the pricing details of main vehicle and Additional vehicles on sales agreement document
    #Then Logout from SVO Portal

  @SVO_Pricing_Logic_20 @SSEC-1037 @PricingLogicBatch
  Scenario: User is able to view 'Total Retail Price' value on Orders page is same as Retail price of main vehicle on Opportunity page, if No additional vehicles present
    #Given Access to SVO Portal by bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Given laterally Access SVO Portal by user name "LoginUsers,B,3" and Password "LoginUsers,C,3"
    When User Navigate to Opportunity tab
    And User creates new SVO Bespoke Opportunity with mandatory fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User adds pricing details on Opportunity page
    And User moves an opportunity to Design Brief and Quote stage
    Then User moves an opportunity to Contract Signed stage from Design Brief and Quote
    And User verifies total retail price on order page after accepting the Quote for Retailer Contact "Opportunity,J,2"
   #Then Logout from an SVO main Portal

  @SVO_Pricing_Logic_21 @SSEC-1038 @PricingLogicBatch2
  Scenario: User is able to view 'Total Retail Price' and 'Total Additional Vehicle Price' field on Quotes page of an Opportunity
    Given Access to SVO Portal by bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    #Given laterally Access SVO Portal by user name "LoginUsers,B,3" and Password "LoginUsers,C,3"
    When User navigates to an Opportunity tab
    And User creates new SVO Bespoke Opportunity with mandatory fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User adds pricing details on Opportunity page
    Then User navigates to Additional Vehicle Link from Quick Links
    And User adds one Vehicle Record with all details Brand "Additional_Vehicles,A,3", Model "Additional_Vehicles,B,3", Series Name "Additional_Vehicles,C,3", Derivative "Additional_Vehicles,D,3" and Model Year "Additional_Vehicles,E,3"
    And User verifies total retail price on Quotes page after accepting the Quote for Retailer Contact "Opportunity,J,2"
    #Then Logout from an SVO main Portal

  @SVO_Pricing_Logic_22 @SSEC-999 @PricingLogicBatch
  Scenario: User is not able to view 'Total Retail price' and 'Total Additional vehicle price' fields on Quotes page of an Opportunity
    #Given Access to SVO Portal by private office user with Username "LoginUsers,B,4" and Password "LoginUsers,C,4"
    Given laterally Access SVO Portal by user name "LoginUsers,B,4" and Password "LoginUsers,C,4"
    When User navigates to an Opportunity tab
    And User create a SVO Private Office Opportunity with Restricted fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,3" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,3" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User is able to view and clear the Restricted Party Screening under details section of an private office opportunity with Restricted Party Screening stage "RestrictedPartyScreening,A,3"
    Then User moves an opportunity to Specification & quote stage
    Then User create private office Quote with all fields like Retailer Contact "Opportunity,J,2"
    And User is not able to view Total Retail price and Total Additional vehicle price fields on Quotes page of an Opportunity
    #Then Logout from an SVO main Portal

  @SVO_Pricing_Logic_23 @SSEC-1000 @SalesAgreement_02 @PricingLogicBatch2
  Scenario: User is able to view 'Total Retail price' and 'Total Additional vehicle price' fields on sales agreement after accepting the quote
    #Given Access to SVO bespoke Portal with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Given laterally Access SVO Portal by user name "LoginUsers,B,3" and Password "LoginUsers,C,3"
    When User navigates to an Opportunity tab
    And User create a SVO Bespoke Opportunity with check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User navigates to Additional Vehicle Link from Quick Links
    And User adds one Vehicle Record with all details Brand "Additional_Vehicles,A,3", Model "Additional_Vehicles,B,3", Series Name "Additional_Vehicles,C,3", Derivative "Additional_Vehicles,D,3" and Model Year "Additional_Vehicles,E,3"
    Then User navigate back to Opportunity page
    Then User create a Design Brief Quote with all fields like Retailer Contact "Opportunity,J,2"
    Then User submit Design Brief Request and return the render pack
    Then User navigate back to Opportunity page
    And User moves the design brief quote stage to proposal sent stage
    Then User send sales agreement document of design brief quote
    And User verifies the pricing details of main vehicle and Additional vehicles on sales agreement document
    #Then User Logout from the SVO Portal

  @SVO_Pricing_Logic_24 @SSEC-1006 @SalesAgreement_03 @PricingLogicBatch2
  Scenario: User is not able to view 'Total Retail price' and 'Total Additional vehicle price' fields on sales agreement at Quote's design brief submitted stage
    #Given Access to SVO bespoke Portal with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Given laterally Access SVO Portal by user name "LoginUsers,B,3" and Password "LoginUsers,C,3"
    When User navigates to an Opportunity tab
    And User create a SVO Bespoke Opportunity with check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User navigates to Additional Vehicle Link from Quick Links
    And User adds one Vehicle Record with all details Brand "Additional_Vehicles,A,3", Model "Additional_Vehicles,B,3", Series Name "Additional_Vehicles,C,3", Derivative "Additional_Vehicles,D,3" and Model Year "Additional_Vehicles,E,3"
    Then User navigate back to Opportunity page
    Then User create a Design Brief Quote with all fields like Retailer Contact "Opportunity,J,2"
    Then User submit Design Brief Request and return the render pack
    Then User navigate back to Opportunity page
    And User is not able to view sales agreement document under agreement section without sending sales agreement
    #Then User Logout from the SVO Portal

  @SVO_Pricing_Logic_25 @SSEC-1007 @PricingLogicBatch2
  Scenario: User is not able to view 'Total Retail price' and 'Total Additional vehicle price' fields on Orders page of an Opportunity
    #Given Access to SVO Portal by private office user with Username "LoginUsers,B,4" and Password "LoginUsers,C,4"
    Given laterally Access SVO Portal by user name "LoginUsers,B,4" and Password "LoginUsers,C,4"
    When User navigates to an Opportunity tab
    And User create a SVO Private Office Opportunity with Restricted fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,3" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,3" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User is able to view and clear the Restricted Party Screening under details section of an private office opportunity with Restricted Party Screening stage "RestrictedPartyScreening,A,3"
    Then User moves an opportunity to Specification & quote stage
    Then User create private office Quote with all fields like Retailer Contact "Opportunity,J,2"
    And User is not able to view Total Retail price and Total Additional vehicle price fields on Orders page of an Opportunity
    #Then Logout from an SVO main Portal

  @SVO_Pricing_Logic_26 @SSEC-1031 @PricingLogicBatch2
  Scenario: User is able to add additional vehicle to an opportunity after accepting the quote
    #Given Access to SVO Portal by bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Given laterally Access SVO Portal by user name "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User create a SVO Private Office Opportunity with Restricted fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,3" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,3" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User moves opportunity to Contract Signed stage
    And User moves an Oppotunities to Order Placed stage after accepting the Quote for Retailer Contact "Opportunity,J,2"
    Then User navigates to Additional Vehicle Link from Quick Links
    And User adds one Vehicle Record with fields like Brand "Additional_Vehicles,A,3", Model "Additional_Vehicles,B,3", Series Name "Additional_Vehicles,C,3", Derivative "Additional_Vehicles,D,3" and Model Year "Additional_Vehicles,E,3"
    #Then Logout from an SVO main Portal

  @SVO_Pricing_Logic_27 @SSEC-1024 @SalesAgreement_04 @PricingLogicBatch
  Scenario: User is able to add additional vehicle to an opportunity after sending sales agreement proposal document
    #Given Access to SVO bespoke Portal with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Given laterally Access SVO Portal by user name "LoginUsers,B,3" and Password "LoginUsers,C,3"
    When User navigates to an Opportunity tab
    And User create a SVO Bespoke Opportunity with check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User create a Design Brief Quote with all fields like Retailer Contact "Opportunity,J,2"
    Then User submit Design Brief Request and return the render pack
    Then User navigate back to Opportunity page
    And User moves the design brief quote stage to proposal sent stage
    Then User send sales agreement document of design brief quote
    And User verifies the pricing details of main vehicle and Additional vehicles on sales agreement document
    Then User navigate back to Opportunity page
    Then User navigates to Additional Vehicle Link from Quick Links
    And User adds one Vehicle Record with all details Brand "Additional_Vehicles,A,3", Model "Additional_Vehicles,B,3", Series Name "Additional_Vehicles,C,3", Derivative "Additional_Vehicles,D,3" and Model Year "Additional_Vehicles,E,3"
    Then User navigate back to Opportunity page
    And User navigates to quotes page
    Then User send sales agreement document of quote
    And User verifies the pricing details of main vehicle and Additional vehicles on sales agreement document
    #Then User Logout from the SVO Portal

  @SVO_Pricing_Logic_28 @SSEC-1025 @PricingLogicBatch
  Scenario: User is able to add additional vehicle to an opportunity after order is placed for an opportunity
    #Given Access to SVO bespoke Portal with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Given laterally Access SVO Portal by user name "LoginUsers,B,3" and Password "LoginUsers,C,3"
    When User navigates to an Opportunity tab
    And User create a SVO Bespoke Opportunity with check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User moves opportunity to Contract Signed stage
    And User moves an Oppotunities to Order Placed stage after accepting the Quote for Retailer Contact "Opportunity,J,2"
    Then User verify the total pricing details of additional vehicle on orders page before adding additional vehicle records
    Then User navigate back to Opportunity page
    Then User navigates to Additional Vehicle Link from Quick Links
    And User adds one Vehicle Record with all details Brand "Additional_Vehicles,A,3", Model "Additional_Vehicles,B,3", Series Name "Additional_Vehicles,C,3", Derivative "Additional_Vehicles,D,3" and Model Year "Additional_Vehicles,E,3"
    Then User navigate back to Opportunity page
    Then User verify the total pricing details of additional vehicle on orders page after adding additional vehicle records
    #Then Logout from an SVO main Portal

  @SVO_Pricing_Logic_29 @SSEC-1026 @PricingLogicBatch
  Scenario: User is able to view single additional vehicle pricing details on Additional vehicles page of an Opportunity
    #Given Access to SVO Portal by bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Given laterally Access SVO Portal by user name "LoginUsers,B,3" and Password "LoginUsers,C,3"
    When User navigates to an Opportunity tab
    And User create a SVO Bespoke Opportunity with check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User navigates to Additional Vehicle Link from Quick Links
    And User adds one Vehicle Record with all details Brand "Additional_Vehicles,A,3", Model "Additional_Vehicles,B,3", Series Name "Additional_Vehicles,C,3", Derivative "Additional_Vehicles,D,3" and Model Year "Additional_Vehicles,E,3"
    Then User verifies total additional vehicle price on opportunity page
    Then User navigates to Additional Vehicle Link from Quick Links
    And User delete the additional vehicle record of an opportunity
    Then User verifies total additional vehicle price on opportunity page
    #Then Logout from an SVO main Portal

  @SVO_Pricing_Logic_30 @SSEC-1125 @PricingLogicBatch
  Scenario: User is not able to add new additional vehicle to the opportunity after accepting the quote
    #Given Access to SVO Portal by bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Given laterally Access SVO Portal by user name "LoginUsers,B,3" and Password "LoginUsers,C,3"
    When User navigates to an Opportunity tab
    And User create a SVO Bespoke Opportunity with check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User create and accept a Design Brief Quote with all fields like Retailer Contact "Opportunity,J,2"
    Then User navigates to Additional Vehicle Link from Quick Links
    And Verify that user is not able to add new additional vehicle to the opportunity after accepting the quote with Brand "Additional_Vehicles,A,3", Model "Additional_Vehicles,B,3", Series Name "Additional_Vehicles,C,3", Derivative "Additional_Vehicles,D,3" and Model Year "Additional_Vehicles,E,3"
    Then User Logout from the SVO Portal
