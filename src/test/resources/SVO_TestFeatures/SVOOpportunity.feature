Feature: SVO Opportunity Validation

#User creates SVO Bespoke Opportunity
@SVO @SVO_74 @SSEC-345 
Scenario: User creates a new Opportunity with SVO Bespoke Record Type
    Given Access to SF-SVO Portal with username "LoginUsers,B,6" and password "LoginUsers,C,6"
    When User Navigate to Opportunity tab
    And User Creates New Opportunity with "Opportunity,A,8" Record Type
    Then User enters mandatory fields for an Opportunity like Stage "Opportunity,R,4" Product Offering "Opportunity,B,8" Region "Opportunity,E,8" Sales Area "Opportunity,F,8" Market "Opportunity,G,8" Preferred Contact "Opportunity,H,8" Account Name "Opportunity,D,5" Source "Opportunity,C,8" Brand "Opportunity,I,8" Model "Opportunity,J,8" 
    And User verifies Reference Number is generated
    And User navigates to Commissioning Request 
    And User Creates a new Commissioning Request with "CommissioningRequest,A,2" Record type
    And User enters all mandatory fields and save commissioning Request
    Then User verifies that linked opportunity is displayed under Bespoke Opportunity Details
    Then Verify that User can create New Reservation from Calendar with Primary Contact "CommissioningRequest,B,2"
    Then Logout from SF_SVO Portal
    
#User creates Classic Bespoke Opportunity
@SVO @SVO_75 @SSEC-346
Scenario: User creates a new Opportunity with Classic Bespoke Record Type
    Given Access to SF-SVO Portal with username "LoginUsers,B,6" and password "LoginUsers,C,6"
    When User Navigate to Opportunity tab
    And User Creates New Opportunity with "Opportunity,A,3" Record Type
    Then User enters mandatory fields for an Opportunity like Stage "Opportunity,R,4" Product Offering "Opportunity,B,3" Region "Opportunity,E,3" Sales Area "Opportunity,F,3" Market "Opportunity,G,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,D,5" Source "Opportunity,C,3" Brand "Opportunity,I,3" Model "Opportunity,J,3" 
    And User verifies Reference Number is generated
    Then Logout from SF_SVO Portal
    
#User creates Classic Continuation Opportunity
@SVO @SVO_76 @SSEC-347
Scenario: User creates a new Opportunity with Classic Continuation Record Type
    Given Access to SF-SVO Portal with username "LoginUsers,B,6" and password "LoginUsers,C,6"
    When User Navigate to Opportunity tab
    And User Creates New Opportunity with "Opportunity,A,7" Record Type
    Then User enters mandatory fields for an Opportunity like Stage "Opportunity,R,9" Product Offering "Opportunity,B,3" Region "Opportunity,E,3" Sales Area "Opportunity,F,3" Market "Opportunity,G,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,D,5" Source "Opportunity,C,3" Brand "Opportunity,I,3" Model "Opportunity,J,3" 
    And User verifies Reference Number is generated
    Then Logout from SF_SVO Portal
    
#User creates Classic Limited Edition Opportunity
@SVO @SVO_77 @SSEC-348
Scenario: User creates a new Opportunity with Classic Limited Edition Record Type
    Given Access to SF-SVO Portal with username "LoginUsers,B,6" and password "LoginUsers,C,6"
    When User Navigate to Opportunity tab
    And User Creates New Opportunity with "Opportunity,A,9" Record Type
    Then User enters mandatory fields for an Opportunity like Stage "Opportunity,R,9" Product Offering "Opportunity,B,3" Region "Opportunity,E,3" Sales Area "Opportunity,F,3" Market "Opportunity,G,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,D,5" Source "Opportunity,C,3" Brand "Opportunity,I,3" Model "Opportunity,J,3" 
    And User verifies Reference Number is generated
    Then Logout from SF_SVO Portal

#User creates Classic Reborn Opportunity
@SVO @SVO_78 @SSEC-349
Scenario: User creates a new Opportunity with Classic Reborn Record Type
    Given Access to SF-SVO Portal with username "LoginUsers,B,6" and password "LoginUsers,C,6"
    When User Navigate to Opportunity tab
    And User Creates New Opportunity with "Opportunity,A,4" Record Type
    Then User enters mandatory fields for an Opportunity like Stage "Opportunity,R,4" Product Offering "Opportunity,B,3" Region "Opportunity,E,3" Sales Area "Opportunity,F,3" Market "Opportunity,G,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,D,5" Source "Opportunity,C,3" Brand "Opportunity,I,3" Model "Opportunity,J,3" 
    And User verifies Reference Number is generated
    Then Logout from SF_SVO Portal
    
#User Creates Classic Service Opportunity
@SVO @SVO_79 @SSEC-350
Scenario: User creates a new Opportunity with Classic Service Record Type
    Given Access to SF-SVO Portal with username "LoginUsers,B,6" and password "LoginUsers,C,6"
    When User Navigate to Opportunity tab
    And User Creates New Opportunity with "Opportunity,A,2" Record Type
    Then User enters mandatory fields for an Opportunity like Stage "Opportunity,R,4" Product Offering "Opportunity,B,3" Region "Opportunity,E,3" Sales Area "Opportunity,F,3" Market "Opportunity,G,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,D,5" Source "Opportunity,C,3" Brand "Opportunity,I,3" Model "Opportunity,J,3" 
    And User verifies Reference Number is generated
    Then Logout from SF_SVO Portal
    
#User creates Classic Works Legend Opportunity
@SVO @SVO_80 @SSEC-351
Scenario: User creates a new Opportunity with Classic Works Legend Type
    Given Access to SF-SVO Portal with username "LoginUsers,B,6" and password "LoginUsers,C,6"
    When User Navigate to Opportunity tab
    And User Creates New Opportunity with "Opportunity,A,6" Record Type
    Then User enters mandatory fields for an Opportunity like Stage "Opportunity,R,4" Product Offering "Opportunity,B,3" Region "Opportunity,E,3" Sales Area "Opportunity,F,3" Market "Opportunity,G,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,D,5" Source "Opportunity,C,3" Brand "Opportunity,I,3" Model "Opportunity,J,3" 
    And User verifies Reference Number is generated
    Then Logout from SF_SVO Portal
    
#User creates SVO Private Office Opportunity
@SVO @SVO_81 @SSEC-352
Scenario: User creates a new Opportunity with SVO Private Office Record Type
    Given Access to SF-SVO Portal with username "LoginUsers,B,6" and password "LoginUsers,C,6"
    When User Navigate to Opportunity tab
    And User Creates New Opportunity with "Opportunity,A,10" Record Type
    Then User enters mandatory fields for an Opportunity like Stage "Opportunity,R,4" Product Offering "Opportunity,B,3" Region "Opportunity,E,3" Sales Area "Opportunity,F,3" Market "Opportunity,G,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,D,5" Source "Opportunity,C,3" Brand "Opportunity,I,3" Model "Opportunity,J,3" 
    And User verifies Reference Number is generated
    And User navigates to Commissioning Request 
    And User Creates a new Commissioning Request with "CommissioningRequest,A,2" Record type
    And User enters all mandatory fields and save commissioning Request
    Then User verifies that linked opportunity is displayed under Bespoke Opportunity Details
    Then Logout from SF_SVO Portal
    
#User tries to re-open the closed opportunity[Move stage from closed to Qualified]
@SVO @SVO_82 @SSEC-353
Scenario: User tries to re-open the closed opportunity[Move stage from closed to Qualified]
 		Given Access SF_SVO Portal
 		Then User Navigate to Opportunity tab
 		And User searched for Closed opportunity
 		Then User tries to reopen closed Opportunity to "Qualified" Stage
 		Then Logout from SF_SVO Portal
 		
#User tries to move the opportunity stage from design brief & quote to contract signed without adding quote for SVO Private Office
@SVO @SVO_83 @SSEC-354
Scenario: User tries to move the opportunity stage from design brief & quote to contract signed without adding quote for SVO Private Office
		Given Access SF_SVO Portal
 		Then User Navigate to Opportunity tab
		And User Creates New Opportunity with "Opportunity,A,8" Record Type
    Then User enters mandatory fields for an Opportunity like Stage "Opportunity,R,4" Product Offering "Opportunity,B,3" Region "Opportunity,E,3" Sales Area "Opportunity,F,3" Market "Opportunity,G,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,D,5" Source "Opportunity,C,3" Brand "Opportunity,I,3" Model "Opportunity,J,3" 
    And Verify that the opportunity stage moves to Design Brief & Quote
    And User click on the mark stage as complete button to complete the process
    Then Verify that user can move to "Contract Signed" stage
    Then Logout from SF_SVO Portal
    
 #User tries to move the opportunity stage from  contract signed to order placed without an order creation
  @SVO @SVO_84 @SSEC-355
  Scenario: User tries to move the opportunity stage from  contract signed to order placed without an order creation
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    Then User searches for "Opportunity,AC,5" opportunities from select list view
    And Open an opportunity with Record Type "Opportunity,R,11"
    And User click on the mark stage as complete button to complete the process
    Then Verify the error message to close the opportunity
    Then Logout from SF_SVO Portal
    
  #User can get vehicle information from Vehicle Search Option for any non-closed Opportunity
  @SVO @SVO_085 @SSEC-356
  Scenario: User can get vehicle information from Vehicle Search Option for any non-closed Opportunity
    Given Access the SVO Portal
    When User Navigate to Opportunity
    And User searches for Contract signed opportunity and selects it
    Then User clicks on search Vehicle
    Then select brand as test brand and click search
    When click on new and enter Record name "New Enquiry,S,5" Full VIN "New Enquiry,K,5" Brand "New Enquiry,I,5" and Model "New Enquiry,J,5"
    And save the vehicle information
    Then Logout from SF_SVO Portal
    
#User tries to move the stage of an opportunity backward[Move from Contract signed to design & brief]
  @SVO @SVO_086 @SSEC-357
  Scenario: User tries to move the stage of an opportunity backward[Move from Contract signed to design & brief]
    Given Access the SVO Portal
    When User Navigate to Opportunity
    And User searches for Contract signed opportunity and selects it
    Then User selects Design Brief and Quote and clicks on mark as current stage
    And verify the displayed flow error message
    Then Logout from SF_SVO Portal

  #User clone the closed opportunity
  @SVO @SVO_087 @SSEC-358
  Scenario: User clone the closed opportunity
    Given Access the SVO Portal
    When User Navigate to Opportunity
    And User searches for Closed won opportunity "Opportunity,L,5" and selects it
    Then user clicks on dropdown and clicks on the clone button
    And user saves the clone opportunity
    When User Navigate to Opportunity
    And User searches for Closed won opportunity "Opportunity,L,5" and selects it
    Then Logout from SF_SVO Portal

  #User Edits the stage of an opportunity from Waitlist to Closed lost
  @SVO @SVO_088 @SSEC-359
  Scenario: User Edits the stage of an opportunity from Waitlist to Closed lost
    Given Access the SVO Portal
    When User Navigate to Opportunity
    And User searches for an opportunity "Opportunity,L,8" and selects it
    Then user selects the closed tab and selects stage as "Opportunity,R,8" and Reason "Opportunity,T,8"
    Then Logout from SF_SVO Portal

  #User tries to change the Record type of an closed opportunity
  @SVO @SVO_089 @SSEC-360
  Scenario: User tries to change the Record type of an closed opportunity
    Given Access the SVO Portal
    When User Navigate to Opportunity
    And User searches for Closed won opportunity "Opportunity,L,5" and selects it
    Then user changes the record type of the opportunity
    And Verify the displayed record error message
    Then Logout from SF_SVO Portal

  #User selects Unchecked for Restricted Party Screening field for an open opportunity
  @SVO @SVO_090 @SSEC-361
  Scenario: User selects Unchecked for Restricted Party Screening field for an open opportunity
    Given Access the SVO Portal
    When User Navigate to Opportunity
    And User searches for an opportunity "Opportunity,L,7" and selects it
    Then user clicks on contracting and clicks on Mark as current stage
    And verify the displayed flow error message
    Then Logout from SF_SVO Portal

	#User get build capacity information by clicking on blue link of Programme
  #Classic Limited Edition opportunity
  @SVO @SVO_091 @SSEC-362
  Scenario: User get build capacity information by clicking on blue link of Programme
    Given Access SF_SVO Portal
    Then User Navigate to SVO
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,5"
    And Enter details of Classic Limited Edition Opportunity like Opportunity name "Opportunity,L,2" Stage "Opportunity,R,9" Region "Opportunity,E,4" Account Name "Opportunity,D,9" Preferred Contact "Opportunity,H,9" Programme "Opportunity,U,6" and CloseDate "Opportunity,AI,2"
    Then Save the created Opportunity
    And Verify the Build Capacity of the Programme
    Then Logout from SF_SVO Portal

  #User does not enter value for Programme field, while creating Classic Limited Edition and Classic Continuation Opportunity
  #Classic Limited Edition and Classic Continuation opportunities
  @SVO @SVO_092 @SSEC-363
  Scenario: User creates Classic Limited Edition and Classic Continuation Opportunity witout entering Programme field
    Given Access SF_SVO Portal
    Then User Navigate to SVO
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,5"
    And User enters fields like OpportunityName "Opportunity,L,9" CloseDate "Opportunity,AG,2" Stage "Opportunity,R,4" Region "Opportunity,E,4" AccountName "Opportunity,D,9" PreferredContact "Opportunity,H,9" Source "Opportunity,C,2"
    Then Verify the error message to fill the Programme
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,7"
    And User enters fields like OpportunityName "Opportunity,L,10" CloseDate "Opportunity,AG,2" Stage "Opportunity,R,4" Region "Opportunity,E,4" AccountName "Opportunity,D,9" PreferredContact "Opportunity,H,9" Source "Opportunity,C,2"
    Then Verify the error message to fill the Programme
    Then Logout from SF_SVO Portal
    
    #User creates a new Programme while creaing Classic Limited Edition opportunity
    @SVO @SVO_093 @SSEC-364
		Scenario: User creates a new Programme while creating Classic Limited Edition opportunity
		Given Access to SVO Portal
		When User Navigate to Opportunity
		Then User Creates a New opportunity with Record Type as "Opportunity,A,5" 
		Then user navigated to Programme Field and clicks on new
		And enter the required information like name "Opportunity,U,5" product offering "Opportunity,B,5" build capacity "Opportunity,V,5" brand "Opportunity,I,5" and model "Opportunity,J,5"
		And save the programme 
		Then cancel the opportunity creation 
		Then Logout from the SF SVO Portal
		
	#User generates document through S-DOCS for any open opportunity
  @SVO @SVO_094 @SSEC-365
  Scenario: User generates document through S-DOCS for any open opportunity
    Given Access the SVO Portal
    When User Navigate to Opportunity
    And User searches for an opportunity "Opportunity,L,8" and selects it
    Then User clicks on S-Docs button
    When user selects a file and clicks on next step
    Then user clicks on the E-sign button to send an email of the document
    Then Logout from SF_SVO Portal

  #User is able to upload and view any document into Files Section for any open opportunity
  @SVO @SVO_095 @SSEC-366
  Scenario: User is able to upload and view any document into Files Section for any open opportunity
    Given Access the SVO Portal
    When User Navigate to Opportunity
    And User searches for an opportunity "Opportunity,L,8" and selects it
    Then user navigates to file section and clicks on add files
    And select a file and click on upload Files
    Then navigate to the file and delete it
    Then Logout from SF_SVO Portal
    
  #User does not enter pricing details for any open SVO Bespoke opportunity which should not be in Order Placed or Closed stage
  @SVO @SVO_096 @SSEC-367
  Scenario: User does not enter pricing details for any open SVO Bespoke opportunity which should not be in Order Placed or Closed stage
    Given Access SF_SVO Portal
    Then User Navigate to SVO
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,8"
    And fill all mandatory fields of SVOBespoke opportunity like OpportunityName "Opportunity,A,8" Closedate "Opportunity,AI,2" Stage "Opportunity,R,4" ProductOffering "Opportunity,B,2" Region "Opportunity,E,2" AccountName "Opportunity,D,9" PreferredContact "Opportunity,H,9" Brand "Opportunity,I,2" Model "Opportunity,J,2"
    Then User remove the Pricing details "Opportunity,X,2"
    Then User Move the stage to Order placed
    Then Verify the error message to fill the pricing details
    Then Logout from SF_SVO Portal
		
  #User enters Production Year corresponds to Model for any open opportunity
  @SVO @SVO_097 @SSEC-368
  Scenario: User enters Production Year corresponds to Model for any open opportunity
    Given Access SF_SVO Portal
    Then User Navigate to SVO
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,3"
    And fill all mandatory fields of SVOBespoke opportunity like OpportunityName "Opportunity,A,3" Closedate "Opportunity,AI,2" Stage "Opportunity,R,4" ProductOffering "Opportunity,B,3" Region "Opportunity,E,2" AccountName "Opportunity,D,9" PreferredContact "Opportunity,H,9" Brand "Opportunity,I,2" Model "Opportunity,J,2"
    Then User enters production year of Vehicle "Opportunity,AQ,2"
    Then Logout from SF_SVO Portal
    
  #User tries to move to Quotation stage for any open opportunity [Other than SVO Opportunities]
  @Opportunity @SVO_098 @SSEC-369
  Scenario: User tries to move to Quotation stage for any open opportunity [Other than SVO Opportunities]
    Given Access to SVO Portal
    When User Navigate to Opportunity
    Then User Create a New Opportunity with Record Type "Opportunity,A,9"
    Then User enters mandatory fields for an Opportunity like Stage "Opportunity,R,4" Product Offering "Opportunity,B,3" Region "Opportunity,E,3" Sales Area "Opportunity,F,3" Market "Opportunity,G,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,D,5" Source "Opportunity,C,3" Brand "Opportunity,I,3" Model "Opportunity,J,3" 
    And User click on the mark stage as complete button to complete the process
    And User click on the mark stage as complete button to complete the process
    And User click on the mark stage as complete button to complete the process
    Then Logout from SF_SVO Portal
    
 	#User is not able to see S-DOCS button for an opportunity with specific record type
  @SVO @SVO_099 @SSEC-370
  Scenario: User is not able to see S-DOCS button for an opportunity with specific record type
    Given Access to SVO Portal
    When User Navigate to Opportunity
    And User searches for Classic Service opportunity and selects it
    Then Validate that user should not be able to see S-DOCS button
    Then Logout from the SF SVO Portal

  #User searches document for different category for any closed opportunity
  @SVO @SVO_100 @SSEC-371
  Scenario: User searches document for different category for any closed opportunity
   	Given Access to SVO Portal
		When User Navigate to Opportunity
		And User searches for Closed opportunity and selects it
		Then Validate that user should not be able to see S-DOCS button
		Then Logout from the SF SVO Portal
		
  #User tries to Move the stage of an Opportunity from Estimate to Contracting without clearing Restricted party screening for Classic Bespoke Opportunity
  #Classic Bespoke Opportunity with Estimate Stage
  @SVO @SVO_114 @SSEC-385
  Scenario: User tries to Move the stage of an Opportunity from Estimate to Contracting without clearing Restricted party screening for Classic Bespoke Opportunity
    Given Access SF_SVO Portal
    Then User Navigate to SVO
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,3"
    And fill all mandatory fields of SVOBespoke opportunity like OpportunityName "Opportunity,A,3" Closedate "Opportunity,AI,2" Stage "Opportunity,R,12" ProductOffering "Opportunity,B,3" Region "Opportunity,E,2" AccountName "Opportunity,D,9" PreferredContact "Opportunity,H,9" Brand "Opportunity,I,2" Model "Opportunity,J,2"
    Then User unchecked the Restricted party screening "Opportunity,AR,2"
    And User click on the mark stage as complete button to complete the process
    Then Verify the error message to clear the Restricted party screening
    Then Logout from SF_SVO Portal
    
	#User skips SAP Account details and try to close an opportunity
  #Classic Bespoke Opportunity with Order Placed Stage
  @SVO @SVO_115 @SSEC-386
  Scenario: User skips SAP Account details and try to close an opportunity
    Given Access SF_SVO Portal
    Then User Navigate to SVO
    Then User Navigate to Opportunity tab
    Then User searched and select "Opportunity,AC,2" opportunities from select list view
    And Open an opportunity with Record Type "Opportunity,R,7"
    Then User clears SAP Account details
    And User click on the mark stage as complete button to complete the process
    Then User selects stage as ClosedWon
    Then Verify the error message to close the opportunity
    Then Logout from SF_SVO Portal

  #User skips the Opportunity currency and VAT Qualifying and tries to Move the stage from Estimate to Contracting
  #Classic Bespoke with Estimate stage
  @SVO @SVO_116 @SSEC-387
  Scenario: User skips the Opportunity currency and VAT Qualifying and tries to Move the stage from Estimate to Contracting
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,3"
    And fill all mandatory fields of SVOBespoke opportunity like OpportunityName "Opportunity,A,3" Closedate "Opportunity,AI,2" Stage "Opportunity,R,12" ProductOffering "Opportunity,B,3" Region "Opportunity,E,2" AccountName "Opportunity,D,9" PreferredContact "Opportunity,H,9" Brand "Opportunity,I,2" Model "Opportunity,J,2"
    Then User clears opportunity currency
    Then Verify the error message to fill Opportunity Currency
    Then User click on cancel button
    Then Logout from SF_SVO Portal
    
  #User can creates a new Campaign and view Campaign details under Campaigns Menu
  #Super User 
  @SVO @SVO_117 
  Scenario: User can creates a new Campaign and view Campaign details under Campaigns Menu
    Given Access SF_SVO Portal
    Then Navigate to Campaign tab "Opportunity,AC,8"
    And Verify that list of Campaigns displayed
    Then User create new campaign with Campaign name "Opportunity,AL,2"
    And Verify that details of Campaign are displayed
    When User add a Parent Campaign "Opportunity,AW,2"
    And User navigate to log a call tab
    Then User log a call for Campaign with Subject "Opportunity,AW,2"
    When User navigate to New Event tab
    Then User create New Event with Subject "Opportunity,AW,2"
    And User navigate to New Task tab
    Then User create new task with Subject "Opportunity,AW,2"
    When User navigate to More tab and click on Email
    Then User send an Email to "Opportunity,AE,3" with subject "Opportunity,AV,2" and body "Opportunity,AC,8"
    Then Logout from SF_SVO Portal
    
#User tries enter two digit number in production year section of an Opportunity
@SVO @SVO_118 @SSEC-389
Scenario: User tries enter two digit number in production year section of an Opportunity
    Given Access SF_SVO Portal
    Then User Navigate to SVO
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,3"
    And fill all mandatory fields of SVOBespoke opportunity like OpportunityName "Opportunity,A,3" Closedate "Opportunity,AI,2" Stage "Opportunity,R,12" ProductOffering "Opportunity,B,3" Region "Opportunity,E,2" AccountName "Opportunity,D,9" PreferredContact "Opportunity,H,9" Brand "Opportunity,I,2" Model "Opportunity,J,2"
    When User tries to enter invalid production year as "Opportunity,N,5"
    Then Verify the error message to fill Opportunity Currency
    And User click on cancel button
    Then Logout from SF_SVO Portal
    
  #User able to Attach a file in compose email section of an Opportunity
  #User Role : Mike King
  #SVO Bespoke Opportunity with stage Design Brief & Quote
  @SVO @SVO_119 @SSEC-390
  Scenario: User able to Attach a file in compose email section of an Opportunity
    Given Access SF_SVO Portal
    Then set up login to the user role "User Role,B,17"
    Then User Navigate to Opportunity tab
    And User click on New button to create an opportunity
    And fill all mandatory fields of SVOBespoke Mike opportunity like OpportunityName "Opportunity,A,8" Closedate "Opportunity,AI,2" Stage "Opportunity,R,6" ProductOffering "Opportunity,B,2" Region "Opportunity,E,2" AccountName "Opportunity,D,9" PreferredContact "Opportunity,H,9" Brand "Opportunity,I,2" Model "Opportunity,J,2"
    Then Click on Compose email
    Then User attach a file in Compose email section
    Then Logout from SF_SVO Portal
    
  #User able to Remove a file in compose email section of an Opportunity
  @SVO @SVO-120 @SSEC-391
  Scenario: User able to Remove a file in compose email section of an Opportunity
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    And Search for Opportunity with stage "Estimate" and open relevant Opportunity
    Then Click on Compose email
    Then User attaches file from different location
    Then Logout from SF_SVO Portal
    
 #User able to retrieve the saved mail from draft of an Opportunity
  @SVO @SVO-121 @SSEC-392
  Scenario: User able to retrieve the saved mail from draft of an Opportunity
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    And Search for Opportunity with stage "Qualified" and open relevant Opportunity
    Then Click on Compose email
    Then edit the required information as Additional to "New Enquiry,U,2" Subject "New Enquiry,X,2" Body "New Enquiry,Y,2"
    And Click on save mail
    Then Click on Compose email
    Then edit the Additional to "New Enquiry,U,2"
    Then Click on drafts and retrive draft with Subject "New Enquiry,X,2"
    And Click on sent mail
    Then Logout from SF_SVO Portal

  #User generates an S-Doc for closed-lost Opportunity
  @SVO @SVO-122 @SSEC-393
  Scenario: User generates an S-Doc for closed-lost Opportunity
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    And Search for Opportunity with stage "Closed Lost" and open relevant Opportunity
    Then Click on S-Docs button
    When user selects a required file and clicks on next step
    Then Logout from SF_SVO Portal

	#User tries to delete Closed Won Opportunity
  @SVO @SVO-123 @SSEC-394
  Scenario: User tries to delete Closed Won Opportunity
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    And Search for Opportunity with stage "Closed Won" and open relevant Opportunity
    Then Click on Delete button
    Then Logout from SF_SVO Portal

	#User selects Order status of an Opportunity other than retailed
  @SVO @SVO-124 @SSEC-395
  Scenario: User selects Order status of an Opportunity other than retailed
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,4"
    And Enter Required details of New Opportunity like Product Offering "Opportunity,B,4" Source "Opportunity,C,4" Account Name "Opportunity,D,4" Region "Opportunity,E,4" Preferred Contact "Opportunity,H,4" Brand "Opportunity,I,4" and Model "Opportunity,J,4"
    Then Save the created Opportunity
    Then Change the opportunity status to Order Placed
    Then navigate to Orders link and open the order
    Then Open the work order
    Then Enter all the required details in work order
    Then Navigate to the order and mark status as Cancelled by customer
    Then Navigate to Opportunity and verify that the status is changed to Closed Lost
    Then Logout from SF_SVO Portal

	#User tries to send an email without entering mandatory fields of an Opportunity
  @SVO @SVO-125 @SSEC-396
  Scenario: User tries to send an email without entering mandatory fields of an Opportunity
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    And Search for Opportunity with stage "Estimate" and open relevant Opportunity
    Then Click on Compose email
    Then Click on Send button and verify the error
    Then Logout from SF_SVO Portal

	#User can edit auto populated field details in Compose email list of an Opportunity
  @SVO @SVO-126 @SSEC-397
  Scenario: User can edit auto populated field details in Compose email list of an Opportunity
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    And Search for Opportunity with stage "Estimate" and open relevant Opportunity
    Then Click on Compose email
    Then Edit auto populated fields like Related to "New Enquiry,T,2" and Importance "New Enquiry,Z,2" in Compose email page
    And Click on save mail
    Then Logout from SF_SVO Portal

	#User tries to edit From section of an email in compose email section
  @SVO @SVO-127 @SSEC-398
  Scenario: User tries to edit From section of an email in compose email section
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    And Search for Opportunity with stage "Estimate" and open relevant Opportunity
    Then Click on Compose email
    Then Verify that the user cannot edit From and Business unit fields in Compose email page
    Then Logout from SF_SVO Portal
    
  #User selects To, CC and BCC fields of mail from contact list search in compose email section of an opportunity
  @SVO @SVO-128 @SSEC-399
  Scenario: User selects To, CC and BCC fields of mail from contact list search in compose email section of an opportunity
    Given Access SF_SVO Portal
    Then User Navigate to SVO
    Then User Navigate to Opportunity tab
    And Open an opportunity which is in Estimate stage
    Then User selects TO BCC and CC section of an email
    Then Logout from SF_SVO Portal

  #User creates an invoice for an order in Opportunity section
  @SVO @SVO-129 @SSEC-400
  Scenario: User creates an invoice for an order in Opportunity section
    Given Access SF_SVO Portal
    Then User Navigate to SVO
    Then User Navigate to Opportunity tab
    Then Open an opportunity which is in OrderPlaced stage
    Then User selects an order which is linked to selected Opportunity
    Then User selects deposit type invoice
    Then User creates new invoice by entering mandatory fields
    Then Logout from SF_SVO Portal

  #User tries to change the Record type of an invoice
  @SVO @SVO-130 @SSEC-401
  Scenario: User tries to change the Record type of an invoice
    Given Access SF_SVO Portal
    Then User Navigate to SVO
    Then User Navigate to Opportunity tab
    Then Open an opportunity which is in OrderPlaced stage
    Then User selects an order which is linked to selected Opportunity
    Then User selects an Invoice linked to Order
    Then User Change the Record type of an Invoice
    Then Logout from SF_SVO Portal
 
 	#User created an S-sign Envelop for Order placed Opportunity
  @SVO @SVO-131 @Error @SSEC-402
  Scenario: User created an S-sign Envelop for Order placed Opportunity
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,5"
    And Enter details of New Opportunity like Opportunity name "Opportunity,L,5" Product Offering "Opportunity,B,5" Account Name "Opportunity,D,5" Region "Opportunity,E,5" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,5" and Model "Opportunity,J,5"
    Then Save the created Opportunity

  #User tries to change the Build Start details of an Work order in Opportunity section
  @SVO @SVO-132 @Admin @SSEC-403
  Scenario: User tries to change the Build Start details of an Work order in Opportunity section
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    And Search for Opportunity with stage "Order Placed" and open relevant Opportunity
    And User navigates to Orders section from Related links of Opportunity
    Then Navigate to Work Order from Order window
    Then Edit Build start dates and Validate start slippage days
    Then Logout from SF_SVO Portal

  #User tries to add ABS-Forecast date of an order before Build start date
  @SVO @SVO-133 @Admin @SSEC-404
  Scenario: User tries to add ABS-Forecast date of an order before Build start date
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    And Search for Opportunity with stage "Order Placed" and open relevant Opportunity
    And User navigates to Orders section from Related links of Opportunity
    Then Navigate to Work Order from Order window
    Then Set ABS-forecast date prior to Build Start date and validate error
    Then Logout from SF_SVO Portal

  #User tries to enter non-sequencial forecast dates for sequential build stages
  @SVO @SVO-134 @SSEC-405 @Admin
  Scenario: User tries to enter non-sequencial forecast dates for sequential build stages
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    And Search for Opportunity with stage "Order Placed" and open relevant Opportunity
    And User navigates to Orders section from Related links of Opportunity
    Then Navigate to Work Order from Order window
    Then Set Strip_Parts Forecast and Paint forecast date in a non sequential manner and validate error
    Then Logout from SF_SVO Portal

  #User tries to Move the status of invoice payment from Planned request to Payment request Paid
  @SVO @SVO-135 @SSEC-406
  Scenario: User tries to Move the status of invoice payment from Planned request to Payment request Paid
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    And Search for Opportunity with stage "Order Placed" and open relevant Opportunity
    Then User selects an order which is linked to selected Opportunity
    Then User selects deposit type invoice
    Then User creates new invoice by entering mandatory fields
    Then User navigates to Invoice which is in Planned Request Status
    And User edits payment date and value and validates the invoice status is moved to Payment request sent
    And User edits paid ammount details and validates the status is changed to Payment Request Paid
    Then Logout from SF_SVO Portal
    
   #User can view and edit details of calendar under Booker25 Menu
   @SVO @SVO-136 @SSEC-407
  Scenario: User can view and edit details of calendar under Booker25 Menu
    Given Access SF_SVO Portal
    Then User Navigate to "Calendars" under "Booker25" menu
    And Select and open any calendar and edit the Reservation title field as "Test title"
    Then Click on dropdown and navigate to change owner
    Then Change the owner and save
    Then Click on dropdown and navigate to change record type
    Then Change the record type to "Schedule Calendar" and save
    Then Logout from SF_SVO Portal

	#User tries to mark Strip & parts /BIW build stage as complete for an work order
  @SVO @SVO-137 @SSEC-408
  Scenario: User tries to mark Strip & parts /BIW build stage as complete for an work order
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,4"
    And Enter Required details of New Opportunity like Product Offering "Opportunity,B,4" Source "Opportunity,C,4" Account Name "Opportunity,D,4" Region "Opportunity,E,4" Preferred Contact "Opportunity,H,4" Brand "Opportunity,I,4" and Model "Opportunity,J,4"
    Then Save the created Opportunity
    Then Change the opportunity status to Order Placed
    Then navigate to Orders link and open the order
    Then Open the work order
    Then Mark Strip & parts /BIW build stage as complete in work order and verify the other details
    Then Logout from SF_SVO Portal

	#User fills the Handover details of vehicle in build status of an Order without selecting vehicle
  @SVO @SVO-138 @SSEC-409
  Scenario: User fills the Handover details of vehicle in build status of an Order without selecting vehicle
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,4"
    And Enter Required details of New Opportunity like Product Offering "Opportunity,B,4" Source "Opportunity,C,4" Account Name "Opportunity,D,4" Region "Opportunity,E,4" Preferred Contact "Opportunity,H,4" Brand "Opportunity,I,4" and Model "Opportunity,J,4"
    Then Save the created Opportunity
    Then Change the opportunity status to Order Placed
    Then navigate to Orders link and open the order
    Then Navigate to Handover details section and enter the required details
    Then User should be able to Mark Status as complete for the order
    Then Logout from SF_SVO Portal
    
  #User tries to move the order status from Accepted sales to Outcome Retailed without filling Handover details
  @SVO @SVO-139 @SSEC-410
  Scenario: User tries to move the order status from Accepted sales to Outcome Retailed without filling Handover details
    Given Access SF_SVO Portal
    Then User Navigate to SVO
    Then User Navigate to Opportunity tab
    Then Open an opportunity which is in OrderPlaced stage
    Then User selects an order which is linked to selected Opportunity
    Then User clears HandOver details
    And User click on the mark status as complete button of an Order
    Then Verify the error message to clear the Restricted party screening
    Then Logout from SF_SVO Portal
    
  #User tries to retrieve the mail from drafts after entering email body and subject
  @SVO @SVO-140 @SSEC-411
  Scenario: User tries to retrieve the mail from drafts after entering email body and subject
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    And Search for Opportunity with stage "Qualified" and open relevant Opportunity
    Then Click on Compose email
    Then edit the required information as Additional to "New Enquiry,U,2" Subject "New Enquiry,X,2" Body "New Enquiry,Y,2"
    And Click on save mail
    Then Click on Compose email
    Then edit the Additional to "New Enquiry,U,2"
    Then Click on drafts and retrive draft with Subject "New Enquiry,X,2"
    Then Logout from SF_SVO Portal
    
  #User Fills the Payment details for an Invoice of an Order
  @SVO @SVO-141 @SSEC-412
  Scenario: User Fills the Payment details for an Invoice of an Order
    Given Access SF_SVO Portal
    Then User Navigate to SVO
    Then User Navigate to Opportunity tab
    Then Open an opportunity which is in OrderPlaced stage
    Then User selects an order which is linked to selected Opportunity
    Then User selects an Invoice linked to Order
    Then User fills Payment details of an Invoice
    Then Verify the payment due date of an Invoice
    Then Logout from SF_SVO Portal

  #User tries to Edit the close date of closed won Opportunity
  @SVO @SVO-142 @SSEC-413
  Scenario: User tries to Edit the close date of closed won Opportunity
    Given Access SF_SVO Portal
    Then User Navigate to SVO
    Then User Navigate to Opportunity tab
    Then Open an opportunity which is in OrderPlaced stage
    Then User edits the Close date of an Opportunity
    And User click on the mark stage as complete button to complete the process
    Then User mark status as closed
    Then Verify the error message to clear the Restricted party screening
    Then Logout from SF_SVO Portal

  #User tries to add pinewood job card under reference numbers of an order
  @SVO @SVO-143 @SSEC-414
  Scenario: User tries to add pinewood job card under reference numbers of an order
    Given Access SF_SVO Portal
    Then User Navigate to SVO
    Then User Navigate to Opportunity tab
    Then Open an opportunity which is in OrderPlaced stage
    Then User selects an order which is linked to selected Opportunity
    Then User enters pinewood job card number
    Then Logout from SF_SVO Portal
   	
  #User Creates a new Quote
  @SVO @SVO-144 @SSEC-415
  Scenario: User Creates a new Quote
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,5"
    And Enter details of New Opportunity like Opportunity name "Opportunity,L,5" Product Offering "Opportunity,B,5" Account Name "Opportunity,D,5" Region "Opportunity,E,5" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,5" and Model "Opportunity,J,5"
    Then Save the created Opportunity
    Then Navigate to Quotes
    Then Create a new Quote "Test Quote" and save
    Then Logout from SF_SVO Portal

  #User is able to edit the Quote
  @SVO @SVO-145 @SSEC-416
  Scenario: User is able to edit the Quote
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,5"
    And Enter details of New Opportunity like Opportunity name "Opportunity,L,5" Product Offering "Opportunity,B,5" Account Name "Opportunity,D,5" Region "Opportunity,E,5" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,5" and Model "Opportunity,J,5"
    Then Save the created Opportunity
    Then Navigate to Quotes
    Then Create a new Quote "Test Quote" and save
    Then Logout from SF_SVO Portal
 
  #User is able to Delete the Quotes
  @SVO @SVO-146 @SSEC-417
  Scenario: User is able to Delete the Quotes
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,5"
    And Enter details of New Opportunity like Opportunity name "Opportunity,L,5" Product Offering "Opportunity,B,5" Account Name "Opportunity,D,5" Region "Opportunity,E,5" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,5" and Model "Opportunity,J,5"
    Then Save the created Opportunity
    Then Navigate to Quotes
    Then Create a new Quote "Test Quote" and save
    Then Delete the Quote created
    Then Logout from SF_SVO Portal
    
  #At design brief stage user skips updating the design brief in related quick links
  @SVO @SVO-147 @SSEC-418
  Scenario: At design brief stage user skips updating the design brief in related quick links
    Given Access SF_SVO Portal
    Then User Navigate to SVO
    Then User Navigate to Opportunity tab
    Then User selects All Bespoke opportunities from select list view
    Then Open an opportunity which is in OrderPlaced stage
    Then Navigate to Quote section
    Then User created new Bespoke Design Brief Quote
    Then User clears Design brief section
    And User click on the mark status as complete button of an Order
    Then Verify the error message to clear the Restricted party screening
    Then Logout from SF_SVO Portal
    
  #User is able to check and un-check the Required design team
  @SVO @SVO_148 @SSEC-419
  Scenario: User is able to check and un-check the Required design team
    Given Access SF_SVO Portal
    Then User Navigate to SVO
    Then User Navigate to Opportunity tab
    Then User searched and select "Opportunity,AC,3" opportunities from select list view
    And Open an opportunity with Record Type "Opportunity,R,7"
    Then Navigate to Quote section
    Then Create a new Quote "Opportunity,A,5" and save
    Then User navigate to Design brief section
    Then User uncheck the design team
    Then Logout from SF_SVO Portal
    
  #User is not able to change the ops Approval Status
  @SVO @SVO-149 @SSEC-420
  Scenario: User is not able to change the ops Approval Status
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    Then Click on New button
    And Enter details of New SVO Bespoke Opportunity like Opportunity name "Opportunity,L,5" Product Offering "Opportunity,B,5" Account Name "Opportunity,D,5" Region "Opportunity,E,5" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,5" and Model "Opportunity,J,5"
    Then Save the created Opportunity
    Then Navigate to Quotes
    Then Create a new SVO Quote "Test Quote" and save
    Then Verify the OPS Approval Status field is not editable
    Then Logout from SF_SVO Portal

	#User is able to fill the Exterior design with some Random text
  @SVO @SVO-150 @SSEC-421
  Scenario: User is able to fill the Exterior design with some Random text
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,5"
    And Enter details of New Opportunity like Opportunity name "Opportunity,L,5" Product Offering "Opportunity,B,5" Account Name "Opportunity,D,5" Region "Opportunity,E,5" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,5" and Model "Opportunity,J,5"
    Then Save the created Opportunity
    Then Navigate to Quotes
    Then Create a new Quote "Test Quote" and save
    Then Navigate and open Design briefs
    Then Enter the Exterior design details and save
    Then Logout from SF_SVO Portal

	#User is able to update the design brief in related Quick links and displays a second pop up for submit
  @SVO @SVO-151 @SSEC-422
  Scenario: User is able to update the design brief in related Quick links and displays a second pop up for submit
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,5"
    And Enter details of New Opportunity like Opportunity name "Opportunity,L,5" Product Offering "Opportunity,B,5" Account Name "Opportunity,D,5" Region "Opportunity,E,5" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,5" and Model "Opportunity,J,5"
    Then Save the created Opportunity
    Then Navigate to Quotes
    Then Create a new Quote "Test Quote" and save
    Then Navigate and open Design briefs
    Then Click on Submit and verify the error
    Then Logout from SF_SVO Portal
    
    #User is able to see Approval history for the quote
  @SVO @SVO_152
  Scenario: User is able to see Approval history for the quote
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,5"
    And Enter details of New SVO Bespoke Opportunity like Opportunity name "Opportunity,L,5" Product Offering "Opportunity,B,5" Account Name "Opportunity,D,5" Region "Opportunity,E,5" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,5" and Model "Opportunity,J,5"
    Then Save the created Opportunity
    Then Navigate to Quotes
    Then Create a new Quote "Test Quote" and save
    Then Navigate and open Design briefs
    Then Move the design brief stage from Draft to Submitted
    Then Verfify the Approval History of Design brief
    Then Logout from SF_SVO Portal

  #User cannot delete Closed Won Opportunity
  @SVO @SVO_153
  Scenario: User cannot delete Closed Won Opportunity
    Given Access SF_SVO Portal
    Then set up login to the user role "User Role,B,11"
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,5"
    And Enter details of New Opportunity like Opportunity name "Opportunity,L,2" Product Offering "Opportunity,B,6" Account Name "Opportunity,D,2" Region "Opportunity,E,2" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,4" and Model "Opportunity,J,4"
    Then Save the created Opportunity
    Then Verify that delete button is not available
    Then Logout from SF_SVO Portal

  #User Skips the pricing Details and price stage
  @SVO @SVO_154
  Scenario: User Skips the pricing Details and price stage
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,5"
    And Enter details of New Opportunity like Opportunity name "Opportunity,L,2" Product Offering "Opportunity,B,6" Account Name "Opportunity,D,2" Region "Opportunity,E,2" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,4" and Model "Opportunity,J,4"
    Then Save the created Opportunity
    Then Navigate to Quotes
    Then Create a new Quote "Test Quote" and save
    Then Navigate and open Design briefs
    Then Move the design brief stage from Draft to Submitted
    Then Return Render pack of design brief
    Then Navigate back to Quote section
    Then User should be able to Mark Status as complete for the order
    Then Verify the error message to clear the Restricted party screening
    Then Logout from SF_SVO Portal

  #User able to fill the price details and proceed further
  @SVO @SVO_155
  Scenario: User able to fill the price details and proceed further
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,5"
    And Enter details of New SVO Bespoke Opportunity like Opportunity name "Opportunity,L,5" Product Offering "Opportunity,B,5" Account Name "Opportunity,D,5" Region "Opportunity,E,5" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,5" and Model "Opportunity,J,5"
    Then Save the created Opportunity
    Then Navigate to Quotes
    Then Create a new Quote "Opportunity,A,5" and save
    Then Navigate and open Design briefs
    Then Move the design brief stage from Draft to Submitted
    Then Return Render pack of design brief
    Then Navigate back to Quote section
    Then fill the price details of Quote like CostPrice "Opportunity,X,2" ListPrice "Opportunity,Y,2" Contribution "Opportunity,Z,2"  RetailPrice "Opportunity,AA,2" Revenue"Opportunity,AB,2"
    Then User should be able to Mark Status as complete for the order
    Then Logout from SF_SVO Portal

  #User able to update the Proposal sent stage
  @SVO @SVO_156
  Scenario: User able to update the Proposal sent stage
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,5"
    And Enter details of New SVO Bespoke Opportunity like Opportunity name "Opportunity,L,5" Product Offering "Opportunity,B,5" Account Name "Opportunity,D,5" Region "Opportunity,E,5" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,5" and Model "Opportunity,J,5"
    Then Save the created Opportunity
    Then Navigate to Quotes
    Then Create a new Quote "Opportunity,A,5" and save
    Then Navigate and open Design briefs
    Then Move the design brief stage from Draft to Submitted
    Then Return Render pack of design brief
    Then Navigate back to Quote section
    Then fill the price details of Quote like CostPrice "Opportunity,X,2" ListPrice "Opportunity,X,2" Contribution "Opportunity,X,2"  RetailPrice "Opportunity,X,2" Revenue"Opportunity,X,2"
    Then User should be able to Mark Status as complete for the order
    Then User should be able to Mark Status as complete for the order
    Then User should be able to Mark Status as complete for the order
    Then User enter Outcome fields like Outcome "Opportunity,W,2" OutcomeStage "Opportunity,W,2"
    Then Logout from SF_SVO Portal

  #User able to send the SVO retailer proposal
  @SVO @SVO_157
  Scenario: User able to send the SVO retailer proposal
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,5"
    And Enter details of New SVO Bespoke Opportunity like Opportunity name "Opportunity,L,5" Product Offering "Opportunity,B,5" Account Name "Opportunity,D,5" Region "Opportunity,E,5" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,5" and Model "Opportunity,J,5"
    Then Save the created Opportunity
    Then Navigate to Quotes
    Then Create a new Quote "Opportunity,A,5" and save
    Then Navigate and open Design briefs
    Then Move the design brief stage from Draft to Submitted
    Then Return Render pack of design brief
    Then Navigate back to Quote section
    Then fill the price details of Quote like CostPrice "Opportunity,X,2" ListPrice "Opportunity,X,2" Contribution "Opportunity,X,2"  RetailPrice "Opportunity,X,2" Revenue"Opportunity,X,2"
    Then User should be able to Mark Status as complete for the order
    Then User send Quote pack
    Then Verify the error message displayed
    Then Logout from SF_SVO Portal
    
  #User tries to change the Status of the Quote while editing the details
  @SVO @SVO-158 @SSEC-429
  Scenario: User tries to change the Status of the Quote while editing the details
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,5"
    And Enter details of New Opportunity like Opportunity name "Opportunity,L,5" Product Offering "Opportunity,B,5" Account Name "Opportunity,D,5" Region "Opportunity,E,5" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,5" and Model "Opportunity,J,5"
    Then Save the created Opportunity
    Then Navigate to Quotes
    Then Create a new Quote "Test Quote" and save
    Then User edit the status as "Opportunity,S,8" of saved Quote
    Then Verify the error message displayed to save quote
    Then Logout from SF_SVO Portal

  #User can update/Change the Outcome Details
  #SVO Private office user
  @SVO @SVO_159
  Scenario: User can update/Change the Outcome Details
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity for SVO Private Office
    And Enter details of New Opportunity for Private Office Opportunity name "Opportunity,L,2" Product Offering "Opportunity,B,6" Account Name "Opportunity,D,5" Region "Opportunity,E,2" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,2" and Model "Opportunity,J,2"
    Then Save the created Opportunity
    Then Navigate to Quotes
    Then Create a new Quote "Test Quote" for Private Office and save
    Then Mark Status as Complete
    Then Mark Status as Complete
    Then Mark the Quote status as Outcome accepted
    Then Click on Edit and verify the outcome details cannot be changed
    Then Logout from SF_SVO Portal
    
  #User who don't have licence, cannot complete 'E-sign documents in person' Activity
  @SVO @SVO_160
  Scenario: User who don't have licence, cannot complete 'E-sign documents in person' Activity
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,8"
    And Enter details of New Opportunity like Opportunity name "Opportunity,L,2" Product Offering "Opportunity,B,2" Account Name "Opportunity,D,5" Region "Opportunity,E,2" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,2" and Model "Opportunity,J,2"
    Then Save the created Opportunity
    Then Navigate to Quotes
    Then Create a new Quote "Test Quote" and save
    Then Navigate and open Design briefs
    Then Click on Submit
    Then Click on Return Render pack and navigate back to Quote
    Then Mark the Quote stage as Price
    Then Click on Send Quote Pack
    Then Select a document and move to next step to E-Sign documents in person and verify the error
    Then Logout from SF_SVO Portal
    
  #User is able to add Orders to the Quotes
  @SVO @SVO-161
  Scenario: User is able to add Orders to the Quotes
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,6"
    And Enter details of New Opportunity like Opportunity name "Opportunity,L,6" Product Offering "Opportunity,B,6" Account Name "Opportunity,D,6" Region "Opportunity,E,6" Preferred Contact "Opportunity,H,6" Brand "Opportunity,I,6" and Model "Opportunity,J,6"
    Then Save the created Opportunity
    Then Navigate to Quotes
    Then Create a new Quote "Test Quote" and save
    Then Navigate to Orders under Quotes and click open
    Then Create a new order with Opportunity "Opportunity,W,2"
    Then Save the order created
    Then Logout from SF_SVO Portal
    
    #User is able to Edit and Delete the orders in Quote section
  @SVO @SVO_162
  Scenario: User is able to Edit and Delete the orders in Quote section
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,8"
    And Enter details of New Opportunity like Opportunity name "Opportunity,L,2" Product Offering "Opportunity,B,2" Account Name "Opportunity,D,5" Region "Opportunity,E,2" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,2" and Model "Opportunity,J,2"
    Then Save the created Opportunity
    Then Navigate to Quotes
    Then Create a new Quote "Test Quote" and save
    Then Delete the Quote created
    Then Logout from SF_SVO Portal

  #Check in files section for approval file
  @SVO @SVO_163
  Scenario: Check in files section for approval file
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,8"
    And Enter details of New Opportunity like Opportunity name "Opportunity,L,2" Product Offering "Opportunity,B,2" Account Name "Opportunity,D,5" Region "Opportunity,E,2" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,2" and Model "Opportunity,J,2"
    Then Save the created Opportunity
    Then Navigate to Quotes
    Then Create a new Quote "Test Quote" and save
    Then Navigate to Files under Quotes and click open
    Then Add a new file and save
    Then Logout from SF_SVO Portal

  #User tries to navigate back to the completed stages at outcome stage
  @SVO @SVO-164
  Scenario: User tries to navigate back to the completed stages at outcome stage
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,6"
    And Enter details of New Opportunity like Opportunity name "Opportunity,L,6" Product Offering "Opportunity,B,6" Account Name "Opportunity,D,6" Region "Opportunity,E,6" Preferred Contact "Opportunity,H,6" Brand "Opportunity,I,6" and Model "Opportunity,J,6"
    Then Save the created Opportunity
    Then Navigate to Quotes
    Then Create a new Quote "Test Quote" and save
    Then Navigate and open Design briefs
    Then Click on Submit and navigate back to Quote
    Then Mark the Quote status as Outcome accepted
    Then Mark the Quote status as Proposal Sent and verify user cannot navigate back stages
    Then Logout from SF_SVO Portal
    
  #User skip to select the SVO proposal Document at 'Create S-Docs' stage
  @SVO @SVO_165
  Scenario: User skip to select the SVO proposal Document at 'Create S-Docs' stage
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,8"
    And Enter details of New Opportunity like Opportunity name "Opportunity,L,2" Product Offering "Opportunity,B,2" Account Name "Opportunity,D,5" Region "Opportunity,E,2" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,2" and Model "Opportunity,J,2"
    Then Save the created Opportunity
    Then Navigate to Quotes
    Then Create a new Quote "Test Quote" and save
    Then Navigate and open Design briefs
    Then Click on Submit
    Then Click on Return Render pack and navigate back to Quote
    Then Mark the Quote stage as Price
    Then Click on Send Quote Pack
    Then Click on next step without selecting a document and verify the error
    Then Logout from SF_SVO Portal
    
  #User tries to Edit comment section of an work order of an Opportunity
  #User Role: Classic Service
  @SVO @SVO_178
  Scenario: User tries to Edit comment section of an work order of an Opportunity
    Given Access SF_SVO Portal
    Then set up login to the user role "User Role,B,6"
    Then User Navigate to Opportunity tab
    Then User searched and select as "Opportunity,AC,2" opportunities from select list view
    And Open an opportunity with Record Type "Opportunity,R,13"
    Then navigate to Orders link and open the order
    Then Open the work order
    Then Verify Edit comment button is not available 
    Then Logout from SF_SVO Portal
    
  #User tries to edit the Stage history and Opportunity field history of an Opportunity
  @SVO @SVO_179
  Scenario: User tries to edit the Stage history and Opportunity field history of an Opportunity
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    And Search for Opportunity with stage "Design Brief & Quote" and open relevant Opportunity
    Then Navigate to Stage History and verify user cannot edit the stage history
    Then Navigate to Opportunity field history and verify user cannot edit the Opportunity field history
    Then Logout from SF_SVO Portal
    
    #User create new Filter for searching an Opportunities
  @SVO @SVO_180
  Scenario: User create new Filter for searching an Opportunities
    Given Access SF_SVO Portal
    Then User Navigate to SVO
    Then User Navigate to Opportunity tab
    Then User selects New option to create New filter
    Then User fill the details like ListName "Opportunity,AL,2" and ListAPIname "Opportunity,AM,2" then click on save
    Then User Add filter by entering details like Field "Opportunity,AN,2" Operator "Opportunity,AO,2" Value "Opportunity,I,2"
    Then User click on Save filter
    Then Verify that New filter is created
    Then Logout from SF_SVO Portal

  #User should be able to do E-Sign the document in person if the user is having Licence
  @SVO @SVO_181
  Scenario: User should be able to do E-Sign the document in person if the user is having Licence
    Given Access SF_SVO Portal
    Then set up login to the user role "User Role,B,17"
    Then User Navigate to Opportunity tab
    And User click on New button to create an opportunity
    And fill all mandatory fields of SVOBespoke Mike opportunity like OpportunityName "Opportunity,A,8" Closedate "Opportunity,AI,2" Stage "Opportunity,R,4" ProductOffering "Opportunity,B,2" Region "Opportunity,E,2" AccountName "Opportunity,D,9" PreferredContact "Opportunity,H,9" Brand "Opportunity,I,2" Model "Opportunity,J,2"
    Then Navigate to Quotes
    Then Create a new Bespoke design brief Quote "Opportunity,D,9"
    Then User save the new Bespoke design brief Mike Quote
    Then User selects the created Quote
    Then Navigate and open Design briefs
    Then Move the design brief stage from Draft to Submitted
    Then Return Render pack of design brief
    Then Navigate back to Quote section
    Then fill the price details of Quote like CostPrice "Opportunity,X,2" ListPrice "Opportunity,X,2" Contribution "Opportunity,X,2"  RetailPrice "Opportunity,X,2" Revenue"Opportunity,X,2"
    Then User should be able to Mark Status as complete for the order
    When User selects documents and click on Next step
    And Generated documents are sent for Electronic Signature through mail
    Then Logout from SF_SVO Portal

  #User tries to select template of e2a mail after adding text into email body
  @SVO @SVO_182
  Scenario: User tries to select template of e2a mail after adding text into email body
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,8"
    And fill all mandatory fields of SVOBespoke opportunity like OpportunityName "Opportunity,A,8" Closedate "Opportunity,AI,2" Stage "Opportunity,R,4" ProductOffering "Opportunity,B,2" Region "Opportunity,E,2" AccountName "Opportunity,D,9" PreferredContact "Opportunity,H,9" Brand "Opportunity,I,2" Model "Opportunity,J,2"
    Then Click on Compose email
    And User selects the template of an email
    Then User save the email
    Then Logout from SF_SVO Portal
    
  #User tries to Re-open the closed quote of an Opportunity
  @SVO @SVO_183
  Scenario: User tries to Re-open the closed quote of an Opportunity
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,8"
    And Enter details of New Opportunity like Opportunity name "Opportunity,L,2" Product Offering "Opportunity,B,2" Account Name "Opportunity,D,5" Region "Opportunity,E,2" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,2" and Model "Opportunity,J,2"
    Then Save the created Opportunity
    Then Navigate to Quotes
    Then Create a new Quote "Test Quote" and save
    Then Navigate and open Design briefs
    Then Click on Submit and navigate back to Quote
    Then Mark the Quote status as Outcome rejected
    Then Mark the Quote status as Design brief draft and verify the error message
    Then Logout from SF_SVO Portal

  #User tries to Edit the Priority position of an Opportunity
  @SVO @SVO_184
  Scenario: User tries to Edit the Priority position of an Opportunity
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,7"
    And Enter details of New Opportunity like Opportunity name "Opportunity,L,7" Stage "Opportunity,R,9" Region "Opportunity,E,2" Account Name "Opportunity,D,5" Preferred Contact "Opportunity,H,9" Programme "Opportunity,U,5" and Source "Opportunity,C,2"
    Then Save the created Opportunity
    Then Edit the priority position of the opportunity as "5" and Save
    Then Logout from SF_SVO Portal

  #User tries to Move the opportunity stage from Waitlist to contracting(without navigating through intermediate stages)
  @SVO @SVO_185
  Scenario: User tries to Move the opportunity stage from Waitlist to contracting(without navigating through intermediate stages)
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,7"
    And Enter details of New Opportunity like Opportunity name "Opportunity,L,7" Stage "Opportunity,R,9" Region "Opportunity,E,2" Account Name "Opportunity,D,5" Preferred Contact "Opportunity,H,9" Programme "Opportunity,U,5" and Source "Opportunity,C,2"
    Then Save the created Opportunity
    Then Click on Contracting stage and mark as current stage
    Then Logout from SF_SVO Portal

  #User sends an contract agreement for electronic signature
  @SVO @SVO_186
  Scenario: User sends an contract agreement for electronic signature
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    And Search for Opportunity with stage "Contracting" and open relevant Opportunity
    Then Click on S-Docs button
    Then Select a document and move to next step to send documents for electronic signature
    Then Logout from SF_SVO Portal
    
  #User tries to Edit the status of an Order in backword direction
  @SVO @SVO_187
  Scenario: User tries to Edit the status of an Order in backword direction
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    And Open an opportunity with Record Type "Opportunity,R,7"
    Then navigate to Orders link and open the order
    When User changes order stage to "Opportunity,AP,2"
    Then Verify the error message displayed to save quote
    Then Logout from SF_SVO Portal

  #User tries to Edit Donor details of an Classic Bespoke Opportunity
  @SVO @SVO_188
  Scenario: User tries to Edit Donor details of an Classic Bespoke Opportunity
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    Then User searched and select "Opportunity,AC,2" opportunities from select list view
    And Open an opportunity with Record Type "Opportunity,R,10"
    Then User edit the donor details of vehicle
    Then Verify the error message to fill the Programme
    Then Logout from SF_SVO Portal

  #User tries to add New Task for an Opportunity
  @SVO @SVO_189
  Scenario: User tries to add New Task for an Opportunity
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    And Open an opportunity with Record Type "Opportunity,R,7"
    When User navigate to Activity tab
    And User navigate to New task tab
    Then User created new task with subject field as "Opportunity,B,3"
    Then User save the Task
    Then Logout from SF_SVO Portal
    
  #User Skips the Common order number Field in Orders Section and mark Status as complete and try to Mark Status as complete
  @SVO @SVO_190
  Scenario: User Skips the Common order number Field in Orders Section and mark Status as complete and try to Mark Status as complete
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    Then User searched and select "Opportunity,AC,4" opportunities from select list view
    And Open an opportunity with Record Type "Opportunity,R,7"
    Then navigate to Orders link and open the order
    When User clears common order number
    And User mark status as complete
    Then Verify the error message to clear the Restricted party screening
    Then Logout from SF_SVO Portal

  #User Able to upload file to the Orders at Outcome Stage
  @SVO @SVO_191
  Scenario: User Able to upload file to the Orders at Outcome Stage
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    Then User searched and select "Opportunity,AC,4" opportunities from select list view
    And Open an opportunity with Record Type "Opportunity,R,7"
    Then navigate to Orders link and open the order
    And User navigate to Files section
    When User add files to the selected order
    Then Logout from SF_SVO Portal
    
    #User Able to share Files in Order section
  @SVO @SVO_192
  Scenario: User Able to share Files in Order section
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    And Search for Opportunity with stage "Order Placed" and open relevant Opportunity
    Then Navigate to Orders section and open the order
    Then Navigate to Files section
    Then Add a new file and save
    Then Logout from SF_SVO Portal

  #User moves to Proposed status of created Commissioning Request from any non-closed Bespoke Opportunity
  @SVO @SVO_193
  Scenario: User moves to Proposed status of created Commissioning Request from any non-closed Bespoke Opportunity
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,8"
    And Enter details of New Opportunity like Opportunity name "Opportunity,L,2" Product Offering "Opportunity,B,2" Account Name "Opportunity,D,5" Region "Opportunity,E,2" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,2" and Model "Opportunity,J,2"
    Then Save the created Opportunity
    Then Navigate to Commissioning Request
    Then Create a new Commissioning Request from "Opportunity,AH,3" to "Opportunity,AI,3" and save
    Then Navigate to Reservations
    Then Create a new "Bespoke" reservation from "Opportunity,AH,3" to "Opportunity,AI,3" with contact "Opportunity,H,5" and save
    Then Navigate back to Commissioning request and verify the status is moved to Proposed
    Then Logout from SF_SVO Portal

  #User verifies that Enquiry Owner receives an email when Commissioning Request status changed to Booked for any non-closed Bespoke Opportunity
  @SVO @SVO_194
  Scenario: User verifies that Enquiry Owner receives an email when Commissioning Request status changed to Booked for any non-closed Bespoke Opportunity
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,8"
    And Enter details of New Opportunity like Opportunity name "Opportunity,L,2" Product Offering "Opportunity,B,2" Account Name "Opportunity,D,5" Region "Opportunity,E,2" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,2" and Model "Opportunity,J,2"
    Then Save the created Opportunity
    Then Navigate to Commissioning Request
    Then Create a new Commissioning Request from "Opportunity,AH,3" to "Opportunity,AI,3" and save
    Then Navigate to Reservations
    Then Create a new "Bespoke" reservation from "Opportunity,AH,3" to "Opportunity,AI,3" with contact "Opportunity,H,5" and save
    Then Open the reservation and change the status to Booked and host "Opportunity,H,5"
    Then Navigate back to Commissioning request and verify the status is moved to "Booked"
    Then Logout from SF_SVO Portal

  #User can be able to change the Closed stage  for opportunities Section
  @SVO @SVO_195
  Scenario: User can be able to change the Closed stage  for opportunities Section
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    And Search for Opportunity with stage "Closed Lost" and open relevant Opportunity
    Then Click on Change closed stage
    Then Edit the stage to Contract signed and save to verify the error
    Then Logout from SF_SVO Portal
    
  #User skips the mandatory fields at contract signed and makes Mark status as complete
  @SVO @SVO_196
  Scenario: User skips the mandatory fields at contract signed and makes Mark status as complete
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    Then User searched and select "Opportunity,AC,2" opportunities from select list view
    And Open an opportunity with Record Type "Opportunity,R,11"
    When User clears Body style details of vehicle
    And User click on the mark stage as complete button to complete the process
    Then Verify the error message to clear the Restricted party screening
    Then Logout from SF_SVO Portal
    
  #User verifies that Enquiry Owner receives an Thank you email for Commissioning Request which is in Closed status for any non-closed Bespoke Opportunity
  @SVO @SVO_197
  Scenario: User verifies that Enquiry Owner receives an Thank you email for Commissioning Request which is in Closed status for any non-closed Bespoke Opportunity
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,8"
    And Enter details of New Opportunity like Opportunity name "Opportunity,L,2" Product Offering "Opportunity,B,2" Account Name "Opportunity,D,5" Region "Opportunity,E,2" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,2" and Model "Opportunity,J,2"
    Then Save the created Opportunity
    Then Navigate to Commissioning Request
    Then Create a new Commissioning Request from "Opportunity,AH,3" to "Opportunity,AI,3" and save
    Then Navigate to Reservations
    Then Create a new "Bespoke" reservation from "Opportunity,AH,3" to "Opportunity,AI,3" with contact "Opportunity,H,5" and save
    Then Open the reservation and change the status to Closed with reason "Attended" and host "Opportunity,H,5"
    Then Navigate back to Commissioning request and verify the status is moved to "Closed"
    Then Logout from SF_SVO Portal

  #User fills the Handover date and check on Fully paid and mark status as complete
  @SVO @SVO_198
  Scenario: User fills the Handover date and check on Fully paid and mark status as complete
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,4"
    And Enter Required details of New Opportunity like Product Offering "Opportunity,B,4" Source "Opportunity,C,4" Account Name "Opportunity,D,5" Region "Opportunity,E,4" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,2" and Model "Opportunity,J,2"
    Then Save the created Opportunity
    Then Change the opportunity status to Order Placed
    Then navigate to Orders link and open the order
    Then Navigate to Handover details section and enter the required details
    And User click on the mark status as complete button of an Order
    Then Logout from SF_SVO Portal

  #User can be able to delete the Invoice in Orders
  @SVO @SVO_199
  Scenario: User can be able to delete the Invoice in Orders
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,4"
    And Enter Required details of New Opportunity like Product Offering "Opportunity,B,4" Source "Opportunity,C,4" Account Name "Opportunity,D,5" Region "Opportunity,E,4" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,2" and Model "Opportunity,J,2"
    Then Save the created Opportunity
    Then Change the opportunity status to Order Placed
    Then navigate to Orders link and open the order
    Then Navigate to invoices
    Then Create a new invoice with Opportunity "Classic bespoke" Account "Opportunity,AE,2" and SAP account "Opportunity,AF,2" and save
    Then Click on Delete button
    Then Logout from SF_SVO Portal

  #User cannot edit/change the auto populate fields for Invoice
  @SVO @SVO_200
  Scenario: User cannot edit/change the auto populate fields for Invoice
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,4"
    And Enter Required details of New Opportunity like Product Offering "Opportunity,B,4" Source "Opportunity,C,4" Account Name "Opportunity,D,5" Region "Opportunity,E,4" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,2" and Model "Opportunity,J,2"
    Then Save the created Opportunity
    Then Change the opportunity status to Order Placed
    Then navigate to Orders link and open the order
    Then Navigate to invoices
    Then Create a new invoice with Opportunity "Classic bespoke" Account "Opportunity,AE,2" and SAP account "Opportunity,AF,2" and save
    Then Verify that auto populated fields like Invoice name and Status are not editable
    Then Logout from SF_SVO Portal
    
    #User can submit Commissioning Request which is Draft status for any non-closed Private Office Opportunity
  #SVO Private Office Opportunity
  @SVO @SVO_201
  Scenario: User can submit Commissioning Request which is Draft status for any non-closed Private Office Opportunity
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,10"
    And fill all mandatory fields of SVO Private Office opportunity like OpportunityName "Opportunity,A,10" Closedate "Opportunity,AI,2" Stage "Opportunity,R,4" ProductOffering "Opportunity,B,10" Region "Opportunity,E,2" AccountName "Opportunity,D,9" PreferredContact "Opportunity,H,9" Brand "Opportunity,I,2" Model "Opportunity,J,2"
    When User navigate to Commissioning Requests
    Then User create new Commissioning Request with Record type "Opportunity,AS,2"
    And fill all mandatory fields of Commissioning Request like Status "Opportunity,AT,2" Starting "Opportunity,AG,2" Ending "Opportunity,AH,2" Duration "Opportunity,V,5" EstAttendees "Opportunity,AK,2" TourType "Opportunity,AU,2"
    Then Save the created Opportunity
    When User open the created commissioning Request
    And User submit the Commissioning Request
    Then Logout from SF_SVO Portal

  #User is able to add Note to the Opportunity
  #SVO Private Office Opportunity
  @SVO @SVO_202
  Scenario: User is able to add Note to the Opportunity
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,10"
    And fill all mandatory fields of SVO Private Office opportunity like OpportunityName "Opportunity,A,10" Closedate "Opportunity,AI,2" Stage "Opportunity,R,4" ProductOffering "Opportunity,B,10" Region "Opportunity,E,2" AccountName "Opportunity,D,9" PreferredContact "Opportunity,H,9" Brand "Opportunity,I,2" Model "Opportunity,J,2"
    When User navigate to Notes Section
    And User Create New Quote with title "Opportunity,A,10"
    Then User Save the Note
    Then Logout from SF_SVO Portal

  #User is able to Share and Delete the Note
  #SVO Private Office Opportunity
  @SVO @SVO_203
  Scenario: User is able to Share and Delete the Note
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,10"
    And fill all mandatory fields of SVO Private Office opportunity like OpportunityName "Opportunity,A,10" Closedate "Opportunity,AI,2" Stage "Opportunity,R,4" ProductOffering "Opportunity,B,10" Region "Opportunity,E,2" AccountName "Opportunity,D,9" PreferredContact "Opportunity,H,9" Brand "Opportunity,I,2" Model "Opportunity,J,2"
    When User navigate to Notes Section
    And User Create New Quote with title "Opportunity,A,10"
    Then User share the created note to user "User Role,B,17"
    And User delete the created note
    Then Logout from SF_SVO Portal
    
  #User is able to delete the order which is at outcome stage in classic bespoke opportunity
  @SVO @SVO_204
  Scenario: User is able to delete the order which is at outcome stage in classic bespoke opportunity
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    Then User searched and select "Opportunity,AC,2" opportunities from select list view
    And Open an opportunity with Record Type "Opportunity,R,13"
    Then navigate to Orders link and open the order
    Then User deletes the order
    Then Logout from SF_SVO Portal
    
  #An order can automatically created after completing quote
  #SVO Bespoke access
  @SVO @SVO_206
  Scenario: An order can automatically created after completing quote
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    And Search for Opportunity with stage "Qualified" and open relevant Opportunity
    Then Navigate to Quotes
    Then Create a new SVO Quote "Test Quote" and save
    Then Navigate and open Design briefs
    Then Click on Submit and navigate back to Quote
    Then Mark the Quote status as Outcome accepted
    Then Navigate back to Opportunity
    Then navigate to Orders link and open the order
    Then Logout from SF_SVO Portal

  #User tries to create a new Opportunity with SVO Bespoke Record Type
  #Classic Bespoke access
  @SVO @SVO_207
  Scenario: User tries to create a new Opportunity with SVO Bespoke Record Type
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    Then Click on New opportunity button and verify user cannot create SVO Bespoke Record Type opportunity
    Then Logout from SF_SVO Portal
    
  #User tries to move the opportunity stage from  contract signed to order placed without an order creation
  #Users : Classic Finance
  @SVO @SVO_208
  Scenario: User tries to move the opportunity stage from  contract signed to order placed without an order creation
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    Then User searches for "Opportunity,AC,5" opportunities from select list view
    And Open an opportunity with Record Type "Opportunity,R,11"
    And User click on the mark stage as complete button to complete the process
    Then Verify the error message to close the opportunity
    Then Logout from SF_SVO Portal
    
  #User tries to create a new Programme while creating Classic Limited Edition opportunity
  #User Role : Classic Sales User : Michael Bishop
  @SVO @SVO_209
  Scenario: User tries to create a new Programme while creating Classic Limited Edition opportunity
    Given Access SF_SVO Portal
    Then set up login to the user role "User Role,B,18"
    Then User Navigate to Opportunity tab
    Then Create New Opportunity with Record Type "Opportunity,A,9"
    When click on Programme text box
    Then Verify that New Record button is not available
    Then Logout from SF_SVO Portal

  #User tries to generate document through S-DOCS for any open opportunity
  #Users : SVO Private Office
  @SVO @SVO_210
  Scenario: User tries to generate document through S-DOCS for any open opportunity
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    Then User searches for "Opportunity,AC,5" opportunities from select list view
    And Open an opportunity with Record Type "Opportunity,R,11"
    When User click on S-Docs button
    Then Verify the error message insufficient Privileges
    Then Logout from SF_SVO Portal
    
  #User tries to delete Closed Won Opportunity
  #Users : Classic Finance
  @SVO @SVO_211
  Scenario: User tries to delete Closed Won Opportunity
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    Then User searches for "Opportunity,AC,5" opportunities from select list view
    And Open an opportunity with Record Type "Opportunity,R,13"
    Then Verify the Delete button
    Then Logout from SF_SVO Portal
    
  #User tries to link an Enquiry for Classic Bespoke opportunity
  #user : admin
  @SVO @SVO_222
  Scenario: User tries to link an Enquiry for Classic Bespoke opportunity
    Given Access SF_SVO Portal
    Then User Navigate to Opportunity tab
    Then User searched and select "Opportunity,AC,2" opportunities from select list view
    And Open an opportunity with Record Type "Opportunity,R,7"
    When User click on Veicle number under donor details
    And User navigate to Related Tab
    Then create an new enquiry with Record type "Opportunity,A,8"
    And User enters Enquiry title "Opportunity,A,8" and click on save
    Then Verify that new enquiry is linked to Opportunity
    Then Logout from SF_SVO Portal
    
 	#User tries to Save vehicle record of Classic Bespoke Opportunity without adding Full VIN
  @SVO @SVO_223
  Scenario: User tries to Save vehicle record of Classic Bespoke Opportunity without adding Full VIN
    Given Access SF_SVO Portal
    When User Navigate to Opportunity
    And User searches for Classic bespoke opportunity and selects it
    Then Navigate to Vehicle field and click on edit
    Then User adds new "Classic Vehicle" record
    Then Enter fields Record name "Opportunity,S,5" Brand "Opportunity,I,2" model "Opportunity,J,2" and save leaving the VIN field blank
    Then Verify the error message displayed for VIN field
    Then Logout from SF_SVO Portal
    