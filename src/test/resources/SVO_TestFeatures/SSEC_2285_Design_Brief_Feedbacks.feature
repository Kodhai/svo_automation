@SSEC_2285
Feature: Validate Design Brief Amendments Feedback
  
  @SSEC_2285_01 @SSEC_2309
  Scenario: Verify that Interiors is present after Rear Console
   Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User creates a Bespoke Opportunity with check cleared like Stage "Opportunity,B,3" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,I,3" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,3"
    Then User verifies that new Opportunity is created
    Then User navigates to Quotes from Related Links
    And User creates new Bespoke Design Brief quotes "Opportunity,J,4" with mandatory details
    And Click on the new Bespoke Design Brief quote created successfully
    And Verify Reear Center console is present before and then Interiors
    Then User Logout from SVO Portal
    
    @SSEC_2285_02 @SSEC_2310
  Scenario: Verify Carpet section is moved after Interior section
   Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User creates a Bespoke Opportunity with check cleared like Stage "Opportunity,B,3" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,I,3" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,3"
    Then User verifies that new Opportunity is created
    Then User navigates to Quotes from Related Links
    And User creates new Bespoke Design Brief quotes "Opportunity,J,4" with mandatory details
    And Click on the new Bespoke Design Brief quote created successfully
    And Verify Interiors is present first and then Carpets
    Then User Logout from SVO Portal
  
  @SSEC_2285_03 @SSEC_2311
  Scenario: Verify Finishes drop down is removed in the Finishes section
   Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User creates a Bespoke Opportunity with check cleared like Stage "Opportunity,B,3" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,I,3" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,3"
    Then User verifies that new Opportunity is created
    Then User navigates to Quotes from Related Links
    And User creates new Bespoke Design Brief quotes "Opportunity,J,4" with mandatory details
    And Click on the new Bespoke Design Brief quote created successfully
    And Verify Finishes drop down is removed in the Finishes section
    Then User Logout from SVO Portal
    
     @SSEC_2285_04 @SSEC_2311
  Scenario: Verify in Paint Colors, additional colors are added in the list of Paint Detail
   Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User creates a Bespoke Opportunity with check cleared like Stage "Opportunity,B,3" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,I,3" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,3"
    Then User verifies that new Opportunity is created
    Then User navigates to Quotes from Related Links
    And User creates new Bespoke Design Brief quotes "Opportunity,J,4" with mandatory details
    And Click on the new Bespoke Design Brief quote created successfully
    And Verify in Paint Colors drop down additional colors are added
    Then User Logout from SVO Portal
    
    @SSEC_2285_05 @SSEC_2311
  Scenario: Verify User is able to fill Paint Details in the Paint section
   Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User creates a Bespoke Opportunity with check cleared like Stage "Opportunity,B,3" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,I,3" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,3"
    Then User verifies that new Opportunity is created
    Then User navigates to Quotes from Related Links
    And User creates new Bespoke Design Brief quotes "Opportunity,J,4" with mandatory details
    And Click on the new Bespoke Design Brief quote created successfully
    And Verify User is able to fill Paint Details in the Paint section PaintColor "Paint,A,2" PaintCode "Paint,B,2"
    Then User Logout from SVO Portal
    
     @SSEC_2285_06 @SSEC_2311
  Scenario: Verify Finishes drop down is removed in the Finishes section
   Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User creates a Bespoke Opportunity with check cleared like Stage "Opportunity,B,3" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,3" Account Name "Opportunity,I,3" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,3"
    Then User verifies that new Opportunity is created
    Then User navigates to Quotes from Related Links
    And User creates new Bespoke Design Brief quotes "Opportunity,J,4" with mandatory details
    And Click on the new Bespoke Design Brief quote created successfully
    And Verify Design Brief is submitted
    Then User Logout from SVO Portal
  
