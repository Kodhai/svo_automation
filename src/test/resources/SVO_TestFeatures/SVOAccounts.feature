Feature: SVO Accounts Validation

	#User is able create a new Individual account from Accounts section
  @SVO @SVO_031
  Scenario: User is able create a new Individual account from Accounts section
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Individual account with Record type "Accounts,A,10" First name "Accounts,L,10" Last name "Accounts,M,10" Region "Accounts,N,10" Email "Accounts,K,10"
    #Then Logout from the SF SVO Portal
	
  #User creates a new SAP Account for an individual account
  @SVO @SVO_032 
  Scenario: User creates a new SAP Account for an individual account
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create a new Individual account
    Then User Searches for account "Accounts,E,3" and selects it
    Then Navigate to the SAP Accounts section and click on New
    Then Enter details in new SAP Account and Save it
    Then create a new SAP account with Requested Status
    #Then Logout from the SF SVO Portal

  #User can be able to create multiple SAP Account for the same individual account
  @SVO @SVO_033 
  Scenario: User can be able to create multiple SAP Account for the same individual account
    #Given Access SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    And User searches for a User Account "New Enquiry,AA,2"
    Then User Navigates to the User Account "New Enquiry,AA,2"
    And Navigates to Users SAP Account
    Then User creates an SAP Account with SAP ID "SAP,A,2"
    And Enter all mandatory fields for SAP Account like Status "SAP,B,2" and Location "SAP,C,2"
    And Click on Save and New
    Then User creates an SAP Account with SAP ID "SAP,A,3"
    And Enter all mandatory fields for SAP Account like Status "SAP,B,3" and Location "SAP,C,3"
    And Save the details
    Then Filter on SAP Accounts using SAP	ID "SAP,A,2"
    And Delete the Selected SAP Account
    Then Filter on SAP Accounts using SAP	ID "SAP,A,3"
    And Delete the Selected SAP Account
    #Then Logout from the SVO Portal

  #User can be able to add relationships to an individual account from Related Contacts section
  @SVO @SVO_034 
  Scenario: User can be able to add relationships to an individual account from Related Contacts section
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    And User searches for a User Account "New Enquiry,AA,2"
    Then User Navigates to the User Account "New Enquiry,AA,2"
    Then Navigate to the Related contacts section and click on add Relationship
    And Enter details for New Account Relationship like Contact "Accounts,B,5",Roles "Accounts,I,2",Relationship Strength "Accounts,J,2" and Start Date
    And Save the details for New Account Relationship
    Then Filter on Related Contacts using Contact "SAP,A,2"
    And Remove Account Relationship
    Given User logins with Classic Sales role
    When User Navigate to Accounts
    And User searches for a User Account "New Enquiry,AA,2"
    Then User Navigates to the User Account "New Enquiry,AA,2"
    Then Navigate to the Related contacts section and click on add Relationship
    Then Verify that relation field is not present
    #Then Logout from the SF SVO Portal

#User can view added relationship's contact name into Contacts Menu
  @SVO @SVO_035 @SSEC-306 
  Scenario: User can view added relationship's contact name into Contacts Menu
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then User Searches for account "Accounts,E,2" and selects it
    Then Navigate to the Related contacts section and click on add Relationship
    And Enter the Relationship contact information "Accounts,B,2" and save it
    Then Verify the contact name "Accounts,B,2" and details
    #Then Logout from the SF SVO Portal
    
  #User adds new vehicle ownership as a driver to an individual account [Excluding end date]
  @SVO @SVO_036 
  Scenario: User adds new vehicle ownership as a driver to an individual account [Excluding end date]
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Individual account with Record type "Accounts,A,10" First name "Accounts,L,10" Last name "Accounts,M,10" Region "Accounts,N,10" Email "Accounts,K,10"
    Then Navigate to the Vehicles Owned section and click on new
    And select the ownership as driver and enter the Contact "Accounts,E,2" Brand "Accounts,C,2" and Model "Accounts,D,2" information and save it
    Then Verify the added new vehicle
    #Then Logout from the SF SVO Portal
    
  #User adds multiple vehicle ownership/vehicle driven to an individual account [As a Owner]
  @SVO @SVO_037 
  Scenario: User adds multiple vehicle ownership/vehicle driven to an individual account [As a Owner]
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Individual account with Record type "Accounts,A,10" First name "Accounts,L,10" Last name "Accounts,M,10" Region "Accounts,N,10" Email "Accounts,K,10"
    Then Navigate to the Vehicles Owned section and click on new
    And select the ownership as Owner and enter the Brand "Accounts,C,2" Model "Accounts,D,2" Start date "Accounts,F,2" and End date "Accounts,G,2" information
    Then click on save and new vehicle button
    And select the ownership as Owner and enter the Brand "Accounts,C,2" Model "Accounts,D,2" Start date "Accounts,F,3" and End date "Accounts,G,3" information
    Then click on save vehicle button
    Then Verify and delete the added vehicle
    Given User logins with Special Ops Data Manager role
    When User Navigate to Accounts
    Then Verify and delete the added vehicle
    #Then Logout from the SF SVO Portal
    
  #User adds Vehicle Driven as a owner to an individual account with end date
  @SVO @SVO_038 
  Scenario: User adds Vehicle Driven as a owner to an individual account with end date
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Individual account with Record type "Accounts,A,10" First name "Accounts,L,10" Last name "Accounts,M,10" Region "Accounts,N,10" Email "Accounts,K,10"
    Then User Searches for account "Accounts,E,3" and selects it
    Then Navigate to the Vehicles Driven section and click on New owner record type
    Then Enter details in new Vehicles driven and Save it
    #Then Logout from the SF SVO Portal
    
  #User cannot be able to select end date prior to start date while creating a new Vehicle Ownership/Vehicle Driven for an individual account
  @SVO @SVO_039 @SSEC-310 
  Scenario: User cannot be able to select end date prior to start date while creating a new Vehicle Ownership/Vehicle Driven for an individual account
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create a new Individual account
    Then User Searches for account "Accounts,E,3" and selects it
    Then Navigate to the Vehicles Driven section and click on New owner record type
    Then Enter details and past end date in new Vehicles driven and Save it to verify the error
    #Then Logout from the SF SVO Portal
    
  #User is able to view list of Enquiries, Opportunities and SVO Client Opportunities
  @SVO @SVO_040 @SSEC-311 
  Scenario: User is able to view list of Enquiries, Opportunities and SVO Client Opportunities
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then User Searches for account "Accounts,E,5" and selects it
    And User is able to view list of Enquiries, Opportunities and SVO Client Opportunities
    #Then Logout from the SF SVO Portal
    
  #User is able to create a new KMI record for an individual account
  @SVO @SVO_041 @SSEC-312 
  Scenario: User is able to create a new KMI record for an individual account
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Individual account with Record type "Accounts,A,10" First name "Accounts,L,10" Last name "Accounts,M,10" Region "Accounts,N,10" Email "Accounts,K,10"
    Then Navigate to the KMI section and click on new
    And Enter all the mandatory information like Contact "Accounts,B,4" Product Offering "Accounts,H,4" Brand "Accounts,C,4" and Model "Accounts,D,4" information and save it
    And Verify the added new KMI record
    #Then Logout from the SF SVO Portal
    
  #An entry of newly created KMI account with any other contact details should not be shown under KMI section
  @SVO @SVO_042 @SSEC-313 
  Scenario: An entry of newly created KMI account with any other contact details should not be shown under KMI section
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then User Searches for account "Accounts,E,3" and selects it
    Then Navigate to the KMI section and click on new
    And Enter all the mandatory information like Contact "Accounts,B,3" Product Offering "Accounts,H,3" Brand "Accounts,C,3" and Model "Accounts,D,3" information and save it
    And Verify the no new KMI record is added
    #Then Logout from the SF SVO Portal
    
  #User is able to view details of an order into Orders Section, which is placed by an Opportunity using an individual account
  @SVO @SVO_043 @SSEC-314 
  Scenario: User is able to view details of an order into Orders Section, which is placed by an Opportunity using an individual account
    #Given Access SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    And User searches for a User Account "New Enquiry,AA,2"
    Then User Navigates to the User Account "New Enquiry,AA,2"
    And User Navigates to Orders Section
    Then Verify Order Details under orders section
    #Then Logout from the SVO Portal
    
  #User is able to create a new order with any record type for an individual account
  @SVO @SVO_044 @SSEC-315 
  Scenario: User is able to create a new order with any record type for an individual account
    #Given Access to SVO Portal
    Given User access SVO Portal
    Given User logins with Special Ops Data Manager role
    When User Navigate to Accounts
    And User searches for a User Account "New Enquiry,AA,2"
    Then User Navigates to the User Account "New Enquiry,AA,2"
    And User Navigates to Orders Section
    And User Creates a New Order
    And User Saves the New order
    And User Deletes the Order
    #Then Logout from the SF SVO Portal
    
  #User is able to add a note to an individual account into Notes section
  @SVO @SVO_045 @SSEC-316 
  Scenario: User is able to add a note to an individual account into Notes section
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Individual account with Record type "Accounts,A,10" First name "Accounts,L,10" Last name "Accounts,M,10" Region "Accounts,N,10" Email "Accounts,K,10"
    Then Navigate to the Notes section and click on New
    Then Create a new Note and Save it
    #Then Logout from the SF SVO Portal
    
  #User is able to view Person Account History for an individual account
  @SVO @SVO_046 @SSEC-317 
  Scenario: User is able to view Person Account History for an individual account
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Individual account with Record type "Accounts,A,10" First name "Accounts,L,10" Last name "Accounts,M,10" Region "Accounts,N,10" Email "Accounts,K,10"
    Then Navigate to the Person Account History section and views Person Account History for an individual account
    #Then Logout from the SF SVO Portal
    
  #User is able to update consent for marketing communication for an individual account
  @SVO @SVO_047 @SSEC-318 
  Scenario: User is able to update consent for marketing communication for an individual account
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then Create new Individual account with Record type "Accounts,A,10" First name "Accounts,L,10" Last name "Accounts,M,10" Region "Accounts,N,10" Email "Accounts,K,10"
    Then Click on Update Consent
    Then Update the consent as required and Save
    #Then Logout from the SF SVO Portal
    
  #User is able to search recently created individual account in Contacts Menu
  @SVO @SVO_048 @SSEC-319 
  Scenario: User is able to search recently created individual account in Contacts Menu
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then User Searches for account "Accounts,E,5" and selects it
    When User Navigate to Contacts tab
    Then User Searches for Contacts "Accounts,E,5" and selects it
    And Verify the contact information Account name "Accounts,E,5" and email ID "Accounts,K,5"
    #Then Logout from the SF SVO Portal
    
  #User is able to change an owner of an individual account
  @SVO @SVO_049 @SSEC-320 
  Scenario: User is able to change an owner of an individual account
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then User Searches for account "Accounts,E,6" and selects it
    When User clicks on the change Owner and change Owner as "Accounts,B,6"
    And User selects all the check boxes
    Then User clicks on submit button for Owner change
    #Then Logout from the SF SVO Portal
    
  #User changes the Person Account type or record type of an individual account
  @SVO @SVO_050 @SSEC-321 
  Scenario: User changes the Person Account type or record type of an individual account
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then User Searches for account "Accounts,E,7" and selects it
    When User clicks on change Record type and selects record type as "Accounts,A,7"
    Then User clicks save to proceed with the selected record type
    Then Logout from the SF SVO Portal
    
  #User can be able to give an access to any user from an individual account
  @SVO @SVO_051 @SSEC-322 @thirdBatch
  Scenario: User can be able to give an access to any user from an individual account
    Given Access to SVO Portal
    When User Navigate to Accounts
    Then Create new Individual account with Record type "Accounts,A,10" First name "Accounts,L,10" Last name "Accounts,M,10" Region "Accounts,N,10" Email "Accounts,K,10"
    Then Click on Sharing from the dropdown
    Then Update the access to any user and save it
    #Then Logout from the SF SVO Portal
    
  #User should not be able to search any account with invalid account name
  @SVO @SVO_052 @SSEC-323
  Scenario: User should not be able to search any account with invalid account name
    #Given Access to SVO Portal
    Given User access SVO Portal
    When User Navigate to Accounts
    Then User Searches for invalid account "testabc" and verifies the error
    #Then Logout from the SF SVO Portal
    
  #User is able to search and view details of an account by account name
  @SVO @SVO_053 @SSEC-324
  Scenario: User is able to search and view details of an account by account name
    Given Access to SVO Portal
    When User Navigate to Accounts
    Then Create new Individual account with Record type "Accounts,A,10" First name "Accounts,L,10" Last name "Accounts,M,10" Region "Accounts,N,10" Email "Accounts,K,10"
    Then Click on Edit
    Then Update the required details of the account
    Then Logout from the SF SVO Portal

  #User is able to create a new corporate account
  @SVO @SVO_054 @SSEC-325
  Scenario: User is able to create a new corporate account
    Given Access to SVO Portal
    When User Navigate to Accounts
    Then Create new Corporate account with Record type "Accounts,A,9" Account name "Accounts,E,9" Email "Accounts,K,10" Region "Accounts,N,10"
    Then Logout from the SF SVO Portal
    
 	#User does not enter phone number or email id while creating a new corporate account
  @SVO @SVO_055 @SSEC-326
  Scenario: User does not enter phone number or email id while creating a new corporate account
    Given Access to SVO Portal
    When User Navigate to Accounts
    Then Create new Corporate account with Record type "Accounts,A,9" Account name "Accounts,E,9" Region "Accounts,I,9" and without Phone or Email and verify the error
    Then Logout from the SF SVO Portal
    
  #User clicks on View Account Hierarchy before adding parent account
  @SVO @SVO_056 @SSEC-327
  Scenario: User clicks on View Account Hierarchy before adding parent account
    Given Access to SVO Portal
    When User Navigate to Accounts
    Then Create new Corporate account with Record type "Accounts,A,9" Account name "Accounts,E,9" Email "Accounts,K,10" Region "Accounts,N,10"
    Then Click on View Account Hierarchy and verify only current account is displayed
    Then Logout from the SF SVO Portal
    
  #User clicks on View Account Hierarchy after adding parent account
  @SVO @SVO_057 @SSEC-328
  Scenario: User clicks on View Account Hierarchy after adding parent account
    Given Access to SVO Portal
    When User Navigate to Accounts
    Then Create new Corporate account with Record type "Accounts,A,9" Account name "Accounts,E,9" Email "Accounts,K,10" Region "Accounts,N,10"
    Then User adds parent account in details section
    Then Click on View Account Hierarchy and verify both parent and current account is displayed
    Then Logout from the SF SVO Portal
    
  #User should be able to change account owner using Change owner option
  @SVO @SVO_058 @SSEC-329
  Scenario: User should be able to change account owner using Change owner option
    Given Access to SVO Portal
    When User Navigate to Accounts
    Then Create new Corporate account with Record type "Accounts,A,9" Account name "Accounts,E,9" Email "Accounts,K,10" Region "Accounts,N,10"
    When User clicks on the change Owner and change Owner as "Accounts,B,5"
    And User selects all the check boxes
    Then User clicks on submit button for Owner change
    Then Logout from the SF SVO Portal
    
  #User creates a new SAP Account for an Corporate account
  @SVO @SVO_059 @SSEC-330
  Scenario: User creates a new SAP Account for an Corporate account
    Given Access to SVO Portal
    When User Navigate to Accounts
    Then Create new Corporate account with Record type "Accounts,A,9" Account name "Accounts,E,9" Email "Accounts,H,9" Region "Accounts,I,9"
    Then Navigate to the SAP Accounts section and click on New
    Then Enter details in new SAP Account and Save it
    Then Logout from the SF SVO Portal
    
  #User can be able to create multiple SAP Account for the same Corporate account
  @SVO @SVO_060 @SSEC-331
  Scenario: User can be able to create multiple SAP Account for the same Corporate account
    Given Access to SVO Portal
    When User Navigate to Accounts
    Then Create new Corporate account with Record type "Accounts,A,9" Account name "Accounts,E,9" Email "Accounts,K,10" Region "Accounts,N,10"
    Then Navigate to the SAP Accounts section and click on New
    Then Enter details in new SAP Account and Save it
    Then Click on New SAP Account
    Then Enter details in new SAP Account and Save it
    Then Logout from the SF SVO Portal
    
  #User can add a New Contact from Related Contacts link
  @SVO @SVO_061 @SSEC-332
  Scenario: User can add a New Contact from Related Contacts link
    Given Access to SVO Portal
    When User Navigate to Accounts
    Then Create new Corporate account with Record type "Accounts,A,9" Account name "Accounts,E,9" Email "Accounts,K,10" Region "Accounts,N,10"
    Then Navigate to the Related contacts section and click on New contact
    Then Enter details for New "Corporate Contact" like first name "Accounts,L,2" last name "Accounts,M,2" email "Accounts,K,5" and Save it
    Then Logout from the SF SVO Portal

  #User can add a Relationship from Related Contacts link
  @SVO @SVO_062 @SSEC-333
  Scenario: User can add a Relationship from Related Contacts link
    Given Access to SVO Portal
    When User Navigate to Accounts
    Then Create new Corporate account with Record type "Accounts,A,9" Account name "Accounts,E,9" Email "Accounts,K,10" Region "Accounts,N,10"
    Then Navigate to the Related contacts section and click on add Relationship
    Then Enter the Relationship contact information "Accounts,B,2" and save it
    Then Logout from the SF SVO Portal

  #User can add New vehicle ownership using Vehicles owned link
  @SVO @SVO_063 @SSEC-334
  Scenario: User can add New vehicle ownership using Vehicles owned link
    Given Access to SVO Portal
    When User Navigate to Accounts
    Then Create new Corporate account with Record type "Accounts,A,9" Account name "Accounts,E,9" Email "Accounts,H,9" Region "Accounts,I,9"
    Then Navigate to the Vehicles Owned section and click on New owner record type
    Then Enter details in new Vehicles owned and Save it
    Then Logout from the SF SVO Portal

  #User can create an enquiry using Enquiries link
  @SVO @SVO_064 @SSEC-335
  Scenario: User can create an enquiry using Enquiries link
    Given Access to SVO Portal
    When User Navigate to Accounts
    Then Create new Corporate account with Record type "Accounts,A,9" Account name "Accounts,E,9" Email "Accounts,K,10" Region "Accounts,N,10"
    Then Navigate to Enquiries section and click on New enquiry
    Then Enter the details "Enquiry title" in new enquiry and save it
    Then Logout from the SF SVO Portal

  #User can create new SVO client Opportunities for a Corporate account
  @SVO @SVO_065 @SSEC-336
  Scenario: User can create new SVO client Opportunities for a Corporate account
    Given Access to SVO Portal
    When User Navigate to Accounts
    Then Create new Corporate account with Record type "Accounts,A,9" Account name "Accounts,E,9" Email "Accounts,H,9" Region "Accounts,I,9"
    Then Navigate to the SVO Client Opportunities section and click on New SVO "SVO Bespoke" opportunity
    Then Enter Required details in New Opportunity like Product Offering "Opportunity,B,2" Source "Opportunity,C,2" Account Name "Opportunity,D,2" Region "Opportunity,E,2" Preferred Contact "Opportunity,H,2" Brand "Opportunity,I,2" and Model "Opportunity,J,2"
    Then Logout from the SF SVO Portal
    
  #User can create a new opportunity with Classic Record type for a corporate account
  @SVO @SVO_066 @SSEC-337
  Scenario: User can create a new opportunity with Classic Record type for a corporate account
    Given Access to SVO Portal
    When User Navigate to Accounts
    Then Create new Corporate account with Record type "Accounts,A,9" Account name "Accounts,E,9" Email "Accounts,H,9" Region "Accounts,I,9"
    Then Navigate to the SVO Client Opportunities section and click on New Classic "Classic service" opportunity
    Then Enter Required details in New Opportunity like Product Offering "Opportunity,B,2" Source "Opportunity,C,2" Account Name "Opportunity,D,2" Region "Opportunity,E,2" Preferred Contact "Opportunity,H,2" Brand "Opportunity,I,2" and Model "Opportunity,J,2"
    Then Logout from the SF SVO Portal
    
  #User can create a new Order for a Corporate account
  @SVO @SVO_67 @SSEC-338
  Scenario: User can create a new Order for a Corporate account
    Given Access to SVO Portal
    When User Navigate to Accounts
    And User searches for a User Account "New Enquiry,AA,3"
    Then User Navigates to the User Account "New Enquiry,AA,3"
    And User Navigates to Orders Section
    And User Creates a New Order
    And User Saves the New order
    And User Deletes the Order
    Then Logout from the SF SVO Portal
    
  #User is able to add a note to a corporate account into Notes section
  @SVO @SVO_068 @SSEC-339
  Scenario: User is able to add a note to a corporate account into Notes section
    Given Access to SVO Portal
    When User Navigate to Accounts
    Then Create new Corporate account with Record type "Accounts,A,9" Account name "Accounts,E,9" Email "Accounts,K,9" Region "Accounts,N,9"
    Then Navigate to the Notes section and click on New
    Then Create a new Note with title "New title" and description "New desc" and Save it
    Then Logout from the SF SVO Portal
    
  #User is able to view Account History for a corporate account
  @SVO @SVO_069 @SSEC-340
  Scenario: User is able to view Account History for a corporate account
    Given Access to SVO Portal
    When User Navigate to Accounts
    Then Create new Corporate account with Record type "Accounts,A,9" Account name "Accounts,E,9" Email "Accounts,K,9" Region "Accounts,N,9"
    Then Navigate to the Account History section and view Account History for an Corporate account
    Then Logout from the SF SVO Portal
    
  #User should not be able to edit the Account History
  @SVO @SVO_070 @SSEC-341
  Scenario: User should not be able to edit the Account History
    Given Access to SVO Portal
    When User Navigate to Accounts
    Then Create new Corporate account with Record type "Accounts,A,9" Account name "Accounts,E,9" Email "Accounts,K,9" Region "Accounts,N,9"
    Then Navigate to the Account History section and verify user cannot edit the Account History for an Corporate account
    Then Logout from the SF SVO Portal

#In this scenario user tries to create classic opportunity for corporate account
@SVO @SVO_071 @SSEC-342
Scenario: User is able to create Opportunities (except SVO Bespoke and SVO Private Office) for a Corporate account
	Given Access to SVO Portal with username "LoginUsers,B,5" and password "LoginUsers,C,5"
  When User Navigate to Accounts
  And User searches for a User Account "Accounts,E,9" with AccountType "Accounts,U,2"
  Then User navigate to Opportunity Section
  And User creates a new Opportunity with Record Type "Opportunity,A,3" for Corporate Account
  Then Enter Required details in New Opportunity like Product Offering "Opportunity,B,2" Source "Opportunity,C,2" Account Name "Opportunity,D,5" Region "Opportunity,E,2" Preferred Contact "Opportunity,H,5" Brand "Opportunity,I,2" and Model "Opportunity,J,2" 
	Then Logout from the SF SVO Portal 
	
#In this Scenario User tries to delete Corporate Account
@SVO @SVO_072 @SSEC-343
Scenario: User can delete the Corporate account created
	Given Access to SVO Portal with username "LoginUsers,B,11" and password "LoginUsers,C,11"
	When User Navigate to Accounts
	Then Create new Corporate account with Record type "Accounts,A,9" Account name "Accounts,E,9" Email "Accounts,K,10" Region "Accounts,N,10" 
	And User tries to delete created Corporate Account
	Then Logout from the SF SVO Portal
	
 	#User should be able to spot difference between Individual and Corporate accounts
  @SVO @SVO_073 @SSEC-344
  Scenario: User should be able to spot difference between Individual and Corporate accounts
    Given Access to SVO Portal
    When User Navigate to Accounts
    Then Create new Individual account with Record type "Accounts,A,10" First name "Accounts,L,10" Last name "Accounts,M,10" Region "Accounts,N,10" Email "Accounts,K,10"
    Then Verify that Vehicles Driven link is available in Related List Quick links section for Individual account
    When User Navigate to Accounts
    Then Create new Corporate account with Record type "Accounts,A,9" Account name "Accounts,E,9" Email "Accounts,K,9" Region "Accounts,N,9"
    Then Verify that Vehicles Driven link is not available in Related List Quick links section for Corporate account
    Then Logout from the SF SVO Portal
 
 	#User tries to create a new Order for a Corporate account
  @SVO @SVO_205
  Scenario: User tries to create a new Order for a Corporate account
    Given Access to SVO Portal
    When User Navigate to Accounts
    Then User Searches for account "Accounts,E,10" and selects it
    And User Navigates to Orders Section
    Then Verify new button is not available to create a new order
    Then Logout from the SF SVO Portal
 
  #User tries to create a new SAP Account for an individual account
  #Users: Special Operations Data Manager
  @SVO @SVO_212
  Scenario: User tries to create a new SAP Account for an individual account
    Given Access to SVO Portal
    When User Navigate to Accounts
    Then User searches for "Accounts,O,2" Account from select list view
    When User click on first Account in the list
    And Verify the SAP Accounts link
    Then Logout from the SF SVO Portal
    
  #User tries to add new vehicle ownership as a driver to an individual account
  #Users : Bespoke user
  @SVO @SVO_213
  Scenario: User tries to add new vehicle ownership as a driver to an individual account
    Given Access to SVO Portal
    When User Navigate to Accounts
    Then User searches for "Accounts,O,2" Account from select list view
    When User click on first Account in the list
    Then Navigate to the Vehicles Owned section
    And Verify the New button to add relationship as a driver
    Then Logout from the SF SVO Portal

  #User can save any type of Reservation from Commissioning Suite menu
  @SVO @SVO_214
  Scenario: User can save any type of Reservation from Commissioning Suite menu
    Given Access to SVO Portal
    When User Navigate to Commissioning Suite
    And User selects time slot "Accounts,P,3" for Reservation
    Then fill all mandatory fields of Reservation like Status "Accounts,Q,2" CommissioningRequest "Accounts,R,2" PrimaryContact "Accounts,B,7" MeetingType "Accounts,S,2" MaxAttendees "Accounts,T,2"
    Then User save the Reservation
    Then Logout from the SF SVO Portal
    
  #User tries to create a new order with any record type for an individual account
  #Classic sales admin login
  @SVO @SVO_215
  Scenario: User tries to create a new order with any record type for an individual account
    Given Access to SVO Portal
    When User Navigate to Accounts
    And User searches for a User Account "New Enquiry,AA,2"
    Then User Navigates to the User Account "New Enquiry,AA,2"
    And User Navigates to Orders Section
    Then Verify new button is not available to create a new order
    Then Logout from the SF SVO Portal

  #User tries to update consent for marketing communication for an individual account
  #Special Operations Data Manager/SVO Bespoke Data Manager login
  @SVO @SVO_216
  Scenario: User tries to update consent for marketing communication for an individual account
    Given Access to SVO Portal
    When User Navigate to Accounts
    And User searches for a User Account "New Enquiry,AA,2"
    Then User Navigates to the User Account "New Enquiry,AA,2"
    Then Verify Update Consent button is not available for the user
    Then Logout from the SF SVO Portal

  #User tries to change the Person Account type or record type of an individual account
  #User Role : Special Operations Data Manager
  @SVO @SVO_217
  Scenario: User tries to change the Person Account type or record type of an individual account
    Given Access to SVO Portal
    Then set up the user role login "User Role,B,7"
    When User Navigate to Accounts
    Then User searches for "Accounts,O,2" Account from select list view
    When User click on first Account in the list
    And User Navigate to Change Record Type section
    Then Verify the access denied error message
    Then Logout from the SF SVO Portal

  #User tries to create a new SAP Account and add New Vehicle Ownership for an Corporate account
  # User Role : SVO Bespoke
  @SVO @SVO_218
  Scenario: User tries to create a new SAP Account and add New Vehicle Ownership for an Corporate account
    Given Access to SVO Portal
    Then set up the user role login "User Role,B,8"
    When User Navigate to Accounts
    Then User searches for "Accounts,O,3" Account from select list view
    When User click on first Account in the list
    And Verify the SAP Accounts link
    Then Logout from the SF SVO Portal
    
  #User tries to create new SVO client Opportunities for a Corporate account
  #Classic service role
  @SVO @SVO_220 @SSEC-491
  Scenario: User tries to create new SVO client Opportunities for a Corporate account
    Given Access to SVO Portal
    When User Navigate to Accounts
    Then User Searches for account "Accounts,E,10" and selects it
    Then Navigate to the SVO Client Opportunities section and click on New and verify SVO Bespoke record type is not available
    Then Logout from the SF SVO Portal
    
  #User tries to create a new opportunity with Classic Record type for a corporate account
  #SVO Bespoke role
  @SVO @SVO_221 @SSEC-492
  Scenario: User tries to create a new opportunity with Classic Record type for a corporate account
    Given Access to SVO Portal
    When User Navigate to Accounts
    Then User Searches for account "Accounts,E,10" and selects it
    Then Navigate to the Opportunities section and click on New and verify Classic record type is not available
    Then Logout from the SF SVO Portal
    
    #User cannot update consent for an Individual Account
		#Private Office role
		@SVO @SVO_230
		Scenario: User cannot update consent for an Individual Account
			Given Access to SVO Portal
			When User Navigate to Accounts
			Then User Searches for account "Accounts,E,5" and selects it
			Then Verify Update Consent button is not available for the user
			Then Logout from the SF SVO Portal

    
    