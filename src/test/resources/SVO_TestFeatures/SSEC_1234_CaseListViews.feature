Feature: Validations for Case List Views

  #User is able to verify all fields added on 'All cases' List view from Cases page
  @SSEC_1234_CaseListViews_01 @Automation_SSEC-1717
  Scenario: User is able to verify all fields added on 'All cases' List view from Cases page
    Given Login to the Salesforce Portal by Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User Navigate to the Cases Tab	
    Then Verify "CaseCreation,M,2" cases list view on cases tab
    And Verify fields like CaseNumber Subject Status CaseOwner Priority added to the list view
    Then Logouts from the Salesforce Portal
						
  #User is able to verify all fields added on 'All Open cases' List view from Cases page
  @SSEC_1234_CaseListView_02 @Automation_SSEC-1718
  Scenario: User is able to verify all fields added on 'All Open cases' List view from Cases page
    Given Login to Salesforce Portal by Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User Navigate to the Cases Tab
    Then Verify "CaseCreation,M,3" cases list view on cases tab
    And Verify fields like CaseNumber Subject Status CaseOwner Priority added to the list view
    Then Logouts from the Salesforce Portal

  #User is able to verify all fields added on 'All Closed cases' List view from Cases page
  @SSEC_1234_CaseListView_03 @Automation_SSEC-1719
  Scenario: User is able to verify all fields added on 'All Closed cases' List view from Cases page
    Given Login to Salesforce Portal by Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User Navigate to the Cases Tab
    Then Verify "CaseCreation,M,4" cases list view on cases tab
    And Verify fields like CaseNumber Subject Status CaseOwner Priority added to the list view
    Then Logouts from the Salesforce Portal

  #User is able to verify all fields added on 'Cases Due Closure' List view from Cases page
  @SSEC_1234_CaseListView_04 @Automation_SSEC-1720
  Scenario: User is able to verify all fields added on 'Cases Due Closure' List view from Cases page
    Given Login to the Salesforce Portal by Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User Navigate to the Cases Tab
    Then Verify "CaseCreation,M,5" cases list view on cases tab
    And Verify fields like CaseNumber Subject Status CaseOwner Priority added to the list view
    Then Logouts from the Salesforce Portal

  #User is able to verify all fields added on 'My Cases' List view from Cases page
  @SSEC_1234_CaseListView_05 @Automation_SSEC-1721
  Scenario: User is able to verify all fields added on 'My Cases' List view from Cases page
    Given Login to Salesforce Portal by Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User Navigate to the Cases Tab
    Then Verify "CaseCreation,M,6" cases list view on cases tab
    And Verify fields like CaseNumber Subject Status CaseOwner Priority added to the list view
    Then Logouts from the Salesforce Portal

  #User is able to verify all fields added on 'Customer Service - All Cases' List view from Cases page
  @SSEC_1234_CaseListView_06 @Automation_SSEC-1722
  Scenario: User is able to verify all fields added on 'Customer Service - All Cases' List view from Cases page
    Given Login to the Salesforce Portal by Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User Navigate to the Cases Tab
    Then Verify "CaseCreation,M,7" cases list view on cases tab
    And Verify fields like CaseNumber Subject Status CaseOwner Priority added to the list view
    Then Logouts from the Salesforce Portal
      
  #User is able to verify all fields added on 'Customer Service  My Cases' List view from Cases page
  @SSEC_1234_CaseListView_07 @Automation_SSEC-1723
  Scenario: User is able to verify all fields added on 'Customer Service  My Cases' List view from Cases page
    Given Login to the Salesforce Portal by Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User Navigate to the Cases Tab
    Then Verify "CaseCreation,M,8" cases list view on cases tab
    And Verify fields like CaseNumber Subject Status CaseOwner Priority added to the list view
    Then Logouts from the Salesforce Portal

   #User is able to verify all fields added on 'Parts & Technical - All Cases' List view from Cases page
  @SSEC_1234_CaseListView_08 @Automation_SSEC-1724
  Scenario: User is able to verify all fields added on 'Parts & Technical - All Cases' List view from Cases page
    Given Login to Salesforce Portal by Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User Navigate to the Cases Tab
    Then Verify "CaseCreation,M,9" cases list view on cases tab
    And Verify fields like CaseNumber Subject Status CaseOwner Priority added to the list view
    Then Logouts from the Salesforce Portal
    
  #User is able to verify all fields added on 'Parts & Technical -  Cases Due Closure' List view from Cases page
  @SSEC_1234_CaseListView_09 @Automation_SSEC-1725
  Scenario: User is able to verify all fields added on 'Parts & Technical -  Cases Due Closure' List view from Cases page
    Given Login to Salesforce Portal by Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User Navigate to the Cases Tab
    Then Verify "CaseCreation,M,10" cases list view on cases tab
    And Verify fields like CaseNumber Subject Status CaseOwner Priority added to the list view
    Then Logouts from the Salesforce Portal
    
  #User is able to verify all fields added on 'Parts & Technical  My Cases' List view from Cases page
  @SSEC_1234_CaseListView_10 @Automation_SSEC-1726
  Scenario: User is able to verify all fields added on 'Parts & Technical  My Cases' List view from Cases page
    Given Login to Salesforce Portal by Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User Navigate to the Cases Tab
    Then Verify "CaseCreation,M,11" cases list view on cases tab
    And Verify fields like CaseNumber Subject Status CaseOwner Priority added to the list view
    Then Logouts from the Salesforce Portal
    
  #User is not able to view All Parts and Technical Closed Cases 
  @SSEC_1234_CaseListView_11 @Automation_SSEC-1727
  Scenario: User is not able to view All Parts and Technical Closed Cases .
    Given Login to the Salesforce Portal by Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User Navigate to the Cases Tab
    Then Verify that user is not able to view All Parts and Technical Closed Cases
    Then Logouts from the Salesforce Portal
    
  #User is not able to view Cases List view if Cases Page not available.
  @SSEC_1234_CaseListView_12 @Automation_SSEC-1728
  Scenario: User is not able to link Contact to Customer Service case if Cases page is not available
    Given Login to Salesforce Portal with Classic Sales user with the Username "LoginUsers,B,5" and Password "LoginUsers,C,5"
    And Verify that cases tab is not available so that user is not able to view Cases List view
    Then Logouts from the Salesforce Portal