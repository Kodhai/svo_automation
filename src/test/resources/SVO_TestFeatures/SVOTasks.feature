Feature: Validate SVO Tasks 

	#User tries to Change the Priority of an completed Task
	@SVO @SVO_240
	Scenario: User tries to Change the Priority of an completed Task
	Given Access SF_SVO Portal
  Then Navigate to Tasks tab
  And User selects New option to create new task
  Then fill all mandatory fields of Task with subject "Opportunity,AL,2" and click on Save
  Then User Mark task as completed
  And User tries to change the priority of completed task
  Then Logout from SF_SVO Portal

	#User tries to change the Due date of an completed Task
  #Tasks
  @SVO @SVO_241
  Scenario: User tries to change the Due date of an completed Task
    Given Access SF_SVO Portal
    Then Navigate to Tasks tab
    And Select "Opportunity,AC,6" Tasks from select list view
    When User select first task from the list
    And User change the Due Date of task to "Opportunity,AG,2"
    Then Logout from SF_SVO Portal

  #User tries to Edit the Task details under Tasks Section
  #User Role : SVO Bespoke Data Manager
  @SVO @SVO_242
  Scenario: User tries to Edit the Task details under Tasks Section
    Given Access SF_SVO Portal
    Then set up login to the user role "User Role,B,10"
    Then Navigate to Tasks tab
    And Select "Opportunity,AC,7" Tasks from select list view
    When User select first task from the list
    Then User Edit the selected task
    And Verify the Access denied error message
    Then Logout from SF_SVO Portal

  #User tries to Delete completed Task  under Tasks section
  #User Role: Special Operations Data Manager
  @SVO @SVO_243
  Scenario: User tries to Delete completed Task  under Tasks section
    Given Access SF_SVO Portal
    Then set up login to the user role "User Role,B,7"
    Then Navigate to Tasks tab
    And Select "Opportunity,AC,7" Tasks from select list view
    When User select first task from the list
    And User assign the Task to "Opportunity,AE,2"
    Then User Mark task as completed
    When User delete the selected Task
    And Verify the Access denied error message to delete the Task
    Then Logout from SF_SVO Portal

  #User tries to create New Task
  #Tasks
  @SVO @SVO_244
  Scenario: User tries to create New Task
    Given Access SF_SVO Portal
    Then Navigate to Tasks tab
    And User selects New option to create new task
    Then fill all mandatory fields of Task with subject "Opportunity,AL,2" and click on Save
    Then Logout from SF_SVO Portal

  #User tries to complete the Task
  #Tasks
  @SVO @SVO_245
  Scenario: User tries to complete the Task
    Given Access SF_SVO Portal
    Then Navigate to Tasks tab
    And Select "Opportunity,AC,7" Tasks from select list view
    When User select first task from the list
    Then User Mark task as completed
    Then Logout from SF_SVO Portal
