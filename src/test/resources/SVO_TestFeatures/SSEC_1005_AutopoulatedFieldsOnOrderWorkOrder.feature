Feature: SVO Validations on AutopoulatedFieldsOnOrderWorkOrder

  #User is able to view auto populated fields of the bespoke requirements on Orders & work Orders page while creating an Classic Reborn Opportunity
  @AutopoulatedFieldsOnOrderWorkOrder_01
  Scenario: User is able to view auto populated fields of the bespoke requirements on Orders & work Orders page while creating an Classic Reborn Opportunity
    Given Access to SVO Portal with Classic Service user with the Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    Then User navigates to the Opportunity tab
    And User create a Classic Reborn Opportunity with all fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then Verify that user is able to view bespoke requirement fields on Opportunity page
    Then User moves an opportunity to order placed stage
    And User Navigates to Orders page of an Opportunity
    Then Verify that user is able to view bespoke requirement fields on Order page
    And User Navigates to work order page of an created order
    Then Verify that user is able to view bespoke requirement fields on Work Order page
    Then User Logout from the SVO Portal

  #User is able to view auto populated fields of the bespoke requirements on Orders & work Orders page which is added after creating an Classic Bespoke Opportunity
  @AutopoulatedFieldsOnOrderWorkOrder_02
  Scenario: User is able to view auto populated fields of the bespoke requirements on Orders & work Orders page which is added after creating an Classic Bespoke Opportunity
    Given Access to SVO Portal with Classic Sales user with the Username "LoginUsers,B,5" and Password "LoginUsers,C,5"
    Then User navigates to the Opportunity tab
    And User create a Classic Bespoke Opportunity with all fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then Verify that user is able to view bespoke requirement fields on Opportunity page
    Then User moves an opportunity to order placed stage
    And User Navigates to Orders page of an Opportunity
    Then Verify that user is able to view bespoke requirement fields on Order page
    And User Navigates to work order page of an created order
    Then Verify that user is able to view bespoke requirement fields on Work Order page
    Then User Logout from the SVO Portal

  #User is able to view auto populated fields of the bespoke requirements on Order page which is added after creating an Classic works legend Opportunity
  @AutopoulatedFieldsOnOrderWorkOrder_03
  Scenario: User is able to view auto populated fields of the bespoke requirements on Order page which is added after creating an Classic works legend Opportunity
    Given Access to SVO Portal as Classic Admin user with Username "LoginUsers,B,13" and Password "LoginUsers,C,13"
    Then User navigates to the Opportunity tab
    And User create a Classic Works Legend Opportunity with fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User is able to enter all bespoke requirement field values on Opportunity page
    Then Verify that user is able to view bespoke requirement fields on Opportunity page
    Then User moves an opportunity to order placed stage
    And User Navigates to Orders page of an Opportunity
    Then Verify that user is able to view bespoke requirement fields on Order page
    And User Navigates to work order page of an created order
    Then Verify that user is able to view bespoke requirement fields on Work Order page
    Then User Logout from the SVO Portal

  #User is able to view auto populated fields of the bespoke requirements on Order page which is added after creating an Classic Service Opportunity
  @AutopoulatedFieldsOnOrderWorkOrder_04
  Scenario: User is able to view auto populated fields of the bespoke requirements on Order page which is added after creating an Classic Service Opportunity
    Given Access to SVO Portal with Classic Service user with the Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    Then User navigates to the Opportunity tab
    And User create a Classic Reborn Opportunity with fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User is able to enter all bespoke requirement field values on Opportunity page
    Then Verify that user is able to view bespoke requirement fields on Opportunity page
    Then User moves an opportunity to order placed stage
    And User Navigates to Orders page of an Opportunity
    Then Verify that user is able to view bespoke requirement fields on Order page
    And User Navigates to work order page of an created order
    Then Verify that user is able to view bespoke requirement fields on Work Order page
    Then User Logout from the SVO Portal

  #User is able to view auto populated fields of the bespoke requirements on Orders & work Orders page which is added while creating a Classic Continuation Opportunity
  @AutopoulatedFieldsOnOrderWorkOrder_05
  Scenario: User is able to view auto populated fields of the bespoke requirements on Orders & work Orders page which is added while creating a Classic Continuation Opportunity
    Given Access to SVO Portal with Classic Sales user with the Username "LoginUsers,B,5" and Password "LoginUsers,C,5"
    Then User navigates to the Opportunity tab
    And User create a Classic Continuation Opportunity with all fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then Verify that user is able to view bespoke requirement fields on Opportunity page
    Then User moves an opportunity to order placed stage
    And User Navigates to Orders page of an Opportunity
    Then Verify that user is able to view bespoke requirement fields on Order page
    And User Navigates to work order page of an created order
    Then Verify that user is able to view bespoke requirement fields on Work Order page
    Then User Logout from the SVO Portal

  #User is able to Update the bespoke requirement fields of Classic Reborn Opportunity on opportunity page which has to flow to respective Order and Work Order
  @AutopoulatedFieldsOnOrderWorkOrder_06
  Scenario: User is able to Update the bespoke requirement fields of Classic Reborn Opportunity on opportunity page which has to flow to respective Order and Work Order
    Given Access to SVO Portal with Classic Service user with the Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    Then User navigates to the Opportunity tab
    And User create a Classic Reborn Opportunity with all fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then Verify that user is able to view bespoke requirement fields on Opportunity page
    Then User moves an opportunity to order placed stage
    And User Navigates to Orders page of an Opportunity
    Then Verify that user is able to view bespoke requirement fields on Order page
    And User Navigates to work order page of an created order
    Then Verify that user is able to view bespoke requirement fields on Work Order page
    And User Navigate Back to Opportunity Page
    Then User is able to enter all bespoke requirement field values on Opportunity page
    And User Navigates to Orders page of an Opportunity
    Then Verify that user is able to view bespoke requirement fields on Order page
    And User Navigates to work order page of an created order
    Then Verify that user is able to view bespoke requirement fields on Work Order page
    Then User Logout from the SVO Portal

  #User is able to Update the bespoke requirement fields of Closed lost Classic Continuation Opportunity on opportunity page which has to flow to respective Order and Work Order
  @AutopoulatedFieldsOnOrderWorkOrder_07
  Scenario: User is able to Update the bespoke requirement fields of Closed lost Classic Continuation Opportunity on opportunity page which has to flow to respective Order and Work Order
    Given Access to SVO Portal with Classic Service user with the Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    Then User navigates to the Opportunity tab
    And User create a Classic Continuation Opportunity with all fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then Verify that user is able to view bespoke requirement fields on Opportunity page
    Then User moves an opportunity to closed lost stage
    Then User is able to enter all bespoke requirement field values on Opportunity page
    Then Verify that user is able to view bespoke requirement fields on Opportunity page
    Then User Logout from the SVO Portal

  #User is able to Update the bespoke requirement fields of Closed lost Classic Continuation Opportunity on opportunity page which has to flow to respective Order and Work Order
  @AutopoulatedFieldsOnOrderWorkOrder_08
  Scenario: User is able to Update the bespoke requirement fields of Closed lost Classic Continuation Opportunity on opportunity page which has to flow to respective Order and Work Order
    Given Access to SVO Portal with Classic Sales user with the Username "LoginUsers,B,5" and Password "LoginUsers,C,5"
    Then User navigates to the Opportunity tab
    And User create a Classic Bespoke Opportunity with all fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then Verify that user is able to view bespoke requirement fields on Opportunity page
    Then User moves an opportunity to order placed stage
    Then User moves an opportunity to closed Won stage
    And User Navigates to Orders page of an Opportunity
    Then Verify that user is able to view bespoke requirement fields on Order page
    And User Navigates to work order page of an created order
    Then Verify that user is able to view bespoke requirement fields on Work Order page
    And User Navigate Back to Opportunity Page
    Then User is able to enter all bespoke requirement field values on Opportunity page
    And User Navigates to Orders page of an Opportunity
    Then Verify that user is able to view bespoke requirement fields on Order page
    And User Navigates to work order page of an created order
    Then Verify that user is able to view bespoke requirement fields on Work Order page
    Then User Logout from the SVO Portal

  #User is not able to view auto populated fields of the bespoke requirements on Orders page of an Private office Opportunity
  @AutopoulatedFieldsOnOrderWorkOrder_09
  Scenario: User is not able to view auto populated fields of the bespoke requirements on Orders page of an Private office Opportunity
    Given Access to SVO Portal by private office user with Username "LoginUsers,B,4" and Password "LoginUsers,C,4"
    Then User navigates to the Opportunity tab
    And User create a SVO Private Office Opportunity with Restricted fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,3" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,3" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User is able to view and select the Restricted Party Screening under details section of an private office opportunity with Restricted Party Screening stage "RestrictedPartyScreening,A,4"
    Then User moves an opportunity to Specification & quote stage
    And User close the opportunity after creating a private office Quote with all fields like Retailer Contact "Opportunity,J,3"
    And User Navigates to Orders page of an Opportunity
    Then Verify that user is not able to view bespoke requirement fields on Order page
    Then User Logout from the SVO Portal

  #User is not able to edit bespoke requirement fields of an Classic Bespoke Opportunity on respective Order page of an Opportunity
  @AutopoulatedFieldsOnOrderWorkOrder_10
  Scenario: User is not able to edit bespoke requirement fields of an Classic Bespoke Opportunity on respective Order page of an Opportunity
    Given Access to SVO Portal with Classic Service user with the Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    Then User navigates to the Opportunity tab
    And User create a Classic Bespoke Opportunity with all fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then Verify that user is able to view bespoke requirement fields on Opportunity page
    Then User moves an opportunity to order placed stage
    And User Navigates to Orders page of an Opportunity
    Then Verify that user is not able to edit bespoke requirement fields on Order page
    Then User Logout from the SVO Portal

  #User is not able to edit bespoke requirement fields of an Classic Bespoke Opportunity on respective Order page of an Opportunity
  @AutopoulatedFieldsOnOrderWorkOrder_11
  Scenario: User is not able to edit bespoke requirement fields of an Classic Bespoke Opportunity on respective Order page of an Opportunity
    Given Access to SVO Portal with Classic Sales user with the Username "LoginUsers,B,5" and Password "LoginUsers,C,5"
    Then User navigates to the Opportunity tab
    And User create a Classic Bespoke Opportunity with all fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then Verify that user is able to view bespoke requirement fields on Opportunity page
    Then User moves an opportunity to order placed stage
    And User Navigates to Orders page of an Opportunity
    Then Verify that user is able to view bespoke requirement fields on Order page
    And User Navigates to work order page of an created order
    Then Verify that user is not able to edit bespoke requirement fields on Work Order page
    Then User Logout from the SVO Portal

  #User is not able to view auto populated fields of the bespoke requirements on Orders page of an SVO Bespoke Opportunity
  @AutopoulatedFieldsOnOrderWorkOrder_12
  Scenario: User is not able to view auto populated fields of the bespoke requirements on Orders page of an SVO Bespoke Opportunity
    Given Access to SVO Portal by bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User create a SVO Bespoke Opportunity with check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User is able to verify the Restricted Party Screening under details section of an opportunity
    Then User moves an opportunity to Design Brief and Quote stage
    Then User moves an opportunity to Contract Signed stage from Design Brief and Quote
    And User moves an Oppotunity to Order Placed stage after accepting the Quote for Retailer Contact "Opportunity,J,2"
    And User Navigates to Orders page of an Opportunity
    Then Verify that user is not able to view bespoke requirement fields on Order page
    Then User Logout from the SVO Portal

  #User is able to add bespoke requirement fields of Classic Reborn Opportunity after order is placed
  @AutopoulatedFieldsOnOrderWorkOrder_13
  Scenario: User is able to add bespoke requirement fields of Classic Reborn Opportunity after order is placed
    Given Access to SVO Portal with Classic Sales user with the Username "LoginUsers,B,5" and Password "LoginUsers,C,5"
    Then User navigates to the Opportunity tab
    And User create a Classic Reborn Opportunity with fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then Verify that user is able to view bespoke requirement fields on Opportunity page
    Then User moves an opportunity to order placed stage
    Then User is able to enter all bespoke requirement field values on Opportunity page
    And User Navigates to Orders page of an Opportunity
    Then Verify that user is able to view bespoke requirement fields on Order page
    And User Navigates to work order page of an created order
    Then Verify that user is able to view bespoke requirement fields on Work Order page
    Then User Logout from the SVO Portal

  #User is not able to view auto populated fields of the bespoke requirements on Orders page of an SVO Bespoke Opportunity
  @AutopoulatedFieldsOnOrderWorkOrder_14
  Scenario: User is not able to view auto populated fields of the bespoke requirements on Orders page of an SVO Bespoke Opportunity
    Given Access to SVO Portal by bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User create a SVO Bespoke Opportunity with check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User is able to verify the Restricted Party Screening under details section of an opportunity
    And User moves an opportunity to Design Brief and Quote stage
    Then User moves an opportunity to Contract Signed stage from Design Brief and Quote
    And Verify that user is not able to view any Order on orders section of an opportunity
    Then User Logout from the SVO Portal
