Feature: Additional Vehicle to be added to an SVO Bespoke Opportunity

  @Sprint18 @AdditionalVehicle1 @SSEC-621
  Scenario: User is able to view different fields under Additional Vehicle section of an open opportunity
    Given Access to SVO Portal with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    When User navigates to Opportunity tab
    And User creates new SVO Bespoke Opportunity with mandatory fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User navigates to Additional Vehicle Link from Quick Links
    And User verifies that all different fields are available under Additional Vehicle Section
    Then Logout from SVO Portal

  @Sprint18 @AdditionalVehicle2 @SSEC-622
  Scenario: User is not able to view some fields under Additional Vehicle section of an opportunity
    #Given Access to SVO Portal with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Given User Access SVO Portal for Specific User Role
    When User navigates to Opportunity tab
    And User creates new SVO Bespoke Opportunity with mandatory fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User navigates to Additional Vehicle Link from Quick Links
    And User verifies that Order and Additional Vehicle Auto Number should not displayed under Additional Vehicle Section
    Then Logout from SVO Portal

  @Sprint18 @AdditionalVehicle3 @SSEC-623
  Scenario: Additional Vehicle hyperlink is present in related quick links list of an open Bespoke opportunity
    #Given Access to SVO Portal with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Given User Access SVO Portal for Specific User Role
    When User navigates to Opportunity tab
    And User creates new SVO Bespoke Opportunity with mandatory fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User navigates to Additional Vehicle Link from Quick Links
    And User adds one Vehicle Record with fields like Brand "Additional_Vehicles,A,3", Model "Additional_Vehicles,B,3", Series Name "Additional_Vehicles,C,3", Derivative "Additional_Vehicles,D,3" and Model Year "Additional_Vehicles,E,3"
    Then User navigates to Additional Vehicle hyperlink from Related Quick Links and verify necessary information displayed when mouse hovered
    Then Logout from SVO Portal

  @Sprint18 @AdditionalVehicle4 @SSEC-624
  Scenario: Additional Vehicle section is present in related quick links list of a closed Bespoke Opportunity
    Given Access to SVO Portal with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    #Given User Access SVO Portal for Specific User Role
    When User navigates to Opportunity tab
    And User create a SVO Bespoke Opportunity with Restricted party screening check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User closes the Opportunity
    Then User navigates to Additional Vehicle Link from Quick Links
    And User adds one Vehicle Record with fields like Brand "Additional_Vehicles,A,3", Model "Additional_Vehicles,B,3", Series Name "Additional_Vehicles,C,3", Derivative "Additional_Vehicles,D,3" and Model Year "Additional_Vehicles,E,3"
    Then User navigates to Additional Vehicle Section of an opportunity
    And User verifies that Brand, Model and Series Name is displayed in Mini page
    And User verifies that full vehicle information is displayed in Additional Vehicle page
    Then Logout from SVO Portal

  @Sprint18 @AdditionalVehicle5 @SSEC-629
  Scenario: Additional Vehicle section/hyperlink is not present for any Private Office Opportunity
    #Given Access to SVO Portal with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Given User Access SVO Portal for Specific User Role
    When User navigates to Opportunity tab
    And User opens an opportunity details for Private Office Opportunity
    Then User verifies that an Additional vehicle section or hyperlink is not present
    Then Logout from SVO Portal

  @Sprint18 @AdditionalVehicle6 @SSEC-628
  Scenario: User is not be able to take any actions on Additional vehicle Section in Orders page
    Given Access to SVO Portal with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    #Given User Access SVO Portal for Specific User Role
    When User navigates to Opportunity tab
    And User create a SVO Bespoke Opportunity with Restricted party screening check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User moves the opportunity to Design Brief & Quote stage
    And User moves Oppotunity to Order Placed stage after accepting Quote for Retailer Contact "Opportunity,J,2"
    Then User navigates to Additional Vehicle Link from Quick Links
    And User adds one Vehicle Record with fields like Brand "Additional_Vehicles,A,3", Model "Additional_Vehicles,B,3", Series Name "Additional_Vehicles,C,3", Derivative "Additional_Vehicles,D,3" and Model Year "Additional_Vehicles,E,3"
    Then User navigates to Additional Vehicle hyperlink from Related Quick Links
    And User verifies that single vehicle is created under Additional Vehicle section
    Then User navigates to Orders page for the same Opportunity for Verification
    Then User navigates to Additional Vehicle Section of an opportunity from Orders Page
    And User verifies that no action can be taken on Additional Vehicle Section from Orders page
    Then Logout from SVO Portal

  @Sprint18 @AdditionalVehicle7 @SSEC-630
  Scenario: User is not able to view Additional Vehicle section under files section of an Orders page for Private Office Opportunity
    #Given Access to SVO Portal with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Given User Access SVO Portal for Specific User Role
    When User navigates to Opportunity tab
    And User opens an opportunity details for Private Office Opportunity which is in "Order Placed" stage
    Then User navigates to Orders page for the same Opportunity
    Then User verifies that an Additional vehicle section or hyperlink is not present
    Then Logout from SVO Portal

  @Sprint18 @AdditionalVehicle8 @SSEC-631
  Scenario: User is able to add multiple vehicles to a single opportunity
    Given Access to SVO Portal with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    #Given User Access SVO Portal for Specific User Role
    When User navigates to Opportunity tab
    And User creates new SVO Bespoke Opportunity with mandatory fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User navigates to Additional Vehicle Link from Quick Links
    And User adds one Vehicle Record with mandatory fields like Brand "Additional_Vehicles,A,3", Model "Additional_Vehicles,B,3", Series Name "Additional_Vehicles,C,3", Derivative "Additional_Vehicles,D,3" and Model Year "Additional_Vehicles,E,3"
    Then User clicks on Save & New button to add one more vehicle record
    And User adds one Vehicle Record with fields like Brand "Additional_Vehicles,A,3", Model "Additional_Vehicles,B,3", Series Name "Additional_Vehicles,C,3", Derivative "Additional_Vehicles,D,3" and Model Year "Additional_Vehicles,E,3"
    Then User navigates to Additional Vehicle hyperlink from Related Quick Links
    And User verifies that multiple vehicles are created under Additional Vehicle section
    Then Logout from SVO Portal

  @Sprint18 @AdditionalVehicle9 @SSEC-632
  Scenario: User is able to add single vehicle after creating an opportunity
    #Given Access to SVO Portal with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Given User Access SVO Portal for Specific User Role
    When User navigates to Opportunity tab
    And User creates new SVO Bespoke Opportunity with mandatory fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User navigates to Additional Vehicle Link from Quick Links
    And User adds one Vehicle Record with fields like Brand "Additional_Vehicles,A,3", Model "Additional_Vehicles,B,3", Series Name "Additional_Vehicles,C,3", Derivative "Additional_Vehicles,D,3" and Model Year "Additional_Vehicles,E,3"
    Then User navigates to Additional Vehicle hyperlink from Related Quick Links
    And User verifies that single vehicle is created under Additional Vehicle section
    Then Logout from SVO Portal

  @Sprint18 @AdditionalVehicle10 @SSEC-633
  Scenario: User is able to navigate back to opportunity page on clicking cancel button of Additional vehicle window
    #Given Access to SVO Portal with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Given User Access SVO Portal for Specific User Role
    When User navigates to Opportunity tab
    And User creates new SVO Bespoke Opportunity with mandatory fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User navigates to Additional Vehicle Section to add new Vehicle Record
    And User tries to add one Vehicle Record with fields like Brand "Additional_Vehicles,A,3", Model "Additional_Vehicles,B,3", Series Name "Additional_Vehicles,C,3", Derivative "Additional_Vehicles,D,3" and Model Year "Additional_Vehicles,E,3"
    Then User clicks on Cancel button and verify that user navigate back to Opportunity page
    Then Logout from SVO Portal

  @Sprint18 @AdditionalVehicle11 @SSEC-635
  Scenario: User is able to view Additional vehicle information in orders page of an opportunity after quote is accepted
    #Given Access to SVO Portal with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Given User Access SVO Portal for Specific User Role
    When User navigates to Opportunity tab
    And User create a SVO Bespoke Opportunity with Restricted party screening check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User moves the opportunity to Design Brief & Quote stage
    Then User navigates to Additional Vehicle Link from Quick Links
    And User adds one Vehicle Record with fields like Brand "Additional_Vehicles,A,3", Model "Additional_Vehicles,B,3", Series Name "Additional_Vehicles,C,3", Derivative "Additional_Vehicles,D,3" and Model Year "Additional_Vehicles,E,3"
    And User moves Oppotunity to Order Placed stage after accepting Quote for Retailer Contact "Opportunity,J,2"
    Then User navigates to Orders page for the same Opportunity
    Then User navigates to Additional Vehicle Section of an opportunity from Orders Page
    And User verifies that full vehicle information is displayed in Additional Vehicle page of an Orders page
    Then Logout from SVO Portal

  @Sprint18 @AdditionalVehicle12 @SSEC-636
  Scenario: User is not able to view additional vehicle information in orders page of an opportunity, if quote is not accepted
    #Given Access to SVO Portal with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Given User Access SVO Portal for Specific User Role
    When User navigates to Opportunity tab
    And User creates new SVO Bespoke Opportunity with mandatory fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User moves the opportunity to Design Brief & Quote stage
    Then User navigates to Additional Vehicle Link from Quick Links
    And User adds one Vehicle Record with fields like Brand "Additional_Vehicles,A,3", Model "Additional_Vehicles,B,3", Series Name "Additional_Vehicles,C,3", Derivative "Additional_Vehicles,D,3" and Model Year "Additional_Vehicles,E,3"
    Then User creates a new Quote with mandatory fields like Retailer Contact "Opportunity,J,2"
    And User verifies that no order is created as the quote is not accepted
    Then Logout from SVO Portal

  @Sprint18 @AdditionalVehicle13 @SSEC-639
  Scenario: User is able to add new vehicle record to an opportunity post order creation under Additional vehicle section
    #Given Access to SVO Portal with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Given User Access SVO Portal for Specific User Role
    When User navigates to Opportunity tab
    And User create a SVO Bespoke Opportunity with Restricted party screening check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User moves the opportunity to Design Brief & Quote stage
    And User moves Oppotunity to Order Placed stage after accepting Quote for Retailer Contact "Opportunity,J,2"
    Then User navigates to Additional Vehicle Link from Quick Links
    And User adds one Vehicle Record with fields like Brand "Additional_Vehicles,A,3", Model "Additional_Vehicles,B,3", Series Name "Additional_Vehicles,C,3", Derivative "Additional_Vehicles,D,3" and Model Year "Additional_Vehicles,E,3"
    Then User navigates to Additional Vehicle hyperlink from Related Quick Links
    And User verifies that single vehicle is created under Additional Vehicle section
    Then Logout from SVO Portal

  @Sprint18 @AdditionalVehicle14 @SSEC-640
  Scenario: User is able to remove an existing vehicle record of an opportunity post order creation under Additional vehicle section
    #Given Access to SVO Portal with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Given User Access SVO Portal for Specific User Role
    When User navigates to Opportunity tab
    And User create a SVO Bespoke Opportunity with Restricted party screening check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User moves the opportunity to Design Brief & Quote stage
    And User moves Oppotunity to Order Placed stage after accepting Quote for Retailer Contact "Opportunity,J,2"
    Then User navigates to Additional Vehicle Link from Quick Links
    And User adds one Vehicle Record with fields like Brand "Additional_Vehicles,A,3", Model "Additional_Vehicles,B,3", Series Name "Additional_Vehicles,C,3", Derivative "Additional_Vehicles,D,3" and Model Year "Additional_Vehicles,E,3"
    Then User navigates to Additional Vehicle hyperlink from Related Quick Links
    Then User removes the vehicle record which is created and verify that it has been removed from the list
    Then Logout from SVO Portal

  @Sprint18 @AdditionalVehicle15 @SSEC-641
  Scenario: User is able to edit an existing vehicle record of an opportunity post order creation under Additional vehicle section
    #Given Access to SVO Portal with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Given User Access SVO Portal for Specific User Role
    When User navigates to Opportunity tab
    And User create a SVO Bespoke Opportunity with Restricted party screening check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User moves the opportunity to Design Brief & Quote stage
    And User moves Oppotunity to Order Placed stage after accepting Quote for Retailer Contact "Opportunity,J,2"
    Then User navigates to Additional Vehicle Link from Quick Links
    And User adds one Vehicle Record with fields like Brand "Additional_Vehicles,A,3", Model "Additional_Vehicles,B,3", Series Name "Additional_Vehicles,C,3", Derivative "Additional_Vehicles,D,3" and Model Year "Additional_Vehicles,E,3"
    Then User edits the vehicle record which is created like Model "Additional_Vehicles,B,2" and Model Year "Additional_Vehicles,E,2"
    And User verifies that the vehicle record has been updated with Model "Additional_Vehicles,B,2"
    Then Logout from SVO Portal

  @Sprint18 @AdditionalVehicle16 @SSEC-642
  Scenario: User is able to view same vehicle details on both opportunity page and on order page under Additional vehicles section
    #Given Access to SVO Portal with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Given User Access SVO Portal for Specific User Role
    When User navigates to Opportunity tab
    And User create a SVO Bespoke Opportunity with Restricted party screening check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User moves the opportunity to Design Brief & Quote stage
    And User moves Oppotunity to Order Placed stage after accepting Quote for Retailer Contact "Opportunity,J,2"
    Then User navigates to Additional Vehicle Link from Quick Links
    And User adds one Vehicle Record with fields like Brand "Additional_Vehicles,A,3", Model "Additional_Vehicles,B,3", Series Name "Additional_Vehicles,C,3", Derivative "Additional_Vehicles,D,3" and Model Year "Additional_Vehicles,E,3"
    Then User navigates to Additional Vehicle hyperlink from Related Quick Links
    And User verifies that single vehicle is created under Additional Vehicle section
    Then User navigates to Orders page for the same Opportunity
    Then User navigates to Additional Vehicle Section of an opportunity from Orders Page
    And User verifies that full vehicle information is displayed in Additional Vehicle page of an Orders page
    Then Logout from SVO Portal

  @Sprint18 @AdditionalVehicle17 @SSEC-634
  Scenario: User is  able to update the vehicle record on order page of an opportunity
    #Given Access to SVO Portal with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Given User Access SVO Portal for Specific User Role
    When User navigates to Opportunity tab
    And User create a SVO Bespoke Opportunity with Restricted party screening check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User moves the opportunity to Design Brief & Quote stage
    And User moves Oppotunity to Order Placed stage after accepting Quote for Retailer Contact "Opportunity,J,2"
    Then User navigates to Additional Vehicle Link from Quick Links
    And User adds one Vehicle Record with fields like Brand "Additional_Vehicles,A,3", Model "Additional_Vehicles,B,3", Series Name "Additional_Vehicles,C,3", Derivative "Additional_Vehicles,D,3" and Model Year "Additional_Vehicles,E,3"
    Then User navigates to Additional Vehicle hyperlink from Related Quick Links
    And User verifies that single vehicle is created under Additional Vehicle section
    Then User navigates to Orders page for the same Opportunity for Verification
    Then User navigates to Additional Vehicle Section of an opportunity from Orders Page
    Then User edits the vehicle record from Orders page which is created like Model "Additional_Vehicles,B,2" and Model Year "Additional_Vehicles,E,2"
    And Verify that same information flows to an Opportunity
    Then Logout from SVO Portal

  @Sprint18 @AdditionalVehicle18 @SSEC-637
  Scenario: User is not able to add new vehicle record to an opportunity under Additional vehicle section
    Given Access to SVO Portal with Private Office Username "LoginUsers,B,4" and Password "LoginUsers,C,4"
    When User navigates to Opportunity tab
    And User opens an opportunity details for SVO Bespoke Opportunity
    Then User navigates to Additional Vehicle Link from Quick Links
    Then User verifies that user is not able to create Additional vehicle Record for an opportunity
    Then Logout from SVO Portal
