@SSEC_2330
Feature: Validate Additional Vehicles Pricing

  @SSEC_2330_01 @sonica
  Scenario: User is able to create new Additional Vehicle on Opportunity tab
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User create a SVO Bespoke Opportunity with check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,4" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User navigates to Additional Vehicles from Quick Links
    And Creates a new Additional Vehicle
    Then Verify new additional vehicle is created
    Then User logouts from the portal

  @SSEC_2330_02 @sonica
  Scenario: User is able to verify Total Retail Price on the Opportunity tab
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    Then Clicks on Opportunity record
    And Verify Total Retail Price
    Then User logouts from the portal

  @SSEC_2330_03 @sonica
  Scenario: User is able to verify Total List Price on the Opportunity tab
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    Then Clicks on Opportunity record
    And Verify Total List Price
    Then User logouts from the portal

  @SSEC_2330_04 @sonica
  Scenario: User is able to verify Total Cost Price on the Opportunity Tab
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    Then Clicks on Opportunity record
    And Verify Total Cost Price
    Then User logouts from the portal

  @SSEC_2330_05 @sonica
  Scenario: User is able to edit Retail price on the Opportunity tab
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    Then Clicks on Opportunity record
    And Verify User is able to edit Retail price on the Opportunity tab
    Then User logouts from the portal
    
    @SSEC_2330_06 @sonica
  Scenario: User is able to mark stage as Complete on Opportunity Tab
    Given Access to SVO Portal as bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    Then Clicks on Opportunity record
    And Verify User is able to mark stage as Complete on Opportunity Tab
    Then User logouts from the portal
