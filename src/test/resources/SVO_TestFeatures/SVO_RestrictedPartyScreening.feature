Feature: SVO Restricted party screening

  @RestrictedPartyScreening_01 @SSEC-917 @RPS_Batch
  Scenario: User is able to progress SVO Bespoke Opportunity to Closed Won stage after clearing Restricted Party Screening
    Given Access to SVO Portal by bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User create a SVO Bespoke Opportunity with check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User is able to verify the Restricted Party Screening under details section of an opportunity
    Then User moves an opportunity to Design Brief and Quote stage
    Then User moves an opportunity to Contract Signed stage from Design Brief and Quote
    And User moves an Oppotunity to Order Placed stage after accepting the Quote for Retailer Contact "Opportunity,J,2"
    Then Logout from the Special vehicle operations Portal

  @RestrictedPartyScreening_02 @SSEC-918 @RPS_Batch
  Scenario: User is able to view all the value fields of Restricted Party Screening dropdown for SVO Private Office opportunity
    Given Access to SVO Portal by private office user with Username "LoginUsers,B,4" and Password "LoginUsers,C,4"
    #Given laterally Access SVO Portal by user name "LoginUsers,B,4" and Password "LoginUsers,C,4"
    Then User navigates to the Opportunity tab
    And User create a SVO Private Office Opportunity with Restricted fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,3" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,3" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User is able to view and select the Restricted Party Screening under details section of an private office opportunity with Restricted Party Screening stage "RestrictedPartyScreening,A,3"
    Then User moves an opportunity to Specification & quote stage
    And User close the opportunity after creating a private office Quote with all fields like Retailer Contact "Opportunity,J,3"
    Then Logout from the Special vehicle operations Portal

  @RestrictedPartyScreening_03 @SSEC-919 @RPS_Batch
  Scenario: User is not able to progress SVO Bespoke Opportunity to Closed Won stage by selecting 'Restricted’ as Restricted Party Screening
    Given Access to SVO Portal by bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    #Given laterally Access SVO Portal by user name "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User create a SVO Bespoke Opportunity with check cleared like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,4" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User is able to verify the Restricted Party Screening as restricted under details section of an opportunity
    Then User moves an opportunity to Design Brief and Quote stage
    Then Verify the error message while moving opportunity to Design Breief and Quote stage
    Then Logout from the Special vehicle operations Portal

  @RestrictedPartyScreening_04 @SSEC-920 @RPS_Batch
  Scenario: User is able to progress SVO Private Office Opportunity to Closed Won stage by selecting 'Check in Progress’ as Restricted Party Screening
    Given Access to SVO Portal by private office user with Username "LoginUsers,B,4" and Password "LoginUsers,C,4"
    #Given laterally Access SVO Portal by user name "LoginUsers,B,4" and Password "LoginUsers,C,4"
    Then User navigates to the Opportunity tab
    And User create a SVO Private Office Opportunity with Restricted fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,3" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,3" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User is able to view and select the Restricted Party Screening under details section of an private office opportunity with Restricted Party Screening stage "RestrictedPartyScreening,A,7"
    Then User moves an opportunity to Specification & quote stage
    And User close the opportunity after creating a private office Quote with all fields like Retailer Contact "Opportunity,J,3"
    Then Logout from the Special vehicle operations Portal

  @RestrictedPartyScreening_05 @SSEC-921 @RPS_Batch
  Scenario: User is not able to move SVO Bespoke Opportunity to Design Brief and Quote stage without clearing Restricted Party Screening
    Given Access to SVO Portal by bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    #Given laterally Access SVO Portal by user name "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User create a SVO Bespoke Opportunity with all fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User tries to move an opportunity stage to Design Brief and Quote stage without clearing Restricted party screening
    And Verify that user is not able to move the opportunity stage to Design Brief and Quote stage
    Then Logout from the Special vehicle operations Portal

  @RestrictedPartyScreening_06 @SSEC-922 @RPS_Batch
  Scenario: User is able to move SVO Private Office Opportunity to Closed won stage without clearing Restricted Party Screening
    Given Access to SVO Portal by private office user with Username "LoginUsers,B,4" and Password "LoginUsers,C,4"
    #Given laterally Access SVO Portal by user name "LoginUsers,B,4" and Password "LoginUsers,C,4"
    Then User navigates to the Opportunity tab
    And User create a SVO Private Office Opportunity with Restricted fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,3" Region "Opportunity,D,2" Client "Opportunity,G,2" Restricted Party Screening Stage "RestrictedPartyScreening,A,3" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Origination Divission "RestrictedPartyScreening,B,3" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User is able to view and select the Restricted Party Screening under details section of an private office opportunity with Restricted Party Screening stage "RestrictedPartyScreening,A,4"
    Then User moves an opportunity to Specification & quote stage
    And User close the opportunity after creating a private office Quote with all fields like Retailer Contact "Opportunity,J,3"
    Then Logout from the Special vehicle operations Portal

  @RestrictedPartyScreening_07 @SSEC-923 @RPS_Batch
  Scenario: User is able to progress SVO Bespoke Opportunity to Closed Won stage by selecting 'Customer unknown / Check not applicable’ from Restricted Party Screening
    Given Access to SVO admin Portal by Username "LoginUsers,B,6" and Password "LoginUsers,C,6"
    #Given laterally Access SVO Portal by user name "LoginUsers,B,6" and Password "LoginUsers,C,6"
    Then User navigates to the Opportunity tab
    And User create a SVO Bespoke Opportunity with all fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User selects the Restricted party screening as "RestrictedPartyScreening,A,5"
    Then User moves opportunity to Contract Signed stage
    Then User create a Design Brief Quote with all fields like Retailer Contact "Opportunity,J,2"
    Then User submit Design Brief Request and return the render pack
    Then User navigate back to Opportunity page
    And User moves an Oppotunity to Order Placed stage after accepting Quote for Retailer Contact "Opportunity,J,2"
    Then Logout from the Special vehicle operations Portal

  @RestrictedPartyScreening_08 @SSEC-924 @RPS_Batch
  Scenario: User is not able to view ‘Customer unknown / Check not applicable’ value field from Restricted Party Screening dropdown list for Classic Service opportunity
    Given Access to SVO Portal as Classic Service user with Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    #Given laterally Access SVO Portal by user name "LoginUsers,B,12" and Password "LoginUsers,C,12"
    Then User navigates to the Opportunity tab
    And User create a Classic Service Opportunity with all fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,7" Region "Opportunity,D,2" Client "Opportunity,G,2" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then Verify that user is not able to view the Customer unknown/check not applicable field from Restricted Party screening
    Then Logout from the Special vehicle operations Portal

  @RestrictedPartyScreening_09 @SSEC-925 @RPS_Batch
  Scenario: User is able to clear the Restricted Party Screening after creating SVO Bespoke Opportunity
    Given Access to SVO Portal by bespoke uat user with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    #Given laterally Access SVO Portal by user name "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then User navigates to the Opportunity tab
    And User create a SVO Bespoke Opportunity with all fields like Stage "Opportunity,B,2" Product Offering "Opportunity,C,2" Region "Opportunity,D,2" Client "Opportunity,G,2" Preferred Contact "Opportunity,H,2" Account Name "Opportunity,I,2" Retailer Contact "Opportunity,J,2" Source "Opportunity,K,2" Brand "Opportunity,L,2" Model "Opportunity,M,2"
    Then User moves an opportunity to Design Brief and Quote stage
    And Verify the user is not able to move the opportunity stage to Design Brief and Quote without clearing Restricted Party Screening
    Then User selects the Restricted party screening as "RestrictedPartyScreening,A,3"
    Then User moves an opportunity to Contract Signed stage from Design Brief and Quote stage
    Then User create a Design Brief Quote with all fields like Retailer Contact "Opportunity,J,2"
    Then User submit Design Brief Request and return the render pack
    Then User navigate back to Opportunity page
    And User moves an Oppotunity to Order Placed stage after accepting Quote for Retailer Contact "Opportunity,J,2"
    Then Logout from the Special vehicle operations Portal
