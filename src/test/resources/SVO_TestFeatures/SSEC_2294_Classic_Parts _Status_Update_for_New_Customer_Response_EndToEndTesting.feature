Feature: Validations on SSEC_2294 User stories

  #User creates the case with "New" status for Classic Parts and Technical
  @SSEC_2294_CaseCreation_1 @ClassicParts
  Scenario: User creates the case with "New" status for Classic Parts and Technical
    Given Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User navigates to Cases tab
    And User creates new Parts and technical record type with Case Origin as Email
    And User saves the Parts and Technical case
    Then User Verify the status as New
    Then User logouts from the SF SVO

  #User is able to move the case status to "Open" for Classic Parts and Technical
  @SSEC_2294_CaseCreation_2 @ClassicParts
  Scenario: User is able to move the case status to "Open" for Classic Parts and Technical
    Given Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User navigates to Cases tab
    And User creates new Parts and technical record type with Case Origin as Email
    And User saves the Parts and Technical case
    Then User Verify the status as Open
    Then User logouts from the SF SVO

  #User is able to move the closed case to open for Classic Parts and Technical
  @SSEC_2294_CaseCreation_3 @ClassicParts
  Scenario: User is able to move the closed case to open for Classic Parts and Technical
    Given Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User navigates to Cases tab
    And User creates new Customer Service record type with Case Origin as Website
    And User saves the Parts and Technical case
    Then User Verify the status as closed
    Then User logouts from the SF SVO

  #User creates the case with "New" status for Classic Customer Service
  @SSEC_2294_CaseCreation_4 @ClassicParts
  Scenario: User creates the case with "New" status for Classic Customer Service
    Given Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User navigates to Cases tab
    And User creates new Customer service record type with Case Origin as Email
    And User saves the Parts and Technical case
    Then User Verify the status as New
    Then User logouts from the SF SVO

  #User is able to move the case status to "Open" for Classic Customer Service
  @SSEC_2294_CaseCreation_5 @ClassicParts
  Scenario: User is able to move the case status to "Open" for Classic Customer Service
    Given Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User navigates to Cases tab
    And User creates new Customer service record type with Case Origin as Email
    And User saves the Parts and Technical case
    Then User Verify the status as Open
    Then User logouts from the SF SVO

  #User is able to move the closed case to open for Classic Customer Service
  @SSEC_2294_CaseCreation_6 @ClassicParts
  Scenario: User is able to move the closed case to open for Classic Customer Service
    Given Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User navigates to Cases tab
    And User creates new Customer service record type with Case Origin as Web
    And User saves the Parts and Technical case
    #Then User Verify the status as closed
    Then User logouts from the SF SVO
