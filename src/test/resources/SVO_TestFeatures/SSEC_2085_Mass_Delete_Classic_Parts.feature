@SSEC_2085
Feature: Validate Mass Delete for Classic Parts

  #User is able to create new Case for Parts and Technical
  @SSEC_2085_01 @sonica
  Scenario: User is able to create new Case for Parts and Technical
    Given Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User navigates to cases tab
    And User creates new Parts and Technical Record type case with mandatory fields like VIN
    Then User saves the case
    And User validates the new case created with the VIN
    And User logout from the portal

  #User is able to create new Case for Customer Service
  @SSEC_2085_02 @sonica
  Scenario: User is able to create new Case for Customer Service
    Given User login to SVO Portal by Classic Customer Service user with Username "LoginUsers, B, 21" and Password "LoginUsers, C, 21"
    When User navigates to cases tab
    And User creates new Customer services case with all mandatory fields
    Then User saves the case
    Then User validates new cust case
    And User logout from the portal

  #User is able to create new Case for Customer Service
  @SSEC_2085_03 @sonica
  Scenario: User is able to mass delete a case created for Parts and Technical
    Given Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User navigates to cases tab
    And Select a checkbox and click on Mass Delete button
    And User logout from the portal

  #User is able to create new Case for Customer Service
  @SSEC_2085_04 @sonica 
  Scenario: User is able to mass delete multiple cases for Parts and Technical
    Given Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User navigates to cases tab
    And Select multiple checkbox and click on Mass Delete button
    And User logout from the portal

  #User is able to mass delete multiple cases for Customer Service
  @SSEC_2085_04 @sonica
  Scenario: User is able to mass delete multiple cases for Customer Service
    Given User login to SVO Portal by Classic Customer Service user with Username "LoginUsers, B, 21" and Password "LoginUsers, C, 21"
    When User navigates to cases tab
    And Select multiple checkbox and click on Mass Delete button
    And User logout from the portal
