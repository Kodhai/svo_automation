Feature: Validations on Case visibility on SF

  #User is able to create and view Customer Service case on Salesforce.
  @SSEC_1235_CaseVisibilityInSF_001 @Automation_SSEC-1827
  Scenario: User is able to create and view Customer Service case on Salesforce.
    Given Login to Salesforce Portal using Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User Navigate to Cases Tab on SVO
    And User creates a new case with record type "CaseCreation,C,2" by linking case with existing Account "Opportunity,I,2" and Contact "Opportunity,J,2" details
    Then Verify the Case Origin of created case on SF
    And User Logouts from the SF SVO Portal

  #User is able to create and view Classic Parts and Technical case on Salesforce
  @SSEC_1235_CaseVisibilityInSF_002 @Automation_SSEC-1828
  Scenario: User is able to create and view Classic Parts and Technical case on Salesforce
    Given User logins to SVO Portal by Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User Navigate to Cases Tab on SVO
    And User creates a new case with record type "CaseCreation,C,3" by linking case with existing Account "Opportunity,I,2" and Contact "Opportunity,J,2" details
    Then Verify the Case Origin of created case on SF
    And User Logouts from the SF SVO Portal

  #User is able to view Classic Parts and Technical case created through email
  @SSEC_1235_CaseVisibilityInSF_003 @Automation_SSEC-1829
  Scenario: User is able to view Classic Parts and Technical case created through email
    Given User logins to email account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    When Customer send an case creation mail to user "CaseCreation,A,2"
    And User logouts from the user email account
    Then Verify User receives an outlook email notification for Parts and Technical case creation
    Given User access to SVO Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User Navigate to Cases Tab on SVO
    And User verifies that Parts and Technical case is automatically created on SVO Portal
    Then Verify the Case Origin of created case on SF
    And User Logouts from the SF SVO Portal

  #User is able to view Classic Customer service case created through Web form
  @SSEC_1235_CaseVisibilityInSF_004 @Automation_SSEC-1830
  Scenario: User is able to view Classic Customer service case created through Web form
    Given User logins to an Classic Parts portal
    Then User Navigates to the Contact Us section
    Then User creates Customer Service Case from web form with Email "CaseCreation,A,2" First name "CaseCreation,G,2" Last name "CaseCreation,H,2" Country "CaseCreation,I,2" Phone number "CaseCreation,K,2" VIN number "CaseCreation,J,2"
    Then Verify that user receives an outlook email notification on Classic Service case creation
    Given User logins to the SVO Portal by Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User Navigate to Cases Tab on SVO
    And User verifies that Classic Customer Service case is automatically created on SVO Portal
    Then Verify the Case Origin of created case on SF
    And User Logouts from the SF SVO Portal

  #User is able to change the record type of an Classic customer service case on Salesforce
  @SSEC_1235_CaseVisibilityInSF_005 @Automation_SSEC-1831
  Scenario: User is able to change the record type of an Classic customer service case on Salesforce
    Given Login to Salesforce Portal using Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User Navigate to Cases Tab on SVO
    And User creates a new case with record type "CaseCreation,C,2" by linking case with existing Account "Opportunity,I,2" and Contact "Opportunity,J,2" details
    Then Verify the Case Origin of created case on SF
    When User change the Record type of created case to "CaseCreation,C,3"
    Then User verifies the case record type on case details page
    And User Logouts from the SF SVO Portal

  #User is able to edit Parts and Technical case created on Salesforce
  @SSEC_1235_CaseVisibilityInSF_006 @Automation_SSEC-1832
  Scenario: User is able to edit Parts and Technical case created on Salesforce
    Given User logins to SVO Portal by Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User Navigate to Cases Tab on SVO
    And User creates a new case with record type "CaseCreation,C,3" by linking case with existing Account "Opportunity,I,2" and Contact "Opportunity,J,2" details
    Then Verify the Case Origin of created case on SF
    And User edit the created parts and technical details
    Then Verify the updated case details on Salesforce
    And User Logouts from the SF SVO Portal
	
  #User is not able to able to change the owner of an Classic customer service case to Bespoke user
  @SSEC_1235_CaseVisibilityInSF_007 @Automation_SSEC-1833
  Scenario: User is not able to able to change the owner of an Classic customer service case to Bespoke user
    Given Login to Salesforce Portal using Classic Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    When User Navigate to Cases Tab on SVO
    And User creates a new case with record type "CaseCreation,C,2" by linking case with existing Account "Opportunity,I,2" and Contact "Opportunity,J,2" details
    Then Verify the Case Origin of created case on SF
    When User tries to change the Owner of created case to "ItemsToApprove,A,3"
    Then Verify an error message caught for updating owner for the case record
    And User Logouts from the SF SVO Portal
