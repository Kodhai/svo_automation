
@SSEC_1567
Feature: Script to add Case Creation in Classic Sales and Classic Service user

  @CC01
  Scenario: User is able to create Classic Sales case and close it
    Given Access to SVO portal for Classic Sales as Username "LoginUsers,B,5" and password "LoginUsers,C,5"
    And user navigate to the Cases tab
    Then User creates Classic Sales record type with mandatory fields like firstName
    Then User saves new case created
    And Verify that Case record type is Classic Sales
    Then user logs a call on the Activity tab
    Then User Closes the case
    And User logouts from the SVO Portal
    
  @CC02
  Scenario: User is able to create Classic Service Case and close it
    Given Access to SVO portal for Classic Service as username "LoginUsers,B,12" and password "LoginUsers,C,12"
    And user navigate to the Cases tab
    Then User creates Classic Service record type with mandatory fields like firstName
    Then User saves new case created
    And Verify that Case record type is Classic Service
    Then user logs a call on the Activity tab
    Then User Closes the case
    And User logouts from the SVO Portal
    
  @CC03
  Scenario: User is able to create multiple cases for same record type
    Given Access to SVO portal for Classic Service as username "LoginUsers,B,12" and password "LoginUsers,C,12"
    And user navigate to the Cases tab
    Then User creates Classic Service record type with mandatory fields like firstName
    Then User click on Save and New button to save and create a new case with the same record type
    Then User validates and creates new Classic Service record type with mandatory fields like firstName
    Then User saves new case created
    And Verify that Case record type is Classic Service
    And User logouts from the SVO Portal






  
 