Feature: Validations for Classic Parts Email template for cases

  #User is able to verify '3D Secure' Email Template for Customer Service cases created on Salesforce
  @SSEC_1557_ClassicPartsCaseEmailTemplate_001 @SSEC-1936
  Scenario: User is able to verify '3D Secure' Email Template for Customer Service cases created on Salesforce
    Given User Access to the salesforce Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User navigate to Cases Tab
    And User creates a new case on SVO of record type "CaseCreation,C,2" by linking case with existing Account "Opportunity,I,2" and Contact "Opportunity,J,2" details
    Then User Navigates to Activity tab
    And User verify and send an email to customer "LoginUsers,B,17" with template "CaseCreation,N,2" on compose email section
    Then User Logouts from an SVO Portal

  #User is able to verify 'CLASSIC WORKS CUST SVC CONTACT' Email Template for Customer Service case created on Salesforce
  @SSEC_1557_ClassicPartsCaseEmailTemplate_002 @SSEC-1937
  Scenario: User is able to verify 'CLASSIC WORKS CUST SVC CONTACT' Email Template for Customer Service case created on Salesforce
    Given User Access to the salesforce Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User navigate to Cases Tab
    And User creates a new case on SVO of record type "CaseCreation,C,3" by linking case with existing Account "Opportunity,I,2" and Contact "Opportunity,J,2" details
    Then User Navigates to Activity tab
    And User verify and send an email to customer "LoginUsers,B,17" with template "CaseCreation,N,21" on compose email section
    Then User Logouts from an SVO Portal

  #User is able to verify 'DEALER CUST SVC NOT CLASSIC' Email Template for Classic Parts and Technical case created through web form
  @SSEC_1557_ClassicPartsCaseEmailTemplate_003 @SSEC-1938
  Scenario: User is able to verify 'DEALER CUST SVC NOT CLASSIC' Email Template for Classic Parts and Technical case created through web form
    Given Login to the Classic Parts portal
    Then User Navigated to Contact Us section
    Then User create an Parts and Technical Case from web form with Email "CaseCreation,A,2" First name "CaseCreation,G,2" Last name "CaseCreation,H,2" Country "CaseCreation,I,2" Phone number "CaseCreation,K,2" VIN number "CaseCreation,J,2"
    Then Verify that an user receives an outlook email notification on Parts and Technical case creation
    Given User login to the salesforce Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User navigate to Cases Tab
    Then User Verifies that Parts and Technical case is automatically created on SVO Portal
    Then User Navigates to Activity tab
    And User verify and send an email to customer "LoginUsers,B,17" with template "CaseCreation,N,6" on compose email section
    Then User Logouts from an SVO Portal

  #User is able to verify 'DELIVERY COSTS' Email Template for Classic Parts and Technical case created through web form
  @SSEC_1557_ClassicPartsCaseEmailTemplate_004 @SSEC-1939
  Scenario: User is able to verify 'DELIVERY COSTS' Email Template for Classic Parts and Technical case created through web form
    Given Login to the Classic Parts portal
    Then User Navigated to Contact Us section
    Then User create an Parts and Technical Case from web form with Email "CaseCreation,A,2" First name "CaseCreation,G,2" Last name "CaseCreation,H,2" Country "CaseCreation,I,2" Phone number "CaseCreation,K,2" VIN number "CaseCreation,J,2"
    Then Verify that an user receives an outlook email notification on Parts and Technical case creation
    Given User login to the salesforce Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User navigate to Cases Tab
    Then User Verifies that Parts and Technical case is automatically created on SVO Portal
    Then User Navigates to Activity tab
    And User verify and send an email to customer "LoginUsers,B,17" with template "CaseCreation,N,22" on compose email section
    Then User Logouts from an SVO Portal

  #User is able to verify 'JDHT ARCHIVE BUILD SHEET' Email Template for Customer Service case created on Salesforce
  @SSEC_1557_ClassicPartsCaseEmailTemplate_005 @SSEC-1940
  Scenario: User is able to verify 'JDHT ARCHIVE BUILD SHEET' Email Template for Customer Service case created on Salesforce
    Given User Access to the salesforce Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User navigate to Cases Tab
    And User creates a new case on SVO of record type "CaseCreation,C,3" by linking case with existing Account "Opportunity,I,2" and Contact "Opportunity,J,2" details
    Then User Navigates to Activity tab
    And User verify and send an email to customer "LoginUsers,B,17" with template "CaseCreation,N,7" on compose email section
    Then User Logouts from an SVO Portal

  #User is able to verify 'JDHT KEY NUMBERS' Email Template for Customer Service case created on Salesforce
  @SSEC_1557_ClassicPartsCaseEmailTemplate_006 @SSEC-1941
  Scenario: User is able to verify 'JDHT KEY NUMBERS' Email Template for Customer Service case created on Salesforce
    Given User Access to the salesforce Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User navigate to Cases Tab
    And User creates a new case on SVO of record type "CaseCreation,C,3" by linking case with existing Account "Opportunity,I,2" and Contact "Opportunity,J,2" details
    Then User Navigates to Activity tab
    And User verify and send an email to customer "LoginUsers,B,17" with template "CaseCreation,N,23" on compose email section
    Then User Logouts from an SVO Portal

  #User is able to verify 'KEYS AND CODES' Email Template for Customer Service case created on Salesforce
  @SSEC_1557_ClassicPartsCaseEmailTemplate_007 @SSEC-1942
  Scenario: User is able to verify 'KEYS AND CODES' Email Template for Customer Service case created on Salesforce
    Given User Access to the salesforce Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User navigate to Cases Tab
    And User creates a new case on SVO of record type "CaseCreation,C,3" by linking case with existing Account "Opportunity,I,2" and Contact "Opportunity,J,2" details
    Then User Navigates to Activity tab
    And User verify and send an email to customer "LoginUsers,B,17" with template "CaseCreation,N,8" on compose email section
    Then User Logouts from an SVO Portal

  #User is able to verify 'L322' Email Template for Customer Service cases created on Salesforce
  @SSEC_1557_ClassicPartsCaseEmailTemplate_008 @SSEC-1943
  Scenario: User is able to verify 'L322' Email Template for Customer Service cases created on Salesforce
    Given User Access to the salesforce Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User navigate to Cases Tab
    And User creates a new case on SVO of record type "CaseCreation,C,2" by linking case with existing Account "Opportunity,I,2" and Contact "Opportunity,J,2" details
    Then User Navigates to Activity tab
    And User verify and send an email to customer "LoginUsers,B,17" with template "CaseCreation,N,9" on compose email section
    Then User Logouts from an SVO Portal

  #User is able to verify 'NLA' Email Template for Classic Parts and Technical case created through web form
  @SSEC_1557_ClassicPartsCaseEmailTemplate_009 @SSEC-1944
  Scenario: User is able to verify 'NLA' Email Template for Classic Parts and Technical case created through web form
    Given Login to the Classic Parts portal
    Then User Navigated to Contact Us section
    Then User create an Parts and Technical Case from web form with Email "CaseCreation,A,2" First name "CaseCreation,G,2" Last name "CaseCreation,H,2" Country "CaseCreation,I,2" Phone number "CaseCreation,K,2" VIN number "CaseCreation,J,2"
    Then Verify that an user receives an outlook email notification on Parts and Technical case creation
    Given User login to the salesforce Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User navigate to Cases Tab
    Then User Verifies that Parts and Technical case is automatically created on SVO Portal
    Then User Navigates to Activity tab
    And User verify and send an email to customer "LoginUsers,B,17" with template "CaseCreation,N,10" on compose email section
    Then User Logouts from an SVO Portal

  #User is able to verify 'NOT IN RANGE' Email Template for Classic Parts and Technical case created through web form
  @SSEC_1557_ClassicPartsCaseEmailTemplate_010 @SSEC-1945
  Scenario: User is able to verify 'NOT IN RANGE' Email Template for Classic Parts and Technical case created through web form
    Given Login to the Classic Parts portal
    Then User Navigated to Contact Us section
    Then User create an Parts and Technical Case from web form with Email "CaseCreation,A,2" First name "CaseCreation,G,2" Last name "CaseCreation,H,2" Country "CaseCreation,I,2" Phone number "CaseCreation,K,2" VIN number "CaseCreation,J,2"
    Then Verify that an user receives an outlook email notification on Parts and Technical case creation
    Given User login to the salesforce Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User navigate to Cases Tab
    Then User Verifies that Parts and Technical case is automatically created on SVO Portal
    Then User Navigates to Activity tab
    And User verify and send an email to customer "LoginUsers,B,17" with template "CaseCreation,N,24" on compose email section
    Then User Logouts from an SVO Portal

  #User is able to verify 'PAINT CODES JAGUAR NON US' Email Template for Customer Service case created on Salesforce
  @SSEC_1557_ClassicPartsCaseEmailTemplate_011 @SSEC-1946
  Scenario: User is able to verify 'PAINT CODES JAGUAR NON US' Email Template for Customer Service case created on Salesforce
    Given User Access to the salesforce Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User navigate to Cases Tab
    And User creates a new case on SVO of record type "CaseCreation,C,3" by linking case with existing Account "Opportunity,I,2" and Contact "Opportunity,J,2" details
    Then User Navigates to Activity tab
    And User verify and send an email to customer "LoginUsers,B,17" with template "CaseCreation,N,12" on compose email section
    Then User Logouts from an SVO Portal

  #User is able to verify 'PAINT CODE LAND ROVER' Email Template for Customer Service cases created on Salesforce
  @SSEC_1557_ClassicPartsCaseEmailTemplate_012 @SSEC-1947
  Scenario: User is able to verify 'PAINT CODE LAND ROVER' Email Template for Customer Service cases created on Salesforce
    Given User Access to the salesforce Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User navigate to Cases Tab
    And User creates a new case on SVO of record type "CaseCreation,C,2" by linking case with existing Account "Opportunity,I,2" and Contact "Opportunity,J,2" details
    Then User Navigates to Activity tab
    And User verify and send an email to customer "LoginUsers,B,17" with template "CaseCreation,N,13" on compose email section
    Then User Logouts from an SVO Portal

  #User is able to verify 'PLEASE REMEMBER' Email Template for Classic Parts and Technical case created through web form
  @SSEC_1557_ClassicPartsCaseEmailTemplate_013 @SSEC-1948
  Scenario: User is able to verify 'PLEASE REMEMBER' Email Template for Classic Parts and Technical case created through web form
    Given Login to the Classic Parts portal
    Then User Navigated to Contact Us section
    Then User create an Parts and Technical Case from web form with Email "CaseCreation,A,2" First name "CaseCreation,G,2" Last name "CaseCreation,H,2" Country "CaseCreation,I,2" Phone number "CaseCreation,K,2" VIN number "CaseCreation,J,2"
    Then Verify that an user receives an outlook email notification on Parts and Technical case creation
    Given User login to the salesforce Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User navigate to Cases Tab
    Then User Verifies that Parts and Technical case is automatically created on SVO Portal
    Then User Navigates to Activity tab
    And User verify and send an email to customer "LoginUsers,B,17" with template "CaseCreation,N,14" on compose email section
    Then User Logouts from an SVO Portal

  #User is able to verify 'RADIO CODES' Email Template for Customer Service case created on Salesforce
  @SSEC_1557_ClassicPartsCaseEmailTemplate_014 @SSEC-1949
  Scenario: User is able to verify 'RADIO CODES' Email Template for Customer Service case created on Salesforce
    Given User Access to the salesforce Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User navigate to Cases Tab
    And User creates a new case on SVO of record type "CaseCreation,C,3" by linking case with existing Account "Opportunity,I,2" and Contact "Opportunity,J,2" details
    Then User Navigates to Activity tab
    And User verify and send an email to customer "LoginUsers,B,17" with template "CaseCreation,N,15" on compose email section
    Then User Logouts from an SVO Portal

  #User is able to verify 'VIN REQUEST' Email Template for Customer Service case created on Salesforce
  @SSEC_1557_ClassicPartsCaseEmailTemplate_015 @SSEC-1950
  Scenario: User is able to verify 'VIN REQUEST' Email Template for Customer Service case created on Salesforce
    Given User Access to the salesforce Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User navigate to Cases Tab
    And User creates a new case on SVO of record type "CaseCreation,C,3" by linking case with existing Account "Opportunity,I,2" and Contact "Opportunity,J,2" details
    Then User Navigates to Activity tab
    And User verify and send an email to customer "LoginUsers,B,17" with template "CaseCreation,N,16" on compose email section
    Then User Logouts from an SVO Portal

  #User is able to verify 'WEB LINK ORDER INSTRUCTIONS' Email Template for Customer Service cases created on Salesforce
  @SSEC_1557_ClassicPartsCaseEmailTemplate_016 @SSEC-1954
  Scenario: User is able to verify 'WEB LINK ORDER INSTRUCTIONS' Email Template for Customer Service cases created on Salesforce
    Given User Access to the salesforce Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User navigate to Cases Tab
    And User creates a new case on SVO of record type "CaseCreation,C,2" by linking case with existing Account "Opportunity,I,2" and Contact "Opportunity,J,2" details
    Then User Navigates to Activity tab
    And User verify and send an email to customer "LoginUsers,B,17" with template "CaseCreation,N,17" on compose email section
    Then User Logouts from an SVO Portal

  #User is able to verify 'WRONG WEB CONTACT' Email Template for Classic Parts and Technical case created through web form
  @SSEC_1557_ClassicPartsCaseEmailTemplate_017 @SSEC-1955
  Scenario: User is able to verify 'WRONG WEB CONTACT' Email Template for Classic Parts and Technical case created through web form
    Given Login to the Classic Parts portal
    Then User Navigated to Contact Us section
    Then User create an Parts and Technical Case from web form with Email "CaseCreation,A,2" First name "CaseCreation,G,2" Last name "CaseCreation,H,2" Country "CaseCreation,I,2" Phone number "CaseCreation,K,2" VIN number "CaseCreation,J,2"
    Then Verify that an user receives an outlook email notification on Parts and Technical case creation
    Given User login to the salesforce Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User navigate to Cases Tab
    Then User Verifies that Parts and Technical case is automatically created on SVO Portal
    Then User Navigates to Activity tab
    And User verify and send an email to customer "LoginUsers,B,17" with template "CaseCreation,N,18" on compose email section
    Then User Logouts from an SVO Portal

  #User is able to verify 'ARCHIVE AT BRITISH MOTOR MUSEUM' Email Template for Customer Service case created on Salesforce
  @SSEC_1557_ClassicPartsCaseEmailTemplate_018 @SSEC-1956
  Scenario: User is able to verify 'ARCHIVE AT BRITISH MOTOR MUSEUM' Email Template for Customer Service case created on Salesforce
    Given User Access to the salesforce Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User navigate to Cases Tab
    And User creates a new case on SVO of record type "CaseCreation,C,3" by linking case with existing Account "Opportunity,I,2" and Contact "Opportunity,J,2" details
    Then User Navigates to Activity tab
    And User verify and send an email to customer "LoginUsers,B,17" with template "CaseCreation,N,19" on compose email section
    Then User Logouts from an SVO Portal

  #User is able to verify 'HISTORIC BUILD SHEET' Email Template for Customer Service case created on Salesforce
  @SSEC_1557_ClassicPartsCaseEmailTemplate_019 @SSEC-1957
  Scenario: User is able to verify 'HISTORIC BUILD SHEET' Email Template for Customer Service case created on Salesforce
    Given User Access to the salesforce Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User navigate to Cases Tab
    And User creates a new case on SVO of record type "CaseCreation,C,3" by linking case with existing Account "Opportunity,I,2" and Contact "Opportunity,J,2" details
    Then User Navigates to Activity tab
    And User verify and send an email to customer "LoginUsers,B,17" with template "CaseCreation,N,20" on compose email section
    Then User Logouts from an SVO Portal

  #User is not able to send an Email with 'WRONG WEB CONTACT' template without mandatory details of email
  @SSEC_1557_ClassicPartsCaseEmailTemplate_020 @SSEC-1958
  Scenario: User is not able to send an Email with 'WRONG WEB CONTACT' template without mandatory details of email
    Given Login to the Classic Parts portal
    Then User Navigated to the Contact Us section
    Then User creates an Classic Parts and Technical Case from web form with Email "CaseCreation,A,2" First name "CaseCreation,G,2" Last name "CaseCreation,H,2" Country "CaseCreation,I,2" Phone number "CaseCreation,K,2" VIN number "CaseCreation,J,2"
    Then Verify that an user receives an outlook mail notification on Parts and Technical case creation
    Given User login to the salesforce Portal as Classic Parts and Technical user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    When User navigate to Cases Tab
    Then User Verifies that Parts and Technical case is automatically created on SVO Portal
    Then User Navigates to Activity tab
    And Verify that user is not able to send an email without madatory fields of email to customer "LoginUsers,B,17" with template "CaseCreation,N,18" on compose email section
    Then User Logouts from an SVO Portal
