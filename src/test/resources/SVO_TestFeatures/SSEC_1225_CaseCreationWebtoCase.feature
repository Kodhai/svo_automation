Feature: Script to add queue to to the Case in Salesforce

  @SSEC_1225_CaseCreationWebToCase_01
   Scenario: Submit a case for Customer service enquiry
    Given Access to the Classic Parts portal
    Then Navigate to contact us section
    Then Submit Contact Us form with Email "ClassicPartsContactDet,B,2" First name "ClassicPartsContactDet,C,2" Last name "ClassicPartsContactDet,D,2" Country "CPData,I,2" Phone number "ClassicPartsContactDet,H,3" Enquiry Type "CPData,S,4" VIN number "CPData,J,2"
    And close the browser
    Then User Access to SVO Portal as Classic Parts and technical user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    Then Search the Case number in the Search box
    And Verify the Case Origin Case Owner and Web Email
    And user logout from the portal

  #Submit a case for Parts and Technical enquiry
  @SSEC_1225_CaseCreationWebToCase_02	
  Scenario: Submit a case for Parts and Technical enquiry
    Given Access to the Classic Parts portal
    Then Submit Contact Us form with Email "ClassicPartsContactDet,B,2" First name "ClassicPartsContactDet,C,2" Last name "ClassicPartsContactDet,D,2" Country "CPData,I,2" Phone number "ClassicPartsContactDet,H,3" Enquiry Type "CPData,S,2" VIN number "CPData,J,2"
    And close the browser
    Then User Access to SVO Portal as Classic Parts and technical user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    Then Search the Case number in the Search box
    And Verify the Case Origin Case Owner and Web Email
    And user logout from the portal
    	
  #User is not able to submit a case, if any mandatory field value is missing
  @SSEC_1225_CaseCreationWebToCase_03
  Scenario: User is not able to submit a case, if any mandatory field value is missing
    Given Access to the Classic Parts portal
    Then Navigate to contact us section
    Then Submit Contact Us form with Email "ClassicPartsContactDet,B,2" First name "ClassicPartsContactDet,C,2" Last name "ClassicPartsContactDet,D,2" Country "CPData,I,2" Enquiry Type "CPData,S,2" VIN number "CPData,J,2"
    And Verify that error message while submitting contact us form
    And user logout from the portal
    	
  #User is not able to view created web case in salesforce, if case is not submitted from Classic Parts web portal
  @SSEC_1225_CaseCreationWebToCase_04
  Scenario: User is not able to view created web case in salesforce, if case is not submitted from Classic Parts web portal
    Given Access to the Classic Parts portal
    Then Navigate to contact us section
    Then Submit Contact Us form with Email "ClassicPartsContactDet,B,2" First name "ClassicPartsContactDet,C,2" Last name "ClassicPartsContactDet,D,2" Country "CPData,I,2" Phone number "ClassicPartsContactDet,H,3" Enquiry Type "CPData,S,4" VIN number "CPData,J,2"
    Then User Access to SVO Portal as Classic Parts and technical user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    And User navigates to the Enquiry tab and validate that no new enquiry has been created
    And user logout from the portal
    
  @Q3 @SSEC-1984
  Scenario: User is not able to submit a case, if any mandatory field value is missing
    Given Access to the Classic Parts portal
    Then Navigate to contact us section
    Then Submit Contact Us form with Email "ClassicPartsContactDet,B,2" First name "ClassicPartsContactDet,C,2" Last name "ClassicPartsContactDet,D,2" Country "ClassicPartsContactForm,I,2" Enquiry Type "ClassicPartsContactForm,S,2" VIN number "ClassicPartsContactForm,J,2"
    And Verify that error message while submitting contact us form
    And user logout from the portal
    
  @Q4 @SSEC-1985
  Scenario: User is not able to view created web case in salesforce, if case is not submitted from Classic Parts web portal
    Given Access to the Classic Parts portal
    Then Navigate to contact us section
    Then Submit Contact Us form with Email "ClassicPartsContactDet,B,2" First name "ClassicPartsContactDet,C,2" Last name "ClassicPartsContactDet,D,2" Country "ClassicPartsContactForm,I,2" Phone number "ClassicPartsContactDet,H,3" Enquiry Type "ClassicPartsContactForm,S,4" VIN number "ClassicPartsContactForm,J,2"
    Then User Access to SVO Portal as Classic Parts and technical user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    And User navigates to the Enquiry tab and validate that no new enquiry has been created
    And user logout from the portal

    
    
    