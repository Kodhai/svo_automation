Feature: SVO_Dashboards Validations

  @SVO_DashBoards_01 @SSEC-880 @DashBoard_Batch
  Scenario: User is able to view details of dashboards on dashboards page
    Given Access to SVO bespoke uat user Portal with Username "LoginUsers,B,3" and Password "LoginUsers,C,3"
    Then Navigate to Dashboards page
    And Verify that dashboard Created by user deatils are updated on All dashboards page
    Then Verify that viewing as user details is updated and displayed on Performance dashboard
    Then Verify that all the details of classic consent dashboard are displayed
    Then Logout from an SVO Portal

  @SVO_DashBoards_02 @SSEC-881 @DashBoard_Batch
  Scenario: User is able to view details along with Last modified data for any Dashboard
    Given Access to SVO Private office user Portal with Username "LoginUsers,B,4" and Password "LoginUsers,C,4"
    #Given laterally Access SVO Portal by user name "LoginUsers,B,4" and Password "LoginUsers,C,4"
    Then Navigate to Dashboards page
    And Verify that dashboard Created by user deatils are updated on All dashboards page
    Then Verify that viewing as user details is updated and displayed on Performance dashboard
    Then Verify that all the details of classic consent dashboard are displayed
    Then Verify that Last Modified data is visible on Dashboards page
    Then Logout from an SVO Portal

  @SVO_DashBoards_03 @SSEC-882 @DashBoard_Batch
  Scenario: User is able to create a new Dashboard and view details of created Dashboard
    Given Access to SVO admin Portal with Username "LoginUsers,B,6" and Password "LoginUsers,C,6"
    #Given laterally Access SVO Portal by user name "LoginUsers,B,6" and Password "LoginUsers,C,6"
    Then Navigate to Dashboards page
    And User is able to create new dashboard in special operation overview folder Folder Name "Dashboards,A,2" Username "Dashboards,B,2"
    Then Logout from an SVO Portal

  @SVO_DashBoards_04 @SSEC-883 @DashBoard_Batch
  Scenario: User is able to view every details of Dashboard on dashboards page
    Given Access to SVO Portal as Classic service user role with Username "LoginUsers,B,12" and Password "LoginUsers,C,12"
    #Given laterally Access SVO Portal by user name "LoginUsers,B,12" and Password "LoginUsers,C,12"
    Then Navigate to Dashboards page
    And Verify that dashboard Created by user deatils are updated on All dashboards page
    Then Verify that viewing as user details is updated and displayed on Performance dashboard
    Then Verify that all the details of classic consent dashboard are displayed
    Then Verify that Last Modified data is visible on Dashboards page
    Then Logout from an SVO Portal

  @SVO_DashBoards_05 @SSEC-884 @DashBoard_Batch
  Scenario: User is able to download or Save any dashboard on Dashboards page
    Given Access to SVO Portal as Classic Admin user with Username "LoginUsers,B,13" and Password "LoginUsers,C,13"
    #Given laterally Access SVO Portal by user name "LoginUsers,B,13" and Password "LoginUsers,C,13"
    Then Navigate to Dashboards page
    And Verify that dashboard Created by user deatils are updated on All dashboards page
    Then Verify that viewing as user details is updated and displayed on Performance dashboard
    Then Verify that all the details of classic consent dashboard are displayed
    Then Verify that Last Modified data is visible on Dashboards page
    And User is able to download save and create new dashboard on dashboard page
    Then Logout from an SVO Portal

  @SVO_DashBoards_06 @SSEC-892 @DashBoard_Batch
  Scenario: User is not able to edit dashaboard which is not created by logged in user
    Given Access to SVO admin Portal with Username "LoginUsers,B,6" and Password "LoginUsers,C,6"
    #Given laterally Access SVO Portal by user name "LoginUsers,B,6" and Password "LoginUsers,C,6"
    Then Navigate to Dashboards page
    And User is able to create new dashboard in special operation overview folder Folder Name "Dashboards,A,2" Username "Dashboards,B,2"
    Given Access to SVO as bespoke operation user with Username "LoginUsers,B,8" and Password "LoginUsers,C,8"
    And User is not able to edit dashboard which is created by other user
    Then Logout from an SVO Portal

  @SVO_DashBoards_07 @SSEC-893 @DashBoard_Batch
  Scenario: Verify that user is able to view details of dashboards on dashboards page
    Given Access to SVO Portal as Classic Operations user with Username "LoginUsers,B,9" and Password "LoginUsers,C,9"
    #Given laterally Access SVO Portal by user name "LoginUsers,B,9" and Password "LoginUsers,C,9"
    When User is not able to view dashboards tab on Home Page of SVO portal
    Then Logout from an SVO Portal
