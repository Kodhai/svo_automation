Feature: SVO_Case_LifeCycle

  #User is able to close the Parts & Technical Case created in Salesforce
  @SSEC_1238_Case_Lifecycle_01 @SSEC-1632
  Scenario: User is able to close the Parts & Technical Case created in Salesforce
    Given Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    And User create new case with record type "Case_Lifecycle,B,2"
    Then User close the case with status "Case_Lifecycle,C,2"
    And Verify that case closure status set to be closed under case closure details
    Then Logout from SVO Portal

  #User is able to edit case closure details for newly created Customer Service case
  @SSEC_1238_Case_Lifecycle_02 @SSEC-1660
  Scenario: User is able to edit case closure details for newly created Customer Service case
    Given Access to SVO Portal as Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    And User create new case with record type "Case_Lifecycle,B,3"
    Then User edits case closure status "Case_Lifecycle,D,2"
    And Verify that updated case closure status details
    Then Logout from SVO Portal

  #User is able to edit case closure details of case which is in open status
  @SSEC_1238_Case_Lifecycle_03 @SSEC-1661
  Scenario: User is able to edit case closure details of case which is in open status
    Given Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    And User create new case with record type "Case_Lifecycle,B,2"
    Then User move the case to open status
    Then User edits case closure status "Case_Lifecycle,D,3"
    And Verify that updated case closure status details
    Then Logout from SVO Portal

  #User is able to edit case closure details of case which is in 'In Progress' status
  @SSEC_1238_Case_Lifecycle_04 @SSEC-1662
  Scenario: User is able to edit case closure details of case which is in 'In Progress' status
    Given Access to SVO Portal as Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    And User create new case with record type "Case_Lifecycle,B,3"
    Then User move the case to inprogres status
    Then User edits case closure status "Case_Lifecycle,D,3"
    And Verify that updated case closure status details
    Then Logout from SVO Portal

  #User is not able to edit case closure status once the case has been closed
  @SSEC_1238_Case_Lifecycle_05 @SSEC-1663
  Scenario: User is not able to edit case closure status once the case has been closed
    Given Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    And User create new case with record type "Case_Lifecycle,B,2"
    Then User close the case with status "Case_Lifecycle,C,2"
    And Verify that case closure status set to be closed under case closure details
    Then User is not able to edit case closure status once the case has been closed status "Case_Lifecycle,D,3"
    Then Logout from SVO Portal

  #User is able to move closed Customer Service case to new status, which is created via Web
  @SSEC_1238_Case_Lifecycle_06 @SSEC-1664
  Scenario: User is able to move closed Customer Service case to new status, which is created via Web
    Given Access to the Classic Parts portal
    Then Submit Contact Us form with Email "Case_Lifecycle,F,2" First name "Case_Lifecycle,G,2" Last name "Case_Lifecycle,H,2" Country "Case_Lifecycle,I,2" Phone number "Case_Lifecycle,J,2" VIN number "Case_Lifecycle,K,2"
    Given User Access to SVO Portal as Customer Service user with Username "LoginUsers,B,20" and Password "LoginUsers,C,20"
    And User select case under respective queue "Case_Lifecycle,L,3"
    Then User close the case with status "Case_Lifecycle,C,2"
    And Verify that case closure status set to be closed under case closure details
    Then User is able to move the case back to new lifecycle after closed successfully "Case_Lifecycle,E,2"
    Then Logout from SVO Portal

  #User is able to edit Parts & Techinical case closure reason after case has been closed, which is created via Email
  @SSEC_1238_Case_Lifecycle_07 @SSEC-1665
  Scenario: User is able to edit Parts & Techinical case closure reason after case has been closed, which is created via Email
    Given Access to mail account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    When Customer send an enquiry mail to user "Case_Lifecycle,F,2"
    Given User Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    And User select customer service case under respective queue "Case_Lifecycle,L,2"
    And Enter VIN number
    Then User close the case with status "Case_Lifecycle,C,4"
    And Verify that case closure status set to be closed under case closure details
    Then User is able to edit case closure reason after case has been closed "Case_Lifecycle,C,2"
    Then Logout from SVO Portal

  #User is not able to create new case without VIN number on salesforce
  @SSEC_1238_Case_Lifecycle_08 @SSEC-1666
  Scenario: User is not able to create new case without VIN number on salesforce
    Given Access to Salesforce Portal as Classic Parts user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    And User not able to create new case without VIN number record type "Case_Lifecycle,B,2"
    Then Logout from SVO Portal

  #User is not able to close the case which is created through email
  @SSEC_1238_Case_Lifecycle_09 @SSEC-1667
  Scenario: User ia not able to close the case which is created through email
    Given Access to mail account with Username "LoginUsers,B,17" and password "LoginUsers,C,17"
    When Customer send an enquiry mail to user "Case_Lifecycle,F,2"
    Given User Access to SVO Portal as Classic Parts user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    And User select case under respective queue "Case_Lifecycle,L,2"
    And Verify that error message while closing the case without VIN number
    Then Logout from SVO Portal

  #User is able to close the case without case closure reason on salesforce
  @SSEC_1238_Case_Lifecycle_10 @SSEC-1668
  Scenario: User is able to close the case without case closure reason on salesforce
    Given Access to Salesforce Portal as Classic Parts user with Username "LoginUsers,B,19" and Password "LoginUsers,C,19"
    And User create new case with record type "Case_Lifecycle,B,2"
    Then User close the case without case closure reason status "Case_Lifecycle,C,4"
    Then Logout from SVO Portal
