
package com.jlr.svo.runner;

import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import com.jlr.listeners.Listener;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/SVO_TestFeatures", dryRun = false, glue = {

		"com.jlr.svo.tests" }, plugin = {
				"com.cucumber.listener.ExtentCucumberFormatter:test-output/extentReport/extent-Report.html" }, tags = {

						"@Shifa16" })

						

public class SVOTestRunner {
	static Listener listener = new Listener();

	@BeforeClass
	public static void start() {
		listener.onStart();

	}

}
