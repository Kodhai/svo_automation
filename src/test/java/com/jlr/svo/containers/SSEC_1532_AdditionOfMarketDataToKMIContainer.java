package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SSEC_1532_AdditionOfMarketDataToKMIContainer {

	@FindBy(how = How.XPATH, using = "//a[@data-tab-value='activityTab']//parent::li")
	public WebElement ActivityTab;

	@FindBy(how = How.XPATH, using = "//button[@title='Add']")
	public WebElement AddBtn;

	@FindBy(how = How.XPATH, using = "//div[@role='none']")
	public WebElement SubjectText;

	@FindBy(how = How.XPATH, using = "//input[@data-value='Call']")
	public WebElement callActivityText;

	@FindBy(how = How.XPATH, using = "(//span[text()='Save']//parent::button)[8]")
	public WebElement ActivitySaveBtn;

	@FindBy(how = How.XPATH, using = "//a[@data-tab-name='Closed']")
	public WebElement ClosedTab;

	@FindBy(how = How.XPATH, using = "(//span[@id='window']//parent::slot)[2]")
	public WebElement LinkKMI;

	@FindBy(how = How.XPATH, using = "(//a[starts-with(@title, 'KMI-')])[1]")
	public WebElement KMIrecord;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Subject__c']")
	public WebElement enquiryTitle;

	@FindBy(how = How.XPATH, using = "//label[text()='Preferred Contact']")
	public WebElement PreferredContactBox;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Contacts...']")
	public WebElement SearchContactBox;
	
	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Vehicle Manufacturers...']")
	public WebElement BrandSelect;
	
	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Vehicle Models...']")
	public WebElement ModelSelect;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Product Offering']")
	public WebElement productOff;
//    @FindBy(how = How.XPATH, using = "//label[text()='Email Address']")
//    public WebElement EmailAddTitle;
//    
//    @FindBy(how = How.XPATH, using = " //input[@name='SO_Email_Address__c']")
//    public WebElement EmailAddTextBox;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Priya Kodhai']")
	public WebElement SearchElement;

	@FindBy(how = How.XPATH, using = "//button[@data-value='Qualified - Opportunity']")
	public WebElement qualifiedOpportBtn;

	@FindBy(how = How.XPATH, using = " //button[@data-value='Qualified - KMI']")
	public WebElement qualifiedKMIBtn;

	@FindBy(how = How.XPATH, using = "//button[@data-value='China']")
	public WebElement editRegionChina;

	@FindBy(how = How.XPATH, using = "//button[@data-value='NSC (China)']")
	public WebElement editSalesArea;

	@FindBy(how = How.XPATH, using = "//button[@data-value='China' and @aria-label='Market, China']")
	public WebElement editMarketChina;

	@FindBy(how = How.XPATH, using = "//button[@name='SaveEdit']")
	public WebElement SaveEditBtn;

	@FindBy(how = How.XPATH, using = "//span[text()='Enquiries']//parent::a")
	public WebElement EnquiriesTab;

	@FindBy(how = How.XPATH, using = "//a[@title='Show 3 more actions']")
	public WebElement EnquiryDD;

	@FindBy(how = How.XPATH, using = " //a[@title='Delete']")
	public WebElement DeleteEnquiryOpt;

	@FindBy(how = How.XPATH, using = "//button[@title='Delete']")
	public WebElement deleteEnqBtn;

	@FindBy(how = How.XPATH, using = "//a[@title='KMIs']")
	public WebElement KMITab;

	@FindBy(how = How.XPATH, using = "//span[text()='Source Enquiry']")
	public WebElement SourceEnq;

	@FindBy(how = How.XPATH, using = "//div[text()='Capture enquiry details for a potential Classic Sales Opportunity.']")
	public WebElement ClassicSalesBtn;

	@FindBy(how = How.XPATH, using = "//span[text()='Region']")
	public WebElement Region;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Owner']")
	public WebElement Owner;

}
