package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SSEC_1005_AutopoulatedFieldsOnOrderWorkOrderContainer {

	@FindBy(how = How.XPATH, using = "//label[text()='Bespoke Interior Requirements']//following-sibling::div//textarea")
	public WebElement BespokeInteriorRequirementTxtBox;

	@FindBy(how = How.XPATH, using = "//label[text()='Bespoke Exterior Requirements']//following-sibling::div//textarea")
	public WebElement BespokeExteriorRequirementTxtBox;

	@FindBy(how = How.XPATH, using = "//label[text()='Bespoke Paint Requirements']//following-sibling::div//textarea")
	public WebElement BespokePaintRequirementTxtBox;

	@FindBy(how = How.XPATH, using = "//button[@class='test-id__section-header-button slds-section__title-action slds-button']//span[text()='Bespoke Specification']")
	public WebElement BespokeSpecificationsSection;

	@FindBy(how = How.XPATH, using = "//span[text()='Bespoke Interior Requirements']//parent::div//following-sibling::div//span//slot//lightning-formatted-text")
	public WebElement BespokeInteriorRequirementFieldValue;

	@FindBy(how = How.XPATH, using = "//span[text()='Bespoke Exterior Requirements']//parent::div//following-sibling::div//span//slot//lightning-formatted-text")
	public WebElement BespokeExteriorRequirementFieldValue;

	@FindBy(how = How.XPATH, using = "//span[text()='Bespoke Paint Requirements']//parent::div//following-sibling::div//span//slot//lightning-formatted-text")
	public WebElement BespokePaintRequirementFieldValue;

	@FindBy(how = How.XPATH, using = "//span[text()='Bespoke Interior Requirements']//parent::div//following-sibling::div//span//span")
	public WebElement BespokeInteriorRequirementOrderField;

	@FindBy(how = How.XPATH, using = "//span[text()='Bespoke Exterior Requirements']//parent::div//following-sibling::div//span//span")
	public WebElement BespokeExteriorRequirementOrderField;

	@FindBy(how = How.XPATH, using = "//span[text()='Bespoke Paint Requirements']//parent::div//following-sibling::div//span//span")
	public WebElement BespokePaintRequirementOrderField;

	@FindBy(how = How.XPATH, using = "//span[text()='Bespoke Request Information']")
	public WebElement BespokeRequestSection;

	@FindBy(how = How.XPATH, using = "//div[@class='changeRecordTypeOptionRightColumn']//span[text()='Classic Bespoke']")
	public WebElement ClassicBespokeRecType;

	@FindBy(how = How.XPATH, using = "//div[@class='changeRecordTypeOptionRightColumn']//span[text()='Classic Works Legend']")
	public WebElement ClassicWorksLegendRecType;

	@FindBy(how = How.XPATH, using = "//div[@class='changeRecordTypeOptionRightColumn']//span[text()='Classic Continuation']")
	public WebElement ClassicContinuationRecType;

	@FindBy(how = How.XPATH, using = "//label[text()='Programme']//following-sibling::div//input[@placeholder='Search Programmes...']")
	public WebElement ProgrammeFieldTxtBox;

	@FindBy(how = How.XPATH, using = "//label[text()='Programme']//following-sibling::div//span[@class='slds-media__body']//span[contains(text(),'Show All Results for')]")
	public WebElement ProgrammeFieldSearchBar;

	@FindBy(how = How.XPATH, using = "//table[contains(@class,'uiVirtualDataGrid')]//child::tbody//child::tr//child::td//child::a[text()='LandRover_Defender']")
	public WebElement DefenderProgrammeFieldValue;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Bespoke Interior Requirements']")
	public WebElement EditBespokeExteriorRequirementFieldValue;

	@FindBy(how = How.XPATH, using = "//button[@name='SaveEdit']")
	public WebElement SaveBespokeExteriorRequirementBtn;

	@FindBy(how = How.XPATH, using = "//a[@title='Closed']")
	public WebElement ClosedStageButton;

	@FindBy(how = How.XPATH, using = "//button//span[text()='Select Closed Stage']")
	public WebElement SelectClosedStageButton;

	@FindBy(how = How.XPATH, using = "//button[@name='StageName']")
	public WebElement ClosedStageDropDown;

	@FindBy(how = How.XPATH, using = "//span[@title='Closed Lost']")
	public WebElement ClosedLostPicklist;

	@FindBy(how = How.XPATH, using = "//button[@name='SO_Lost_Reason__c']")
	public WebElement LostReasonDropDown;

	@FindBy(how = How.XPATH, using = "//span[@title='Purchase deferred']")
	public WebElement LostReasonPicklist;

	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
	public WebElement DoneButton;

}
