package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SSEC_2170_DesignBriefAmendments_IPCCContainer {

	@FindBy(how = How.XPATH, using = "//button[@title='Edit IP & CC Material']")
	public WebElement EditIPCCMaterial;
	
	@FindBy(how = How.XPATH, using = "//span[text()='IP & Centre console']")
	public WebElement IPCCText;
	
	public String IPCC(String value) {
		return "//span[@title='" + value + "']";
	}
	
	@FindBy(how = How.XPATH, using = "//label[text()='IP & CC Material']")
	public WebElement IPCCMaterialText;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Column Cowl Colour']")
	public WebElement ColumnCowlColor;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Topper Pad Colour']")
	public WebElement TopperPadColor;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Mid Roll Colour']")
	public WebElement MidRollColor;
	
	@FindBy(how = How.XPATH, using = "//label[text()='IP Lower Colour']")
	public WebElement IPLowerColor;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Console Colour']")
	public WebElement ConsoleColor;
	
	@FindBy(how = How.XPATH, using = "//button[@title='Edit Mid Roll Colour']")
	public WebElement EditMidRollColor;
	
	@FindBy(how = How.XPATH, using = "//label[text()='SV Ceramics']")
	public WebElement SVCeramics;
	
	@FindBy(how = How.XPATH, using = "//button[@title='Edit Front Door Armrest Material']")
	public WebElement EditFrontDoorArmrestMaterial;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Front Door Armrest Material']")
	public WebElement FrontDoorArmrestMaterial;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Rear Door Armrest Material']")
	public WebElement RearDoorArmrestMaterial;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Top Roll Front Colour']")
	public WebElement TopRollFrontColor;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Main Carrier Front Colour']")
	public WebElement MainCarrierFrontColor;
	
	@FindBy(how = How.XPATH, using = "//button[@title='Edit Arm Rest Front colour']")
	public WebElement EditArmRestFrontColorBtn;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Arm Rest Front colour']")
	public WebElement ArmRestFrontColorLabel;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Arm Rest Rear colour']")
	public WebElement ArmRestRearColor;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Top Roll Rear Colour']")
	public WebElement TopRollRearColor;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Main Carrier Rear Colour']")
	public WebElement MainCarrierRearColor;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Doors']")
	public WebElement DoorsLabel;
	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement
	
}
