package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SSEC_2293_Classic_Parts_Owner_Help_Form_Container {

	@FindBy(how = How.XPATH, using = "//span[text()='Vin/Chassis number']")
	public WebElement VINforOwnerHelpForm;

	@FindBy(how = How.XPATH, using = "// span[text()='Mileage (Approx)']")
	public WebElement MileageforOwnerHelpForm;

	@FindBy(how = How.XPATH, using = "//select[@name='faultlocation']")
	public WebElement FaultLocation;

	@FindBy(how = How.XPATH, using = "//textarea[@id='faultdescription']")
	public WebElement DescriptionForOwnerHelpForm;

	@FindBy(how = How.XPATH, using = "//button[text()='Allow all']")
	public WebElement Allowall;

	@FindBy(how = How.XPATH, using = "//span[text()='Mileage (Approx)']")
	public WebElement Mileage;

	@FindBy(how = How.XPATH, using = "//span[text()='Town']")
	public WebElement Town;

	@FindBy(how = How.XPATH, using = "//select[@name='symptom']")
	public WebElement Symptom;

	@FindBy(how = How.XPATH, using = " //select[@name='faultlocation']")
	public WebElement Faultlocation;

	@FindBy(how = How.XPATH, using = "//textarea[@id='faultdescription']")
	public WebElement Description;

	@FindBy(how = How.XPATH, using = "//span[text()='First Name']")
	public WebElement FirstName;

	@FindBy(how = How.XPATH, using = "//span[text()='Last Name']")
	public WebElement Lastname;

	@FindBy(how = How.XPATH, using = "//span[text()='Email']")
	public WebElement Email;

	@FindBy(how = How.XPATH, using = "//span[text()='Phone Number']")
	public WebElement PhoneNumber;

	@FindBy(how = How.XPATH, using = "//span[text()='Submit']")
	public WebElement Submit;

	@FindBy(how = How.XPATH, using = "//div[text()='Please enter your vin/chassis number and try again.']")
	public WebElement Error;

	@FindBy(how = How.ID, using = "username")
	public WebElement userNameTextBox;

	@FindBy(how = How.ID, using = "password")
	public WebElement passwordTextBox;

	@FindBy(how = How.ID, using = "Login")
	public WebElement loginBtn;

	@FindBy(how = How.ID, using = "emc")
	public WebElement verificationCode;

	@FindBy(how = How.ID, using = "save")
	public WebElement verifyBtn;

	@FindBy(how = How.XPATH, using = "//input[@id='emc']")
	public WebElement vCodeTextBox;

	@FindBy(how = How.ID, using = "p")
	public WebElement mobileNumText;

	@FindBy(how = How.LINK_TEXT, using = "Remind Me Later")
	public WebElement remindMeLater;

	@FindBy(how = How.LINK_TEXT, using = "I Don't Want to Register My Phone")
	public WebElement notRegister;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-show']")
	public WebElement menuBtn;

	@FindBy(how = How.XPATH, using = "(//span[@class='slds-truncate'])[1]")
	public WebElement SVOText;

	@FindBy(how = How.XPATH, using = "//input[@value='Next']")
	public WebElement outlookNext;

	@FindBy(how = How.XPATH, using = "//input[contains(@value,'Sign in')]")
	public WebElement outlookSignInBtn;

	@FindBy(how = How.XPATH, using = "//input[contains(@value,'Yes')]")
	public WebElement outlookConfirmYes;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Sign in')]")
	public WebElement outlookSignin;

	@FindBy(how = How.NAME, using = "loginfmt")
	public WebElement outlookUsername;

	@FindBy(how = How.NAME, using = "passwd")
	public WebElement outlookPassword;

	@FindBy(how = How.XPATH, using = "(//div[contains(@aria-label,'noreply@salesforce.com')])[1]")
	public WebElement outlookEmail;

	@FindBy(how = How.XPATH, using = "//div[@class='PlainText']")
	public WebElement outlookVerificationCode;

	@FindBy(how = How.XPATH, using = "//img[@alt='Outlook']")
	public WebElement outlookIcon;

	@FindBy(how = How.XPATH, using = "//a[@title='Cases']")
	public WebElement CasesTab;

	@FindBy(how = How.XPATH, using = "//div[@class='slds-dropdown-trigger slds-dropdown-trigger_click']")
	public WebElement Allviews;

}
