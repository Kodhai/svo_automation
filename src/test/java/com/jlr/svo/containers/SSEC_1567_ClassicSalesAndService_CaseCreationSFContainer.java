package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SSEC_1567_ClassicSalesAndService_CaseCreationSFContainer {

	@FindBy(how = How.XPATH, using="//span[text()='Case Record Type']")
	public WebElement CaseRecLabel;
	
	@FindBy(how = How.XPATH, using="//span[text()='Classic Sales']")
	public WebElement ClassicSalesLabelRec;
	
	@FindBy(how = How.XPATH, using="//span[text()='Classic Service']")
	public WebElement ClassicServLabelRec;
	
	@FindBy(how = How.XPATH, using="//button[@title='Save & New' and @type='button']")
	public WebElement SaveAndNewBtn;
	
	
}
