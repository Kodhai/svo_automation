package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SVO_WorkOrder_AdditionalOwner_Container {

	@FindBy(how = How.XPATH, using = "(//a[starts-with(@title,'WO-')])[1]")
	public WebElement FirstWorkOrder;

	@FindBy(how = How.XPATH, using = "//span[text()='Additional Owner']")
	public WebElement AdditionalOwnerField;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Additional Owner']")
	public WebElement EditAdditionalOwnerFieldIcon;

	@FindBy(how = How.XPATH, using = "//button[@title='Clear Selection']")
	public WebElement ClearAdditionalOwnerFieldIcon;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search People...']")
	public WebElement AdditionalOwnerFieldInputTextBox;

	@FindBy(how = How.XPATH, using = "//strong[text()='classic-ops-uat-user']")
	public WebElement ClassicOperationsUser;

	@FindBy(how = How.XPATH, using = "//strong[text()='Classic-service-uat-user'] ")
	public WebElement ClassicServiceUser;

	@FindBy(how = How.XPATH, using = "//button[@name='SaveEdit']")
	public WebElement AdditionalOwnerFieldSaveButton;

	@FindBy(how = How.XPATH, using = "//*[@id='window']")
	public WebElement AdditionalOwnerAutoPopulatedField;

	@FindBy(how = How.XPATH, using = "//span[text()='Additional Owner']")
	public WebElement AdditinalOwnerFiled;

	@FindBy(how = How.XPATH, using = "//span[text()='Edit Additional Owner']//parent::button")
	public WebElement AdditinalOwnerFiledEditBtn;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search People...']")
	public WebElement UserNameSearchBox;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Show All Results for')]")
	public WebElement ShowAllUsersList;

	@FindBy(how = How.XPATH, using = "//div[@class='name']")
	public WebElement UserFullName;

	@FindBy(how = How.XPATH, using = "//button[@name='SaveEdit']")
	public WebElement SaveBtn;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'classic-')]")
	public WebElement AdditinalOwnerFiledName;

	@FindBy(how = How.XPATH, using = "//button[@title='Clear Selection']")
	public WebElement ClearAdditinalOwnerName;

	@FindBy(how = How.XPATH, using = "//lightning-formatted-text[contains(text(),'WO-')]")
	public WebElement WorkOrderNumber;

	@FindBy(how = How.XPATH, using = "//span[text()='All']")
	public WebElement AllWorkOrderList;

	@FindBy(how = How.XPATH, using = "//a[contains(@title,'WO-')]")
	public WebElement SelectFirstWorkOrder;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search this list...']")
	public WebElement WorkOrderSearchBox;

	@FindBy(how = How.XPATH, using = "//span[text()='Start - Forecast']")
	public WebElement SortStartForecast;

	@FindBy(how = How.XPATH, using = "//force-owner-lookup//child::force-lookup//child::div//child::force-hoverable-link//child::div//a[@class='flex-wrap-ie11']")
	public WebElement OwnerName;

	@FindBy(how = How.XPATH, using = "//Strong[text()='Review the errors on this page.']")
	public WebElement ErrormessageForSameAdditionalOwnerName;

	@FindBy(how = How.XPATH, using = "//button[text()='Delete']")
	public WebElement DeleteBtn;

	@FindBy(how = How.XPATH, using = "//button[@title='Delete']")
	public WebElement DeleteDoneBtn;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Work Order \"')]")
	public WebElement DelecteSuccessfullMessage;

	@FindBy(how = How.XPATH, using = "//button[@name='CancelEdit']")
	public WebElement CancelBtn;

	@FindBy(how = How.XPATH, using = "//span[text()='Classic Reborn']")
	public WebElement ClassicRebornRecType;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Retail_Price__c']")
	public WebElement MainVehicleRetailPrice;

	@FindBy(how = How.XPATH, using = "//label[text()='Vehicle']//following-sibling::div//input[@placeholder='Search Records...']")
	public WebElement ClassicServiceVehicleRecord;

	@FindBy(how = How.XPATH, using = "(//lightning-base-combobox-formatted-text[starts-with(@title,'Defender')])[4]")
	public WebElement ClassicServiceDefenderVehicle;

	@FindBy(how = How.XPATH, using = "//label[text()='VAT Qualifying']//following-sibling::div//button[@type='button']")
	public WebElement VATQualifyingDropdown;

	@FindBy(how = How.XPATH, using = "//span[@title='Yes']")
	public WebElement VATyesButton;

	@FindBy(how = How.XPATH, using = "//a[@title='Order Placed']")
	public WebElement OrderPlacedButton;

	@FindBy(how = How.XPATH, using = "//button//span[text()='Mark as Current Stage']")
	public WebElement MarkasCurrentStatusButton;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Vehicle Body Styles...']")
	public WebElement BodyStyleTextBox;

	@FindBy(how = How.XPATH, using = "(//lightning-base-combobox-formatted-text[@title='Defender'])[2]")
	public WebElement TestDefenderBodystyle;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Build_Colour__c']")
	public WebElement BuildColour;

	@FindBy(how = How.XPATH, using = "//label[text()='Hand of Drive']//following-sibling::div//button")
	public WebElement HandOfDrivedropDown;

	@FindBy(how = How.XPATH, using = "//span[@title='LHD']")
	public WebElement HandOfDriveLHD;

	@FindBy(how = How.XPATH, using = "//label[text()='Speedometer']//following-sibling::div//button")
	public WebElement Speedometerdropdown;

	@FindBy(how = How.XPATH, using = "//span[@title='MPH']")
	public WebElement SpeedometerMPH;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Build_Colour_Shade__c']")
	public WebElement BuildColourShade;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Build_Interior_Colour__c']")
	public WebElement BuildInteriorColour;

	@FindBy(how = How.XPATH, using = "//label[text()='Additional Spec Details']//following-sibling::div//textarea[@class='slds-textarea']")
	public WebElement AdditionalSepcDetails;

	@FindBy(how = How.XPATH, using = "//a[starts-with(text(),'WO-')]")
	public WebElement WorkOrdernumberOnOrdersPage;

	@FindBy(how = How.XPATH, using = "//strong[text()='Bespoke-uat-user']")
	public WebElement BespokeUserasAdditionalOwner;

}
