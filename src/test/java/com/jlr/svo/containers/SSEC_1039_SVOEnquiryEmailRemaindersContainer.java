package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SSEC_1039_SVOEnquiryEmailRemaindersContainer {

	@FindBy(how = How.XPATH, using = "//span[text()='SLA - First Contact - Time Remaining']//parent::div//following-sibling::div[@class='slds-form-element__control']//child::lightning-formatted-text")
	public WebElement SLAFirstContactTimeRemaining;

}
