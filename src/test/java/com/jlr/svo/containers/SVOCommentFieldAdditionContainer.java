package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SVOCommentFieldAdditionContainer 
{

	@FindBy(how = How.XPATH, using = "(//a[contains(@title,'WO-')])[1]")
	public WebElement FirstWorkOrderfromAllList;
	
	@FindBy(how = How.XPATH, using = "//div[contains(@class,'slds-form-element')]//child::span[text()='Start - Comments']")
	public WebElement BuildStartComments;
	
	@FindBy(how = How.XPATH, using = "//div[contains(@class,'slds-form-element')]//child::span[text()='Strip & Parts/BiW - Comments']")
	public WebElement StripAndPartsAndBiWComments;
	
	@FindBy(how = How.XPATH, using = "//div[contains(@class,'slds-form-element')]//child::span[text()='Chassis/PreBuild/Build - Comments']")
	public WebElement ChassisAndPreBuildAndBuildComments;
	
	@FindBy(how = How.XPATH, using = "//div[contains(@class,'slds-form-element')]//child::span[text()='Paint - Comments']")
	public WebElement PaintComments;
	
	@FindBy(how = How.XPATH, using = "//div[contains(@class,'slds-form-element')]//child::span[text()='Engine, Gearbox, Trim - Comments']")
	public WebElement EngineAndGearBoxAndTrimComments;
	
	@FindBy(how = How.XPATH, using = "//div[contains(@class,'slds-form-element')]//child::span[text()='Snagging - Comments']")
	public WebElement SnaggingComments;
	
	@FindBy(how = How.XPATH, using = "//div[contains(@class,'slds-form-element')]//child::span[text()='ABS - Comments']")
	public WebElement ABSComments;
	
	@FindBy(how = How.XPATH, using = "//div[contains(@class,'slds-form-element')]//child::span[text()='MOT - Comments']")
	public WebElement MOTComments;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Start - Comments']//parent::span//following-sibling::div//child::div[@class='slds-rich-text-area__content slds-grow slds-text-color-weak standin']")
	public WebElement NewWOBuildStartCommentBox;
	
	@FindBy(how = How.XPATH, using = "//div[contains(@class,'ql-editor ql-blank slds-rich-text-area__content')]//child::p")
	public WebElement NewWOCommentBoxLine;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Chassis/PreBuild/Build - Comments']//parent::span//parent::lightning-input-rich-text//child::div[contains(@class,'ql-editor slds-rich-text-area__content')]//child::p")
	public WebElement WOChassisCommentBoxLine;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Strip & Parts/BiW - Comments']//parent::span//following-sibling::div//child::div[@class='slds-rich-text-area__content slds-grow slds-text-color-weak standin']")
	public WebElement NewWOStripPartsBiWCommentBox;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Chassis/PreBuild/Build - Comments']//parent::span//following-sibling::div//child::div[@class='slds-rich-text-area__content slds-grow slds-text-color-weak standin']")
	public WebElement NewWOChassisPreBuildBuildCommentBox;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Paint - Comments']//parent::span//following-sibling::div//child::div[@class='slds-rich-text-area__content slds-grow slds-text-color-weak standin']")
	public WebElement NewWOPaintCommentBox;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Engine, Gearbox, Trim - Comments']//parent::span//following-sibling::div//child::div[@class='slds-rich-text-area__content slds-grow slds-text-color-weak standin']")
	public WebElement NewWOEngineGearboxTrimCommentBox;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Snagging - Comments']//parent::span//following-sibling::div//child::div[@class='slds-rich-text-area__content slds-grow slds-text-color-weak standin']")
	public WebElement NewWOSnaggingCommentBox;
	
	@FindBy(how = How.XPATH, using = "//span[text()='ABS - Comments']//parent::span//following-sibling::div//child::div[@class='slds-rich-text-area__content slds-grow slds-text-color-weak standin']")
	public WebElement NewWOABSCommentBox;
	
	@FindBy(how = How.XPATH, using = "//span[text()='MOT - Comments']//parent::span//following-sibling::div//child::div[@class='slds-rich-text-area__content slds-grow slds-text-color-weak standin']")
	public WebElement NewWOMOTCommentBox;
	
	@FindBy(how = How.XPATH, using = "//button[@name='Edit']")
	public WebElement WorkOrderEditButton;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Edit Start - Comments']//parent::button[contains(@title,'Edit Start')]")
	public WebElement WOBuildStartCommentEditBtn;
	
	@FindBy(how = How.XPATH, using = "//div[contains(@class,'ql-editor slds-rich-text-area__content')]")
	public WebElement WOEditChassisCommentBox;
	
	@FindBy(how = How.XPATH, using = "//th[@title='Work Order']//child::a[@class='toggle slds-th__action slds-text-link--reset ']")
	public WebElement AllWorkOrderSort;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Strip & Parts/BiW - Comments']//parent::span//parent::lightning-input-rich-text//child::div//child::label[text()='Font']//parent::lightning-combobox//child::input[@placeholder='Select an Option']")
	public WebElement WOStripPartsCommentFontStyle;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Strip & Parts/BiW - Comments']//parent::span//parent::lightning-input-rich-text//child::div//button[@title='Remove formatting']")
	public WebElement WOStripPartsRemoveFormatting;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Arial']")
	public WebElement WOStripPartsCommentArialFont;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Strip & Parts/BiW - Comments']//parent::span//parent::lightning-input-rich-text//child::div//child::label[text()='Font Size']//parent::lightning-combobox//child::input[@placeholder='Select an Option']")
	public WebElement WOStripPartsCommentFontSize;	
	
	@FindBy(how = How.XPATH, using = "//span[text()='16']")
	public WebElement WOStripPartsComment16Font;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Strip & Parts/BiW - Comments']//parent::span//parent::lightning-input-rich-text//child::div//child::label[text()='Font']//parent::lightning-combobox//parent::div//parent::div//parent::div//child::button[contains(@class,'slds-button slds-color-picker__summary-button')]")
	public WebElement WOStripPartsCommentFontColor;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Hex']//parent::div//child::div//child::input[@placeholder='#FFFFFF']")
	public WebElement WOStripPartsCommentFontColorHexa;
	
	@FindBy(how = How.XPATH, using = "//button[@name='done']")
	public WebElement WOStripPartsCommentFontColorDoneBtn;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Strip & Parts/BiW - Comments']//parent::span//parent::lightning-input-rich-text//child::div//child::label[text()='Font']//parent::lightning-combobox//parent::div//parent::div//parent::div//child::ul//child::li//child::button[@title='Bold']")
	public WebElement WOStripPartsCommentTextBold;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Strip & Parts/BiW - Comments']//parent::span//parent::lightning-input-rich-text//child::div//child::label[text()='Font']//parent::lightning-combobox//parent::div//parent::div//parent::div//child::ul//child::li//child::button[@title='Italic']")
	public WebElement WOStripPartsCommentTextItalic;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Strip & Parts/BiW - Comments']//parent::span//parent::lightning-input-rich-text//child::div//child::label[text()='Font']//parent::lightning-combobox//parent::div//parent::div//parent::div//child::ul//child::li//child::button[@title='Underline']")
	public WebElement WOStripPartsCommentTextUnderline;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Strip & Parts/BiW - Comments']//parent::span//following-sibling::div//child::div[contains(@class,'ql-editor slds-rich-text-area__content')]")
	public WebElement WOStripPartsCommentBoxWithFormat;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Strip & Parts/BiW - Comments']//parent::span//following-sibling::div//child::div[contains(@class,'ql-editor ql-blank slds-rich-text-area__content')]//child::p")
	public WebElement WOStripPartsCommentBoxWithFormatLine;
	
	@FindBy(how = How.XPATH, using = "//button[@title='Edit Strip & Parts/BiW - Comments']")
	public WebElement WOStripAndProcessCommentEditBtn;
	
	@FindBy(how = How.XPATH, using = "//button[@title='Edit Chassis/PreBuild/Build - Comments']")
	public WebElement WOChassisCommentEditBtn;
	
	@FindBy(how = How.XPATH, using = "//button[@title='Edit Paint - Comments']")
	public WebElement WOPaintCommentEditBtn;
	
	@FindBy(how = How.XPATH, using = "//p[text()='Test Cancel Comments']")
	public WebElement WOChassisCancelComment;
	
	@FindBy(how = How.XPATH, using = "//button[@name='CancelEdit']")
	public WebElement WOChassisCancelButton;
	
	@FindBy(how = How.XPATH, using = "//div[@class='ql-editor slds-rich-text-area__content slds-grow slds-text-color_weak ql-blank']")
	public WebElement NewWOBuildCommentBox;
	
	@FindBy(how = How.XPATH, using = "//div[@class='ql-editor slds-rich-text-area__content slds-grow slds-text-color_weak']")
	public WebElement ExistingWOBuildCommentBox;
	
}
