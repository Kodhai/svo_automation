package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SSEC_2158_DesignBriefAmendments_Wheels_TreadPlatesContainer {

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Wheel Inserts']")
	public WebElement EditWheelInsertsBtn;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Wheel details']")
	public WebElement WheelDetailsText;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Wheel Inserts']")
	public WebElement WheelInsertsDropDown;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Wheel Colour']")
	public WebElement WheelColorDropDown;
	
	public String Wheels(String value) {
		return "//span[@title='" + value + "']";
	}
	
	@FindBy(how = How.XPATH, using = "//label[text()='Lower Trim Colour']")
	public WebElement LowerTrimColorDropDown;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Upper Trim Colour']")
	public WebElement UpperTrimColorDropDown;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Treadplate Personalization']")
	public WebElement TreadPlatePersonalization;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Thread plate  & Pillar trims Details']")
	public WebElement TreadplateTrimDetailsText;
	
	@FindBy(how = How.XPATH, using = "//button[@title='Edit Lower Trim Colour']")
	public WebElement EditLowerTrimColor;
	
	@FindBy(how = How.XPATH, using = "//button[@title='Edit Head Rest Material']")
	public WebElement EditHeadrestMaterial;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Head rest Details']")
	public WebElement HeadRestDetailsText;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Head Rest Material']")
	public WebElement HeadRestMaterialDropDown;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Front Seat Front Squab']")
	public WebElement FrontSeatFrontSquabDropDown;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Front Seat Rear Squab']")
	public WebElement FrontSeatRearSquabDropDown;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Rear seat Front Squab']")
	public WebElement RearSeateatFrontSquabDropDown;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Rear seat Rear Squab']")
	public WebElement RearSeatRearSquabDropDown;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Perforation']")
	public WebElement PerforationDropDown;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Rare Center Console details']")
	public WebElement RearCenterConsoleDetailsText;
	
	@FindBy(how = How.XPATH, using = "//button[@title='Edit Rear Center Console Material']")
	public WebElement EditRearCenterConsoleMaterial;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Rear Center Console Arm Rest']")
	public WebElement RearCenterConsoleArmRestDropDown;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Rear Center Console Finisher Colour']")
	public WebElement RearCenterFinisherColorDropDown;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Rear Center Console Sides']")
	public WebElement RearCenterConsoleSidesDropDown;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Rear Center Console Material']")
	public WebElement RearCenterConsoleMaterialDropDown;
	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
	
}
