package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SSEC_2285_Design_Brief_FeedbacksContainer {

	@FindBy(how = How.XPATH, using = "//span[text()='Rear centre console']")
	public WebElement RearCenterConsole;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Interior']")
	public WebElement Interior;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Carpets']")
	public WebElement Carpets;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Finishes']")
	public WebElement FinishesSection;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Veneer Colour']//parent::div")
	public WebElement VeneerColor;
	
	@FindBy(how = How.XPATH, using = "//span[text()='SV Ceramics']//parent::div")
	public WebElement SVCeramics;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Chrome Packs']//parent::div")
	public WebElement ChromePacks;
	
	@FindBy(how = How.XPATH, using = "//button[text()='Submit']")
	public WebElement SubmitBtn;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Save']//parent::button//preceding-sibling::button//following-sibling::button")
	public WebElement SaveSubmitBtn;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Paint Detail']//parent::a")
	public WebElement PaintDetail;
	
	@FindBy(how = How.XPATH, using = "//div[@title='New']")
	public WebElement NewBtnPaint;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Paint colour']")
	public WebElement PaintColorText;
	
	@FindBy(how = How.XPATH, using = "//button[text()='Save']")
	public WebElement SaveBtnPaint;
	
	@FindBy(how = How.XPATH, using = "//button[text()='Cancel']")
	public WebElement CancelPaintBtn;
	
	public String PaintColor(String value) {
		return "//span[@title='" + value + "']";
	}
	
	@FindBy(how = How.XPATH, using = "//label[text()='Paint code']")
	public WebElement PaintCodeText;
	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
	
}
