package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SSEC_1234_CaseListViews_Container {

	@FindBy(how = How.XPATH, using = "//span[@title='Case Number']")
	public WebElement CaseNumberField;

	public String CaseListViewSelection(String ListView) {
		return "(//li//span[text()='" + ListView + "'])[1]";
	}

	@FindBy(how = How.XPATH, using = "//span[@title='Contact Name']")
	public WebElement ContactNameField;

	@FindBy(how = How.XPATH, using = "//span[@title='Subject']")
	public WebElement SubjectField;

	@FindBy(how = How.XPATH, using = "//span[@title='Status']")
	public WebElement StatusField;

	@FindBy(how = How.XPATH, using = "//span[@title='Priority']")
	public WebElement PriorityField;

	@FindBy(how = How.XPATH, using = "//span[@title='Case Owner Alias']")
	public WebElement CaseOwnerAliasField;

	@FindBy(how = How.XPATH, using = "//span[@title='Case Record Type']")
	public WebElement CaseRecordTypeField;

	@FindBy(how = How.XPATH, using = "(//li//span[text()='Parts & Technical -  Cases Due Closure'])[1]")
	public WebElement CasesDueClosure;
}
