package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SSEC_1089_RemoveItemsToApproveSectionContainer {

	@FindBy(how = How.XPATH, using = "//span[@title='Items to Approve']")
	public WebElement ItemsToApproveSection;
}
