package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SVO_CustomerResponsesContainer {

	@FindBy(how = How.XPATH, using = "//input[@id='identifierId']")
	public WebElement outlookEmailId;

	@FindBy(how = How.XPATH, using = "//a[text()='Sign in']")
	public WebElement signinbutton;

	@FindBy(how = How.XPATH, using = "//span[text()='Next']")
	public WebElement gmailNext;

	@FindBy(how = How.XPATH, using = "//input[@name='password']")
	public WebElement gmailPassword;

	@FindBy(how = How.XPATH, using = "//div[@class='gb_Dd gb_Vc gb_Wc']//div//div//a[@title='Gmail']")
	public WebElement gmailText;

	@FindBy(how = How.XPATH, using = "(//table[@class='F cf zt']//tr[@class='zA zE']//span[contains(text(),'3D Secure')])[1]")
	public WebElement firstEmail;

	@FindBy(how = How.XPATH, using = "//div[@class='T-I T-I-KE L3']")
	public WebElement NewMailCompose;

	@FindBy(how = How.XPATH, using = "//div[@class='T-I J-J5-Ji aoO v7 T-I-atl L3']")
	public WebElement SendMailBtn;

	@FindBy(how = How.XPATH, using = "//textarea[@name='to']")
	public WebElement GmailToTextBox;

	@FindBy(how = How.XPATH, using = "//input[@name='subjectbox']")
	public WebElement GmailSubjectTextBox;

	@FindBy(how = How.XPATH, using = "//div[@class='Am Al editable LW-avf tS-tW']")
	public WebElement GmailBodyTextBox;

	@FindBy(how = How.XPATH, using = "//h2[text()='Sandbox: Thank you for your enquiry']")
	public WebElement ThankYouEmail;

	@FindBy(how = How.XPATH, using = "//a[contains(@href,'https://accounts.google.com/SignOutOptions')]")
	public WebElement GmailaccountLogo;

	@FindBy(how = How.XPATH, using = "//div[text()='Sign out']")
	public WebElement signoutGmailIcon;

	@FindBy(how = How.XPATH, using = "//iframe[contains(@src,'https://ogs.google.com/u/0/widget/account?origin=https')]")
	public WebElement mailframe;

	@FindBy(how = How.XPATH, using = "//span[text()='All Bespoke Enquiries']")
	public WebElement AllBespokeEnquiriesList;

	@FindBy(how = How.XPATH, using = "//button[@name='change owner']")
	public WebElement ChangeOwnerSaveBtn;

	@FindBy(how = How.XPATH, using = "//span[text()='All Private Office Enquiries']")
	public WebElement AllPrivateOfficeEnquiriesList;

	@FindBy(how = How.XPATH, using = "//iframe[@title='accessibility title']")
	public WebElement Emailframe;

	@FindBy(how = How.XPATH, using = "(//li[@class='slds-dropdown__item has-icon--left   forceVirtualAutocompleteMenuOption'])[5]")
	public WebElement ClassicServiceQueue;

	@FindBy(how = How.XPATH, using = "//div[@class='a3s aiL ']")
	public WebElement outlookVerificationCode;

	@FindBy(how = How.XPATH, using = "//span[text()='Mail Delivery Subsystem']")
	public WebElement BlockedMessage;

	@FindBy(how = How.XPATH, using = "//a//span[text()='Data Manager Queue']")
	public WebElement DataManagerQueue;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Customer response received')]")
	public WebElement NoCustomerResponseNotification;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Assigned To You,')]")
	public WebElement EnquiryAssignedNotification;

	@FindBy(how = How.XPATH, using = "//label//parent::th//following-sibling::td//span//input[contains(@onkeydown,'return')]")
	public WebElement OwnerEmailSubjectTextBox;

	@FindBy(how = How.XPATH, using = "//td[@class='dataCo']//span//child::a[contains(text(),'ENQ-')]")
	public WebElement E2AEMailLink;

	@FindBy(how = How.XPATH, using = "//a[@title='Enquiries']")
	public WebElement EnquiriesTab;

	@FindBy(how = How.XPATH, using = "//span[@class='triggerLinkText selectedListView slds-page-header__title slds-truncate slds-p-right--xx-small uiOutputText']")
	public WebElement SelectOpportunityListView;

	@FindBy(how = How.XPATH, using = "//span[@class='slds-grid slds-grid--align-spread forceInlineEditCell']")
	public WebElement EnquiryName;

	@FindBy(how = How.XPATH, using = "//span[text()='New']")
	public WebElement EnquiryStatus;

	@FindBy(how = How.XPATH, using = "//span[@class='slds-truncate uiOutputText']")
	public WebElement EnquiryTitle;

	@FindBy(how = How.XPATH, using = "//span[@class='slds-truncate uiOutputDateTime']")
	public WebElement EnquiryCreatedDate;

	@FindBy(how = How.XPATH, using = "//a[text()='Emails']")
	public WebElement EmailsSection;

	@FindBy(how = How.XPATH, using = "//a[@class='flex-wrap-ie11 slds-truncate']")
	public WebElement E2ACustomerEmailSubject;

	@FindBy(how = How.XPATH, using = "//button[text()='e2a Send an Email']")
	public WebElement e2aSendEmailButton;

	@FindBy(how = How.XPATH, using = "//td[@class='data2Col  first ']//select")
	public WebElement BusinessUnitDropDown;

	public String BusinessUnitSelection(String Unit) {
		return "//option[text()='" + Unit + "']";
	}

	@FindBy(how = How.XPATH, using = "//tr//a[text()='To']//parent::th//following-sibling::td//child::span//child::textarea")
	public WebElement EmailToTextBox;

	@FindBy(how = How.XPATH, using = "//label[contains(@for,'email_subject')]")
	public WebElement EmailSubjectTextBox;

	@FindBy(how = How.XPATH, using = "//div[@class='pbBottomButtons']//input[contains(@name,'send')]")
	public WebElement EmailSendButton;

	@FindBy(how = How.XPATH, using = "//button[@name='ChangeOwnerOne']")
	public WebElement EnquiryChangeOwnerButton;

	@FindBy(how = How.XPATH, using = "//input[@title='Search Users']")
	public WebElement EnquiryChangeOwnerTextBox;

	@FindBy(how = How.XPATH, using = "//span[contains(@title,'in Users')]")
	public WebElement EnquiryChangeOwnerSearchbar;

	@FindBy(how = How.XPATH, using = "//a[text()='UAT test']")
	public WebElement EnquirySelectOwner;

	@FindBy(how = How.XPATH, using = "//button//span[text()='Submit']")
	public WebElement ChangeOwnerSubmitBtn;

	@FindBy(how = How.XPATH, using = "//h2[@class='hP']")
	public WebElement ReceivedRequestEmail;

	@FindBy(how = How.XPATH, using = "//div[@class='T-I J-J5-Ji T-I-Js-IF aaq T-I-ax7 L3 T-I-JW']")
	public WebElement EmailReplyBtn;

	@FindBy(how = How.XPATH, using = "//img[@class='hB T-I-J3 ']")
	public WebElement ClassicEmailReplyBtn;

	@FindBy(how = How.XPATH, using = "//div[@class='Am aO9 Al editable LW-avf tS-tW']")
	public WebElement EmailReplyTextBox;

	@FindBy(how = How.XPATH, using = "//span[@name='noreply@salesforce..']")
	public WebElement VerificationCodeMail;

	@FindBy(how = How.XPATH, using = "//div[@class='a3s aiL ']")
	public WebElement VerificationCode;

	@FindBy(how = How.XPATH, using = "(//div[@class='ar9 T-I-J3 J-J5-Ji'])[2]")
	public WebElement EmailDeleteButton;

	@FindBy(how = How.XPATH, using = "//div[@class='headerButtonBody']")
	public WebElement NotificationsIcon;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Customer email response received')]")
	public WebElement CustomerResponseNotification;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Go to parent: ENQ-')]")
	public WebElement EnquiryNameOnE2AMailpage;

	@FindBy(how = How.XPATH, using = "//div[@class='gs']//child::div//child::a")
	public WebElement E2AMailLink;

	@FindBy(how = How.XPATH, using = "//div[text()='Use another account']")
	public WebElement UseAnotherAccount;

	@FindBy(how = How.XPATH, using = "//h1[text()='e2a Email']")
	public WebElement E2AMailPage;

	@FindBy(how = How.XPATH, using = "//div[@class='listContent']//li//a[@role='option']//span[text()='Classic Sales Queue']")
	public WebElement ClassicSalesQueueEnquiriesList;

	@FindBy(how = How.XPATH, using = "//div[@class='listContent']//li//a[@role='option']//span[text()='Classic Service Queue']")
	public WebElement ClassicServiceQueueEnquiriesList;

	@FindBy(how = How.XPATH, using = "//div[@class='listContent']//li//a[@role='option']//span[text()='Private Office Queue']")
	public WebElement PrivateOfficeQueueEnquiriesList;

}
