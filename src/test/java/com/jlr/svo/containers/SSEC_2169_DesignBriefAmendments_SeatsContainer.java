package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SSEC_2169_DesignBriefAmendments_SeatsContainer {

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Monotone / Duotone']")
	public WebElement EditMonotoneDuotone;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Monotone / Duotone']")
	public WebElement MonotoneDuotoneLabel;
	
	public String Seats(String value) {
		return "//span[@title='" + value + "']";
	}
	
	@FindBy(how = How.XPATH, using = "//label[text()='Hand of Drive']")
	public WebElement HandofDriveLabel;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Front Seat Cushion Material']")
	public WebElement FrontSeatCushionMaterial;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Rear Seat Material']")
	public WebElement RearSeatMaterial;
	
	@FindBy(how = How.XPATH, using = "//button[@title='Edit Contrast Stitch Type']")
	public WebElement EditContrastStitchTypeBtn;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Contrast Stitch Type']")
	public WebElement ContrastStitchTypeLabel;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Contrast Colour1']")
	public WebElement ContrastColor1;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Contrast Colour2']")
	public WebElement ContrastColor2;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Seat Details']")
	public WebElement SeatDetailsText;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Contrast Colour3']")
	public WebElement ContrastColor3;
	
	@FindBy(how = How.XPATH, using = "//button[@title='Edit Seat Embroidery']")
	public WebElement EditSeatEmbroidery;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Seat Embroidery']")
	public WebElement SeatEmbroideryLabel;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Seat Embroidery personalization details']")
	public WebElement SeatEmbroiPersonalizationDetails;
	
	@FindBy(how = How.XPATH, using = "//button[@title='Edit Rear Seat cushion Colour']")
	public WebElement EditRearSeatCushionColor;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Rear Seat cushion Colour']")
	public WebElement RearSeatCushionColorText;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Rear seat backboard colour']")
	public WebElement RearSeatBackgroundColor;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Bulkhead Colour']")
	public WebElement BulkheadColor;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Rear seat backboard colour']")
	public WebElement RearSeatBackboardColor;
	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
}
