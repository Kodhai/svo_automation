package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SSEC_1228_LinkContactAndAccountTocase_Container {

	@FindBy(how = How.XPATH, using = "//div[@title='New']")
	public WebElement NewCaseButton;

	public String CaseRecordTypeSelection(String RecType) {
		return "//div[@class='changeRecordTypeOptionRightColumn']//span[text()='" + RecType + "']";
	}

	@FindBy(how = How.XPATH, using = "//button//span[text()='Next']")
	public WebElement CaseNextButton;

	@FindBy(how = How.XPATH, using = "//span[text()='First Name']//parent::label//following-sibling::input")
	public WebElement FirstNameTxtBox;

	@FindBy(how = How.XPATH, using = "//span[text()='Last Name']//parent::label//following-sibling::input")
	public WebElement LastNameTxtBox;

	@FindBy(how = How.XPATH, using = "//input[@title='Search Accounts']")
	public WebElement AccountNameTxtBx;

	@FindBy(how = How.XPATH, using = "//input[@title='Search Contacts']")
	public WebElement ContactNameTxtBx;

	@FindBy(how = How.XPATH, using = "//div[@class='searchButton itemContainer slds-lookup__item-action--label slds-text-link--reset slds-grid slds-grid--vertical-align-center slds-truncate forceSearchInputLookupDesktopActionItem lookup__header highlighted']")
	public WebElement AccountSearchBar;

	@FindBy(how = How.XPATH, using = "(//table[@class='forceRecordLayout slds-table slds-table_cell-buffer slds-table_bordered uiVirtualDataGrid--default uiVirtualDataGrid']//a[@data-refid='recordId'])[1]")
	public WebElement AccountName;

	@FindBy(how = How.XPATH, using = "//span[contains(@title,'in Contacts')]")
	public WebElement ContactSearchBar;

	@FindBy(how = How.XPATH, using = "//h1//slot//lightning-formatted-text")
	public WebElement CaseTitle;

	@FindBy(how = How.XPATH, using = "//span[text()='VIN / Chassis Number']//parent::label//following-sibling::input")
	public WebElement Vintxtbx;

	@FindBy(how = How.XPATH, using = "//button[@title='Save']")
	public WebElement SaveCaseButton;

	@FindBy(how = How.XPATH, using = "//span[text()='Account Name']//parent::div//following-sibling::div//span//slot//force-lookup//div//records-hoverable-link//div//a//slot//slot//span")
	public WebElement AccountNameField;

	@FindBy(how = How.XPATH, using = "//span[text()='Contact Name']//parent::div//following-sibling::div//span//slot//force-lookup//div//records-hoverable-link//div//a//slot//slot//span")
	public WebElement ContactNameField;

	@FindBy(how = How.XPATH, using = "//span[text()='Contact Email']//parent::div//following-sibling::div//span//slot//emailui-formatted-email-case//emailui-formatted-email-wrapper//force-aura-action-wrapper//div//a")
	public WebElement ContactEmailField;

	@FindBy(how = How.XPATH, using = "//a[@title='Contacts']")
	public WebElement ContactsTab;

	@FindBy(how = How.XPATH, using = "//input[@name='lastName']")
	public WebElement ContactNameTextBox;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Accounts...']")
	public WebElement AccountTxtBx;

	@FindBy(how = How.XPATH, using = "//span[contains(@title,'Show All Results for')]")
	public WebElement AccountSearch;

	@FindBy(how = How.XPATH, using = "//button[@name='SaveEdit']")
	public WebElement SaveContactButton;

	@FindBy(how = How.XPATH, using = "(//h1//slot[@name='primaryField']//force-aura-action-wrapper//div//div//span)[1]")
	public WebElement ContactTitle;

	@FindBy(how = How.XPATH, using = "(//ul[@class='slds-button-group-list']//li//button[@name='Edit'])[1]")
	public WebElement EditCaseButton;

	@FindBy(how = How.XPATH, using = "(//ul[@class='slds-button-group-list']//li//button[@name='Edit'])[2]")
	public WebElement EditCaseButton1;

	@FindBy(how = How.XPATH, using = "(//a[@class='deleteAction'])[1]")
	public WebElement ClearAccountIcon;

	@FindBy(how = How.XPATH, using = "(//a[@class='deleteAction'])[1]")
	public WebElement ClearContactIcon;

	@FindBy(how = How.XPATH, using = "//span[text()='First Name']//parent::label//following-sibling::div//input")
	public WebElement FirstNameTxtBx;

	@FindBy(how = How.XPATH, using = "//span[text()='Last Name']//parent::label//following-sibling::div//input")
	public WebElement LastNameTxtBx;

	@FindBy(how = How.XPATH, using = "//span[text()='Email']//parent::label//following-sibling::div//input")
	public WebElement InputEmailAddress;

	@FindBy(how = How.XPATH, using = "//span[text()='Phone Number']//parent::label//following-sibling::div//input")
	public WebElement PhoneNumberTxtBx;

	@FindBy(how = How.XPATH, using = "//select[@name='recordType']")
	public WebElement HelpSelect;

	@FindBy(how = How.XPATH, using = "//span[text()='Country']//parent::label//following-sibling::div//select")
	public WebElement CountrySelection;

	@FindBy(how = How.XPATH, using = "//input[@title='Vehicle Identification Number (VIN)']")
	public WebElement VINNumber;

	@FindBy(how = How.XPATH, using = "//textarea[@name='description']")
	public WebElement CommentText;

	@FindBy(how = How.XPATH, using = "//button[@title='Submit']")
	public WebElement SubmitBtn;

	@FindBy(how = How.XPATH, using = "//div[@class='message-success success message']")
	public WebElement SubmitSuccessfullMessage;

	@FindBy(how = How.XPATH, using = "//button[@class='CybotCookiebotDialogBodyButton' and text()='Allow all']")
	public WebElement AllowCookies;

	@FindBy(how = How.XPATH, using = "//div[@class='block-content content']//div//ul//li//a[text()='CONTACT US']")
	public WebElement ContactUsBtn;

}
