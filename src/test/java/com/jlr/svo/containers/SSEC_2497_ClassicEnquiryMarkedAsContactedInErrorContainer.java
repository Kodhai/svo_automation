package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SSEC_2497_ClassicEnquiryMarkedAsContactedInErrorContainer {

	@FindBy(how = How.XPATH, using = "//a[text()='Emails']")
	public WebElement EmailTab;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Holding Email']")
	public WebElement HoldingEmailSubject;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Enquiry Status']//parent::div//following-sibling::div//child::span//child::slot//child::lightning-formatted-text[text()='New']")
    public WebElement NewEnquiryStatus;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Enquiry Status']//parent::div//following-sibling::div//child::span//child::slot//child::lightning-formatted-text[text()='Contacted']")
	public WebElement ContactedStatus;
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
	
	
}
