package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SSEC_1225_CaseCreationWebtoCaseContainer {

	@FindBy(how = How.XPATH, using = "//input[@placeholder = 'Search Cases and more...']")
	public WebElement SearchBox;

	@FindBy(how = How.XPATH, using = "//img[@title='Case']")
	public WebElement CaseSearch;

	@FindBy(how = How.XPATH, using = "//a[@class='emailuiFormattedEmail']")
	public WebElement WebEmailBox;

	@FindBy(how = How.XPATH, using = "(//span[@class='displayLabel'])[1]")
	public WebElement CaseOwnerBox;

	
	
	@FindBy(how = How.XPATH, using = "//input[@id='email']")
	public WebElement InputEmailAddress;


	@FindBy(how = How.XPATH, using = "//button[@class='action allow primary']")
	public WebElement AllowCookies;

	@FindBy(how = How.XPATH, using = "//a[@href='/merchandise-and-lifestyle-products.html']")
	public WebElement MerchandiseTab;

	@FindBy(how = How.XPATH, using = "(//a[@class='action primary tocart listing'])[2]")
	public WebElement SelectProd;

	@FindBy(how = How.XPATH, using = "(//a[@class='action primary tocart listing'])[1]")
	public WebElement NewsProd;

	@FindBy(how = How.XPATH, using = "//button[@class='action primary tocart']")
	public WebElement AddToBasket;

	@FindBy(how = How.XPATH, using = "(//button[@class='action-close'])[2]")
	public WebElement ClosePopup;

	@FindBy(how = How.XPATH, using = "(//input[@class='required-entry'])[2]")
	public WebElement AgreeTC;

	@FindBy(how = How.XPATH, using = "//input[@id='sagepaysuitepi_cardholder']")
	public WebElement CardName;

	@FindBy(how = How.XPATH, using = "//input[@id='sagepaysuitepi_cc_number']")
	public WebElement CardNumber1;

	@FindBy(how = How.XPATH, using = "//select[@class='select select-month']")
	public WebElement SelectMonth;

	@FindBy(how = How.XPATH, using = "//select[@class='select select-year']")
	public WebElement SelectYear;

	@FindBy(how = How.XPATH, using = "//input[@class='input-text cvv']")
	public WebElement CVC1;

	@FindBy(how = How.XPATH, using = "//div[@class='shipping-information-title']")
	public WebElement DeliverTo;

	@FindBy(how = How.XPATH, using = "(//a[@class='js-qty-add control__action control__action--add'])[1]")
	public WebElement AddQty;

	@FindBy(how = How.XPATH, using = "//a[@class='action action-delete']")
	public WebElement DeleteBtn;

	@FindBy(how = How.XPATH, using = "//a[@class='action continue']")
	public WebElement ContinueShopping;

	@FindBy(how = How.XPATH, using = "(//a[@class='action primary tocart listing'])[2]")
	public WebElement SecondProd;

	@FindBy(how = How.XPATH, using = "//div[@class='title']//strong[text()='Apply Discount Code']")
	public WebElement ApplyDiscountCodeSection;

	@FindBy(how = How.XPATH, using = "//span[text()='Change Billing Address']")
	public WebElement EditBillingAddress;

	@FindBy(how = How.XPATH, using = "//div[@class='panel header']//child::ul//child::li//a[text()=' Register']")
	public WebElement signInButton;

	@FindBy(how = How.XPATH, using = "//a[@class='action create primary']")
	public WebElement registerButton;

	@FindBy(how = How.XPATH, using = "//input[@id='firstname']")
	public WebElement firstName;

	@FindBy(how = How.XPATH, using = "//input[@id='lastname']")
	public WebElement lastName;

	@FindBy(how = How.XPATH, using = "//input[@id='email_address']")
	public WebElement emailAddress;

	@FindBy(how = How.XPATH, using = "//input[@id='password']")
	public WebElement password;

	@FindBy(how = How.XPATH, using = "//input[@id='password-confirmation']")
	public WebElement passswordConfirmation;

	@FindBy(how = How.XPATH, using = "//button[@class='action submit primary']")
	public WebElement submitRegisterButton;

	@FindBy(how = How.XPATH, using = "//div[@class='box box-billing-address']//child::a//child::span[text()='Edit Address']")
	public WebElement editBilling;

	@FindBy(how = How.XPATH, using = "//div[@class='box box-shipping-address']//child::div//child::a//child::span[text()='Edit Address']")
	public WebElement editShipping;

	@FindBy(how = How.XPATH, using = "//div[@class='box box-information']//child::div//child::a//child::span[text()='Edit']")
	public WebElement editButton;

	@FindBy(how = How.XPATH, using = "//div[@class='box box-information']//child::div//child::a[@class='action change-password']")
	public WebElement changePassword;

	@FindBy(how = How.XPATH, using = "//div[@class='panel header']//child::ul//child::li[@class='my-account-link']")
	public WebElement myAccountLink;

	@FindBy(how = How.XPATH, using = "//div[@class='content account-nav-content']//child::ul//child::li//child::a[text()='My Account']")
	public WebElement myAccount;

	@FindBy(how = How.XPATH, using = "//div[@class='content account-nav-content']//child::ul//child::li//a[text()='My Orders']")
	public WebElement myOrders;

	@FindBy(how = How.XPATH, using = "//input[@id='telephone']")
	public WebElement phoneNumber;

	@FindBy(how = How.XPATH, using = "//input[@id='street_1']")
	public WebElement streetAddress;

	@FindBy(how = How.XPATH, using = "//input[@id='city']")
	public WebElement city;

	@FindBy(how = How.XPATH, using = "//input[@class='input-text validate-zip-international required-entry']")
	public WebElement postCode;

	@FindBy(how = How.XPATH, using = "//input[@id='change-password']")
	public WebElement changePasswordCheckbox;

	@FindBy(how = How.XPATH, using = "//input[@id='change-email']")
	public WebElement changeEmailCheckbox;

	@FindBy(how = How.XPATH, using = "//button[@class='action save primary']")
	public WebElement saveButton;

	@FindBy(how = How.XPATH, using = "//button[@class='action submit primary']")
	public WebElement saveAddressButton;

	@FindBy(how = How.XPATH, using = "//input[@id='current-password']")
	public WebElement currentPassword;

	@FindBy(how = How.XPATH, using = "//span[text()='Out of Stock']")
	public WebElement OutOfStock;

	@FindBy(how = How.XPATH, using = "(//a[@class='action primary tocart tocart--disabled listing'])[1]")
	public WebElement FindOutMore;

	@FindBy(how = How.XPATH, using = "//div[@class='stock out-of-stock']")
	public WebElement Availability;

	@FindBy(how = How.XPATH, using = "//a[text()='VIEW YOUR BASKET']")
	public WebElement ViewYourBasket;

	@FindBy(how = How.XPATH, using = "//a[text()='DELIVERY & RETURNS INFORMATION']")
	public WebElement DeliveryReturns;

	@FindBy(how = How.XPATH, using = "//span[text()='Returns']")
	public WebElement ReturnsTab;

	@FindBy(how = How.XPATH, using = "//select[@class='finder__select js-lookup-make']")
	public WebElement SelectMake;

	@FindBy(how = How.XPATH, using = "//select[@class='finder__select js-lookup-model']")
	public WebElement SelectModel;

	public String SelectmakeDropDown() {
		return "//select[@class='finder__select js-lookup-make']";
	}

	public String SelectmodelDropDown() {
		return "//select[@class='finder__select js-lookup-model']";
	}

	@FindBy(how = How.XPATH, using = "//button[@class='button finder__button']")
	public WebElement ClickSearch;

	@FindBy(how = How.XPATH, using = "(//a[@class='action-primary'])[1]")
	public WebElement AvailableParts;

	@FindBy(how = How.XPATH, using = "//a[@href='/warranty-information']")
	public WebElement WarrantyInformation;

	@FindBy(how = How.XPATH, using = "//a[@data-am-js='filter-item-default']//span[text()='Black']")
	public WebElement FilterbyColor;

	@FindBy(how = How.XPATH, using = "//div[@class='ui-slider-range ui-widget-header ui-corner-all']")
	public WebElement FilterbyPrice;

	@FindBy(how = How.XPATH, using = "//a[@data-am-js='filter-item-default']//span[text()='In Stock'] ")
	public WebElement FilterbyInStock;

	@FindBy(how = How.XPATH, using = "//a[@data-am-js='filter-item-default']//span[text()='Out of Stock']")
	public WebElement FilterbyOutOfStock;

	@FindBy(how = How.XPATH, using = "(//li[@class='authorization-link']//a[contains(@href,'/customer/account/login/referer/')])[1]")
	public WebElement SignInSection;

	@FindBy(how = How.XPATH, using = "//a[@class='action change-password']")
	public WebElement ChangePasswordButton;

	@FindBy(how = How.XPATH, using = "//input[@name='current_password']")
	public WebElement CurrentPasswordTxtBox;

	@FindBy(how = How.XPATH, using = "//input[@name='password']")
	public WebElement NewPasswordTxtBox;

	@FindBy(how = How.XPATH, using = "//input[@name='password_confirmation']")
	public WebElement ConfirmPasswordTxtBox;

	@FindBy(how = How.XPATH, using = "//button[@title='Save']")
	public WebElement PasswordSaveButton;

	@FindBy(how = How.XPATH, using = "(//div[@class='row-full-width-inner']//ul[@class='header links']//a[@href='/customer-service'])[1]")
	public WebElement CustomerServiceLink;

	@FindBy(how = How.XPATH, using = "//div[@aria-labelledby='block-footer_column_1-heading']//a[text()='FAQs']")
	public WebElement ProblemsPlacingOrderLink;

	@FindBy(how = How.XPATH, using = "//h1[@class='page-title-wrapper']")
	public WebElement FAQPageTitle;

	@FindBy(how = How.XPATH, using = "//a[@href='/delivery-returns-policy#shiplist']")
	public WebElement CountryList;

	@FindBy(how = How.XPATH, using = "//a[@href='/delivery-returns-policy#customs']")
	public WebElement CustomsDuties;

	@FindBy(how = How.XPATH, using = "(//a[@class='action edit'])[1]")
	public WebElement EditBtn;

	@FindBy(how = How.XPATH, using = "//input[@name='change_email']")
	public WebElement ChangeEmail;

	@FindBy(how = How.XPATH, using = "//a[contains(@href,'/consignment/')]")
	public WebElement ConsignmentServices;

	@FindBy(how = How.XPATH, using = "//h1[text()='CLASSIC CONSIGNMENT SERVICES']")
	public WebElement ClassicConsignmentServicesTitle;

	@FindBy(how = How.XPATH, using = "//a[contains(@href,'/downloadable/customer/products/')]")
	public WebElement MyDownloadableProductsLink;

	@FindBy(how = How.XPATH, using = "//span[@data-ui-id='page-title-wrapper']")
	public WebElement MyDownloadableProductsTitle;

	@FindBy(how = How.XPATH, using = "//header[@class='page-header']//ul[@class='header links']//li[@class='my-account-link']//a[contains(@href,'/customer/account/')]")
	public WebElement MyAccountSection;

	@FindBy(how = How.XPATH, using = "//div[@class='row-full-width-inner']//ul[@class='header links']//a[contains(@href,'/approved-vehicles/')]")
	public WebElement ClassicVehiclesLink;

	@FindBy(how = How.XPATH, using = "//h1[text()='WORKS LEGENDS']")
	public WebElement WorkLegendPageTitle;

	@FindBy(how = How.XPATH, using = "//div[@class='button module']//a[@title='Experience This Car']")
	public WebElement ExperianceThisCarButton;

	@FindBy(how = How.XPATH, using = "//h1//span[text()='Search Works Legends Enquiry']")
	public WebElement EnquiryPageTitle;

	@FindBy(how = How.XPATH, using = "//button[@data-id='Form_customer_title']")
	public WebElement EnquiryTitleDropdown;

	@FindBy(how = How.XPATH, using = "//a//span[text()='Mr']")
	public WebElement EnquiryTitleMr;

	@FindBy(how = How.XPATH, using = "//input[@id='Form_customer_firstName']")
	public WebElement EnquiryFirstNameTxtBox;

	@FindBy(how = How.XPATH, using = "//input[@id='Form_customer_lastName']")
	public WebElement EnquiryLastNameTxtBox;

	@FindBy(how = How.XPATH, using = "//input[@id='Form_customer_email']")
	public WebElement EnquiryEmailTxtBox;

	@FindBy(how = How.XPATH, using = "//input[@id='Form_customer_phoneNumber']")
	public WebElement EnquiryContactNumberTxtBox;

	@FindBy(how = How.XPATH, using = "//button[@class='btn button']")
	public WebElement ProceedButton;

	@FindBy(how = How.XPATH, using = "//button[@data-action='submit']")
	public WebElement AgreeTAndC;

	@FindBy(how = How.XPATH, using = "//a[@title='Accept Recommended']")
	public WebElement AcceptCookies;

	@FindBy(how = How.XPATH, using = "//span[text()='Thank you for your Source a Vehicle enquiry']")
	public WebElement WorkLegendEnquiryConfirmation;

	@FindBy(how = How.XPATH, using = "//a[@href='/parts/index/brand/key/jaguar/']")
	public WebElement JaguarPartsTab;

	@FindBy(how = How.XPATH, using = "(//a[@class='text_link__has-arrow'])[4]")
	public WebElement SelectJagProd;

	@FindBy(how = How.XPATH, using = "(//a[contains(@href,'/all-products/applicability')])[1]")
	public WebElement ViewAllProductsTab;

	@FindBy(how = How.XPATH, using = "(//a[@class='product-item-link'])[2]")
	public WebElement SelectVehiclePart;

	@FindBy(how = How.XPATH, using = "(//li[@class='authorization-link']//a)[1]")
	public WebElement SignOutBtn;

	@FindBy(how = How.XPATH, using = "//a[text()='TRACK MY ORDER']")
	public WebElement TrackMyOrder;

	@FindBy(how = How.XPATH, using = "//input[@name='oar_order_id']")
	public WebElement OrderID;

	@FindBy(how = How.XPATH, using = "//input[@name='oar_billing_lastname']")
	public WebElement BillingLastName;

	@FindBy(how = How.XPATH, using = "//input[@name='oar_zip']")
	public WebElement BillingZipCode;

	@FindBy(how = How.XPATH, using = "//button[@class='action submit primary']")
	public WebElement ContinueButton;

	@FindBy(how = How.XPATH, using = "//select[@class='select']")
	public WebElement FindOrderBy;

	@FindBy(how = How.XPATH, using = "(//span[@class='field-tooltip-action action-help'])[1]")
	public WebElement AddAccountBtn;

	@FindBy(how = How.XPATH, using = "(//input[@name='billing-address-same-as-shipping'])[1]")
	public WebElement BillingAddressCheckBox;

	@FindBy(how = How.XPATH, using = "//div[@class='choice field']//child::input[@class='checkbox']")
	public WebElement SaveAddressCheckBox;

	@FindBy(how = How.XPATH, using = "//div[@class='field _required _error']//child::input[@name='firstname']")
	public WebElement BillingAddressFirstName;

	@FindBy(how = How.XPATH, using = "//div[@class='field _required _error']//child::input[@name='lastname']")
	public WebElement BillingAddressLastName;

	@FindBy(how = How.XPATH, using = "//select[@name='billing_address_id']")
	public WebElement NewBillingAddressDropdown;

	@FindBy(how = How.XPATH, using = "//div[@name='billingAddresssagepaysuitepi.street.0']//child::input[@name='street[0]']")
	public WebElement BillingAddressStreetName;

	@FindBy(how = How.XPATH, using = "//div[@name='billingAddresssagepaysuitepi.city']//child::input[@name='city']")
	public WebElement BillingAddressCityName;

	@FindBy(how = How.XPATH, using = "//div[@name='billingAddresssagepaysuitepi.postcode']//child::input[@name='postcode']")
	public WebElement BillingAddressPostCode;

	@FindBy(how = How.XPATH, using = "//div[@name='billingAddresssagepaysuitepi.telephone']//child::input[@name='telephone']")
	public WebElement BillingAddressTelephone;

	@FindBy(how = How.XPATH, using = "//button[@class='action action-update']")
	public WebElement UpdateBillingAddressBtn;

	@FindBy(how = How.XPATH, using = "//a[@class='logo']")
	public WebElement LandRoverLogo;

	@FindBy(how = How.XPATH, using = "//fieldset//child::input[@id='customer-email']")
	public WebElement LoginEmail;

	@FindBy(how = How.XPATH, using = "//fieldset//child::input[@id='customer-password']")
	public WebElement LoginPassword;

	@FindBy(how = How.XPATH, using = "//span[text()='Login']")
	public WebElement LoginButton;

	@FindBy(how = How.XPATH, using = "//a[@class='order-number']")
	public WebElement OrderNumber;

	@FindBy(how = How.XPATH, using = "//a[text()='My Orders']")
	public WebElement MyOrders;

	@FindBy(how = How.XPATH, using = "//table[@class='data table table-order-items history']//child::tbody//child::tr[1]//child::td[@class='col id']")
	public WebElement OrderNumberOnOrdersPage;

	@FindBy(how = How.XPATH, using = "//span[text()='View Order']//parent::a")
	public WebElement ViewOrder;

	@FindBy(how = How.XPATH, using = "//tr//strong[text()='Grand Total (Incl.Tax)']//parent::th//following-sibling::td//child::strong//child::span")
	public WebElement GrandTotalInclussiveTax;

	@FindBy(how = How.XPATH, using = "//tr//strong[text()='Grand Total (Excl.Tax)']//parent::th//following-sibling::td//child::strong//child::span")
	public WebElement GrandTotalExclussiveTax;

	@FindBy(how = How.XPATH, using = "//a[text()='REQUEST A RETURN']")
	public WebElement RequestReturnBtn;

	@FindBy(how = How.XPATH, using = "//div[@class='message-error error message']")
	public WebElement VerifyNonExixtingUserErrorMessage;

	@FindBy(how = How.XPATH, using = "//span[text()='In Stock']")
	public WebElement InStockItems;

	@FindBy(how = How.XPATH, using = "//a[text()='Buy Now']")
	public WebElement BuyNowBtn;

	@FindBy(how = How.XPATH, using = "//div[@class='payment-method']//child::input[@name='payment[method]']")
	public WebElement CardOption;

	@FindBy(how = How.XPATH, using = "//div[@class='content account-nav-content']//child::ul//child::li//a[text()='Order by SKU']")
	public WebElement OrderBySKU;

	@FindBy(how = How.XPATH, using = "//input[@name='items[0][sku]']")
	public WebElement SKUID;

	@FindBy(how = How.XPATH, using = "//input[@name='items[0][qty]']")
	public WebElement Quantity;

	@FindBy(how = How.XPATH, using = "//button[@class='action tocart primary']")
	public WebElement AddToBasketBtn;

	@FindBy(how = How.XPATH, using = "//a[text()='PRIVACY POLICY']")
	public WebElement PrivacyPolicySection;

	@FindBy(how = How.XPATH, using = "//div[@class='block-content content']//div//ul//li//a[text()='CONTACT US']")
	public WebElement ContactUsBtn;

	@FindBy(how = How.XPATH, using = "//span[text()='Privacy Policy']")
	public WebElement PrivacyPolicyInformation;

	@FindBy(how = How.XPATH, using = "//select[@name='how_help']")
	public WebElement HelpSelect;

	@FindBy(how = How.XPATH, using = "//span[text()='Country']//parent::label//following-sibling::div//select")
	public WebElement CountrySelection;

	@FindBy(how = How.XPATH, using = "//textarea[@name='comment']")
	public WebElement CommentText;

	@FindBy(how = How.XPATH, using = "//button[@title='Submit']")
	public WebElement SubmitBtn;

	@FindBy(how = How.XPATH, using = "//div[@class='message-success success message']")
	public WebElement SubmitSuccessfullMessage;

	@FindBy(how = How.XPATH, using = "//input[@name='vin']")
	public WebElement VINNumber;

	@FindBy(how = How.XPATH, using = "//a[text()='PURCHASE TERMS & CONDITIONS']")
	public WebElement TermsandConditionSection;

	@FindBy(how = How.XPATH, using = "//a[text()='PURCHASE TERMS & CONDITIONS']")
	public WebElement TermsandConditionInformation;

	@FindBy(how = How.XPATH, using = "//a[text()='JAGUAR LAND ROVER CLASSIC PARTS']")
	public WebElement JagAndLRLink;

	@FindBy(how = How.XPATH, using = "//span[text()=' JAGUAR LAND ROVER CLASSIC PARTS']")
	public WebElement JagAndLRText;

	@FindBy(how = How.XPATH, using = "(//span[text()='FACEBOOK'])[1]")
	public WebElement LRFacebook;

	@FindBy(how = How.XPATH, using = "(//span[text()='FACEBOOK'])[2]")
	public WebElement JagFacebook;

	@FindBy(how = How.XPATH, using = "(//span[text()='TWITTER'])[1]")
	public WebElement LRTwitter;

	@FindBy(how = How.XPATH, using = "(//span[text()='TWITTER'])[2]")
	public WebElement JagTwitter;

	@FindBy(how = How.XPATH, using = "(//span[text()='YOUTUBE'])[1]")
	public WebElement LRYoutube;

	@FindBy(how = How.XPATH, using = "(//span[text()='YOUTUBE'])[2]")
	public WebElement JagYoutube;

	@FindBy(how = How.XPATH, using = "(//span[text()='INSTAGRAM'])[1]")
	public WebElement LRInstagram;

	@FindBy(how = How.XPATH, using = "(//span[text()='INSTAGRAM'])[2]")
	public WebElement JagInstagram;

	@FindBy(how = How.XPATH, using = "(//a[text()='NEWS'])[1]")
	public WebElement NewsHeader;

	@FindBy(how = How.XPATH, using = "//span[text()='News and Events']")
	public WebElement NewsAndEventsText;

	@FindBy(how = How.XPATH, using = "//div[@class='section-item-content nav-sections-item-content']//ul[@class='none ub-mega-menu level0  accordion-root ']//a[contains(@href,'/offers')]")
	public WebElement OffersSection;

	@FindBy(how = How.XPATH, using = "//section[@class='offer lr']//span[@class='pagebuilder-button-primary']")
	public WebElement ShopNowButton;

	@FindBy(how = How.XPATH, using = "(//header[@class='modal-header']//following-sibling::div//div[@class='block-content content']//ol[@class='products list items product-items']//form[@data-role='tocart-form']//button[@title=' Add to Basket'])[1]")
	public WebElement AddItemToBasketBtnFromPopUp;

	@FindBy(how = How.XPATH, using = "//a[@class='js-qty-add control__action control__action--add']")
	public WebElement ProductQunatity;

	@FindBy(how = How.XPATH, using = "(//a[@class='text_link__has-arrow'])[2]")
	public WebElement SelectsmallJagProd;

	public String CardExpMonth() {
		return "//select[@class='select select-month']";
	}

	public String CardExpYear() {
		return "//select[@class='select select-year']";
	}

	@FindBy(how = How.XPATH, using = "//div[text()='This is a required field.']")
	public WebElement ContactUsErrorMessage;

	@FindBy(how = How.XPATH, using = "//a[@class='action remind']")
	public WebElement ForgetPasswordLink;

	@FindBy(how = How.XPATH, using = "//input[@name='email']")
	public WebElement EmailIDForResetPassword;

	@FindBy(how = How.XPATH, using = "//span[text()='Reset My Password']//parent::button")
	public WebElement ResetPasswordBtn;

	@FindBy(how = How.XPATH, using = "//span[@class='message error']")
	public WebElement IncorrectPaymentDetailsErrorMessage;

	@FindBy(how = How.XPATH, using = "//a[text()='Set a New Password']")
	public WebElement SetNewPasswordLink;

	@FindBy(how = How.XPATH, using = "//div//span[text()='New Password']//parent::label//following-sibling::div//child::input")
	public WebElement NewPassword;

	@FindBy(how = How.XPATH, using = "//span[text()='Set a New Password']//parent::button")
	public WebElement SetANewpasswordLink;

	@FindBy(how = How.XPATH, using = "//div[text()='You updated your password.']")
	public WebElement PasswordUpdateConfirmationMessage;

	@FindBy(how = How.XPATH, using = "//a[@class='action print']")
	public WebElement PrintReceiptBtn;

	@FindBy(how = How.XPATH, using = "//cr-button[@class='action-button']")
	public WebElement PrintBtn;

	@FindBy(how = How.XPATH, using = "//input[@name='newsletter-subscribe']")
	public WebElement UnsubscribeNewsletter;

	@FindBy(how = How.XPATH, using = "//a[@href='https://shop.jaguar.com/']")
	public WebElement ShopJaguarFromFooter;

	@FindBy(how = How.XPATH, using = "//a[@href='https://shop.landrover.com/']")
	public WebElement ShopLandRoverFromFooter;

	@FindBy(how = How.XPATH, using = "//input[@class='glDefaultBtn saveBtn']")
	public WebElement SaveDestinationBtn;

	@FindBy(how = How.XPATH, using = "//a[text()='View all parts for this vehicle']")
	public WebElement ViewAllPartsBtn;

	@FindBy(how = How.XPATH, using = "//span[@class='count-number']")
	public WebElement NumberOfPartsAvailable;

	@FindBy(how = How.XPATH, using = "//tr[@class='grand_total']//child::td//child::span[@class='price']")
	public WebElement GrandTotalExclussiveTaxOnInvoiceMail;

	@FindBy(how = How.XPATH, using = "//tr[@class='grand_total_incl']//child::td//child::span[@class='price']")
	public WebElement GrandTotalInclussiveTaxOnInvoiceMail;

	@FindBy(how = How.XPATH, using = "//h1//child::span[@class='no-link']")
	public WebElement OrderNumberOnInvoiceMail;

	@FindBy(how = How.XPATH, using = "//input[@id='username']")
	public WebElement MagentoAdminUsername;

	@FindBy(how = How.XPATH, using = "//input[@id='login']")
	public WebElement MagentoAdminPassword;

	@FindBy(how = How.XPATH, using = "//span[text()='Sign in']//parent::button")
	public WebElement MagentoAdminSignInBtn;

	@FindBy(how = How.XPATH, using = "//span[text()='Sales']//parent::a")
	public WebElement SalesSection;

	@FindBy(how = How.XPATH, using = "//span[text()='Invoices']//parent::a")
	public WebElement InvoicesTab;

	@FindBy(how = How.XPATH, using = "//input[text()='Search by keyword']")
	public WebElement SearchKeywordBox;

	@FindBy(how = How.XPATH, using = "//div[@class='data-grid-search-control-wrap']//child::button[@class='action-submit']")
	public WebElement SearchBtn;

	@FindBy(how = How.XPATH, using = "//a[text()='View']")
	public WebElement ViewInvoice;

	@FindBy(how = How.XPATH, using = "//textarea[@name='comment[comment]']")
	public WebElement CommentTextField;

	@FindBy(how = How.XPATH, using = "//input[@name='comment[is_customer_notified]']")
	public WebElement CustomerNotifiedCheckBox;

	@FindBy(how = How.XPATH, using = "//span[text()='Submit Comment']//parent::button")
	public WebElement SubmitUpdate;

	@FindBy(how = How.XPATH, using = "//p[contains(text(),'has been updated')]")
	public WebElement InvoiceUpdateMail;

	@FindBy(how = How.XPATH, using = "//span[text()='Customers']//parent::a")
	public WebElement CustomerSection;

	@FindBy(how = How.XPATH, using = "//span[text()='All Customers']//parent::a")
	public WebElement AllCustomersTab;

	@FindBy(how = How.XPATH, using = "(//a[text()='Edit'])[1]")
	public WebElement EditOrderBtn;

	@FindBy(how = How.XPATH, using = "//ul[@class='admin__page-nav-items items']//child::li//child::span[text()='Orders']//parent::a")
	public WebElement OrdersSection;

	@FindBy(how = How.XPATH, using = "(//table[@class='data-grid']//tbody//tr[2]/td)[1]")
	public WebElement SelectOrder;

	@FindBy(how = How.XPATH, using = "//span[text()='Invoice']//parent::button")
	public WebElement InvoiceCreationBtn;

	@FindBy(how = How.XPATH, using = "//input[@name='invoice[send_email]']")
	public WebElement EmailCopyCheckBox;

	@FindBy(how = How.XPATH, using = "//button[@title='Submit Invoice']")
	public WebElement SubmitInvoice;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Your Invoice')]")
	public WebElement NewInvoiceVerificationOnMail;

	@FindBy(how = How.XPATH, using = "//button[text()='Filters']")
	public WebElement FilterBtn;

	@FindBy(how = How.XPATH, using = "//span[text()='Blocks']//parent::a")
	public WebElement BlocksTab;

	@FindBy(how = How.XPATH, using = "//span[text()='Content']//parent::a")
	public WebElement ContentSection;

	@FindBy(how = How.XPATH, using = "//input[@name='title']")
	public WebElement FooterTitle;

	@FindBy(how = How.XPATH, using = "//span[text()='Apply Filters']//parent::button")
	public WebElement ApplyFilterButton;

	@FindBy(how = How.XPATH, using = "//span[text()='Reports']//parent::a")
	public WebElement ReportsSection;

	@FindBy(how = How.XPATH, using = "//span[text()='Abandoned Carts']//parent::a")
	public WebElement AbondonedCartTab;

	@FindBy(how = How.XPATH, using = "//div[@class='admin__control-support-text']")
	public WebElement NumberOfRecordsFound;

	@FindBy(how = How.XPATH, using = "//select[@name='gridAbandoned_export']")
	public WebElement ReportsDropdown;

	public String ReportTypeSelection(String Value) {
		return "//option[text() = '" + Value + "']";
	}

	@FindBy(how = How.XPATH, using = "//option[text()='CSV']")
	public WebElement SelectReportType;

	@FindBy(how = How.XPATH, using = "//span[text()='Export']//parent::button")
	public WebElement ExportBtn;

	@FindBy(how = How.XPATH, using = "//span[text()='Coupons']//parent::a")
	public WebElement CouponsTab;

	@FindBy(how = How.XPATH, using = "//span[text()='Show Report']//parent::button")
	public WebElement ShowReportBtn;

	@FindBy(how = How.XPATH, using = "//span[text()='Marketing']//parent::a")
	public WebElement MarketingSection;

	@FindBy(how = How.XPATH, using = "//span[text()='Newsletter Subscribers']//parent::a")
	public WebElement NewsletterSubscriptionTab;

	@FindBy(how = How.XPATH, using = "//input[@name='email']")
	public WebElement EmailTextArea;

	@FindBy(how = How.XPATH, using = "//span[text()='Search']//parent::button")
	public WebElement SearchButton;

	@FindBy(how = How.XPATH, using = "//tbody//child::tr//child::td//child::label//child::input[@name='subscriber']")
	public WebElement SelectCheckBox;

	@FindBy(how = How.XPATH, using = "//select[@class='required-entry local-validation admin__control-select ']")
	public WebElement ActionsDropdown;

	@FindBy(how = How.XPATH, using = "//option[text()='Unsubscribe']")
	public WebElement SelectUnsubscribe;

	@FindBy(how = How.XPATH, using = "//span[text()='Submit']//parent::button")
	public WebElement SubmitButton;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'A total of ')]")
	public WebElement VerifyUpdateMessage;

	@FindBy(how = How.XPATH, using = "//tbody//tr[1]//td[7]")
	public WebElement UnsubscriptionUpdate;

	@FindBy(how = How.XPATH, using = "//button[text()='Select']")
	public WebElement SelectBtn;

	@FindBy(how = How.XPATH, using = "//li//child::a[text()='Edit']")
	public WebElement EditButton;

	@FindBy(how = How.XPATH, using = "//div[@class='admin__field-control']//input[@name='title']")
	public WebElement EditFooterTitle;

	@FindBy(how = How.XPATH, using = "//span[text()='Save']//parent::button")
	public WebElement FooterSaveBtn;

	@FindBy(how = How.XPATH, using = "//span[text()='Back']//parent::button")
	public WebElement BackBtn;

	@FindBy(how = How.XPATH, using = "//table[@class='data-grid data-grid-draggable']//child::tbody//child::tr[2]//child::td[3]")
	public WebElement VerifyFooterTitle;

	@FindBy(how = How.XPATH, using = "//span[text()='Stores']//parent::a")
	public WebElement StoresSection;

	@FindBy(how = How.XPATH, using = "//li[@class='item-sales-order    level-2']//child::span[text()='Orders']//parent::a")
	public WebElement OrdersTab;

	@FindBy(how = How.XPATH, using = "//input[@name='created_at[from]']")
	public WebElement PurchaseFromDate;

	@FindBy(how = How.XPATH, using = "//input[@name='created_at[to]']")
	public WebElement PurchaseToDate;

	@FindBy(how = How.XPATH, using = "//table[@class='data-grid data-grid-draggable']//child::tbody//child::tr//child::td[4]")
	public WebElement PurchaseDate;

	@FindBy(how = How.XPATH, using = "(//table[@class='data-grid data-grid-draggable']//child::thead//child::tr//child::th[4])[2]")
	public WebElement PurchaseDateSort;

	@FindBy(how = How.XPATH, using = "//span[text()='Cart Price Rules']//parent::a")
	public WebElement CartPriceRulesTab;

	@FindBy(how = How.XPATH, using = "//span[text()='Add New Rule']//parent::button")
	public WebElement AddNewRuleBtn;

	@FindBy(how = How.XPATH, using = "//input[@name='name']")
	public WebElement RuleName;

	@FindBy(how = How.XPATH, using = "//option[text()='LR UK']")
	public WebElement SelectWebsites;

	@FindBy(how = How.XPATH, using = "//option[text()='General']")
	public WebElement SelectGroups;

	@FindBy(how = How.XPATH, using = "//select[@name='coupon_type']")
	public WebElement SelectCouponsType;

	@FindBy(how = How.XPATH, using = "//option[text()='Specific Coupon']")
	public WebElement SelectSpecificCoupon;

	@FindBy(how = How.XPATH, using = "//input[@name='coupon_code']")
	public WebElement CouponCode;

	@FindBy(how = How.XPATH, using = "//span[text()='Actions']")
	public WebElement Actions;

	@FindBy(how = How.XPATH, using = "//select[@name='simple_action']")
	public WebElement AppliedDiscountType;

	@FindBy(how = How.XPATH, using = "//option[text()='Percent of product price discount']")
	public WebElement PercentOfProductPriceDiscount;

	@FindBy(how = How.XPATH, using = "//input[@name='discount_amount']")
	public WebElement DiscountAmount;

	@FindBy(how = How.XPATH, using = "//span[text()='Save']//parent::button")
	public WebElement SaveCouponBtn;

	@FindBy(how = How.XPATH, using = "//table[@class='data-grid data-grid-draggable']//child::tbody//child::tr//child::td[13]")
	public WebElement CardSecure;

	@FindBy(how = How.XPATH, using = "//table[@class='data-grid data-grid-draggable']//child::tbody//child::tr//child::td[14]")
	public WebElement CardAddress;

	@FindBy(how = How.XPATH, using = "//table[@class='data-grid data-grid-draggable']//child::tbody//child::tr//child::td[15]")
	public WebElement CardPostCode;

	@FindBy(how = How.XPATH, using = "//table[@class='data-grid data-grid-draggable']//child::tbody//child::tr//child::td[16]")
	public WebElement CardCV2;

	@FindBy(how = How.XPATH, using = "//div[text()='You saved the rule.']")
	public WebElement VerifyNewCouponRuleCreatedMessage;

	@FindBy(how = How.XPATH, using = "//h1[@class='page-title']")
	public WebElement VerifyInvoicePage;

	@FindBy(how = How.XPATH, using = "//select[@name='status']")
	public WebElement StatusDropdown;

	@FindBy(how = How.XPATH, using = "//option[text()='Unsubscribed']")
	public WebElement SelectUnsubscribed;

	@FindBy(how = How.XPATH, using = "//li[@class='item-menu parent last level-0']")
	public WebElement UberThemeExtensions;

	@FindBy(how = How.XPATH, using = "//span[text()='Ubertheme Marketplace']//parent::a")
	public WebElement UberthemeMarketPlace;

	@FindBy(how = How.XPATH, using = "//a[text()='Got it!']")
	public WebElement GotItCoockies;

	@FindBy(how = How.XPATH, using = "//span[@itemprop='name']")
	public WebElement MagentoExtension;

	@FindBy(how = How.XPATH, using = "//a[text()='Magento 1']")
	public WebElement Magento1;

	@FindBy(how = How.XPATH, using = "//input[@id='sales_report_from']")
	public WebElement SalesFromDate;

	@FindBy(how = How.XPATH, using = "//input[@id='sales_report_to']")
	public WebElement SalesToDate;

	@FindBy(how = How.XPATH, using = "//span[text()='Attribute Set']//parent::a")
	public WebElement AttributeSet;

	@FindBy(how = How.XPATH, using = "//table[@class='data-grid']")
	public WebElement AttributeSetTable;

	@FindBy(how = How.XPATH, using = "//span[text()='Fraud ']//parent::a")
	public WebElement Fraud;

	@FindBy(how = How.XPATH, using = "//span[text()='Fraud ']//parent::a")
	public WebElement FraudOrders;

	@FindBy(how = How.XPATH, using = "//a[@id='CybotCookiebotDialogBodyEdgeMoreDetailsLink']")
	public WebElement ShowDetails;

	@FindBy(how = How.XPATH, using = "(//input[@class='CybotCookiebotDialogBodyLevelButton CybotCookiebotDialogBodyLevelConsentCheckbox'])[4]")
	public WebElement SelectToggle;

	@FindBy(how = How.XPATH, using = "//button[@id='CybotCookiebotDialogBodyButtonDecline']")
	public WebElement DenyButton;

	@FindBy(how = How.XPATH, using = "//span[text()='Stores']")
	public WebElement Stores;

	@FindBy(how = How.XPATH, using = "//div[@class='submenu']//ul[@role='menu']//li[@class='item-system-config    level-2']//a[contains(@href,'/system_config')]")
	public WebElement Configuration;

	@FindBy(how = How.XPATH, using = "(//div[@class='admin__page-nav-title title _collapsible'])[3]")
	public WebElement General;

	@FindBy(how = How.XPATH, using = "//ul[@class='admin__page-nav-items items']//child::li//child::span[text()='Web']//parent::a")
	public WebElement General_Web;

	@FindBy(how = How.XPATH, using = "(//div[@class='admin__page-nav-title title _collapsible'])[8]")
	public WebElement LR_submenu;

	@FindBy(how = How.XPATH, using = "(//ul[@class='admin__page-nav-items items']//child::li//child::span[text()='General']//parent::a)[2]")
	public WebElement LR_General;

	@FindBy(how = How.XPATH, using = "(//div[@class='admin__page-nav-title title _collapsible'])[11]")
	public WebElement Sales_submenu;

	@FindBy(how = How.XPATH, using = "//ul[@class='admin__page-nav-items items']//child::li//child::span[text()='Shipping Settings']//parent::a")
	public WebElement Sales_shipping;

	@FindBy(how = How.XPATH, using = "//span[text()='Save Config']")
	public WebElement Save_config;

	@FindBy(how = How.XPATH, using = "//div[text()='You saved the configuration.']")
	public WebElement Save_success_message;

	@FindBy(how = How.XPATH, using = "//a[contains(@href,'https://shopdev.landrover.co.uk/admin_1dwlvb/sales/order')]//child::span[text()='Orders']")
	public WebElement BackendOrdersPage;

	@FindBy(how = How.XPATH, using = "//button[@title='Create New Order']")
	public WebElement CreateNewOrderBtn;

	@FindBy(how = How.XPATH, using = "//input[@name='name']")
	public WebElement NewOrderCustName;

	public String CustNameSelection(String custName) {
		return "//table[@id='sales_order_create_customer_grid_table']//child::tbody//child::tr//child::td[contains(text(), '"
				+ custName + "')]";
	}

	public String StoreSelection(String store) {
		return "//label[contains(text(), '" + store + "')]";
	}

	@FindBy(how = How.XPATH, using = "//button[@id='add_products']")
	public WebElement NewOrderAddProducts;

	@FindBy(how = How.XPATH, using = "//input[@id='sales_order_create_search_grid_filter_price_from']")
	public WebElement NewOrderFromValue;

	@FindBy(how = How.XPATH, using = "//input[@id='sales_order_create_search_grid_filter_price_to']")
	public WebElement NewOrderToValue;

	@FindBy(how = How.XPATH, using = "(//input[@class='checkbox admin__control-checkbox'])[1]")
	public WebElement SelectFirstProduct;

	@FindBy(how = How.XPATH, using = "//button[@title='Add Selected Product(s) to Order']")
	public WebElement AddSelectedProductOrderBtn;

	@FindBy(how = How.XPATH, using = "//input[@id='order-billing_address_firstname']")
	public WebElement NewOrderFirstName;

	@FindBy(how = How.XPATH, using = "//input[@id='order-billing_address_lastname']")
	public WebElement NewOrderLastName;

	@FindBy(how = How.XPATH, using = "//input[@id='order-billing_address_street0']")
	public WebElement NewOrderStreetAddress;

	@FindBy(how = How.XPATH, using = "//input[@id='order-billing_address_city']")
	public WebElement NewOrderCityName;

	@FindBy(how = How.XPATH, using = "//input[@id='order-billing_address_postcode']")
	public WebElement NewOrderPostalCode;

	@FindBy(how = How.XPATH, using = "//input[@id='order-billing_address_telephone']")
	public WebElement NewOrderTelephone;

	@FindBy(how = How.XPATH, using = "//span[text()='Get shipping methods and rates']//parent::a[@class='action-default']")
	public WebElement NewOrderShippingMethod;

	@FindBy(how = How.XPATH, using = "//span[text()='Get available payment methods']//parent::a[@class='action-default']")
	public WebElement NewOrderPaymentMethod;

	@FindBy(how = How.XPATH, using = "//input[@id='p_method_cashondelivery']//parent::dt//child::label[contains(text(),'Cash On Delivery')]")
	public WebElement NewOrderPaymentMethodCOD;

	@FindBy(how = How.XPATH, using = "//input[@id='s_method_jlr_admin_free_shipping_jlr_admin_free_shipping']//parent::li//child::label[contains(text(),'Admin Free Shipping')]")
	public WebElement NewOrderShippingMethodFreeShipping;

	@FindBy(how = How.XPATH, using = "//button[@id='submit_order_top_button']")
	public WebElement NewOrderSubmitBtn;

	@FindBy(how = How.XPATH, using = "//div[@class='message message-success success']")
	public WebElement NewOrderCreationSuccessMessage;

	@FindBy(how = How.XPATH, using = "(//div[@class='admin__page-section-item order-information'])[1]")
	public WebElement NewOrderInformation;

	@FindBy(how = How.XPATH, using = "(//div[@class='admin__page-section-item-title']//child::span[@class='title'])[1]")
	public WebElement NewOrderNumber;

	@FindBy(how = How.XPATH, using = "//a[contains(@href,'https://shopdev.landrover.co.uk/admin_1dwlvb/admin/cache')]//child::span[text()='Cache Management']")
	public WebElement MagentoBackendCacheManagement;

	@FindBy(how = How.XPATH, using = "(//table[@id='cache_grid_table']//child::tbody//child::tr//child::td//child::label[@class='data-grid-checkbox-cell-inner']//child::input)[1]")
	public WebElement CacheFirstItem;

	@FindBy(how = How.XPATH, using = "//button[@id='flush_magento']")
	public WebElement FlushMagentoBtn;

	@FindBy(how = How.XPATH, using = "//div[@class='message message-success success']")
	public WebElement FlushSuccessMessage;

	@FindBy(how = How.XPATH, using = "//div[@class='message message-success success']//child::div")
	public WebElement FlushSuccessMessageText;

	@FindBy(how = How.XPATH, using = "//div[@class='dashboard-container row']")
	public WebElement MagentoBackendDashboard;

	@FindBy(how = How.XPATH, using = "//span[@class='admin__action-dropdown-text']//span")
	public WebElement MagentoBackendUserName;

	@FindBy(how = How.XPATH, using = "((//table[@class = 'data-grid data-grid-draggable'])[2]//child::tbody//child::td//following-sibling::td//child::div[@class='data-grid-cell-content'])[1]")
	public WebElement MagentoBackendDashboardTable;

	@FindBy(how = How.XPATH, using = "//button[@id= 'back']")
	public WebElement MagentoBackendBackBtn;

	@FindBy(how = How.XPATH, using = "//a[contains(@href,'https://shopdev.landrover.co.uk/admin_1dwlvb/sales/invoice')]//child::span[text()='Invoices']")
	public WebElement MagentoBackendInvoice;

	@FindBy(how = How.XPATH, using = "//a[contains(@href,'https://shopdev.landrover.co.uk/admin_1dwlvb/sales/shipment')]//child::span[text()='Shipments']")
	public WebElement MagentoBackendShipments;

	@FindBy(how = How.XPATH, using = "//a[contains(@href,'https://shopdev.landrover.co.uk/admin_1dwlvb/sales/creditmemo')]//child::span[text()='Credit Memos']")
	public WebElement MagentoBackendCreditMemo;

	@FindBy(how = How.XPATH, using = "//a[contains(@href,'https://shopdev.landrover.co.uk/admin_1dwlvb/admin/rma')]//child::span[text()='Returns']")
	public WebElement MagentoBackendReturns;

	@FindBy(how = How.XPATH, using = "//a[contains(@href,'https://shopdev.landrover.co.uk/admin_1dwlvb/paypal/billing_agreement')]//child::span[text()='Billing Agreements']")
	public WebElement MagentoBackendBillingAgreements;

	@FindBy(how = How.XPATH, using = "//a[contains(@href,'https://shopdev.landrover.co.uk/admin_1dwlvb/sales/transactions')]//child::span[text()='Transactions']")
	public WebElement MagentoBackendTransactions;

	@FindBy(how = How.XPATH, using = "((//table[@class = 'data-grid data-grid-draggable'])[1]//child::tbody//child::td//following-sibling::td//child::div[@class='data-grid-cell-content'])[1]")
	public WebElement FirstOrderFromList;

	@FindBy(how = How.XPATH, using = "(//table[@class = 'data-grid']//child::tbody//child::tr//child::td[contains(@class,' col-rma-number col-increment_id')])[1]")
	public WebElement FirstReturnOrderFromList;

	@FindBy(how = How.XPATH, using = "//table[@id= 'billing_agreements_table']")
	public WebElement MagentoBackendBillingAgreementMessage;

	@FindBy(how = How.XPATH, using = "//table[@id= 'billing_agreements_table']//child::tbody//child::tr//child::td")
	public WebElement MagentoBackendBillingAgreementMessageText;

	@FindBy(how = How.XPATH, using = "(//table[@class = 'data-grid']//child::tbody//child::tr//child::td[contains(@class,'col-id col-transaction_id col-number')])[1]")
	public WebElement FirstTransactionFromList;

	@FindBy(how = How.XPATH, using = "//li[@id = 'menu-magento-catalog-catalog']")
	public WebElement MagentoBackendCatalog;

	@FindBy(how = How.XPATH, using = "//span[text()= 'Plumrocket']//parent::a")
	public WebElement MagentoBackendPlumrocket;

	@FindBy(how = How.XPATH, using = "//li[@id= 'menu-menuamasty-base-menu']")
	public WebElement MagentoBackendAmasty;

	@FindBy(how = How.XPATH, using = "//span[text()= 'Magic Toolbox']//parent::a")
	public WebElement MagentoBackendMagicToolBox;

	@FindBy(how = How.XPATH, using = "//span[text()= 'Wyomind']//parent::a")
	public WebElement MagentoBackendWyomind;

	@FindBy(how = How.XPATH, using = "//span[text()= 'Jlr']//parent::a")
	public WebElement MagentoBackendJLR;

	@FindBy(how = How.XPATH, using = "//span[text()='Reorder']")
	public WebElement ReorderButton;

	@FindBy(how = How.XPATH, using = "//span[text()='New']")
	public WebElement NewCustomerReportTab;

	@FindBy(how = How.XPATH, using = "//input[@name='report_from']")
	public WebElement CustomerReportFromDateTxtBx;

	@FindBy(how = How.XPATH, using = "//input[@name='report_to']")
	public WebElement CustomerReportToDateTxtBx;

	@FindBy(how = How.XPATH, using = "//button[@data-ui-id='adminhtml-report-grid-refresh-button']")
	public WebElement ReportRefreshButton;

	@FindBy(how = How.XPATH, using = "//span[text()='New Accounts']")
	public WebElement NewAccountsReport;

	@FindBy(how = How.XPATH, using = "//span[text()='Manage Menu']//parent::a")
	public WebElement ManageMenu;

	@FindBy(how = How.XPATH, using = "(//button[text()='Select'])[1]")
	public WebElement SelectItem;

	@FindBy(how = How.XPATH, using = "//select[@title='Menu Group Status']")
	public WebElement MenuStatus;

	@FindBy(how = How.XPATH, using = "//input[@name='invoice[comment_customer_notify]']")
	public WebElement AppendComments;

	@FindBy(how = How.XPATH, using = "//textarea[@name='invoice[comment_text]']")
	public WebElement Comments;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Provided Zip/Postal Code seems to be invalid.')]")
	public WebElement PostCodeInvalidWarningMessage;

	@FindBy(how = How.XPATH, using = "//a[@class='js-qty-sub control__action control__action--sub']//span[text()='Subtract']")
	public WebElement ReduceProductQuantityBtn;

	@FindBy(how = How.XPATH, using = "//div[@class='control control__has-actions']//child::a[@class='js-qty-sub control__action control__action--sub ']")
	public WebElement ReduceProductQuantityButton;

	@FindBy(how = How.XPATH, using = "//div[@class='mage-error']")
	public WebElement QuantityReductionErrorMessage;

	@FindBy(how = How.XPATH, using = "//div[@class='mage-error']")
	public WebElement TermsandConditionsErrorMessage;

	@FindBy(how = How.XPATH, using = "//a[@class='js-qty-add control__action control__action--add']//span[text()='Add']")
	public WebElement QuantityChangeOnShippingPage;

	@FindBy(how = How.XPATH, using = "//div[@class='field-error']")
	public WebElement MandatoryFieldErrorMessage;

	@FindBy(how = How.XPATH, using = "//div[@class='message-error error message']")
	public WebElement ErrorMessageWhileRegisteringWithExistingUser;

	public String SortBySection() {
		return "//div[@class='sidebar sidebar-main']//child::select[@id='sorter']";
	}

	public String SortByProductName(String Value) {
		return "//option[text() = '" + Value + "']";
	}

	public String FilterByPrice() {
		return "//a[@class='ui-slider-handle ui-state-default ui-corner-all']";
	}

	@FindBy(how = How.XPATH, using = "//div[@class='message-error error message']")
	public WebElement IncorrectSignin;

	@FindBy(how = How.XPATH, using = "//div[@class='message-error error message']")
	public WebElement IncorrectPassword;

	@FindBy(how = How.XPATH, using = "(//a[@class='pagebuilder-button-primary'])[2]")
	public WebElement NewsReadMore;

	@FindBy(how = How.XPATH, using = "(//span[@class='pagebuilder-button-primary'])[4]")
	public WebElement HomeShopNow;

	@FindBy(how = How.XPATH, using = "//input[@name='email']")
	public WebElement NewOrderCustEmail;

	@FindBy(how = How.XPATH, using = "//a[@href=\"https://partsdev.jaguarlandroverclassic.com/customer/account/forgotpassword/\"]")
	public WebElement CPForgetPasswordLink;

	@FindBy(how = How.XPATH, using = "//span[text()='Out of Stock']//parent::a")
	public WebElement OutOfStockLink;

	@FindBy(how = How.XPATH, using = "(//a[@class='product-item-link'])[1]")
	public WebElement OutOfStockFirstProduct;

	@FindBy(how = How.XPATH, using = "//a[contains(@title,'Notify me ')]")
	public WebElement NotifyMeProduct;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Alert subscription')]")
	public WebElement AlertMessage;

	@FindBy(how = How.XPATH, using = "//button[@class='action action-show-popup']")
	public WebElement NewAddressBtn;

	@FindBy(how = How.XPATH, using = "//input[@class='input-text qty']")
	public WebElement ProductQuantity;

	@FindBy(how = How.XPATH, using = "//span[@class='counter-number']")
	public WebElement BasketCount;

	@FindBy(how = How.XPATH, using = "//a[text()='Pay Now']")
	public WebElement PayNowBtn;

	@FindBy(how = How.XPATH, using = "//a[contains(@href,'/giftregistry/')]")
	public WebElement GiftRegistry;

	@FindBy(how = How.XPATH, using = "//a[@class='action primary add']")
	public WebElement AddNewGiftRegistryBtn;

	public String GiftRegistryTypeDropDown() {
		return "//select[@name='type_id']";
	}

	@FindBy(how = How.XPATH, using = "//button[@title='Next']")
	public WebElement GiftRegistryNextButton;

	@FindBy(how = How.XPATH, using = "//input[@name='title']")
	public WebElement EventTitle;

	@FindBy(how = How.XPATH, using = "//textarea[@name='message']")
	public WebElement MessageTextBox;

	public String PrivacyDropDown() {
		return "//select[@name='is_public']";
	}

	public String StatusDropDown() {
		return "//select[@name='is_active']";
	}

	@FindBy(how = How.XPATH, using = "//span[text()='First Name']//parent::label//following-sibling::div//input")
	public WebElement FirstNameTxtBox;

	@FindBy(how = How.XPATH, using = "//span[text()='Last Name']//parent::label//following-sibling::div//input")
	public WebElement LastNameTxtBox;

	@FindBy(how = How.XPATH, using = "//span[text()='Email']//parent::label//following-sibling::div//input")
	public WebElement EmailTxtBox;

	@FindBy(how = How.XPATH, using = "//input[@id='firstname']")
	public WebElement FirstNameTxtBx;

	@FindBy(how = How.XPATH, using = "//input[@id='lastname']")
	public WebElement LastNameTxtBx;

	@FindBy(how = How.XPATH, using = "//input[@id='address:street1']")
	public WebElement AddressTxtBx;

	@FindBy(how = How.XPATH, using = "//input[@id='address:city']")
	public WebElement CityTxtBx;

	@FindBy(how = How.XPATH, using = "//input[@id='address:postcode']")
	public WebElement PostCodeTxtBx;

	@FindBy(how = How.XPATH, using = "//input[@id='address:telephone']")
	public WebElement PhoneNumberTxtBx;

	@FindBy(how = How.XPATH, using = "//button[@id='submit.save']")
	public WebElement SaveBtn;

	@FindBy(how = How.XPATH, using = "//select[@id='giftregistry_entity']")
	public WebElement GiftRegistryEntity;

	@FindBy(how = How.XPATH, using = "//input[@id='customer_sku_csv']")
	public WebElement ChooseFile;

	@FindBy(how = How.XPATH, using = "//div[text()='Please upload the file in .csv format.']")
	public WebElement invalidformaterror;

	@FindBy(how = How.XPATH, using = "//button[@class='action reset']")
	public WebElement ResetButton;

	@FindBy(how = How.XPATH, using = "(//span[@class='amshopby-choice'])[1]")
	public WebElement FirstModel;

	@FindBy(how = How.XPATH, using = "(//span[@class='amshopby-choice'])[2]")
	public WebElement SecondModel;

	@FindBy(how = How.XPATH, using = "(//span[@class='amshopby-choice'])[1]")
	public WebElement ShipHereBtn;

	@FindBy(how = How.XPATH, using = "(//a[@class='product-item-link'])[1]")
	public WebElement SelectPart;

	@FindBy(how = How.XPATH, using = "(//div[@class='product actions']//child::div[@class='primary'])[1]")
	public WebElement SettingBtnOnBasketPage;

	@FindBy(how = How.XPATH, using = "//button[@title='Update Cart']")
	public WebElement UpdateBasketBtn;

	@FindBy(how = How.XPATH, using = "//div[@class='message-success success message']")
	public WebElement UpdateBaskerSuccessfullMessage;

	@FindBy(how = How.XPATH, using = "//div[@class='message notice']")
	public WebElement NoSearchResultMessage;

	@FindBy(how = How.XPATH, using = "(//div[@class='payment-method']//child::input[@name='payment[method]'])[2]")
	public WebElement SelectTestPayment;

	@FindBy(how = How.XPATH, using = "//li[@data-label='Land Rover']")
	public WebElement LandroverPart;

	@FindBy(how = How.XPATH, using = "//li[@data-label='Jaguar']")
	public WebElement JaguarPart;

	@FindBy(how = How.XPATH, using = "//a[@href='/parts/index/brand/key/land-rover/']")
	public WebElement LandroverPartsTab;

	@FindBy(how = How.XPATH, using = "(//a[@class='text_link__has-arrow'])[2]")
	public WebElement SelectLandrverProd;

	@FindBy(how = How.XPATH, using = "(//ol//child::li[@class='item '])[1]")
	public WebElement SelectPartCatagory;

	@FindBy(how = How.XPATH, using = "(//input[@class='required-entry'])[3]")
	public WebElement AgreeTandC;

	@FindBy(how = How.XPATH, using = "(//div[@class='header__links--extra']//div//ul//li//a[text()='CONTACT US'])[1]")
	public WebElement contantUsHeader;

	@FindBy(how = How.XPATH, using = "//section[@class='offer']//span[@class='pagebuilder-button-primary']")
	public WebElement JaguarShopNowButton;

	@FindBy(how = How.XPATH, using = "//div[@class='mage-error']")
	public WebElement InvalidDetailsErrorMessage;

	@FindBy(how = How.XPATH, using = "//div[@id='vin-error']")
	public WebElement InvalidVinErrorMessage;

	public String SelectHowCanHelp() {
		return "//select[@id='how_help']";
	}

}
