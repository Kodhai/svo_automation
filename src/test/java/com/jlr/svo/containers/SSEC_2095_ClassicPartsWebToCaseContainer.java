package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SSEC_2095_ClassicPartsWebToCaseContainer {

	@FindBy(how = How.XPATH, using = "(//a[@data-refid='recordId'])[1]")
	public WebElement CaseRec;
	
	@FindBy(how = How.XPATH, using = "//span[text()='First Name']")
	public WebElement FirstNameLabel;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Last Name']")
	public WebElement LastNameLabel;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Case Record Type']")
	public WebElement CaseRecordType;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Web Email']")
	public WebElement WebEmailLabel;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Web Phone']")
	public WebElement WebPhoneLabel;
	
	@FindBy(how = How.XPATH, using = "//button[@title='Edit First Name']")
	public WebElement EditFirstName;
	
	@FindBy(how = How.XPATH, using = "//button[@name='SaveEdit']")
	public WebElement SaveEditBtn;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Case Owner']")
	public WebElement CaseOwnerText;
	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
}
