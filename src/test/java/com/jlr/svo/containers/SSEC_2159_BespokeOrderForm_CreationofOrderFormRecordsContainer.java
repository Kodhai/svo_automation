package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SSEC_2159_BespokeOrderForm_CreationofOrderFormRecordsContainer {

	@FindBy(how = How.XPATH, using = "(//span[starts-with(text(), 'Quotes')])[1]")
	public WebElement QuotesQuickLink;
	
	@FindBy(how = How.XPATH, using = "//div[@title='New Quote']")
	public WebElement NewQuoteBtn;
	
	@FindBy(how = How.XPATH, using = "(//span[text()='Quote Name'])[2]")
	public WebElement QuoteName;
	
	@FindBy(how = How.XPATH, using = "(//button[@title='Save'])[2]")
	public WebElement SaveQuoteBtn;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Retailer Contact']//parent::label")
	public WebElement RetailerContactLabel;
	
	public String RetailerContact(String value) {
		return "//div[@title='" + value + "']";
	}
	
	public String PrefContact(String value) {
		return "//span[@title='" + value + "']";
	}
	
	@FindBy(how = How.XPATH, using = "//a[starts-with(@title, 'SVO-20')]")
	public WebElement QuoteCreatedLink;
	
	@FindBy(how = How.XPATH, using = "(//span[starts-with(text(), 'SVO-2022')])[2]")
	public WebElement VerifyQuoteName;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Design Briefs']//parent::a//parent::div")
	public WebElement DesignBriefsQuickLink;
	
	@FindBy(how = How.XPATH, using = "(//a[starts-with(@title, 'SVO-202')])[3]")
	public WebElement DesignBriefsText;
	
	@FindBy(how = How.XPATH, using = "(//a[starts-with(@title, 'SVO-2022-L0070-B-1')])[3]")
	public WebElement DesignBriefRecord;
	
	@FindBy(how = How.XPATH, using = "//button[text()='Submit']")
	public WebElement SubmitBtn;
	
	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-button_brand cuf-publisherShareButton undefined uiButton']")
	public WebElement SaveDesignBriefBtn;
	
	@FindBy(how = How.XPATH, using = "//button[text()='Return Render Pack']")
	public WebElement ReturnRenderPackBtn;
	
	@FindBy(how = How.XPATH, using = "//button[@name='Save']")
	public WebElement SaveReturnRenderPackBtn;
	
	@FindBy(how = How.XPATH, using = "(//span[starts-with(text(), 'Orders')]//parent::slot//parent::a)[2]")
	public WebElement OrdersTab;
	
	@FindBy(how = How.XPATH, using = "(//a[@title='New'])[2]")
	public WebElement NewOrderBtn;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Common Order Number']")
	public WebElement CommonOrderFormText;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Opportunity']")
	public WebElement OpportunityText;
	
	@FindBy(how = How.XPATH, using = "(//button[@title='Save'])[2]")
	public WebElement SaveOrderBtn;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Order Start Date']//parent::label")
	public WebElement OrderStartDateText;
	
	@FindBy(how = How.XPATH, using = "//button[text()='Today']")
	public WebElement TodayBtnOrders;
	
	@FindBy(how = How.XPATH, using = "(//a[starts-with(@title, '0000')])[2]")
	public WebElement OrderRecord;
	
	@FindBy(how = How.XPATH, using = "(//span[text()='Status']//parent::span//following-sibling::div//child::div//child::div//child::div//child::a[text()='Created'])[2]")
	public WebElement StatusOrderCreatedText;
	
	public String StatusOrder(String value) {
		return "//a[@title='" + value + "']";
	}
	
	@FindBy(how = How.XPATH, using = "//a[starts-with(text(), 'a')]")
	public WebElement OrderFormRec;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Monotone / Duotone']")
	public WebElement MonotoneDuotoneText;
	
	@FindBy(how = How.XPATH, using = "(//span[text()='Hand of Drive'])[2]")
	public WebElement HandOfDrive;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Owner']")
	public WebElement OwnerOrders;
	
	@FindBy(how = How.XPATH, using = "(//a[@data-refid='recordId'])[1]")
	public WebElement OpportunityRec;
	
	@FindBy(how = How.XPATH, using = "//span[starts-with(text(), 'Order forms')]//parent::a")
	public WebElement OrderFormsLink;
	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
}
