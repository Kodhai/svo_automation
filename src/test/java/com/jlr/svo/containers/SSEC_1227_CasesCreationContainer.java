package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SSEC_1227_CasesCreationContainer {

	
	@FindBy(how = How.ID, using = "username")
	public WebElement UserNameTextBox;

	@FindBy(how = How.XPATH, using = "//button[@class='action allow primary']")
	public WebElement AllowCookies;

	@FindBy(how = How.XPATH, using = "//span[text()='Email']//parent::label//following-sibling::div//input")
	public WebElement InputEmailAddress;

	@FindBy(how = How.XPATH, using = "//span[text()='Phone Number']//parent::label//following-sibling::div//input")
	public WebElement PhoneNumberTxtBx;

	@FindBy(how = How.XPATH, using = "//select[@name='recordType']")
	public WebElement HelpSelect;

	@FindBy(how = How.XPATH, using = "//button[@title='Submit']")
	public WebElement SubmitBtn;

	@FindBy(how = How.XPATH, using = "//div[@class='message-success success message']")
	public WebElement SubmitSuccessfullMessage;

	@FindBy(how = How.XPATH, using = "//span[text()='First Name']")
	public WebElement validateFN;

	@FindBy(how = How.XPATH, using = "//span[text()='Country']//parent::label//following-sibling::div//select")
	public WebElement CountrySelection;

	@FindBy(how = How.XPATH, using = "//input[@name='vin']")
	public WebElement VINNumber;

	@FindBy(how = How.XPATH, using = "//textarea[@name='description']")
	public WebElement CommentText;

	@FindBy(how = How.XPATH, using = "(//div[@class='header__links--extra']//div//ul//li//a[text()='CONTACT US'])[1]")
	public WebElement contantUsHeader;

	@FindBy(how = How.XPATH, using = "//span[text()='Parts & Technical']//parent::div")
	public WebElement PartandTechRecordType;
	
	@FindBy(how = How.XPATH, using = "//span[text()='First Name']//parent::label//following-sibling::div//input")
	public WebElement FirstNameTxtBx;

	@FindBy(how = How.XPATH, using = "//span[text()='Last Name']//parent::label//following-sibling::div//input")
	public WebElement LastNameTxtBx;

	@FindBy(how = How.XPATH, using = "//span[text()='Street Address']//parent::legend//following-sibling::div//input")
	public WebElement AddressTxtBx;

	@FindBy(how = How.XPATH, using = "//span[text()='City']//parent::label//following-sibling::div//input")
	public WebElement CityTxtBx;

	@FindBy(how = How.XPATH, using = "//span[text()=' Postcode']//parent::label//following-sibling::div//input")
	public WebElement PostCodeTxtBx;

	@FindBy(how = How.ID, using = "password")
	public WebElement PasswordTextBox;

	@FindBy(how = How.ID, using = "Login")
	public WebElement LoginBtn;

	@FindBy(how = How.ID, using = "code")
	public WebElement VerificationCode;
	
	@FindBy(how = How.XPATH, using = "//div[@title='New']")
	public WebElement Newbtn;

	@FindBy(how = How.NAME, using = "save")
	public WebElement VerifyBtn;

	@FindBy(how = How.XPATH, using = "//a[@title='Cases']")
	public WebElement CasesTab;

	@FindBy(how = How.XPATH, using = "//a[@title='New']")
	public WebElement NewCaseBtn;

	

	@FindBy(how = How.XPATH, using = "//span[text()='Next']//parent::button")
	public WebElement NextBtn;

	@FindBy(how = How.XPATH, using = "//span[text()='First Name']")
	public WebElement FirstNameBtn;

	@FindBy(how = How.XPATH, using = "//button[text()='Edit' and @type='button']")
	public WebElement EditBtn;

	@FindBy(how = How.XPATH, using = "//span[text()='Last Name']//parent::label//following-sibling::input")
	public WebElement LastNameText;

	@FindBy(how = How.XPATH, using = "//span[text()='VIN / Chassis Number']")
	public WebElement VINText;

	@FindBy(how = How.XPATH, using = "//li[text()='These required fields must be completed: VIN / Chassis Number']")
	public WebElement VINError;

	@FindBy(how = How.XPATH, using = "//button[@title='Save' and @type='button']")
	public WebElement SaveBtn;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Save & New']")
	public WebElement SaveNewBtn;

	@FindBy(how = How.XPATH, using = "(//p[contains(@class, 'slds-truncate')])[4]")
	public WebElement ValidateVINNum;

	@FindBy(how = How.XPATH, using = "//input[@class='slds-input']//parent::div")
	public WebElement SearchBox;

	@FindBy(how = How.XPATH, using = "//button[@title='Cancel']")
	public WebElement CancelBtn;

	@FindBy(how = How.XPATH, using = "//span[@class='uiImage']")
	public WebElement ProfileBtn;

	@FindBy(how = How.XPATH, using = "//a[@href='/secur/logout.jsp']")
	public WebElement LogoutBtn;

}
