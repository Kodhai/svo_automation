package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SVOOpportunityContainer {

	@FindBy(how = How.ID, using = "username")
	public WebElement userNameTextBox;

	@FindBy(how = How.ID, using = "password")
	public WebElement passwordTextBox;

	@FindBy(how = How.ID, using = "Login")
	public WebElement loginBtn;

	@FindBy(how = How.ID, using = "emc")
	public WebElement verificationCode;

	@FindBy(how = How.ID, using = "save")
	public WebElement verifyBtn;

	@FindBy(how = How.XPATH, using = "(//span[@class='slds-truncate'])[1]")
	public WebElement SVOText;

	@FindBy(how = How.XPATH, using = "(//button[@class='slds-button slds-button_icon-border-filled'])[2]")
	public WebElement showmore;

	@FindBy(how = How.XPATH, using = "//span[text()='Recently Viewed']")
	public WebElement RecentlyViewedOpp;

	@FindBy(how = How.XPATH, using = "//span[text()='All Opportunities']")
	public WebElement allOpportunity;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Vehicle Search')]")
	public WebElement VehicleSearch;

	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Vehicle Search')]")
	public WebElement VehicleSearchbutton;

	@FindBy(how = How.XPATH, using = "//input[@name='Brands']")
	public WebElement VehicleBrandSearch;

	@FindBy(how = How.XPATH, using = "(//button[@class='slds-button slds-button_brand'])[1]")
	public WebElement Search;

	@FindBy(how = How.XPATH, using = "(//button[@class='slds-button slds-button_brand'])[2]")
	public WebElement NewBrand;

	@FindBy(how = How.XPATH, using = "(//input[@class=' input'])[1]")
	public WebElement RecordName;

	@FindBy(how = How.XPATH, using = "(//input[@class=' input'])[3]")
	public WebElement VIN;

	@FindBy(how = How.XPATH, using = "(//span[@class='test-id__field-value slds-form-element__static slds-grow '])[1]")
	public WebElement VerRecord;

	@FindBy(how = How.XPATH, using = "(//span[@class='test-id__field-value slds-form-element__static slds-grow '])[3]")
	public WebElement VerBrand;

	@FindBy(how = How.XPATH, using = "(//span[@class='test-id__field-value slds-form-element__static slds-grow '])[4]")
	public WebElement VerVIN;

	@FindBy(how = How.XPATH, using = "//a[@title='Design Brief & Quote']")
	public WebElement DesignBrief;

	@FindBy(how = How.XPATH, using = "//div[@class='slds-theme--info slds-notify--toast slds-notify slds-notify--toast forceToastMessage']")
	public WebElement errorFlow;

	@FindBy(how = How.XPATH, using = "//button[@title='Close']")
	public WebElement errorclose;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Clone')]")
	public WebElement cloneopp;

	@FindBy(how = How.XPATH, using = "//div[@title='New Contact']")
	public WebElement AddContact;

	public String RTSelection(String value) {
		return "//span[contains(text(),'" + value + "')]";
	}

	@FindBy(how = How.XPATH, using = "(//a[@title='Closed'])[3]")
	public WebElement oppClosed;

	@FindBy(how = How.XPATH, using = "//span[text()='Select Closed Stage']")
	public WebElement oppClosedStage;

	@FindBy(how = How.XPATH, using = "//input[@name='StageName']")
	public WebElement oppStage;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Lost_Reason__c']")
	public WebElement OppLostReason;

	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
	public WebElement DoneButton;

	@FindBy(how = How.XPATH, using = "//div[@class='recordTypeName slds-grow slds-truncate']//child::span")
	public WebElement oppHeading;

	@FindBy(how = How.XPATH, using = "//span[contains(@title,'Show All Results for')]")
	public WebElement showAllResult;

	@FindBy(how = How.XPATH, using = "//span[text()='Source Information']")
	public WebElement souInfo;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[11]")
	public WebElement souceDD;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[18]")
	public WebElement CRsouceDD;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[20]")
	public WebElement CBsouceDD;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[23]")
	public WebElement ClsLimiEdisouceDD;

	@FindBy(how = How.XPATH, using = "//button[@title='Change Record Type']")
	public WebElement changeRecType;

	@FindBy(how = How.XPATH, using = "//span[text()='Classic Reborn']")
	public WebElement ClassicReborn;

	@FindBy(how = How.XPATH, using = "//div[@class='genericNotification']")
	public WebElement error;

	@FindBy(how = How.XPATH, using = "(//a[@title='Contracting'])")
	public WebElement oppContracting;

	@FindBy(how = How.XPATH, using = "//span[@title='JLR Retail Case Management']")
	public WebElement jlrText;

	public String selectMenu(String text) {
		return "//b[text()='" + text + "']";
	}

	@FindBy(how = How.XPATH, using = "//span[text()='Opportunities']")
	public WebElement Opportunity;

	@FindBy(how = How.XPATH, using = "(//button[@title='Save'])")
	public WebElement SaveRelation;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-button_icon-border-filled']")
	public WebElement ShowMore;

	public String addedRelationship(String text) {
		return "//a[@title='" + text + "']";
	}

	@FindBy(how = How.XPATH, using = "//button[@name='SaveEdit']")
	public WebElement SaveVehicle;

	@FindBy(how = How.XPATH, using = "//div[@class='pageLevelErrors']")
	public WebElement saveError;

	@FindBy(how = How.XPATH, using = "//a[@title='Returned']")
	public WebElement DesignBriefStatus;

	@FindBy(how = How.XPATH, using = "//span[text()='Next']")
	public WebElement Next;

	@FindBy(how = How.XPATH, using = "//button[@name='CancelEdit']")
	public WebElement cancelVehicle;

	@FindBy(how = How.XPATH, using = "//button[@title='Close error dialog']")
	public WebElement closeSaveErrorBox;

	@FindBy(how = How.XPATH, using = "//input[@class=' input']")
	public WebElement name;

	@FindBy(how = How.XPATH, using = "(//a[@class='select'])[2]")
	public WebElement prodoffer;

	@FindBy(how = How.XPATH, using = "(//input[@placeholder='Search Vehicle Manufacturers...'])[2]")
	public WebElement probrand;

	@FindBy(how = How.XPATH, using = "(//input[@placeholder='Search Vehicle Models...'])[2]")
	public WebElement promodel;

	public String dropdown(String text) {
		return "//a[text()='" + text + "']";
	}

	@FindBy(how = How.XPATH, using = "//input[@class='input uiInputSmartNumber']")
	public WebElement BuildCap;

	@FindBy(how = How.XPATH, using = "//a[@title='Send Sales Agreement']")
	public WebElement sendSalesAgreement;

	@FindBy(how = How.XPATH, using = "//input[@value='E-Sign Documents In Person']")
	public WebElement ESign;

	@FindBy(how = How.XPATH, using = "//span[text()='Draft']")
	public WebElement draftText;

	@FindBy(how = How.XPATH, using = "//button[text()='Send']")
	public WebElement sendESignEmail;

	@FindBy(how = How.XPATH, using = "(//span[text()='All Bespoke Opportunities'])[1]")
	public WebElement BespokeOpp;

	@FindBy(how = How.XPATH, using = "//button[text()='Send POF']")
	public WebElement SendPOF;

	@FindBy(how = How.XPATH, using = "//button[text()='Send Sales Agreement']")
	public WebElement SendAgreement;

	@FindBy(how = How.XPATH, using = "//a[@id='apxt-c8-start-merge']")
	public WebElement MergeAdobe;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-button_brand slds-button slds-button_brand slds-float_left']")
	public WebElement Send;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[2]")
	public WebElement StageDD;

	@FindBy(how = How.XPATH, using = "(//span[text()='Upload Files'])[2]")
	public WebElement UploadFiles;

	@FindBy(how = How.XPATH, using = "(//span[text()='Save'])[2]")
	public WebElement SaveShare;

	@FindBy(how = How.XPATH, using = "//button[@title='Apply']")
	public WebElement Apply;

	@FindBy(how = How.XPATH, using = "//span[@title='Files']")
	public WebElement Filesbutton;

	@FindBy(how = How.XPATH, using = "(//span[text()='Show Actions'])")
	public WebElement ShowActions;

	@FindBy(how = How.XPATH, using = "//div[@title='Delete']")
	public WebElement DeleteVehicle;

	@FindBy(how = How.XPATH, using = "//span[text()='Delete']")
	public WebElement ConfDeleteVehicle;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Done')]")
	public WebElement Done;

	@FindBy(how = How.ID, using = "p")
	public WebElement mobileNumText;

	@FindBy(how = How.LINK_TEXT, using = "Remind Me Later")
	public WebElement remindMeLater;

	@FindBy(how = How.XPATH, using = "(//span[text()='Cancel'])[2]")
	public WebElement CancelDesignBrief;

	@FindBy(how = How.XPATH, using = "//span[text()='Confirm Message']")
	public WebElement ConfirmMessage;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Outcome__c']")
	public WebElement OutcomeDropDown;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Outcome_Detail__c']")
	public WebElement OutcomeDetailDropDown;

	@FindBy(how = How.XPATH, using = "(//input[@type='checkbox'])[1]")
	public WebElement FirstDocCheckbox;

	@FindBy(how = How.XPATH, using = "(//input[@type='checkbox'])[2]")
	public WebElement SecondDocCheckbox;

	@FindBy(how = How.XPATH, using = "(//input[@value='Next Step'])[1]")
	public WebElement NextStepBtn;

	@FindBy(how = How.XPATH, using = "//input[@value='E-Sign Documents In Person']")
	public WebElement EsignDocBtn;

	@FindBy(how = How.XPATH, using = "//button[@value='Download']")
	public WebElement DownloadBtn;

	@FindBy(how = How.XPATH, using = "//input[@name='title']")
	public WebElement ListNameTxtBox;

	@FindBy(how = How.XPATH, using = "//button[@title='List View Controls']")
	public WebElement ListViewControlBtn;

	@FindBy(how = How.XPATH, using = "//input[@name='developerName']")
	public WebElement ListAPINameTxtBox;

	@FindBy(how = How.XPATH, using = "(//span[text()='Save'])[2]")
	public WebElement SaveListViewBtn;

	@FindBy(how = How.XPATH, using = "//a[@class=' addFilter']")
	public WebElement AddFilterLink;

	@FindBy(how = How.XPATH, using = "(//input[@placeholder='Select an Option'])[1]")
	public WebElement FieldDropDown;

	@FindBy(how = How.XPATH, using = "(//input[@placeholder='Select an Option'])[2]")
	public WebElement OperatorDropDown;

	@FindBy(how = How.XPATH, using = "(//input[@type='text'])[5]")
	public WebElement ValueTxtBx;

	@FindBy(how = How.XPATH, using = "//button[text()='Save']")
	public WebElement SaveFilterBtn;

	@FindBy(how = How.XPATH, using = "(//span[text()='Opportunities'])[2]")
	public WebElement FilterTitle;

	@FindBy(how = How.XPATH, using = "//span[text()='Done']")
	public WebElement DoneFilterBtn;

	@FindBy(how = How.XPATH, using = "(//input[@aria-autocomplete='none'])[2]")
	public WebElement ProductOffering;

	@FindBy(how = How.XPATH, using = "(//input[@placeholder='Search Accounts...'])[1]")
	public WebElement ClientName;

	@FindBy(how = How.XPATH, using = "(//input[@value='Select Template'])[1]")
	public WebElement SelectTemplateBtn;

	@FindBy(how = How.XPATH, using = "(//a[contains(text(),'15Shift')])[1]")
	public WebElement Template1;

	@FindBy(how = How.XPATH, using = "//input[@value='Ok']")
	public WebElement OkTempBtn;

	@FindBy(how = How.XPATH, using = "//input[@value='Send Documents for Electronic Signature']")
	public WebElement ElectronicSignatureBtn;

	@FindBy(how = How.XPATH, using = "(//input[@value='Send'])[1]")
	public WebElement SendDocBtn;

	@FindBy(how = How.XPATH, using = "(//button[@title='Save'])[2]")
	public WebElement SaveQuoteMike;

	@FindBy(how = How.XPATH, using = "//h2[text()='Submit']")
	public WebElement PopUptitle;

	@FindBy(how = How.XPATH, using = "(//a[@class='select'])[1]")
	public WebElement OrderStageDropDown;

	@FindBy(how = How.XPATH, using = "//div[@title='Edit']")
	public WebElement EditOrderBtn;

	@FindBy(how = How.XPATH, using = "//span[text()='Save']")
	public WebElement SaveOrderBtn;

	@FindBy(how = How.XPATH, using = "(//button[@title='Clear Selection'])[7]")
	public WebElement ClearDonorBtn;

	@FindBy(how = How.XPATH, using = "//a[@id='activityTab__item']")
	public WebElement ActivityTab;

	@FindBy(how = How.XPATH, using = "(//span[@class='title'])[2]")
	public WebElement NewTaskTab;

	@FindBy(how = How.XPATH, using = "(//input[@type='text'])[3]")
	public WebElement SubjectTextBox;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-button--brand cuf-publisherShareButton MEDIUM uiButton']")
	public WebElement SaveTask;

	@FindBy(how = How.XPATH, using = "(//span[contains(@class,'inline-edit-trigger-icon slds-button__icon slds-button__icon_hint')])[43]")
	public WebElement WO_BuildStart_edit;

	@FindBy(how = How.XPATH, using = "(//*[contains(@href,'/lightning/r/Order/')])")
	public WebElement OrderWindow;

	@FindBy(how = How.XPATH, using = "(//*[contains(@class,'slds-truncate outputLookupLink slds-truncate outputLookupLink')])[31]")
	public WebElement Order1fromsearch;

	@FindBy(how = How.XPATH, using = "(//button[@title='Edit Pinewood Job Card'])[2]")
	public WebElement editBuildStart;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Start_Original__c']")
	public WebElement StartOriginalDate;

	@FindBy(how = How.XPATH, using = "//*[@title='Save']")
	public WebElement SaveWO;

	@FindBy(how = How.XPATH, using = "(//button[text()='Save'])[3]")
	public WebElement saveReservation;

	@FindBy(how = How.XPATH, using = "(//records-formula-output[@data-output-element-id='output-field'])[21]")
	public WebElement SlippingDays;

	@FindBy(how = How.XPATH, using = "//li[text()='ABS Forecast cannot be earlier than any build stage forecast.']")
	public WebElement ABS_Prior_Build_error;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Stage_1_Forecast__c']")
	public WebElement StripForecast;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Stage_3_Forecast__c']")
	public WebElement PaintForecast;

	@FindBy(how = How.XPATH, using = "//li[text()='Build stages must have sequential forecast dates. Please check your forecast dates to ensure each stage does not have a forecast date before any earlier stage forecast.']")
	public WebElement Non_SeqnceDate_error;

	@FindBy(how = How.XPATH, using = "(//a[contains(@class,'textUnderline outputLookupLink slds-truncate outputLookupLink')][contains(text(),'INV-00000')])[2]")
	public WebElement Invoice_1;

	@FindBy(how = How.LINK_TEXT, using = "I Don't Want to Register My Phone")
	public WebElement notRegister;

	@FindBy(how = How.XPATH, using = "//div[@class='slds-icon-waffle']")
	public WebElement Menu;

	@FindBy(how = How.XPATH, using = "//a[@title='Home']")
	public WebElement Home;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search apps and items...']")
	public WebElement SearchBar;

	@FindBy(how = How.XPATH, using = "//div[@class='slds-size_small']")
	public WebElement SVO;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-show']")
	public WebElement menuBtn;

	@FindBy(how = How.XPATH, using = "//input[@class='slds-input']")
	public WebElement searchBox;

	@FindBy(how = How.XPATH, using = "//a[@title='Opportunities']")
	public WebElement OpportTab;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Programmes...']")
	public WebElement Programmes;

	@FindBy(how = How.XPATH, using = "//lightning-base-combobox-item[@data-value='actionCreateNew']")
	public WebElement newProgrammes;

	@FindBy(how = How.XPATH, using = "(//button[contains(text(),'View all dependencies')])[4]")
	public WebElement ViewallDependencies;

	@FindBy(how = How.XPATH, using = "(//a[contains(@class,' textUnderline outputLookupLink slds-truncate outputLookupLink')])[12]")
	public WebElement WorkOrder1;

	@FindBy(how = How.XPATH, using = "//span[text()='Handover Details']")
	public WebElement handoverDetails;

	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Apply')]")
	public WebElement Applybtn;

	@FindBy(how = How.XPATH, using = "(//input[@title='Search Contacts'])[1]")
	public WebElement preferredCntBS;

	@FindBy(how = How.XPATH, using = "(//input[@title='Search Accounts'])[2]")
	public WebElement OpportunityAccountNameBe;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'in Accounts')]")
	public WebElement OppAccountSearch;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Programmes...']")
	public WebElement ProgrammeSearch;

	@FindBy(how = How.XPATH, using = "(//div[@role='none'])[2]")
	public WebElement OppoStage;

	@FindBy(how = How.XPATH, using = "//span[@title='Check cleared']")
	public WebElement Checkcleared;

	@FindBy(how = How.XPATH, using = "(//button[contains(text(),'S-Docs')])[2]")
	public WebElement SDocs;

	@FindBy(how = How.XPATH, using = "//span[text()='Recently Viewed']")
	public WebElement selectlistview;

	@FindBy(how = How.XPATH, using = "(//span[text()='Recently Viewed'])[1]")
	public WebElement recentlyViewed;

	@FindBy(how = How.XPATH, using = "//span[text()='Pricing Details']")
	public WebElement pricingDetails;

	@FindBy(how = How.XPATH, using = "//a[@href='/lightning/r/Opportunity/0060N00000lwajYQAQ/related/Quotes/view']")
	public WebElement SelectQuote;

	@FindBy(how = How.XPATH, using = "//i[contains(@class,'ms-ohp-Icon--outlookLogo ms-ohp-Icon--outlookLogoFill')]")
	public WebElement outlookIcon;

	@FindBy(how = How.XPATH, using = "(//div[contains(@aria-label,'noreply@salesforce.com')])[1]")
	public WebElement outlookEmail;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Sign in')]")
	public WebElement outlookSignin;

	@FindBy(how = How.NAME, using = "loginfmt")
	public WebElement outlookUsername;

	@FindBy(how = How.XPATH, using = "//input[@class='win-button button_primary button ext-button primary ext-primary']")
	public WebElement outlookNext;
	// @FindBy(how = How.XPATH, using = "//input[@class='button ext-button primary
	// ext-primary']")
	// public WebElement outlookNext;

	// @FindBy(how = How.XPATH, using = "//input[@type='submit']")

	@FindBy(how = How.XPATH, using = "//input[@name='passwd']")
	public WebElement outlookPassword;

	@FindBy(how = How.XPATH, using = "//input[contains(@value,'Sign in')]")
	public WebElement outlookSignInBtn;

	@FindBy(how = How.XPATH, using = "//a[text()='OK']")
	public WebElement OKButton;

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Signature requested on')])[1]")
	public WebElement signFirstEmail;

	@FindBy(how = How.XPATH, using = "//a[contains(@href,'https://jaguarlandrover.na1.adobesign.com/public/esign') and text()='Review and sign ']")
	public WebElement ESignLink;

	@FindBy(how = How.XPATH, using = "//div[text()='Start']")
	public WebElement startSigning;

	@FindBy(how = How.XPATH, using = "//div[@class='faux_field']")
	public WebElement clickToSign;

	@FindBy(how = How.XPATH, using = "//input[@class='form-control signature-type-name adobehandb']")
	public WebElement signHereTextBox;

	@FindBy(how = How.XPATH, using = "(//button[text()='Apply'])[2]")
	public WebElement signApplyBtn;

	@FindBy(how = How.XPATH, using = "//button[text()='Click to Sign']")
	public WebElement clickToSignBtn;

	@FindBy(how = How.XPATH, using = "//div[@class='postsign-descr-message-container']")
	public WebElement alldoneMessage;

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'You signed:')])[1]")
	public WebElement signedEmail;

	@FindBy(how = How.XPATH, using = "//div[text()='Send Sales Agreement']")
	public WebElement SendSalesAgreement;

	@FindBy(how = How.XPATH, using = "(//span[text()='All Bespoke Opportunities'])[1]")
	public WebElement AllBespokeOpportunities;

	@FindBy(how = How.XPATH, using = "(//a[@data-refid='recordId'])[46]")
	public WebElement OpportunitySel11;

	@FindBy(how = How.XPATH, using = "(//button[@title='Edit Retail Price'])[2]")
	public WebElement editPricingDetails;

	@FindBy(how = How.XPATH, using = "//span[text()='All Classic Service Opportunities']")
	public WebElement ClassicService;

	@FindBy(how = How.XPATH, using = "//input[@name='Opportunity-search-input']")
	public WebElement SearchOpportunity;

	@FindBy(how = How.XPATH, using = "(//a[@data-refid='recordId'])[1]")
	public WebElement OpportunitySel1;

	@FindBy(how = How.XPATH, using = "//a[contains(@title,'SVO-2021-')]")
	public WebElement firstQuote;

	@FindBy(how = How.XPATH, using = "//a[contains(@href,'related/Agreement__r/view')]")
	public WebElement agreementsSection;

	@FindBy(how = How.XPATH, using = "//span[text()='Signed']")
	public WebElement contractStatus;

	@FindBy(how = How.XPATH, using = "(//a[contains(@title,'Proposal')])[2]")
	public WebElement agreementName;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'signed.pdf')]")
	public WebElement signedPDF;

	@FindBy(how = How.XPATH, using = "(//button[contains(@title,'Edit Cost Price')])[2]")
	public WebElement EditCostPrice;

	@FindBy(how = How.XPATH, using = "(//input[@class='input uiInputSmartNumber'])[1]")
	public WebElement CostPriceTextBox;

	@FindBy(how = How.XPATH, using = "(//input[@class='input uiInputSmartNumber'])[2]")
	public WebElement ListPriceTextBox;

	@FindBy(how = How.XPATH, using = "(//input[@class='input uiInputSmartNumber'])[4]")
	public WebElement RetailPriceTextBox;

	@FindBy(how = How.XPATH, using = "(//input[@class='input uiInputSmartNumber'])[3]")
	public WebElement ContributionTextBox;

	@FindBy(how = How.XPATH, using = "(//input[@class='input uiInputSmartNumber'])[5]")
	public WebElement RevenueTextBox;

	@FindBy(how = How.XPATH, using = "//button[@name='SaveEdit']")
	public WebElement SaveButton;

	@FindBy(how = How.XPATH, using = "(//button[@title='Save'])")
	public WebElement DBSaveButton;

	@FindBy(how = How.XPATH, using = "//button[@title='Next']")
	public WebElement NextButton;

	@FindBy(how = How.XPATH, using = "//a[contains(@title,'REQ')]")
	public WebElement firstCommRequest;

	@FindBy(how = How.XPATH, using = "(//a[contains(@title,'00')])")
	public WebElement firstOrder;

	@FindBy(how = How.XPATH, using = "//span[text()='Work Order Details']")
	public WebElement workOrderSection;

	@FindBy(how = How.XPATH, using = "//span[text()='Bespoke Opportunity Details']")
	public WebElement commReqOppList;

	@FindBy(how = How.XPATH, using = "//button[text()='View Calendar']")
	public WebElement viewCalendarBtn;

	@FindBy(how = How.XPATH, using = "(//input[@placeholder='Search Contacts...'])[1]")
	public WebElement commReqPrimaryCnt;

	@FindBy(how = How.XPATH, using = "//div[@class='calendar_default_shadow_inner']")
	public WebElement CalendarCell;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Commissioning Requests...']")
	public WebElement searchCommReq;

	@FindBy(how = How.XPATH, using = "(//span[@class='test-id__section-header-title slds-truncate'])[14]")
	public WebElement VehicleDetails;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Production Year']")
	public WebElement EditProductionYear;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Production_Year__c']")
	public WebElement ProductionYearTextBox;

	@FindBy(how = How.XPATH, using = "(//span[@class='title slds-path__title'])[5]")
	public WebElement OrderPlaced;

	@FindBy(how = How.XPATH, using = "//span[text()='Mark as Current Stage']")
	public WebElement MarkAsCurrentStage;

	@FindBy(how = How.XPATH, using = "//div[@class='toastTitle slds-text-heading--small']")
	public WebElement PriceError;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Restricted Party Screening']")
	public WebElement EditRestrictedPS;

	@FindBy(how = How.XPATH, using = "(//input[@aria-autocomplete='none'])[8]")
	public WebElement RestrictedDropDown;

	@FindBy(how = How.XPATH, using = "//button[@title='Save']")
	public WebElement SaveRPS;

	@FindBy(how = How.XPATH, using = "(//span[text()='Mark Stage as Complete'])")
	public WebElement MarkStageAsComplete;

	@FindBy(how = How.XPATH, using = "//div[text()='You encountered some errors when trying to save this record']")
	public WebElement ErrorRPS;

	@FindBy(how = How.XPATH, using = "//a[@title='Select List View']")
	public WebElement SelectListView;

	@FindBy(how = How.XPATH, using = "(//span[text()='Pricing Details'])[5]")
	public WebElement pricingDetailsSB;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Cost_Price__c']")
	public WebElement costPriceSB;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Contribution__c']")
	public WebElement contributionSB;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Revenue__c']")
	public WebElement revenueSB;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_List_Price__c']")
	public WebElement listPriceSB;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Retail_Price__c']")
	public WebElement retailPriceSB;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[11]")
	public WebElement CRRestrictedParty;

	@FindBy(how = How.XPATH, using = "//lightning-formatted-text[text()='Contract Signed']")
	public WebElement contractSigned;

	@FindBy(how = How.XPATH, using = "//a[@title='Contract Signed']")
	public WebElement ContractSigned;

	@FindBy(how = How.XPATH, using = "(//input[@type='text'])[3]")
	public WebElement ListViewTextBox;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit SAP Account']")
	public WebElement EditSAPaccount;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search SAP Accounts...']")
	public WebElement SAPtextbox;

	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
	public WebElement Donebutton;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Opportunity Currency']")
	public WebElement EditOpportunityCurrency;

	@FindBy(how = How.XPATH, using = "(//input[@aria-autocomplete='none'])[12]")
	public WebElement OpportunityCurrency;

	@FindBy(how = How.XPATH, using = "//span[@title='--None--']")
	public WebElement NoneOption;

	@FindBy(how = How.XPATH, using = "//h2[text()='We hit a snag.']")
	public WebElement ErrorOpportunityCurrency;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit VAT Qualifying']")
	public WebElement EditVAT;

	@FindBy(how = How.XPATH, using = "(//input[@aria-autocomplete='none'])[13]")
	public WebElement VATdropdown;

	@FindBy(how = How.XPATH, using = "//span[text()='Bespoke Specification']")
	public WebElement PricingDetails;

	@FindBy(how = How.XPATH, using = "//button[@name='Opportunity.SO_Compose_Email']")
	public WebElement ComposeEmail;

	@FindBy(how = How.XPATH, using = "(//input[@value='Attach File'])[1]")
	public WebElement AttachFile;

	@FindBy(how = How.XPATH, using = "(//select[starts-with(@ortoo-elem-id,'EAAD')])[1]")
	public WebElement DocumentListbox;

	@FindBy(how = How.XPATH, using = "//input[@value='Attach']")
	public WebElement AttachBtn;

	@FindBy(how = How.XPATH, using = "//input[@value='Click to Complete!']")
	public WebElement ClickToCompleteBtn;

	@FindBy(how = How.XPATH, using = "//input[@value='Save']")
	public WebElement FileSaveBtn;

	@FindBy(how = How.XPATH, using = "//div[@title='New']")
	public WebElement newOpportunityBtn;

	@FindBy(how = How.XPATH, using = "(//span[@class='slds-form-element__label'])[4]")
	public WebElement ClassicLimitedEdition;

	@FindBy(how = How.XPATH, using = "(//span[@class='slds-form-element__label'])[3]")
	public WebElement ClassicContinuation;

	@FindBy(how = How.XPATH, using = "//span[text()='Next']")
	public WebElement nextRType;

	@FindBy(how = How.XPATH, using = "//input[@name='Name']")
	public WebElement OppName;

	@FindBy(how = How.XPATH, using = "//input[@name='CloseDate']")
	public WebElement CloseDate;

	@FindBy(how = How.XPATH, using = "//input[@name='Opportunity-search-input']")
	public WebElement OpportSearchBox;

	@FindBy(how = How.XPATH, using = "(//a[@data-refid='recordId'])[1]")
	public WebElement FirstOpportunity;

	@FindBy(how = How.XPATH, using = "(//span[@class='test-id__section-header-title slds-truncate'])[4]")
	public WebElement VehicleRequestDetails;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Stage']")
	public WebElement editStage;

	@FindBy(how = How.XPATH, using = "(//a[starts-with(@href,'/lightning/r/a2V')])[1]")
	public WebElement Programme;

	@FindBy(how = How.XPATH, using = "//span[text()='Build Capacity']")
	public WebElement BuildCapacity;

	@FindBy(how = How.XPATH, using = "(//span[@class='uiImage'])[1]")
	public WebElement icon_image;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Log Out')]")
	public WebElement Logout;

	public String StageSelection(String Stage) {
		return "//span[contains(text(),'" + Stage + "')]";
	}

	public String RegionSelection(String Region) {
		return "//span[contains(text(),'" + Region + "')]";
	}

	@FindBy(how = How.XPATH, using = "//input[@name='StageName']")
	public WebElement StageDW;

	@FindBy(how = How.XPATH, using = "(//button[text()='View all dependencies'])[1]")
	public WebElement stageViewAllDependancies;

	@FindBy(how = How.XPATH, using = "(//input[@aria-autocomplete='none'])[15]")
	public WebElement SourceDW;

	@FindBy(how = How.XPATH, using = "(//input[@aria-haspopup='listbox'])[4]")
	public WebElement RegionDW;

	@FindBy(how = How.XPATH, using = "(//span[@class='test-id__section-header-title section-header-title slds-p-horizontal--small slds-truncate'])[2]")
	public WebElement Client;

	@FindBy(how = How.XPATH, using = "(//input[@placeholder='Search Accounts...'])[1]")
	public WebElement AccountName;

	@FindBy(how = How.XPATH, using = "(//input[@placeholder='Search Contacts...'])[1]")
	public WebElement PrefContact;

	@FindBy(how = How.XPATH, using = "//button[@name='SaveEdit']")
	public WebElement SaveOpportunity;

	@FindBy(how = How.XPATH, using = "//h2[text()='We hit a snag.']")
	public WebElement ProgrammeError;

	@FindBy(how = How.XPATH, using = "//button[@name='CancelEdit']")
	public WebElement Cancel;

	@FindBy(how = How.XPATH, using = "//span[@class='slds-float_right']")
	public WebElement SearchTO;

	@FindBy(how = How.XPATH, using = "//select[@class='multiselect_custom']")
	public WebElement SelectToList;

	@FindBy(how = How.XPATH, using = "(//input[@value='>>'])[1]")
	public WebElement ToArrowBtn;

	@FindBy(how = How.XPATH, using = "(//input[@value='>>'])[2]")
	public WebElement CCArrowBtn;

	@FindBy(how = How.XPATH, using = "(//input[@value='>>'])[3]")
	public WebElement BCCArrowBtn;

	@FindBy(how = How.XPATH, using = "//input[@value='OK']")
	public WebElement OkBtn;

	@FindBy(how = How.XPATH, using = "//input[@name='Opportunity-search-input']")
	public WebElement OpportunitiesSearchBox;

	@FindBy(how = How.XPATH, using = "(//a[contains(@class,'slds-truncate')])[1]")
	public WebElement stageOpportunity;

	@FindBy(how = How.XPATH, using = "//button[@name='Opportunity.SO_Compose_Email']")
	public WebElement OppComposeEmail;

	@FindBy(how = How.XPATH, using = "(//input[@value='Attach File'])[1]")
	public WebElement enquiryAttachFile;

	@FindBy(how = How.XPATH, using = "//span[@id='theErrorPage:theError']")
	public WebElement attachFileError;

	@FindBy(how = How.XPATH, using = "//textarea[@id='thePage:theform:thePB:pbS:pbSI_to:to_addresses']")
	public WebElement Additionalto;

	@FindBy(how = How.XPATH, using = "//input[@id='thePage:theform:thePB:pbS:j_id230:email_subject']")
	public WebElement Subject;

	@FindBy(how = How.XPATH, using = "//body[@id='tinymce']")
	public WebElement Body;

	@FindBy(how = How.XPATH, using = "(//input[@value='Save'])[2]")
	public WebElement saveemail;

	@FindBy(how = How.XPATH, using = "(//input[@value='Cancel'])[2]")
	public WebElement cancelemail;

	@FindBy(how = How.XPATH, using = "(//input[@value='Drafts'])[1]")
	public WebElement Drafts;

	@FindBy(how = How.XPATH, using = "//td[contains(text(),'Retrieve Draft')]")
	public WebElement RetrieveDrafts;

	public String SelectDrafts(String Draft) {
		return "//a[contains(text(),'" + Draft + "')]";
	}

	@FindBy(how = How.XPATH, using = "//input[@id='thePage:theform:thePB:thePBBb:send']")
	public WebElement sendemail;

	@FindBy(how = How.XPATH, using = "//button[text()='S-Docs']")
	public WebElement OppSDocs;

	@FindBy(how = How.XPATH, using = "(//input[contains(@name,'j_id')])[5]")
	public WebElement selectFile;

	@FindBy(how = How.XPATH, using = "(//input[@type='submit'])[1]")
	public WebElement Nextstep;

	@FindBy(how = How.XPATH, using = "(//input[@value='Generate Doc'])[1]")
	public WebElement GenDoc;

	@FindBy(how = How.XPATH, using = "//span[starts-with(text(),'Orders')]")
	public WebElement OrderLink;

	@FindBy(how = How.XPATH, using = "//a[starts-with(@href,'/lightning/r/SO_Invoice__c/')]")
	public WebElement InvoiceLink;

	@FindBy(how = How.XPATH, using = "//a[starts-with(@href,'/lightning/r/']")
	public WebElement FirstOrder;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-button_icon-border-filled']")
	public WebElement InvoiceDropDown;

	@FindBy(how = How.XPATH, using = "(//div[@title='New'])[2]")
	public WebElement InvoiceNewBtn;

	@FindBy(how = How.XPATH, using = "(//span[@class='slds-radio--faux'])[3]")
	public WebElement DepositInvoiceType;

	@FindBy(how = How.XPATH, using = "(//span[@class='slds-radio--faux'])[2]")
	public WebElement CreditNoteInvoiceType;

	@FindBy(how = How.XPATH, using = "//span[text()='Next']")
	public WebElement NextBtn;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Opportunities...']")
	public WebElement OpportunityTxtBx;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Accounts...']")
	public WebElement AccountTxtBx;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search SAP Accounts...']")
	public WebElement SAPtxtBx;

	@FindBy(how = How.XPATH, using = "//span[@title='New SAP Account']")
	public WebElement NewSAPAcnt;

	@FindBy(how = How.XPATH, using = "//input[@class=' input']")
	public WebElement SAPid;

	@FindBy(how = How.XPATH, using = "(//span[text()='Save'])[4]")
	public WebElement SaveSAPBtn;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Value__c']")
	public WebElement PaymentValue;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Payment_Amount__c']")
	public WebElement PaymentAmount;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_SAP_Invoice_Number__c']")
	public WebElement SapInvoice;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_SAP_Invoice_Value__c")
	public WebElement SapInvoiceValue;

	@FindBy(how = How.XPATH, using = "//a[starts-with(@data-proxy-id,'aura-pos-lib-']")
	public WebElement InvoiceNumber;

	@FindBy(how = How.XPATH, using = "(//button[@class='slds-button slds-button_icon-border-filled'])[2]")
	public WebElement ShowMoreActionsBtn;

	@FindBy(how = How.XPATH, using = "//a[@name='ChangeRecordType']")
	public WebElement ChangeRecordType;

	@FindBy(how = How.XPATH, using = "//option[@value='0']")
	public WebElement FirstCheckbox;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Vehicle']")
	public WebElement EditVehicle;

	@FindBy(how = How.XPATH, using = "(//input[@class=' input'])[2]")
	public WebElement HandOverDate;

	@FindBy(how = How.XPATH, using = "//button[@title='Save']")
	public WebElement SaveBtn;

	@FindBy(how = How.XPATH, using = "(//a[@class='tabHeader slds-path__link'])[6]")
	public WebElement OutcomeTab;

	@FindBy(how = How.XPATH, using = "//span[text()='Mark as Current Status']")
	public WebElement MarkAsCurrentStatus;

	@FindBy(how = How.XPATH, using = "//a[@data-tab-name='Outcome']")
	public WebElement OutcomeList;

	@FindBy(how = How.XPATH, using = "//span[@title='Retailed']")
	public WebElement RetailedOutcome;

	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
	public WebElement DoneBtn;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Value']")
	public WebElement EditPaymentValue;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Send_By_Date__c']")
	public WebElement SendByDate;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Payment_Made_Date__c']")
	public WebElement PaymentMadeDate;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Payment_Request_Sent_Date__c']")
	public WebElement SentDate;

	@FindBy(how = How.XPATH, using = "(//span[@class='test-id__field-value slds-form-element__static slds-grow word-break-ie11 is-read-only'])[16]")
	public WebElement PaymentDueDate;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Close Date']")
	public WebElement EditCloseDate;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Close Date']")
	public WebElement CloseDateTextBox;

	@FindBy(how = How.XPATH, using = "//a[@data-tab-name='Closed']")
	public WebElement ClosedTab;

	@FindBy(how = How.XPATH, using = "//span[text()='Select Closed Stage']")
	public WebElement SelectClosedStage;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Pinewood Job Card']")
	public WebElement EditPineWoodJobCard;

	@FindBy(how = How.XPATH, using = "//input[@class=' input' and @maxlength='15']")
	public WebElement PineWoodJobCardTxtBox;

	@FindBy(how = How.XPATH, using = "//input[@class=' input' and @maxlength='10']")
	public WebElement BuildNumberTxtBox;

	@FindBy(how = How.XPATH, using = "//a[@href='/lightning/r/Quote/0060N00000lwajYQAQ/related/Quotes/view']")
	public WebElement QuoteLink;

	@FindBy(how = How.XPATH, using = "(//a[@data-refid='recordId'])[1]")
	public WebElement FirstQuote;

	@FindBy(how = How.XPATH, using = "//div[@title='New Quote']")
	public WebElement NewQuoteBtn;

	@FindBy(how = How.XPATH, using = "//span[text()='Bespoke Design Brief']")
	public WebElement BespokeDesignBrief;

	@FindBy(how = How.XPATH, using = "(//input[@type='text' and @maxlength='255'])[2]")
	public WebElement QuoteNameTextBox;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Contacts...']")
	public WebElement RetailerContactTxtBx;

	@FindBy(how = How.XPATH, using = "//a[starts-with(@href,'/lightning/r/SO_Design_Brief__c/')]")
	public WebElement DesignBriefLink;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Design Brief']")
	public WebElement EditDesignBrief;

	@FindBy(how = How.XPATH, using = "//span[text()='Press Delete to Remove']")
	public WebElement ClearDesignBrief;

	@FindBy(how = How.XPATH, using = "//span[text()='Mark Status as Complete']")
	public WebElement MarkStatusAsComplete;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Requires Design Team']")
	public WebElement EditRequiredDesign;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Requires_Design_Team__c']")
	public WebElement RequiredDesignCheckBx;

	@FindBy(how = How.XPATH, using = "//button[@name='Delete']")
	public WebElement OppDelete;

	@FindBy(how = How.XPATH, using = "//button[@title='Delete']")
	public WebElement OppDeleteConfirm;

	@FindBy(how = How.XPATH, using = "(//a[contains(@href,'/lightning/r/Order/')])[5]")
	public WebElement OrdersSection;

	public String OpportunityRecordType(String record) {
		return "//span[text()='" + record + "']";
	}

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input'])[2]")
	public WebElement OpportunityName;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input'])[3]")
	public WebElement OpportunityCloseDate;

	@FindBy(how = How.XPATH, using = "//span[@class='slds-day weekday todayDate selectedDate DESKTOP uiDayInMonthCell--default']")
	public WebElement OpportunityToday;

	@FindBy(how = How.XPATH, using = "//span[text()='25' and @data-aura-class='uiDayInMonthCell--default']")
	public WebElement OpportunityEnddate;

	@FindBy(how = How.XPATH, using = "//a[@title='Go to next month']")
	public WebElement OpportunityNextMonth;

	@FindBy(how = How.XPATH, using = "(//span[text()='Restricted Party Screening'])[3]")
	public WebElement restrictedPartyScreening;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[2]")
	public WebElement OpportunityStage;

	@FindBy(how = How.XPATH, using = "//button[@name='apply']")
	public WebElement stageApplyBtn;

	@FindBy(how = How.XPATH, using = "//span[@title='Returned']")
	public WebElement designBriefStage;

	public String SourceSelection(String value) {
		return "//span[@title='" + value + "']";
	}

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[4]")
	public WebElement Opportunityeditregion;

	public String ValueSelection(String value) {
		return "//span[@title='" + value + "']";
	}

	@FindBy(how = How.XPATH, using = "(//input[@placeholder='Search Accounts...'])[1]")
	public WebElement OpportunityAccountName;

	@FindBy(how = How.XPATH, using = "(//input[@placeholder='Search Contacts...'])[1]")
	public WebElement preferredCnt;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'in Contacts')]")
	public WebElement pCntSearch;

	public String OppAccountSearchselect(String Account) {
		return "//a[@title='" + Account + "']";
	}

	@FindBy(how = How.XPATH, using = "(//textarea[@class='slds-textarea'])[1]")
	public WebElement requestDetails;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Vehicle Manufacturers...']")
	public WebElement brand;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Vehicle Models...']")
	public WebElement model;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'in Vehicle Manufacturers')]")
	public WebElement brandSearch;

	@FindBy(how = How.XPATH, using = "(//span[contains(@title,'Show All Results')])[2]")
	public WebElement SBbrandSearch;

	@FindBy(how = How.XPATH, using = "(//span[contains(@title,'Show All Results')])[3]")
	public WebElement SBModelSearch;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'in Vehicle Models')]")
	public WebElement modelSearch;

	public String brandSearch(String brand) {
		return "//a[@title='" + brand + "']";
	}

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[23]")
	public WebElement OpportunitysourceReborn;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[10]")
	public WebElement restrictedPartyScreeningDropdown;

	@FindBy(how = How.XPATH, using = "//span[text()='Check cleared']")
	public WebElement checkCleared;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Vehicle Body Styles...']")
	public WebElement bodyStyle;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Production_Year__c']")
	public WebElement prodYear;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[14]")
	public WebElement ExtColour;

	@FindBy(how = How.XPATH, using = "//span[text()='Blue']")
	public WebElement colourBlue;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[15]")
	public WebElement CRSpeedometer;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Records...']")
	public WebElement vehicle;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[18]")
	public WebElement HandOfDrive;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[13]")
	public WebElement handDrive;

	@FindBy(how = How.XPATH, using = "//span[text()='LHD']")
	public WebElement DriveLHD;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[19]")
	public WebElement Speedometer;

	@FindBy(how = How.XPATH, using = "//span[text()='MPH']")
	public WebElement SpeedMPH;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Build_Colour__c']")
	public WebElement buildColour;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Build_Colour_Shade__c']")
	public WebElement ColourShade;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Build_Interior_Colour__c']")
	public WebElement interiorColour;

	@FindBy(how = How.XPATH, using = "(//textarea[@class='slds-textarea'])[2]")
	public WebElement specDetails;

	@FindBy(how = How.XPATH, using = "(//a[contains(@href,'/lightning/r/SO_Invoice__c/')])[2]")
	public WebElement invoiceMenu;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Retail_Price__c']")
	public WebElement retailPrice;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[21]")
	public WebElement VAT;

	@FindBy(how = How.XPATH, using = "//span[text()='Email']")
	public WebElement Source;

	@FindBy(how = How.XPATH, using = "(//input[@placeholder='Search Accounts...'])[2]")
	public WebElement PrefRetailer;

	@FindBy(how = How.XPATH, using = "(//input[@placeholder='Search Contacts...'])[2]")
	public WebElement RetailerContact;

	@FindBy(how = How.XPATH, using = "(//span[@class='title slds-path__title'])[5]")
	public WebElement OrderPlacedTab;

	@FindBy(how = How.XPATH, using = "//span[text()='Mark as Current Stage']")
	public WebElement MarkasCurrentStage;

	@FindBy(how = How.XPATH, using = "//a[contains(@href,'related/Orders/view')]")
	public WebElement OppOrder;

	@FindBy(how = How.XPATH, using = "//a[starts-with(@href,'/lightning/r/')]")
	public WebElement OppopenOrder;

	@FindBy(how = How.XPATH, using = "(//a[contains(text(),'WO')])")
	public WebElement WorkOrder;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Start - Forecast']")
	public WebElement BuildForecast;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Start_Forecast__c']")
	public WebElement BuildForecastText;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Start_Started__c']")
	public WebElement BuildCheckbox;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_ABS_Forecast__c']")
	public WebElement ABSForecast;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_ABS_Original__c']")
	public WebElement ABSOriginal;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_ABS_Complete__c']")
	public WebElement ABSCheckbox;

	@FindBy(how = How.XPATH, using = "(//a[@class='flex-wrap-ie11']//child::span[contains(text(),'00')])[2]")
	public WebElement SelectOrder;

	@FindBy(how = How.XPATH, using = "//span[text()='Mark Status as Complete']")
	public WebElement markStatusAsComplete;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Outcome__c']")
	public WebElement Outcome;

	@FindBy(how = How.XPATH, using = "(//a[@title='Outcome'])[2]")
	public WebElement outcomeStage;

	@FindBy(how = How.XPATH, using = "//span[@title='Cancelled by Customer']")
	public WebElement CancByCustomer;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-button--brand slds-path__mark-complete stepAction current uiButton']")
	public WebElement MarkStaComplete;

	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
	public WebElement OrderDone;

	@FindBy(how = How.XPATH, using = "//a[@title='Closed Lost']")
	public WebElement oppClosedLost;

	@FindBy(how = How.XPATH, using = "(//span[text()='Opportunity'])[2]")
	public WebElement OppField;

	@FindBy(how = How.XPATH, using = "(//a[contains(text(),'Test_Opportunity') and contains(@class,'textUnderline outputLookupLink slds-truncate')])[2]")
	public WebElement SelectOpp;

	@FindBy(how = How.XPATH, using = "//slot[@name='primaryField']//child::slot//child::lightning-formatted-text")
	public WebElement opporName;

	@FindBy(how = How.XPATH, using = "//select[@id='thePage:theform:thePB:pbS:pbSI_relto:relto_prefix']")
	public WebElement Relatedto;

	public String Relatedtooption(String Title) {
		return "//option[text()='" + Title + "']";
	}

	@FindBy(how = How.XPATH, using = "//select[@id='thePage:theform:thePB:pbS:j_id223:importance']")
	public WebElement Importance;

	@FindBy(how = How.XPATH, using = "//select[@id='thePage:theform:thePB:pbS:j_id122:business_unit']")
	public WebElement BUnit;

	@FindBy(how = How.XPATH, using = "//select[@id='thePage:theform:thePB:pbS:fromblock:from_address']")
	public WebElement EmailFrom;

	@FindBy(how = How.XPATH, using = "//input[@id='thePage:theform:thePB:thePBB:send']")
	public WebElement EmailSend;

	@FindBy(how = How.XPATH, using = "//span[text()='Yes']")
	public WebElement VATYes;

	@FindBy(how = How.XPATH, using = "//div[@class='slds-icon-waffle']")
	public WebElement MenuIcon;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search apps and items...']")
	public WebElement searchMenu;

	@FindBy(how = How.XPATH, using = "//a[@title='Calendars']")
	public WebElement CalendarOpt;

	@FindBy(how = How.XPATH, using = "(//a[contains(@class,'slds-truncate')])[1]")
	public WebElement selectCalendar;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Reservation Title Field']")
	public WebElement editTitle;

	@FindBy(how = How.XPATH, using = "//input[@name='B25__Reservation_Title_Field__c']")
	public WebElement titleInput;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-button_icon-border-filled']")
	public WebElement DropdownBtn;

	@FindBy(how = How.XPATH, using = "//a[@name='ChangeOwnerOne']")
	public WebElement changeOwner;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Users...']")
	public WebElement inputOwner;

	@FindBy(how = How.XPATH, using = "//span[text()='Submit']")
	public WebElement submitOwner;

	@FindBy(how = How.XPATH, using = "//span[text()='Change Record Type']")
	public WebElement changeRecordType;

	@FindBy(how = How.XPATH, using = "//span[text()='Single Resource Calendar']")
	public WebElement selectRecordType;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Strip & Parts/BiW - Complete']")
	public WebElement StripComplete;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Stage_1_Complete__c']")
	public WebElement StripCheckbox;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit ABS Complete']")
	public WebElement ABSComplete;

	@FindBy(how = How.XPATH, using = "(//input[@type='checkbox'])[1]")
	public WebElement ABSCompleteCheck;

	@FindBy(how = How.XPATH, using = "(//input[@type='checkbox'])[2]")
	public WebElement FullyPaid;

	@FindBy(how = How.XPATH, using = "(//input[@type='checkbox'])[3]")
	public WebElement DVLAReq;

	@FindBy(how = How.XPATH, using = "(//input[@type='checkbox'])[4]")
	public WebElement Heritage;

	@FindBy(how = How.XPATH, using = "(//input[@type='checkbox'])[5]")
	public WebElement FMSObt;

	@FindBy(how = How.XPATH, using = "(//input[@type='checkbox'])[6]")
	public WebElement FMSCompleted;

	@FindBy(how = How.XPATH, using = "(//input[@type='checkbox'])[7]")
	public WebElement RemoveEtracker;

	@FindBy(how = How.XPATH, using = "//button[@title='Save']")
	public WebElement SaveOrder;

	@FindBy(how = How.XPATH, using = "//span[text()='Mark Status as Complete']")
	public WebElement MarkStatusComplete;

	@FindBy(how = How.XPATH, using = "(//span[text()='Mark Status as Complete'])[2]")
	public WebElement DBMarkStatusComplete;

	@FindBy(how = How.XPATH, using = "(//input[@placeholder='Search Accounts...'])[2]")
	public WebElement SVOAccountName;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[3]")
	public WebElement prodOffering;

	@FindBy(how = How.XPATH, using = "(//a[contains(@href,'related/Quotes/view')])[2]")
	public WebElement OppQuotes;

	@FindBy(how = How.XPATH, using = "//a[@title='New Quote']")
	public WebElement NewQuote;

	@FindBy(how = How.XPATH, using = "(//input[@class=' input'])[1]")
	public WebElement quoteName;

	@FindBy(how = How.XPATH, using = "//input[@title='Search Contacts']")
	public WebElement quoteContact;

	@FindBy(how = How.XPATH, using = "(//a[contains(@class,'slds-truncate outputLookupLink slds-truncate outputLookupLink')])[1]")
	public WebElement selectQuote;

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Design Briefs')])")
	public WebElement DesignBriefs;

	@FindBy(how = How.XPATH, using = "//a[contains(@title,'SVO-2021')]")
	public WebElement QuoteName;

	@FindBy(how = How.XPATH, using = "(//a[contains(@title,'SVO-2021')])[4]")
	public WebElement DesignBriefName;

	@FindBy(how = How.XPATH, using = "(//a[contains(@class,'slds-truncate outputLookupLink slds-truncate outputLookupLink')])[1]")
	public WebElement selectDesignBriefs;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Paint']")
	public WebElement editPaint;

	@FindBy(how = How.XPATH, using = "//div[@class='ql-editor ql-blank slds-rich-text-area__content slds-grow slds-text-color_weak']")
	public WebElement inputPaint;

	@FindBy(how = How.XPATH, using = "(//div[@class='slds-rich-text-area__content slds-grow slds-text-color-weak standin'])[1]")
	public WebElement inputFinish;

	@FindBy(how = How.XPATH, using = "(//div[@class='slds-rich-text-area__content slds-grow slds-text-color-weak standin'])[2]")
	public WebElement inputDesignPacks;

	@FindBy(how = How.XPATH, using = "(//div[@class='slds-rich-text-area__content slds-grow slds-text-color-weak standin'])[3]")
	public WebElement inputWheelStyle;

	@FindBy(how = How.XPATH, using = "(//button[@title='Save'])")
	public WebElement SaveQuote;

	@FindBy(how = How.XPATH, using = "//button[@name='SO_Design_Brief__c.SO_Submit_to_Design']")
	public WebElement SubmitDesign;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-button_brand cuf-publisherShareButton undefined uiButton']")
	public WebElement SaveSubmit;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-button--neutral cuf-publisherCancelButton uiButton--default uiButton--brand uiButton']")
	public WebElement CancelSubmit;

	@FindBy(how = How.XPATH, using = "//div[@class='desktop forcePageError']")
	public WebElement SubmitError;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[1]")
	public WebElement SVOOpportunityStage;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[2]")
	public WebElement SVOprodOffering;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[3]")
	public WebElement SVOOpportunityeditregion;

	@FindBy(how = How.XPATH, using = "//div[@title='Edit']")
	public WebElement EditQuoteBtn;

	@FindBy(how = How.XPATH, using = "//a[text()='Design Brief Draft']")
	public WebElement QuoteStatusList;

	@FindBy(how = How.XPATH, using = "//span[text()='Review the errors on this page.']")
	public WebElement SaveQuoteError;

	public String StatusSelection(String ProposalSent) {
		return "//a[text()='" + ProposalSent + "']";
	}

	@FindBy(how = How.XPATH, using = "(//button[@title='Cancel'])[2]")
	public WebElement CancelQuote;

	@FindBy(how = How.XPATH, using = "//button[@name='SO_Design_Brief__c.SO_Submit_to_Design']")
	public WebElement SubmitDesignBrief;

	@FindBy(how = How.XPATH, using = "(//span[text()='Save'])[4]")
	public WebElement SaveDesignBreif;

	@FindBy(how = How.XPATH, using = "//span[@title='Approval History']")
	public WebElement ApprovalHistory;

	@FindBy(how = How.XPATH, using = "//a[@class='menuTriggerLink slds-button slds-button_icon slds-button_icon slds-button_icon-container slds-button_icon-small slds-global-actions__setup slds-global-actions__item-action']")
	public WebElement setUpBtn;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Setup']")
	public WebElement SearchSetup;

	@FindBy(how = How.XPATH, using = "(//input[@value=' Login '])[1]")
	public WebElement UserLoginBtn;

	@FindBy(how = How.XPATH, using = "//button[@name='SO_Design_Brief__c.SO_Return_Render_Pack']")
	public WebElement ReturnRenderPack;

	@FindBy(how = How.XPATH, using = "//button[@name='Save']")
	public WebElement SaveReturnPack;

	@FindBy(how = How.XPATH, using = "(//span[starts-with(text(),'SVO-2021')])[8]")
	public WebElement QuoteBackLink;

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Orders')])[3]")
	public WebElement QuoteOrder;

	@FindBy(how = How.XPATH, using = "//span[text()='SVO Bespoke Order']")
	public WebElement SVOOrder;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Opportunities...']")
	public WebElement OrderOpp;

	@FindBy(how = How.XPATH, using = "(//input[@class=' input'])[4]")
	public WebElement OrderOppStart;

	@FindBy(how = How.XPATH, using = "//button[text()='Today']")
	public WebElement SelectDate;

	@FindBy(how = How.XPATH, using = "//div[@title='Delete']")
	public WebElement QuoteDelete;

	@FindBy(how = How.XPATH, using = "(//span[text()='Files'])[1]")
	public WebElement QuoteFiles;

	@FindBy(how = How.XPATH, using = "//div[@title='Add Files']")
	public WebElement AddFiles;

	@FindBy(how = How.XPATH, using = "(//input[@type='checkbox'])[2]")
	public WebElement FileSelect;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-button--neutral attach uiButton--default uiButton--brand uiButton']")
	public WebElement AddButton;

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'VIP-')])[9]")
	public WebElement QuoteSelect;

	@FindBy(how = How.XPATH, using = "//span[@title='Accepted']")
	public WebElement OutcomeAccept;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Outcome_Detail__c']")
	public WebElement OutcomeDetail;

	@FindBy(how = How.XPATH, using = "(//span[@title='Accepted'])[2]")
	public WebElement OutcomeDetAccept;

	@FindBy(how = How.XPATH, using = "//span[text()='Proposal Sent']")
	public WebElement StageProposal;

	@FindBy(how = How.XPATH, using = "//span[@class='toastMessage forceActionsText']")
	public WebElement StageError;

	@FindBy(how = How.XPATH, using = "(//div[@title='New'])[2]")
	public WebElement NewOrder;

	@FindBy(how = How.XPATH, using = "(//input[@data-aura-class='uiInputSmartNumber'])[1]")
	public WebElement QuoteCP;

	@FindBy(how = How.XPATH, using = "(//input[@data-aura-class='uiInputSmartNumber'])[2]")
	public WebElement QuoteLP;

	@FindBy(how = How.XPATH, using = "(//input[@data-aura-class='uiInputSmartNumber'])[3]")
	public WebElement QuoteContribution;

	@FindBy(how = How.XPATH, using = "(//input[@data-aura-class='uiInputSmartNumber'])[4]")
	public WebElement QuoteRP;

	@FindBy(how = How.XPATH, using = "(//input[@data-aura-class='uiInputSmartNumber'])[5]")
	public WebElement QuoteRevenue;

	@FindBy(how = How.XPATH, using = "//button[@name='SO_Design_Brief__c.SO_Return_Render_Pack']")
	public WebElement DesignReturned;

	@FindBy(how = How.XPATH, using = "//div[@title='Send Quote Pack']")
	public WebElement SendQuotePack;

	@FindBy(how = How.XPATH, using = "//input[@value='E-Sign Documents In Person']")
	public WebElement ESignDoc;

	@FindBy(how = How.XPATH, using = "//span[@id='theErrorPage:theError']")
	public WebElement ESignError;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[2]")
	public WebElement POOpportunityStage;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[1]")
	public WebElement POprodOffering;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[4]")
	public WebElement PORegion;

	@FindBy(how = How.XPATH, using = "(//input[@placeholder='Search Accounts...'])[2]")
	public WebElement POAccountName;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[11]")
	public WebElement POrestrictedParty;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Check cleared')]")
	public WebElement POCheckcleared;

	@FindBy(how = How.XPATH, using = "(//a[@title='Edit Key Fields'])[2]")
	public WebElement EditOutcome;

	@FindBy(how = How.XPATH, using = "//button[@title='Close this window']")
	public WebElement CloseWindow;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Show All')]")
	public WebElement ShowAllBtn;

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Stage History')])[1]")
	public WebElement StageHistory;

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Opportunity Field History')])[1]")
	public WebElement OppFieldHistory;

	@FindBy(how = How.XPATH, using = "(//a[@class='forceBreadCrumbItem'])[2]")
	public WebElement BackToOpp;

	@FindBy(how = How.XPATH, using = "//span[@title='Rejected']")
	public WebElement OutcomeReject;

	@FindBy(how = How.XPATH, using = "(//span[@title='Rejected'])[2]")
	public WebElement OutcomeDetReject;

	@FindBy(how = How.XPATH, using = "(//a[@class='tabHeader slds-path__link'])[1]")
	public WebElement DesignBriefDraft;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[2]")
	public WebElement ContinuationStage;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[4]")
	public WebElement ContinuationRegion;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Programmes...']")
	public WebElement ContinuationPrgm;

	@FindBy(how = How.XPATH, using = "(//input[@class=' input'])[1]")
	public WebElement ClaLimEdiProgramme;

	@FindBy(how = How.XPATH, using = "(//a[@class='select'])[1]")
	public WebElement ClaLimEdiProStatua;

	@FindBy(how = How.XPATH, using = "//a[@title='Planned']")
	public WebElement ClaLimEdiProSelectStatus;

	@FindBy(how = How.XPATH, using = "(//a[@class='select'])[2]")
	public WebElement ClaLimEdiProOffering;

	@FindBy(how = How.XPATH, using = "//a[@title='Limited Edition']")
	public WebElement ClaLimEdiProOfferingValue;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[11]")
	public WebElement restrictedPS;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[24]")
	public WebElement ContinuationSource;

	@FindBy(how = How.XPATH, using = "(//button[@title='Edit Priority Position'])[1]")
	public WebElement PriorityPositionBtn;

	@FindBy(how = How.XPATH, using = "//input[@data-aura-class='uiInputSmartNumber']")
	public WebElement PriorityPosition;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[22]")
	public WebElement VATinput;

	@FindBy(how = How.XPATH, using = "//a[@data-tab-name='Contracting']")
	public WebElement ContractingTab;

	@FindBy(how = How.XPATH, using = "(//input[@type='checkbox'])[2]")
	public WebElement selectFileEmail;

	@FindBy(how = How.XPATH, using = "//input[@value='Send Documents for Electronic Signature']")
	public WebElement DocESign;

	@FindBy(how = How.XPATH, using = "//span[text()='Change Closed Stage']")
	public WebElement ChangeClosedStage;

	@FindBy(how = How.XPATH, using = "//input[@name='StageName']")
	public WebElement InputStage;

	@FindBy(how = How.XPATH, using = "//span[@title='Contract Signed']")
	public WebElement ContractSignedStage;

	@FindBy(how = How.XPATH, using = "//a[@class='action-link']")
	public WebElement LogoutUser;

	@FindBy(how = How.XPATH, using = "// div[@class='toastContent slds-notify__content']")
	public WebElement OppStageError;

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Commissioning Request')])[1]")
	public WebElement ComRequest;

	@FindBy(how = How.XPATH, using = "(//input[@type='text'])[3]")
	public WebElement ListViewTextBox1;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Model_Year_Start__c']")
	public WebElement InputModelStart;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Model_Year_End__c']")
	public WebElement InputModelEnd;

	@FindBy(how = How.XPATH, using = "//div[@class='slds-form-element__help']")
	public WebElement YearError;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Name']")
	public WebElement EditName;

	@FindBy(how = How.XPATH, using = "//input[@name='Name']")
	public WebElement InputName;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-button_neutral search-button slds-truncate']")
	public WebElement SearchBtn;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search...']")
	public WebElement SearchBox;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Model Year Start']")
	public WebElement ModelStart;

	@FindBy(how = How.XPATH, using = "(//input[@name='SO_Starting__c'])[1]")
	public WebElement StartDate;

	@FindBy(how = How.XPATH, using = "(//input[@name='SO_Ending__c'])[1]")
	public WebElement EndDate;

	@FindBy(how = How.XPATH, using = "(//a[contains(@class,'slds-truncate outputLookupLink slds-truncate outputLookupLink')])[1]")
	public WebElement SelectComReq;

	@FindBy(how = How.XPATH, using = "//span[@title='Reservations']")
	public WebElement Reservations;

	@FindBy(how = How.XPATH, using = "(//input[@name='B25__StartLocal__c'])[1]")
	public WebElement StartReserv;

	@FindBy(how = How.XPATH, using = "(//input[@name='B25__EndLocal__c'])[1]")
	public WebElement EndReserv;

	@FindBy(how = How.XPATH, using = "(//button[@title='Edit Status'])[2]")
	public WebElement EditStatus;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[3]")
	public WebElement InputStatus;

	@FindBy(how = How.XPATH, using = "//span[@title='Booked']")
	public WebElement BookedStatus;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search People...']")
	public WebElement inputHost;

	@FindBy(how = How.XPATH, using = "(//a[@class='flex-wrap-ie11'])[3]")
	public WebElement BackComReq;

	@FindBy(how = How.XPATH, using = "//span[@title='Closed']")
	public WebElement ClosedStatus;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[5]")
	public WebElement InputReason;

	@FindBy(how = How.XPATH, using = "(//input[@class=' input'])[5]")
	public WebElement HandoverDate;

	@FindBy(how = How.XPATH, using = "//span[@title='Invoices']")
	public WebElement Invoices;

	@FindBy(how = How.XPATH, using = "(//a[contains(@class,'slds-truncate outputLookupLink slds-truncate outputLookupLink')])[1]")
	public WebElement OpenInvoice;

	@FindBy(how = How.XPATH, using = "(//div[@class='slds-form-element slds-hint-parent test-id__output-root slds-form-element_readonly slds-form-element_stacked'])[1]")
	public WebElement InvoiceName;

	@FindBy(how = How.XPATH, using = "(//div[@class='slds-form-element slds-hint-parent test-id__output-root slds-form-element_readonly slds-form-element_stacked'])[2]")
	public WebElement InvoiceStatus;

	@FindBy(how = How.XPATH, using = "(//div[@title='New'])[2]")
	public WebElement newInvoice;

	@FindBy(how = How.XPATH, using = "(//input[@aria-haspopup='listbox'])[3]")
	public WebElement RegionDWMike;

	@FindBy(how = How.XPATH, using = "(//input[@aria-haspopup='listbox'])[1]")
	public WebElement StageDWMike;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Common Order Number']")
	public WebElement EditCommonOrder;

	@FindBy(how = How.XPATH, using = "//input[@class=' input' and @maxlength='50']")
	public WebElement CommonOrderTextBox;

	@FindBy(how = How.XPATH, using = "//a[starts-with(@href,'/lightning/r/AttachedContentDocument/')]")
	public WebElement FilesLink;

	@FindBy(how = How.XPATH, using = "//div[@title='Add Files']")
	public WebElement AddFilesBtn;

	@FindBy(how = How.XPATH, using = "(//span[@class='slds-checkbox--faux'])[1]")
	public WebElement FirstFileCheckBox;

	@FindBy(how = How.XPATH, using = "//span[text()='Add (1)']")
	public WebElement AddBtn;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Body Style']")
	public WebElement EditBodyStyle;

	@FindBy(how = How.XPATH, using = "(//button[@title='Clear Selection'])[7]")
	public WebElement ClearBodyStyle;

	@FindBy(how = How.XPATH, using = "//span[text()='Client']")
	public WebElement ClientDetails;

	@FindBy(how = How.XPATH, using = "(//button[@title='Clear Selection'])[3]")
	public WebElement ClearSAP;

	@FindBy(how = How.XPATH, using = "//option[@value='RELATED_OBJECT_EMAIL']")
	public WebElement ROE;

	@FindBy(how = How.XPATH, using = "//option[text='Topic Shared Document Folder']")
	public WebElement TopicSharedDocument;

	@FindBy(how = How.XPATH, using = "(//input[@aria-haspopup='listbox'])[5]")
	public WebElement RegionPrivateOffice;

	@FindBy(how = How.XPATH, using = "//a[starts-with(@href,'/lightning/r/SO_Commissioning_Request__c/')]")
	public WebElement CommissioningRequestLink;

	@FindBy(how = How.XPATH, using = "//div[@title='New']")
	public WebElement NewCRBtn;

	public String RTypeSelection(String RecordType) {
		return "//span[text()='" + RecordType + "']";
	}

	@FindBy(how = How.XPATH, using = "(//input[@aria-autocomplete='none'])[1]")
	public WebElement StatusDW;

	@FindBy(how = How.XPATH, using = "(//input[@aria-autocomplete='none'])[2]")
	public WebElement TourTypeDW;

	@FindBy(how = How.XPATH, using = "(//input[@name='SO_Starting__c'])[1]")
	public WebElement Starting;

	@FindBy(how = How.XPATH, using = "(//input[@name='SO_Ending__c'])[1]")
	public WebElement Ending;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Duration_hours__c']")
	public WebElement Duration;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Est_Attendees__c']")
	public WebElement EstAttendees;

	@FindBy(how = How.XPATH, using = "//button[@name='SO_Commissioning_Request__c.SO_Submit']")
	public WebElement SubmitCRbtn;

	@FindBy(how = How.XPATH, using = "//a[starts-with(@href,'/lightning/r/AttachedContentNote/')]")
	public WebElement NoteLink;

	@FindBy(how = How.XPATH, using = "//input[@class='inputText notesTitle flexInput input']")
	public WebElement NoteTitle;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Share')]")
	public WebElement ShareBtn;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search People...']")
	public WebElement ShareWithuser;

	@FindBy(how = How.XPATH, using = "(//span[text()='Share'])[2]")
	public WebElement ShareBtn1;

	@FindBy(how = How.XPATH, using = "(//span[text()='Delete'])[2]")
	public WebElement DeleteNote;

	@FindBy(how = How.XPATH, using = "//input[@type='text']")
	public WebElement SearchListTextBox;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Programmes...']")
	public WebElement ProgrammeTextBox;

	@FindBy(how = How.XPATH, using = "//span[@title='New Programme']")
	public WebElement NewProgrammeRecord;

	@FindBy(how = How.XPATH, using = "(//span[@class='test-id__section-header-title section-header-title slds-p-horizontal--small slds-truncate'])[5]")
	public WebElement VehicleRequestDetail;

	@FindBy(how = How.XPATH, using = "//span[text()='Insufficient Privileges']")
	public WebElement InsufficientPrivileges;

	@FindBy(how = How.XPATH, using = "//span[text()='Client']")
	public WebElement ClientDetail;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Restricted Party Screening']")
	public WebElement EditRPS;

	@FindBy(how = How.XPATH, using = "(//input[@aria-autocomplete='none'])[8]")
	public WebElement RPSdropdown;

	@FindBy(how = How.XPATH, using = "//lightning-icon[@class='slds-button__icon slds-icon-utility-down slds-icon_container forceIcon']")
	public WebElement orderDrpodownBtn;

	@FindBy(how = How.XPATH, using = "(//span[text()='Cancel'])[2]")
	public WebElement CancelOpp;

	@FindBy(how = How.XPATH, using = "(//a[contains(@class,'textUnderline outputLookupLink slds-truncate outputLookupLink')])[1]")
	public WebElement OpenOpp;

	@FindBy(how = How.XPATH, using = "//span[text()='Donor Details']")
	public WebElement donorDetails;

	@FindBy(how = How.XPATH, using = "//a[starts-with(@href,'/lightning/r/02i0N000')]")
	public WebElement VehicleNumber;

	@FindBy(how = How.XPATH, using = "(//span[@class='title'])[1]")
	public WebElement RelatedTab;

	@FindBy(how = How.XPATH, using = "(//a[@title='New'])[2]")
	public WebElement NewEnqBtn;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Subject__c']")
	public WebElement EnqTitle;

	@FindBy(how = How.XPATH, using = "//th[@title='Enquiry Title']")
	public WebElement CreatedEnqTitle;

	@FindBy(how = How.XPATH, using = "//span[@title='New Record']")
	public WebElement VehicleNewRecord;

	@FindBy(how = How.XPATH, using = "//li[@class='form-element__help']")
	public WebElement VINError;

	@FindBy(how = How.XPATH, using = "//button[@title='Cancel']")
	public WebElement CancelVehicle;

	@FindBy(how = How.XPATH, using = "(//button[@title='Clear Selection'])[7]")
	public WebElement ClearSelection;

	@FindBy(how = How.XPATH, using = "//span[text()='More']")
	public WebElement MoreBtn;

	@FindBy(how = How.XPATH, using = "(//span[text()='Vehicle Manufacturers'])[2]")
	public WebElement VehicleManuf;

	@FindBy(how = How.XPATH, using = "(//span[@class='slds-checkbox--faux'])[2]")
	public WebElement Vehicle1;

	@FindBy(how = How.XPATH, using = "(//span[@class='slds-checkbox--faux'])[3]")
	public WebElement Vehicle2;

	@FindBy(how = How.XPATH, using = "(//span[@class='slds-checkbox--faux'])[4]")
	public WebElement Vehicle3;

	@FindBy(how = How.XPATH, using = "//div[@title='Change Owner']")
	public WebElement ChangeOwnBtn;

	@FindBy(how = How.XPATH, using = "(//span[@class='slds-grid slds-grid--align-spread forceInlineEditCell'])[2]")
	public WebElement SelectJLROwned;

	@FindBy(how = How.XPATH, using = "(//button[@class='slds-button trigger slds-button_icon-border'])[2]")
	public WebElement EditJLROwned;

	@FindBy(how = How.XPATH, using = "(//input[@type='checkbox'])[3]")
	public WebElement CheckJLROwned;

	@FindBy(how = How.XPATH, using = "//a[@title='Jaguar']")
	public WebElement SelectManuf;

	@FindBy(how = How.XPATH, using = "(//a[@class='flex-wrap-ie11 slds-truncate'])[1]")
	public WebElement SelectModel;

	@FindBy(how = How.XPATH, using = "//input[@name='Name']")
	public WebElement VehicleName;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Model_Year_Start__c']")
	public WebElement startYear;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Model_Year_End__c']")
	public WebElement endYear;

	@FindBy(how = How.XPATH, using = "(//span[text()='All Classic Bespoke Opportunities'])[1]")
	public WebElement ClassicBespoke;

	@FindBy(how = How.XPATH, using = "(//button[@name='New'])[3]")
	public WebElement NewBodyStyles;

	@FindBy(how = How.XPATH, using = "(//button[@name='New'])[4]")
	public WebElement NewDerivatives;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Vehicle Series...']")
	public WebElement SelectSeries;

	@FindBy(how = How.XPATH, using = "//span[text()='Sharing']")
	public WebElement SharingBtn;

	@FindBy(how = How.XPATH, using = "//button[@title='Clear Selection']")
	public WebElement ClearModel;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search User...']")
	public WebElement SearchUser;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-button--neutral  button-brand uiButton--default uiButton--brand uiButton uiButton']")
	public WebElement SaveSharing;

	@FindBy(how = How.XPATH, using = "//a[@title='Tasks']")
	public WebElement TasksTab;

	@FindBy(how = How.XPATH, using = "(//div[@class='slds-grid slds-wrap'])[2]")
	public WebElement FirstTask;

	@FindBy(how = How.XPATH, using = "//a[@title='Show 3 more actions']")
	public WebElement EditDropDown;

	@FindBy(how = How.XPATH, using = "//a[@title='Change Date']")
	public WebElement ChangeDate;

	@FindBy(how = How.XPATH, using = "//input[@class='inputDate input']")
	public WebElement ChangeDateTxtBx;

	@FindBy(how = How.XPATH, using = "//p[@class='detail']")
	public WebElement ErrorMsg;

	@FindBy(how = How.XPATH, using = "//span[text()='Cancel']")
	public WebElement CancelError;

	@FindBy(how = How.XPATH, using = "//div[@title='Edit']")
	public WebElement EditTask;

	@FindBy(how = How.XPATH, using = "//a[text()='Normal']")
	public WebElement taskPriority;

	@FindBy(how = How.XPATH, using = "//a[text()='High']")
	public WebElement highTaskPriority;

	@FindBy(how = How.XPATH, using = "//button[@title='Save']")
	public WebElement editTaskSave;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Assigned To']")
	public WebElement EditAssignTo;

	@FindBy(how = How.XPATH, using = "(//span[@class='deleteIcon'])[1]")
	public WebElement ClearAssignTo;

	@FindBy(how = How.XPATH, using = "//input[@aria-describedby='Assigned To']")
	public WebElement AssignTo;

	@FindBy(how = How.XPATH, using = "//button[@data-aura-class='runtime_sales_activitiesTaskStatusButton']")
	public WebElement MarkComplete;

	@FindBy(how = How.XPATH, using = "//a[@title='Delete']")
	public WebElement DeleteTask;

	@FindBy(how = How.XPATH, using = "//div[@class='detail slds-text-align--center']")
	public WebElement ErrorNote;

	@FindBy(how = How.XPATH, using = "//button[@title='Close this window']")
	public WebElement CancelIcon;

	@FindBy(how = How.XPATH, using = "//a[@title='Show one more action']")
	public WebElement NewDropDown;

	@FindBy(how = How.XPATH, using = "//a[@title='New Task']")
	public WebElement NewTask;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[2]")
	public WebElement TaskSubject;

	@FindBy(how = How.XPATH, using = "//a[@title='New']")
	public WebElement NewCampaign;

	@FindBy(how = How.XPATH, using = "(//a[@title='New'])[2]")
	public WebElement NewCommReq;

	@FindBy(how = How.XPATH, using = "//input[@maxlength='80']")
	public WebElement CampaignName;

	@FindBy(how = How.XPATH, using = "//a[@data-tab-name='detailTab']")
	public WebElement CampaignDetails;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Parent Campaign']")
	public WebElement EditParentCampaign;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Campaigns...']")
	public WebElement SearchParentCampaign;

	@FindBy(how = How.XPATH, using = "//a[@data-tab-name='LogACall']")
	public WebElement LogACallTab;

	@FindBy(how = How.XPATH, using = "//a[@data-tab-name='NewEvent']")
	public WebElement NewEventTab;

	@FindBy(how = How.XPATH, using = "//a[@data-tab-name='NewTask']")
	public WebElement TaskTab;

	@FindBy(how = How.XPATH, using = "(//a[text()='More'])[3]")
	public WebElement MoreTab;

	@FindBy(how = How.XPATH, using = "(//a[@title='Email'])[2]")
	public WebElement EmailTab;

	@FindBy(how = How.XPATH, using = "(//input[@type='text'])[1]")
	public WebElement EmailTo;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Enter Subject...']")
	public WebElement EmailSubject;

	@FindBy(how = How.XPATH, using = "//button[@title='Add']")
	public WebElement AddLogBtn;

	@FindBy(how = How.XPATH, using = "//span[text()='Send']")
	public WebElement SendEmail;

	@FindBy(how = How.XPATH, using = "//input[@class='slds-input slds-combobox__input']")
	public WebElement SubjectLogTxtBx;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[1]")
	public WebElement SubjectEventTxtBx;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[4]")
	public WebElement SubjectTaskTxtBx;

	@FindBy(how = How.XPATH, using = "(//span[text()='Items to Approve'])")
	public WebElement ItemsToApprove;

	@FindBy(how = How.XPATH, using = "//button[text()='Manage All']")
	public WebElement ManageAll;

	@FindBy(how = How.XPATH, using = "//span[text()='New email']")
    public WebElement NewEmail;

    @FindBy(how = How.XPATH, using = "//div[@role='textbox']")
    public WebElement Tomail;

    @FindBy(how = How.XPATH, using = "//div[contains(@class,'TextField-fieldGroup fieldGroup')]")
    public WebElement SubjectEmail;
    
    @FindBy(how = How.XPATH, using = "//span[text()='Mark Enquiry Status as Complete']")
    public WebElement MarkEnquiryCompleted;
    
}
