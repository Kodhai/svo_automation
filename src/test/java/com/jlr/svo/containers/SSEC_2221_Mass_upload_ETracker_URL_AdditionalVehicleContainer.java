package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SSEC_2221_Mass_upload_ETracker_URL_AdditionalVehicleContainer {

	@FindBy(how = How.XPATH, using = "//label[text()='Brand']")
	public WebElement BrandAdditionalVehicle;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Model']")
	public WebElement ModelAdditionalVehicle;
	
	public String ETracker(String value) {
		return "//span[text()='" + value + "'] + //child::lightning-base-combobox-formatted-text[@title='Defender']";
	}
	
	@FindBy(how = How.XPATH, using = "//label[text()='E-Tracker URL']")
	public WebElement EtrackerURLlabel;
	
	@FindBy(how = How.XPATH, using = "//label[text()='E-Tracker ID']")
	public WebElement ETRackerIDLabel;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Setup']")
	public WebElement SetupBtn;
	
	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Setup']")
	public WebElement SearchSetupBox;
	
	@FindBy(how = How.XPATH, using = "//div[text()='User']")
	public WebElement MikeKingUser;
	
	@FindBy(how = How.XPATH, using = "//input[@title='Login']")
	public WebElement LoginUserBtn;
	
	@FindBy(how = How.XPATH, using = "//input[@placeholder='Quick Find']")
	public WebElement QuickFindSearchTab;
	
	@FindBy(how = How.XPATH, using = "//div[@title='Data Import Wizard']")
	public WebElement DataImportWizard;
	
	@FindBy(how = How.XPATH, using = "//a[text()='Launch Wizard!']")
	public WebElement LaunchWizardBtn;
	
	@FindBy(how = How.XPATH, using = "//button[@title='Edit E-Tracker URL']")
	public WebElement EditETrackerURL; 
	
	@FindBy(how = How.XPATH, using = "//button[@name='SaveEdit']")
	public WebElement SaveEditBtn;
	

}
