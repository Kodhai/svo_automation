package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SSEC_1126_AddHandOfDriveToKMIContainer {

	@FindBy(how = How.XPATH, using = "//a//child::span[text()='KMIs']")
	public WebElement KMIsTab;
	
	@FindBy(how = How.XPATH, using = "//div[@title='New']")
	public WebElement NewKMIBtn;
	
	public String selectMenu(String text) {
		return "//b[text()='" + text + "']";
	}
	
	@FindBy(how = How.XPATH, using = "//input[@name='SO_Active__c']")
	public WebElement ActiveStatus;
	
	@FindBy(how = How.XPATH, using = "//div//child::input[@placeholder='Search Contacts...']")
	public WebElement KMIContacts;
	
	@FindBy(how = How.XPATH, using = "//button[@name='SaveEdit']")
	public WebElement SaveKMIBtn;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Contact']//following-sibling::div//child::div//child::lightning-base-combobox//child::div//child::div[contains(@class,'slds-dropdown_left')]//child::lightning-base-combobox-item//child::span[@class='slds-media__body']//child::span[contains(@class,'slds-listbox__option-text_entity')]//span[contains(text(),'Show All Results for')]")
	public WebElement KMIContactNameSearch;
	
	public String ValueSelection(String value) {
		return "//table[contains(@class,'uiVirtualDataGrid')]//child::tbody//child::tr//child::td//child::a[text()='"
				+ value + "']";
	}
	
	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Product Offering')]//following-sibling::div//child::lightning-base-combobox//child::div//child::div//child::button")
	public WebElement KMIProductoffering;
	
	public String ProdOffSelection(String Value) {
		return "//span[text() = '" + Value + "']";
	}
	
	@FindBy(how = How.XPATH, using = "//div[@class='slds-media__body']//child::slot//child::lightning-formatted-text")
	public WebElement KMIName;
	
	@FindBy(how = How.XPATH, using = "//button[@title='Select a List View']")
	public WebElement KMIsListViewDropdon;
	
	@FindBy(how = How.XPATH, using = "//span[text()='All Active KMI']")
	public WebElement AllActiveKMIsListView;
	
	@FindBy(how = How.XPATH, using = "//span[text()='All Inactive KMI']")
	public WebElement AllInactiveKMIsListView;
	
	@FindBy(how = How.XPATH, using = "//button[@name='Edit']")
	public WebElement EditKMIBtn;
	
	@FindBy(how = How.XPATH, using = "//input[@name='SO_KMI__c-search-input']")
	public WebElement KMIsSearchBox;
	
	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Enquiries...']")
	public WebElement SourceEnquirySearchBox;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Source Enquiry']//following-sibling::div//child::div//child::lightning-base-combobox//child::div//child::div[contains(@class,'slds-dropdown_left')]//child::lightning-base-combobox-item//child::span[@class='slds-media__body']//child::span[contains(@class,'slds-listbox__option-text_entity')]//span[contains(text(),'Show All Results for')]")
	public WebElement SourceEnquirySearch;
	
	public String SourceEnquirySelection(String Value) {
		return "//a[text() = '" + Value + "']";
	}
	
	@FindBy(how = How.XPATH, using = "//div//child::span[text()='Source Enquiry']//parent::div//following-sibling::div//child::span//child::slot//child::force-lookup//child::div//child::records-hoverable-link//child::div//child::a//child::slot//child::slot//child::span")
	public WebElement SourceEnquiryLink;
	
	@FindBy(how = How.XPATH, using = "//lightning-formatted-text[contains(text(),'ENQ-')]")
	public WebElement EnquiryName;
	
	@FindBy(how = How.XPATH, using = "//strong[text()='Review the errors on this page.']")
	public WebElement ErrorMessage;
	
	@FindBy(how = How.XPATH, using = "//button[@title='Edit KMI Name']")
	public WebElement EditKMINameBtn;
	
	@FindBy(how = How.XPATH, using = "//tbody//tr//td[@class='slds-cell-edit cellContainer']")
	public WebElement KMICheckBox;
	
	@FindBy(how = How.XPATH, using = "//div[@title='Change Owner']")
	public WebElement ChangeOwnerBtn;
	
	@FindBy(how = How.XPATH, using = "//input[@title='Search Users']")
	public WebElement OwnerSearchBox;
	
	@FindBy(how = How.XPATH, using = "//span[contains(@title,' in Users')]")
	public WebElement OwnerSearch;
	
	public String OwnerSelection(String Value) {
		return "//a[text() = '" + Value + "']";
	}
	
	@FindBy(how = How.XPATH, using = "//span[text()='Submit']")
	public WebElement SaveChangeOwner;
	
	@FindBy(how = How.XPATH, using = "//input[@class=' input']")
	public WebElement enquiryTitle;
	
	@FindBy(how = How.XPATH, using = "//button[@name='CancelEdit']")
	public WebElement CancelBtn;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Source Enquiry']//following-sibling::div//child::div//child::lightning-base-combobox//child::div//child::div[contains(@class,'slds-dropdown_left')]//child::lightning-base-combobox-item//child::span[@class='slds-media__body']//child::span[contains(@class,'slds-listbox__option-text_entity')]//span[contains(text(),'New Enquiry')]")
	public WebElement NewEnquiryBtn;

	
}
