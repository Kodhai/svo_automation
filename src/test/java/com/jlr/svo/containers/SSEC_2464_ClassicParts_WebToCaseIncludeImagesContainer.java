package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SSEC_2464_ClassicParts_WebToCaseIncludeImagesContainer {

	@FindBy(how = How.XPATH, using = "//h2[text()='JAGUAR LAND ROVER CLASSIC']")
	public WebElement JaguarLandRoverDescripton;
	
	@FindBy(how = How.XPATH, using = "//input[@name='faultImages']")
	public WebElement ChooseFileBtn;
	
	@FindBy(how = How.XPATH, using = "//button[@title='Delete image']")
	public WebElement DeleteImageBtn;
	
	@FindBy(how = How.XPATH, using = "//label[@class='rc-anchor-center-item rc-anchor-checkbox-label']")
	public WebElement Captcha;
	
	@FindBy(how = How.XPATH, using = "//a[text()='GENUINE PARTS']")
	public WebElement GenuineParts;
	
	@FindBy(how = How.XPATH, using = "//a[text()='TAILOR MADE & BESPOKE']")
	public WebElement TailorMadeandBespoke;
	
	@FindBy(how = How.XPATH, using = "//a[text()='PARTS TECHNICAL ADVICE']")
	public WebElement PartsTechnicalAdvice;
	
	@FindBy(how = How.XPATH, using = "//a[text()='GLOBAL DELIVERY']")
	public WebElement GlobalDelivery;
	
	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
	
}
