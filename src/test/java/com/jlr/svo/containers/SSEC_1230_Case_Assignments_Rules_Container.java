package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SSEC_1230_Case_Assignments_Rules_Container {

	@FindBy(how = How.XPATH, using = "//li[@class='slds-dropdown__item has-icon--left   forceVirtualAutocompleteMenuOption']//child::a//child::span[text()='Parts and Technical Queue']")
	public WebElement SelectPartsAndTechnicalCaseQueue;
	
	@FindBy(how = How.XPATH, using = "//li[@class='slds-dropdown__item has-icon--left   forceVirtualAutocompleteMenuOption']//child::a//child::span[text()='Customer Service - All Cases']")
	public WebElement SelectCustomerServiceCaseQueue;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Case Record Type']//parent::div//following-sibling::div//child::span//child::slot//child::records-record-type//child::div//child::div//child::span")
	public WebElement CaseRecordType;
}
