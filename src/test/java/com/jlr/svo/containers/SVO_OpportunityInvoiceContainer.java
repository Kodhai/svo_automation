package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SVO_OpportunityInvoiceContainer {

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search SAP Accounts...']")
	public WebElement SAPAccountSearch;

	@FindBy(how = How.XPATH, using = "//label[text()='SAP Account']//following-sibling::div//child::div//child::lightning-base-combobox//child::div//child::div[contains(@class,'slds-dropdown_left')]//child::lightning-base-combobox-item//child::span[@class='slds-media__body']//child::span[contains(@class,'slds-listbox__option-text_entity')]//span[contains(text(),'Show All Results for')]")
	public WebElement SAPAccountNameSearch;

	public String SAPAccount(String Value) {
		return "//a[@title = '" + Value + "']";
	}

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Vehicle']")
	public WebElement EditVehicleBtn;

	@FindBy(how = How.XPATH, using = "//input[@title='Search Records']")
	public WebElement VehicleSearch;

	@FindBy(how = How.XPATH, using = "//input[@class='input uiInputSmartNumber']")
	public WebElement ClassicRetailPrice;

	@FindBy(how = How.XPATH, using = "//span[text()='Save']")
	public WebElement ClassicOppDetailsSave;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit VAT Qualifying']")
	public WebElement ClassicOppPriceDetailsEditBtn;

	@FindBy(how = How.XPATH, using = "//button[@aria-label='VAT Qualifying, --None--']")
	public WebElement ClassicOppVATQualifying;

	@FindBy(how = How.XPATH, using = "//span[@title='Yes']")
	public WebElement ClassicOppVATQualifyingYes;

	@FindBy(how = How.XPATH, using = "//button[text()='Save']")
	public WebElement ClassicOppVATQualifyingSaveBtn;

	@FindBy(how = How.XPATH, using = "//div[@class='test-id__field-label-container slds-form-element__label no-utility-icon']//child::span[text()='Account Name']//parent::div//following-sibling::div//child::span//child::slot//child::force-lookup//child::div//child::records-hoverable-link//child::div//child::a//child::slot//child::slot//child::span")
	public WebElement AccountName;

	@FindBy(how = How.XPATH, using = "//div[@class='test-id__field-label-container slds-form-element__label no-utility-icon']//child::span[text()='SAP Account']//parent::div//following-sibling::div//child::span//child::slot//child::force-lookup//child::div//child::records-hoverable-link//child::div//child::a//child::slot//child::slot//child::span")
	public WebElement SAPAccountName;

	@FindBy(how = How.XPATH, using = "//div[@title='New']")
	public WebElement NewInvoiceBtn;

	@FindBy(how = How.XPATH, using = "//span[text()='Deposit']")
	public WebElement DepositRecordType;

	public String RecordType(String Value) {
		return "//span[text() = '" + Value + "']";
	}

	@FindBy(how = How.XPATH, using = "//span[text()='Credit Note']")
	public WebElement CreditNoteRecordType;

	@FindBy(how = How.XPATH, using = "//span[text()='Invoice']")
	public WebElement InvoiceRecordType;

	@FindBy(how = How.XPATH, using = "//span[text()='Next']")
	public WebElement InvoiceNextBtn;

	@FindBy(how = How.XPATH, using = "//input[@class='slds-combobox__input slds-input slds-combobox__input-value']")
	public WebElement OpportunityNameOnInvoiceCreationPage;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Orders...']")
	public WebElement OrderNumberOnInvoiceCreationPage;

	@FindBy(how = How.XPATH, using = "//input[@class='slds-combobox__input slds-input']")
	public WebElement AccountNameOnInvoiceCreationPage;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search SAP Accounts...']")
	public WebElement SAPAccountNameOnInvoiceCreationPage;

	@FindBy(how = How.XPATH, using = "//div[@class='test-id__field-label-container slds-form-element__label no-utility-icon']//child::span[text()='Opportunity']//parent::div//following-sibling::div//child::span//child::slot//child::force-lookup//child::div//child::records-hoverable-link//child::div//child::a//child::slot//child::slot//child::span")
	public WebElement OpportunityNameOnInvoicePage;

	@FindBy(how = How.XPATH, using = "//div[@class='test-id__field-label-container slds-form-element__label no-utility-icon']//child::span[text()='Account']//parent::div//following-sibling::div//child::span//child::slot//child::force-lookup//child::div//child::records-hoverable-link//child::div//child::a//child::slot//child::slot//child::span")
	public WebElement AccountNameOnInvoicePage;

	@FindBy(how = How.XPATH, using = "//div[@class='test-id__field-label-container slds-form-element__label no-utility-icon']//child::span[text()='SAP Account']//parent::div//following-sibling::div//child::span//child::slot//child::force-lookup//child::div//child::records-hoverable-link//child::div//child::a//child::slot//child::slot//child::span")
	public WebElement SAPAccountNameOnInvoicePage;

	@FindBy(how = How.XPATH, using = "//div[@class='test-id__field-label-container slds-form-element__label no-utility-icon']//child::span[text()='Order']//parent::div//following-sibling::div//child::span//child::slot//child::force-lookup//child::div//child::records-hoverable-link//child::div//child::a//child::slot//child::slot//child::span")
	public WebElement OrderNumberOnInvoicePage;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Value__c']")
	public WebElement PaymentValue;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Payment_Amount__c']")
	public WebElement PaymentAmount;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Payment_Request_Sent_Date__c']")
	public WebElement PaymentSentDate;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Payment_Made_Date__c']")
	public WebElement PaymentMadeDate;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Send_By_Date__c']")
	public WebElement PaymentSendByDate;

	@FindBy(how = How.XPATH, using = "//span[@title='Agreements & Invoices']")
	public WebElement AgreementAndInvoice;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'INV-')]")
	public WebElement InvoiveNumber;

	@FindBy(how = How.XPATH, using = "//div[@class='test-id__field-label-container slds-form-element__label no-utility-icon']//child::span[text()='Value']//parent::div//following-sibling::div//child::span//child::slot//child::lightning-formatted-text")
	public WebElement PaymentValueOnInvoicePage;

	@FindBy(how = How.XPATH, using = "//div[@class='test-id__field-label-container slds-form-element__label no-utility-icon']//child::span[text()='Payment Amount']//parent::div//following-sibling::div//child::span//child::slot//child::lightning-formatted-text")
	public WebElement PaymentAmountOnInvoicePage;

	@FindBy(how = How.XPATH, using = "//div[@class='test-id__field-label-container slds-form-element__label no-utility-icon']//child::span[text()='Sent Date']//parent::div//following-sibling::div//child::span//child::slot//child::lightning-formatted-text")
	public WebElement PaymentSentDateOnInvoicePage;

	@FindBy(how = How.XPATH, using = "//div[@class='test-id__field-label-container slds-form-element__label no-utility-icon']//child::span[text()='Payment Made Date']//parent::div//following-sibling::div//child::span//child::slot//child::lightning-formatted-text")
	public WebElement PaymentMadeDateOnInvoicePage;

	@FindBy(how = How.XPATH, using = "//div[@class='test-id__field-label-container slds-form-element__label no-utility-icon']//child::span[text()='Send By Date']//parent::div//following-sibling::div//child::span//child::slot//child::lightning-formatted-text")
	public WebElement PaymentSendByDateOnInvoicePage;

	@FindBy(how = How.XPATH, using = "(//div[@class='scroller']//child::ul//parent::li//parent::li//parent::li//child::a//child::span[text()='All Classic Service Opportunities'])[1]")
	public WebElement AllClassicServiceOpportunitiestab;

	@FindBy(how = How.XPATH, using = "(//div[@class='scroller']//child::ul//parent::li//parent::li//parent::li//child::a//child::span[text()='All Bespoke Opportunities'])[1]")
	public WebElement AllBespokeOpportunitiestab;

	@FindBy(how = How.XPATH, using = "(//a[contains(@href,'/related/Invoices__r/view')]//child::slot//child::span[contains(text(),'Invoices')])")
	public WebElement InvoiceLink;

	@FindBy(how = How.XPATH, using = "//div[@title='New']")
	public WebElement InvoiceNewButton;

	@FindBy(how = How.XPATH, using = "//span[text()='Deposit']")
	public WebElement DepositeInvoiceType;

	@FindBy(how = How.XPATH, using = "//div[@class='forceChangeRecordTypeFooter']//button//span[text()='Next']")
	public WebElement InvoiceNextButton;

	@FindBy(how = How.XPATH, using = "//label[text()='Opportunity']//following-sibling::div//button[@title='Clear Selection']")
	public WebElement ClearOpportunityFieldIcon;

	@FindBy(how = How.XPATH, using = "//div[@class='center-align-buttons']//button[@name='SaveEdit']")
	public WebElement SaveInvoiceButton;

	@FindBy(how = How.XPATH, using = "//li[text()='Either Opportunity or Order should be populated']")
	public WebElement ErrorMsgToSelectAnOpportunity;

	@FindBy(how = How.XPATH, using = "//div[@class='center-align-buttons']//button[@name='CancelEdit']")
	public WebElement CancelInvoiceButton;

	@FindBy(how = How.XPATH, using = "//label[text()='Order']//following-sibling::div//input")
	public WebElement OrderField;

	@FindBy(how = How.XPATH, using = "//span[text()='Status']//parent::div//following-sibling::div//lightning-formatted-text[text()='Planned Request']")
	public WebElement InvoicePlannedRequestStage;

	@FindBy(how = How.XPATH, using = "//ul[@class='slds-button-group-list']//button[@name='Edit']")
	public WebElement EditInvoiceButton;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Value__c']")
	public WebElement InvoicePaymentValueTextBox;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Payment_Amount__c']")
	public WebElement InvoicePaymentAmountTextBox;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Payment_Request_Sent_Date__c']")
	public WebElement InvoicePaymentSentDate;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Payment_Made_Date__c']")
	public WebElement InvoicePaymentMadeDate;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_SAP_Invoice_Number__c']")
	public WebElement InvoiceSapNumberTextBox;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_SAP_Invoice_Value__c']")
	public WebElement InvoiceSapValueTextBox;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_SAP_Invoice_Raised_Date__c']")
	public WebElement SapInvoiceRaisedDate;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_SAP_Invoice_Date__c']")
	public WebElement SapInvoicePaidDate;

	@FindBy(how = How.XPATH, using = "//span[text()='Status']//parent::div//following-sibling::div//lightning-formatted-text[text()='SAP Invoice Paid']")
	public WebElement InvoiceSAPInvoicePaidStage;

	@FindBy(how = How.XPATH, using = "//label[text()='Opportunity']//following-sibling::div[2]//input")
	public WebElement MultipleOpportunityField;

	@FindBy(how = How.XPATH, using = "//span[text()='Classic Service']")
	public WebElement SVOClassicServiceOpportunity;

	@FindBy(how = How.XPATH, using = "//span[@title='Retailed']")
	public WebElement ClosedWonStage;

	@FindBy(how = How.XPATH, using = "//span[contains(@title,'Defender')]")
	public WebElement VehicleNameSearch;

	public String VehicleSelection(String Value) {
		return "//a[text() = '" + Value + "']";
	}

	@FindBy(how = How.XPATH, using = "//div[@class='test-id__field-label-container slds-form-element__label']//child::span[text()='Opportunity']//parent::div//following-sibling::div//child::span//child::div//child::a")
	public WebElement OpportunityNameOnOrderPage;

	@FindBy(how = How.XPATH, using = "//table[@class='slds-table forceRecordLayout slds-table--header-fixed slds-table--edit slds-table--bordered resizable-cols slds-table--resizable-cols uiVirtualDataTable']//child::tr[2]//child::th[@class='slds-cell-edit cellContainer']//a[starts-with(@title,'INV-')]")
	public WebElement SecondInvoice;

	@FindBy(how = How.XPATH, using = "//span[text()='Cancelled by Customer']")
	public WebElement ClosedLostStage;

	@FindBy(how = How.XPATH, using = "//span[text()='Edit SAP Account']")
	public WebElement EditSAPAccountBtn;

	@FindBy(how = How.XPATH, using = "//div//child::span[text()='SAP Account']")
	public WebElement SAPAccountfieldOnInvoicePage;
}
