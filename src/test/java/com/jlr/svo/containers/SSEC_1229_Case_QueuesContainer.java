package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;


public class SSEC_1229_Case_QueuesContainer {
	
	@FindBy(how = How.XPATH, using = "//span[text()='Case Owner']")
	public WebElement CaseOwnerTitle;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Parts & Technical']//parent::div")
	public WebElement PartandTechRecordType;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Vehicle Details']")
	public WebElement VehicleDet;
	
	@FindBy(how = How.XPATH, using = "//button[@title='Save' and @type='button']")
	public WebElement SaveBtn;
	
	@FindBy(how=How.XPATH, using="//span[text()='Case Information']")
	public WebElement CaseInfoText;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Customer Service']")
	public WebElement CustServiceBtn;
	
	@FindBy(how = How.XPATH, using = "//slot[text()='Parts and Technical Queue']//parent::span")
	public WebElement COPartsTechQueue;
	
	@FindBy(how = How.XPATH, using = "//slot[text()='Customer Service Queue']//parent::span")
	public WebElement COCustServQueue;
	
	@FindBy(how = How.XPATH, using = "//button[@title='Select a List View']")
	public WebElement CasesDD;
	
	@FindBy(how = How.XPATH, using = "(//span[text()='Customer Service - All Cases']//parent::a)[1]")
	public WebElement CustServQueu;
	
	@FindBy(how=How.XPATH, using="//span[text()='Recently Viewed']//parent::h1")
	public WebElement RecViewedCases;
	
	@FindBy(how = How.XPATH, using = "(//span[text()='Parts & Technical - All Cases']//parent::a)[1]")
	public WebElement PartsTechQueue;
	
	@FindBy(how = How.XPATH, using = "//button[@title='Change Owner']")
	public WebElement EditChangeOwner;
	
	@FindBy(how = How.XPATH, using = "//a[@aria-label= 'Enter new owner name—Current Selection: Users, Pick an object']")
	public WebElement CaseOwnerDD;
	
	@FindBy(how = How.XPATH, using = "//span[@title='Queues']")
	public WebElement QueuesDD;
	
	
	
	@FindBy(how = How.XPATH, using = "//button[@value='change owner']")
	public WebElement ChangeOwnerBtn;
	
	@FindBy(how = How.XPATH, using = "//slot[text()='Parts and Technical Queue']//parent::span")
	public WebElement CaseOwnerPartsTechQueue;
	
	@FindBy(how = How.XPATH, using = "//button[@value='cancel']")
	public WebElement CancelChangeOwner;
	
	

}
