package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SSEC_2330_AdditionalVehiclesPricingContainer {

	@FindBy(how = How.XPATH, using = "(//a[@data-refid='recordId'])[1]")
	public WebElement OpportunityRec;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Total Retail Price']")
	public WebElement TotalRetailPriceText;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Total List Price']")
	public WebElement TotalListPriceText;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Total Cost Price (£)']")
	public WebElement TotalCostPriceText;
	
	@FindBy(how = How.XPATH, using = "//button[@title='Edit Retail Price']")
	public WebElement EditRetailPriceBtn;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Retail Price']")
	public WebElement RetailPriceLabel;
	
	@FindBy(how = How.XPATH, using = "//button[text()='Save']")
	public WebElement SaveOpportunityBtn;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Mark Stage as Complete']")
	public WebElement MarkStageAsCompleteBtn;
	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
}
