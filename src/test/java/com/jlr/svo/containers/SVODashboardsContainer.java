package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SVODashboardsContainer {

	@FindBy(how = How.XPATH, using = "//a[@title='Dashboards']")
	public WebElement DashboardsTab;

	@FindBy(how = How.XPATH, using = "//one-app-nav-bar-menu-button[@class='slds-grid slds-grid_vertical-stretch more-button slds-dropdown-trigger slds-dropdown-trigger--click']//child::a//child::span[text()='More']")
	public WebElement MoreButton;

	@FindBy(how = How.XPATH, using = "//a[@href='/lightning/o/Dashboard/home']//child::span//child::span[text()='Dashboards']")
	public WebElement DashBoardDropDownTab;

	@FindBy(how = How.XPATH, using = "//a[@title='All Dashboards']")
	public WebElement AllDashBoardsfolder;

	@FindBy(how = How.XPATH, using = "//table[@class='slds-table slds-table_header-fixed slds-table_bordered slds-table_edit slds-table_resizable-cols']//child::tbody//child::tr[1]//child::td[@data-label='Created By']//child::lightning-primitive-cell-factory//child::span//child::div//child::lightning-formatted-url//child::a[@target='_self']")
	public WebElement DashBoardCreatedByUser;

	@FindBy(how = How.XPATH, using = "//a[@title='Classic Consent Dashboard']")
	public WebElement ClassicConsentDashboardLink;

	@FindBy(how = How.XPATH, using = "//div[@class='slds-page-header__meta-text']//child::div//child::span//child::span[@class='runningUserName']")
	public WebElement DashBoardViewingAsUser;

	@FindBy(how = How.XPATH, using = "//div[@title='Consented to Email Marketing']")
	public WebElement ConsentedToEmailDetails;

	@FindBy(how = How.XPATH, using = "//div[@title='Consented to Phone Marketing']")
	public WebElement ConsentedToPhoneMarketingDetails;

	@FindBy(how = How.XPATH, using = "//div[@title='Consented to SMS Marketing']")
	public WebElement ConsentedToSMSMarketingDetails;

	@FindBy(how = How.XPATH, using = "//div[@title='Consented to Post Marketing']")
	public WebElement ConsentedToPostMarketingDetails;

	@FindBy(how = How.XPATH, using = "//div[@class='slds-page-header__meta-text']//child::div//child::span//child::span[@class='lastRefreshDate']")
	public WebElement LastModifiedData;

	@FindBy(how = How.XPATH, using = "//div[text()='New Dashboard']")
	public WebElement NewDashboardBtn;

	@FindBy(how = How.XPATH, using = "//input[@id='dashboardNameInput']")
	public WebElement DashboardNameBox;

	@FindBy(how = How.XPATH, using = "//input[@id='dashboardDescriptionInput']")
	public WebElement DashboardDescriptionBox;

	@FindBy(how = How.XPATH, using = "//button[@id='submitBtn']")
	public WebElement CreateBtn;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search folders...']")
	public WebElement FolderSearchBox;

	@FindBy(how = How.XPATH, using = "//span[@class='highlightSearchText']")
	public WebElement SelectFolder;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Dashboard Properties']")
	public WebElement EditDashboardPropertiesBtn;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-button_icon slds-pill__remove slds-button_icon-bare']")
	public WebElement CancelAdminUser;

	@FindBy(how = How.XPATH, using = "//button[text()='Done']")
	public WebElement DoneBtn;

	@FindBy(how = How.XPATH, using = "//button[@id='modalBtn2']")
	public WebElement SaveDashboard;

	@FindBy(how = How.XPATH, using = "//span[text()='More Dashboard Actions']")
	public WebElement DropdownBtn;

	@FindBy(how = How.XPATH, using = "//span[text()='Download']")
	public WebElement DashboardDownloadBtn;

	@FindBy(how = How.XPATH, using = "//span[text()='Save As']")
	public WebElement DashboardSaveAsBtn;

	@FindBy(how = How.XPATH, using = "//span[text()='New Dashboard']")
	public WebElement CreateNewDashboardFromDashboard;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Copy of')]")
	public WebElement CopyOfDashboard;

	@FindBy(how = How.XPATH, using = "//a[text()='Add Username']")
	public WebElement AddUsername;

	@FindBy(how = How.XPATH, using = "//button[text()='Edit']")
	public WebElement EditBtn;

	@FindBy(how = How.XPATH, using = "//span[text()='Another person']")
	public WebElement AnotherPersonRadioBtn;

	@FindBy(how = How.XPATH, using = "//iframe[@title='dashboard']")
	public WebElement Dashboardframe;

	@FindBy(how = How.XPATH, using = "//div[@class='windowViewMode-normal oneContent active lafPageHost']//child::div[@class='standalone dashboardTopLevelContainer desktopDashboardsDashboard']//child::div[@class='dashboardContainer']//child::iframe[@title='dashboard']")
	public WebElement CopyDashboardframe;

	@FindBy(how = How.XPATH, using = "//div[@class='modal-footer slds-modal__footer']//child::button[@title='Select Folder']")
	public WebElement SelectFolderBtn1;

	@FindBy(how = How.XPATH, using = "//button[text()='Select Folder']")
	public WebElement SelectFolderBtn;

	@FindBy(how = How.XPATH, using = "//footer[@class='slds-modal__footer']//child::button[@id='submitBtn']")
	public WebElement SaveBtn;

	@FindBy(how = How.XPATH, using = "//span[@class='slds-icon_container slds-icon-standard-user']//following-sibling::span[@class='dataLookupItemLabel truncation userItemLabel']")
	public WebElement SelectUsername;

	@FindBy(how = How.XPATH, using = "//div[@class='slds-form-element__control slds-input-has-icon slds-input-has-icon_right']//child::input[@class='slds-input']")
	public WebElement AnotherPersonSearchBox;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'New')]")
	public WebElement NewDashboardName;

	@FindBy(how = How.XPATH, using = "//input[@class='search-text-field slds-input input uiInput uiInputText uiInput--default uiInput--input']")
	public WebElement DashboardSearchBox;

	@FindBy(how = How.XPATH, using = "//table[@class='slds-table slds-table_header-fixed slds-table_bordered slds-table_edit slds-table_resizable-cols']//child::tBody//child::tr[1]//child::th//child::lightning-primitive-cell-factory//child::span//child::div//child::lightning-formatted-url//child::a//child::span[@class='highlightSearchText']")
	public WebElement SelectDashboard;

	@FindBy(how = How.XPATH, using = "//button[text()='Save']")
	public WebElement SaveDashboardBtn;

}
