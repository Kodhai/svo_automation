package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SVO_RestrictedPartyScreeningContainer {

	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Client')]//following-sibling::div//child::lightning-base-combobox//child::div//child::div//child::input[@placeholder='Search Accounts...']")
	public WebElement SVOBespokeClientSearch;

	@FindBy(how = How.XPATH, using = "//span[text()='SVO Private Office']")
	public WebElement PrivateOfficeOpportunityRecType;

	@FindBy(how = How.XPATH, using = "//span[text()='Private Office Closed']")
	public WebElement PrivateOfficeclosedOpportunityRecType;

	@FindBy(how = How.XPATH, using = "//table[contains(@class,'uiVirtualDataTable')]//child::tbody//child::tr//child::td[@class='slds-cell-edit cellContainer']//child::span[@class='slds-grid slds-grid--align-spread']//child::a[contains(text(),'SVO-')]")
	public WebElement FirstQuoteFromOpportunity;

	@FindBy(how = How.XPATH, using = "//button[@title='Save']")
	public WebElement SaveCommonOrderNumberButton;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Classic Service')]")
	public WebElement ClassicServiceRecordType;

	@FindBy(how = How.XPATH, using = "//span[@class='toastMessage forceActionsText']")
	public WebElement RestrictedPartyScreeningErrorMsg;

	@FindBy(how = How.XPATH, using = "//span[text()='Mark Status as Complete']//parent::button")
	public WebElement QuoteMarkStageAsComplete;

	@FindBy(how = How.XPATH, using = "//input[@name='StageName']")
	public WebElement SelectStage;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Lost_Reason__c']")
	public WebElement LostReason;

	@FindBy(how = How.XPATH, using = "//span[@title='Closed Won']")
	public WebElement SelectColsedWonStage;

	@FindBy(how = How.XPATH, using = "//button[text()='Done']")
	public WebElement StageDone;

	@FindBy(how = How.XPATH, using = "//a[contains(@title,'VIP-')]")
	public WebElement FirstPrivateOfficeQuote;

	@FindBy(how = How.XPATH, using = "//lightning-formatted-text[text()='Closed Won']")
	public WebElement VerifyClosedonStage;

	@FindBy(how = How.XPATH, using = "(//label[contains(text(),'Restricted Party Screening')]//following-sibling::div//child::lightning-base-combobox//child::div//child::div)[1]")
	public WebElement RestrictedPartyScreening;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Restricted Party Screening']")
	public WebElement RestrictedPartyScreeningEditBtn;

	@FindBy(how = How.XPATH, using = "//span[text()='Check cleared']")
	public WebElement SelectCheckCleared;

	public String ValueSelection(String Checkcleared) {
		return "//span[text() = '" + Checkcleared + "']";
	}
	
	public String OriginatingDivisionSelection(String Value) {
		return "//span[@class='slds-media__body']//span[@title = '" + Value + "']";
	}

	@FindBy(how = How.XPATH, using = "//span[text() = 'Customer Unknow/Check not Applicable']")
	public WebElement CustomerUnknowChecknotApplicable;

	@FindBy(how = How.XPATH, using = "//button[@name='SaveEdit']")
	public WebElement RestrictedPartyScreeningSaveBtn;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Originating Division']")
	public WebElement OriginationDivisionEditBtn;

	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Originating Division')]//following-sibling::div//child::lightning-base-combobox//child::div//child::div//child::input[@aria-haspopup='listbox']")
	public WebElement OriginationDivisionDropDownList;

	@FindBy(how = How.XPATH, using = "//lightning-formatted-text[text()='Closed Won']")
	public WebElement VerifyClosedoneStage;

	@FindBy(how = How.XPATH, using = "//lightning-formatted-text[text()='Check cleared']")
	public WebElement VerifyRestrictedPartyScreeningfield;

	@FindBy(how = How.XPATH, using = "//lightning-formatted-text[text()='Restricted']")
	public WebElement VerifyRestrictedPartyScreeningfieldAsRestricted;

	@FindBy(how = How.XPATH, using = "//button[@name='SaveEdit']")
	public WebElement SaveOpportunityDetailsBtn;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Restricted Party Screening: You cannot start contracting without Restricted Party Screening being Cleared')]")
	public WebElement VerifyRestrictedErrormessage;

	@FindBy(how = How.XPATH, using = "//span[text()='Restricted Party screening: You cannot contracting without Restricted Party Screening being cleared']")
	public WebElement VerifyCheckInProgressErrormessage;

	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Originating Division')]//following-sibling::div//child::lightning-base-combobox//child::div//child::div")
	public WebElement SVOOriginatingDivission;

	@FindBy(how = How.XPATH, using = "//span[@class='itemLabel slds-truncate slds-show--inline-block slds-m-left--xx-small']")
	public WebElement RetailerContactSearch;

	public String RetailserContactSelection(String retailerContact) {
		return "//a[@title = '" + retailerContact + "']";
	}

	@FindBy(how = How.XPATH, using = "//div[@class='footer active']//child::span[text()='Save']")
	public WebElement SaveCommonOrderNumber1;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Common Order Number']")
	public WebElement CommonOrderNumber1;
}
