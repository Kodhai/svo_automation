package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SSEC_2294_ClassicPartsStatusUpdatefor_New_Customer_Container {

	@FindBy(how = How.XPATH, using = "//span[text()='Status']")
	public static WebElement Status;

	@FindBy(how = How.XPATH, using = "//lightning-formatted-text[text()='New']")
	public static WebElement New;

	@FindBy(how = How.XPATH, using = "//span[text()='Edit Status']")
	public static WebElement Editstatus;

	@FindBy(how = How.XPATH, using = "//label[text()='Status']//parent::lightning-combobox//child::div")
	public static WebElement Statusdropdown;

	public static String RecordType(String Value) {
		return "//span[text() = '" + Value + "']";
	}

	@FindBy(how = How.XPATH, using = "//span[text()='New']")
	public static WebElement SelectNew;

	@FindBy(how = How.XPATH, using = "//button[text()='Save']")
	public static WebElement Save;

	@FindBy(how = How.XPATH, using = "//lightning-formatted-text[text()='Open']")
	public static WebElement Open;

	@FindBy(how = How.XPATH, using = "//span[text()='Customer Service']")
	public static WebElement Customerservice;

	@FindBy(how = How.XPATH, using = "//span[text()='Web Email']")
	public static WebElement WebEmail;

	@FindBy(how = How.XPATH, using = "//a[text()='Inbox']")
	public WebElement inbox;

	@FindBy(how = How.XPATH, using = "(//tr[@role='row'])[1]")
	public WebElement firstrecord;

	@FindBy(how = How.XPATH, using = "//div[@id=':nx']")
	public WebElement Verifythemail;

	@FindBy(how = How.XPATH, using = "(//input[@type='text'])[1]")
	public WebElement firstname;

	@FindBy(how = How.XPATH, using = "(//input[@type='text'])[2]")
	public WebElement lastname;

	@FindBy(how = How.XPATH, using = "(//input[@type='text'])[3]")
	public WebElement Email;

	@FindBy(how = How.XPATH, using = "(//input[@type='text'])[4]")
	public WebElement Phone;

	@FindBy(how = How.XPATH, using = "(//input[@type='text'])[5]")
	public WebElement VINno;

	@FindBy(how = How.XPATH, using = "(//textarea[@name='description'])")
	public WebElement Description;

	@FindBy(how = How.XPATH, using = "//select[@name='recordType']")
	public WebElement selectanyqueries;

	@FindBy(how = How.XPATH, using = "//select[@title='Country']")
	public WebElement Country;

	@FindBy(how = How.XPATH, using = "//input[@type='submit']")
	public WebElement Submit;

	@FindBy(how = How.XPATH, using = "//button[@title='Select a List View']")
	public WebElement Selectalistview;

	@FindBy(how = How.XPATH, using = "//span[text()='Parts & Technical - All Cases']")
	public WebElement partstechnical;

	@FindBy(how = How.XPATH, using = "(//div[@class='slds-form-element slds-hint-parent test-id__output-root slds-form-element_readonly slds-form-element_stacked'])[1]")
	public WebElement Casenumbr;

	@FindBy(how = How.XPATH, using = "//a[@data-refid='recordId']")
	public WebElement Firstrecord;
}
