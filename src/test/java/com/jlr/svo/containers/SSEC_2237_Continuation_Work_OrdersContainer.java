package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SSEC_2237_Continuation_Work_OrdersContainer {

	@FindBy(how = How.XPATH, using = "//a[@title='Opportunities']")
	public WebElement OpportunitiesLink;
	
	@FindBy(how = How.XPATH, using = "//div[@title='New']")
	public WebElement NewOpportunityBtn;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Classic Continuation']")
	public WebElement ClassicContinuationRadio;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Next']")
	public WebElement NextBtn;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Opportunity Name']")
	public WebElement OpportunityNameLabel;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Close Date']")
	public WebElement CloseDateLabel;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Stage']")
	public WebElement StageLabel;
	
	public String Stage(String value) {
		return "//span[text()='" + value + "']";
	}
	
	@FindBy(how = How.XPATH, using = "//label[text()='Region']")
	public WebElement RegionLabel;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Market']")
	public WebElement MarketLabel;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Account Name']")
	public WebElement AccountNameLabel;
	
	public String AccountName(String value) {
		return "//lightning-base-combobox-formatted-text[@title='" + value + "']";
	}
	
	@FindBy(how = How.XPATH, using = "//label[text()='Preferred Contact']")
	public WebElement PreferredContactLabel;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Programme']")
	public WebElement ProgrammeLabel;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Retail Price']")
	public WebElement RetailPriceLabel;
	
	@FindBy(how = How.XPATH, using = "//label[text()='VAT Qualifying']")
	public WebElement VATQualifyingLabel;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Source']")
	public WebElement SourceLabel;
	
	@FindBy(how = How.XPATH, using = "//button[text()='Save']")
	public WebElement SaveOpportunityBtn;
	
	@FindBy(how = How.XPATH, using = "//button[@title='Edit Vehicle']")
	public WebElement EditVehicleVIN;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Vehicle']")
	public WebElement VehicleLabel;
	
	@FindBy(how = How.XPATH, using = "//button[@name='SaveEdit']")
	public WebElement SaveEditOpportunity;
	
	@FindBy(how = How.XPATH, using = "//button[@title='Edit Hand of Drive']")
	public WebElement EditHandOfDrive;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Hand of Drive']")
	public WebElement HandOfDriveLabel;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Speedometer']")
	public WebElement SpeedometerLabel;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Build Colour']")
	public WebElement BuildColorLabel;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Build Interior Colour']")
	public WebElement BuildInteriorColorLabel;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Build Colour Shade']")
	public WebElement BuildColorShadeLabel;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Additional Spec Details']")
	public WebElement AdditionalSpecDetailsLabel;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Body Style']")
	public WebElement BodyStyleLabel;
	
	@FindBy(how = How.XPATH, using = "//button[@title='Edit Body Style']")
	public WebElement EditBodyStyle;
	
	@FindBy(how = How.XPATH, using = "(//a[@title='90'])[1]")
	public WebElement BodyStyleLink;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Mark Stage as Complete']")
	public WebElement MarkStageCompleteBtn;
	
	@FindBy(how = How.XPATH, using = "//span[starts-with(text(), 'Orders (1)')]")
	public WebElement OrdersTab;
	
	@FindBy(how = How.XPATH, using = "//a[starts-with(text(), '000')]")
	public WebElement OrderRec;
	
	@FindBy(how = How.XPATH, using = "//a[starts-with(text(), 'WO-0000')]")
	public WebElement WorkOrderRec;
	
	@FindBy(how = How.XPATH, using = "//span[text()='VIN']")
	public WebElement VINTextWorkOrder;
	
	@FindBy(how = How.XPATH, using = "//span[text()='No items to display.']")
	public WebElement OrdersEmptyPage;
	
	@FindBy(how = How.XPATH, using = "//button[@title='Edit Vehicle']")
	public WebElement EditVehicleDonorSection;
	
	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Records...']")
	public WebElement SearchVehicleOpportunityDonor;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Vehicle Request Details']")
	public WebElement VehicleDetails;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Donor details']")
	public WebElement DonorDetails;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Mark Stage as Complete']")
	public WebElement MarkStageAsCompleteBtn;
	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
	
	
}
