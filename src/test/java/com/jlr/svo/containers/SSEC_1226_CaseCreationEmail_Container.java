package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SSEC_1226_CaseCreationEmail_Container {

	@FindBy(how = How.XPATH, using = "//a[@title='Cases']")
	public WebElement CasesTab;

	@FindBy(how = How.XPATH, using = "(//li//span[text()='Parts and Technical Queue'])[1]")
	public WebElement PartsAndTechnicalQueue;

	@FindBy(how = How.XPATH, using = "//span[@title='Date/Time Opened']")
	public WebElement DateOfCaseOpenField;

	@FindBy(how = How.XPATH, using = "//span[text()='Case Origin']//parent::div//following-sibling::div//span//slot//lightning-formatted-text")
	public WebElement CaseOrigin;

	@FindBy(how = How.XPATH, using = "(//li//span[text()='Customer Service - All Cases'])[1]")
	public WebElement CustomerServiceCases;

	@FindBy(how = How.XPATH, using = "(//div//span[@title='case-update@jlr-apps.com'])[1]")
	public WebElement caseoutlookEmail;

	@FindBy(how = How.XPATH, using = "//div[@class='PlainText']")
	public WebElement CaseNumber;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search this list...']")
	public WebElement SearchCaseTxtBox;

	@FindBy(how = How.XPATH, using = "//span[text()='Already have an account? Sign in']")
	public WebElement SignInLink;

	@FindBy(how = How.XPATH, using = "(//li//span[text()='Customer Service - All Cases'])[1]")
	public WebElement CustomerServiceQueue;

}
