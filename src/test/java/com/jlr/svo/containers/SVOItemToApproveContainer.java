package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SVOItemToApproveContainer {

	@FindBy(how = How.XPATH, using = "(//span[text()='Items to Approve'])")
	public WebElement ItemsToApproveSection;

	@FindBy(how = How.XPATH, using = "//span[text()='Design Brief Pending Approvals']")
	public WebElement SVODesignBriefPendingApprovals;

	@FindBy(how = How.XPATH, using = "//a[@href='/lightning/n/Design_Brief_Approval']")
	public WebElement ManageAllHyperLink;

	@FindBy(how = How.XPATH, using = "//span[text()='Home']")
	public WebElement Hometab;

	@FindBy(how = How.XPATH, using = "//span[text()='Next']")
	public WebElement QuoteNextBtn;

	@FindBy(how = How.XPATH, using = "//a[text()='Manage All']")
	public WebElement ManageAll;

	@FindBy(how = How.XPATH, using = "//span[@title='Approval History']")
	public WebElement ApprovalHistorySection;

	@FindBy(how = How.XPATH, using = "//ul[@class='branding-actions slds-button-group slds-m-left--xx-small small oneActionsRibbon forceActionsContainer']//child::li//child::a//child::div[@title='Reassign']")
	public WebElement DesignBriefReassignBtn;

	@FindBy(how = How.XPATH, using = "//input[@title='Search Users']")
	public WebElement DesignBriefReassignUserTextbox;

	@FindBy(how = How.XPATH, using = "//div[@class='undefined lookup__menu uiAbstractList uiAutocompleteList uiInput uiAutocomplete uiInput--default uiInput--lookup']//child::div[@class='searchButton itemContainer slds-lookup__item-action--label slds-text-link--reset slds-grid slds-grid--vertical-align-center slds-truncate forceSearchInputLookupDesktopActionItem lookup__header highlighted']//child::span[@class='itemLabel slds-truncate slds-show--inline-block slds-m-left--xx-small']")
	public WebElement DesignBriefReassignSearchbar;

	@FindBy(how = How.XPATH, using = "//div[@class='modal-footer slds-modal__footer']//child::div//child::button//child::span[text()='Reassign']")
	public WebElement DesignBriefApproverRequestReassignBtn;

	@FindBy(how = How.XPATH, using = "//table[@class='slds-table slds-table_header-fixed slds-table_bordered slds-table_edit slds-table_resizable-cols']//child::tbody//child::tr[1]/child::td//child::lightning-primitive-cell-checkbox//child::span//child::label//child::span[@class='slds-checkbox_faux']")
	public WebElement FirstDesignBriefApprovalRequestCheckbox;

	@FindBy(how = How.XPATH, using = "//table[@class='slds-table slds-table_header-fixed slds-table_bordered slds-table_edit slds-table_resizable-cols']//child::tbody//child::tr[2]/child::td//child::lightning-primitive-cell-checkbox//child::span//child::label//child::span[@class='slds-checkbox_faux']")
	public WebElement SecondDesignBriefApprovalRequestCheckbox;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search People...']")
	public WebElement DesignBriefReassignTextbox;

	@FindBy(how = How.XPATH, using = "//div[@class='DESKTOP uiModal open active']//child::div//child::div//child::button[@class='slds-button slds-button_brand']")
	public WebElement DesignBriefReassignPopupWindowBtn;

	@FindBy(how = How.XPATH, using = "//input[@name='Opportunity-search-input']")
	public WebElement OpportunitysearchBox;

	public String DesignBriefRequestApprovalreassignedto(String DesignBriefRequestreassignTo) {
		return "(//table[@class='slds-table forceRecordLayout slds-table--header-fixed slds-table--edit slds-table--bordered resizable-cols slds-table--resizable-cols uiVirtualDataTable']//child::tbody//child::tr//child::td//child::span//child::a[@title='"
				+ DesignBriefRequestreassignTo + "'])[1]";
	}
	public String AssignedTo(String AssignTo) {
		return "//a[@title='"+ AssignTo + "']";
	}

	@FindBy(how = How.XPATH, using = "//button[@name()='Reassign']")
	public WebElement BespokeDesignBriefReassignBtn;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search People...']")
	public WebElement BespokeDesignBriefReassignTextbox;

	@FindBy(how = How.XPATH, using = "//span[contains(@title,'Show All Results for')]")
	public WebElement DesignBriefUserSearchBtn;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'No results for')]")
	public WebElement noresultsmessage;

	@FindBy(how = How.XPATH, using = "//div[@class='DESKTOP uiModal open active']//child::div//child::button[text()='Cancel']")
	public WebElement ReassignWindowCancelBtn;

	@FindBy(how = How.XPATH, using = "//li[@role='presentation']//child::lightning-base-combobox-item//child::span//child::span//child::lightning-base-combobox-formatted-text[@class='slds-truncate']")
	public WebElement DesignBriefRequestApprover;

	@FindBy(how = How.XPATH, using = "//li[@class='lookup__item  default uiAutocompleteOption forceSearchInputLookupDesktopOption']//child::a//child::div//child::div[@class='primaryLabel slds-truncate slds-lookup__result-text']")
	public WebElement BespokeDesignBriefApproverUser;

	@FindBy(how = How.XPATH, using = "(//div[@class='name']//child::div//child::a[@target='_blank'])[1]")
	public WebElement BespokeDesignBriefApproverUsername;

	@FindBy(how = How.XPATH, using = "(//table[@class='slds-table slds-table_header-fixed slds-table_bordered slds-table_edit slds-table_resizable-cols']//child::tbody//child::tr//child::td//child::lightning-primitive-cell-checkbox//child::span[@class='slds-checkbox']//child::label[@class='slds-checkbox__label']//child::span[@class='slds-checkbox_faux'])[1]")
	public WebElement BespokeOpsDesignBriefApprovalRequestCheckbox;

	@FindBy(how = How.XPATH, using = "//div[@title='Test ESign']")
	public WebElement QuoteRetailerContact;
	
	@FindBy(how = How.XPATH, using = "//a[@title='Test ESign']")
	public WebElement QuoteRetailerContacts;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'in Contacts')]")
	public WebElement QuoteRetailerContactSearch;

	@FindBy(how = How.XPATH, using = "//div[@class='slds-listbox slds-listbox_vertical slds-dropdown slds-dropdown_fluid slds-dropdown_left slds-dropdown_length-with-icon-7']//child::ul//child::li//child::lightning-base-combobox-item//child::span//child::span//child::lightning-base-combobox-formatted-text[@title='Test Esign']")
	public WebElement SVOBespokePreferredContactname;

	@FindBy(how = How.XPATH, using = "//span[@class='triggerLinkText selectedListView slds-page-header__title slds-truncate slds-p-right--xx-small uiOutputText']")
	public WebElement SelectOpportunityListView;

	@FindBy(how = How.XPATH, using = "//div[@class='scroller']//child::ul//parent::li//parent::li//parent::li//child::a//child::span[text()='All Opportunities']")
	public WebElement AllOpportunitiestab;
	
	@FindBy(how = How.XPATH, using = "(//div[@class='scroller']//child::ul//parent::li//parent::li//parent::li//child::a//child::span[text()='All Bespoke Opportunities'])[1]")
	public WebElement AllBespokeOpportunitiestab;

	@FindBy(how = How.XPATH, using = "//table[@class='slds-table forceRecordLayout slds-table--header-fixed slds-table--edit slds-table--bordered resizable-cols slds-table--resizable-cols uiVirtualDataTable']/tbody//preceding-sibling::tr//child::td//child::span//a[contains(@title,'SVO-')]")
	public WebElement DesignBriefQuotename;

	@FindBy(how = How.XPATH, using = "//lightning-icon[@class='slds-button__icon slds-global-header__icon slds-icon-utility-notification slds-icon_container forceIcon']")
	public WebElement DesignBriefRequestsNotificationbar;

	public String DesignBriefApprovalrequestnotification(String DesignBriefrequestnotification) {
		return "//span[contains(text(),'" + DesignBriefrequestnotification + "')]";
	}

	@FindBy(how = How.XPATH, using = "//button[text()='×']")
	public WebElement CloseDesignBriefRequestsNotificationBar;

	@FindBy(how = How.XPATH, using = "//button[@name='Reject']")
	public WebElement DesignBriefrRequestRejectBtn;

	@FindBy(how = How.XPATH, using = "//button[@name='Approve']")
	public WebElement DesignBriefrRequestApproveBtn;

	@FindBy(how = How.XPATH, using = "//textarea[@class='slds-textarea']")
	public WebElement DesignBriefRequestRejectCommentBox;

	@FindBy(how = How.XPATH, using = "//div[@class='modal-footer slds-modal__footer']//child::button[text()='Reject']")
	public WebElement PopupWindowDesignBriefRequestRejectBtn;

	@FindBy(how = How.XPATH, using = "//div[@class='modal-footer slds-modal__footer']//child::button[text()='Approve']")
	public WebElement PopupWindowDesignBriefRequestApproveBtn;

	@FindBy(how = How.XPATH, using = "//span[text()='Rejected']")
	public WebElement DesignBriefRequestRejectionStatus;

	@FindBy(how = How.XPATH, using = "//following-sibling::th[@class='slds-cell-edit cellContainer']//following-sibling::td//child::span//child::span[text()='Approved']")
	public WebElement DesignBriefRequestApprovedStatus;

	@FindBy(how = How.XPATH, using = "//span[text()='Rejected']")
	public WebElement DesignBriefRequestname;

	@FindBy(how = How.XPATH, using = "//table[@class='slds-table forceRecordLayout slds-table--header-fixed slds-table--edit slds-table--bordered resizable-cols slds-table--resizable-cols uiVirtualDataTable']//child::tbody//child::tr[1]//following-sibling::th//following-sibling::td//following-sibling::td//child::span//child::span[text()='Approved']")
	public WebElement DesignBriefRequestFirstApprovedStatus;

	@FindBy(how = How.XPATH, using = "//table[@class='slds-table forceRecordLayout slds-table--header-fixed slds-table--edit slds-table--bordered resizable-cols slds-table--resizable-cols uiVirtualDataTable']//child::tbody//child::tr[3]//following-sibling::th//following-sibling::td//following-sibling::td//child::span//child::span[text()='Approved']")
	public WebElement DesignBriefRequestSecondApprovedStatus;

	@FindBy(how = How.XPATH, using = "//table[@class='slds-table forceRecordLayout slds-table--header-fixed slds-table--edit slds-table--bordered resizable-cols slds-table--resizable-cols uiVirtualDataTable']//child::tbody//child::tr[5]//following-sibling::th//following-sibling::td//following-sibling::td//child::span//child::span[text()='Approved']")
	public WebElement DesignBriefRequestThirdApprovedStatus;

	@FindBy(how = How.XPATH, using = "//a[@title='Setup']//child::div//child::div[@class='slds-col slds-size_10-of-12']")
	public WebElement SetUpIcon;

	@FindBy(how = How.XPATH, using = "//td[@id='topButtonRow']//child::input[@title='Login']")
	public WebElement SetUpUserLoginBtn;

	@FindBy(how = How.XPATH, using = "//span[@class='mruName slds-text-body--regular slds-text-color--default slds-truncate slds-show']")
	public WebElement DesignBriefApprovalUser;

	@FindBy(how = How.XPATH, using = "//button[text()='Reject']")
	public WebElement BespokeDesignBriefrRequestRejectBtn;

	@FindBy(how = How.XPATH, using = "//a[@class='slds-th__action slds-text-link_reset slds-is-sorted slds-is-sorted_asc']//child::lightning-primitive-icon[@class='slds-icon_container']")
	public WebElement SortBySubmittedDate;
	
	@FindBy(how = How.XPATH, using = "//span[@title='Submitted Date']")
	public WebElement SubmittedDateList;

	@FindBy(how = How.XPATH, using = "//footer[@class='slds-modal__footer']//child::lightning-button//child::button[text()='Reject']")
	public WebElement BespokeRequestPopupWindowRejectBtn;

	@FindBy(how = How.XPATH, using = "//button[@title='Close Notifications']")
	public WebElement ClosesNotificationBar;

	@FindBy(how = How.XPATH, using = "//a[@href='/secur/logout.jsp']")
	public WebElement BespokeUserLogoutlink;

	@FindBy(how = How.XPATH, using = "//button[text()='Reassign']")
	public WebElement BespokeDesignBriefRequestReassignBtn;

	@FindBy(how = How.XPATH, using = "//button[text()='Approve']")
	public WebElement BespokeDesignBriefRequestApproveBtn;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search...']")
	public WebElement BespokeDesignBriefRequestReassignTextbox;

	@FindBy(how = How.XPATH, using = "//li[@class='slds-p-around_x-small']")
	public WebElement BespokeDesignBriefRequestApprover;
	
	@FindBy(how = How.XPATH, using = "//li[text()='Bespoke-uat-user']")
	public WebElement BespokeDesignBriefApprover;

	@FindBy(how = How.XPATH, using = "//footer[@class='slds-modal__footer']//child::lightning-button//child::button[text()='Reassign']")
	public WebElement BespokeDesignBriefReassignPopupWindowBtn;

	@FindBy(how = How.XPATH, using = "//footer[@class='slds-modal__footer']//child::lightning-button//child::button[text()='Cancel']")
	public WebElement BespokeDesignBriefCancelPopupWindowBtn;

	@FindBy(how = How.XPATH, using = "//footer[@class='slds-modal__footer']//child::lightning-button//child::button[text()='Approve']")
	public WebElement BespokeDesignBriefApprovePopupWindowBtn;

	@FindBy(how = How.XPATH, using = "//div[@class='searchButton itemContainer slds-lookup__item-action--label slds-text-link--reset slds-grid slds-grid--vertical-align-center slds-truncate forceSearchInputLookupDesktopActionItem lookup__header highlighted']//span[@class='itemLabel slds-truncate slds-show--inline-block slds-m-left--xx-small']")
	public WebElement BespokeDesignBriefUserSearchBtn;

	@FindBy(how = How.XPATH, using = "//footer[@class='slds-modal__footer']//button[text()='Cancel']")
	public WebElement BespokeReassignWindowCancelBtn;

	@FindBy(how = How.XPATH, using = "//div[@class='modal-footer slds-modal__footer']//span[text()='Cancel']")
	public WebElement DesignBriefCancelPopupWindowBtn;

	@FindBy(how = How.XPATH, using = "//ul[@class='pendingApprovalCardList']//child::li//preceding-sibling::li//preceding-sibling::li//preceding-sibling::li//preceding-sibling::li//div[@class='slds-grid slds-grid--align-spread slds-has-flexi-truncate']//child::h3[@class='slds-truncate']//a")
	public WebElement DesignBriefApprovalRequestLink;

	@FindBy(how = How.XPATH, using = "//div[@title='Approve']")
	public WebElement BespokeDesignBriefApproveBtn;

	@FindBy(how = How.XPATH, using = "//textarea[@class='inputTextArea cuf-messageTextArea textarea']")
	public WebElement DesignBriefRequestApprovetCommentBox;

	@FindBy(how = How.XPATH, using = "//div[@class='modal-footer slds-modal__footer']//span[text()='Approve']")
	public WebElement PopupDesignBriefRequestApproveBtn;

	@FindBy(how = How.XPATH, using = "//div[@class='outputLookupContainer forceOutputLookupWithPreview']//a[@target='_blank']")
	public WebElement DesignBriefnameLink;

	@FindBy(how = How.XPATH, using = "//div[@class='toastTitle slds-text-heading--small']")
	public WebElement ErrorMessageToselectAtleastOneUser;

	public String DesignBriefRequestApprovalLink(String value) {
		return "//table[@class='slds-table slds-table_header-fixed slds-table_bordered slds-table_edit slds-table_resizable-cols']//child::tbody//child::tr//child::th//child::lightning-primitive-cell-factory//child::span//child::div//child::lightning-formatted-url//child::a[text()='"
				+ value + "']";
	}

	@FindBy(how = How.XPATH, using = "//table[@class='slds-table slds-table_header-fixed slds-table_bordered slds-table_edit slds-table_resizable-cols']//child::tbody//child::tr[1]//child::td[4]//child::lightning-primitive-cell-factory//child::span//child::div//child::lightning-formatted-url//child::a[@target='_blank']")
	public WebElement Opportunityhyperlink;

	@FindBy(how = How.XPATH, using = "//a[@class='slds-truncate outputLookupLink slds-truncate outputLookupLink-0057a000008LE1rAAG-4630:0 forceOutputLookup']")
	public WebElement RequestSubmittedusername;

	@FindBy(how = How.XPATH, using = "//div[@class='slds-form-element__control slds-grid itemBody']//child::span//child::div//child::a[@class=' textUnderline outputLookupLink slds-truncate outputLookupLink-0067a00000L71UmAAJ-90:2819;a forceOutputLookup']")
	public WebElement ViewApprovalrequestdetails;

	@FindBy(how = How.XPATH, using = "//div[@class='toastTitle slds-text-heading--small']")
	public WebElement DesignBriefApprovalrequestLink;

	@FindBy(how = How.XPATH, using = "//span[text()='SVO Private Office']")
	public WebElement PrivateOfficeOpportunity;

	@FindBy(how = How.XPATH, using = "//button[text()='Manage All']")
	public WebElement ItemsToApproveManageAll;

	@FindBy(how = How.XPATH, using = "//span[@title='Related to']")
	public WebElement SVOItemtoapproveRelatedTocolumn;

	@FindBy(how = How.XPATH, using = "//span[@title='Type']")
	public WebElement SVOItemtoapproveTypecolumn;

	@FindBy(how = How.XPATH, using = "//span[@title='Submitted By']")
	public WebElement SVOItemtoapproveSubmittedBycolumn;

	@FindBy(how = How.XPATH, using = "//span[@title='Submitted Date']")
	public WebElement SVOItemtoapproveSubmittedDatecolumn;

	@FindBy(how = How.XPATH, using = "//span[@title='Derivative Code']")
	public WebElement SVOItemtoapproveDerivativeCodecolumn;

	@FindBy(how = How.XPATH, using = "//span[@title='Opportunity']")
	public WebElement SVOItemtoapproveOpportunitycolumn;

	@FindBy(how = How.XPATH, using = "//span[@title='Comment']")
	public WebElement SVOItemtoapproveCommentcolumn;

	@FindBy(how = How.XPATH, using = "//button[text()='Approve']")
	public WebElement ApproveButton;

	@FindBy(how = How.XPATH, using = "//button[text()='Reject']")
	public WebElement RejectButton;

	@FindBy(how = How.XPATH, using = "//button[text()='Reassign']")
	public WebElement ReassignButton;

	@FindBy(how = How.XPATH, using = "//table[@class='slds-table slds-table_header-fixed slds-table_bordered slds-table_edit slds-table_resizable-cols']//child::tbody//child::tr[1]//child::child::td//child::lightning-primitive-cell-checkbox//child::span//child::label//child::span[@class='slds-checkbox_faux']")
	public WebElement SelectfirstapprovalRequest;

	@FindBy(how = How.XPATH, using = "//table[@class='slds-table slds-table_header-fixed slds-table_bordered slds-table_edit slds-table_resizable-cols']//child::tbody//child::tr[2]//child::child::td//child::lightning-primitive-cell-checkbox//child::span//child::label//child::span[@class='slds-checkbox_faux']")
	public WebElement SelectsecondapprovalRequest;

	@FindBy(how = How.XPATH, using = "(//a[contains(@href,'related//child::Quotes//child::view')]//child::slot//child::span[contains(text(),'Quotes')])")
	public WebElement QuotesLink;

	@FindBy(how = How.XPATH, using = "//div[@class='slds-table_header-fixed_container slds-scrollable_x']//child::div//child::table[@class='slds-table slds-table_header-fixed slds-table_bordered slds-table_edit slds-table_resizable-cols']//child::tbody//child::tr//child::th[@data-label='Related To']//child::lightning-primitive-cell-factory[@data-label='Related To']//child::span//child::div//child::lightning-primitive-custom-cell//child::c-record-reference//child::a[contains(text(),'SVO-')]")
	public WebElement ApprovalrequestQuoteID;

	@FindBy(how = How.XPATH, using = "//slot[@slot='outputField']//child::force-lookup//child::div[@class='slds-grid']//child::force-hoverable-link//child::div[@class='slds-grid']//child::a//child::slot//child::slot//child::span[contains(text(),'SVO-')]")
	public WebElement DesignbriefQuoteID;

	@FindBy(how = How.XPATH, using = "//div[@class='listViewContent slds-table--header-fixed_container']//child::div[@class='uiScroller scroller-wrapper scroll-bidirectional native']//child::div//child::div[@class='slds-table--edit_container slds-is-relative  inlineEdit--disabled keyboardMode--active inlineEditGrid forceInlineEditGrid']//child::table[@class='slds-table forceRecordLayout slds-table--header-fixed slds-table--edit slds-table--bordered resizable-cols slds-table--resizable-cols uiVirtualDataTable']//child::tbody//child::tr//child::td[@class='slds-cell-edit cellContainer']//child::span//child::div//child::a//child::span[@class='slds-icon_container slds-icon-utility-down']")
	public WebElement DesignBriefDropdown;

	@FindBy(how = How.XPATH, using = "//a//child::div[@title='Delete']")
	public WebElement DesignBriefDelete;

	@FindBy(how = How.XPATH, using = "//span[@title='Approval History']")
	public WebElement DesignBriefApprovalHistory;

	@FindBy(how = How.XPATH, using = "//div[text()='Reassign']")
	public WebElement Reassignbutton;

	@FindBy(how = How.XPATH, using = "//input[@aria-autocomplete='list']")
	public WebElement DesignbriefReassignUsersearch;

	@FindBy(how = How.XPATH, using = "//span[text()='Reassign']")
	public WebElement DesignbriefReassignbutton;

	@FindBy(how = How.XPATH, using = "//a[text()='Add Username']")
	public WebElement Addusername;

	@FindBy(how = How.XPATH, using = "//span[text()='Select at least one approval request and try again.']")
	public WebElement ApprovalrequestErrormessege;

	@FindBy(how = How.XPATH, using = "//span[text()='SVO Private Office']")
	public WebElement SVOPrivateOfficeOpportunity;

	@FindBy(how = How.XPATH, using = "//span[text()='Private Office Closed']")
	public WebElement SVOPrivateOfficeclosedRecordtype;

	@FindBy(how = How.XPATH, using = "//span[text()='Next']")
	public WebElement SVOOpportunityNext;

	@FindBy(how = How.XPATH, using = "//span[text()='SVO Bespoke']")
	public WebElement SVOBespokeOpportunity;

	@FindBy(how = How.XPATH, using = "//span[text()='Bespoke Design Brief']")
	public WebElement SVODesignBriefRecordtype;

	@FindBy(how = How.XPATH, using = "//span[text()='Notifications']")
	public WebElement NotificationIcon;

	@FindBy(how = How.XPATH, using = "//span[text()='SO so-sys-adm-uat-user is requesting approval for design brief']")
	public WebElement NotificationStatus;

	@FindBy(how = How.XPATH, using = "//div[text()='Approve']")
	public WebElement SelectApprovebutton;

	@FindBy(how = How.XPATH, using = "//textarea[@class='inputTextArea cuf-messageTextArea textarea']")
	public WebElement ApprovalComment;

	@FindBy(how = How.XPATH, using = "//span[text()='Approve']")
	public WebElement ApprovebuttonOnApprovepage;

	@FindBy(how = How.XPATH, using = "//table[@class='slds-table forceRecordLayout slds-table--header-fixed slds-table--edit slds-table--bordered resizable-cols slds-table--resizable-cols uiVirtualDataTable']//child::tbody//child::tr//child::th//child::span//child::a[@class='slds-truncate outputLookupLink slds-truncate outputLookupLink-0067a00000L9GNvAAN-6970:0 forceOutputLookup']")
	public WebElement Recentlycreatedopportunity;

	@FindBy(how = How.XPATH, using = "//span[text()='Approval request for the design brief is approved']")
	public WebElement Approvednotificationstatus;

	@FindBy(how = How.XPATH, using = "//span[@class='test-id__field-value slds-form-element__static slds-grow word-break-ie11']//child::slot//child::slot//child::lightning-formatted-text[text()='Approved']")
	public WebElement ApprovedstatusonDesignbriefpage;

	@FindBy(how = How.XPATH, using = "//div[@class='outputLookupContainer forceOutputLookupWithPreview']//child::a[@class='textUnderline outputLookupLink slds-truncate outputLookupLink-04h7a000001IDeTAAW-12:4156;a forceOutputLookup']")
	public WebElement Fullbespokesteplink;

	@FindBy(how = How.XPATH, using = "//div[@class='slds-page-header__title slds-m-right--small slds-align-middle clip-text slds-line-clamp']//child::div//child::span[text()='Approved']")
	public WebElement Approvedstatusonprocessinstancesteppage;

	@FindBy(how = How.XPATH, using = "(//div[@class='slds-form-element slds-form-element_readonly slds-grow slds-hint-parent override--slds-form-element']//child::div[@class='slds-form-element__control slds-grid itemBody']//child::span//child::div//child::a[@data-refid='recordId'])[3]")
	public WebElement DesignBrieflinkQuoteOnProcessInstanceStepPage;

	@FindBy(how = How.XPATH, using = "//div[@class='outputLookupContainer forceOutputLookupWithPreview']//child::a[@class=' textUnderline outputLookupLink slds-truncate outputLookupLink-0067a00000L9GeGAAV-90:9571;a forceOutputLookup']")
	public WebElement PaintOnlySteplink;

	@FindBy(how = How.XPATH, using = "//footer[@class='slds-modal__footer']//child::lightning-button//child::button[text()='Cancel']")
	public WebElement CancelbuttonOnApprovepage;

	@FindBy(how = How.XPATH, using = "//div[@class='searchButton itemContainer slds-lookup__item-action--label slds-text-link--reset slds-grid slds-grid--vertical-align-center slds-truncate forceSearchInputLookupDesktopActionItem lookup__header highlighted']//child::span[@class='itemLabel slds-truncate slds-show--inline-block slds-m-left--xx-small']")
	public WebElement SelectRetailContactorOnQuote;

	@FindBy(how = How.XPATH, using = "//a[@class='action-link']")
	public WebElement LogoutFromSetup;

	@FindBy(how = How.XPATH, using = "//table[@class='slds-table forceRecordLayout slds-table--header-fixed slds-table--edit slds-table--bordered resizable-cols slds-table--resizable-cols uiVirtualDataTable']//child::tbody//child::tr[1]//child::th[1]//child::span[@class='slds-grid slds-grid--align-spread']")
	public WebElement StepLink;

	@FindBy(how = How.XPATH, using = "//div[@class='slds-page-header__title slds-m-right--small slds-align-middle clip-text slds-line-clamp']//child::div//child::span[text()='Pending']")
	public WebElement PendingApprovalstatusonprocessinstancesteppage;

	@FindBy(how = How.XPATH, using = "//span[@title='Status']")
	public WebElement SVODesignBriefStatus;

	@FindBy(how = How.XPATH, using = "//div[@class='dt-outer-container']/div/div[@class='slds-scrollable_y']/table[@class='slds-table slds-table_header-fixed slds-table_bordered slds-table_edit slds-table_resizable-cols']/tbody/tr[1]/th[1]/lightning-primitive-cell-factory/span[@class='slds-grid slds-grid_align-spread']/div[@class='slds-truncate']/lightning-formatted-url//a[contains(text(),'SVO-')]")
	public WebElement DesignBriefApprovalRequest;

	@FindBy(how = How.XPATH, using = "//a[@title='Home']")
	public WebElement backtoHome;
	
	@FindBy(how = How.XPATH, using = "//div[@class='toastTitle slds-text-heading--small']")
	public WebElement SelectAtleastOneUser;
	
	@FindBy(how = How.XPATH, using = "//table[@class='slds-table slds-table_header-fixed slds-table_bordered slds-table_edit slds-table_resizable-cols']//tbody//tr[1]//th[1]//lightning-primitive-cell-factory//span//div//lightning-formatted-url//a[contains(text(),'SVO-')]")
	public WebElement FirstRelatedToLink;

	@FindBy(how = How.XPATH, using = "//table[@class='slds-table slds-table_header-fixed slds-table_bordered slds-table_edit slds-table_resizable-cols']//child::tbody//child::tr[1]//child::td[@data-label='Opportunity']//child::lightning-primitive-cell-factory//child::span//child::div//child::lightning-formatted-url//child::a[contains(text(),'Test_SVO')]")
	public WebElement FirstOpportunityLink;

	@FindBy(how = How.XPATH, using = "//table[@class='slds-table slds-table_header-fixed slds-table_bordered slds-table_edit slds-table_resizable-cols']//child::tbody//child::tr[1]//child::td[@data-label='Submitted By']//child::lightning-primitive-cell-factory//child::span//child::div//child::lightning-formatted-url//child::a[contains(text(),'SO so')]")
	public WebElement FirstSubmittedByLink;

	@FindBy(how = How.XPATH, using = "//slot[@name='outputField']//child::lightning-formatted-text[contains(text(),'SVO-')]")
	public WebElement VerifyDesignBriefdetails;

	@FindBy(how = How.XPATH, using = "//div[@class='slds-grid']//child::a//child::slot//child::slot//child::span[contains(text(),'SO so-')]")
	public WebElement VerifyUserInformation;

	@FindBy(how = How.XPATH, using = "//slot[@name='primaryField']//child::lightning-formatted-text[contains(text(),'Test_SVO')]")
	public WebElement VerifyOpportunityName;

	@FindBy(how = How.XPATH, using = "//div[@class='primaryFieldAndActions truncate primaryField highlightsH1 slds-m-right--small']//child::span[contains(text(),'SO so-')]")
	public WebElement VerifySubmittedUser;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Preferred Contact']")
	public WebElement PreferredContact;

}
