package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SVOTestContainer
{
	@FindBy(how = How.XPATH, using = "//i[contains(@class,'ms-ohp-Icon--outlookLogo ms-ohp-Icon--outlookLogoFill')]")
	public WebElement outlookIcon;
	
	@FindBy(how = How.XPATH, using = "(//div[contains(@aria-label,'noreply@salesforce.com')])[1]")
	public WebElement outlookEmail;
	
	@FindBy(how = How.XPATH, using = "//div[@class='PlainText']")
	public WebElement outlookVerificationCode;
	
	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Sign in')]")
	public WebElement outlookSignin;
	
	@FindBy(how = How.NAME, using = "loginfmt")
	public WebElement outlookUsername;
	
	@FindBy(how = How.XPATH, using = "//input[@value='Next']")
	public WebElement outlookNext;
	
	@FindBy(how = How.XPATH, using = "(//a[contains(text(),'Employee')])[1]")
	public WebElement outlookEmp;
	
	@FindBy(how = How.ID, using = "username")
	public WebElement userNameTextBox;
	
	@FindBy(how = How.XPATH, using = "//input[@id='emc']")
	public WebElement vCodeTextBox;

	@FindBy(how = How.ID, using = "password")
	public WebElement passwordTextBox;
	
	@FindBy(how = How.ID, using = "Login")
	public WebElement loginBtn;
	
	@FindBy(how = How.ID, using = "emc")
	public WebElement verificationCode;
	
	@FindBy(how = How.ID, using = "save")
	public WebElement verifyBtn;

	@FindBy(how = How.XPATH, using = "(//span[@class='slds-truncate'])[1]")
	public WebElement SVOText;
	
	@FindBy(how = How.ID, using = "p")
	public WebElement mobileNumText;
	
	@FindBy(how = How.LINK_TEXT, using = "Remind Me Later")
	public WebElement remindMeLater;
	
	@FindBy(how = How.LINK_TEXT, using = "I Don't Want to Register My Phone")
	public WebElement notRegister;
	
	@FindBy(how = How.XPATH, using = "(//span[text()='Enquiries'])[1]")
	public WebElement EnquiryTAB;
	
	@FindBy(how = How.XPATH, using = "//*[@name='SO_Enquiry__c-search-input']")
	public WebElement EnquirySearchBox;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Select item 1']")
	public WebElement SelectItem1;
	
	@FindBy(how = How.XPATH, using = "//*[@class='lvmTooltipButton inlineEditTooltipButton']")
	public WebElement EditEnquiry;
	
	@FindBy(how = How.XPATH, using = "//div[contains(text(),'New')]")
	public WebElement newEnquiryBtn;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Next']")
	public WebElement nextRType;
	
	public String selectRecordType(String record) {
		return "//div[contains(text(),'"+record+"')]";
	}
	
	@FindBy(how = How.XPATH, using = "//div[@class='recordTypeName slds-grow slds-truncate']")
	public WebElement SVORecordType;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Enquiries Menu')]")
	public WebElement enquiryTab;
	
	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input'])[2]")
	public WebElement enquiryTitle;
	
	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[23]")
	public WebElement BespokesourceDD;
	
	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[2]")
	public WebElement POsourceDD;
	
	@FindBy(how = How.XPATH, using = "(//button[contains(text(),'View all dependencies')])[1]")
	public WebElement POViewDepend;
	
	@FindBy(how = How.XPATH, using = "//div[@class='emptyContentInner']")
	public WebElement displayText;
	
	public String SourceSelection(String value) {
		return "(//a[contains(text(),'"+value+"')])[1]";
	}
	
	public String ValueSelection(String value) {
		return "//span[@title='"+value+"']";
	}
	
	public String marketValueSelection(String market) {
		return "//a[@title='"+market+"']";
	}
	
	@FindBy(how = How.XPATH, using = "//input[@title='Search Campaigns']")
	public WebElement searchCampaign;
	
	@FindBy(how = How.XPATH, using = "(//a[contains(text(),'Details')])[3]")
	public WebElement detailsTab;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Manufacturing')]")
	public WebElement campaign;
	
	@FindBy(how = How.XPATH, using = "//div[contains(text(),'No results')]")
	public WebElement noResultCam;
	
	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-button_neutral']")
	public WebElement cancelBtn;
	
	@FindBy(how = How.XPATH, using = "(//a[@class='select'])[6]")
	public WebElement proOffering;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[10]")
	public WebElement POTypeSale;
	
	@FindBy(how = How.XPATH, using = "(//a[@class='select'])[1]")
	public WebElement enqStatus;
	
	@FindBy(how = How.XPATH, using = "(//a[@class='select'])[3]")
	public WebElement salesAreaDD;
	
	@FindBy(how = How.XPATH, using = "(//a[@class='select'])[7]")
	public WebElement originatingDiv;
	
	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Accounts...'][1]")
	public WebElement clientAccount;
	
	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[11]")
	public WebElement POclientAccount;
	
	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[4]")
	public WebElement CSclientAccount;
	
	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Contacts...']")
	public WebElement preferredCnt;
	
	@FindBy(how = How.XPATH, using = "(//input[@title='Search Contacts'])[1]")
	public WebElement preferredCntBS;
	
	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[11]")
	public WebElement POpreferredCnt;
	
	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[6]")
	public WebElement CSpreferredCnt;
	
	public String clientAccountSearch(String client) {
		return "(//a[@title='"+client+"'])[2]";
	}
	
	public String clientNameSearch(String client) {
		return "(//a[@title='"+client+"'])[1]";
	}
	
	public String retailerAccountSearch(String retailer) {
		return "//a[@title='"+retailer+"']";
	}
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'in Accounts')]")
	public WebElement cAccountSearch;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'in Contacts')]")
	public WebElement pCntSearch;
	
	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Show All Results for')])[2]")
	public WebElement CSpCntSearch;
	
	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'in Accounts')])[2]")
	public WebElement rAccountSearch;
	
	@FindBy(how = How.XPATH, using = "(//textarea[@class='slds-textarea'])[1]")
	public WebElement requestDetails;
	
	@FindBy(how = How.XPATH, using = "//input[@title='Search Vehicle Manufacturers']")
	public WebElement brand;
	
	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[7]")
	public WebElement CSbrand;
	
	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[11]")
	public WebElement PObrand;
	
	@FindBy(how = How.XPATH, using = "//input[@title='Search Vehicle Models']")
	public WebElement model;
	
	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[7]")
	public WebElement CSmodel;
	
	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[12]")
	public WebElement POmodel;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'in Vehicle Manufacturers')]")
	public WebElement brandSearch;
	
	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Show All Results for')])[3]")
	public WebElement CSbrandSearch;
	
	@FindBy(how = How.XPATH, using = "//input[@name='SO_Email_Address__c']")
	public WebElement emailId;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'in Vehicle Models')]")
	public WebElement modelSearch;
	
	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Show All Results for')])[4]")
	public WebElement CSmodelSearch;
	
	public String TextSearch(String text) {
		return "//div[@title='"+text+"']";
	}
	
	public String brandSearch(String brand) {
		return "//a[@title='"+brand+"']";
	}
	
	public String enquiryNum(String enquiry) {
		return "//a[@title='"+enquiry+"']";
	}
	
	@FindBy(how = How.XPATH, using = "(//input[@placeholder='Search Accounts...'])[2]")
	public WebElement retailer;
	
	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[11]")
	public WebElement POretailer;
	
	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[11]")
	public WebElement CSretailer;
	
	@FindBy(how = How.XPATH, using = "(//button[contains(text(),'Save')])[2]")
	public WebElement saveEnquiry;
	
	@FindBy(how = How.XPATH, using = "//a[@id='activityTab__item']")
	public WebElement activityTab;
	
	@FindBy(how = How.XPATH, using = "//button[@title='Create new...']")
	public WebElement newLogCall;
	
	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[2]")
	public WebElement subjectDD;
	
	@FindBy(how = How.XPATH, using = "//span[@title='Call']")
	public WebElement callLog;
	
	@FindBy(how = How.XPATH, using = "//textarea[@class='textarea textarea uiInput uiInputTextArea uiInput--default uiInput--textarea']")
	public WebElement commentText;
	
	@FindBy(how = How.XPATH, using = "//input[@title='Search Contacts']")
	public WebElement name;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(), 'in Contacts')]")
	public WebElement nameSearch;
	
	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Save')])[4]")
	public WebElement saveLog;
	
	@FindBy(how = How.XPATH, using = "//a[@id='detailTab__item']")
	public WebElement enqDetails;
	
	@FindBy(how = How.XPATH, using = "(//button[@class='test-id__inline-edit-trigger slds-shrink-none inline-edit-trigger slds-button slds-button_icon-bare'])[13]")
	public WebElement editEmail;
	
	@FindBy(how = How.XPATH, using = "//input[@name='SO_Email_Address__c']")
	public WebElement emailIdTextBox;	
	
	@FindBy(how = How.XPATH, using = "//a[@title='Discovery']")
	public WebElement discoveryTab;	
	
	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Save')]")
	public WebElement saveEmailBtn;
	
	@FindBy(how = How.XPATH, using = "//a[@class='emailuiFormattedEmail']")
	public WebElement afterEmail;
	
	@FindBy(how = How.XPATH, using = "(//*[@class='slds-button slds-button--icon-x-small slds-button--icon-border-filled'])[1]")
	public WebElement ShowMoreOptions;
	
	@FindBy(how = How.XPATH, using = "(//*[@class='slds-grid slds-grid--align-spread'])[3]")
	public WebElement ClickOnEnquiryTitle;
	
	public String selectEnquiry(String arg1) {
	        return "//a[contains(text(),'"+arg1+"')]";
	    }
	public String TabHeader(String arg1) {
	        return "(//span[text()='"+arg1+"'])[1]";
	   //     (//span[text()='Discovery'])[2]
	    }
	@FindBy(how = How.XPATH, using = "//*[@class='uiInput uiInputEmail uiInput--default uiInput--input']")
	public WebElement Email;
	
	@FindBy(how = How.XPATH, using = "(//span[text()='Cancel'])[3]")
	public WebElement Cancel;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Close this window')]")
	public WebElement enqClose;
	
	@FindBy(how = How.XPATH, using = "//input[@name='SO_Closed_State__c']")
	public WebElement closedState;
	
	@FindBy(how = How.XPATH, using = "//span[@title='Qualified - Opportunity']")
	public WebElement selectClosedState;
	
	@FindBy(how = How.XPATH, using = "//div[@class='toastContent slds-notify__content']")
	public WebElement CSCloseStateError;
	
	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Done')]")
	public WebElement doneClosedState;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Mark as Current Enquiry Status']")
	public WebElement MarkCurrentEnquiry;
	
	@FindBy(how = How.XPATH, using = "(//span[text()='Mark Enquiry Status as Complete'])")
	public WebElement MarkEnquiryComplete;
	
	@FindBy(how = How.XPATH, using = "(//*[@class='fieldComponent slds-text-body--regular slds-show_inline-block slds-truncate'])[4]")
	public WebElement EnquiryStatus;
	
	@FindBy(how = How.XPATH, using = "(//*[text()='Done'])")
	public WebElement Done;
	
	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Create')]")
	public WebElement CraeteLog;
	
	@FindBy(how = How.XPATH, using = "(//*[@class='lastName compoundBorderBottom form-element__row input'])")
	public WebElement LastName;
	
	@FindBy(how = How.XPATH, using = "(//span[text()='Save'])[3]")
	public WebElement Save;
	
	@FindBy(how = How.XPATH, using = "(//*[@class='slds-form__item slds-no-space'])[11]")
	public WebElement EmailAfterSave;
	
	@FindBy(how = How.XPATH, using = "//*[@class='icon noicon']")
	public WebElement icon_image;
	
	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Log Out')]")
	public WebElement Logout;
	
	@FindBy(how = How.XPATH, using = "(//span[text()='Save'])[3]")
	public WebElement SaveContact;
	
	@FindBy(how = How.XPATH, using = "//*[text()='Log a Call']")
	public WebElement LogCall;
	
	@FindBy(how = How.XPATH, using = "(//span[text()='Create new...'])")
	public WebElement CreateNEWLog;
	
	@FindBy(how = How.XPATH, using = "(//span[text()='Add'])")
	public WebElement AddNEWLog;
	
	@FindBy(how = How.XPATH, using = "(//*[text()='Activity'])")
	public WebElement Activity;
	
	@FindBy(how = How.XPATH, using = "(//*[@class='slds-input slds-combobox__input'])[2]")
	public WebElement Subect;
	
	@FindBy(how = How.XPATH, using = "(//span[text()='Save'])[4]")
	public WebElement SaveLog;
	
	@FindBy(how = How.XPATH, using = "//div[@class='slds-align-middle slds-hyphenate']")
	public WebElement discoveryError;
	
	@FindBy(how = How.XPATH, using = "(//lightning-formatted-text[contains(text(),'New')])[1]")
	public WebElement enquiryStatus;
	
	@FindBy(how = How.XPATH, using = "//button[@title='Close']")
	public WebElement errorClose;
	
	@FindBy(how = How.XPATH, using = "//div[@class='toastContent slds-notify__content']")
	public WebElement Error;
	
	@FindBy(how = How.XPATH, using = "(//span[@class='slds-truncate'])[20]")
	public WebElement status;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'error')]")
	public WebElement ErrorsinPage ;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Classic Sales')]")
	public WebElement classicSales ;
	
	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[2]")
	public WebElement CSProOfferDD ;
	
	@FindBy(how = How.XPATH, using = "//input[@name='SO_Region__c']")
	public WebElement CSRegionDD ;
	
	@FindBy(how = How.XPATH, using = "//input[@name='SO_Sales_Area__c']")
	public WebElement CSSalesAreaDD ;
	
	@FindBy(how = How.XPATH, using = "//input[@name='SO_Market__c']")
	public WebElement CSMarketDD ;
	
	@FindBy(how = How.XPATH, using = "//li[contains(text(),'Complete this field.')]")
	public WebElement enqTitleError ;
	
	@FindBy(how = How.XPATH, using = "//button[@title='Close this window']")
	public WebElement enqCloseBtn ;
	
	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[8]")
	public WebElement CSEnqSource ;
	
	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[9]")
	public WebElement CSOriDiv ;
	
	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Show All Results for')])[5]")
	public WebElement CSACSrch ;
	
	@FindBy(how = How.XPATH, using = "(//button[contains(text(),'Edit')])[1]")
    public WebElement editbtn;
	
	@FindBy(how = How.XPATH, using = "(//ul[@class='orderedList'])[3]")
    public WebElement Precontact;
	
	public String EditValueSelection(String value) {
		return "(//a[contains(text(),'"+value+"')])[2]";
	}
	
	@FindBy(how = How.XPATH, using = "//a[@id='activityTab__item']")
	public WebElement CSactivityTab;
	
	@FindBy(how = How.XPATH, using = "//a[@title='Discovery']")
	public WebElement CSdiscoveryTab;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Mark Enquiry Status as Complete']")
	public WebElement CSMarkEnquiryComplete;
	
	@FindBy(how = How.XPATH, using = "//span[@title='Qualified - KMI']")
	public WebElement CSselectClosedState;
	
	public String selectClosedState(String value)
	{
		return "//span[@title='"+value+"']";
	}
	
	@FindBy(how = How.XPATH, using = "(//a[@class='select'])[8]")
    public WebElement Closedstate;
   
    @FindBy(how = How.XPATH, using = "(//a[@class='select'])[9]")
    public WebElement Closedreason;
    
    @FindBy(how = How.XPATH, using = "(//span[@class='title'])[2]")
    public WebElement newtask; 
    
    @FindBy(how = How.XPATH, using = "//span[contains(text(),'Specification & Quotation')]")
    public WebElement QuoteText;

    @FindBy(how = How.XPATH, using = "//a[@title='Test Private Office']")
    public WebElement OppTest1;

    @FindBy(how = How.XPATH, using = "//input[@class='inputDate input']")
    public WebElement duedate; 
    
    @FindBy(how = How.XPATH, using = "//button[contains(text(),'Today')]")
    public WebElement today; 
    
    @FindBy(how = How.XPATH, using = "(//input[@title='Search Contacts'])[2]")
    public WebElement Name;
    
    @FindBy(how = How.XPATH, using = "(//span[contains(text(),'Save')])[7]")
    public WebElement savetask;
    
    @FindBy(how = How.XPATH, using = "(//button[contains(text(),'View all dependencies')])[4]")
	public WebElement ViewallDependencies;
    
    @FindBy(how = How.XPATH, using = "(//button[contains(text(),'View all dependencies')])[3]")
	public WebElement CSViewallDependencies;

    @FindBy(how = How.XPATH, using = "//input[@name='SO_Region__c']")
	public WebElement regionDD;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Sales_Area__c']")
	public WebElement BespokesalesAreaDD;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Market__c']")
	public WebElement marketDD;

	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Apply')]")
	public WebElement Applybtn;
	
	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[2]")
	public WebElement proOfferingservice;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[4]")
	public WebElement clientAccountservice;

	@FindBy(how = How.XPATH, using = "(//button[contains(text(),'View all dependencies')])[3]")
	public WebElement ViewallDependenciesservice;		

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Region__c']")
	public WebElement regionservice;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Sales_Area__c']")
	public WebElement Salesareaservice;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Market__c']")
	public WebElement marketservice;

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Show All Results for')])[3]")
	public WebElement brandSearchservice;
	
	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Show All Results for')])[4]")
	public WebElement modelSearchservice;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[8]")
	public WebElement Sourceservice;
	
	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[9]")
	public WebElement OrigindivService;

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Show All Results for')])[5]")
	public WebElement rAccountSearchservice;
	
	@FindBy(how = How.XPATH, using = "(//a[@title= 'Opportunities'])")
	public WebElement opportunityTab;
	
	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Opportunities')])[1]")
	public WebElement firstOpportunity;
	
	@FindBy(how = How.XPATH, using = "(//button[@title='Recap your call...'])")
	public WebElement recapCall;
	
	@FindBy(how = How.XPATH, using = "(//textarea[@class='textarea textarea uiInput uiInputTextArea uiInput--default uiInput--textarea'])[2]")
	public WebElement oppComment;
	
	@FindBy(how = How.XPATH, using = "(//input[@title='Search Contacts'])[2]")
	public WebElement Oppname;
	
	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Save')])[9]")
	public WebElement OppsaveLog;
	
	@FindBy(how = How.XPATH, using = "//div[contains(text(),'New')]")
	public WebElement newOpportunityBtn;
	
	public String OpportunityRecordType(String record) {
		return "//span[text()='"+record+"']";
	}
	
	@FindBy(how = How.XPATH, using = "(//a[@class='select'])[7]")
	public WebElement RestrictedPartyScreening;
	
	@FindBy(how = How.XPATH, using = "(//a[contains(text(),'Check cleared')])[1]")
	public WebElement Checkcleared;
	
	@FindBy(how = How.XPATH, using = "//input[@title='Search Vehicle Body Styles']")
	public WebElement BodyStyles;
	
	@FindBy(how = How.XPATH, using = "//input[@title='Search Records']")
	public WebElement Vehiclee;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'in Vehicle Body Styles')]")
	public WebElement BodyStylesSearch;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'in Records')]")
	public WebElement VehicleSearch;
	
	@FindBy(how = How.XPATH, using = "//input[@class='input uiInputSmartNumber']")
	public WebElement retailprice;
	
	@FindBy(how = How.XPATH, using = "(//a[@class='select'])[11]")
	public WebElement VATqualifying;
	
	@FindBy(how = How.XPATH, using = "(//input[@class=' input'])[1]")
	public WebElement OpportunityName;
	
	@FindBy(how = How.XPATH, using = "(//input[@class=' input'])[2]")
	public WebElement OpportunityCloseDate;
	
	@FindBy(how = How.XPATH, using = "//span[@class='slds-day weekday todayDate selectedDate DESKTOP uiDayInMonthCell--default']")
	public WebElement OpportunityToday;
	
	@FindBy(how = How.XPATH, using = "//span[text()='25' and @data-aura-class='uiDayInMonthCell--default']")
	public WebElement OpportunityEnddate;
	
	@FindBy(how = How.XPATH, using = "//a[@title='Go to next month']")
	public WebElement OpportunityNextMonth;
	
	@FindBy(how = How.XPATH, using = "(//span[text()='Restricted Party Screening'])[3]")
	public WebElement restrictedPartyScreening;
	
	@FindBy(how = How.XPATH, using = "(//button[@title='Edit Restricted Party Screening'])[3]")
	public WebElement editRPS;
	
	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[8]")
	public WebElement editRPSDD;
	
	@FindBy(how = How.XPATH, using = "(//a[@class='select'])[1]")
	public WebElement OpportunityStage;
	
	@FindBy(how = How.XPATH, using = "//input[@title='Search Records']")
	public WebElement Vehicle;
	
	@FindBy(how = How.XPATH, using = "//input[@class='input uiInputSmartNumber']")
	public WebElement RetailPrice;
	
	@FindBy(how = How.XPATH, using = "(//input[@class='input uiInputSmartNumber'])[4]")
	public WebElement RetailPriceBS;
	
	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Test_Opportunity_')]")
	public WebElement SelectOpportunity;
	
	@FindBy(how = How.XPATH, using = "(//a[@class='select'])[2]")
	public WebElement OpportunityproOffering;
	
	@FindBy(how = How.XPATH, using = "//input[@title='Search Accounts']")
	public WebElement OpportunityAccountName;
	
	@FindBy(how = How.XPATH, using = "(//input[@title='Search Accounts'])[2]")
	public WebElement OpportunityAccountNameBe;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'in Accounts')]")
	public WebElement OppAccountSearch;
	
	public String OppAccountSearchselect(String Account) {
		return "//a[@title='"+Account+"']";
	}
	
	@FindBy(how = How.XPATH, using = "(//a[@class='select'])[13]")
	public WebElement Opportunitysource;
	
	@FindBy(how = How.XPATH, using = "(//a[@class='select'])[8]")
	public WebElement OpportunitysourceBS;
	
	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Save')])[3]")
	public WebElement saveOpportunity;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Edit Request Detail')]")
	public WebElement editRequestDetail;
	
	@FindBy(how = How.XPATH, using = "//textarea[@class='slds-textarea']")
	public WebElement inputRequestDetail;
	
	@FindBy(how = How.XPATH, using = "(//button[contains(text(),'View all dependencies')])[3]")
	public WebElement OpportunityRegionDependencies;
	
	@FindBy(how = How.XPATH, using = "//input[@name='SO_Region__c']")
	public WebElement Opportunityeditregion;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Sales_Area__c']")
	public WebElement OpportunityeditsalesArea;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Market__c']")
	public WebElement Opportunityeditmarket;
	
	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Apply')]")
	public WebElement OpportunityApplybtn;
	
	@FindBy(how = How.XPATH, using = "(//span[@class='slds-grid slds-grid--align-spread'])[2]")
	public WebElement OpportunitySelectIteam1;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Opportunities Menu')]")
	public WebElement OpportunitiesTab;
	
	@FindBy(how = How.XPATH, using = "//input[@name='Opportunity-search-input']")
	public WebElement OpportunitiesSearchBox;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Edit Vehicle')]")
	public WebElement editdonorvehicle;
	
	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Records...']")
	public WebElement inputdonorvehicle;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Show All Results for')]  ")
	public WebElement inputvehicleSearch;
	
	@FindBy(how = How.XPATH, using = "(//button[@title='Save'])[2]")
	public WebElement savecommit;
	
	@FindBy(how = How.XPATH, using = "//button[@title='Save']")
	public WebElement saveOppo;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Mark Stage as Complete']")
	public WebElement markStageAsComplete;

    @FindBy(how = How.XPATH, using = "(//a[contains(@class,'slds-truncate')])[1]")
	public WebElement stageOpportunity;
    
    @FindBy(how = How.XPATH, using = "(//span[contains(text(),'Opportunities')])[4]")
	public WebElement Opportunitynav;
	
	public String Opportunitynavelement(String value) {
		return "//a[@title='"+value+"']";
	}

	@FindBy(how = How.XPATH, using = "(//a[contains(@class,'slds-truncate outputLookupLink slds-truncate')])[2]")
	public WebElement OpporName;
	
	@FindBy(how = How.XPATH, using = "(//button[@title='Show More'])")
	public WebElement Showkeyfield;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Commission') and @class ='title slds-path__title']")
	public WebElement Commission;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Experience') and @class ='title slds-path__title']")
	public WebElement Experience;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Mark as Current Stage')]")
	public WebElement MarkasCurrentStage;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Mark Status as Complete')]")
	public WebElement markStatusasComplete;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Mark Stage as Complete')]")
	public WebElement MarkStageasComplete;
	
	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Experiences')])[1]")
	public WebElement experienceTab;

	@FindBy(how = How.XPATH, using = "(//div[@title='New'])[2]")
	public WebElement newExpBtn;

	@FindBy(how = How.XPATH, using = "(//input[@class=' input'])[1]")
	public WebElement experienceName;

	@FindBy(how = How.XPATH, using = "(//a[@class='datePicker-openIcon display'])[1]")
	public WebElement startDateIcon;

	@FindBy(how = How.XPATH, using = "//a[@class='navLink nextMonth']")
	public WebElement nextMonthIcon;
	
	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[16]")
	public WebElement VATQua;

	@FindBy(how = How.XPATH, using = "//span[text()='15']")
	public WebElement selectStartDate;

	@FindBy(how = How.XPATH, using = "(//a[@class='datePicker-openIcon display'])[2]")
	public WebElement endDateIcon;

	@FindBy(how = How.XPATH, using = "//span[text()='20']")
	public WebElement selectEndDate;

	@FindBy(how = How.XPATH, using = "(//a[@class='select'])[3]")
	public WebElement expType;

	@FindBy(how = How.XPATH, using = "(//a[@class='select'])[4]")
	public WebElement expLocation;

	@FindBy(how = How.XPATH, using = "//button[@title='Save']")
	public WebElement saveBtnExp;

	@FindBy(how = How.XPATH, using = "//span[@title='Guests']")
	public WebElement guestTab;

	@FindBy(how = How.XPATH, using = "(//div[@title='New'])[3]")
	public WebElement newGuestBtn;

	@FindBy(how = How.XPATH, using = "(//a[@title='Experiences'])")
	public WebElement experienceLinkToBackNavigate;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Status']")
	public WebElement editStatusBtn;

	@FindBy(how = How.XPATH, using = "(//button[contains(text(),'View all dependencies')])[1]")
	public WebElement ViewallDependenciesExpStatus;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Status__c']")
	public WebElement statusExp;

	@FindBy(how = How.XPATH, using = "//span[@title='Closed']")
	public WebElement closedStatus;
    
	@FindBy(how = How.XPATH, using = "//input[@name='SO_Sub_Status__c']")
	public WebElement subStatusExp;

	@FindBy(how = How.XPATH, using = "(//span[@title='No show'])")
	public WebElement noShowSubStatus;

	@FindBy(how = How.NAME, using = "apply")
	public WebElement applyBtn;
	
	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Commissioning Requests')])[3]")
	public WebElement OppCommissioningRequests;
	
	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Commissioning Requests')])[1]")
	public WebElement OppCommissioningRequests1;
	
	@FindBy(how = How.XPATH, using = "(//a[@title ='New' and @role='button'])[2]")
	public WebElement NewOppCommissioningRequests;
	
	public String CommissioningRequestsRecTy(String record) {
		return "//span[contains(text(),'"+record+"') and @class ='slds-form-element__label']";
	}
	
	@FindBy(how = How.XPATH, using = "(//a[@class='datePicker-openIcon display'])[1]")
	public WebElement TimeframeStart;
	
	@FindBy(how = How.XPATH, using = "(//input[@name='SO_Starting__c'])[1]")
	public WebElement TimeframeStart1;
	
	@FindBy(how = How.XPATH, using = "(//a[@class='datePicker-openIcon display'])[2]")
	public WebElement TimeframeStop;
	
	@FindBy(how = How.XPATH, using = "(//input[@name='SO_Ending__c'])[1]")
	public WebElement TimeframeStop1;
	
	@FindBy(how = How.XPATH, using = "(//a[contains(text(),'REQ')])[1]")
	public WebElement CommReqName;
	
	@FindBy(how = How.XPATH, using = "//button[@name='SO_Commissioning_Request__c.SO_Submit']")
	public WebElement CommReqSubmit;
	
	@FindBy(how = How.XPATH, using = "(//button[@name='SO_Commissioning_Request__c.SO_Submit'])[2]")
	public WebElement CommReqSubmit1;
	
	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Save')])[6]")
	public WebElement CommReqSave;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Design Brief & Quote') and @class ='title slds-path__title']")
	public WebElement DesignBriefQuote;
	
	@FindBy(how = How.XPATH, using = "(//a[@title='Order Placed'])")
	public WebElement OrderPlacedTab;

	@FindBy(how = How.XPATH, using = "(//a[@title='Estimate'])")
	public WebElement EstQuotTab;
	
	@FindBy(how = How.XPATH, using = "(//a[@title='Contract Signed'])[1]")
	public WebElement ContractSignedTab;

	@FindBy(how = How.XPATH, using = "(//a[@title='Test Classic Service 2'])")
	public WebElement OppTest6;
	
	@FindBy(how = How.XPATH, using = "(//button[@title='Save'])")
	public WebElement saveCommReq;
	
	@FindBy(how = How.XPATH, using = "//a[@title='Select List View']")
	public WebElement listView;
	
	@FindBy(how = How.XPATH, using = "(//span[text()='All Opportunities'])[1]")
	public WebElement allOpportunity;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Bespoke Specification')]")
	public WebElement bespokeSpecification;
	
	@FindBy(how = How.XPATH, using = "(//span[@class='inline-edit-trigger-icon slds-button__icon slds-button__icon_hint'])[17]")
	public WebElement editField;
	
	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[10]")
	public WebElement handOfDrive;
	
	@FindBy(how = How.XPATH, using = "(//textarea[@class='slds-textarea'])[2]")
	public WebElement bespokeIntReq;
	
	@FindBy(how = How.XPATH, using = "//input[@name='SO_Retail_Price__c']")
	public WebElement retailPrice;
	
	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[11]")
	public WebElement OppCurrency;
	
	@FindBy(how = How.XPATH, using = "(//a[@class='select'])[14]")
	public WebElement OpportunitysourceReborn;

    @FindBy(how = How.XPATH, using = "//span[contains(text(),'Specification & Quotation') and @class ='title slds-path__title']")
	public WebElement SpecificationQuotation;
    
    @FindBy(how = How.XPATH, using = "(//span[text()='Classic AutomatedTesting'])[5]")
   	public WebElement classicAutoTest;
    
    @FindBy(how = How.XPATH, using = "(//a[@title='Contracting'])[2]")
   	public WebElement contracting;
    
    @FindBy(how = How.XPATH, using = "//div[@class='slds-theme--info slds-notify--toast slds-notify slds-notify--toast forceToastMessage']")
   	public WebElement contractingError;
    
    @FindBy(how = How.XPATH, using = "//span[@class='toastMessage forceActionsText']")
   	public WebElement contractingErrorMsgText;
    
    @FindBy(how = How.XPATH, using = "//span[contains(text(),'Order Placed') and @class ='title slds-path__title']")
	public WebElement OrderPlaced;
	
	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Orders')])[9]")
	public WebElement OppOrder;
	
	@FindBy(how = How.XPATH, using = "(//a[contains(@class,'slds-truncate outputLookupLink slds-truncate outputLookupLink')])[22]")
	public WebElement OppopenOrder;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Outcome') and @class ='title slds-path__title']")
	public WebElement Outcome;
	
	@FindBy(how = How.XPATH, using = "//input[@name = 'SO_Status__c']")
	public WebElement OrderStatus;
	
	@FindBy(how = How.XPATH, using = "//input[@name = 'SO_Outcome__c']")
	public WebElement OrderStatusoutcome;
	
	@FindBy(how = How.XPATH, using = "(//span[text()='Edit FMS Passout Obtained'])[1]")
	public WebElement EditFMSPassoutObtained;
	
	@FindBy(how = How.XPATH, using = "(//input[@type='checkbox'])[5]")
	public WebElement CheckFMSPassoutObtained;
	
	@FindBy(how = How.XPATH, using = "(//input[@type='checkbox'])[6]")
	public WebElement checkFMSDisposalCompleted;
	
	@FindBy(how = How.XPATH, using = "(//input[@type='checkbox'])[7]")
	public WebElement checkRemovedfromETracker;
	
	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Save')])[3]")
	public WebElement savechange;
	
	@FindBy(how = How.XPATH, using = "//a[@data-tab-name='Closed']")
    public WebElement ClosedState;

	@FindBy(how = How.XPATH, using = "//span[text()='Select Closed Stage']")
	public WebElement selectClosedStageBtn;

	@FindBy(how = How.XPATH, using = "//input[@name='StageName']")
	public WebElement editStageName;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Lost_Reason__c']")
	public WebElement lostReason;
	
	@FindBy(how = How.XPATH, using = "//span[@title='Closed Lost']")
	public WebElement closedLostStage;
	
	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
	public WebElement doneBtn;

	@FindBy(how = How.XPATH, using = "//span[@title='No longer purchasing']")
	public WebElement noLongerPurchasingReason;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'New Record')]")
	public WebElement Createvehicle;
	
	@FindBy(how = How.XPATH, using = "(//input[@class=' input'])[1]")
	public WebElement RecordName;
	
	@FindBy(how = How.XPATH, using = "(//input[@class=' input'])[3]")
	public WebElement FullVin;
	
	@FindBy(how = How.XPATH, using = "(//button[@title='Save'])")
	public WebElement savechange2;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Wait List') and @class ='title slds-path__title']")
	public WebElement WaitList;
	
	@FindBy(how = How.XPATH, using = "//button[@title='Edit Restricted Party Screening']")
	public WebElement editRestrictedPartyScreening;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[8]")
    public WebElement restrictedPartyScreeningDropdown;

	@FindBy(how = How.XPATH, using = "//span[text()='Check in progress']")
    public WebElement checkInProgress;

	@FindBy(how = How.XPATH, using = "//button[text()='Save']")
    public WebElement saveBtn;

	@FindBy(how = How.XPATH, using = "//span[text()='Check cleared']")
    public WebElement checkCleared;
	
	@FindBy(how = How.XPATH, using = "//span[@title='JLR Retail Case Management']")
    public WebElement jlrText;
	
	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-show']")
    public WebElement menuBtn;
	
	@FindBy(how = How.XPATH, using = "//input[@class='slds-input']")
    public WebElement searchBox;
	
	public String selectMenu(String text) {
		return "//b[text()='"+text+"']";
	}

	@FindBy(how = How.XPATH, using = "(//a[@title='Test Private Office'])[2]")
	public WebElement OppTestBtn1;

	@FindBy(how = How.XPATH, using = "(//a[@role='option'])[13]")
	public WebElement RetailerOp1;

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Quotes')])[1]")
	public WebElement QuoteBtn1;
	
	@FindBy(how = How.XPATH, using = "(//div[contains(text(),'New Quote')])")
	public WebElement NewQuoteBtn1;
	
	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Qualified')])[2]")
	public WebElement QualifiedHdr;
	
	@FindBy(how = How.XPATH, using = "(//a[@title='FreeText'])[1]")
	public WebElement OppTest2;
	
	@FindBy(how = How.XPATH, using = "(//a[@title='Test Classic Bespoke'])[1]")
	public WebElement OppTest3;
	
	@FindBy(how = How.XPATH, using = "(//input[@title='Search Contacts'])")
    public WebElement RetailerConIP;
	
    @FindBy(how = How.XPATH, using = "(//input[@class=' input'])[1]")
    public WebElement QuoteNameIP;
    
    @FindBy(how = How.XPATH, using = "(//span[contains(text(),'Next')])")
    public WebElement QuoteNextBtn1;
    
    @FindBy(how = How.XPATH, using = "(//button[@title='Save'])[2]")
    public WebElement SaveQuote1;

    @FindBy(how = How.XPATH, using = "(//a[@title='Test Commited'])[1]")
	public WebElement OppTest4;
    
    @FindBy(how = How.XPATH, using = "(//a[@title='Test1309'])")
	public WebElement OppTest1309;

	@FindBy(how = How.XPATH, using = "//a[@title='Specification & Quotation']")
	public WebElement SpecNQuotTab;

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Mark as Current Stage')])")
	public WebElement MarkAsCurrent;
	
	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Specification & Quotation')])[3]")
	public WebElement SelectSpecification;
	
	@FindBy(how = How.XPATH, using = "//a[@title='Home']")
    public WebElement homeBtn;
	
	@FindBy(how = How.XPATH, using = "(//div[@class='slds-media__body'])[4]")
    public WebElement Quotes;
	
	@FindBy(how = How.XPATH, using = "(//a[@role='option'])[7]")
	public WebElement SelectRetailer;
	
	@FindBy(how = How.XPATH, using = "(//a[@class='forceBreadCrumbItem'])[2]")
	public WebElement BackToOpp;
	
	@FindBy(how = How.XPATH, using = "//a[@title='.']")
	public WebElement OpenQuote;
	
	@FindBy(how = How.XPATH, using = "//a[@title='Outcome']")
	public WebElement QuoteOutcome;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Mark as Current Status')]")
	public WebElement MarkCurrentStatus;
	
	@FindBy(how = How.XPATH, using = "//span[@title='Accepted']")
	public WebElement OutcomeAccept;
	
	@FindBy(how = How.XPATH, using = "//input[@name = 'SO_Outcome_Detail__c']")
	public WebElement OutomeDetail;
	
	@FindBy(how = How.XPATH, using = "(//span[@title='Accepted'])[2]")
	public WebElement OutcomeDetailAccept;
	
	@FindBy(how = How.XPATH, using = "//a[contains(@href,'/lightning/r/Quote/')]")
	public WebElement SelectQuote;
	
	@FindBy(how = How.XPATH, using = "//slot[@class='slds-page-header__title slds-m-right--small slds-align-middle clip-text slds-line-clamp']")
	public WebElement OppName;
	
	public String OppName(String Name) {
        return "//a[contains(text(),'"+Name+"')]";
    }
	
	@FindBy(how = How.XPATH, using = "//a[@title='Contacted']")
	public WebElement contactedTab;	
	
	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Select')][1]")
	public WebElement PotentialMatchSelect;
	
	

}




