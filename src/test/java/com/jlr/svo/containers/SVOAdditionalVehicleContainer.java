package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SVOAdditionalVehicleContainer {
	@FindBy(how = How.XPATH, using = "//div[text()='New']")
	public WebElement NewOpportunity;

	@FindBy(how = How.XPATH, using = "//input[@name='Name']")
	public WebElement SVOBespokeOppName;

	@FindBy(how = How.XPATH, using = "//input[@name='CloseDate']")
	public WebElement SVOBespokeClosedDate;

	@FindBy(how = How.XPATH, using = "(//th[@class='slds-cell-edit cellContainer']//child::span[@class='slds-grid slds-grid--align-spread']//child::a[contains(text(),'SVO-')])[2]")
	public WebElement SecondDesignBrief;

	@FindBy(how = How.XPATH, using = "(//label[contains(text(),'Stage')]//following-sibling::div//child::lightning-base-combobox//child::div//child::div)[1]")
	public WebElement SVOBespokeStage;

	@FindBy(how = How.XPATH, using = "(//label[contains(text(),'Product Offering')]//following-sibling::div//child::lightning-base-combobox//child::div//child::div)[1]")
	public WebElement SVOBespokeProductOffering;

	@FindBy(how = How.XPATH, using = "(//label[contains(text(),'Region')]//following-sibling::div//child::lightning-base-combobox//child::div//child::div)[1]")
	public WebElement SVOBespokeRegion;

	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Client')]//following-sibling::div//child::lightning-base-combobox//child::div//child::div//child::input")
	public WebElement SVOBespokeClient;

	@FindBy(how = How.XPATH, using = "(//input[@placeholder='Search Accounts...'])[1]")
	public WebElement SVOClassicClient;

	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Preferred Contact')]//following-sibling::div//child::lightning-base-combobox//child::div//child::div//child::input")
	public WebElement SVOBespokePreferredContact;

	@FindBy(how = How.XPATH, using = "(//input[@placeholder='Search Contacts...'])[1]")
	public WebElement SVOClassicPreferredContact;

	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Account Name')]//following-sibling::div//child::lightning-base-combobox//child::div//child::div//child::input")
	public WebElement SVOBespokeAccountName;

	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Retailer Contact')]//following-sibling::div//child::lightning-base-combobox//child::div//child::div//child::input")
	public WebElement SVOBespokeRetailerContact;

	@FindBy(how = How.XPATH, using = "(//label[text()='Source']//following-sibling::div//child::lightning-base-combobox//child::div//child::div)[1]")
	public WebElement SVOBespokeSource;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Vehicle Manufacturers...']")
	public WebElement SVOBespokeBrand;

	@FindBy(how = How.XPATH, using = "//div[@class='test-id__section slds-m-vertical_none slds-section has-header slds-p-bottom_medium slds-is-open']//span[text()='Vehicle Details']")
	public WebElement VehicleDetails;

	@FindBy(how = How.XPATH, using = "//span[text()='Source Information']")
	public WebElement SourceInformation;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Vehicle Models...']")
	public WebElement SVOBespokeModel;

	@FindBy(how = How.XPATH, using = "//button[@name='SaveEdit']")
	public WebElement SVOBespokeSaveOpportunity;

	public String StageSelection(String Stage) {
		return "//label[text()='Stage']//following-sibling::div//child::lightning-base-combobox//child::div//child::div//child::lightning-base-combobox-item//child::span//child::span[contains(text(),'"
				+ Stage + "')]";
	}

	public String ProOfferingSelection(String proOffering) {
		return "//span[@title = '" + proOffering + "']";
	}

	public String RegionSelection(String region) {
		return "//span[@title = '" + region + "']";
	}

	public String SourceSelection(String source) {
		return "//span[@title = '" + source + "']";
	}

	public String ModelYearSelection(String my) {
		return "//span[@title = '" + my + "']";
	}

	public String BackToOpportunity(String oppor) {
		return "//li[@class='slds-breadcrumb__item slds-line-height--reset']//child::a[text()= '" + oppor + "']";
	}

	public String BackToOpportunityFromAV(String oppor) {
		return "//span[text() = '" + oppor + "']";
	}

	@FindBy(how = How.XPATH, using = "//label[text()='Client']//following-sibling::div//child::div//child::lightning-base-combobox//child::div//child::div[contains(@class,'slds-dropdown_left')]//child::lightning-base-combobox-item//child::span[@class='slds-media__body']//child::span[contains(@class,'slds-listbox__option-text_entity')]//span[contains(text(),'Show All Results for')]")
	public WebElement SVOBespokeClientNameSearch;

	@FindBy(how = How.XPATH, using = "//lightning-base-combobox-formatted-text[@title='Test ESign Retailer']")
	public WebElement SVOClassicClientNameSearch;

	public String ValueSelection(String value) {
		return "//table[contains(@class,'uiVirtualDataGrid')]//child::tbody//child::tr//child::td//child::a[text()='"
				+ value + "']";
	}

	@FindBy(how = How.XPATH, using = "//div[@class='slds-listbox slds-listbox_vertical slds-dropdown slds-dropdown_fluid slds-dropdown_left slds-dropdown_length-with-icon-7']//child::ul//child::li//child::lightning-base-combobox-item//child::span//child::span//child::lightning-base-combobox-formatted-text[@title='Test ESign']")
	public WebElement SVOBespokePreferredContactname;

	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Originating Division')]//following-sibling::div//child::lightning-base-combobox//child::div//child::div//child::input[@class='slds-input slds-combobox__input']")
	public WebElement SVOOriginatingDivission;

	public String RPSSelection(String Checkcleared) {
		return "//span[text() = '" + Checkcleared + "']";
	}

	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Restricted Party Screening')]//following-sibling::div//child::lightning-base-combobox//child::div//child::div//child::input[@aria-haspopup='listbox']")
	public WebElement RestrictedPartyScreening;

	public String PreferredContactSelection(String Contact) {
		return "//table[contains(@class,'uiVirtualDataGrid')]//a[text()='" + Contact + "']";
	}

	@FindBy(how = How.XPATH, using = "//label[text()='Preferred Contact']//following-sibling::div//child::div//child::lightning-base-combobox//child::div//child::div[contains(@class,'slds-dropdown_left')]//child::lightning-base-combobox-item//child::span[@class='slds-media__body']//child::span[contains(@class,'slds-listbox__option-text_entity')]//span[contains(text(),'Show All Results for')]")
	public WebElement SVOBespokePreferredContactSearch;

	@FindBy(how = How.XPATH, using = "//label[text()='Account Name']//following-sibling::div//child::div//child::lightning-base-combobox//child::div//child::div[contains(@class,'slds-dropdown_left')]//child::lightning-base-combobox-item//child::span[@class='slds-media__body']//child::span[contains(@class,'slds-listbox__option-text_entity')]//span[contains(text(),'Show All Results for')]")
	public WebElement SVOBespokeAccountNameSearch;

	@FindBy(how = How.XPATH, using = "//label[text()='Retailer Contact']//following-sibling::div//child::div//child::lightning-base-combobox//child::div//child::div[contains(@class,'slds-dropdown_left')]//child::lightning-base-combobox-item//child::span[@class='slds-media__body']//child::span[contains(@class,'slds-listbox__option-text_entity')]//span[contains(text(),'Show All Results for')]")
	public WebElement SVOBespokeRetailerContactSearch;

	@FindBy(how = How.XPATH, using = "//label[text()='Brand']//following-sibling::div//child::div//child::lightning-base-combobox//child::div//child::div[contains(@class,'slds-dropdown_left')]//child::lightning-base-combobox-item//child::span[@class='slds-media__body']//child::span[contains(@class,'slds-listbox__option-text_entity')]//span[contains(text(),'Show All Results for')]")
	public WebElement SVOBespokeBrandSearch;

	@FindBy(how = How.XPATH, using = "//label[text()='Model']//following-sibling::div//child::div//child::lightning-base-combobox//child::div//child::div[contains(@class,'slds-dropdown_left')]//child::lightning-base-combobox-item//child::span[@class='slds-media__body']//child::span[contains(@class,'slds-listbox__option-text_entity')]//span[contains(text(),'Show All Results for')]")
	public WebElement SVOBespokeModelSearch;

	@FindBy(how = How.XPATH, using = "//span[text()='Recently Viewed']")
	public WebElement RecentlyViewedLink;

	@FindBy(how = How.XPATH, using = "//li[contains(@class,'slds-dropdown__item has-icon--left   forceVirtualAutocompleteMenuOption')]//child::a//child::lightning-icon//following-sibling::span[contains(@class,' virtualAutocompleteOptionText') and contains(text(),'All Private Office Opportunities')] ")
	public WebElement AllPrivateOfficeOpportunityLink;

	@FindBy(how = How.XPATH, using = "(//a[contains(@href,'/lightning/r/') and @data-refid='recordId'])[1]")
	public WebElement FirstOpportunityLink;

	@FindBy(how = How.XPATH, using = "//input[@name='Opportunity-search-input']")
	public WebElement OpportunitySearchBox;

	@FindBy(how = How.XPATH, using = "(//a[contains(@href,'related/Orders/view')]//child::slot//child::span[contains(text(),'Orders')])")
	public WebElement OrdersLink;

	@FindBy(how = How.XPATH, using = "//a[contains(@href,'related/Orders/view')]//child::span[contains(text(),'Orders')]")
	public WebElement OppOrdersLink;

	@FindBy(how = How.XPATH, using = "//span[@class='slds-grid slds-grid--align-spread']//child::a[contains(@href,'/lightning/r/') and contains(text(),'0000')]")
	public WebElement FirstOrder;

	@FindBy(how = How.XPATH, using = "//a[contains(@href,'/lightning/r/') and contains(text(),'0000')]")
	public WebElement FirstOppOrder;

	@FindBy(how = How.XPATH, using = "//span[text()='Mark Stage as Complete']//parent::button")
	public WebElement MarkStageAsComplete;

	@FindBy(how = How.XPATH, using = "(//a[contains(@href,'related/Quotes/view')]//child::slot//child::span[contains(text(),'Quotes')])")
	public WebElement QuotesLink;

	@FindBy(how = How.XPATH, using = "//child::span[text()='Close tab']//parent::button[contains(@class,'slds-m-right_x-small')]")
	public WebElement CloseOrderTab;

	@FindBy(how = How.XPATH, using = "//a[@title='New Quote']//child::div[text()='New Quote']")
	public WebElement NewQuoteBtn;

	@FindBy(how = How.XPATH, using = "//span[text()='Quote Name']//parent::label//following-sibling::input[@class=' input']")
	public WebElement QuoteNameBox;

	@FindBy(how = How.XPATH, using = "//input[@title='Search Contacts']")
	public WebElement QuoteRetailerNameBox;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Cost Price ')]//parent::label//following-sibling::input[@class='input uiInputSmartNumber']")
	public WebElement QuoteCostPrice;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'List Price')]//parent::label//following-sibling::input[@class='input uiInputSmartNumber']")
	public WebElement QuoteListPrice;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Contribution')]//parent::label//following-sibling::input[@class='input uiInputSmartNumber']")
	public WebElement QuoteContribution;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Retail Price')]//parent::label//following-sibling::input[@class='input uiInputSmartNumber']")
	public WebElement QuoteRetailPrice;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Revenue')]//parent::label//following-sibling::input[@class='input uiInputSmartNumber']")
	public WebElement QuoteRevenue;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Expiration Date')]//parent::label//following-sibling::div//child::input[@class=' input']")
	public WebElement QuoteExpirationDate;

	@FindBy(how = How.XPATH, using = "//div[@class='button-container-inner slds-float_right']//child::button[@title='Save']//child::span[text()='Save']")
	public WebElement QuoteSaveBtn;

	@FindBy(how = How.XPATH, using = "//a[contains(@title,'SVO-')]")
	public WebElement FirstQuote;

	@FindBy(how = How.XPATH, using = "//span[@title='Design Briefs']//parent::a[contains(@href,'related/Design_Briefs__r/view')]")
	public WebElement DesignBriefLink;

	@FindBy(how = How.XPATH, using = "//th[@class='slds-cell-edit cellContainer']//child::span[@class='slds-grid slds-grid--align-spread']//child::a[contains(text(),'SVO-')]")
	public WebElement FirstDesignBrief;

	@FindBy(how = How.XPATH, using = "//button[@name='SO_Design_Brief__c.SO_Submit_to_Design']")
	public WebElement DesignBriefSubmit;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-button_brand cuf-publisherShareButton undefined uiButton']//child::span[text()='Save']")
	public WebElement DesignBriefSaveSubmission;

	@FindBy(how = How.XPATH, using = "//button[@name='SO_Design_Brief__c.SO_Return_Render_Pack']")
	public WebElement DesignBriefReturnRenderPack;

	@FindBy(how = How.XPATH, using = "//button[@name='Save']")
	public WebElement DesignBriefSaveRenderPack;

	@FindBy(how = How.XPATH, using = "//slot[@name='primaryField']//child::slot//child::lightning-formatted-text[contains(text(),'SVO-')]")
	public WebElement DesignBriefName;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Design_Brief__c-search-input']")
	public WebElement DesignBriefSearchBox;

	@FindBy(how = How.XPATH, using = "//h1[@class='slds-scrollable_none']//child::span[text()='Design Team Queue']")
	public WebElement DesignBriefQueue;

	@FindBy(how = How.XPATH, using = "//span[text()='All']")
	public WebElement DesignBriefAllList;

	@FindBy(how = How.XPATH, using = "//td[@class='slds-cell-edit cellContainer']//following-sibling::th[@class='slds-cell-edit cellContainer']//child::span//child::a[contains(text(),'SVO-')]")
	public WebElement FirstDesignBriefFromAllList;

	@FindBy(how = How.XPATH, using = "//div[@class='slds-grid']//child::a[@class='flex-wrap-ie11' and contains(@href,'/lightning/r')]//child::slot//child::slot//span[contains(text(),'SVO-')]")
	public WebElement QuoteNameFromDesignBrief;

	@FindBy(how = How.XPATH, using = "//span[text()='Mark Status as Complete']//parent::button")
	public WebElement QuoteMarkStageAsComplete;

	// @FindBy(how = How.XPATH, using = "//input[@name='SO_Outcome__c']")
	// public WebElement QuoteOutcomeDrpDwn;

	@FindBy(how = How.XPATH, using = "//button[@name='SO_Outcome__c']")
	public WebElement QuoteOutcomeDrpDwn;

	@FindBy(how = How.XPATH, using = "//span[text()='Accepted']")
	public WebElement QuoteOutcomeAccepted;

//	@FindBy(how = How.XPATH, using = "//input[@name='SO_Outcome_Detail__c']")
//	public WebElement QuoteOutcomeDetailDrpDwn;

	@FindBy(how = How.XPATH, using = "//button[@name='SO_Outcome_Detail__c']")
	public WebElement QuoteOutcomeDetailDrpDwn;

	@FindBy(how = How.XPATH, using = "//label[text()='Outcome Detail']//parent::lightning-combobox//child::div//child::lightning-base-combobox//child::div//child::div[contains(@class,'slds-dropdown_fluid slds-dropdown_left')]//child::lightning-base-combobox-item[@data-value='Accepted']//child::span[@class='slds-media__body']//child::span[text()='Accepted']")
	public WebElement QuoteOutcomeDetailAccepted;

	@FindBy(how = How.XPATH, using = "//button[text()='Done']")
	public WebElement QuoteOutcomeDoneBtn;

	@FindBy(how = How.XPATH, using = "(//div[contains(@class,'slds-p-top--none')]//child::div[contains(@class,'forcePageBlockSectionRow')]//child::div[contains(@class,'forcePageBlockItemView')]//child::div[@class='slds-form-element slds-form-element_readonly slds-grow slds-hint-parent override--slds-form-element']//child::div[@class='slds-form-element__control slds-grid itemBody']//child::a[contains(@href,'/lightning/r')])[1]")
	public WebElement QuoteOpportunityLink;

	@FindBy(how = How.XPATH, using = "(//button[@title='Edit Common Order Number']//child::span[text()='Edit Common Order Number'])[1]")
	public WebElement CommonOrderNumber;

	@FindBy(how = How.XPATH, using = "//span[text()='Common Order Number']//parent::label//following-sibling::input[@class=' input']")
	public WebElement CommonOrderNumberBox;

	@FindBy(how = How.XPATH, using = "//button[@title='Save']")
	public WebElement SaveCommonOrderNumber;

	@FindBy(how = How.XPATH, using = "//div[@class='slds-media__body']//child::slot//child::lightning-formatted-text")
	public WebElement OpportunityName;

	@FindBy(how = How.XPATH, using = "//table[contains(@class,'uiVirtualDataTable')]//child::tbody//child::tr//child::td[@class='slds-cell-edit cellContainer']//child::span[@class='slds-grid slds-grid--align-spread']//child::a[contains(text(),'SVO-')]")
	public WebElement FirstQuoteFromOpportunity;

	@FindBy(how = How.XPATH, using = "//span[text()='Closed']//parent::a")
	public WebElement OpportunityClosedStage;

	@FindBy(how = How.XPATH, using = "//span[text()='Select Closed Stage']//parent::button")
	public WebElement OpportunitySelectClosedStage;

	@FindBy(how = How.XPATH, using = "//input[@name='StageName']")
	public WebElement OpportunityClosedStageName;

	@FindBy(how = How.XPATH, using = "//span[text()='Closed Lost']//parent::span//parent::lightning-base-combobox-item")
	public WebElement OpportunityClosedLostStage;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Lost_Reason__c']")
	public WebElement OpportunityClosedLostReason;

	@FindBy(how = How.XPATH, using = "//span[text()='No response']")
	public WebElement OpportunityClosedLostReasonValue;

	@FindBy(how = How.XPATH, using = "//a[contains(@href,'related/Additional_Vehicles__r/view')]//child::slot//child::span[contains(text(),'Additional Vehicles')]")
	public WebElement AdditionalVehicleLink;

	@FindBy(how = How.XPATH, using = "(//a[@title='New']//child::div[text()='New'])[2]")
	public WebElement NewAdditionalVehicleBtn;

	@FindBy(how = How.XPATH, using = "//a[@title='New']//child::div[text()='New']")
	public WebElement NewAdditionalVehicleBtn1;

	@FindBy(how = How.XPATH, using = "//button[@name='CancelEdit']")
	public WebElement CancelAdditionalVehicleBtn;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Vehicle Manufacturers...']")
	public WebElement AdditionalVehicleBrand;

	@FindBy(how = How.XPATH, using = "(//a[contains(@href,'related/Additional_Vehicles__r/view')]//child::slot//child::span[contains(text(),'Additional Vehicles')])[2]")
	public WebElement AdditionalVehicleHyperLink;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Vehicle Models...']")
	public WebElement AdditionalVehicleModel;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Vehicle Series...']")
	public WebElement AdditionalVehicleSeriesName;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Vehicle Derivatives...']")
	public WebElement AdditionalVehicleDerivative;

	@FindBy(how = How.XPATH, using = "//label[text()='Wheel Base']//parent::lightning-combobox//child::div//child::lightning-base-combobox//child::div//child::div")

	public WebElement AdditionalVehicleWheelBase;

	@FindBy(how = How.XPATH, using = "//label[text()='Model Year']//parent::lightning-combobox//child::div//child::lightning-base-combobox//child::div//child::div")

	public WebElement AdditionalVehicleModelYear;

	@FindBy(how = How.XPATH, using = "//label[text()='Hand of Drive']//parent::lightning-combobox//child::div//child::lightning-base-combobox//child::div//child::div")
	public WebElement AdditionalVehicleHandOfDrive;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Records...']")
	public WebElement AdditionalVehicleVehicleRecord;

	@FindBy(how = How.XPATH, using = "//label[text()='Series Name']//following-sibling::div//child::div//child::lightning-base-combobox//child::div//child::div[contains(@class,'slds-dropdown_left')]//child::lightning-base-combobox-item//child::span[@class='slds-media__body']//child::span[contains(@class,'slds-listbox__option-text_entity')]//span[contains(text(),'Show All Results for')]")
	public WebElement AdditionalVehicleVehicleSeriesNameSearch;

	@FindBy(how = How.XPATH, using = "//label[text()='Derivative']//following-sibling::div//child::div//child::lightning-base-combobox//child::div//child::div[contains(@class,'slds-dropdown_left')]//child::lightning-base-combobox-item//child::span[@class='slds-media__body']//child::span[contains(@class,'slds-listbox__option-text_entity')]//span[contains(text(),'Show All Results for')]")
	public WebElement AdditionalVehicleVehicleDerivativeSearch;

	@FindBy(how = How.XPATH, using = "//span[text()='Standard Wheelbase']")
	public WebElement AdditionalVehicleWheelBaseStandard;

	@FindBy(how = How.XPATH, using = "//span[text()='LHD']")
	public WebElement AdditionalVehicleHODLHD;

	@FindBy(how = How.XPATH, using = "//button[contains(@class,'slds-accordion__summary-action')]//child::span[@title='Additional Vehicles']")
	public WebElement AdditionalVehicleSection;

	@FindBy(how = How.XPATH, using = "//a[contains(@class,'baseCard__header-title-container')]//child::span[@title='Additional Vehicles']")
	public WebElement AdditionalVehicleSectionFromOrders;

	@FindBy(how = How.XPATH, using = "//div[@class='slds-tile__detail']")
	public WebElement AdditionalVehicleSectionMiniTable;

	@FindBy(how = How.XPATH, using = "//dt[text()='Brand']")
	public WebElement AdditionalVehicleSectionMiniTableBrand;

	@FindBy(how = How.XPATH, using = "//span[@class='slds-truncate slds-m-right--xx-small']")
	public WebElement AdditionalVehicleOption;

	@FindBy(how = How.XPATH, using = "//dt[text()='Model']")
	public WebElement AdditionalVehicleSectionMiniTableModel;

	@FindBy(how = How.XPATH, using = "//dt[text()='Series Name']")
	public WebElement AdditionalVehicleSectionMiniTableSeriesName;

	@FindBy(how = How.XPATH, using = "//a[contains(@href,'/related/Additional_Vehicles__r/view')]//child::span[contains(text(),'View All')]")
	public WebElement AdditionalVehicleSectionViewAll;

	@FindBy(how = How.XPATH, using = "//a[@class='toggle slds-th__action slds-text-link--reset ']//child::span[text()='Brand']")
	public WebElement AdditionalVehicleSectionBrand;

	@FindBy(how = How.XPATH, using = "//a[@class='toggle slds-th__action slds-text-link--reset ']//child::span[text()='Model']")
	public WebElement AdditionalVehicleSectionModel;

	@FindBy(how = How.XPATH, using = "//a[@class='toggle slds-th__action slds-text-link--reset ']//child::span[text()='Series Name']")
	public WebElement AdditionalVehicleSectionSeriesName;

	@FindBy(how = How.XPATH, using = "//a[@class='toggle slds-th__action slds-text-link--reset ']//child::span[text()='Derivative']")
	public WebElement AdditionalVehicleSectionDerivative;

	@FindBy(how = How.XPATH, using = "//a[@class='toggle slds-th__action slds-text-link--reset ']//child::span[text()='Model Year']")
	public WebElement AdditionalVehicleSectionModelYear;

	@FindBy(how = How.XPATH, using = "//a[@class='toggle slds-th__action slds-text-link--reset ']//child::span[text()='Wheel Base']")
	public WebElement AdditionalVehicleSectionWheelBase;

	@FindBy(how = How.XPATH, using = "//a[@class='toggle slds-th__action slds-text-link--reset ']//child::span[text()='Hand of Drive']")
	public WebElement AdditionalVehicleSectionHandOfDrive;

	@FindBy(how = How.XPATH, using = "//a[@class='toggle slds-th__action slds-text-link--reset ']//child::span[text()='Vehicle']")
	public WebElement AdditionalVehicleSectionVehicleRecord;

	@FindBy(how = How.XPATH, using = "//span[@class='slds-grid slds-grid--align-spread']//child::a[contains(text(),'AV')]")
	public WebElement AdditionalVehicleSectionFirstAVRecord;

	@FindBy(how = How.XPATH, using = "(//span[@class='slds-grid slds-grid--align-spread']//child::a[contains(text(),'AV')])[2]")
	public WebElement AdditionalVehicleLinkFirstAVRecord;

	@FindBy(how = How.XPATH, using = "(//a[contains(text(),'AV')])")
	public WebElement AdditionalVehicleSecFirstAVRecord;

	@FindBy(how = How.XPATH, using = "//slot[contains(text(),'uat-user')]//parent::span[@class='displayLabel']")
	public WebElement AdditionalVehicleOwnerlabel;

	@FindBy(how = How.XPATH, using = "//button[@name='SaveAndNew']")
	public WebElement AdditionalVehicleSaveAndNew;

	@FindBy(how = How.XPATH, using = "//button[@name='Delete']")
	public WebElement AdditionalVehicleRecordDeleteBtn;

	@FindBy(how = How.XPATH, using = "//span[text()='Delete']//parent::button")
	public WebElement AdditionalVehicleRecordConfirmDeleteBtn;

	@FindBy(how = How.XPATH, using = "//lightning-formatted-rich-text[@class='slds-rich-text-editor__output']//child::span[contains(text(),'No')]")
	public WebElement AdditionalVehicleNoItemDisplayLabel;

	@FindBy(how = How.XPATH, using = "//span[text()='Show more actions']//parent::button")
	public WebElement AdditionalVehicleShowMoreActions;

	@FindBy(how = How.XPATH, using = "(//span[text()='Show more actions']//parent::button)[2]")
	public WebElement AdditionalVehicleShowMoreActionsFromOrders;

	@FindBy(how = How.XPATH, using = "//a[@title='Edit']")
	public WebElement AdditionalVehicleEditBtn;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Show All')]")
	public WebElement OpportunityShowAllLinks;

	@FindBy(how = How.XPATH, using = "//span[text()='Edit']//parent::a[@name='Edit']")
	public WebElement AdditionalVehicleEditBtnFromOrders;

	@FindBy(how = How.XPATH, using = "//label[text()='Model']//parent::lightning-grouped-combobox//child::button[@title='Clear Selection']")
	public WebElement AdditionalVehicleModelClearText;

	@FindBy(how = How.XPATH, using = "//label[text()='Series Name']//parent::lightning-grouped-combobox//child::button[@title='Clear Selection']")
	public WebElement AdditionalVehicleSeriesNameClearText;

	@FindBy(how = How.XPATH, using = "//label[text()='Derivative']//parent::lightning-grouped-combobox//child::button[@title='Clear Selection']")
	public WebElement AdditionalVehicleDerivativeClearText;

	@FindBy(how = How.XPATH, using = "//p[@title='Model']//parent::div//following-sibling::p//child::a[@class='flex-wrap-ie11 slds-truncate']//child::span")
	public WebElement AdditionalVehicleModelName;

	@FindBy(how = How.XPATH, using = "(//span[text()='Show actions for this object']/ancestor::a[contains(@class,'slds-button--icon-border-filled')])[2]")
	public WebElement AVOrdersNoActionAvailableBtn;

	@FindBy(how = How.XPATH, using = "//a[@title='undefined' and contains(text(),'No')]")
	public WebElement AVOrdersNoActionAvailableLabel;

	@FindBy(how = How.XPATH, using = "(//slot[@name='actionsProvider']//following-sibling::ul//child::li//child::lightning-button-menu//child::button[@class='slds-button slds-button_icon-border-filled slds-button_icon-x-small'])[3]")
	public WebElement AdditionalVehicleDropDown;

	@FindBy(how = How.XPATH, using = "//a[@name='New']")
	public WebElement NewVehicleBtn;

	@FindBy(how = How.XPATH, using = "//button[@name='CancelEdit']")
	public WebElement CancelBtn;

	@FindBy(how = How.XPATH, using = "//div[@class='entityNameTitle slds-line-height--reset']")
	public WebElement OpportunityTitle;

	@FindBy(how = How.XPATH, using = "//a[@title='New']")
	public WebElement NewBtn;

	@FindBy(how = How.XPATH, using = "//li[contains(@class,'slds-dropdown__item has-icon--left   forceVirtualAutocompleteMenuOption')]//child::a//child::lightning-icon//following-sibling::span[contains(@class,' virtualAutocompleteOptionText') and contains(text(),'All Bespoke Opportunities')]")
	public WebElement BespokeOpportunities;

	@FindBy(how = How.XPATH, using = "(//span[text()='Items to Approve'])")
	public WebElement ItemsToApprove;

	@FindBy(how = How.XPATH, using = "//button[text()='Manage All']")
	public WebElement ManageAll;

	@FindBy(how = How.XPATH, using = "(//span[text()='Related To'])[1]")
	public WebElement RelatedTo;

	@FindBy(how = How.XPATH, using = "(//span[text()='Type'])[1]")
	public WebElement Type;

	@FindBy(how = How.XPATH, using = "(//span[text()='Submitted By'])[1]")
	public WebElement SubmittedBy;

	@FindBy(how = How.XPATH, using = "(//span[text()='Date Submitted'])[1]")
	public WebElement SubmittedDate;

	@FindBy(how = How.XPATH, using = "(//span[text()='Submitter Comments'])[1]")
	public WebElement SubmittedComments;

	@FindBy(how = How.XPATH, using = "//button[text()='Approve']")
	public WebElement Approve;

	@FindBy(how = How.XPATH, using = "//button[text()='Reject']")
	public WebElement Reject;

	@FindBy(how = How.XPATH, using = "//button[text()='Reassign']")
	public WebElement Reassign;

	@FindBy(how = How.XPATH, using = "//span[text()='View Approval Request']")
	public WebElement Approvalrequestdetails;

	@FindBy(how = How.XPATH, using = "//span[text()='View Design Brief']")
	public WebElement DesignBriefDetails;

	@FindBy(how = How.XPATH, using = "(//span[@class='test-id__field-value slds-form-element__static slds-grow word-break-ie11'])[1]")
	public WebElement DesignBrief;

	@FindBy(how = How.XPATH, using = "//span[text()='Home']")
	public WebElement Home;

	@FindBy(how = How.XPATH, using = "//span[text()='SVO Private Office']")
	public WebElement PrivateOffice;

	@FindBy(how = How.XPATH, using = "//span[text()='Private Office Closed']")
	public WebElement PrivateOfficeclosed;

}
