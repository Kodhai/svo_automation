package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SSEC_1085_BillingAddressOnEnquiryContainer {

	@FindBy(how = How.XPATH, using = "//label[text()='Product Offering']//following-sibling::div//button")
	public WebElement EnquiryProdOfferDropDown;

	public String POSelection(String Value) {
		return "//span[text() = '" + Value + "']";
	}

	@FindBy(how = How.XPATH, using = "//label[text()='Client']//following-sibling::div//input")
	public WebElement EnquiryClientSearchBox;

	@FindBy(how = How.XPATH, using = "//table[@class='forceRecordLayout slds-table slds-table_cell-buffer slds-table_bordered uiVirtualDataGrid--default uiVirtualDataGrid']//a[@data-refid='recordId']")
	public WebElement EnquiryClientSelect;

	@FindBy(how = How.XPATH, using = "//label[text()='Region']//following-sibling::div//button")
	public WebElement EnquiryRegionDropDown;

	@FindBy(how = How.XPATH, using = "//label[text()='Sales Area']//following-sibling::div//button")
	public WebElement EnquirySalesAreaDropDown;

	@FindBy(how = How.XPATH, using = "//label[text()='Market']//following-sibling::div//button")
	public WebElement EnquiryMarketDropDown;

	@FindBy(how = How.XPATH, using = "//label[text()='Preferred Contact']//following-sibling::div//input")
	public WebElement EnquiryPreferredContactTextBox;

	@FindBy(how = How.XPATH, using = "//label[text()='Preferred Contact']//following-sibling::div//child::div//child::lightning-base-combobox//child::div//child::div[contains(@class,'slds-dropdown_left')]//child::lightning-base-combobox-item//child::span[@class='slds-media__body']//child::span[contains(@class,'slds-listbox__option-text_entity')]//span[contains(text(),'Show All Results for')]")
	public WebElement EnquiryPreferredContactSearchbar;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Programmes...']")
	public WebElement EnquiryProgrammeTextBox;

	@FindBy(how = How.XPATH, using = "//label[text()='Programme']//following-sibling::div//child::div//child::lightning-base-combobox//child::div//child::div[contains(@class,'slds-dropdown_left')]//child::lightning-base-combobox-item//child::span[@class='slds-media__body']//child::span[contains(@class,'slds-listbox__option-text_entity')]//span[contains(text(),'Show All Results for')]")
	public WebElement EnquiryProgrammeSearchbar;

	@FindBy(how = How.XPATH, using = "//label[text()='Vehicle']//following-sibling::div//input")
	public WebElement VehicleRecordTextBox;

	@FindBy(how = How.XPATH, using = "//div[@class='slds-listbox slds-listbox_vertical slds-dropdown slds-dropdown_fluid slds-dropdown_left slds-dropdown_length-with-icon-7']//lightning-base-combobox-formatted-text")
	public WebElement EnquiryVehicleRecord;

	@FindBy(how = How.XPATH, using = "//label[text()='Preferred Retailer']//following-sibling::div//input")
	public WebElement EnquiryPreferredRetailerTextBox;

	@FindBy(how = How.XPATH, using = "//label[text()='Preferred Retailer']//following-sibling::div//child::div//child::lightning-base-combobox//child::div//child::div[contains(@class,'slds-dropdown_left')]//child::lightning-base-combobox-item//child::span[@class='slds-media__body']//child::span[contains(@class,'slds-listbox__option-text_entity')]//span[contains(text(),'Show All Results for')]")
	public WebElement EnquiryPreferredRetailerSearchBar;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Edit Account Name']")
	public WebElement EditAccountNameBtn;
	
	@FindBy(how = How.XPATH, using = "//a[@class='deleteAction']//child::span[text()='Press Delete to Remove']")
	public WebElement ClearAccountName;
	
	@FindBy(how = How.XPATH, using = "//div[@class='footer ']//div[@class='button-container-inner slds-float_none']//button[@title='Save']//span[text()='Save']")
	public WebElement SaveContactNameBtn;
	
	@FindBy(how = How.XPATH, using = "//button[@name='SO_Closed_Reason__c']")
	public WebElement ClosedReasonDropdonList;
	
	@FindBy(how = How.XPATH, using = "//div//child::span[text()='Billing Address']//parent::div//following-sibling::div//child::span//child::span[@class='lvm-grid-no-fade-out undefined forceOutputFormulaHtml']")
	public WebElement BillingAddress;
	
	@FindBy(how = How.XPATH, using = "//lightning-formatted-text[text()='New']")
	public WebElement EnquiryStatusAsNew;
	
	@FindBy(how = How.XPATH, using = "//lightning-formatted-text[text()='Closed']")
	public WebElement EnquiryStatusAsClosedLost;


}
