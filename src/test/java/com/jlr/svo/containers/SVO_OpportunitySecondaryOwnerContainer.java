package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SVO_OpportunitySecondaryOwnerContainer {

	@FindBy(how = How.XPATH, using = "//a//child::span[text()='Qualified']")
	public WebElement OpportunityQualifiedStage;

	public String RecordTypeSelection(String recordtype) {
		return "//span[text() = '" + recordtype + "']";
	}

	@FindBy(how = How.XPATH, using = "//button[@name='Edit']")
	public WebElement EditOpportunityBtn;

	@FindBy(how = How.XPATH, using = "//button[@name='SaveEdit']")
	public WebElement SaveOpportunityBtn;

	@FindBy(how = How.XPATH, using = "//a//child::span[text()='Qualified']")
	public WebElement OpportunityEstimateStage;

	@FindBy(how = How.XPATH, using = "//button[@name='Delete']")
	public WebElement OpportunityDeleteBtn;

	@FindBy(how = How.XPATH, using = "//button[@title='Delete']")
	public WebElement OpportunityDeleteDoneBtn;

	@FindBy(how = How.XPATH, using = "//span[text()='Edit Secondry Owner']")
	public WebElement EditClassicOpportunityBtn;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search People...']")
	public WebElement SecondaryOwnerSearchBox;

	@FindBy(how = How.XPATH, using = "//label[text()='Secondry Owner']//following-sibling::div//child::div//child::lightning-base-combobox//child::div//child::div[contains(@class,'slds-dropdown_left')]//child::lightning-base-combobox-item//child::span[@class='slds-media__body']//child::span[contains(@class,'slds-listbox__option-text_entity')]//span[contains(text(),'Show All Results for')]")
	public WebElement SecondaryOwnerNameSearch;

	public String SecndaryOwnerSelection(String Value) {
		return "//a[text()='" + Value + "']";
	}

	@FindBy(how = How.XPATH, using = "//button[@name='SaveEdit']")
	public WebElement SaveBtn;

	@FindBy(how = How.XPATH, using = "//button[@name='CancelEdit']")
	public WebElement CancelBtn;

	@FindBy(how = How.XPATH, using = "//span[text()='Secondary Opportunities']/parent::a")
	public WebElement SecondaryOpportunities;

	@FindBy(how = How.XPATH, using = "//lightning-formatted-text[contains(text(),'Test_Classic')]")
	public WebElement ClassicOpportunityDetails;

	@FindBy(how = How.XPATH, using = "//div//child::label[text()='Secondry Owner']//parent::lightning-grouped-combobox//child::div//child::div//child::lightning-base-combobox//child::div//child::div//child::div//child::button//child::span[text()='Clear Selection']")
	public WebElement ClearSecondaryOwner;

	@FindBy(how = How.XPATH, using = "//div//child::span[text()='Secondry Owner']//parent::div//following-sibling::div//child::span//child::slot//child::force-lookup//child::div//child::records-hoverable-link//child::div//child::a//child::slot//child::span")
	public WebElement SecondaryOwnerNameField;

	@FindBy(how = How.XPATH, using = "//div//child::label[text()='SAP Account']//parent::lightning-grouped-combobox//child::div//child::div//child::lightning-base-combobox//child::div//child::div//child::div//child::button//child::span[text()='Clear Selection']")
	public WebElement ClearSapAccount;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Fully Paid']")
	public WebElement EditHandOverDetailsBtn;

	@FindBy(how = How.XPATH, using = "//span[text()='Fully Paid']//parent::label//following-sibling::input")
	public WebElement FullyPaidCheckBox;

	@FindBy(how = How.XPATH, using = "//span[text()='Heritage Certificate Requested']//parent::label//following-sibling::input")
	public WebElement HeritageCertificateRequestedCheckBox;

	@FindBy(how = How.XPATH, using = "//span[text()='FMS Passout Obtained']//parent::label//following-sibling::input")
	public WebElement FMSPassoutObtainedCheckBox;

	@FindBy(how = How.XPATH, using = "//span[text()='FMS Disposal Completed']//parent::label//following-sibling::input")
	public WebElement FMSDisposalCompletedCheckBox;

	@FindBy(how = How.XPATH, using = "//span[text()='Removed from E-Tracker']//parent::label//following-sibling::input")
	public WebElement RemovedFromETrackerCheckBox;

	@FindBy(how = How.XPATH, using = "//button[@title='Save']")
	public WebElement SaveHandOverDetails;

	@FindBy(how = How.XPATH, using = "//div//child::span[text()='Opportunity']//parent::div//following-sibling::div//child::span//child::div//child::a")
	public WebElement OpportunityLinkOnOrdersPage;

	@FindBy(how = How.XPATH, using = "//lightning-formatted-text[text()='Closed Won']")
	public WebElement OpportunityClosedWonStage;

	@FindBy(how = How.XPATH, using = "//div[@class='slds-media__body']//child::slot//child::lightning-formatted-text")
	public WebElement OpportunityName;

	@FindBy(how = How.XPATH, using = "//div[text()='Order']")
	public WebElement OrderNumber;

	@FindBy(how = How.XPATH, using = "//div[@aria-label='Path Header']//ul//li[@data-name='Order Placed']//a//span[text()='Order Placed']")
	public WebElement OrderPlacedStage;

	@FindBy(how = How.XPATH, using = "//span[text()='All Classic Opportunities']/parent::a")
	public WebElement AllClassicOpportunities;

	@FindBy(how = How.XPATH, using = "//a[text()='Secondry Owner']")
	public WebElement ErrorMEssageForBespokeUser;

	@FindBy(how = How.XPATH, using = "//lightning-formatted-text[text()='Closed Lost']")
	public WebElement OpportunityClosedLostStage;

	@FindBy(how = How.XPATH, using = "//div[@title='Edit']//parent::a")
	public WebElement OrderEditBtn;

	@FindBy(how = How.XPATH, using = "//div[@class='button-container-inner slds-float_right']//child::button[@title='Save']")
	public WebElement SaveOrderBtn;

}
