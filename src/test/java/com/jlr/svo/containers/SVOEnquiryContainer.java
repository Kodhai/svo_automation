package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SVOEnquiryContainer {

	@FindBy(how = How.XPATH, using = "(//span[text()='Enquiries'])[1]")
	public WebElement EnquiryButton;

	@FindBy(how = How.XPATH, using = "//*[@name='SO_Enquiry__c-search-input']")
	public WebElement EnquirySearchBox;

	@FindBy(how = How.XPATH, using = "//p[text()='No items to display.']")
	public WebElement NoItems;

	@FindBy(how = How.ID, using = "username")
	public WebElement userNameTextBox;
	
	@FindBy(how = How.XPATH, using = "//div[text()='Capture enquiry details for a potential Classic Sales Opportunity.']")
	public WebElement ClassicSalesRecType;

	@FindBy(how = How.ID, using = "password")
	public WebElement passwordTextBox;

	@FindBy(how = How.ID, using = "Login")
	public WebElement loginBtn;

	@FindBy(how = How.ID, using = "emc")
	public WebElement verificationCode;

	@FindBy(how = How.ID, using = "save")
	public WebElement verifyBtn;

	@FindBy(how = How.XPATH, using = "(//a[@title='Closed'])[4]")
	public WebElement clickOnClosed;

	@FindBy(how = How.XPATH, using = "(//div[@class='slds-form-element__control slds-grow']//child::input[@class='slds-input'])[2]")
	public WebElement vSearchVIN;

	@FindBy(how = How.XPATH, using = "//input[@name='Brands']")
	public WebElement vSearchBrand;

	@FindBy(how = How.XPATH, using = "//div[@class='slds-m-around_medium']")
	public WebElement vSearchVehicleNotFound;

	@FindBy(how = How.XPATH, using = "//button[text()='New']")
	public WebElement vSearchNewVehicle;

	@FindBy(how = How.XPATH, using = "(//input[@class=' input'])[1]")
	public WebElement newVehicleRecType;

	@FindBy(how = How.XPATH, using = "(//span[@class='slds-truncate'])[1]")
	public WebElement SVOText;

	@FindBy(how = How.ID, using = "p")
	public WebElement mobileNumText;

	@FindBy(how = How.LINK_TEXT, using = "Remind Me Later")
	public WebElement remindMeLater;

	@FindBy(how = How.LINK_TEXT, using = "I Don't Want to Register My Phone")
	public WebElement notRegister;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-show']")
	public WebElement menuBtn;

	@FindBy(how = How.XPATH, using = "//input[@class='slds-input']")
	public WebElement searchBox;

	public String selectMenu(String text) {
		return "//b[text()='" + text + "']";
	}

	@FindBy(how = How.XPATH, using = "//a[@title='Home']")
	public WebElement homePage;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Sign in')]")
	public WebElement outlookSignin;

	@FindBy(how = How.NAME, using = "loginfmt")
	public WebElement outlookUsername;

	@FindBy(how = How.NAME, using = "passwd")
	public WebElement outlookPassword;

	@FindBy(how = How.XPATH, using = "//input[@value='Next']")
	public WebElement outlookNext;

	@FindBy(how = How.XPATH, using = "//input[contains(@value,'Sign in')]")
	public WebElement outlookSignInBtn;

	@FindBy(how = How.XPATH, using = "//input[contains(@value,'Yes')]")
	public WebElement outlookConfirmYes;

	@FindBy(how = How.XPATH, using = "(//a[contains(text(),'Employee')])[1]")
	public WebElement outlookEmp;

	@FindBy(how = How.XPATH, using = "//img[@alt='Outlook']")
	public WebElement outlookIcon;

	@FindBy(how = How.XPATH, using = "(//div[contains(@aria-label,'noreply@salesforce.com')])[1]")
	public WebElement outlookEmail;

	@FindBy(how = How.XPATH, using = "//div[@class='PlainText']")
	public WebElement outlookVerificationCode;

	@FindBy(how = How.XPATH, using = "//input[@id='emc']")
	public WebElement vCodeTextBox;

	@FindBy(how = How.XPATH, using = "//span[text()='Recently Viewed']")
	public WebElement listView;

	@FindBy(how = How.XPATH, using = "(//span[text()='All Opportunities'])[1]")
	public WebElement allOpportunity;

	@FindBy(how = How.XPATH, using = "(//span[text()='All Enquiries'])[1]")
	public WebElement AllEnquiries;

	@FindBy(how = How.XPATH, using = "//span[@title='Owner Alias']")
	public WebElement Owner;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Closed (Lost)')]")
	public WebElement EnqclosedLostState;

	@FindBy(how = How.XPATH, using = "//span[@title='SLA Status']")
	public WebElement Status;

	@FindBy(how = How.XPATH, using = "//span[@title='JLR Retail Case Management']")
	public WebElement jlrText;

	@FindBy(how = How.XPATH, using = "//*[@class='icon noicon']")
	public WebElement icon_image;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Log Out')]")
	public WebElement Logout;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'New')]")
	public WebElement newEnquiryBtn;

	public String selectRecordType(String record) {
		return "//div[contains(text(),'" + record + "')]";
	}

	@FindBy(how = How.XPATH, using = "//input[@name='q']")
	public WebElement googletest;

	@FindBy(how = How.XPATH, using = "(//input[@name='btnK'])[2]")
	public WebElement gsearch;

	@FindBy(how = How.XPATH, using = "//span[text()='Next']")
	public WebElement nextRType;

	@FindBy(how = How.XPATH, using = "(//span[text()='Cancel'])[2]")
	public WebElement cancelRType;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input'])[2]")
	public WebElement enquiryTitle;

	@FindBy(how = How.XPATH, using = "(//button[contains(text(),'Save')])")
	public WebElement saveEnquiry;

	@FindBy(how = How.XPATH, using = "//a[@title='Home']")
	public WebElement backtoHome;

	@FindBy(how = How.XPATH, using = "//a[@title='Contacted']")
	public WebElement contactedTab;

	@FindBy(how = How.XPATH, using = "//span[text()='Mark as Current Enquiry Status']//parent::button")
	public WebElement MarkCurrentEnquiry;

	@FindBy(how = How.XPATH, using = "//div[@class='slds-align-middle slds-hyphenate']")
	public WebElement discoveryError;

	@FindBy(how = How.XPATH, using = "(//lightning-formatted-text[contains(text(),'New')])[1]")
	public WebElement enquiryStatus;

	@FindBy(how = How.XPATH, using = "//button[@title='Close']")
	public WebElement errorClose;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Enquiries')]")
	public WebElement enquiryTab;

	@FindBy(how = How.XPATH, using = "//a[@href='/lightning/o/SO_Enquiry__c/home']")
	public WebElement back2enquiryTab;

	@FindBy(how = How.XPATH, using = "(//span[@class='slds-truncate'])[20]")
	public WebElement status;

	@FindBy(how = How.XPATH, using = "//a[@id='detailTab__item']")
	public WebElement enqDetails;

	@FindBy(how = How.XPATH, using = "(//button[@title='Edit Email Address'])")
	public WebElement editEmail;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Enquiry Title']")
	public WebElement editEnqTitle;

	@FindBy(how = How.XPATH, using = "(//button[@title='Edit Email Address'])[2]")
	public WebElement POeditEmail;

	@FindBy(how = How.XPATH, using = "(//button[@title='Edit Email Address'])[3]")
	public WebElement ClSaleseditEmail;

	@FindBy(how = How.XPATH, using = "(//button[@title='Edit Email Address'])[4]")
	public WebElement ClaServiceeditEmail;

	@FindBy(how = How.XPATH, using = "(//button[@title='Edit Region'])[2]")
	public WebElement editRegion;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'in Users')]")
	public WebElement ChOwnerSearch;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Email_Address__c']")
	public WebElement emailIdTextBox;

	@FindBy(how = How.XPATH, using = "//span[contains(@title,'Potential Matches')]")
	public WebElement matcheslabel;

	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Save')]")
	public WebElement saveEmailBtn;

	@FindBy(how = How.XPATH, using = "(//button[text()='Select' and @class='slds-button slds-button_neutral'])[1]")
	public WebElement PotentialMatchSelect;

	@FindBy(how = How.XPATH, using = "//a[@id='activityTab__item']")
	public WebElement activityTab;

	@FindBy(how = How.XPATH, using = "//button[@title='Create new...']")
	public WebElement newLogCall;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[1]")
	public WebElement subjectDD;

	@FindBy(how = How.XPATH, using = "//lightning-base-combobox-formatted-text[@title='Call']")
	public WebElement callLog;

	@FindBy(how = How.XPATH, using = "//textarea[@class='textarea textarea uiInput uiInputTextArea uiInput--default uiInput--textarea']")
	public WebElement commentText;

	@FindBy(how = How.XPATH, using = "//input[@title='Search Contacts']")
	public WebElement name;

	@FindBy(how = How.XPATH, using = "//span[contains(text(), 'in Contacts')]")
	public WebElement nameSearch;

	public String clientNameSearch(String client) {
		return "(//a[@title='" + client + "'])[1]";
	}

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Save')])[5]")
	public WebElement saveLog;

	@FindBy(how = How.XPATH, using = "//input[@title='Search Vehicle Manufacturers']")
	public WebElement brand;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'in Vehicle Manufacturers')]")
	public WebElement brandSearch;

	public String brandSearch(String brand) {
		return "//a[@title='" + brand + "']";
	}

	public String NewProModelSearch(String model) {
		return "(//a[@title='" + model + "'])[2]";
	}

	@FindBy(how = How.XPATH, using = "//input[@title='Search Vehicle Models']")
	public WebElement model;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'in Vehicle Models')]")
	public WebElement modelSearch;

	@FindBy(how = How.XPATH, using = "//a[@title='Discovery']")
	public WebElement discoveryTab;

	@FindBy(how = How.XPATH, using = "(//button[@title='Show More'])")
	public WebElement Showkeyfield;

	@FindBy(how = How.XPATH, using = "//p[text()='Edit']")
	public WebElement Vehicleinfoedit;

	@FindBy(how = How.XPATH, using = "(//p[text()='Edit'])[2]")
	public WebElement POVehicleinfoedit;

	@FindBy(how = How.XPATH, using = "(//p[text()='Edit'])[3]")
	public WebElement claSalesVehicleinfoedit;

	@FindBy(how = How.XPATH, using = "(//p[text()='Edit'])[4]")
	public WebElement claServiceVehicleinfoedit;

	@FindBy(how = How.XPATH, using = "(//span[text()='Save'])[3]")
	public WebElement Savechanges;

	@FindBy(how = How.XPATH, using = "//a[@class='select'][1]")
	public WebElement proOffering;

	public String SourceSelection(String value) {
		return "(//a[contains(text(),'" + value + "')])[1]";
	}

	public String ownerSelection(String value) {
		return "(//a[contains(text(),'" + value + "')])[2]";
	}

	@FindBy(how = How.XPATH, using = "(//button[contains(text(),'View all dependencies')])[4]")
	public WebElement ViewallDependencies;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Region__c']")
	public WebElement regionDD;

	public String ValueSelection(String value) {
		return "//span[@title='" + value + "']";
	}

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Sales_Area__c']")
	public WebElement BespokesalesAreaDD;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Market__c']")
	public WebElement marketDD;

	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Apply')]")
	public WebElement Applybtn;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Programme']")
	public WebElement editProgrammeField;

	@FindBy(how = How.XPATH, using = "(//input[@class=' input'])[3]")
	public WebElement newVehicleVIN;

	@FindBy(how = How.XPATH, using = "(//button[@title='Save'])[2]")
	public WebElement newVehicleSaveBtn;

	@FindBy(how = How.XPATH, using = "(//a[contains(@href,'/lightning/r/a2C7')])[1]")
	public WebElement firstEnquiryRecentlyViewed;

	@FindBy(how = How.XPATH, using = "//span[text()='Enquiry Status']//parent::div")
	public WebElement enquiryStatusLabel;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Enquiry Status']")
	public WebElement editEnquiryStatus;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[3]")
	public WebElement enquiryStatusDD;

	@FindBy(how = How.XPATH, using = "//div[@class='slds-popover__body']//child::div[@class='pageLevelErrors']")
	public WebElement enquirySaveError;

	@FindBy(how = How.XPATH, using = "//button[@title='Close error dialog']")
	public WebElement enquiryErrorClose;

	@FindBy(how = How.XPATH, using = "//span[text()='Log a Call']")
	public WebElement LogCall;

	@FindBy(how = How.XPATH, using = "//button[@name='CancelEdit']")
	public WebElement chRecTypeCancel;

	@FindBy(how = How.XPATH, using = "(//input[@placeholder='Search Accounts...'])[2]")
	public WebElement retailer;

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'in Accounts')])[2]")
	public WebElement rAccountSearch;

	public String retailerAccountSearch(String retailer) {
		return "//a[@title='" + retailer + "']";
	}

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[21]")
	public WebElement originatingDiv;

	@FindBy(how = How.XPATH, using = "//span[text()='Vehicle Details']")
	public WebElement VehicleDetailsSection;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Programmes...']")
	public WebElement programmeField;

	@FindBy(how = How.XPATH, using = "(//a[@class='select'])[1]")
	public WebElement NewProgrammeStatus;

	@FindBy(how = How.XPATH, using = "(//a[@class='select'])[2]")
	public WebElement NewProgrammeProOffering;

	@FindBy(how = How.XPATH, using = "//input[@class='input uiInputSmartNumber']")
	public WebElement NewProgrammeBuildCapacity;

	@FindBy(how = How.XPATH, using = "//span[text()='Vehicle Information']")
	public WebElement vehicleInformationLabel;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[20]")
	public WebElement BespokesourceDD;

	@FindBy(how = How.XPATH, using = "(//span[text()='Mark Enquiry Status as Complete'])")
	public WebElement MarkEnquiryComplete;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Closed_State__c']")
	public WebElement closedState;

	@FindBy(how = How.XPATH, using = "//span[@title='Qualified - Opportunity']")
	public WebElement selectClosedState;

	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Done')]")
	public WebElement doneClosedState;

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Opportunities')])[4]")
	public WebElement Opportunitynav;

	@FindBy(how = How.XPATH, using = "(//a[contains(@class,'slds-truncate outputLookupLink slds-truncate')])[2]")
	public WebElement OpporName;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Recently Viewed')]")
	public WebElement RecentlyViewed;

	@FindBy(how = How.XPATH, using = "//lightning-formatted-text[text()='Success'] ")
	public WebElement SLAstatussuccess;

	@FindBy(how = How.XPATH, using = "//button[@title='Change Owner']")
	public WebElement ChangeOwner;

	@FindBy(how = How.XPATH, using = "//input[@title='Search Users']")
	public WebElement SearchUsers;

	@FindBy(how = How.XPATH, using = "//button[@name='change owner']")
	public WebElement ChangeOwnerbutton;

	@FindBy(how = How.XPATH, using = "//span[@class='toastMessage slds-text-heading--small forceActionsText']")
	public WebElement ChangeOwnererror;

	@FindBy(how = How.XPATH, using = "//input[@name='SendEmail']")
	public WebElement Ownersentmail;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-button_icon slds-modal__close closeIcon slds-button_icon-bare slds-button_icon-inverse']")
	public WebElement closeOwner;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Closed_Reason__c']")
	public WebElement Closedreason;

	public String EnquiryTitle(String Title) {
		return "//lightning-formatted-text[text()='" + Title + "']";
	}

	@FindBy(how = How.XPATH, using = "(//a[@class='toggle slds-th__action slds-text-link--reset '])[6]")
	public WebElement enquiryStatusColumn;

	@FindBy(how = How.XPATH, using = "(//a[contains(@href,'/lightning/r/')])[1]")
	public WebElement firstEnquiryAllList;

	@FindBy(how = How.XPATH, using = "(//span[@class='slds-grid slds-grid--align-spread'])[7]")
	public WebElement newEnquiryListView;

	@FindBy(how = How.XPATH, using = "(//a[contains(@href,'/lightning/r/Opportunity/')])")
	public WebElement OpportunityLinkEnqDetail;

	@FindBy(how = How.XPATH, using = "(//div[contains(@class,'emptyContentInner')]//child::p)[position()=3]")
	public WebElement noOpportunityErrorMsg;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[2]")
	public WebElement newEnqProOff;

	@FindBy(how = How.XPATH, using = "//span[text()='Select item 1']")
	public WebElement SelectItem1;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-button_icon-border-filled']")
	public WebElement Dropdown;

	@FindBy(how = How.XPATH, using = "//input[@id='thePage:theform:thePB:thePBB:j_id69']")
	public WebElement EmailSave;

	@FindBy(how = How.XPATH, using = "//input[@id='thePage:theform:thePB:thePBB:j_id68']")
	public WebElement EmailDrafts;

	@FindBy(how = How.XPATH, using = "//input[@name='thePage:theform:thePB:thePBB:j_id70']")
	public WebElement EmailCancel;

	@FindBy(how = How.XPATH, using = "(//input[@placeholder='Search Accounts...'])[1]")
	public WebElement newEnqClient;

	@FindBy(how = How.XPATH, using = "(//input[@placeholder='Search Contacts...'])[1]")
	public WebElement newEnqPreferredCnt;

	@FindBy(how = How.XPATH, using = "//input[@id='thePage:theform:thePB:thePBB:send']")
	public WebElement EmailSend;

	@FindBy(how = How.XPATH, using = "(//span[@title='Opportunities])[3]")
	public WebElement OpportunityLink;

	@FindBy(how = How.XPATH, using = "(//a[contains(@class,'slds-truncate outputLookupLink slds-truncate outputLookupLink')])")
	public WebElement SelectOpp;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Reference Number')]")
	public WebElement RefNumber;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[6]")
	public WebElement newEnqRegion;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[7]")
	public WebElement newEnqSalesArea;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[8]")
	public WebElement newEnqMarket;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Vehicle Manufacturers...']")
	public WebElement newEnqBrand;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Vehicle Models...']")
	public WebElement newEnqModel;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[21]")
	public WebElement newEnqSource;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[22]")
	public WebElement newEnqOriginatingDiv;

	@FindBy(how = How.XPATH, using = "//span[contains(@title,'Show All Results for')]")
	public WebElement newEnqClientSearch;

	@FindBy(how = How.XPATH, using = "(//span[contains(@title,'Show All Results for')])[2]")
	public WebElement newEnqPreferredCntSearch;

	@FindBy(how = How.XPATH, using = "(//span[contains(@title,'Show All Results for')])[3]")
	public WebElement newEnqPreferredRetailerSearch;

	@FindBy(how = How.XPATH, using = "(//span[contains(@title,'Show All Results for')])[4]")
	public WebElement newEnqBrandSearch;

	@FindBy(how = How.XPATH, using = "(//span[contains(@title,'Show All Results for')])[5]")
	public WebElement newEnqModelSearch;

	@FindBy(how = How.XPATH, using = "(//button[@title='Edit Closed State'])[1]")
	public WebElement editClosedState;

	@FindBy(how = How.XPATH, using = "(//a[@class='select'])[1]")
	public WebElement enqClosedStateDD;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[17]")
	public WebElement editClosedStateDD;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[16]")
	public WebElement editClosedStateDD4QuaOpp;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[17]")
	public WebElement editClosedStateDD4QuaOppReason;

	@FindBy(how = How.XPATH, using = "//ul[@class='errorsList slds-list_dotted slds-m-left_medium']")
	public WebElement closedStatusErrorMsg;

	@FindBy(how = How.XPATH, using = "(//span[text()='SLA Status'])[1]")
	public WebElement SLAStatusSection;

	@FindBy(how = How.XPATH, using = "(//button[@title='Edit SLA - First Contact Target'])")
	public WebElement editFirstContactTarget;

	@FindBy(how = How.XPATH, using = "(//input[@name='SO_SLA_First_Contact_Target__c'])[1]")
	public WebElement editFirstCntTargetDate;

	@FindBy(how = How.XPATH, using = "(//input[@name='SO_SLA_First_Contact_Target__c'])[2]")
	public WebElement editFirstCntTargetTime;

	@FindBy(how = How.XPATH, using = "(//span[@class='test-id__field-value slds-form-element__static slds-grow word-break-ie11 is-read-only']//child::lightning-formatted-text[text()='Failed'])[1]")
	public WebElement SLAFailedStatus;

	@FindBy(how = How.XPATH, using = "(//span[@class='test-id__field-value slds-form-element__static slds-grow word-break-ie11 is-read-only']//child::lightning-formatted-text[text()='In Progress'])[1]")
	public WebElement SLAInProgressStatus;

	@FindBy(how = How.XPATH, using = "//span[text()='Compose Email']")
	public WebElement ComposeEmail;

	@FindBy(how = How.XPATH, using = "//form[@id='thePage:theform']")
	public WebElement ComposeEmailbody;

	@FindBy(how = How.XPATH, using = "//input[@class='ui-autocomplete-input']")
	public WebElement emailto;

	@FindBy(how = How.XPATH, using = "//textarea[@id='thePage:theform:thePB:pbS:pbSI_to:to_addresses']")
	public WebElement Additionalto;

	@FindBy(how = How.XPATH, using = "//textarea[@id='thePage:theform:thePB:pbS:pbSI_cc:cc_addresses']")
	public WebElement CC;

	@FindBy(how = How.XPATH, using = "//textarea[@id='thePage:theform:thePB:pbS:pbSI_bcc:bcc_addresses']")
	public WebElement BCC;

	@FindBy(how = How.XPATH, using = "//select[@id='thePage:theform:thePB:pbS:pbSI_relto:relto_prefix']")
	public WebElement Relatedto;

	public String Relatedtooption(String Title) {
		return "//option[text()='" + Title + "']";
	}

	@FindBy(how = How.XPATH, using = "//select[@id='thePage:theform:thePB:pbS:j_id223:importance']")
	public WebElement Importance;

	@FindBy(how = How.XPATH, using = "//input[@id='thePage:theform:thePB:pbS:j_id230:email_subject']")
	public WebElement Subject;

	@FindBy(how = How.XPATH, using = "//body[@id='tinymce']")
	public WebElement Body;

	@FindBy(how = How.XPATH, using = "//input[@id='thePage:theform:thePB:thePBBb:send']")
	public WebElement sendemail;

	@FindBy(how = How.XPATH, using = "(//input[@value='Save'])[2]")
	public WebElement saveemail;

	@FindBy(how = How.XPATH, using = "(//input[@value='Cancel'])[2]")
	public WebElement cancelemail;

	@FindBy(how = How.XPATH, using = "(//a[@class='flex-wrap-ie11'])[7]")
	public WebElement KMILink;

	@FindBy(how = How.XPATH, using = "(//div[@title='New'])[2]")
	public WebElement NewKMIbtn;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Contacts...']")
	public WebElement Contacttxtbx;

	@FindBy(how = How.XPATH, using = "//input[@aria-autocomplete='none']")
	public WebElement ProductOffering;

	public String PrOfSelection(String ProductOffering) {
		return "//span[@title='" + ProductOffering + "']";
	}

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Vehicle Manufacturers...']")
	public WebElement Brandtxtbx;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Vehicle Models...']")
	public WebElement Modeltxtbx;

	@FindBy(how = How.XPATH, using = "//button[@name='SaveEdit']")
	public WebElement SaveKMI;

	@FindBy(how = How.XPATH, using = "(//a[@role='button'])[16]")
	public WebElement EditKMIdrpdn;

	@FindBy(how = How.XPATH, using = "//a[@title='Edit']")
	public WebElement EditKMIBtn;

	@FindBy(how = How.XPATH, using = "(//span[@class='slds-checkbox--faux'])[2]")
	public WebElement KMIcheckbx;

	@FindBy(how = How.XPATH, using = "//div[@title='Change Owner']")
	public WebElement ChangeOwnerBtn;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Users...']")
	public WebElement SearchUsersTxtbx;

	@FindBy(how = How.XPATH, using = "//button[@title='Submit']")
	public WebElement SubmitOwnerBtn;

	@FindBy(how = How.XPATH, using = "(//input[@value='Select Template'])[1]")
	public WebElement SelectTemplate;

	@FindBy(how = How.XPATH, using = "(//a[contains(text(),'15Shift')])[1]")
	public WebElement Template1;

	@FindBy(how = How.XPATH, using = "(//input[@value='Drafts'])[1]")
	public WebElement Drafts;

	@FindBy(how = How.XPATH, using = "//td[contains(text(),'Retrieve Draft')]")
	public WebElement RetrieveDrafts;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Test Mail')]")
	public WebElement SelectDrafts;

	public String SelectDrafts(String Draft) {
		return "//a[contains(text(),'" + Draft + "')]";

	}

	@FindBy(how = How.XPATH, using = "//select[@id='thePage:theform:thePB:pbS:j_id122:business_unit']")
	public WebElement businessUnit;

	@FindBy(how = How.XPATH, using = "//a[@id='thePage:theform:thePB:pbS:j_id126:html_switch']")
	public WebElement emailFormat;

	@FindBy(how = How.XPATH, using = "//select[@id='thePage:theform:thePB:pbS:fromblock:from_address']")
	public WebElement emailFrom;

	@FindBy(how = How.XPATH, using = "(//a[@title='Closed'])")
	public WebElement closeStageBtn;

	@FindBy(how = How.XPATH, using = "//div[contains(@class,'slds-notify--toast forceToastMessage')]")
	public WebElement SLAInProgressErrorMsg;

	@FindBy(how = How.XPATH, using = "(//span[text()='All Enquiries SLA Breached'])[1]")
	public WebElement SLABreachEnquiry;

	@FindBy(how = How.XPATH, using = "//span[text()='Show filters']")
	public WebElement filterOnSLABreach;

	@FindBy(how = How.XPATH, using = "//div[@class='forceFilterPanelScope']")
	public WebElement filterByOwner;

	@FindBy(how = How.XPATH, using = "//span[text()='My enquiries']")
	public WebElement myEnquiryOption;

	@FindBy(how = How.XPATH, using = "//span[text()='Done']")
	public WebElement filterDoneButton;

	@FindBy(how = How.XPATH, using = "//button[text()='Save']")
	public WebElement filterSaveButton;

	@FindBy(how = How.XPATH, using = "//button[@title='Close Filters']")
	public WebElement FilterCloseBtn;

	@FindBy(how = How.XPATH, using = "(//h2[text()='Related List Quick Links'])[4]")
	public WebElement relatedQuickList;

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Enquiry History')])[1]")
	public WebElement enquiryHistoryLink;

	@FindBy(how = How.XPATH, using = "(//input[@value='Attach File'])[1]")
	public WebElement enquiryAttachFile;

	@FindBy(how = How.XPATH, using = "//span[@id='theErrorPage:theError']")
	public WebElement attachFileError;

	@FindBy(how = How.XPATH, using = "//a[@title='New' and @class='tabHeader slds-path__link']")
	public WebElement newEnquiryStatus;

	@FindBy(how = How.XPATH, using = "(//span[text()='Change Record Type'])[1]")
	public WebElement changeRecordTypeBtn;

	@FindBy(how = How.XPATH, using = "(//div[@class='recordTypeName slds-grow slds-truncate'])[2]//child::span")
	public WebElement recordTypeVerify;

	@FindBy(how = How.XPATH, using = "//ul[@class='errorsList slds-list_dotted slds-m-left_medium']")
	public WebElement chRecordTypeErr;

	@FindBy(how = How.XPATH, using = "//span[text()='Vehicle Search']")
	public WebElement vehicleSearch;

	@FindBy(how = How.XPATH, using = "(//div[@class='slds-form-element__control slds-grow']//child::input[@class='slds-input'])[1]")
	public WebElement vSearchRegistrationNumber;

	@FindBy(how = How.XPATH, using = "//button[text()='Search']")
	public WebElement vSearchSearchBtn;

	@FindBy(how = How.XPATH, using = "//button[@title='Select']")
	public WebElement vSearchSelect;

	@FindBy(how = How.XPATH, using = "(//span[text()='Vehicle'])[1]")
	public WebElement vehicleDetails;

	@FindBy(how = How.XPATH, using = "(//a[contains(@href,'/lightning/r/') and @class='flex-wrap-ie11']//child::span)[2]")
	public WebElement verifyVehicleDetails;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Subject__c']")
	public WebElement EnquiryTitle;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Contacts...']")
	public WebElement PreferredContact;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Show All Results for')]")
	public WebElement CSpCntSearch;

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Show All Results for')])[2]")
	public WebElement PCntSearch;

	@FindBy(how = How.XPATH, using = "(//input[@placeholder='Search Accounts...'])[1]")
	public WebElement Client;

	@FindBy(how = How.XPATH, using = "//button[@name='SaveEdit']")
	public WebElement SaveEnquiry;

	@FindBy(how = How.XPATH, using = "(//button[@title='Edit Client'])")
	public WebElement EditClient;

	@FindBy(how = How.XPATH, using = "(//span[text()='Client'])[2]")
	public WebElement clientLabel;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Preferred Contact']")
	public WebElement EditPreferredContact;

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Please select the contact in the Contact Details section.')])")
	public WebElement Errorencountered;

	@FindBy(how = How.XPATH, using = "(//input[@title='Search contacts...'])[1]")
	public WebElement ContactDetails;

	@FindBy(how = How.XPATH, using = "//div[@class='toastContent slds-notify__content']")
	public WebElement CSCloseStateError;

	@FindBy(how = How.XPATH, using = "(//div[@class='slds-tabs_card'])[3]")
	public WebElement ActivityWindow;

	@FindBy(how = How.XPATH, using = "//a[text()='Add Additional Related To']")
	public WebElement AdditionalRelated;

	@FindBy(how = How.XPATH, using = "//select[@name='thePage:theform:thePB:pbS:repeater:0:relto_prefix2']")
	public WebElement RelatedTo;

	@FindBy(how = How.XPATH, using = "//input[@id='thePage:theform:thePB:pbS:repeater:0:relto_name']")
	public WebElement AddContact;

	@FindBy(how = How.XPATH, using = "(//input[@value='Save'])[1]")
	public WebElement SaveEmail;

	@FindBy(how = How.XPATH, using = "(//span[@title='Opportunities'])[2]")
	public WebElement OpportunitiesLink;

	public String OppSearch(String enquiryTitle) {
		return "//a[@title='" + enquiryTitle + "']";
	}

	@FindBy(how = How.XPATH, using = "//records-formula-output[@data-output-element-id='output-field']")
	public WebElement ReferenceNumber;

	@FindBy(how = How.XPATH, using = "(//span[@class='title slds-path__title' and text()='Closed'])[4]")
	public WebElement closedLostState;

	@FindBy(how = How.XPATH, using = "//a[@id='customTab__item']")
	public WebElement EmailTab;

	@FindBy(how = How.XPATH, using = "//button[text()='e2a Send an Email']")
	public WebElement SendanEmail;

	@FindBy(how = How.XPATH, using = "//textarea[@id='thePage:theform:thePB:pbS:pbSI_to:to_addresses']")
	public WebElement toemail;

	@FindBy(how = How.XPATH, using = "//button[@name='ChangeOwnerOne']")
	public WebElement EnqChangeOwner;

	@FindBy(how = How.XPATH, using = "//span[text()='Submit']")
	public WebElement SubmitOwner;

	@FindBy(how = How.XPATH, using = "(//a[@title='Closed'])")
	public WebElement closedBtn;

	@FindBy(how = How.XPATH, using = "(//span[text()='Closed (Lost)'])")
	public WebElement closeState;

	@FindBy(how = How.XPATH, using = "//input[@value='Close']")
	public WebElement closedraft;

	@FindBy(how = How.XPATH, using = "(//a[contains(@class,'slds-truncate outputLookupLink slds-truncate outputLookupLink')])[1]")
	public WebElement selectEnquiry;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Enquiry__c-search-input']")
	public WebElement SearchBox;

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'KMIs')])[1]")
	public WebElement KMI;

	@FindBy(how = How.XPATH, using = "(//div[@title='New'])[2]")
	public WebElement NewKMI;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Contacts...']")
	public WebElement KMIContact;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[2]")
	public WebElement KMIproduct;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Vehicle Manufacturers...']")
	public WebElement KMIbrand;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Vehicle Models...']")
	public WebElement KMImodel;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search KMIs...']")
	public WebElement sourceKMI;

	@FindBy(how = How.XPATH, using = "//button[@name='Delete']")
	public WebElement DeleteKMIBtn;

	@FindBy(how = How.XPATH, using = "//button[@title='Delete']")
	public WebElement DeletePopUp;

	@FindBy(how = How.XPATH, using = "(//a[@data-refid='recordId'])[1]")
	public WebElement FirstKMI;

	@FindBy(how = How.XPATH, using = "//li[@title='Related']")
	public WebElement RelatedTab;

	@FindBy(how = How.XPATH, using = "//button[@name='New']")
	public WebElement NewBtn;

	@FindBy(how = How.XPATH, using = "//button[@name='SaveEdit']")
	public WebElement SaveKMIenq;

	@FindBy(how = How.XPATH, using = "//a[@class='flex-wrap-ie11 slds-truncate']")
	public WebElement FirstEnqKMI;

	@FindBy(how = How.XPATH, using = "//button[@name='ChangeOwnerOne']")
	public WebElement ChangeOwnerEnq;

	@FindBy(how = How.XPATH, using = "//span[text()='Show Actions']")
	public WebElement KMIdropdown;

	@FindBy(how = How.XPATH, using = "//div[@title='Delete']")
	public WebElement KMIdelete;

	@FindBy(how = How.XPATH, using = "//button[@title='Delete']")
	public WebElement KMIdeleteconfirm;

	@FindBy(how = How.XPATH, using = "(//a[contains(@class,'slds-truncate outputLookupLink slds-truncate outputLookupLink-')])[1]")
	public WebElement SelectKMI;

	@FindBy(how = How.XPATH, using = "(//a[@id='relatedListsTab__item'])[2]")
	public WebElement KMIRelated;

	@FindBy(how = How.XPATH, using = "//button[@name='New']")
	public WebElement KMIEnquiry;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Subject__c']")
	public WebElement KMIEnquiryTitle;

	@FindBy(how = How.XPATH, using = "//a[@title='Closed']")
	public WebElement EnqClosed;

	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Cancel')]")
	public WebElement EnqCancel;

	@FindBy(how = How.XPATH, using = "(//button[@title='Cancel'])")
	public WebElement cancelEditBtn;

	@FindBy(how = How.XPATH, using = "//a[starts-with(@href,'/lightning/r/ortoo_e2a__EmailMessage__c/')]")
	public WebElement EnqEmails;

	@FindBy(how = How.XPATH, using = "//div[@title='e2a Send an Email']")
	public WebElement e2aEmail;

	@FindBy(how = How.XPATH, using = "(//span[@class='test-id__field-value slds-form-element__static slds-grow word-break-ie11 is-read-only'])[7]//child::slot//child::slot//child::slot//child::lightning-formatted-text")
	public WebElement SLAKickOffDate;

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'e2a Email')])[1]")
	public WebElement Opene2aEmail;

	@FindBy(how = How.XPATH, using = "(//a[contains(@class,'slds-truncate outputLookupLink slds-truncate outputLookupLink')])[1]")
	public WebElement OpenEmail;

	@FindBy(how = How.XPATH, using = "(//input[@value='Edit'])[1]")
	public WebElement EditEmail;

	@FindBy(how = How.XPATH, using = "//select[@id='page:form:thepb:informationBlock:j_id191:j_id193']")
	public WebElement EmailImp;

	@FindBy(how = How.XPATH, using = "(//input[@value='Save'])[1]")
	public WebElement SaveChanges;

	@FindBy(how = How.XPATH, using = "(//input[@value='Reply'])[1]")
	public WebElement ReplyEmail;

	@FindBy(how = How.XPATH, using = "(//input[@value='Forward'])[1]")
	public WebElement ForwardEmail;

	@FindBy(how = How.XPATH, using = "(//iframe[@title='accessibility title'])[2]")
	public WebElement EmailFrame;

	@FindBy(how = How.XPATH, using = "//option[@value='0']")
	public WebElement FirstCheckbox;

	@FindBy(how = How.XPATH, using = "(//input[@value='>>'])[1]")
	public WebElement ToArrowBtn;

	@FindBy(how = How.XPATH, using = "//input[@value='OK']")
	public WebElement OkBtn;

	@FindBy(how = How.XPATH, using = "//span[@class='slds-float_right']")
	public WebElement SearchTO;

	@FindBy(how = How.XPATH, using = "//input[@name='os_username']")
	public WebElement jirauser;

	@FindBy(how = How.XPATH, using = "//input[@name='os_password']")
	public WebElement jirauserpass;

	@FindBy(how = How.XPATH, using = "//input[@name='login']")
	public WebElement jirauserlogin;

	@FindBy(how = How.XPATH, using = "//input[@id='quickSearchInput']")
	public WebElement jiraSearchBox;

	@FindBy(how = How.XPATH, using = "//span[@title='The test run has passed']")
	public WebElement passexe;

	@FindBy(how = How.XPATH, using = "(//span[@title='The test run has not started'])")
	public WebElement toDoStatus;

	@FindBy(how = How.XPATH, using = "(//span[@title='The test run has failed'])")
	public WebElement failedStatus;

	@FindBy(how = How.XPATH, using = "//span[@title='The test run is currently being executed']")
	public WebElement executingStatus;

	@FindBy(how = How.XPATH, using = "//span[@title='The test run was aborted']")
	public WebElement abortedStatus;

	@FindBy(how = How.XPATH, using = "//span[@title='This Test is awaiting further validation before being run']")
	public WebElement pendingValidationStatus;

	@FindBy(how = How.XPATH, using = "//input[@id='projects-filter-field']")
	public WebElement filterAllProjects;

	@FindBy(how = How.XPATH, using = "//textarea[@id='status-filter-textarea']")
	public WebElement statusCol;

	@FindBy(how = How.XPATH, using = "//h4[@id='testrun-div-filter']")
	public WebElement filterText;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Classic Sales']")
    public WebElement ClassicSalesEnquiry;
	
	@FindBy(how = How.XPATH, using = "//p[text()='THANK YOU FOR YOUR ENQUIRY ']")
    public WebElement ThankYouenquirymsg;

    @FindBy(how = How.XPATH, using = "//span[text()='test']")
    public WebElement VerifyAccount;
    


}
