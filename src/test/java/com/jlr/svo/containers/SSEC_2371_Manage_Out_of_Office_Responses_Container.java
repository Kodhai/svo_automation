package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SSEC_2371_Manage_Out_of_Office_Responses_Container {

	@FindBy(how = How.XPATH, using = "(//div[@class='milestoneTimerText ontrackTimer'])[2]")
	public WebElement milestone;

	@FindBy(how = How.XPATH, using = "(//input[@type='text'])[1]")
	public WebElement firstname;

	@FindBy(how = How.XPATH, using = "(//input[@type='text'])[2]")
	public WebElement lastname;

	@FindBy(how = How.XPATH, using = "(//input[@type='text'])[3]")
	public WebElement Email;

	@FindBy(how = How.XPATH, using = "(//input[@type='text'])[4]")
	public WebElement Phone;

	@FindBy(how = How.XPATH, using = "(//input[@type='text'])[5]")
	public WebElement VINno;

	@FindBy(how = How.XPATH, using = "(//textarea[@name='description'])")
	public WebElement Description;

	@FindBy(how = How.XPATH, using = "//select[@name='recordType']")
	public WebElement selectanyqueries;

	@FindBy(how = How.XPATH, using = "//select[@title='Country']")
	public WebElement Country;

	@FindBy(how = How.XPATH, using = "//input[@type='submit']")
	public WebElement Submit;

	@FindBy(how = How.XPATH, using = "//button[@title='Select a List View']")
	public WebElement Selectalistview;

	@FindBy(how = How.XPATH, using = "//span[text()='Parts & Technical - All Cases']")
	public WebElement partstechnical;

	@FindBy(how = How.XPATH, using = "(//div[@class='slds-form-element slds-hint-parent test-id__output-root slds-form-element_readonly slds-form-element_stacked'])[1]")
	public WebElement Casenumbr;

	@FindBy(how = How.XPATH, using = "//a[@data-refid='recordId']")
	public WebElement Firstrecord;

	@FindBy(how = How.XPATH, using = "(//a[@role='button'])[1]")
	public WebElement SignIn;

	@FindBy(how = How.XPATH, using = "//input[@type='email']")
	public WebElement Username;

	@FindBy(how = How.XPATH, using = "//input[@type='submit']")
	public WebElement outlookNext;

	@FindBy(how = How.XPATH, using = "//input[@name='passwd']")
	public WebElement gmailPassword;

	@FindBy(how = How.XPATH, using = "//span[text()='New email']")
	public WebElement NewEmail;

	@FindBy(how = How.XPATH, using = "(//div[@role='textbox'])[1]")
	public WebElement MailToTextBox;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Add a subject']")
	public WebElement GmailSubjectTextBox;

	@FindBy(how = How.XPATH, using = "(//div[@role='textbox'])[3]")
	public WebElement GmailBodyTextBox;

	@FindBy(how = How.XPATH, using = "//span[text()='Send']")
	public WebElement SendMailBtn;

	@FindBy(how = How.XPATH, using = "//div[@id='mectrl_headerPicture']")
	public WebElement Signout;

	@FindBy(how = How.XPATH, using = "//a[text()='Sign out']")
	public WebElement Signoutbtn;

	@FindBy(how = How.XPATH, using = "//div[@title='Inbox']")
	public WebElement Inbox;

	@FindBy(how = How.XPATH, using = "(//div[@role='option'])[1]")
	public WebElement firstmail;

	@FindBy(how = How.XPATH, using = "//span[text()='Customer Service - All Cases']")
	public WebElement CustomerServiceAllCases;
}
