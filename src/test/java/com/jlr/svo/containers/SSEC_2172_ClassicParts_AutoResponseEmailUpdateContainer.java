package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SSEC_2172_ClassicParts_AutoResponseEmailUpdateContainer {

	@FindBy(how = How.XPATH, using = "//a[@data-label='Activity']")
	public WebElement ActivityTab;
	
	@FindBy(how=How.XPATH, using = "//span[text()='Compose']")
	public WebElement ComposeBtn;
	
	@FindBy(how=How.XPATH, using = "//a[@class='slds-button slds-button--icon slds-p-top--xxx-small slds-p-left--xxx-small slds-p-right--xxx-small']")
	public WebElement InsertTemplateBtn;
	
	@FindBy(how=How.XPATH, using = "//a[@title='SO Classic parts web to case default response for Classic Parts and Technical']")
	public WebElement TemplateSelectionOption;
	
	@FindBy(how=How.XPATH, using = "//span[text()='Send']//parent::button")
	public WebElement SendEmailBtn;
	
	@FindBy(how=How.XPATH, using = "//body[@class='cke_editable cke_editable_themed cke_contents_ltr cke_show_borders']")
	public WebElement BodyEmail;
	
	@FindBy(how=How.XPATH, using = "//span[starts-with(text(), 'Emails')]//parent::slot//parent::a")
	public WebElement EmailsQuickLink;
	
	@FindBy(how=How.XPATH, using = "//a[starts-with(@title, 'Contact Form')]")
	public WebElement EmailRecord;
	
//	@FindBy(how=How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how=How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how=How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how=How.XPATH, using = "")
//	public WebElement 
}
