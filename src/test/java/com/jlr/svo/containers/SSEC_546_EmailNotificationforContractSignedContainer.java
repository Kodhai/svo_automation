package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SSEC_546_EmailNotificationforContractSignedContainer {

	@FindBy(how = How.XPATH, using = "//div[@class='slds-path__scroller uiScroller scroller-wrapper scroll-horizontal']//a[@title='Contract Signed']")
	public WebElement ContractSignedtab;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-button--brand slds-path__mark-complete stepAction active uiButton']")
	public WebElement MarkAscurrentStatusButton;

	@FindBy(how = How.XPATH, using = "(//div[contains(@title,'Contract Agreement')])[1]")
	public WebElement ContractSignedConfirmation;

	@FindBy(how = How.XPATH, using = "//button[text()='Send Sales Agreement']")
	public WebElement sendSalesAgreementbutton;

	@FindBy(how = How.XPATH, using = "(//div//div//button[text()='View all dependencies'])[4]")
	public WebElement ViewAllDependencies;

	@FindBy(how = How.XPATH, using = "//button[@name='apply']")
	public WebElement ApplyButton;

	public String RegionSelection(String region) {
		return "(//span[@title = '" + region + "'])[1]";
	}

	@FindBy(how = How.XPATH, using = "(//label[text()='Region']//following-sibling::div//button)[2]")
	public WebElement SVOBespokeRegion;

	@FindBy(how = How.XPATH, using = "//span[text()='Source Information']")
	public WebElement SourceInformation;

	@FindBy(how = How.XPATH, using = "//span[text()='Tools & Settings']")
	public WebElement AgreementdraftText;

	@FindBy(how = How.XPATH, using = "//iframe[@title='Conga Composer']")
	public WebElement AdobeFrame;

	@FindBy(how = How.XPATH, using = "//b[text()='Merge & Adobe Sign']")
	public WebElement MergeAndSignButton;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Enter email addresses separated by commas']")
	public WebElement emailCCtextbox;

	@FindBy(how = How.XPATH, using = "//a[text()='OK']")
	public WebElement OkConfirmationButton;

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Signature reques')])[1]")
	public WebElement AdobesignFirstEmail;

	@FindBy(how = How.XPATH, using = "//button[text()='Continue']")
	public WebElement ContinueButton;

	@FindBy(how = How.XPATH, using = "//button[@data-analytics-click='esign']")
	public WebElement ClickToSignButton;

	@FindBy(how = How.XPATH, using = "//input[@name='Comm1']")
	public WebElement ConfirmCheckbox;

	@FindBy(how = How.XPATH, using = "//input[@name='Comm2']")
	public WebElement ConfirmTermsCheckbox;

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Contract Agreement for')])[1]")
	public WebElement SignedConfirmationEmail;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Secondary Owner']")
	public WebElement EditClassicOpportunityBtn;

	@FindBy(how = How.XPATH, using = "//span[contains(@title,'Show All Results for')]")
	public WebElement SecondaryOwnerNameSearch;

	@FindBy(how = How.XPATH, using = "//div[@style='word-wrap: break-word;']")
	public WebElement InvalidEmailCCErrorMsg;

}
