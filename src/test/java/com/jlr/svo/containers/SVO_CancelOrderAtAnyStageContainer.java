package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SVO_CancelOrderAtAnyStageContainer {

	@FindBy(how = How.XPATH, using = "//a[@title='Outcome']")
	public WebElement OrderOutcome;

	@FindBy(how = How.XPATH, using = "//span[text()='Mark as Current Status']//parent::button")
	public WebElement MarkStatusAsComplete;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Mark as Current Stage']//parent::button")
	public WebElement MarkStageAsComplete;

	public String OrderOucomeStageSelection(String Value) {
		return "//span[@title = '" + Value + "']";
	}

	@FindBy(how = How.XPATH, using = "//div//span[text()='Status']//parent::div//following-sibling::div//child::span//child::span")
	public WebElement OrderStatus;

	@FindBy(how = How.XPATH, using = "//div//span[text()='Outcome']//parent::div//following-sibling::div//child::span//child::span")
	public WebElement OrderOutcomeStatus;

	@FindBy(how = How.XPATH, using = "//span[text()='Edit Outcome']//parent::button")
	public WebElement EditOutcomeBtn;

	@FindBy(how = How.XPATH, using = "//span[text()='Outcome']//parent::span//following-sibling::div//child::div//child::div//child::div//child::a")
	public WebElement OutcomeDropdown;

	public String OrderOucomeStage(String Value) {
		return "//a[@title = '" + Value + "']";
	}

	@FindBy(how = How.XPATH, using = "//div[@class='footer active']//child::span[text()='Save']")
	public WebElement SaveOrder;

	@FindBy(how = How.XPATH, using = "//li[text()='You cannot change the outcome reason once set.']")
	public WebElement ErrorMessageOnOrdersPage;

	@FindBy(how = How.XPATH, using = "//div[@class='footer active']//child::span[text()='Cancel']")
	public WebElement CancelOrder;

	@FindBy(how = How.XPATH, using = "//span[text()='Outcome']")
	public WebElement StatusOutcome;

	@FindBy(how = How.XPATH, using = "//span[text()='Cancelled by Customer']")
	public WebElement OutcomeCancelledByCustomer;

	@FindBy(how = How.XPATH, using = "//a[@title='Edit']")
	public WebElement EditOrder;

	@FindBy(how = How.XPATH, using = "(//a[@class='select'])[1]")
	public WebElement EditStatus;

	@FindBy(how = How.XPATH, using = "//a[text()='Outcome']")
	public WebElement SelectOutcome;

	@FindBy(how = How.XPATH, using = "(//a[@class='select'])[2]")
	public WebElement EditOutcome;

	@FindBy(how = How.XPATH, using = "//a[text()='Retailed']")
	public WebElement SelectRetailed;

	@FindBy(how = How.XPATH, using = "(//a[contains(@href,'/lightning/r/') and @data-refid='recordId'])[2]")
	public WebElement OpportunityLink;

	@FindBy(how = How.XPATH, using = "//span[text()='Closed Won']")
	public WebElement CheckStage;

	@FindBy(how = How.XPATH, using = "//div[@class='footer active']//button[@title='Cancel']")
	public WebElement OrderOucomeCancelButton;

	@FindBy(how = How.XPATH, using = "//div[@class='disabled uiMenu']")
	public WebElement DisabledOutcomeDropDown;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Outcome']")
	public WebElement EditOutcomeIcon;

	@FindBy(how = How.XPATH, using = "//span[text()='Status']//parent::span//following-sibling::div//a[text()='Outcome']")
	public WebElement OrderStatusOutcomeDropDown;

	@FindBy(how = How.XPATH, using = "//div[@class='select-options']//li//a[@title='Built']")
	public WebElement OrderStatusBuilt;

	@FindBy(how = How.XPATH, using = "//div[@class='footer active']//button[@title='Save']")
	public WebElement SaveOrderStatusButton;

	@FindBy(how = How.XPATH, using = "//span[text()='Review the following errors']")
	public WebElement OrderStatusErrorMessage;

	@FindBy(how = How.XPATH, using = "//li[text()='Please enter Common Order Number to proceed further']")
	public WebElement CancelErrorMsg;

	@FindBy(how = How.XPATH, using = "//li[text()='You cannot change the outcome reason once set.']")
	public WebElement RetailErrorMsg;

	@FindBy(how = How.XPATH, using = "//span[text()='Classic Service']")
	public WebElement RecordType;

	@FindBy(how = How.XPATH, using = "//span[text()='Next']")
	public WebElement NextBtn;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Retail_Price__c']")
	public WebElement RetailPrice;

	@FindBy(how = How.XPATH, using = "(//label[contains(text(),'VAT Qualifying')]//following-sibling::div//child::lightning-base-combobox//child::div//child::div)[1]")
	public WebElement VATDropdown;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Records...']")
	public WebElement Vehicle;

	@FindBy(how = How.XPATH, using = "//div[@class='button-container-inner slds-float_right']//child::button[@title='Cancel']//child::span[text()='Cancel']")
	public WebElement QuoteCancelBtn;

	@FindBy(how = How.XPATH, using = "//a//span[contains(text(),'Closed')]")
	public WebElement OpportunityStage;

	@FindBy(how = How.XPATH, using = "//span[text()='Lost Reason']//parent::div//following-sibling::div//span//slot//lightning-formatted-text")
	public WebElement LostReasonOnOpportunityPage;

	
	
}
