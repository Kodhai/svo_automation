package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SSEC_1565_ClassicSalesAndServiceCaseRecordTypes_Container {

	@FindBy(how = How.XPATH, using = "//button[@name='ChangeRecordType']")
	public WebElement ChangeRecordTypeButton;

	@FindBy(how = How.XPATH, using = "//button[@title='Next']")
	public WebElement ChangeRecordTypeNextButton;

	@FindBy(how = How.XPATH, using = "//button[@title='Save']")
	public WebElement ChangeRecordTypeSaveButton;

	@FindBy(how = How.XPATH, using = "//div[@title='Delete']")
	public WebElement CaseDeleteButton;

	@FindBy(how = How.XPATH, using = "//h1//lightning-formatted-text[@slot='primaryField']")
	public WebElement CaseNumberField;

	@FindBy(how = How.XPATH, using = "//span[@class='triggerLinkText selectedListView slds-page-header__title slds-truncate slds-p-right--xx-small uiOutputText']")
	public WebElement SelectOpportunityListView;

	@FindBy(how = How.XPATH, using = "(//li//span[text()='All cases'])[1]")
	public WebElement AllCasesListView;

	@FindBy(how = How.XPATH, using = "//div[@class='forceVirtualActionMarker forceVirtualAction']")
	public WebElement CaseMoreActionsButton;

	@FindBy(how = How.XPATH, using = "//input[@name='Case-search-input']")
	public WebElement CasesSearchTextBox;

	@FindBy(how = How.XPATH, using = "//button[@title='Delete']")
	public WebElement CaseDeleteConfirmButton;

	@FindBy(how = How.XPATH, using = "//div[@class='toastContent slds-notify__content']//span[contains(text(),'was deleted.')]")
	public WebElement DeleteConfirmMessage;

	@FindBy(how = How.XPATH, using = "//button[@name='ChangeOwnerOne']")
	public WebElement ChangeOwnerButton;

	@FindBy(how = How.XPATH, using = "//button[@title='Submit']")
	public WebElement ChangeOwnerSubmitButton;

	@FindBy(how = How.XPATH, using = "//div[@class='contentWrapper slds-box--border']//input")
	public WebElement SearchUserTxtBx;

	@FindBy(how = How.XPATH, using = "//div[@class='undefined lookup__menu uiAbstractList uiAutocompleteList uiInput uiAutocomplete uiInput--default uiInput--lookup']//span[contains(@title,'in Users')]")
	public WebElement UserSearchBar;

	public String OwnerSelection(String User) {
		return "//a[text()='" + User + "']";
	}

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Before you can transfer this record, the new owner needs Read permission on it and related records.')]")
	public WebElement ChangeOwnerErrorMessage;

	@FindBy(how = How.XPATH, using = "//button[@title='Cancel']")
	public WebElement ChangeOwnerCancelButton;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Status']")
	public WebElement EditStatus;

	@FindBy(how = How.XPATH, using = "//label[text()='Status']//following-sibling::div//lightning-base-combobox//div//div//button[@class='slds-combobox__input slds-input_faux slds-combobox__input-value']")
	public WebElement SelectStatus;

	@FindBy(how = How.XPATH, using = "//button[text()='Save']")
	public WebElement SaveEdit;

	@FindBy(how = How.XPATH, using = "//span[@title='New']")
	public WebElement StatusNew;

	@FindBy(how = How.XPATH, using = "//p[text()='Status']//parent::div//parent::records-highlights-details-item[@class='slds-page-header__detail-block']//lightning-formatted-text")
	public WebElement CaseStatus;

}
