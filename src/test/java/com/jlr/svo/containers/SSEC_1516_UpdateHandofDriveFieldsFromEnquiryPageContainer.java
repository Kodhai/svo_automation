package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SSEC_1516_UpdateHandofDriveFieldsFromEnquiryPageContainer {

	@FindBy(how = How.XPATH, using = "//button[@name='Edit']")
	public WebElement EditEnquiryBtn;
	
	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search KMIs...']")
	public WebElement SourceKMISearchBox;
	
	@FindBy(how = How.XPATH, using = "//span[@title='New KMI']")
	public WebElement NewKMIBtn;
	
	@FindBy(how = How.XPATH, using = "//input[@checked='checked']")
	public WebElement ActiveStatus;
	
	@FindBy(how = How.XPATH, using = "//div//child::input[@placeholder='Search Contacts...']")
	public WebElement KMIContacts;
	
	@FindBy(how = How.XPATH, using = "//input[@title='Search Contacts']")
	public WebElement KMIContactNameSearch;
	
	public String ValueSelection(String value) {
		return "//div[@title='" + value + "']";
	}
	
	@FindBy(how = How.XPATH, using = "//a[@class='select']")
	public WebElement KMIProductoffering;
	
	public String ProdOffSelection(String Value) {
		return "//a[text() = '" + Value + "']";
	}
	
	@FindBy(how = How.XPATH, using = "(//input[@placeholder='Search Vehicle Manufacturers...'])[2]")
	public WebElement SVOBespokeBrand;
	
	public String BrandValueSelection(String value) {
		return "//a[@title='"+ value + "']";
	}
	
	@FindBy(how = How.XPATH, using = "(//input[@placeholder='Search Vehicle Models...'])[2]")
	public WebElement SVOBespokeModel;
	
	public String ModelValueSelection(String value) {
		return "//a[@title='"+ value + "']";
	}
	
	
	@FindBy(how = How.XPATH, using = "(//button[@title='Save'])[2]")
	public WebElement SaveKMIBtn;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Source KMI']//parent::div//following-sibling::div//child::span//child::a//child::span")
	public WebElement KMIName;
	
	@FindBy(how = How.XPATH, using = "//span[@title='Hand of drive']")
	public WebElement HandOfDriveSection;
	
	@FindBy(how = How.XPATH, using = "//button[@name='Edit']")
	public WebElement EditKMIBtn;
	
	@FindBy(how = How.XPATH, using = "(//button[@class='slds-combobox__input slds-input_faux slds-combobox__input-value']//span[@class='slds-truncate'])[2]")
	public WebElement HandofDriveTextArea;
	
	@FindBy(how = How.XPATH, using = "(//a[@class='select'])[2]")
	public WebElement HandofDrive;
	
	@FindBy(how = How.XPATH, using = "//button[@name='SaveEdit']")
	public WebElement UpdateKMIBtn;
	
	@FindBy(how = How.XPATH, using = "//span[@title='--None--']")
	public WebElement RemoveHandOfDrive;
	
	public String HandofDriveSelection(String Value) {
		return "//span[@title = '" + Value + "']";
	}
	
	public String RemoveHandofDrive() {
		return "//span[@title='--None--']";
	}
	
	public String HandofDrive(String Value) {
		return "//a[@title = '" + Value + "']";
	}
	
	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search this list...']")
	public WebElement EnquirySearchBox;
	
	@FindBy(how = How.XPATH, using = "//span[@class='test-id__field-value slds-form-element__static slds-grow ']")
	public WebElement EnquiryStatus;
	
	@FindBy(how = How.XPATH, using = "//button[@name='SaveEdit']")
	public WebElement SaveEditBtn;
	
	
	
}
