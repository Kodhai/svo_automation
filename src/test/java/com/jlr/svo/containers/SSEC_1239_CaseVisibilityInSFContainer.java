package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SSEC_1239_CaseVisibilityInSFContainer {

	@FindBy(how = How.XPATH, using = "//button[@title = 'Select a List View']")
	public WebElement CasesDD;

	@FindBy(how = How.XPATH, using = "//span[text()='All cases']")
	public WebElement DDAllCases;

	@FindBy(how = How.XPATH, using = "//input[@aria-label='Search Recently Viewed list view.']")
	public WebElement SearchCases;

	@FindBy(how = How.XPATH, using = "(//a[@data-refid='recordId']//parent::span//following-sibling::a)[1]")
	public WebElement CasesFind;

	@FindBy(how = How.XPATH, using = "//span[text()='Case Origin']")
	public WebElement CaseOriginField;

	@FindBy(how = How.XPATH, using = "//button[@aria-label='Search']")
	public WebElement Searchbox;

}
