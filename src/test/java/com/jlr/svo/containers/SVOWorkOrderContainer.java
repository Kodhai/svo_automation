package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SVOWorkOrderContainer {

	@FindBy(how = How.XPATH, using = "//li[contains(@class,'slds-dropdown__item has-icon--left   forceVirtualAutocompleteMenuOption')]//child::a//child::lightning-icon//following-sibling::span[contains(@class,' virtualAutocompleteOptionText') and contains(text(),'My Work Orders')]")
	public WebElement MyWorkOrdersListView;

	@FindBy(how = How.XPATH, using = "//li[contains(@class,'slds-dropdown__item has-icon--left   forceVirtualAutocompleteMenuOption')]//child::a//child::lightning-icon//following-sibling::span[contains(@class,' virtualAutocompleteOptionText') and contains(text(),'All')]")
	public WebElement AllWorkOrdersListView;

	@FindBy(how = How.XPATH, using = "//li[contains(@class,'slds-dropdown__item has-icon--left   forceVirtualAutocompleteMenuOption')]//child::a//child::lightning-icon//following-sibling::span[contains(@class,' virtualAutocompleteOptionText') and contains(text(),'New Work Orders')]")
	public WebElement NewWorkOrdersListView;

	@FindBy(how = How.XPATH, using = "//li[contains(@class,'slds-dropdown__item has-icon--left   forceVirtualAutocompleteMenuOption')]//child::a//child::lightning-icon//following-sibling::span[contains(@class,' virtualAutocompleteOptionText') and contains(text(),'Orders in Build')]")
	public WebElement OrdersInBuildWorkOrdersListView;

	@FindBy(how = How.XPATH, using = "//span[@title='Programme']")
	public WebElement ProgrammeField;

	@FindBy(how = How.XPATH, using = "//span[@class='triggerLinkText selectedListView slds-page-header__title slds-truncate slds-p-right--xx-small uiOutputText'] ")
	public WebElement RecentlyViewedLink;

	@FindBy(how = How.XPATH, using = "//div[@aria-label='Apps']//child::p[text()='No results']")
	public WebElement NoAppOrdItemResults;

	@FindBy(how = How.XPATH, using = "//div[@class='slds-icon-waffle']")
	public WebElement SVOMenuBar;

	@FindBy(how = How.XPATH, using = "//input[@class='slds-input']")
	public WebElement SVOAppSearchBox;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-button--reset downIcon slds-m-top_xxx-small slds-p-right_xxx-small']")
	public WebElement WorkOrdersDropdown;

	@FindBy(how = How.XPATH, using = "//span[text()='New Work Orders']")
	public WebElement NewWorkOrders;

	@FindBy(how = How.XPATH, using = "//span[text()='Orders in Build']")
	public WebElement OrdersInBuild;

	@FindBy(how = How.XPATH, using = "//div[text()='New']")
	public WebElement NewButtonForCreateNewWorkOrder;
	
	@FindBy(how = How.XPATH, using = "//span[text()='No items to display.']")
	public WebElement NoworordersfoundfortheSearch;

	@FindBy(how = How.XPATH, using = "//span[text()='New Build']")
	public WebElement NewBuildRecordType;

	@FindBy(how = How.XPATH, using = "//span[text()='Next']")
	public WebElement NextButtonForCreateNewWorkOrder;

	@FindBy(how = How.XPATH, using = "//button[text()='Save']")
	public WebElement SaveButtonForCreateNewWorkOrder;

	@FindBy(how = How.XPATH, using = "//div[@class='slds-form-element slds-hint-parent test-id__output-root slds-form-element_readonly slds-form-element_stacked']//child::div//child::span//child::slot//lightning-formatted-text[@data-output-element-id='output-field']")
	public WebElement NewlyCreatedWorkOrderNumber;

	@FindBy(how = How.XPATH, using = "//span[text()='My Work Orders']")
	public WebElement MyWorkOrders;

	@FindBy(how = How.XPATH, using = "//lightning-input[@class='slds-form-element']//child::div//child::input[@class='slds-input']")
	public WebElement OrderNumberSearchBox;

	@FindBy(how = How.XPATH, using = "//tbody//child::tr[1]//child::th[1]//span[@class='slds-grid slds-grid--align-spread']")
	public WebElement ViewNewWorkOrder;

	@FindBy(how = How.XPATH, using = "//span[text()='All']")
	public WebElement AllWorkOrders;

	@FindBy(how = How.XPATH, using = "//a[text()='Add Username']")
	public WebElement AddUsername;
	
	public String NewlyCreatedWorkOrderNumber(String text) {
		return "//a[text()='" + text + "']";
	}
}
