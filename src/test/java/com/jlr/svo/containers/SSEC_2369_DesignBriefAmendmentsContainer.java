package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SSEC_2369_DesignBriefAmendmentsContainer {

	@FindBy(how = How.XPATH, using = "//span[text()='Donor']")
	public WebElement DonorSection;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Seats']")
	public WebElement SeatsSection;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Interior']")
	public WebElement InteriorSection;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Scatter Cushion Colour']//parent::div")
	public WebElement ScatterCushionColorText;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Scatter Cushion Embroidery']//parent::div")
	public WebElement ScatterCushionEmbroideryText;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Comfort Pack Colour']//parent::div")
	public WebElement ComfortPackColorText;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Calf Rest Quantity']//parent::div")
	public WebElement CalfRestQuantityText;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Edit Calf Rest Quantity']")
	public WebElement EditCalfRestQuantityText;
	
	@FindBy(how = How.XPATH, using = "//button[@name='SaveEdit']")
	public WebElement SaveEditText;
	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
	
	
}
