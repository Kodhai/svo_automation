package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SSEC_2150_Booker25AmendementsContainer {

	@FindBy(how = How.XPATH, using = "//span[text()='Commissioning Requests']")
	public WebElement CommissioningRequestTab;
	
	@FindBy(how = How.XPATH, using = "//div[text()='New']")
	public WebElement NewBtn;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Next']")
	public WebElement NextBtn;
	
	@FindBy(how = How.XPATH, using = "(//input[@name='SO_Starting__c'])[1]")
	public WebElement StartingDate;
	
	@FindBy(how = How.XPATH, using = "(//input[@name='SO_Ending__c'])[1]")
	public WebElement EndingDate;
	
	@FindBy(how = How.XPATH, using = "//button[@name='SaveEdit']")
	public WebElement SaveEdit;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Private Office']")
	public WebElement PrivateOffice;
	
	@FindBy(how = How.XPATH, using = "//input[@name='SO_Duration_hours__c']")
	public WebElement DurationHours;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Tour Type']")
	public WebElement TourType;
	
	public String TourType(String value) {
		return "//span[@title='" + value + "']";
	}
	
	@FindBy(how = How.XPATH, using = "//label[text()='Est. Attendees']")
	public WebElement EstAttendees;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Includes Classic Visit?']")
	public WebElement IncludesClassicVisit;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Catering Required']")
	public WebElement CateringRequired;
	
	@FindBy(how = How.XPATH, using = "//span[text()='AV Required?']")
	public WebElement AVRequired;
	
	@FindBy(how = How.XPATH, using = "//span[text()='PPE Required']")
	public WebElement PPERequired;
	
	@FindBy(how = How.XPATH, using = "//button[text()='Submit']")
	public WebElement SubmitBtnCommissioningReq;
	
	@FindBy(how = How.XPATH, using = "(//span[text()='Save']//parent::button)[3]")
	public WebElement SaveBtn;
	
	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-button_icon-border-filled slds-button_icon-x-small']")
	public WebElement ReservationDropDownBtn;
	
	@FindBy(how = How.XPATH, using = "//a[@name='New']")
	public WebElement NewReservationBtn;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Bespoke']")
	public WebElement BespokeReservationRecordType;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Next']//parent::button")
	public WebElement NextBtnReservation;
	
	@FindBy(how = How.XPATH, using = "(//input[@name='B25__StartLocal__c'])[1]")
	public WebElement StartDateReservation;
	
	@FindBy(how = How.XPATH, using = "(//input[@name='B25__StartLocal__c'])[2]")
	public WebElement StartTimeReservation;
	
	@FindBy(how = How.XPATH, using = "(//input[@name='B25__EndLocal__c'])[1]")
	public WebElement EndDateReservation;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Primary Contact']")
	public WebElement PrimaryContactRservation;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Max. Attendees']")
	public WebElement MaxAttendeesReservation;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Meeting Type']")
	public WebElement MeetingTypeReservation;
	
	public String PrimaryContact(String value) {
		return "//span[@title='" + value + "']";
	}
	
	@FindBy(how = How.XPATH, using = "//span[starts-with(text(), 'R-')]")
	public WebElement ReservationRecord;
	
	@FindBy(how = How.XPATH, using = "//button[@title='Edit Status']")
	public WebElement EditStatusReservation;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Status']")
	public WebElement StatusLabelReservation;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Host']")
	public WebElement HostReservation;
	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
	
}
