package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SSEC_2157_DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer {

	@FindBy(how = How.XPATH, using = "(//a[starts-with(@title, 'SVO')])[1]")
	public WebElement QuoteRecord;
	
	@FindBy(how = How.XPATH, using = "//span[starts-with(text(), 'Design Briefs')]//following-sibling::span")
	public WebElement DesignBriefsTab;
	
	@FindBy(how = How.XPATH, using = "//a[starts-with(@title, 'SVO-2022')]//parent::span//parent::th")
	public WebElement DesignBriefRecord;
	
	@FindBy(how = How.XPATH, using = "//button[@title='Edit Front seat Belt Colour']")
	public WebElement EditFrontSeatBeltColorBtn;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Seat Belts Details']")
	public WebElement SeatBeltDetails;
	
	@FindBy(how = How.XPATH, using = "Front seat Belt Colour")
	public WebElement FrontSeatBeltColorOption;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Rear seat Belt Colour']")
	public WebElement RearSeatBeltColorOption;
	
	public String Color(String value) {
		return "//span[@title='" + value + "']";
	}
	
	
	@FindBy(how = How.XPATH, using = "//button[text()='Save']")
	public WebElement SaveDesignBrief;
	
	@FindBy(how = How.XPATH, using = "title=\"Edit Cabin Carpet Colour\"")
	public WebElement EditCabinCarpetColor;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Cabin Carpet Colour']")
	public WebElement CabinColorCarpetOpt;
	
	@FindBy(how = How.XPATH, using = "//button[@title='Edit Fridge Material']")
	public WebElement EditFridgeMaterialBtn;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Fridge Material']")
	public WebElement FridgeMaterialOption;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Veneer Colour']")
	public WebElement VeneerColorOpt;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Fridge Colour']")
	public WebElement FridgeColorOpt;
	
	@FindBy(how = How.XPATH, using = "//span[starts-with(@title, 'Headliner')]")
	public WebElement HeadlinerDetailsText;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Edit Headliner Material']")
	public WebElement EditHeadlinerMaterialBtn;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Headliner Material']")
	public WebElement HeadLinerMaterialText;
	
	@FindBy(how = How.XPATH, using = "(//label[starts-with(text(), 'Headliner')])[2]")
	public WebElement HeadlinerColorText;
	

}
