package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SSEC_2383_Classic_Team_Wide_Signature_Amendment_Container {

	@FindBy(how = How.XPATH, using = "//li[@title='Emails']")
	public WebElement Emails;

	@FindBy(how = How.XPATH, using = "//button[text()='e2a Send an Email']")
	public WebElement e2asendanemail;

	@FindBy(how = How.XPATH, using = "//input[@value='Select Template']")
	public WebElement SelectTemplate;

	@FindBy(how = How.XPATH, using = "//option[text()='All Lightning Templates']//parent::select")
	public WebElement Alllightningtemplates;

	@FindBy(how = How.XPATH, using = "//option[text()='Classic Email Templates']//parent::select")
	public WebElement Classicemail;

	@FindBy(how = How.XPATH, using = "//iframe[@title='accessibility title']")
	public WebElement EmailFrame;

	@FindBy(how = How.XPATH, using = "//option[text()='All']//parent::select")
	public WebElement All;

	@FindBy(how = How.XPATH, using = "//option[text()='SO e2a Signature Templates']//parent::select")
	public WebElement SOe2a;

	@FindBy(how = How.XPATH, using = "//a[text()='SO e2a Classic New Email Signature']")
	public WebElement Newemailsignature;

	@FindBy(how = How.XPATH, using = "//input[@class='btn']")
	public WebElement Btn;

	@FindBy(how = How.XPATH, using = "//span[text()=' Classic Service User ']")
	public WebElement Classicserviceuser;

	@FindBy(how = How.XPATH, using = "//a[text()='View our Available Works Legends Vehicles']")
	public WebElement Vehicle;

	public String selectemail(String ClassicEmail) {
		return "//option[text()='" + ClassicEmail + "']";
	}

}
