package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SSEC_1566_ClassicSalesAndClassicService_CaseObject_fields_Container {

	@FindBy(how = How.XPATH, using = "//a[@title='Enquiries']")
	public WebElement EnquiryTab;

	@FindBy(how = How.XPATH, using = "//div[@title='New']")
	public WebElement NewEnquiry;

	@FindBy(how = How.XPATH, using = "//div[@class='changeRecordTypeOptionRightColumn']//child::span[text()='Classic Sales']")
	public WebElement ClassicSalesEnquiry;

	@FindBy(how = How.XPATH, using = "//div[@class='changeRecordTypeOptionRightColumn']//child::span[text()='Classic Service']")
	public WebElement ClassicServiceEnquiry;

	@FindBy(how = How.XPATH, using = "//span[text()='Next']")
	public WebElement NextEnquiry;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Subject__c']")
	public WebElement title;

	@FindBy(how = How.XPATH, using = "//button[@aria-label='Product Offering, --None--']//child::span[text()='--None--']")
	public WebElement ProductOfferingEnquiry;

	@FindBy(how = How.XPATH, using = "//label[text()='Preferred Contact']")
	public WebElement preferredContact;

	@FindBy(how = How.XPATH, using = "//label[text()='Brand']")
	public WebElement brandEnquiry;

	@FindBy(how = How.XPATH, using = "//label[text()='Model']")
	public WebElement modelEnquiry;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Priority_Position__c']")
	public WebElement Priority;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Provisional_Order_Form_Signed__c']")
	public WebElement CheckBox;

	@FindBy(how = How.XPATH, using = "//span[@class='slds-checkbox slds-checkbox_standalone']")
	public WebElement CheckBoxEdit;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Deposit_Paid__c']")
	public WebElement Deposite;

	@FindBy(how = How.XPATH, using = "//button[text()='Save']")
	public WebElement saveEnquiry;

	@FindBy(how = How.XPATH, using = "//li[@class='slds-tabs_default__item']//child::a[@data-label='Activity']")
	public WebElement Activity;

	@FindBy(how = How.XPATH, using = "//a[@data-label='Activity']")
	public WebElement Activity2;

	@FindBy(how = How.XPATH, using = "//button[@title='Add']")
	public WebElement Add;

	@FindBy(how = How.XPATH, using = "(//button[@title='Create new...'])[2]")
	public WebElement log;

	@FindBy(how = How.XPATH, using = "//label[text()='Subject']")
	public WebElement subject;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-button--brand cuf-publisherShareButton MEDIUM uiButton']")
	public WebElement saveActivity;

	@FindBy(how = How.XPATH, using = "//span[text()='Mark Enquiry Status as Complete']")
	public WebElement MarkStatusComplete;

	@FindBy(how = How.XPATH, using = "//button[@aria-label='Closed State, --None--']")
	public WebElement ClosedState;

	@FindBy(how = How.XPATH, using = "//button[text()='Done']")
	public WebElement DoneClosed;

	@FindBy(how = How.XPATH, using = "//button[@title='Save']")
	public WebElement SaveCase;

	@FindBy(how = How.XPATH, using = "//a[@title='Contacts']")
	public WebElement ContactsTab;

	@FindBy(how = How.XPATH, using = "//button[@name='salutation']")
	public WebElement Salutation;

	@FindBy(how = How.XPATH, using = "//div[@title='New']")
	public WebElement NewButton;

	@FindBy(how = How.XPATH, using = "//input[@name='lastName']")
	public WebElement LastName;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Accounts...']")
	public WebElement AccountName;

	@FindBy(how = How.XPATH, using = "//button[text()='Save']")
	public WebElement SaveContact;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Contacts...']")
	public WebElement Contact;

	@FindBy(how = How.XPATH, using = "//span[text()='Save']")
	public WebElement SaveEditCase;

	@FindBy(how = How.XPATH, using = "//span[text()='First Name']//parent::label//following-sibling::div//input")
	public WebElement FirstNameWeb;

	@FindBy(how = How.XPATH, using = "//span[text()='Last Name']//parent::label//following-sibling::div//input")
	public WebElement LastNameWeb;

	@FindBy(how = How.XPATH, using = "//span[text()='Email']//parent::label//following-sibling::div//input")
	public WebElement EmailWeb;

	@FindBy(how = How.XPATH, using = "//span[text()='Phone Number']//parent::label//following-sibling::div//input")
	public WebElement MobileNum;

	@FindBy(how = How.XPATH, using = "//select[@name='recordType']")
	public WebElement Select;

	@FindBy(how = How.XPATH, using = "//textarea[@name='description']")
	public WebElement CommentBox;

	@FindBy(how = How.XPATH, using = "//button[@title='Submit']")
	public WebElement Submit;

	@FindBy(how = How.XPATH, using = "//div[@class='block-content content']//div//ul//li//a[text()='CONTACT US']")
	public WebElement ContactUsBtn;

	@FindBy(how = How.XPATH, using = "//span[text()='Cases']")
	public WebElement CasesTab;

	@FindBy(how = How.XPATH, using = "//span[@class='triggerLinkText selectedListView slds-page-header__title slds-truncate slds-p-right--xx-small uiOutputText']")
	public WebElement ListView;

	@FindBy(how = How.XPATH, using = "(//a[contains(text(),'00')])[1]")
	public WebElement FirstCase;

	@FindBy(how = How.XPATH, using = "//span[text()='Parts & Technical - All Cases']")
	public WebElement CaseSelect;

	@FindBy(how = How.XPATH, using = "//span[text()='Customer Service - All Cases']")
	public WebElement CaseCustomer;

	@FindBy(how = How.XPATH, using = "//button[@name='Edit']")
	public WebElement EditCase;

	@FindBy(how = How.XPATH, using = "(//a[@class='deleteAction'])[1]")
	public WebElement ClearContactIcon;

	public String CaseSelection(String Case) {
		return "//span[contains(text(),'" + Case + "')]";
	}

	@FindBy(how = How.XPATH, using = "//a[text()='Emails']")
	public WebElement Emails;

	@FindBy(how = How.XPATH, using = "//button[text()='e2a Send an Email']")
	public WebElement EmailButton;

	@FindBy(how = How.XPATH, using = "//iframe[@title='accessibility title']")
	public WebElement EmailFrame;

	@FindBy(how = How.XPATH, using = "//tr[@jsmodel='nXDxbd']")
	public WebElement FirstEmail;

	@FindBy(how = How.XPATH, using = "//select[contains(@name,'business_unit')]")
	public WebElement BusinessUnit;

	@FindBy(how = How.XPATH, using = "//textarea[contains(@name,'to_addresses')]")
	public WebElement ToEmail;

	@FindBy(how = How.XPATH, using = "//input[contains(@name,'email_subject')]")
	public WebElement EmailSubject;

	@FindBy(how = How.XPATH, using = "//body[@class='mce-content-body ']")
	public WebElement EmailBody;

	@FindBy(how = How.XPATH, using = "//a[text()='View our Works Legends Stock']")
	public WebElement EmailLink;

	@FindBy(how = How.XPATH, using = "//td[@class='pbButtonb ']//child::input[@class='btn cvf_button']")
	public WebElement EmailSend;

	@FindBy(how = How.XPATH, using = "//span[text()='Product Offering']/parent::span//following-sibling::div//div//div//div//a")
	public WebElement ProdOfferingCase;

	@FindBy(how = How.XPATH, using = "//span[text()='Case Origin']/parent::span//following-sibling::div//div//div//div//a")
	public WebElement CaseOrigin;

	@FindBy(how = How.XPATH, using = "//span[text()='Rating']/parent::span//following-sibling::div//div//div//div//a")
	public WebElement Rating;

	@FindBy(how = How.XPATH, using = "//span[text()='Region']/parent::span//following-sibling::div//div//div//div//a")
	public WebElement CaseRegion;

	@FindBy(how = How.XPATH, using = "//span[text()='Account Name']")
	public WebElement AccountNameCase;

	@FindBy(how = How.XPATH, using = "//span[text()='Contact Name']")
	public WebElement ContactNameCase;

	@FindBy(how = How.XPATH, using = "//span[text()='Sales Area']/parent::span//following-sibling::div//div//div//div//a")
	public WebElement SalesArea;

	@FindBy(how = How.XPATH, using = "//span[text()='First Name']/parent::label")
	public WebElement FirstNameCase;

	@FindBy(how = How.XPATH, using = "//span[text()='country']/parent::span//following-sibling::div//div//div//div//a[text()='--None--']")
	public WebElement Country;

	@FindBy(how = How.XPATH, using = "//span[text()='Last Name']/parent::label")
	public WebElement LastNameCase;

	@FindBy(how = How.XPATH, using = "//span[text()='Additional Spec Details']/parent::label//following-sibling::textarea")
	public WebElement Spec;

	@FindBy(how = How.XPATH, using = "//span[text()='Web Email']/parent::label")
	public WebElement WebEmailCase;

	@FindBy(how = How.XPATH, using = "//span[text()='Web Phone']/parent::label")
	public WebElement WebPhoneCase;

	@FindBy(how = How.XPATH, using = "//span[text()='Subject']/parent::label")
	public WebElement SubjectCase;

	@FindBy(how = How.XPATH, using = "//span[text()='Description']/parent::label")
	public WebElement DescriptionCase;

	@FindBy(how = How.XPATH, using = "//span[text()='Customer Contact']/parent::span//following-sibling::div//div//div//div//a[text()='--None--']")
	public WebElement CustomerContact;

	@FindBy(how = How.XPATH, using = "//span[text()='Primary Exterior Colour']/parent::span//following-sibling::div//div//div//div//a[text()='--None--']")
	public WebElement PrimaryExterior;

	@FindBy(how = How.XPATH, using = "//span[text()='Hand of Drive']/parent::span//following-sibling::div//div//div//div//a[text()='--None--']")
	public WebElement HandOfDriver;

	@FindBy(how = How.XPATH, using = "//span[text()='Source']/parent::span//following-sibling::div//div//div//div//a[text()='--None--']")
	public WebElement SourceCase;

	@FindBy(how = How.XPATH, using = "//span[text()='Brand']")
	public WebElement BrandCase;

	@FindBy(how = How.XPATH, using = "//span[text()='Programme']")
	public WebElement Programme;

	@FindBy(how = How.XPATH, using = "//span[text()='Model']")
	public WebElement ModelCase;

	@FindBy(how = How.XPATH, using = "//span[text()='Series Name']")
	public WebElement SeriesNameCase;

	@FindBy(how = How.XPATH, using = "//span[text()='Body Style']")
	public WebElement BodyStyleCase;

	@FindBy(how = How.XPATH, using = "//span[text()='Vehicle']//parent::label//following-sibling::div//input")
	public WebElement VehicleCase;

	@FindBy(how = How.XPATH, using = "//span[text()='Vehicle Registration']//parent::label//following-sibling::input")
	public WebElement VehicleRegistration;

	@FindBy(how = How.XPATH, using = "//span[text()='VIN / Chassis Number']//parent::label//following-sibling::input")
	public WebElement VINCase;

	@FindBy(how = How.XPATH, using = "//span[text()='Source Campaign']//parent::label//following-sibling::div//input")
	public WebElement SourceCamp;

	@FindBy(how = How.XPATH, using = "//span[text()='Source KMI']//parent::label//following-sibling::div//input")
	public WebElement SourceKMI;

	@FindBy(how = How.XPATH, using = "//span[text()='Preferred Retailer']//parent::label//following-sibling::div//input")
	public WebElement PreffRetailer;

	@FindBy(how = How.XPATH, using = "//span[text()='Hand of Drive']/parent::span//following-sibling::div//div//div//div//a")
	public WebElement HandOfDrive;

	@FindBy(how = How.XPATH, using = "//span[text()='Production Year']")
	public WebElement ProdYear;

	@FindBy(how = How.XPATH, using = "//span[text()='Originating Division']/parent::span//following-sibling::div//div//div//div//a")
	public WebElement Originating;

	@FindBy(how = How.XPATH, using = "//span[text()='Product Offering']/parent::div//following-sibling::div//span")
	public WebElement VerifyProdOff;

	@FindBy(how = How.XPATH, using = "//span[text()='Status']/parent::div//following-sibling::div//span")
	public WebElement VerifyStatus;

	@FindBy(how = How.XPATH, using = "//span[text()='Rating']/parent::div//following-sibling::div//span")
	public WebElement VerifyRating;

	@FindBy(how = How.XPATH, using = "//span[text()='Case Owner']/parent::div//following-sibling::div//span")
	public WebElement VerifyOwner;

	@FindBy(how = How.XPATH, using = "//span[text()='Case Record Type']/parent::div//following-sibling::div//span")
	public WebElement VerifyRecord;

	@FindBy(how = How.XPATH, using = "//span[text()='Case Origin']/parent::div//following-sibling::div//span")
	public WebElement VerifyCaseOrigin;

	@FindBy(how = How.XPATH, using = "//span[text()='Priority']/parent::div//following-sibling::div//span")
	public WebElement VerifyPriority;

	@FindBy(how = How.XPATH, using = "//span[text()='Account Name']/parent::div//following-sibling::div//span")
	public WebElement VerifyAccount;

	@FindBy(how = How.XPATH, using = "//span[text()='Contact Name']/parent::div//following-sibling::div//span")
	public WebElement VerifyContact;

	@FindBy(how = How.XPATH, using = "//span[text()='Contact Email']/parent::div//following-sibling::div//span")
	public WebElement VerifyEmail;

	@FindBy(how = How.XPATH, using = "//span[text()='First Name']/parent::div//following-sibling::div//span")
	public WebElement VerifyFirst;

	@FindBy(how = How.XPATH, using = "//span[text()='Last Name']/parent::div//following-sibling::div//span")
	public WebElement VerifyLast;

	@FindBy(how = How.XPATH, using = "//span[text()='Customer Contact']/parent::div//following-sibling::div//span")
	public WebElement VerifyCustomer;

	@FindBy(how = How.XPATH, using = "//span[text()='Region']/parent::div//following-sibling::div//span")
	public WebElement VerifyRegion;

	@FindBy(how = How.XPATH, using = "//span[text()='Sales Area']/parent::div//following-sibling::div//span")
	public WebElement VerifyArea;

	@FindBy(how = How.XPATH, using = "//span[text()='country']/parent::div//following-sibling::div//span")
	public WebElement VerifyCountry;

	@FindBy(how = How.XPATH, using = "//span[text()='Web Email']/parent::div//following-sibling::div//span")
	public WebElement VerifyWebEmail;

	@FindBy(how = How.XPATH, using = "//span[text()='Web Phone']/parent::div//following-sibling::div//span")
	public WebElement VerifyWebPhone;

	@FindBy(how = How.XPATH, using = "//span[text()='Subject']/parent::div//following-sibling::div//span")
	public WebElement VerifySubject;

	@FindBy(how = How.XPATH, using = "//span[text()='Description']/parent::div//following-sibling::div//span")
	public WebElement VerifyDescription;

	@FindBy(how = How.XPATH, using = "//span[text()='Programme']/parent::div//following-sibling::div//span")
	public WebElement VerifyProgramme;

	@FindBy(how = How.XPATH, using = "//span[text()='Brand']/parent::div//following-sibling::div//span")
	public WebElement VerifyBrand;

	@FindBy(how = How.XPATH, using = "//span[text()='Model']/parent::div//following-sibling::div//span")
	public WebElement VerifyModel;

	@FindBy(how = How.XPATH, using = "//span[text()='Series Name']/parent::div//following-sibling::div//span")
	public WebElement VerifySeriesName;

	@FindBy(how = How.XPATH, using = "//span[text()='Body Style']/parent::div//following-sibling::div//span")
	public WebElement VerifyBodyStyle;

	@FindBy(how = How.XPATH, using = "//span[text()='Vehicle Registration']/parent::div//following-sibling::div//span")
	public WebElement VerifyRegistration;

	@FindBy(how = How.XPATH, using = "//span[text()='Production Year']/parent::div//following-sibling::div//span")
	public WebElement VerifyPYear;

	@FindBy(how = How.XPATH, using = "//span[text()='Hand of Drive']/parent::div//following-sibling::div//span")
	public WebElement VerifyHDrive;

	@FindBy(how = How.XPATH, using = "//span[text()='Primary Exterior Colour']/parent::div//following-sibling::div//span")
	public WebElement VerifyPrimaryColour;

	@FindBy(how = How.XPATH, using = "//span[text()='Additional Spec Details']/parent::div//following-sibling::div//span")
	public WebElement VerifySpec;

	@FindBy(how = How.XPATH, using = "//span[text()='VIN / Chassis Number']/parent::div//following-sibling::div//span")
	public WebElement VerifyVIN;

	@FindBy(how = How.XPATH, using = "//span[text()='Vehicle']/parent::div//following-sibling::div//span")
	public WebElement VerifyVehicle;

	@FindBy(how = How.XPATH, using = "//span[text()='Source']/parent::div//following-sibling::div//span")
	public WebElement VerifySource;

	@FindBy(how = How.XPATH, using = "//span[text()='Source Campaign']/parent::div//following-sibling::div//span")
	public WebElement VerifyCamp;

	@FindBy(how = How.XPATH, using = "//span[text()='Originating Division']/parent::div//following-sibling::div//span")
	public WebElement VerifyDivision;

	@FindBy(how = How.XPATH, using = "//span[text()='Source KMI']/parent::div//following-sibling::div//span")
	public WebElement VerifyKMI;

	@FindBy(how = How.XPATH, using = "//span[text()='Preferred Retailer']/parent::div//following-sibling::div//span")
	public WebElement VerifyRetailer;

	@FindBy(how = How.XPATH, using = "//span[text()='Created By']/parent::div//following-sibling::div//span")
	public WebElement VerifyCreatedBy;

	@FindBy(how = How.XPATH, using = "//span[text()='Last Modified By']/parent::div//following-sibling::div//span")
	public WebElement VerifyModifiedBy;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-button--brand slds-path__mark-complete stepAction current uiButton']")
	public WebElement MarkStatusCase;

	public String CaseRecordTypeSelection(String RecType) {
		return "//div[@class='changeRecordTypeOptionRightColumn']//span[text()='" + RecType + "']";
	}

	public String ValueSelection(String value) {
		return "//a[text()='" + value + "']";
	}

	@FindBy(how = How.XPATH, using = "//span[contains(@title,'in Accounts')]")
	public WebElement AccountSearchBar;

	public String AccountSelection(String AccountName) {
		return "//a[@title='" + AccountName + "']";
	}

	@FindBy(how = How.XPATH, using = "//span[contains(@title,'in Contacts')]")
	public WebElement ContactSearchBar;

	@FindBy(how = How.XPATH, using = "//span[contains(@title,'in Programmes')]")
	public WebElement ProgrammeSearchBar;

	@FindBy(how = How.XPATH, using = "//span[contains(@title,'in Vehicle Manufacturers')]")
	public WebElement BrandSearchBar;

	@FindBy(how = How.XPATH, using = "//span[contains(@title,'in Vehicle Models')]")
	public WebElement ModelSearchBar;

	@FindBy(how = How.XPATH, using = "//span[contains(@title,'in Vehicle Series')]")
	public WebElement SeriesSearchBar;

	@FindBy(how = How.XPATH, using = "//span[contains(@title,'in Vehicle Body Styles')]")
	public WebElement BodyStyleSearchBar;

	@FindBy(how = How.XPATH, using = "//span[contains(@title,'in Campaigns')]")
	public WebElement CampaignSearchBar;

	@FindBy(how = How.XPATH, using = "//span[contains(@title,'in KMIs')]")
	public WebElement KMISearchBar;

}
