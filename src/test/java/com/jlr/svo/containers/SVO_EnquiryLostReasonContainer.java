package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SVO_EnquiryLostReasonContainer {

	@FindBy(how = How.XPATH, using = "//a[@title='Enquiries']")
	public WebElement EnquiriesTab;

	@FindBy(how = How.XPATH, using = "//div[@title='New']")
	public WebElement EnquiryNewButton;

	@FindBy(how = How.XPATH, using = "//div[@class='forceChangeRecordTypeFooter']//span[text()='Next']")
	public WebElement EnquiryNextButton;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Subject__c']")
	public WebElement EnquiryTitleTextBox;

	@FindBy(how = How.XPATH, using = "//button[@name='SaveEdit']")
	public WebElement EnquirySaveButton;

	@FindBy(how = How.XPATH, using = "//div//child::p[@title='Enquiry Name']//following-sibling::p//lightning-formatted-text")
	public WebElement EnquiryName;

	@FindBy(how = How.XPATH, using = "//p[@title='SLA Status']//following-sibling::p//span//img[@alt='In Progress']")
	public WebElement SLAStatus;

	@FindBy(how = How.XPATH, using = "//a[@id='activityTab__item']")
	public WebElement EnquiryActivitytab;

	@FindBy(how = How.XPATH, using = "//button[@title='Add']")
	public WebElement LogACallAddButton;

	@FindBy(how = How.XPATH, using = "//div[@class='bottomBarRight slds-col--bump-left']//button//span[text()='Save']")
	public WebElement EnquiryActivitySaveButton;

	@FindBy(how = How.XPATH, using = "//label[text()='Subject']//following-sibling::div//input")
	public WebElement LogACallSubjectTextBox;

	@FindBy(how = How.XPATH, using = "//input[@title='Search contacts...']")
	public WebElement EnquiryContactTextBox;

	@FindBy(how = How.XPATH, using = "//span[contains(@title,'in Contacts')]")
	public WebElement EnquiryContactSearch;

	@FindBy(how = How.XPATH, using = "//button//span[text()='Mark Enquiry Status as Complete']")
	public WebElement MarkEnquiryStatusAsCompleteBtn;

	@FindBy(how = How.XPATH, using = "//div[@class='slds-path__scroller_inner scroller']//a[@data-tab-name='Closed']//span[text()='Closed']")
	public WebElement ClosedEnquiryStatusTab;

	@FindBy(how = How.XPATH, using = "//button[@name='SO_Closed_State__c']")
	public WebElement ClosedStateDropDownList;

	@FindBy(how = How.XPATH, using = "//button//span[text()='Mark as Current Enquiry Status']")
	public WebElement MarkAsCurrentEnquiryStatusBtn;

	@FindBy(how = How.XPATH, using = "//button[@name='SO_Closed_Reason__c']")
	public WebElement ClosedReasonDropDownList;

	@FindBy(how = How.XPATH, using = "//button[text()='Done']")
	public WebElement EnquiryClosedDoneButton;

	@FindBy(how = How.XPATH, using = "//button[text()='Cancel']")
	public WebElement EnquiryCloseCancelButton;

	@FindBy(how = How.XPATH, using = "//span[text()='Closed State']//parent::div//following-sibling::div//span//span")
	public WebElement EnquiryClosedState;

	@FindBy(how = How.XPATH, using = "//span[text()='Closed Reason']//parent::div//following-sibling::div//span//span")
	public WebElement EnquiryClosedReason;

	@FindBy(how = How.XPATH, using = "(//button[@title='Edit Closed Reason'])[1]")
	public WebElement EditEnquiryClosedreasonBtn;

	@FindBy(how = How.XPATH, using = "//span[text()='Closed Reason']//parent::span//following-sibling::div//a")
	public WebElement EditClosedReasonDropDownList;

	public String EnquiryClosedReason(String value) {
		return "//a[text() = '" + value + "']";
	}

	public String RecTypeSelection(String Value) {
		return "//div[@class='changeRecordTypeOptionRightColumn']//span[text() = '" + Value + "']";
	}

	@FindBy(how = How.XPATH, using = "//button[@title='Save']")
	public WebElement EditEnquirySaveBtn;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Enquiry Status']")
	public WebElement EditEnquiryStatusBtn;

	@FindBy(how = How.XPATH, using = "//label[text()='Enquiry Status']//following-sibling::div//button")
	public WebElement EditEnquiryStatusDropDownlist;

	public String EnquiryStatusSelection(String value) {
		return "//span[@title = '" + value + "']";
	}

	@FindBy(how = How.XPATH, using = "//h2[@title='We hit a snag.']")
	public WebElement EnquiryStatusReopenErrorMsg;

	@FindBy(how = How.XPATH, using = "//button[@name='CancelEdit']")
	public WebElement CancelEnquiryBtn;

	@FindBy(how = How.XPATH, using = "//button[@name='Edit']")
	public WebElement EditEnquiryBtn;

	@FindBy(how = How.XPATH, using = "//h1//lightning-formatted-text")
	public WebElement UpdatedEnquiryTitle;

	@FindBy(how = How.XPATH, using = "//p[@title='Enquiry Status']//parent::div//following-sibling::p//lightning-formatted-text")
	public WebElement EnquiryStatusField;

	@FindBy(how = How.XPATH, using = "//button[@name='Delete']")
	public WebElement DeleteEnquiryBtn;

	@FindBy(how = How.XPATH, using = "//button[@title='Delete']")
	public WebElement DeleteEnquiryPopUpBtn;

	@FindBy(how = How.XPATH, using = "//span[text()='Enquiry Transferred to Essen']")
	public WebElement EnquiryTransferredToEssenPicklist;

	@FindBy(how = How.XPATH, using = "(//div[@class='scroller']//child::ul//parent::li//parent::li//parent::li//child::a//child::span[text()='All Classic Sales Enquiries'])[1]")
	public WebElement AllClassicSalesEnquirytab;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Enquiry__c-search-input']")
	public WebElement EnquirysearchBox;

	@FindBy(how = How.XPATH, using = "//th[@title='SLA Status']//a[@class='toggle slds-th__action slds-text-link--reset ']")
	public WebElement SLAStatusField;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-button_icon-border-filled']")
	public WebElement ChangeRecordTypeMoreActionBtn;

	@FindBy(how = How.XPATH, using = "//div[contains(@class,'slds-dropdown__list slds-dropdown')]//span[text()='Change Record Type']")
	public WebElement ChangeRecordTypeEnquiryBtn;

	@FindBy(how = How.XPATH, using = "//p[@class='changeRecordTypeTagLine']")
	public WebElement ChangeRecordTypeTagline;

	@FindBy(how = How.XPATH, using = "//div[@class='modal-footer slds-modal__footer']//button[@title='Cancel']")
	public WebElement EnquiryCancelButton;

	@FindBy(how = How.XPATH, using = "//button[@title='Next']")
	public WebElement ChangeRecordTypeNextButton;
	
	
	

}
