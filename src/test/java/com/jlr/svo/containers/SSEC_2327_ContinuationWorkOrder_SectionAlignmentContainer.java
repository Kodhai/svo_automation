package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SSEC_2327_ContinuationWorkOrder_SectionAlignmentContainer {

	@FindBy(how = How.XPATH, using = "//span[text()='Opportunity Record Type']")
	public WebElement OpportunityRecordType;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Opportunity Owner']")
	public WebElement OpportunityOwner;
	
	@FindBy(how = How.XPATH, using = "//button[@title='Edit Production Year']")
	public WebElement EditProductionYear;
	
	@FindBy(how = How.XPATH, using = "//input[@name='SO_Production_Year__c']")
	public WebElement ProductionYearTextBox;
	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
	
}
