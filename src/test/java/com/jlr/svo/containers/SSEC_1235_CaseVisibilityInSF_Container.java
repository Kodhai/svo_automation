package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SSEC_1235_CaseVisibilityInSF_Container {

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-button_icon-border-filled']")
	public WebElement MoreActionsIcon;

	@FindBy(how = How.XPATH, using = "//runtime_platform_actions-action-renderer[@title='Change Record Type']//span[text()='Change Record Type']")
	public WebElement ChangeRecordTypeTab;

	@FindBy(how = How.XPATH, using = "//p[text()='Case Record Type']//parent::div//div[@class='recordTypeName slds-grow slds-truncate']")
	public WebElement CaseRecType;

	@FindBy(how = How.XPATH, using = "//button[@name='Edit']")
	public WebElement EditCaseButton;

	@FindBy(how = How.XPATH, using = "//a[@name='ChangeOwnerOne']")
	public WebElement ChangeOwnerTab;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Users...']")
	public WebElement SearchUsersTxtBox;

	@FindBy(how = How.XPATH, using = "//span[contains(@title,' in Users')]")
	public WebElement OwnerSearchbar;

	@FindBy(how = How.XPATH, using = "(//a[@data-refid='recordId'])[1]")
	public WebElement BespokeUser;

	@FindBy(how = How.XPATH, using = "//button[@title='Submit']")
	public WebElement SubmitButton;

	@FindBy(how = How.XPATH, using = "//span[@class='toastMessage slds-text-heading--small forceActionsText']")
	public WebElement ErrorMsg;

	@FindBy(how = How.XPATH, using = "//button[@title='Cancel']")
	public WebElement CancelButton;
}
