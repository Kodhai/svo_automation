package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SSEC_1237_CaseClosureContainer {

	@FindBy(how = How.XPATH, using = "//a[@title='New']")
	public WebElement NewBtn;

	@FindBy(how = How.XPATH, using = "(//a[text()='--None--'])[1]")
	public WebElement CaseOrigin;

	@FindBy(how = How.XPATH, using = "//a[@title='Closed']")
	public WebElement ClosedLink;

	@FindBy(how = How.XPATH, using = "//span[text()='Mark Case Lifecycle as Complete']")
	public WebElement MarkCaseClose;

	@FindBy(how = How.XPATH, using = "//button[@name='SO_Case_Closure_Reason__c']")
	public WebElement CloseCaseReason;

	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
	public WebElement doneBtn;

	@FindBy(how = How.XPATH, using = "//span[text()='Last Name']")
	public WebElement LastName;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit VIN / Chassis Number']")
	public WebElement EditVINBtn;

	@FindBy(how = How.XPATH, using = "//input[@name='RTC_VIN_Number__c']")
	public WebElement EditVINinput;

	@FindBy(how = How.XPATH, using = "//label[text()='VIN / Chassis Number']")
	public WebElement VINlabel;

	@FindBy(how = How.XPATH, using = "//button[@name='SaveEdit']")
	public WebElement SaveEditBtn;

	@FindBy(how = How.XPATH, using = "//button[@id='window']")
	public WebElement ErrorBtn;

}
