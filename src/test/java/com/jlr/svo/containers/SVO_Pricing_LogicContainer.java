package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SVO_Pricing_LogicContainer {

	@FindBy(how = How.XPATH, using = "//a[contains(@href,'related/Additional_Vehicles__r/view')]//child::slot//child::span[contains(text(),'Additional Vehicles')]")
	public WebElement AdditionalVehicleLink;

	@FindBy(how = How.XPATH, using = "//span[@title='Design Briefs']//parent::a[contains(@href,'related/Design_Briefs__r/view')]")
	public WebElement DesignBriefLink;

	@FindBy(how = How.XPATH, using = "//div[@title='Send Sales Agreement']")
	public WebElement SendSalesAgreementBtn;

	@FindBy(how = How.XPATH, using = "//span[text()='Total Additional Vehicle Price']//parent::div//following-sibling::div//child::span//child::span")
	public WebElement TotalAddtionalVehPriceQuotePage;

	//table[contains(@class,'uiVirtualDataTable')]//child::tbody//child::tr//child::td[@class='slds-cell-edit cellContainer']//child::span[@class='slds-grid slds-grid--align-spread']//child::a[contains(text(),'SVO-')]
	@FindBy(how = How.XPATH, using = "//input[@name='Cost_Price__c']")
	public WebElement AdditionalVehicleCostPrice;

	@FindBy(how = How.XPATH, using = "//input[@name='List_Price__c']")
	public WebElement AdditionalVehicleListPrice;

	@FindBy(how = How.XPATH, using = "//input[@name='Contribution__c']")
	public WebElement AdditionalVehicleContribution;

	@FindBy(how = How.XPATH, using = "//input[@name='Retail_Price__c']")
	public WebElement AdditionalVehicleRetailPrice;

	@FindBy(how = How.XPATH, using = "//input[@name='Revenue__c']")
	public WebElement AdditionalVehicleRevenue;

	@FindBy(how = How.XPATH, using = "//span[@class='slds-truncate uiOutputNumber']")
	public WebElement AutopopulatedAdditionalVehicleRetailPrice;

	@FindBy(how = How.XPATH, using = "//table[@class='slds-table forceRecordLayout slds-table--header-fixed slds-table--edit slds-table--bordered resizable-cols slds-table--resizable-cols uiVirtualDataTable']//child::tbody//child::tr[2]//child::td//span[@class='slds-grid slds-grid--align-spread']//span[@class='slds-truncate uiOutputNumber']")
	public WebElement SecondAdditionalVehicleRetailPrice;

	@FindBy(how = How.XPATH, using = "//ol//li[@class='slds-breadcrumb__item slds-line-height--reset']//following-sibling::li//child::a")
	public WebElement OpportunityNameOnAdditionalVehiclePage;

	@FindBy(how = How.XPATH, using = "//div[@id='viewDocumentPanel']")
	public WebElement SalesAgreementPdfdocument;

	@FindBy(how = How.XPATH, using = "//table[@class='slds-table forceRecordLayout slds-table--header-fixed slds-table--edit slds-table--bordered resizable-cols slds-table--resizable-cols uiVirtualDataTable']//child::tr[1]//child::th[@class='slds-cell-edit cellContainer']//a[starts-with(@title,'AV-')]")
	public WebElement FirstAdditionalVehicleLink;

	@FindBy(how = How.XPATH, using = "//table[@class='slds-table forceRecordLayout slds-table--header-fixed slds-table--edit slds-table--bordered resizable-cols slds-table--resizable-cols uiVirtualDataTable']//child::tr[2]//child::th[@class='slds-cell-edit cellContainer']//a[starts-with(@title,'AV-')]")
	public WebElement SecondAdditionalVehicleLink;

	@FindBy(how = How.XPATH, using = "//div[text()='Return to Quote']")
	public WebElement ReturnToQuoteButton;

	@FindBy(how = How.XPATH, using = "//span[@title='Agreements']//parent::a[contains(@href,'/related/Agreements__r/view')]")
	public WebElement SalesAgreementListLink;

	@FindBy(how = How.XPATH, using = "//*[@id='plugin']")
	public WebElement SalesAgreementPdfdocumentText;

	@FindBy(how = How.XPATH, using = "//span[text()='Cost Price (£)']//parent::div//following-sibling::div//child::span//child::slot//child::lightning-formatted-number")
	public WebElement AdditionalVehicleCostPriceDetails;

	@FindBy(how = How.XPATH, using = "//span[text()='Contribution (£)']//parent::div//following-sibling::div//child::span//child::slot//child::lightning-formatted-number")
	public WebElement AdditionalVehicleContributionPriceDetails;

	@FindBy(how = How.XPATH, using = "//span[text()='Revenue (£)']//parent::div//following-sibling::div//child::span//child::slot//child::lightning-formatted-number")
	public WebElement AdditionalVehicleRevenuePriceDetails;

	@FindBy(how = How.XPATH, using = "//span[text()='List Price']//parent::div//following-sibling::div//child::span//child::slot//child::lightning-formatted-number")
	public WebElement AdditionalVehicleListPriceDetails;

	@FindBy(how = How.XPATH, using = "//span[text()='Retail Price']//parent::div//following-sibling::div//child::span//child::slot//child::lightning-formatted-number")
	public WebElement AdditionalVehicleRetailPriceDetails;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Retail Price']")
	public WebElement PricingDetailsEditBtn;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Retail_Price__c']")
	public WebElement MainVehicleRetailPrice;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_List_Price__c']")
	public WebElement MainVehicleListPrice;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Cost_Price__c']")
	public WebElement MainVehicleCostPrice;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Contribution__c']")
	public WebElement MainVehicleContribution;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Revenue__c']")
	public WebElement MainVehicleRevenue;

	@FindBy(how = How.XPATH, using = "//span[text()='Total Retail Price']//parent::div//following-sibling::div//child::span//child::slot//child::records-formula-output//child::slot//child::lightning-formatted-text")
	public WebElement TotalRetailPrice;

	@FindBy(how = How.XPATH, using = "//span[text()='Total List Price']//parent::div//following-sibling::div//child::span//child::slot//child::records-formula-output//child::slot//child::lightning-formatted-text")
	public WebElement TotalListPrice;

	@FindBy(how = How.XPATH, using = "//span[text()='Total Cost Price (£)']//parent::div//following-sibling::div//child::span//child::slot//child::records-formula-output//child::slot//child::lightning-formatted-number")
	public WebElement TotalCostPrice;

	@FindBy(how = How.XPATH, using = "//span[text()='Total Contribution (£)']//parent::div//following-sibling::div//child::span//child::slot//child::records-formula-output//child::slot//child::lightning-formatted-number")
	public WebElement TotalContribution;

	@FindBy(how = How.XPATH, using = "//span[text()='Total Revenue (£)']//parent::div//following-sibling::div//child::span//child::slot//child::records-formula-output//child::slot//child::lightning-formatted-number")
	public WebElement TotalRevenue;

	@FindBy(how = How.XPATH, using = "//a[text()='Add Username']")
	public WebElement AddUsername;

	@FindBy(how = How.XPATH, using = "//span[text()='Total Retail Price']//parent::div//following-sibling::div//child::span//child::span")
	public WebElement TotalRetailPriceOnOrdersPage;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-button--reset downIcon slds-m-top_xxx-small slds-p-right_xxx-small']")
	public WebElement OpportunityDropdown;

	@FindBy(how = How.XPATH, using = "//span[text()='All Opportunities']/parent::a")
	public WebElement AllOpportunities;

	@FindBy(how = How.XPATH, using = "(//button[@title='Edit Cost Price (£)'])[2]")
	public WebElement AddVehEditCostPrice;

	@FindBy(how = How.XPATH, using = "//input[@name='Cost_Price__c']")
	public WebElement AddVehCostPrice;

	@FindBy(how = How.XPATH, using = "//input[@name='List_Price__c']")
	public WebElement AddVehListPrice;

	@FindBy(how = How.XPATH, using = "//input[@name='Contribution__c']")
	public WebElement AddVehContribution;

	@FindBy(how = How.XPATH, using = "//input[@name='Retail_Price__c']")
	public WebElement AddVehRetailPrice;

	@FindBy(how = How.XPATH, using = "//input[@name='Revenue__c']")
	public WebElement AddVehRevenue;

	@FindBy(how = How.XPATH, using = "//button[@name='Edit']")
	public WebElement EditOpp;

	@FindBy(how = How.XPATH, using = "//span[text()='Pricing Details']")
	public WebElement PricingDetailsOpp;

	@FindBy(how = How.XPATH, using = "(//button[contains(@class,'slds-button')]//child::lightning-primitive-icon//following-sibling::span[text()='Show more actions'])[4]")
	public WebElement AddVehDropdown;

	@FindBy(how = How.XPATH, using = "//a[@name='Edit']")
	public WebElement EditAddVeh;

	public String ValueSelection(String value) {
		return "//table[contains(@class,'uiVirtualDataGrid')]//child::tbody//child::tr//child::td//child::a[text()='"
				+ value + "']";
	}

	@FindBy(how = How.ID, using = "username")
	public WebElement userNameTextBox;

	@FindBy(how = How.ID, using = "password")
	public WebElement passwordTextBox;

	@FindBy(how = How.ID, using = "Login")
	public WebElement loginBtn;

	@FindBy(how = How.XPATH, using = "//input[@id='emc']")
	public WebElement verificationCode;

	@FindBy(how = How.XPATH, using = "//input[@id='emc']")
	public WebElement vCodeTextBox;

	@FindBy(how = How.ID, using = "save")
	public WebElement verifyBtn;

	@FindBy(how = How.ID, using = "p")
	public WebElement mobileNumText;

	@FindBy(how = How.LINK_TEXT, using = "Remind Me Later")
	public WebElement remindMeLater;

	@FindBy(how = How.LINK_TEXT, using = "I Don't Want to Register My Phone")
	public WebElement notRegister;

	@FindBy(how = How.XPATH, using = "(//span[@class='slds-truncate'])[1]")
	public WebElement SVOText;

	@FindBy(how = How.XPATH, using = "//a[@title='Opportunities']")
	public WebElement OpportTab;

	@FindBy(how = How.XPATH, using = "//div[@title='New']")
	public WebElement newOpportunityBtn;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Vehicle Manufacturers...']")
	public WebElement brand;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Vehicle Models...']")
	public WebElement model;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'in Vehicle Manufacturers')]")
	public WebElement brandSearch;

	@FindBy(how = How.XPATH, using = "(//span[contains(@title,'Show All Results')])[2]")
	public WebElement SBbrandSearch;

	@FindBy(how = How.XPATH, using = "(//span[contains(@title,'Show All Results')])[3]")
	public WebElement SBModelSearch;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'in Vehicle Models')]")
	public WebElement modelSearch;

	@FindBy(how = How.XPATH, using = "(//textarea[@class='slds-textarea'])[1]")
	public WebElement requestDetails;

	@FindBy(how = How.XPATH, using = "//span[text()='Pricing Details']")
	public WebElement pricingDetailsSB;

	@FindBy(how = How.XPATH, using = "(//span[text()='Pricing Details'])[2]")
	public WebElement pricingDetails;

	@FindBy(how = How.XPATH, using = "//input[@name='Cost_Price__c']")
	public WebElement costPriceSB;

	@FindBy(how = How.XPATH, using = "input[@name='SO_Cost_Price__c']")
	public WebElement costPrice;

	@FindBy(how = How.XPATH, using = "(//a[@title='Edit'])[2]")
	public WebElement AdditionalVehicleEditBtn;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Cost Price')]//parent::div[contains(@class,'test-id_')]//following-sibling::div[contains(@class,'slds-form')]//child::span[contains(@class,'test-id__field-value')]//child::span[@class='uiOutputNumber']")
	public WebElement quoteCostPrice;

	@FindBy(how = How.XPATH, using = "//span[text()='List Price']//parent::div[contains(@class,'test-id__field')]//following-sibling::div[contains(@class,'slds-form-element')]//child::span[contains(@class,'is-read-only')]//child::span[contains(@class,'forceOutputCurrency')]")
	public WebElement quoteListPrice;

	@FindBy(how = How.XPATH, using = "//input[@name='Contribution__c']")
	public WebElement contributionSB;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Contribution__c']")
	public WebElement contribution;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Revenue__c']")
	public WebElement revenue;

	@FindBy(how = How.XPATH, using = "//input[@name='Revenue__c']")
	public WebElement revenueSB;

	@FindBy(how = How.XPATH, using = "//input[@name='List_Price__c']")
	public WebElement listPriceSB;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_List_Price__c']")
	public WebElement listPrice;

	@FindBy(how = How.XPATH, using = "//input[@name='Retail_Price__c']")
	public WebElement retailPriceSB;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Retail_Price__c']")
	public WebElement retailPrice;

	@FindBy(how = How.XPATH, using = "//button[@name='SaveEdit']")
	public WebElement SaveButton;

	@FindBy(how = How.XPATH, using = "//button[contains(@class,'slds-accordion__summary-action')]//child::span[@title='Additional Vehicles']")
	public WebElement AdditionalVehicleSection;

	@FindBy(how = How.XPATH, using = "//a[contains(@class,'slds-card__header-link baseCard__header-title-')]//child::span[@title='Additional Vehicles']")
	public WebElement AdditionalVehicleSecondSection;

	@FindBy(how = How.XPATH, using = "(//slot[@name='actionsProvider']//following-sibling::ul//child::li//child::lightning-button-menu//child::button[@class='slds-button slds-button_icon-border-filled slds-button_icon-x-small'])[3]")
	public WebElement AdditionalVehicleDropDown;

	@FindBy(how = How.XPATH, using = "//a[contains(@class,'forceAction')]//child::div[text()='New']")
	public WebElement NewVehicleBtn;

	@FindBy(how = How.XPATH, using = "(//a[@title='New']//child::div[text()='New'])[2]")
	public WebElement NewAdditionalVehicleBtn;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Vehicle Manufacturers...']")
	public WebElement AdditionalVehicleBrand;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Vehicle Models...']")
	public WebElement AdditionalVehicleModel;

	@FindBy(how = How.XPATH, using = "//label[text()='Model']//following-sibling::div//child::div//child::lightning-base-combobox//child::div//child::div[contains(@class,'slds-dropdown_left')]//child::lightning-base-combobox-item//child::span[@class='slds-media__body']//child::span[contains(@class,'slds-listbox__option-text_entity')]//span[contains(text(),'Show All Results for')]")
	public WebElement SVOBespokeModelSearch;

	@FindBy(how = How.XPATH, using = "//label[text()='Hand of Drive']//parent::lightning-combobox//child::div//child::lightning-base-combobox//child::div//child::div//child::input[@class='slds-input slds-combobox__input']")
	public WebElement AdditionalVehicleHandOfDrive;

	@FindBy(how = How.XPATH, using = "//span[text()='LHD']")
	public WebElement AdditionalVehicleHODLHD;

	@FindBy(how = How.XPATH, using = "//label[text()='Brand']//following-sibling::div//child::div//child::lightning-base-combobox//child::div//child::div[contains(@class,'slds-dropdown_left')]//child::lightning-base-combobox-item//child::span[@class='slds-media__body']//child::span[contains(@class,'slds-listbox__option-text_entity')]//span[contains(text(),'Show All Results for')]")
	public WebElement SVOBespokeBrandSearch;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Vehicle Series...']")
	public WebElement AdditionalVehicleSeriesName;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Vehicle Derivatives...']")
	public WebElement AdditionalVehicleDerivative;

	@FindBy(how = How.XPATH, using = "//label[text()='Wheel Base']//parent::lightning-combobox//child::div//child::lightning-base-combobox//child::div//child::div//child::input[@class='slds-input slds-combobox__input']")
	public WebElement AdditionalVehicleWheelBase;

	@FindBy(how = How.XPATH, using = "//label[text()='Model Year']//parent::lightning-combobox//child::div//child::lightning-base-combobox//child::div//child::div//child::input[@class='slds-input slds-combobox__input']")
	public WebElement AdditionalVehicleModelYear;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Records...']")
	public WebElement AdditionalVehicleVehicleRecord;

	@FindBy(how = How.XPATH, using = "(//a[contains(@class,'truncate outputLookupLink-a')])[6]")
	public WebElement NewAddedAdditionalVehicle;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Test_SVO_Bespoke')]")
	public WebElement ExistingOpportunityLink;

	@FindBy(how = How.XPATH, using = "//span[text()='Pricing Details']")
	public WebElement PricingDetailsOpportunity;

	@FindBy(how = How.XPATH, using = "//span[text()='Retail Price']")
	public WebElement RetailPriceOpportunity;

	@FindBy(how = How.XPATH, using = "//input[@id='input-374']")
	public WebElement RetailPriceDetails;

	@FindBy(how = How.XPATH, using = "//*[text()='Total Retail Price']")
	public WebElement TotalRetailPriceOpportunity;

	@FindBy(how = How.XPATH, using = "//a[text()='Test ESign']")
	public WebElement ClassicPrefferedContact;

	@FindBy(how = How.XPATH, using = "(//a[text()='Test ESign Retailer'])")
	public WebElement ClassicRetailer;

	@FindBy(how = How.XPATH, using = "(//input[@placeholder='Search Accounts...'])[1]")
	public WebElement SVOClassicClient;

	@FindBy(how = How.XPATH, using = "//span[text()='Total Additional Vehicle Price']")
	public WebElement TotalAdditionalVehiclePriceQuote;

	@FindBy(how = How.XPATH, using = "//span[text()='Total Additional Vehicle Price']//parent::div//following-sibling::div//child::span[@class='uiOutputNumber']")
	public WebElement TotalAdditionalVehiclePriceQuoteValue;

	@FindBy(how = How.XPATH, using = "//span[text()='Total Retail Price']//parent::div//following-sibling::div//child::span[contains(@class,'_static slds-grow  is-read-only')]")
	public WebElement TotalRetailPriceQuoteValue;

	@FindBy(how = How.XPATH, using = "//div[contains(@class,'-slds-form-element')]//child::div[contains(@class,'slds-form-element__label')]//child::span[text()='Total Retail Price']")
	public WebElement TotalRetailPriceQuote;

	@FindBy(how = How.XPATH, using = "//span[text()='Show more actions']//parent::button")
	public WebElement EditBtnDropdownOnAdditionalVehicle;

	@FindBy(how = How.XPATH, using = "//span[text()='Edit']")
	public WebElement EditBtnOnAdditionalVehicle;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Test_SVO_Bespoke_Opportunity')]")
	public WebElement OpportunityNameLink;

	@FindBy(how = How.XPATH, using = "//div//child::div//child::span//child::div//child::a[contains(text(),'Test_SVO_Bespoke_Opportunity')]")
	public WebElement OpportunityNameLinkOnOrderPage;

	@FindBy(how = How.XPATH, using = "//span[text()='Total Additional Vehicle Price']")
	public WebElement TotalAdditionalVehiclePriceSection;

	@FindBy(how = How.XPATH, using = "//iframe[@title='Conga Composer']")
	public WebElement AgreementDocumentFrame;

	@FindBy(how = How.XPATH, using = "//button[@name='Delete']//parent::lightning-button")
	public WebElement DeleteAdditionalVehicle;

	@FindBy(how = How.XPATH, using = "//span[text()='Delete']")
	public WebElement DeleteAdditionalVehicleBtn;

	@FindBy(how = How.XPATH, using = "//span[text()='Total Additional Vehicle Retail Price']//parent::div//following-sibling::div//child::span//child::slot//child::records-formula-output//child::slot//child::lightning-formatted-number")
	public WebElement TotalAdditionalVehicleRetailPrice;

	@FindBy(how = How.XPATH, using = "//span[text()='Total Additional Vehicle List Price']//parent::div//following-sibling::div//child::span//child::slot//child::records-formula-output//child::slot//child::lightning-formatted-number")
	public WebElement TotalAdditionalVehicleListPrice;

	@FindBy(how = How.XPATH, using = "//span[text()='Total Additional Vehicle Cost Price (£)']//parent::div//following-sibling::div//child::span//child::slot//child::records-formula-output//child::slot//child::lightning-formatted-number")
	public WebElement TotalAdditionalVehicleCostPrice;

	@FindBy(how = How.XPATH, using = "//span[text()='Total Additional Vehicle Contribution(£)']//parent::div//following-sibling::div//child::span//child::slot//child::records-formula-output//child::slot//child::lightning-formatted-number")
	public WebElement TotalAdditionalVehicleContribution;

	@FindBy(how = How.XPATH, using = "//span[text()='Total Revenue (£)']//parent::div//following-sibling::div//child::span//child::slot//child::records-formula-output//child::slot//child::lightning-formatted-number")
	public WebElement TotalAdditionalVehicleRevenue;

	@FindBy(how = How.XPATH, using = "//span[text()='Show Actions']//parent::span")
	public WebElement AdditionalVehicleShowMoreDropdownBtn;

	@FindBy(how = How.XPATH, using = "//li[text()='You cannot add a new vehicle as Quote is already Accepted .']")
	public WebElement ErrorMessageOnAdditionalVehiclePage;

	@FindBy(how = How.XPATH, using = "//button[text()='Cancel']")
	public WebElement CancelBtnOnAddtitionalVehiclePage;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Total Retail Price']//parent::div//following-sibling::div//child::span//child::span")
	public WebElement TotalRetailPriceOnQuotePage;

}
