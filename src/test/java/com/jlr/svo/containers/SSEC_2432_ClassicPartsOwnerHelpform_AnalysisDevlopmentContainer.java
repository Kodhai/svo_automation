package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SSEC_2432_ClassicPartsOwnerHelpform_AnalysisDevlopmentContainer {

	@FindBy(how = How.XPATH, using = "//slot[text()='Parts and Technical Queue']//parent::span")
	public WebElement queue;

	@FindBy(how = How.XPATH, using = "//button[text()='Delete']")
	public WebElement Delete;

	@FindBy(how = How.XPATH, using = "//span[text()='Delete']")
	public WebElement deletebtn;

	@FindBy(how = How.XPATH, using = "//button[@name='Edit']")
	public WebElement Edit;

	@FindBy(how = How.XPATH, using = "(//input[@class=' input'])[1]")
	public WebElement Firstname;

	@FindBy(how = How.XPATH, using = "(//span[text()='Save'])[2]")
	public WebElement Save;

	@FindBy(how = How.XPATH, using = "//label[text()='Upload']")
	public WebElement Upload;
}
