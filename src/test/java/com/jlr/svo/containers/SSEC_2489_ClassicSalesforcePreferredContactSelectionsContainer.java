package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SSEC_2489_ClassicSalesforcePreferredContactSelectionsContainer {

	@FindBy(how = How.XPATH, using="//span[text()='Enquiries']")
	public WebElement EnquiriesTab;
	
	@FindBy(how = How.XPATH, using = "//div[text()='New']")
	public WebElement NewBtn;
	
	@FindBy(how = How.XPATH, using = "//button[@title='Edit Preferred Contact']")
	public WebElement EditPreferredContact;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Clear Selection']")
	public WebElement ClearSelectionBtn;
	
	@FindBy(how = How.XPATH, using = "//span[@class='slds-listbox__option-text slds-listbox__option-text_entity']//following-sibling::span")
	public WebElement PreferredContactDropDown;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Email Address']")
	public WebElement EmailAdd;
	
	@FindBy(how = How.XPATH, using = "//button[@name='SaveEdit']")
	public WebElement SaveEditBtn;
	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
//	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
	
	
	
	
	
	
}
