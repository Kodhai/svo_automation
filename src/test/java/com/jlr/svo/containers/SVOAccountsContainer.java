package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SVOAccountsContainer {

	@FindBy(how = How.XPATH, using = "(//span[text()='Enquiries'])[1]")
	public WebElement EnquiryButton;

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Enquiries')])[3]")
	public WebElement RelLinkEnquiries;

	@FindBy(how = How.XPATH, using = "//*[@name='SO_Enquiry__c-search-input']")
	public WebElement EnquirySearchBox;

	@FindBy(how = How.XPATH, using = "(//div[@title='New'])[2]")
	public WebElement NewEnquiry;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Subject__c']")
	public WebElement EnqTitle;

	@FindBy(how = How.XPATH, using = "//p[text()='No items to display.']")
	public WebElement NoItems;

	@FindBy(how = How.ID, using = "username")
	public WebElement userNameTextBox;

	@FindBy(how = How.ID, using = "password")
	public WebElement passwordTextBox;

	@FindBy(how = How.ID, using = "Login")
	public WebElement loginBtn;

	@FindBy(how = How.ID, using = "emc")
	public WebElement verificationCode;

	@FindBy(how = How.ID, using = "save")
	public WebElement verifyBtn;

	@FindBy(how = How.XPATH, using = "(//span[@class='slds-truncate'])[1]")
	public WebElement SVOText;

	@FindBy(how = How.ID, using = "p")
	public WebElement mobileNumText;

	@FindBy(how = How.LINK_TEXT, using = "Remind Me Later")
	public WebElement remindMeLater;

	@FindBy(how = How.LINK_TEXT, using = "I Don't Want to Register My Phone")
	public WebElement notRegister;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-show']")
	public WebElement menuBtn;

	@FindBy(how = How.XPATH, using = "//input[@class='slds-input']")
	public WebElement searchBox;

	public String selectMenu(String text) {
		return "//b[text()='" + text + "']";
	}

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Sign in')]")
	public WebElement outlookSignin;

	@FindBy(how = How.NAME, using = "loginfmt")
	public WebElement outlookUsername;

	@FindBy(how = How.XPATH, using = "//input[@value='Next']")
	public WebElement outlookNext;

	@FindBy(how = How.XPATH, using = "(//a[contains(text(),'Employee')])[1]")
	public WebElement outlookEmp;

	@FindBy(how = How.XPATH, using = "//i[contains(@class,'ms-ohp-Icon--outlookLogo ms-ohp-Icon--outlookLogoFill')]")
	public WebElement outlookIcon;

	@FindBy(how = How.XPATH, using = "(//div[contains(@aria-label,'noreply@salesforce.com')])[1]")
	public WebElement outlookEmail;

	@FindBy(how = How.XPATH, using = "//div[@class='PlainText']")
	public WebElement outlookVerificationCode;

	@FindBy(how = How.XPATH, using = "//input[@id='emc']")
	public WebElement vCodeTextBox;

	@FindBy(how = How.XPATH, using = "//a[@title='Select List View']")
	public WebElement listView;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Recently Viewed']")
	public WebElement recentlyViewedAccounts;
	
	@FindBy(how = How.XPATH, using = "(//span[text()='All Opportunities'])[1]")
	public WebElement allOpportunity;

	@FindBy(how = How.XPATH, using = "(//span[text()='All Enquiries'])[1]")
	public WebElement AllEnquiries;

	@FindBy(how = How.XPATH, using = "//span[@title='Owner Alias']")
	public WebElement Owner;

	@FindBy(how = How.XPATH, using = "//span[@title='SLA Status']")
	public WebElement Status;

	@FindBy(how = How.XPATH, using = "//span[@title='JLR Retail Case Management']")
	public WebElement jlrText;

	@FindBy(how = How.XPATH, using = "//*[@class='icon noicon']")
	public WebElement icon_image;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Log Out')]")
	public WebElement Logout;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Accounts Menu')]")
	public WebElement Accounts;

	@FindBy(how = How.XPATH, using = "//a[@title='Contacts']")
	public WebElement Contacts;

	@FindBy(how = How.XPATH, using = "(//a[contains(@href,'/lightning/r/AccountContactRelation/')])[1]")
	public WebElement RelatedContacts;

	@FindBy(how = How.XPATH, using = "//span[contains(@title,'in Contacts')]")
	public WebElement searchContact;
	
	@FindBy(how = How.XPATH, using = "//div[@title='Add Relationship']")
	public WebElement AddRelation;

	@FindBy(how = How.XPATH, using = "(//input[@placeholder='Search Contacts...'])")
	public WebElement SearchContacts;

	@FindBy(how = How.XPATH, using = "//button[@title='Show quick filters']")
	public WebElement FilterAccounts;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_SAP_Account__c-SO_SAP_ID__c']")
	public WebElement SAPid;

	@FindBy(how = How.XPATH, using = "//button[@title='Apply']")
	public WebElement Apply;

	@FindBy(how = How.XPATH, using = "//button[@title='Close Filters']")
	public WebElement CloseFilter;

	@FindBy(how = How.XPATH, using = "(//span[text()='Show Actions'])")
	public WebElement ShowActions;
	
	@FindBy(how = How.XPATH, using = "//button[@name='Delete']")
	public WebElement Delete;

	@FindBy(how = How.XPATH, using = "//span[text()='Delete']")
	public WebElement DeleteAccount;
	
	@FindBy(how = How.XPATH, using = "(//a[contains(@title,'Individual')])[1]")
	public WebElement firstAccountIndividual;

    @FindBy(how = How.XPATH, using = "(//button[@class='slds-button slds-button_icon-border-filled'])[3]")
    public WebElement sideMenu;
	
	@FindBy(how = How.XPATH, using = "//a[contains(text(),'FIN')]")
	public WebElement FIN;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Show All')]")
	public WebElement ShowAll;

	@FindBy(how = How.XPATH, using = "//a[contains(@href,'related/Orders/view')]")
	public WebElement OrdersSection;

	@FindBy(how = How.XPATH, using = "//span[text()='Order Number']")
	public WebElement OrderNumber;

	@FindBy(how = How.XPATH, using = "//span[text()='Order Record Type']")
	public WebElement OrderRecordType;

	@FindBy(how = How.XPATH, using = "(//span[text()='Status'])")
	public WebElement OrderStatus;

	@FindBy(how = How.XPATH, using = "//span[text()='Brand']")
	public WebElement OrderBrand;

	@FindBy(how = How.XPATH, using = "//span[text()='Model']")
	public WebElement OrderModel;

	@FindBy(how = How.XPATH, using = "(//div[text()='New'])[4]")
	public WebElement NewOrder;

	@FindBy(how = How.XPATH, using = "(//button[@title='Save'])")
	public WebElement SaveRelation;

	public String addedRelationship(String text) {
		return "//a[@title='" + text + "']";
	}

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Vehicles Owned')])[1]")
	public WebElement VehiclesOwned;

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Vehicles Driven')])[1]")
	public WebElement VehiclesDriven;

	@FindBy(how = How.XPATH, using = "(//a[@title='New'])[2]")
	public WebElement NewVehiclesDriven;

	@FindBy(how = How.XPATH, using = "(//div[@title='New'])[2]")
	public WebElement NewVehiclesOwned;
	
	@FindBy(how = How.XPATH, using = "(//a[@title='New'])[2]")
	public WebElement NewKMI;

	@FindBy(how = How.XPATH, using = "//span[text()='Driver']")
	public WebElement Driveradd;

	@FindBy(how = How.XPATH, using = "//span[text()='Owner']")
	public WebElement Owneradd;

	@FindBy(how = How.XPATH, using = "//span[text()='Next']")
	public WebElement DriverNext;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Vehicle Manufacturers...']")
	public WebElement brand;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Vehicle Models...']")
	public WebElement model;

	@FindBy(how = How.XPATH, using = "//button[@name='SaveEdit']")
	public WebElement SaveVehicle;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Contacts...']")
	public WebElement ContactSearch;
	
	@FindBy(how = How.XPATH, using = "//span[contains(@title,'Show All Results')]")
	public WebElement showAllResultForContact;
	
	@FindBy(how = How.XPATH, using = "(//span[contains(@title,'Show All Results')])[2]")
	public WebElement showAllResultForBrand;
	
	@FindBy(how = How.XPATH, using = "(//span[contains(@title,'Show All Results')])[3]")
	public WebElement showAllResultForModel;

	@FindBy(how = How.XPATH, using = "//img[@class='slds-truncate checked']")
	public WebElement Vehicleaddverify;
	
	@FindBy(how = How.XPATH, using = "(//a[contains(@title,'VO')])[1]")
	public WebElement firstVehicleOwnership;
	
	@FindBy(how = How.XPATH, using = "//a[contains(@title,'KMI')]")
	public WebElement verifyKMI;
	
	@FindBy(how = How.XPATH, using = "(//a[@class='flex-wrap-ie11']//child::span[contains(text(),'Test')])[1]")
	public WebElement KMIContact;

	@FindBy(how = How.XPATH, using = "//input[@name='Account-search-input']")
	public WebElement SearchBox;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Start_Date__c']")
	public WebElement StartDate;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_End_Date__c']")
	public WebElement EndDate;

	@FindBy(how = How.XPATH, using = "//button[@name='SaveAndNew']")
	public WebElement SaveandNew;

	@FindBy(how = How.XPATH, using = "(//span[text()='Show Actions'])[2]")
	public WebElement ShowAction;

	@FindBy(how = How.XPATH, using = "//button[@name='Delete']")
	public WebElement DeleteVehicle;

	@FindBy(how = How.XPATH, using = "//span[text()='Delete']")
	public WebElement ConfDeleteVehicle;

	@FindBy(how = How.XPATH, using = "//div[@title='New']")
	public WebElement NewAccount;

	public String selectRecordType(String record) {
		return "//span[contains(text(),'" + record + "')]";
	}

	@FindBy(how = How.XPATH, using = "//span[text()='Next']")
	public WebElement nextRType;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[2]")
	public WebElement salutation;

	public String ValueSelection(String value) {
		return "//span[@title='" + value + "']";
	}

	@FindBy(how = How.XPATH, using = "//input[@name='firstName']")
	public WebElement FirstName;

	@FindBy(how = How.XPATH, using = "//input[@name='lastName']")
	public WebElement LastName;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[3]")
	public WebElement newRegion;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[4]")
	public WebElement newSalesArea;
	
	@FindBy(how = How.XPATH, using = "//button[@title='Close this window']")
	public WebElement CloseWindow;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[5]")
	public WebElement newMarket;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Accounts...']")
	public WebElement newPreferredRet;
	
	@FindBy(how = How.XPATH, using = "(//button[text()='View all dependencies'])[1]")
	public WebElement viewAllDependencies;
	
	@FindBy(how = How.XPATH, using = "//span[contains(@title,'Show All Results for')]")
	public WebElement newPreferredRetSearch;

	public String clientNameSearch(String client) {
		return "(//a[@title='" + client + "'])[1]";
	}

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[8]")
	public WebElement BillingAddress;

	@FindBy(how = How.XPATH, using = "//span[@title='UK']")
	public WebElement BillingAddressSearch;

	@FindBy(how = How.XPATH, using = "(//textarea[@class='slds-textarea'])[3]")
	public WebElement ClientBkgd;

	@FindBy(how = How.XPATH, using = "(//textarea[@class='slds-textarea'])[4]")
	public WebElement Interests;

	@FindBy(how = How.XPATH, using = "(//textarea[@class='slds-textarea'])[5]")
	public WebElement BusinessInt;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input'])[18]")
	public WebElement AddLine;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input'])[24]")
	public WebElement AddState;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input'])[27]")
	public WebElement AddCountry;

	@FindBy(how = How.XPATH, using = "//button[@name='SaveEdit']")
	public WebElement saveAccount;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[7]")
	public WebElement ContactDetails;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input'])[8]")
	public WebElement newEmail;

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'SAP Accounts')])[1]")
	public WebElement SAPAccounts;

	@FindBy(how = How.XPATH, using = "(//div[@title='New'])[2]")
	public WebElement NewSAP;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input'])[2]")
	public WebElement SAPID;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[2]")
	public WebElement SAPStatus;

	@FindBy(how = How.XPATH, using = "//button[@name='SaveEdit']")
	public WebElement saveSAP;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[2]")
	public WebElement AccName;

	@FindBy(how = How.XPATH, using = "//span[contains(@title,'Show All Results for')]")
	public WebElement AccNameSearch;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Vehicle Manufacturers...']")
	public WebElement newManufacturer;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Vehicle Models...']")
	public WebElement newModel;

	@FindBy(how = How.XPATH, using = "(//span[contains(@title,'Show All Results for')])[2]")
	public WebElement newManufacturerSearch;

	@FindBy(how = How.XPATH, using = "(//span[contains(@title,'Show All Results for')])[3]")
	public WebElement newModelSearch;

	public String brandSearch(String brand) {
		return "//a[@title='" + brand + "']";
	}

	@FindBy(how = How.XPATH, using = "//button[@name='CancelEdit']")
	public WebElement cancelVehicle;

	@FindBy(how = How.XPATH, using = "(//a[contains(text(),'Show All')])")
	public WebElement showAllButton;

	@FindBy(how = How.XPATH, using = "//a[contains(@href,'related/Client_Enquiries__r/view')]")
	public WebElement EnquiriesLink;

	@FindBy(how = How.XPATH, using = "//a[contains(@href,'Opportunities/view')]")
	public WebElement OpportunitiesLink;

	@FindBy(how = How.XPATH, using = "//a[contains(@href,'Client_Opportunities__r/view')]")
	public WebElement SVOClientOpportunitiesLink;

	@FindBy(how = How.XPATH, using = "//a[contains(@href,'related/KMIs__pr/view')]")
	public WebElement KMI;

	@FindBy(how = How.XPATH, using = "(//div[@class='emptyContent slds-is-absolute'])")
	public WebElement NoItemstoDisplay;

	@FindBy(how = How.XPATH, using = "//button[@title='Clear Selection']")
	public WebElement ClearSelection;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[2]")
	public WebElement ProductOff;

	public String SelectValue(String text) {
		return "//span[@title='" + text + "']";
	}

	@FindBy(how = How.XPATH, using = "(//a[contains(text(),'Show All')])")
	public WebElement showAll;

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Notes')])[1]")
	public WebElement Notes;

	@FindBy(how = How.XPATH, using = "(//div[@title='New'])[2]")
	public WebElement NewNotes;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Untitled Note']")
	public WebElement NoteTitle;

	@FindBy(how = How.XPATH, using = "//div[@data-placeholder='Enter a note...']")
	public WebElement NoteDesc;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Done')]")
	public WebElement Done;

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Account History')])[1]")
	public WebElement AccHistory;

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Account History')])[1]")
	public WebElement PerAccHistory;

	@FindBy(how = How.XPATH, using = "//button[@name='Account.SO_Update_Consent']")
	public WebElement UpdateConsent;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[2]")
	public WebElement Consent;

	@FindBy(how = How.XPATH, using = "//lightning-base-combobox-item[@data-value='Yes']")
	public WebElement ConsentYes;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-button_brand']")
	public WebElement ConsentNext;

	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Finish')]")
	public WebElement ConsentFinish;

	@FindBy(how = How.XPATH, using = "(//button[@class='slds-button slds-button_icon-border-filled'])[1]")
	public WebElement DropdownBtn;

	@FindBy(how = How.XPATH, using = "(//span[text()='Sharing'])")
	public WebElement SharingBtn;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search User...']")
	public WebElement SearchUser;

	@FindBy(how = How.XPATH, using = "//li[@class='lookup__item  default uiAutocompleteOption forceSearchInputLookupDesktopOption']")
	public WebElement SelectUser;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[2]")
	public WebElement AccAccess;

	@FindBy(how = How.XPATH, using = "(//span[text()='Read/Write'])[1]")
	public WebElement AccRWAccess;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[4]")
	public WebElement ContactAccess;

	@FindBy(how = How.XPATH, using = "(//span[text()='Read Only'])[2]")
	public WebElement ContactROAccess;

	@FindBy(how = How.XPATH, using = "//button[text()='Save']")
	public WebElement SaveShare;

	@FindBy(how = How.XPATH, using = "//p[text()='No items to display.']")
	public WebElement AccError;

	@FindBy(how = How.XPATH, using = "(//button[@name='Edit'])")
	public WebElement EditBtn;

	@FindBy(how = How.XPATH, using = "//input[@name='Account-search-input']")
	public WebElement SearchAccounts;

	public String selectUserAccount(String User) {
		return "(//a[@title='" + User + "'])[1]";
	}

	@FindBy(how = How.XPATH, using = "//span[text()='SAP Accounts']")
	public WebElement SAPAccountlink;

	@FindBy(how = How.XPATH, using = "(//div[text()='New'])[2]")
	public WebElement NewSAPAccount;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_SAP_ID__c']")
	public WebElement SAPIDName;

	@FindBy(how = How.XPATH, using = "(//div[@class='slds-combobox__form-element slds-input-has-icon slds-input-has-icon_right'])[2]")
	public WebElement newStatus;

	public String StatusSelection(String status) {
		return "//span[@title='" + status + "']";
	}

	@FindBy(how = How.XPATH, using = "(//div[@class='slds-combobox__form-element slds-input-has-icon slds-input-has-icon_right'])[4]")
	public WebElement newLocation;

	public String LocationSelection(String location) {
		return "//span[@title='" + location + "']";
	}

	@FindBy(how = How.XPATH, using = "//span[text()='Next']")
	public WebElement Next;

	@FindBy(how = How.XPATH, using = "//input[@title='Search Opportunities']")
	public WebElement SearchOpportunities;
	
	@FindBy(how = How.XPATH, using = "//button[@name='apply']")
	public WebElement applyBtn;

	@FindBy(how = How.XPATH, using = "(//span[text()='Date Picker'])[1]")
	public WebElement OrderStartDate;
	
	@FindBy(how = How.XPATH, using = "//a[text()='Add Username']")
	public WebElement addUserName;

	@FindBy(how = How.XPATH, using = "//button[text()='Today']")
	public WebElement Today;

	@FindBy(how = How.XPATH, using = "(//span[text()='Save'])[4]")
	public WebElement SaveOrder;

	public String RolesSelection(String role) {
		return "//span[text()='" + role + "']";
	}

	@FindBy(how = How.XPATH, using = "//button[@title='Move selection to Chosen']")
	public WebElement ChooseRoles;

	@FindBy(how = How.XPATH, using = "//a[text()='--None--']")
	public WebElement RelatnStrength;

	public String StrengthSelection(String Strength) 
	{
		return "//a[text()='" + Strength + "']";
	}

	@FindBy(how = How.XPATH, using = "(//span[text()='Save'])[3]")
	public WebElement SaveDetails;
	
	@FindBy(how = How.XPATH, using = "//button[@title='Save']")
	public WebElement SaveRelationship;

	@FindBy(how = How.XPATH, using = "//input[@name='AccountContactRelation-Contact.Name']")
	public WebElement ContactName;

	@FindBy(how = How.XPATH, using = "(//span[text()='Show Actions'])[7]")
	public WebElement Showactions;

	@FindBy(how = How.XPATH, using = "(//div[text()='Delete'])")
	public WebElement delete;

	@FindBy(how = How.XPATH, using = "(//div[text()='Remove Relationship'])")
	public WebElement RemoveRelationship;

	@FindBy(how = How.XPATH, using = "(//span[text()='Remove Relationship'])")
	public WebElement RemoveRelationshipBtn;

	public String ContactSelection(String Contact) {
		return "//span[text()='" + Contact + "']";
	}

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'in Contacts')])")
	public WebElement BrowseContacts;

	@FindBy(how = How.XPATH, using = "//input[@name='Contact-search-input']")
	public WebElement SearchBoxContact;

	public String Value(String text) {
		return "//lightning-formatted-name[contains(text(),'" + text + "')]";
	}

	public String Value2(String text) {
		return "(//a[contains(text(),'" + text + "')])";
	}

	@FindBy(how = How.XPATH, using = "//button[@name='ChangeOwnerOne']")
	public WebElement ChangeOwner;

	@FindBy(how = How.XPATH, using = "//input[@title='Search Users']")
	public WebElement SearchUsers;
	
	@FindBy(how = How.XPATH, using = "//span[contains(@title,'in Users')]")
	public WebElement ChOwnerSearchUser;

	@FindBy(how = How.XPATH, using = "//input[@name='TransferOthersOpenOpportunities']")
	public WebElement chkbox1;

	@FindBy(how = How.XPATH, using = "//input[@name='TransferOwnedClosedOpportunities']")
	public WebElement chkbox2;

	@FindBy(how = How.XPATH, using = "//input[@name='TransferOwnedOpenCases']")
	public WebElement chkbox3;

	@FindBy(how = How.XPATH, using = "//input[@name='TransferAllOwnedCases']")
	public WebElement chkbox4;

	@FindBy(how = How.XPATH, using = "//input[@name='SendEmail']")
	public WebElement chkbox5;

	@FindBy(how = How.XPATH, using = "//span[text()='Submit']")
	public WebElement Submit;

	public String ValueSelection2(String value) {
		return "//span[contains(text(),'" + value + "')]";
	}

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-button_icon-border-filled']")
	public WebElement ShowMore;

	@FindBy(how = How.XPATH, using = "//span[text()='Change Record Type']")
	public WebElement ChangeRecordType;

	@FindBy(how = How.XPATH, using = "//a[contains(@title,'VO')]")
	public WebElement OwnedVehicleID;

	@FindBy(how = How.XPATH, using = "//div[text()='TOPIC']")
	public WebElement Topixtable;

	@FindBy(how = How.XPATH, using = "//div[@class='setupGear']")
	public WebElement setupGear;

	@FindBy(how = How.XPATH, using = "//li[@id='related_setup_app_home']")
	public WebElement setup;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Setup']")
	public WebElement roleSearch;

	@FindBy(how = How.XPATH, using = "//input[@title='Login']")
	public WebElement roleLogin;

	@FindBy(how = How.XPATH, using = "//span[text()='Relationship Strength']")
	public WebElement RelationStrenght;

	@FindBy(how = How.XPATH, using = "//button[@title='Cancel']")
	public WebElement Cancel;
	
	@FindBy(how = How.XPATH, using = "//lightning-formatted-name[@data-output-element-id='output-field']")
	public WebElement IndividualAccountName;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Opportunities Menu')]")
	public WebElement Opportunity;

	@FindBy(how = How.XPATH, using = "//input[@name='Opportunity-search-input']")
	public WebElement SearchOpportunity;

	@FindBy(how = How.XPATH, using = "(//a[@data-refid='recordId'])[1]")
	public WebElement OpportunitySel1;

	@FindBy(how = How.XPATH, using = "(//button[@class='slds-button slds-button_icon-border-filled'])[2]")
	public WebElement showmore;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Vehicle Search')]")
	public WebElement VehicleSearch;

	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Vehicle Search')]")
	public WebElement VehicleSearchbutton;

	@FindBy(how = How.XPATH, using = "//input[@name='Brands']")
	public WebElement VehicleBrandSearch;

	@FindBy(how = How.XPATH, using = "(//button[@class='slds-button slds-button_brand'])[1]")
	public WebElement Search;

	@FindBy(how = How.XPATH, using = "(//button[@class='slds-button slds-button_brand'])[2]")
	public WebElement NewBrand;

	@FindBy(how = How.XPATH, using = "(//input[@class=' input'])[1]")
	public WebElement RecordName;

	@FindBy(how = How.XPATH, using = "(//input[@class=' input'])[3]")
	public WebElement VIN;

	@FindBy(how = How.XPATH, using = "(//span[@class='test-id__field-value slds-form-element__static slds-grow '])[1]")
	public WebElement VerRecord;

	@FindBy(how = How.XPATH, using = "(//span[@class='test-id__field-value slds-form-element__static slds-grow '])[3]")
	public WebElement VerBrand;

	@FindBy(how = How.XPATH, using = "(//span[@class='test-id__field-value slds-form-element__static slds-grow '])[4]")
	public WebElement VerVIN;

	@FindBy(how = How.XPATH, using = "//a[@title='Design Brief & Quote']")
	public WebElement DesignBrief;

	@FindBy(how = How.XPATH, using = "//span[text()='Mark as Current Stage']")
	public WebElement MarkasCurrentStage;

	@FindBy(how = How.XPATH, using = "//div[@class='slds-theme--info slds-notify--toast slds-notify slds-notify--toast forceToastMessage']")
	public WebElement errorFlow;

	@FindBy(how = How.XPATH, using = "//button[@title='Close']")
	public WebElement errorclose;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Clone')]")
	public WebElement cloneopp;

	@FindBy(how = How.XPATH, using = "//div[@title='New Contact']")
	public WebElement AddContact;

	@FindBy(how = How.XPATH, using = "//a[@data-tab-name='Closed']")
	public WebElement oppClosed;

	@FindBy(how = How.XPATH, using = "//span[text()='Select Closed Stage']")
	public WebElement oppClosedStage;

	@FindBy(how = How.XPATH, using = "//input[@name='StageName']")
	public WebElement oppStage;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Lost_Reason__c']")
	public WebElement OppLostReason;

	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
	public WebElement DoneButton;

	@FindBy(how = How.XPATH, using = "//button[@title='Change Record Type']")
	public WebElement changeRecType;

	@FindBy(how = How.XPATH, using = "//span[text()='Classic Reborn']")
	public WebElement ClassicReborn;

	@FindBy(how = How.XPATH, using = "//div[@class='genericNotification']")
	public WebElement error;

	@FindBy(how = How.XPATH, using = "//a[@data-tab-name='Contracting']")
	public WebElement oppContracting;

	@FindBy(how = How.XPATH, using = "(//input[@name='Name'])")
	public WebElement AccountName;

	@FindBy(how = How.XPATH, using = "(//input[@name='RTC_Email__c'])")
	public WebElement AccEmail;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[3]")
	public WebElement CorpRegion;

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Show All Results for')])[1]")
	public WebElement OwnManufacturerSearch;

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Show All Results for')])[2]")
	public WebElement OwnModelSearch;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Records...']")
	public WebElement newVehicle;

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Show All Results for')])[3]")
	public WebElement VehicleSearchresults;

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'SVO Client Opportunities')])[1]")
	public WebElement SVOClientOpp;

	@FindBy(how = How.XPATH, using = "(//div[@title='New'])[2]")
	public WebElement NewSVOOpp;

	@FindBy(how = How.XPATH, using = "//div[@class='recordTypeName slds-grow slds-truncate']")
	public WebElement SVORecordType;

	@FindBy(how = How.XPATH, using = "(//input[@name='Name'])")
	public WebElement OpportunityName;

	@FindBy(how = How.XPATH, using = "(//input[@name='CloseDate'])")
	public WebElement OpportunityCloseDate;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[2]")
	public WebElement OpportunityStage;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[3]")
	public WebElement OpportunityproOffering;

	@FindBy(how = How.XPATH, using = "(//input[@placeholder='Search Accounts...'])[1]")
	public WebElement OpportunityAccountName;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[4]")
	public WebElement OppRegion;

	@FindBy(how = How.XPATH, using = "(//input[@placeholder='Search Contacts...'])[1]")
	public WebElement preferredCnt;

	@FindBy(how = How.XPATH, using = "(//span[contains(@title,'Show All Results for')])[1]")
	public WebElement pCntSearch;

	@FindBy(how = How.XPATH, using = "(//span[contains(@title,'Show All Results for')])[2]")
	public WebElement CpCntSearch;

	public String OppAccountSearchselect(String Account) {
		return "//a[@title='" + Account + "']";
	}

	public String OpportunityRecordType(String record) {
		return "//span[text()='" + record + "']";
	}

	@FindBy(how = How.XPATH, using = "(//span[contains(@title,'Show All Results for')])[5]")
	public WebElement brandSearch;

	@FindBy(how = How.XPATH, using = "(//span[contains(@title,'Show All Results for')])[6]")
	public WebElement modelSearch;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[23]")
	public WebElement Opportunitysource;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[10]")
	public WebElement OpportunitysourceBS;

	@FindBy(how = How.XPATH, using = "(//span[contains(@title,'Show All Results for')])[6]")
	public WebElement brandSearchBS;

	@FindBy(how = How.XPATH, using = "(//span[contains(@title,'Show All Results for')])[7]")
	public WebElement modelSearchBS;

	@FindBy(how = How.XPATH, using = "//button[@name='AccountHierarchy']")
	public WebElement AccHierarchy;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Parent Account']")
	public WebElement ParentAcc;

	@FindBy(how = How.XPATH, using = "(//input[@placeholder='Search Accounts...'])[1]")
	public WebElement parent;

	@FindBy(how = How.XPATH, using = "(//span[contains(@title,'Show All Results for')])[1]")
	public WebElement parentsearch;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Accounts Menu')]")
	public WebElement AccountsTab;

	@FindBy(how = How.XPATH, using = "//input[@class=' input']")
	public WebElement name;

	@FindBy(how = How.XPATH, using = "(//a[@class='select'])[2]")
	public WebElement prodoffer;

	@FindBy(how = How.XPATH, using = "(//input[@placeholder='Search Vehicle Manufacturers...'])[2]")
	public WebElement probrand;

	@FindBy(how = How.XPATH, using = "(//input[@placeholder='Search Vehicle Models...'])[2]")
	public WebElement promodel;

	public String dropdown(String text) {
		return "//a[text()='" + text + "']";
	}

	@FindBy(how = How.XPATH, using = "//input[@class='input uiInputSmartNumber']")
	public WebElement BuildCap;

	@FindBy(how = How.XPATH, using = "//button[text()='S-Docs']")
	public WebElement SDoc;

	@FindBy(how = How.XPATH, using = "(//input[@type='checkbox'])[3]")
	public WebElement selectFile;

	@FindBy(how = How.XPATH, using = "//input[@type='submit']")
	public WebElement Nextstep;

	@FindBy(how = How.XPATH, using = "//input[@value='Generate Doc']")
	public WebElement GenDoc;

	@FindBy(how = How.XPATH, using = "//input[@value='E-Sign Documents In Person']")
	public WebElement ESign;

	@FindBy(how = How.XPATH, using = "//div[@title='Add Files']")
	public WebElement AddFiles;

	@FindBy(how = How.XPATH, using = "(//span[text()='Upload Files'])[2]")
	public WebElement UploadFiles;

	@FindBy(how = How.XPATH, using = "//span[@title='Files']")
	public WebElement Filesbutton;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Programmes...']")
	public WebElement Programmes;

	@FindBy(how = How.XPATH, using = "//lightning-base-combobox-item[@data-value='actionCreateNew']")
	public WebElement newProgrammes;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'New')]")
	public WebElement newOpportunityBtn;

	@FindBy(how = How.XPATH, using = "//a[@title='Go to next month']")
	public WebElement OpportunityNextMonth;

	@FindBy(how = How.XPATH, using = "(//span[text()='Restricted Party Screening'])")
	public WebElement restrictedPartyScreening;

	@FindBy(how = How.XPATH, using = "(//button[contains(text(),'View all dependencies')])[4]")
	public WebElement ViewallDependencies;

	@FindBy(how = How.XPATH, using = "//input[@name='SO_Region__c']")
	public WebElement Opportunityeditregion;

	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Apply')]")
	public WebElement Applybtn;

	@FindBy(how = How.XPATH, using = "(//input[@title='Search Contacts'])[1]")
	public WebElement preferredCntBS;

	@FindBy(how = How.XPATH, using = "(//input[@title='Search Accounts'])[2]")
	public WebElement OpportunityAccountNameBe;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'in Accounts')]")
	public WebElement OppAccountSearch;

	@FindBy(how = How.XPATH, using = "(//textarea[@class='slds-textarea'])[1]")
	public WebElement requestDetails;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Programmes...']")
	public WebElement ProgrammeSearch;

	@FindBy(how = How.XPATH, using = "(//div[@role='none'])[2]")
	public WebElement OppoStage;

	@FindBy(how = How.XPATH, using = "//span[text()='25' and @data-aura-class='uiDayInMonthCell--default']")
	public WebElement OpportunityEnddate;

	@FindBy(how = How.XPATH, using = "(//a[contains(text(),'Check cleared')])[1]")
	public WebElement Checkcleared;

	@FindBy(how = How.XPATH, using = "(//button[contains(text(),'S-Docs')])[2]")
	public WebElement SDocs;

	@FindBy(how = How.XPATH, using = "//a[@title='Select List View']")
	public WebElement selectlistview;

	@FindBy(how = How.XPATH, using = "(//a[@data-refid='recordId'])[1]")
	public WebElement FirstAccount;

	@FindBy(how = How.XPATH, using = "//a[@title='Select List View']")
	public WebElement AccountsSelectListView;

	@FindBy(how = How.XPATH, using = "//input[@type='text']")
	public WebElement AccountsSearchListTextBox;

	@FindBy(how = How.XPATH, using = "//a[@title='Commissioning Suite']")
	public WebElement CommissioningSuiteTab;

	@FindBy(how = How.XPATH, using = "//button[@value='SOCommissionDayView'] ")
	public WebElement DayButton;

	@FindBy(how = How.XPATH, using = "//button[text()='Today']")
	public WebElement TodayButton;

	@FindBy(how = How.XPATH, using = "(//div[@class='calendar_default_cell_inner'])[3]")
	public WebElement TimeSlot;

	@FindBy(how = How.XPATH, using = "(//input[@aria-autocomplete='none'])[6]")
	public WebElement StatusDropDown;

	public String StatusSelect(String Status) {
		return "//span[@title='" + Status + "']";
	}

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Commissioning Requests...']")
	public WebElement CommissioningReq;

	@FindBy(how = How.XPATH, using = "(//input[@placeholder='Search Contacts...'])[1]")
	public WebElement PrimaryContact;

	@FindBy(how = How.XPATH, using = "(//span[@class='slds-listbox__option-text slds-listbox__option-text_entity'])[3]")
	public WebElement Contact;

	@FindBy(how = How.XPATH, using = "//span[@title='Details']")
	public WebElement Details;

	@FindBy(how = How.XPATH, using = "(//button[@type='submit'])[2]")
	public WebElement SaveBtn;

	@FindBy(how = How.XPATH, using = "(//input[@aria-autocomplete='none'])[8]")
	public WebElement MeetingTypeDropDown;

	@FindBy(how = How.XPATH, using = "//input[@aria-label='Max. Attendees']")
	public WebElement MaxAttendees;

	@FindBy(how = How.XPATH, using = "(//div[@class='slds-media slds-listbox__option slds-listbox__option_entity slds-media_center'])[1]")
	public WebElement Request;

	@FindBy(how = How.XPATH, using = "//span[@title='Recurring Reservation']")
	public WebElement RecurringReservation;

	@FindBy(how = How.XPATH, using = "(//span[text()='All Classic Service Opportunities'])[2]")
	public WebElement ClassicService;

	@FindBy(how = How.XPATH, using = "//a[@class='menuTriggerLink slds-button slds-button_icon slds-button_icon slds-button_icon-container slds-button_icon-small slds-global-actions__setup slds-global-actions__item-action']")
	public WebElement setUpBtn;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Setup']")
	public WebElement SearchSetup;

	@FindBy(how = How.XPATH, using = "(//input[@value=' Login '])[1]")
	public WebElement UserLoginBtn;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-button_icon-border-filled']")
	public WebElement ChangeRTypeDW;

	@FindBy(how = How.XPATH, using = "//a[@name='ChangeRecordType']")
	public WebElement ChangeRType;

	@FindBy(how = How.XPATH, using = "//p[@class='detail']")
	public WebElement AccessDeniedMsg;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-button--neutral slds-button slds-button_neutral uiButton']")
	public WebElement cancelOpp;

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Opportunities')])[4]")
	public WebElement SVOOpp;

}