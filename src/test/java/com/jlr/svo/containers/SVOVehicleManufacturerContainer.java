package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SVOVehicleManufacturerContainer 
{
	@FindBy(how = How.XPATH, using = "//div[@class='slds-icon-waffle']")
	public WebElement sideMenu;
	
	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search apps and items...']")
	public WebElement seachBar;
	
	public String menuSelection(String Menu) {
		return "//b[text()='" + Menu + "']";
	}
	
	@FindBy(how = How.XPATH, using = "(//span[text()='Recently Viewed'])[1]")
	public WebElement recentlyViewed;
	
	@FindBy(how = How.XPATH, using = "//span[text()='All']")
	public WebElement allVehicleManufacturer;
	
	@FindBy(how = How.XPATH, using = "//div[@title='Change Owner']")
	public WebElement changeOwnerBtn;
	
	@FindBy(how = How.XPATH, using = "//div[@title='New']")
	public WebElement newVehicleManufacturer;
	
	@FindBy(how = How.XPATH, using = "(//a[contains(@class,'slds-truncate outputLookupLink slds-truncate outputLookupLink')])[1]")
	public WebElement firstVehicleManufacturer;
	
	@FindBy(how = How.XPATH, using = "(//button[@name='New'])")
	public WebElement NewVehicleSeries;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Next']")
	public WebElement nextRType;
	
	@FindBy(how = How.XPATH, using = "//input[@name='Name']")
	public WebElement VehicleName;
	
	@FindBy(how = How.XPATH, using = "//button[@name='SaveEdit']")
	public WebElement SaveVehicle;
	
	@FindBy(how = How.XPATH, using = "(//span[text()='View All'])[3]")
	public WebElement viewAllModel;
	
	@FindBy(how = How.XPATH, using = "(//span[text()='Show Actions']//parent::lightning-icon[@class='slds-icon-utility-down slds-icon_container']//parent::a[contains(@href,'javascript:void(0)')])[16]")
	public WebElement showActionsMenu;
	
	@FindBy(how = How.XPATH, using = "(//button[@name='Delete'])[2]")
	public WebElement deleteModel;
	
	@FindBy(how = How.XPATH, using = "//a[@class='flex-wrap-ie11 slds-truncate']")
	public WebElement firstModel;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Delete']")
	public WebElement confirmDeleteModel;
	
	//a[contains(@class,'keyboardMode--trigger')]//child::lightning-icon//child::span[text()='Show Actions']
	//(//span[text()='Show Actions']//parent::lightning-icon[@class='slds-icon-utility-down slds-icon_container']//parent::a[contains(@href,'javascript:void(0)')])[16]
	
	
}
