package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SSEC_2119_AdditionalVehicleName_AutoNumberContainer {

	@FindBy(how = How.XPATH, using = "(//span[starts-with(text(), 'Additional Vehicles')])[1]")
	public WebElement AdditionalVehicle;
	
	@FindBy(how = How.XPATH, using = "(//div[@title='New'])[2]")
	public WebElement NewBtnAdditionalVehicle;
	
	@FindBy(how = How.XPATH, using = "//button[@name='SaveEdit']")
	public WebElement SaveEditAdditionalVehicle;
	
	@FindBy(how = How.XPATH, using = "(//span[text()='Additional Vehicle Reference Number'])[2]")
	public WebElement AdditionalVehicleRefNum;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Opportunity']")
	public WebElement OpportunityText;
	
	@FindBy(how = How.XPATH, using = "//span[starts-with(text(), 'SVO-2022')]")
	public WebElement AdditionalVehicleVerify;
	
	@FindBy(how = How.XPATH, using = "//a[@title='Show 2 more actions']")
	public WebElement DropDownAdditionalVehicle;
	
	@FindBy(how = How.XPATH, using = "//a[@title='Edit']")
	public WebElement EditOptionAdditionalVehicle;
	
	@FindBy(how = How.XPATH, using = "//button[text()='Cancel']")
	public WebElement CancelEditAdditionalVehicle;
	
//	@FindBy(how = How.XPATH, using = "")
//	public WebElement 
	
	
}
