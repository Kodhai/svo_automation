package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SSEC_1533_ProvisionalOrderFormContainer {

	@FindBy(how = How.XPATH, using = "//span[text()='Classic Sales']//following-sibling::div")
	public WebElement ClassicSalesRecType;
	
	@FindBy(how = How.XPATH, using = "//span[text()= 'Provisional Order Form']")
	public WebElement ProvOrderFormText;
	
	@FindBy(how=How.XPATH, using = "//button[@title='Show Less']")
	public WebElement ShowLessBtn;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Provisional Order Form Signed']//parent::label")
	public WebElement ProvOrderFormCheckbox;

	@FindBy(how = How.XPATH, using = "//span[text()='Deposit Paid']//parent::label")
	public WebElement DepositPaidCheckbox;
	
	@FindBy(how = How.XPATH, using = "//span[text() = 'KMIs']//parent::a")
	public WebElement KMItab;
	
	@FindBy(how = How.XPATH, using = "//div[@title='New']")
	public WebElement NewKMIBtn;
	
	@FindBy(how = How.XPATH, using = "//button[@title='Edit Provisional Order Form Signed']")
	public WebElement EditProvOrderFormBtn;
	
	@FindBy(how = How.XPATH, using = "//input[@name='SO_Provisional_Order_Form_Signed__c']")
	public WebElement EditProvOrderCheckBox;
	
	@FindBy(how = How.XPATH, using = "//button[@name= 'SaveEdit']")
	public WebElement SaveEditBtn;
	
	@FindBy(how = How.XPATH, using = "(//span[text()='Provisional Order Form Signed'])[1]")
	public WebElement KMIProvOrder;
	
	@FindBy(how = How.XPATH, using = "//button[@title='Cancel']")
	public WebElement CancelCaseBtn;
	
	@FindBy(how = How.XPATH, using = "//button[@name='SaveEdit']")
	public WebElement SaveEditKMIBtn;
	
	@FindBy(how = How.XPATH, using = "//h2[text()='Related List Quick Links']")
	public WebElement QuickLinksText;
	
	
}
