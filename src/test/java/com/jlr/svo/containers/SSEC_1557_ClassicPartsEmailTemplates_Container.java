package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SSEC_1557_ClassicPartsEmailTemplates_Container {

	@FindBy(how = How.XPATH, using = "//a[@title='Insert a template...']")
	public WebElement InsertEmailTemplate;

	@FindBy(how = How.XPATH, using = "//input[@class='templateSearch input']")
	public WebElement SearchEmailTemplateTxtBx;

	@FindBy(how = How.XPATH, using = "//body[@class='cke_editable cke_editable_themed cke_contents_ltr cke_show_borders']")
	public WebElement EmailTemplateContent;

}
