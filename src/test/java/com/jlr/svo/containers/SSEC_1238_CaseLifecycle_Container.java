package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SSEC_1238_CaseLifecycle_Container {

	@FindBy(how = How.XPATH, using = "//a[@title='Cases']")
	public WebElement CasesTab;
	
	@FindBy(how = How.XPATH, using = "//div[@title='New']")
	public WebElement NewBtn;
	
	public String CaseRecordType(String value) {
		return "//span[text()='" + value + "']";
	}
	
	
	@FindBy(how = How.XPATH, using = "//span[text()='Next']")
	public WebElement NextBtn;
	
	@FindBy(how = How.XPATH, using = "//span[text()='VIN / Chassis Number']//parent::label//following-sibling::input")
	public WebElement ChassisNumber;
	
	@FindBy(how = How.XPATH, using = "(//button[@title='Save']//span)")
	public WebElement SaveBtn;
	
	@FindBy(how = How.XPATH, using = "//a[@title='Closed']")
	public WebElement ClosedStatus;
	
	@FindBy(how = How.XPATH, using = "//slot[@name='primaryField']")
	public WebElement CaseNumber;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Mark as Current Case Lifecycle']")
	public WebElement MarkCurrentCaseLifecycle;
	
	@FindBy(how = How.XPATH, using = "//button[@name='SO_Case_Closure_Reason__c']")
	public WebElement CaseClosureReasonDropdown;
	
	public String CaseClosureReason(String value) {
		return "//span[@title='" + value + "']";
	}
	
	@FindBy(how = How.XPATH, using = "//button[text()='Done']")
	public WebElement DoneBtn;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Status']//parent::div//following-sibling::div//child::span//child::slot//child::lightning-formatted-text")
	public WebElement CaseClosureStatus;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Case Lifecycle']//parent::div//following-sibling::div//child::span//child::slot//child::lightning-formatted-text")
	public WebElement CaseClosureLifecycle;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Case Closure Reason']//parent::div//following-sibling::div//child::span//child::slot//child::lightning-formatted-text")
	public WebElement CaseClosureReason;
	
	@FindBy(how = How.XPATH, using = "//button[@name='Edit']")
	public WebElement CaseEditBtn;
	
	@FindBy(how = How.XPATH, using = "(//a[text()='New'])[1]")
	public WebElement StatusTextBox;
	
	public String StatusSelection(String value) {
		return "//span[@title='" + value + "']";
	}
	
	@FindBy(how = How.XPATH, using = "//button[@title='Save']")
	public WebElement SaveEdit;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Mark Case Lifecycle as Complete']")
	public WebElement MarkCaseLifecycleAsComplete;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Status']//parent::div//following-sibling::div//following-sibling::button")
	public WebElement EditCaseClosureBtn;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Status']//parent::lightning-combobox//following-sibling::div//child::lightning-base-combobox//child::div//child::div//child::button")
	public WebElement CaseClosureStatusDetails;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Case Lifecycle']//parent::lightning-combobox//following-sibling::div//child::lightning-base-combobox//child::div//child::div//child::button")
	public WebElement CaseClosureLifecycleDetails;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Case Closure Reason']//parent::lightning-combobox//following-sibling::div//child::lightning-base-combobox//child::div//child::div//child::button")
	public WebElement CaseClosureReasonDetails;
	
	@FindBy(how = How.XPATH, using = "//button[@name='SaveEdit']")
	public WebElement SaveEditBtn;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Parts & Technical - All Cases'] ")
	public WebElement SelectPartsAndTechnicalQueue;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Customer Service - All Cases'] ")
	public WebElement SelectCustomerServiceQueue;
	
	public String QueueSelection(String value) {
		return "//span[text()='" + value + "']";
	}
	
	@FindBy(how = How.XPATH, using = "//input[@title='Vehicle Identification Number (VIN)']")
	public WebElement VINNumber;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Case Origin']//parent::div//following-sibling::div//child::span//child::slot//child::lightning-formatted-text")
	public WebElement CaseOrigin;
	
	@FindBy(how = How.XPATH, using = "//span[@class='toastMessage forceActionsText']")
	public WebElement ErrorMessageForWithoutVinNumber;
	
	@FindBy(how = How.XPATH, using = "//button[text()='Allow all']")
	public WebElement AllowCookies;
	

	@FindBy(how = How.XPATH, using = "//span[text()='First Name']//parent::label//following-sibling::div//input")
	public WebElement FirstNameTxtBx;

	@FindBy(how = How.XPATH, using = "//span[text()='Last Name']//parent::label//following-sibling::div//input")
	public WebElement LastNameTxtBx;

	@FindBy(how = How.XPATH, using = "//span[text()='Street Address']//parent::legend//following-sibling::div//input")
	public WebElement AddressTxtBx;

	@FindBy(how = How.XPATH, using = "//span[text()='City']//parent::label//following-sibling::div//input")
	public WebElement CityTxtBx;

	@FindBy(how = How.XPATH, using = "//span[text()=' Postcode']//parent::label//following-sibling::div//input")
	public WebElement PostCodeTxtBx;

	@FindBy(how = How.XPATH, using = "//span[text()='Phone Number']//parent::label//following-sibling::div//input")
	public WebElement PhoneNumberTxtBx;
	
	@FindBy(how = How.XPATH, using = "//button[@title='Submit']")
	public WebElement SubmitBtn;

	@FindBy(how = How.XPATH, using = "//div[@class='message-success success message']")
	public WebElement SubmitSuccessfullMessage;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Email']//parent::label//following-sibling::div//input")
	public WebElement InputEmailAddress;
	
	@FindBy(how = How.XPATH, using = "//select[@title='How can we help?']")
	public WebElement HelpSelect;
	
	public String SelectHowCanHelp() {
		return "//select[@title='How can we help?']";
	}
	
	@FindBy(how = How.XPATH, using = "//span[text()='Country']//parent::label//following-sibling::div//select")
	public WebElement CountrySelection;

	@FindBy(how = How.XPATH, using = "//textarea[@name='description']")
	public WebElement CommentText;
	
	@FindBy(how = How.XPATH, using = "//ul[@class='errorsList']")
	public WebElement VINNumberErrorMessage;
	
	@FindBy(how = How.XPATH, using = "//button[@title='Cancel']")
	public WebElement CancelBtn;
	
	@FindBy(how = How.XPATH, using = "//input[@name='RTC_VIN_Number__c']")
	public WebElement VINNumberOnCaseDetailsPage;
	


}
