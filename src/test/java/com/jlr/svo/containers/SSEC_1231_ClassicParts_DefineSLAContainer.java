package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SSEC_1231_ClassicParts_DefineSLAContainer {

	@FindBy(how = How.XPATH, using = "//p[text()='SLA Status']//parent::div//following-sibling::p//child::slot//child::records-formula-output//child::slot//child::formula-output-formula-html//child::lightning-formatted-rich-text//child::span//child::img")
	public WebElement SLAStatus;
}
