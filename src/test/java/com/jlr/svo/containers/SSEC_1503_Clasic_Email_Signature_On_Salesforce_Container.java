package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SSEC_1503_Clasic_Email_Signature_On_Salesforce_Container {

	@FindBy(how = How.XPATH, using = "//a[text()='View our Works Legends Stock']")
	public WebElement WorksLegendsStockLink;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Accept Recommended']")
	public WebElement AcceptRecommendedBtn;
	
	@FindBy(how = How.XPATH, using = "(//span[@class='value'])[1]")
	public WebElement WorksLegendsStockAvailableNumber;
	
	@FindBy(how = How.XPATH, using = "//a[text()='View our Works Legends Stock']")
	public WebElement WorksLegendsStockLinkOnEmail;
	
	@FindBy(how = How.XPATH, using = "//iframe[@title='accessibility title']")
	public WebElement EmailFrame;
	
	@FindBy(how = How.XPATH, using = "(//label//parent::th//following-sibling::td//span//input)[7]")
	public WebElement SubjectFieldWithotBuisinessUnit;
	
	
	
}
