package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SSEC_2085_Mass_delete_Classic_PartsContainer {

	@FindBy(how = How.XPATH, using = "//span[text()='Select item 1']")
	public WebElement SelectItemCheckbox;
	
	@FindBy(how = How.XPATH, using = "//div[text()='Mass Delete']")
	public WebElement MassDeleteBtn;
	
	@FindBy(how = How.XPATH, using = "//button[text()='Delete Records']")
	public WebElement DeleteRecordsBtn;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Select item 2']")
	public WebElement SelectItemCheckbox2;
	
	
}
