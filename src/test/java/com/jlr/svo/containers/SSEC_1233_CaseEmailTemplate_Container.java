package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SSEC_1233_CaseEmailTemplate_Container {

	@FindBy(how = How.XPATH, using = "//a[@id='activityTab__item']")
	public WebElement ActivityTab;

	public String TemplateSelection(String Template) {
		return "//button[@title='" + Template + "']";
	}

	@FindBy(how = How.XPATH, using = "//button//span[text()='Compose']")
	public WebElement ComposeEmailButton;

	@FindBy(how = How.XPATH, using = "//span[text()='To']//parent::label//following-sibling::div//input")
	public WebElement EmailToTxtBox;

	@FindBy(how = How.XPATH, using = "//span[text()='Subject']//parent::label//following-sibling::input")
	public WebElement EmailSubjectTxtBox;

	@FindBy(how = How.XPATH, using = "//div[@class='cuf-Icon']//lightning-icon[@class='slds-icon-utility-insert-template slds-button__icon slds-icon_container forceIcon']")
	public WebElement EmailTemplateIcon;

	@FindBy(how = How.XPATH, using = "//a[@title='Insert a template...']")
	public WebElement InsertEmailTemplate;

	@FindBy(how = How.XPATH, using = "//input[@class='templateSearch input']")
	public WebElement SearchEmailTemplateTxtBx;

	@FindBy(how = How.XPATH, using = "//button//span[text()='Send']")
	public WebElement EmailSendButton;

	@FindBy(how = How.XPATH, using = "//iframe[@class='cke_wysiwyg_frame cke_reset']")
	public WebElement EmailTemplateFrame;

	@FindBy(how = How.XPATH, using = "//body[@class='cke_editable cke_editable_themed cke_contents_ltr cke_show_borders']")
	public WebElement EmailTemplateContent;

	@FindBy(how = How.XPATH, using = "//body[@class='cke_editable cke_editable_themed cke_contents_ltr cke_show_borders']")
	public WebElement EditTemplate;

	public String TemplateMail(String TemplateEmail) {
		return "(//table[@class='F cf zt']//tr[@class='zA zE']//span[contains(text(),'" + TemplateEmail + "')])[1]";
	}

	@FindBy(how = How.XPATH, using = "//lightning-icon[@class='slds-icon-utility-delete slds-button__icon slds-icon_container forceIcon']")
	public WebElement RemoveEmailTemplate;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-button_brand']")
	public WebElement DiscordTemplate;

	@FindBy(how = How.XPATH, using = "(//button[@class='slds-pill__remove slds-button_reset'])[1]")
	public WebElement EMailToClearIcon;

	@FindBy(how = How.XPATH, using = "//span[text()='Review the errors on this page.']")
	public WebElement EmailSendError;

	@FindBy(how = How.XPATH, using = "(//button[@class='slds-pill__remove slds-button_reset'])[2]")
	public WebElement EMailBCCClearIcon;

}
