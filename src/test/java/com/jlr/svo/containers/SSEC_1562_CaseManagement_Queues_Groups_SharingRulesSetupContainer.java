package com.jlr.svo.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SSEC_1562_CaseManagement_Queues_Groups_SharingRulesSetupContainer {

	@FindBy(how=How.XPATH, using= "//div[text()='Capture enquiry details for a potential Classic Sales Opportunity.']")
	public WebElement ClassicSalesRecType;
	
	@FindBy(how=How.XPATH, using= "//slot[text()='Classic Sales Queue']//parent::span")
	public WebElement COClassicSales;
	
	@FindBy(how=How.XPATH, using= "//span[text()='Mark Status as Complete']//parent::button")
	public WebElement MarkCompleteBtn;
	
	@FindBy(how=How.XPATH, using= "//span[text()='Mark as Current Status']//parent::button")
	public WebElement MarkCurrentStatusBtn;
	
	@FindBy(how=How.XPATH, using= "//span[text()='Classic Service']")
	public WebElement ClassicServiceRecType;
	
	@FindBy(how=How.XPATH, using= "(//slot[text()='Classic Service Queue']//parent::span)[2]")
	public WebElement COClassicService;
	
	@FindBy(how=How.XPATH, using= "//span[text()='All Classic Service Cases']//parent::a")
	public WebElement AllClassicServ;
	
	@FindBy(how = How.XPATH, using="//input[@type='text']")
	public WebElement CaseDDSearchbox;
	
	@FindBy(how = How.XPATH, using= "//button[text()='Change Owner']")
	public WebElement ChangeOwnerBtn;
	
	@FindBy(how = How.XPATH, using= "//label[text()='Case Origin']")
	public WebElement CaseOriginLabel;
	
	@FindBy(how = How.XPATH, using= "//button[text()='Save' and @name='SaveEdit']")
	public WebElement SaveEditCaseBtn;
	
	@FindBy(how = How.XPATH, using= "//button[@name='cancel' and text()='Cancel']")
	public WebElement CancelCaseOwnerBtn;
	
	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Queues...']")
	public WebElement SearchQueues;
	
	@FindBy(how = How.XPATH, using="//button[@title='Submit']")
	public WebElement SubmitBtnCaseChngOwner;
	
	@FindBy(how = How.XPATH, using="//button[@name='ChangeOwnerOne']")
	public WebElement ChangeOwnerOneBtn;
}
