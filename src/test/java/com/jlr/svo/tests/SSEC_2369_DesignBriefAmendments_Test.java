package com.jlr.svo.tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.containers.SSEC_2369_DesignBriefAmendmentsContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.Then;

public class SSEC_2369_DesignBriefAmendments_Test extends TestBaseCC {

	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory.getLogger(SVOWorkOrderProgrammeField_Test.class.getName());
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
	com.jlr.svo.containers.SSEC_2369_DesignBriefAmendmentsContainer SSEC_2369_DesignBriefAmendmentsContainer = PageFactory.initElements(driver, SSEC_2369_DesignBriefAmendmentsContainer.class);

	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	public void onStart() {

		setupTest("SVOTest");
		driver = getDriver();

		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		SSEC_2369_DesignBriefAmendmentsContainer = PageFactory.initElements(driver, SSEC_2369_DesignBriefAmendmentsContainer.class);

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);


	}
	
    @Then("^Verify that donor section is present before the Seats section$")
    public void Verify_Donor_section_present_before_Seats_Section() throws Exception{
    	
    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	
    	VerifyHandler.verifyElementPresent(SSEC_2369_DesignBriefAmendmentsContainer.DonorSection);
    	ActionHandler.wait(1);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Donor section is present before the seats section");
    	
    	VerifyHandler.verifyElementPresent(SSEC_2369_DesignBriefAmendmentsContainer.SeatsSection);
    	ActionHandler.wait(1);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Seats section is present after the donor section");
    	
    	
    }

    @Then("^Verify that Scatter Cushion Color is present in the Interior section$")
    public void Verify_that_ScatterCushionColor_present_In_InteriorSection() throws Exception{
    	
    	ActionHandler.scrollToView(SSEC_2369_DesignBriefAmendmentsContainer.InteriorSection);
    	ActionHandler.wait(1);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User scrolls to view Interior section");
    	
    	VerifyHandler.verifyElementPresent(SSEC_2369_DesignBriefAmendmentsContainer.ScatterCushionColorText);
    	ActionHandler.wait(1);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Verify Scatter Cushion Color is present in the Interior section");
    	
    	
    }
    
    @Then("^Verify that Scatter Cushion Embroidery is present in the Interior section$")
    public void Verify_that_Scatter_Cushion_Embroidery_is_present() throws Exception{
    	
    	ActionHandler.scrollToView(SSEC_2369_DesignBriefAmendmentsContainer.InteriorSection);
    	ActionHandler.wait(1);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User scrolls to view Interior section");
    	
    	VerifyHandler.verifyElementPresent(SSEC_2369_DesignBriefAmendmentsContainer.ScatterCushionEmbroideryText);
    	ActionHandler.wait(1);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Verify Scatter Cushion Color is present in the Interior section");
    	
    }
    
    @Then("^Verify Comfort Pack Color is present in Interior section$")
    public void Verify_Comfort_pack_color_is_present() throws Exception{
    	
    	ActionHandler.scrollToView(SSEC_2369_DesignBriefAmendmentsContainer.InteriorSection);
    	ActionHandler.wait(1);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User scrolls to view Interior section");
    	
    	VerifyHandler.verifyElementPresent(SSEC_2369_DesignBriefAmendmentsContainer.ComfortPackColorText);
    	ActionHandler.wait(1);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Verify Scatter Cushion Color is present in the Interior section");
    	
    }
    
    @Then("^Verify Calf Rest Quantity is present with 2 options$")
    public void Verify_CalfRestQuantity_is_present() throws Exception{
    	
    	ActionHandler.scrollToView(SSEC_2369_DesignBriefAmendmentsContainer.InteriorSection);
    	ActionHandler.wait(1);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User scrolls to view Interior section");
    	
    	VerifyHandler.verifyElementPresent(SSEC_2369_DesignBriefAmendmentsContainer.CalfRestQuantityText);
    	ActionHandler.wait(1);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Verify Scatter Cushion Color is present in the Interior section");
    	
    	ActionHandler.click(SSEC_2369_DesignBriefAmendmentsContainer.CalfRestQuantityText);
    	ActionHandler.wait(1);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User scrolls to view Interior section");
    	
    	ActionHandler.click(SSEC_2369_DesignBriefAmendmentsContainer.SaveEditText);
    	ActionHandler.wait(1);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User scrolls to view Interior section");
    	
    }








}
