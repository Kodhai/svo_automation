package com.jlr.svo.tests;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.autotest.utils.Config;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.constants.Constants;
import com.jlr.svo.containers.SSEC_1227_CasesCreationContainer;
import com.jlr.svo.containers.SSEC_1237_CaseClosureContainer;
import com.jlr.svo.containers.SSEC_1238_CaseLifecycle_Container;
import com.jlr.svo.containers.SSEC_2294_ClassicPartsStatusUpdatefor_New_Customer_Container;
import com.jlr.svo.containers.SSEC_2371_Manage_Out_of_Office_Responses_Container;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SSEC_2371_Manage_OutofOfficeResponses_Milestone_Analysis_Development_Test extends TestBaseCC {

	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory.getLogger(SVOWorkOrderProgrammeField_Test.class.getName());
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	com.jlr.svo.containers.SSEC_2294_ClassicPartsStatusUpdatefor_New_Customer_Container SSEC_2294_ClassicPartsStatusUpdatefor_New_Customer_Response_EndToEndTesting_Container = PageFactory
			.initElements(driver, SSEC_2294_ClassicPartsStatusUpdatefor_New_Customer_Container.class);
	SSEC_1238_CaseLifecycle_Container CaseLifecycle_Container = PageFactory.initElements(driver,
			SSEC_1238_CaseLifecycle_Container.class);
	SSEC_1227_CasesCreationContainer CasesContainer = PageFactory.initElements(driver,
			SSEC_1227_CasesCreationContainer.class);
	SSEC_1237_CaseClosureContainer CaseClosureContainer = PageFactory.initElements(driver,
			SSEC_1237_CaseClosureContainer.class);
	SSEC_2371_Manage_Out_of_Office_Responses_Container SSEC_2371_Manage_Out_of_Office_Responses_Container = PageFactory
			.initElements(driver, SSEC_2371_Manage_Out_of_Office_Responses_Container.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);

	static double randomNum = getRandomIntegerBetweenRange(0, 10000);
	public static String verificationCode;
	public static String vCode;
	public static String EnquiryTitle;

	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	public void onStart() {

		setupTest("SVOTest");
		driver = getDriver();

		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		SSEC_2294_ClassicPartsStatusUpdatefor_New_Customer_Response_EndToEndTesting_Container = PageFactory
				.initElements(driver, SSEC_2294_ClassicPartsStatusUpdatefor_New_Customer_Container.class);
		SSEC_2371_Manage_Out_of_Office_Responses_Container = PageFactory.initElements(driver,
				SSEC_2371_Manage_Out_of_Office_Responses_Container.class);
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

	}

	@Then("^User Verify the mail test$")
	public void User_Verify_the_mail_test() throws Throwable {

		ActionHandler.click(SSEC_2371_Manage_Out_of_Office_Responses_Container.Inbox);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SSEC_2371_Manage_Out_of_Office_Responses_Container.firstmail);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

//		VerifyHandler.verifyElementPresent(SSEC_2371_Manage_Out_of_Office_Responses_Container.Verifythemail);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

	}

	@Given("^Access to outlook account with Username \"([^\"]*)\" and password \"([^\"]*)\"$")
	public void access_to_outlook_account_with_Username_and_password(String UserName, String Password)
			throws Throwable {

		onStart();
		driver = getDriver();
		driver.get(Constants.outlook);

		String s[] = UserName.split(",");
		UserName = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String p[] = Password.split(",");
		Password = CommonFunctions.readExcelMasterData(p[0], p[1], p[2]);

//		ActionHandler.click(SSEC_2371_Manage_Out_of_Office_Responses_Container.SignIn);
		ActionHandler.wait(5);
		ActionHandler.setText(SSEC_2371_Manage_Out_of_Office_Responses_Container.Username, UserName);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(1);
		ActionHandler.click(SSEC_2371_Manage_Out_of_Office_Responses_Container.outlookNext);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);

		ActionHandler.wait(1);
		ActionHandler.setText(SSEC_2371_Manage_Out_of_Office_Responses_Container.gmailPassword, Password);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(1);
		ActionHandler.click(SSEC_2371_Manage_Out_of_Office_Responses_Container.outlookNext);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User logged into outlook Account");

		ActionHandler.wait(2);
		ActionHandler.click(SSEC_2371_Manage_Out_of_Office_Responses_Container.outlookNext);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(10);
//		VerifyHandler.verifyElementPresent(SSEC_2371_Manage_Out_of_Office_Responses_Container.gmailText);

	}

	@Given("^Access to contact as form$")
	public void Access_to_contact_as_form() throws Throwable {

		onStart();
		driver = getDriver();
		ActionHandler.wait(5);
		driver.get(Constants.Contactus);

		Reporter.addStepLog("User tries to login to Gmail");
		CommonFunctions.attachScreenshot();

	}

	@Then("^User Fill the form with mandatory details$")
	public void User_Fill_the_form_with_mandatory_details() throws Throwable {

		ActionHandler
				.click(SSEC_2294_ClassicPartsStatusUpdatefor_New_Customer_Response_EndToEndTesting_Container.firstname);
		ActionHandler.setText(
				SSEC_2294_ClassicPartsStatusUpdatefor_New_Customer_Response_EndToEndTesting_Container.firstname,
				"Test");
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler
				.click(SSEC_2294_ClassicPartsStatusUpdatefor_New_Customer_Response_EndToEndTesting_Container.lastname);
		ActionHandler.setText(
				SSEC_2294_ClassicPartsStatusUpdatefor_New_Customer_Response_EndToEndTesting_Container.lastname, "12");
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler
				.click(SSEC_2294_ClassicPartsStatusUpdatefor_New_Customer_Response_EndToEndTesting_Container.Email);
		ActionHandler.setText(
				SSEC_2294_ClassicPartsStatusUpdatefor_New_Customer_Response_EndToEndTesting_Container.Email,
				"enquiryRequest2022@gmail.com");
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler
				.click(SSEC_2294_ClassicPartsStatusUpdatefor_New_Customer_Response_EndToEndTesting_Container.VINno);
		ActionHandler.setText(
				SSEC_2294_ClassicPartsStatusUpdatefor_New_Customer_Response_EndToEndTesting_Container.VINno, "T-12345");
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler
				.click(SSEC_2294_ClassicPartsStatusUpdatefor_New_Customer_Response_EndToEndTesting_Container.Phone);
		ActionHandler.setText(
				SSEC_2294_ClassicPartsStatusUpdatefor_New_Customer_Response_EndToEndTesting_Container.Phone,
				"9668993778");
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(
				SSEC_2294_ClassicPartsStatusUpdatefor_New_Customer_Response_EndToEndTesting_Container.selectanyqueries);
		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		ActionHandler.pressEnter();

		ActionHandler
				.click(SSEC_2294_ClassicPartsStatusUpdatefor_New_Customer_Response_EndToEndTesting_Container.Country);
		Actions act1 = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		ActionHandler.pressEnter();

		ActionHandler.click(
				SSEC_2294_ClassicPartsStatusUpdatefor_New_Customer_Response_EndToEndTesting_Container.Description);
		ActionHandler.setText(
				SSEC_2294_ClassicPartsStatusUpdatefor_New_Customer_Response_EndToEndTesting_Container.Description,
				"Testing");

		ActionHandler
				.click(SSEC_2294_ClassicPartsStatusUpdatefor_New_Customer_Response_EndToEndTesting_Container.Submit);
		ActionHandler.wait(1);
	}

	@Given("^User login to lateral access SVO Portal by Classic Customer Service user with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void user_login_to_lateral_access__SVO_portal_by_Classic_Customer_Service_user_with_Username_and_Password(
			String userName, String password) throws Throwable {

		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.wait(1);
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(1);
		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

	}

	@When("^User Verify the cases created$")
	public void User_Verify_the_cases_created() throws Throwable {

		ActionHandler.click(
				SSEC_2294_ClassicPartsStatusUpdatefor_New_Customer_Response_EndToEndTesting_Container.Selectalistview);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);

		ActionHandler.click(
				SSEC_2294_ClassicPartsStatusUpdatefor_New_Customer_Response_EndToEndTesting_Container.partstechnical);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);

		ActionHandler.click(SSEC_2371_Manage_Out_of_Office_Responses_Container.Firstrecord);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);

		VerifyHandler.verifyElementPresent(
				SSEC_2294_ClassicPartsStatusUpdatefor_New_Customer_Response_EndToEndTesting_Container.Casenumbr);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

	}

	public String checkEmailForClassicPartsUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(320, 326);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	@Given("^Laterally Access to the SVO Portal as Classic Parts user with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void Laterally_Access_to_the_SVO_Portal_Classic_Parts_with_username_password(String userName,
			String password) throws Exception {

		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		Reporter.addStepLog("User access SF SVO Portal");

		driver = getDriver();
		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailForClassicPartsUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(2);

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	@And("^User Verify that milestone has not started$")
	public void User_Verify_that_milestone_has_not_started() throws Exception {

		ActionHandler.scrollDown();
		ActionHandler.wait(2);
		VerifyHandler.verifyElementPresent(SSEC_2371_Manage_Out_of_Office_Responses_Container.milestone);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate to SVO Home Page successfully");
//		(//div[@class='milestoneTimerText ontrackTimer'])[2]
	}

	@When("^Customer send an enquiry outlook mail to user \"([^\"]*)\"$")
	public void customer_send_an_enquiry_outlook_mail_to_user(String User) throws Throwable {

		String p[] = User.split(",");
		User = CommonFunctions.readExcelMasterData(p[0], p[1], p[2]);

		String EnqSubject = "Enquiry creation mail from customer_";
		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		EnquiryTitle = "Enquiry creation mail from customer";

		driver.navigate().refresh();
		ActionHandler.wait(10);

		ActionHandler.wait(1);
		ActionHandler.click(SSEC_2371_Manage_Out_of_Office_Responses_Container.NewEmail);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Compose email icon");

		ActionHandler.click(SSEC_2371_Manage_Out_of_Office_Responses_Container.MailToTextBox);
		ActionHandler.wait(1);
		ActionHandler.setText(SSEC_2371_Manage_Out_of_Office_Responses_Container.MailToTextBox, User);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters To address to compose an email");

		ActionHandler.click(SSEC_2371_Manage_Out_of_Office_Responses_Container.GmailSubjectTextBox);
		ActionHandler.wait(1);
		ActionHandler.setText(SSEC_2371_Manage_Out_of_Office_Responses_Container.GmailSubjectTextBox,
				EnqSubject + randomNumber);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters subject to the mail as Enquiry creation request");

		ActionHandler.click(SSEC_2371_Manage_Out_of_Office_Responses_Container.GmailBodyTextBox);
		ActionHandler.wait(1);
		ActionHandler.setText(SSEC_2371_Manage_Out_of_Office_Responses_Container.GmailBodyTextBox, EnquiryTitle);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Body text to the mail as Enquiry creation request");

		ActionHandler.wait(1);
		ActionHandler.click(SSEC_2371_Manage_Out_of_Office_Responses_Container.SendMailBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on send email button");

		ActionHandler.wait(5);
		ActionHandler.click(SSEC_2371_Manage_Out_of_Office_Responses_Container.Signout);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(1);
		ActionHandler.click(SSEC_2371_Manage_Out_of_Office_Responses_Container.Signoutbtn);
		CommonFunctions.attachScreenshot();

	}

	@Then("^User Verify in customer service case havent created via email$")
	public void User_Verify_in_customer_service_case_haven_t_created_via_email() throws Throwable {

		ActionHandler.click(
				SSEC_2294_ClassicPartsStatusUpdatefor_New_Customer_Response_EndToEndTesting_Container.Selectalistview);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);

		ActionHandler.click(SSEC_2371_Manage_Out_of_Office_Responses_Container.CustomerServiceAllCases);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);

		CommonFunctions.attachScreenshot();

	}
}