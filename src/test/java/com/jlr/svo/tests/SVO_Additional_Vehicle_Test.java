package com.jlr.svo.tests;

import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.autotest.utils.Config;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.constants.Constants;
import com.jlr.svo.containers.SVOAccountsContainer;
import com.jlr.svo.containers.SVOAdditionalVehicleContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.containers.SVOItemToApproveContainer;
import com.jlr.svo.containers.SVOOpportunityContainer;
import com.jlr.svo.containers.SVOVehicleManufacturerContainer;
import com.jlr.svo.containers.SVO_RestrictedPartyScreeningContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class SVO_Additional_Vehicle_Test extends TestBaseCC {
	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory.getLogger(SVOAccounts.class.getName());
	SVOAccountsContainer SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
	SVOVehicleManufacturerContainer SVOVehicleManufacturerContainer = PageFactory.initElements(driver,
			SVOVehicleManufacturerContainer.class);
	SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	SVOAdditionalVehicleContainer SVOAdditionalvehicleContainer = PageFactory.initElements(driver,
			SVOAdditionalVehicleContainer.class);
	SVO_RestrictedPartyScreeningContainer RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
			SVO_RestrictedPartyScreeningContainer.class);
	SVOItemToApproveContainer SVOItemToapproveContainer = PageFactory.initElements(driver,
			SVOItemToApproveContainer.class);

	public static String verificationCode;
	public static String vCode;
	public static String veriCode;
	public static String DesignBriefName;
	public static String OpportunityName;

	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	public void onStart() {
		setupTest("SVOTest");
		driver = getDriver();
		SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		SVOVehicleManufacturerContainer = PageFactory.initElements(driver, SVOVehicleManufacturerContainer.class);
		SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
		SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
		SVOAdditionalvehicleContainer = PageFactory.initElements(driver, SVOAdditionalVehicleContainer.class);

		RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
				SVO_RestrictedPartyScreeningContainer.class);
		driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);

		verificationCode = null;
		vCode = null;
		DesignBriefName = null;
		OpportunityName = null;
	}

	public String checkEmail() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);
		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(334, 340);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	public String checkEmailPrivateOffice() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		// onStart();
		driver.get(Constants.outlookURL);
		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {
			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);
				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(341, 347);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	// Below code is to navigate for SVO page//
	public void navigateToSVO() throws Exception {
		String SVO = "Special Vehicle Operations";

		Reporter.addStepLog("User tries to navigate to SVO Portal");
		if (VerifyHandler.verifyElementPresent(SVOAccountsContainer.jlrText)) {

			ActionHandler.click(SVOAccountsContainer.menuBtn);

			CommonFunctions.attachScreenshot();

			// user enter text in search box//
			ActionHandler.setText(SVOAccountsContainer.searchBox, SVO);

			CommonFunctions.attachScreenshot();

			ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.selectMenu(SVO))));

			CommonFunctions.attachScreenshot();

			Reporter.addStepLog("User navigate to SVO Portal");
		}
	}

	@Given("^User Access SVO Portal for Specific User Role$")
	public void User_Access_SVO_Portal_for_specific_User() throws Exception {
		driver = getDriver();
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);

		if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.userNameTextBox)) {

			ActionHandler.click(SVOEnquiryContainer.icon_image);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Click on Logout");

			ActionHandler.click(SVOEnquiryContainer.Logout);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logout from SVO Portal");
		}

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		Reporter.addStepLog("User Logins to SVO Portal");

		String UserName = Config.getPropertyValue("BespokeUser_UserName");
		String Password = Config.getPropertyValue("BespokeUser_Password");

		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, UserName);

		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, Password);

		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();

		ActionHandler.waitForElement(SVOEnquiryContainer.SVOText, 60);
		CommonFunctions.attachScreenshot();
		VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
		Reporter.addStepLog("User navigate to SVO Home Page successfully");
	}

	@Given("^Access to SVO Portal with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void Access_SF_SVO_Portal_with_Username_Password(String userName, String password) throws Exception {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");
		onStart();
		driver = getDriver();
		driver.get(Constants.SVOURL);
		Reporter.addStepLog("User Logins to SVO Portal");

		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");
			vCode = checkEmail();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");
			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				CommonFunctions.attachScreenshot();
			} else {

				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {

			CommonFunctions.attachScreenshot();
			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	@Given("^Access to SVO Portal with Private Office Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void Access_SVO_Portal_With_Private_Office(String UserName, String Password) throws Exception {
		String un[] = UserName.split(",");
		UserName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = Password.split(",");
		Password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");
		onStart();
		driver = getDriver();

		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");

		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, UserName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, Password);
		CommonFunctions.attachScreenshot();
		

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");
			vCode = checkEmailPrivateOffice();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, UserName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, Password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				CommonFunctions.attachScreenshot();
			} else {

				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {

			CommonFunctions.attachScreenshot();
			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	@Then("^User navigates to Opportunity tab$")
	public void user_navigates_to_Opportunity_tab() throws Throwable {

		Reporter.addStepLog("User Navigate to Opportunity");

		javaScriptUtil.clickElementByJS(SVO_OpportunityContainer.OpportTab);

		CommonFunctions.attachScreenshot();
	}

	@And("^User creates new SVO Bespoke Opportunity with mandatory fields like Stage \"([^\"]*)\" Product Offering \"([^\"]*)\" Region \"([^\"]*)\" Client \"([^\"]*)\" Restricted Party Screening Stage \"([^\"]*)\" Preferred Contact \"([^\"]*)\" Account Name \"([^\"]*)\" Retailer Contact \"([^\"]*)\" Source \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\"$")
	public void User_creates_new_SVO_Bespoke_Opportunity(String stage, String productOffering, String region,
			String client, String Checkcleared, String preferredContact, String accountName, String retailerContact,
			String source, String brand, String model) throws Exception {
		OpportunityName = null;
		String oppName = "Test_SVO_Bespoke_Opportunity_";
		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		String closeDate = CommonFunctions.selectFutureDate();
		Reporter.addStepLog("Opportunity Close Date is = " + closeDate);

		String s[] = stage.split(",");
		stage = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String proOff[] = productOffering.split(",");
		productOffering = CommonFunctions.readExcelMasterData(proOff[0], proOff[1], proOff[2]);

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String c[] = client.split(",");
		client = CommonFunctions.readExcelMasterData(c[0], c[1], c[2]);

		String RPS[] = Checkcleared.split(",");
		Checkcleared = CommonFunctions.readExcelMasterData(RPS[0], RPS[1], RPS[2]);

		String preCon[] = preferredContact.split(",");
		preferredContact = CommonFunctions.readExcelMasterData(preCon[0], preCon[1], preCon[2]);

		String accName[] = accountName.split(",");
		accountName = CommonFunctions.readExcelMasterData(accName[0], accName[1], accName[2]);

		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		String so[] = source.split(",");
		source = CommonFunctions.readExcelMasterData(so[0], so[1], so[2]);

		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String m[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		ActionHandler.wait(3);
		Reporter.addStepLog("User starts creating new SVO Bespoke Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.NewOpportunity);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.QuoteNextBtn)) {
			ActionHandler.click(SVOItemToapproveContainer.SVOBespokeOpportunity);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects opportunity record type as SVO bespoke");

			ActionHandler.click(SVOItemToapproveContainer.QuoteNextBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User clicks on next button");

		}

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeOppName, oppName + randomNumber);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Name");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeClosedDate, closeDate);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Closed date");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeStage);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.StageSelection(stage))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Stage for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeProductOffering);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ProOfferingSelection(productOffering))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Product Offering for an Opportunity");

		ActionHandler.scrollToView(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.RegionSelection(region))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Region for an Opportunity");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.SVOBespokeClient);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeClient, client);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		
		
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeClientNameSearch);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(client))));
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Client for an Opportunity");

		ActionHandler.scrollToView(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		ActionHandler.wait(2);
		VerifyHandler.verifyElementPresent(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(RestrictedPartyScreeningContainer.ValueSelection(Checkcleared))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects restricted party screening as Check cleared for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokePreferredContact, preferredContact);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		act1.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(3);
		Reporter.addStepLog("User selects Preferred Contact for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeAccountName, accountName);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeAccountNameSearch);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(accountName))));
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Account Name for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeRetailerContact, retailerContact);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Actions act3 = new Actions(driver);
		act3.sendKeys(Keys.ARROW_DOWN).build().perform();
		act3.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(3);
		Reporter.addStepLog("User selects Retailer Contact for an Opportunity");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSource);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.SourceSelection(source))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Source for an Opportunity");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeBrand, brand);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeBrandSearch);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(brand))));
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Brand for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeModel, model);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(model))));
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Model for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSaveOpportunity);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves SVO Bespoke Opportunity");

		ActionHandler.wait(4);
		OpportunityName = SVOAdditionalvehicleContainer.OpportunityName.getText();
		Reporter.addStepLog("Opportunity Name is = " + OpportunityName);
	}

	@And("^User creates new SVO Bespoke Opportunity with mandatory fields like Stage \"([^\"]*)\" Product Offering \"([^\"]*)\" Region \"([^\"]*)\" Client \"([^\"]*)\" Preferred Contact \"([^\"]*)\" Account Name \"([^\"]*)\" Retailer Contact \"([^\"]*)\" Source \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\"$")
	public void User_creates_new_SVO_Bespoke_Opportunity(String stage, String productOffering, String region,
			String client, String preferredContact, String accountName, String retailerContact, String source,
			String brand, String model) throws Exception {
		String oppName = "Test_SVO_Bespoke_Opportunity_";
		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		String closeDate = CommonFunctions.selectFutureDate();
		Reporter.addStepLog("Opportunity Close Date is = " + closeDate);

		String s[] = stage.split(",");
		stage = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String proOff[] = productOffering.split(",");
		productOffering = CommonFunctions.readExcelMasterData(proOff[0], proOff[1], proOff[2]);

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String c[] = client.split(",");
		client = CommonFunctions.readExcelMasterData(c[0], c[1], c[2]);

		String preCon[] = preferredContact.split(",");
		preferredContact = CommonFunctions.readExcelMasterData(preCon[0], preCon[1], preCon[2]);

		String accName[] = accountName.split(",");
		accountName = CommonFunctions.readExcelMasterData(accName[0], accName[1], accName[2]);

		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		String so[] = source.split(",");
		source = CommonFunctions.readExcelMasterData(so[0], so[1], so[2]);

		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String m[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		Reporter.addStepLog("User starts creating new SVO Bespoke Opportunity");
		ActionHandler.click(SVOAdditionalvehicleContainer.NewOpportunity);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeOppName, oppName + randomNumber);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Name");
		
		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeClosedDate, closeDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Closed date");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeStage);

		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.StageSelection(stage))));

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Stage for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeProductOffering);

		CommonFunctions.attachScreenshot();
		ActionHandler.click(
				driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ProOfferingSelection(productOffering))));

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Product Offering for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRegion);

		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.RegionSelection(region))));

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Region for an Opportunity");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);

		VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.SVOBespokeClient);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeClient, client);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeClientNameSearch);

		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(client))));

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Client for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokePreferredContact, preferredContact);

		CommonFunctions.attachScreenshot();
		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		
		act1.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(1);
		ActionHandler.wait(3);

		Reporter.addStepLog("User selects Preferred Contact for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeAccountName, accountName);

		CommonFunctions.attachScreenshot();
		
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeAccountNameSearch);

		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(accountName))));

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Account Name for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeRetailerContact, retailerContact);

		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Actions act3 = new Actions(driver);
		act3.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		ActionHandler.wait(1);
		ActionHandler.wait(3);
		Reporter.addStepLog("User selects Retailer Contact for an Opportunity");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSource);

		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.SourceSelection(source))));

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Source for an Opportunity");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeBrand, brand);

		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeBrandSearch);

		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(brand))));

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Brand for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeModel, model);

		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);

		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(model))));

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Model for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSaveOpportunity);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves SVO Bespoke Opportunity");

		OpportunityName = SVOAdditionalvehicleContainer.OpportunityName.getText();
		Reporter.addStepLog("Opportunity Name is = " + OpportunityName);
	}

	@Then("^Logout from SVO Portal$")
	public void logout_from_SVO_Portal() throws Throwable {

		ActionHandler.click(SVO_OpportunityContainer.icon_image);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Logout");
		ActionHandler.click(SVO_OpportunityContainer.Logout);

		CommonFunctions.attachScreenshot();
		if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.userNameTextBox)) {
			MarkupHelper.createLabel("Logged out successfully", ExtentColor.GREEN);
		} else {
			MarkupHelper.createLabel("Log out failed", ExtentColor.RED);
		}

		driver.close();
		Reporter.addStepLog("Driver closed");
	}

	@Then("^User closes the Opportunity$")
	public void User_Closes_Opportunity() throws Exception {

		ActionHandler.click(SVOAdditionalvehicleContainer.OpportunityClosedStage);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User wants to close an opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.OpportunitySelectClosedStage);

		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.OpportunityClosedStageName);

		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.OpportunityClosedLostStage);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Stage Name as Closed Lost for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.OpportunityClosedLostReason);

		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.OpportunityClosedLostReasonValue);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Closed Lost reason for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDoneBtn);

		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("Verify that Opportunity has been closed with Closed Lost");

	}

	@And("^User opens an opportunity details for Private Office Opportunity$")
	public void User_Opens_an_Opportunity_details_for_Private_Office_Opportunity() throws Exception {

		ActionHandler.click(SVOAdditionalvehicleContainer.RecentlyViewedLink);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on recently viewed link");

		ActionHandler.click(SVOAdditionalvehicleContainer.AllPrivateOfficeOpportunityLink);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("All private office opportunities are shown on the page");

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstOpportunityLink);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens first opportunity details from the Opportunity list");
	}

	@And("^User opens an opportunity details for Private Office Opportunity which is in \"([^\"]*)\" stage$")
	public void User_opens_opportunity_details_for_private_pffice_opportunity_which_is_in_Order_placed_stage(
			String stage) throws Exception {

		ActionHandler.click(SVOAdditionalvehicleContainer.RecentlyViewedLink);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on recently viewed link");

		ActionHandler.click(SVOAdditionalvehicleContainer.AllPrivateOfficeOpportunityLink);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("All private office opportunities are shown on the page");

		ActionHandler.setText(SVOAdditionalvehicleContainer.OpportunitySearchBox, stage);

		CommonFunctions.attachScreenshot();

		ActionHandler.pressEnter();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User searches an opportunity for Order placed stage");

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstOpportunityLink);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"User opens first opportunity details from the Opportunity list which is in Order Placed stage");
	}

	@Then("^User navigates to Orders page for the same Opportunity$")
	public void User_navigates_Orders_Page() throws Exception {

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.OrdersLink);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Orders link");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstOrder);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens Order details for the opportunity");
	}

	@Then("^User navigates to Orders page for the same Opportunity for Verification$")
	public void User_navigates_Orders_Page_Verification() throws Exception {

		driver.navigate().refresh();

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.OppOrdersLink);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Orders link");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstOppOrder);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens Order details for the opportunity");
		System.out.println("User clicks on First Order from the list");
	}

	@Then("^User moves the opportunity to Design Brief & Quote stage$")
	public void User_moves_Opportunity_to_Design_Brief_Quote_Stage() throws Exception {

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.MarkStageAsComplete);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User moves the Opportunity to Commission from Qualified Stage");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.MarkStageAsComplete);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User moves the Opportunity to Design Brief & Quote from Commission Stage");
	}

	@And("^User moves Oppotunity to Order Placed stage after accepting Quote for Retailer Contact \"([^\"]*)\"$")
	public void User_moves_opportunity_to_Order_Placed_after_accepting_quote(String retailerContact) throws Exception {
		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		String quoteName = "SVO Bespoke Test Quote";
		String expirtaionDate = CommonFunctions.selectFutureDate();

		ActionHandler.pageDown();

		CommonFunctions.attachScreenshot();

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuotesLink);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Quotes page");

		ActionHandler.click(SVOAdditionalvehicleContainer.NewQuoteBtn);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on New button to create new Quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteNameBox, quoteName);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User writes quote name for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRetailerNameBox, retailerContact);

		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Actions act3 = new Actions(driver);
		act3.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act3.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(1);
		ActionHandler.wait(3);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters retailer contact details for new quote");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);

		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteCostPrice, "123");

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Cost Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteListPrice, "203");

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters List Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteContribution, "23");

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Contribution for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRetailPrice, "213");

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Retail Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRevenue, "113");

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Revenue for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteExpirationDate, expirtaionDate);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Expiration Date for new quote");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteSaveBtn);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User save new quote");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstQuote);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens created quote details");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.DesignBriefLink);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Design Brief Section");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstDesignBrief);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens first design brief from the list");

		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefSubmit);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Design Brief status moves to Submitted from Draft");

		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefSaveSubmission);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves Design Brief Submission");

		DesignBriefName = SVOAdditionalvehicleContainer.DesignBriefName.getText();
		Reporter.addStepLog("Design Brief Name is = " + DesignBriefName);

		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefReturnRenderPack);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User returns render pack for Design Brief");

		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefSaveRenderPack);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves Design Brief for Return Render Pack");

		javaScriptUtil.clickElementByJS(SVO_OpportunityContainer.OpportTab);

		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOAdditionalvehicleContainer.OpportunitySearchBox, OpportunityName);
		ActionHandler.wait(2);

		Actions act5 = new Actions(driver);
		act5.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(1);
		ActionHandler.wait(3);

		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstOpportunityLink);

		CommonFunctions.attachScreenshot();

		ActionHandler.pageDown();

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuotesLink);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Quotes page");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstQuoteFromOpportunity);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens created quote details");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Quote status moves to Price stage");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Quote status moves to Proposal Sent stage");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);

		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDrpDwn);

		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeAccepted);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User mark accepeted as Quote Outcome");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDetailDrpDwn);

		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDetailAccepted);

		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDoneBtn);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User accepeted the Quote");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);

		CommonFunctions.attachScreenshot();

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuoteOpportunityLink);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate to Opportunity");

		driver.navigate().refresh();

		CommonFunctions.attachScreenshot();

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.OrdersLink);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Orders link");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstOrder);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens Order details for the opportunity");

		

		ActionHandler.click(SVOAdditionalvehicleContainer.CommonOrderNumber);

		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOAdditionalvehicleContainer.CommonOrderNumberBox, "123456");

		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.SaveCommonOrderNumber);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves common order number");

		ActionHandler.click(SVOAdditionalvehicleContainer.CloseOrderTab);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User closed Orders Tab");

		javaScriptUtil.clickElementByJS(SVO_OpportunityContainer.OpportTab);

		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOAdditionalvehicleContainer.OpportunitySearchBox, OpportunityName);
		ActionHandler.wait(2);

		Actions act6 = new Actions(driver);
		act6.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(1);
		ActionHandler.wait(3);

		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstOpportunityLink);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User moves the Opportunity to Order Placed from Contract Signed");
	}

	@Then("^User creates a new Quote with mandatory fields like Retailer Contact \"([^\"]*)\"$")
	public void User_creates_New_Quote_with_Mandatory_field(String retailerContact) throws Exception {
		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		String quoteName = "SVO Bespoke Test Quote";
		String expirtaionDate = CommonFunctions.selectFutureDate();

		ActionHandler.pageDown();

		CommonFunctions.attachScreenshot();

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuotesLink);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Quotes page");

		ActionHandler.click(SVOAdditionalvehicleContainer.NewQuoteBtn);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on New button to create new Quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteNameBox, quoteName);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User writes quote name for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRetailerNameBox, retailerContact);

		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Actions act3 = new Actions(driver);
		act3.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act3.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(1);
		ActionHandler.wait(3);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters retailer contact details for new quote");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);

		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteCostPrice, "123");

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Cost Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteListPrice, "203");

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters List Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteContribution, "23");

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Contribution for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRetailPrice, "213");

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Retail Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRevenue, "113");

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Revenue for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteExpirationDate, expirtaionDate);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Expiration Date for new quote");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteSaveBtn);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User save new quote");

		ActionHandler
				.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.BackToOpportunity(OpportunityName))));

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate back to the Opportunity");
	}

	@Then("^User navigates to Additional Vehicle Link from Quick Links$")
	public void User_navigates_to_Additional_vehicle_link() throws Exception {

		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);

		CommonFunctions.attachScreenshot();
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.AdditionalVehicleLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Additional Vehicle Link");
	}

	@And("^Verify that same information flows to an Opportunity$")
	public void Verify_same_information_flows_to_Opportunity() throws Exception {

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.AdditionalVehicleHyperLink);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Additional Vehicle Link");

		VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.AdditionalVehicleSectionFirstAVRecord);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"User verifies that single vehicle record has been created under Additional Vehicle section");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.AdditionalVehicleSectionFirstAVRecord);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens details of created additional vehicle");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);

		CommonFunctions.attachScreenshot();

		ActionHandler.click(
				driver.findElement(By.xpath(SVOAdditionalvehicleContainer.BackToOpportunityFromAV(OpportunityName))));

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate back to the opportunity");
	}

	@And("^User verifies that all different fields are available under Additional Vehicle Section$")
	public void User_verifies_all_different_fields_available_under_additional_vehicle_section() throws Exception {

		ActionHandler.click(SVOAdditionalvehicleContainer.NewAdditionalVehicleBtn);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on New button after navigating to Additional vehicle section");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);

		VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.AdditionalVehicleBrand);

		VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.AdditionalVehicleModel);

		VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.AdditionalVehicleSeriesName);

		VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.AdditionalVehicleDerivative);

		VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.AdditionalVehicleHandOfDrive);

		VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.AdditionalVehicleModelYear);

		VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.AdditionalVehicleWheelBase);

		VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.AdditionalVehicleVehicleRecord);

		Reporter.addStepLog("All the required fields are available on Additional vehicle page");

		ActionHandler.click(SVOAdditionalvehicleContainer.CancelAdditionalVehicleBtn);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Cancel button on Additional vehicle section");
	}

	@And("^User verifies that Order and Additional Vehicle Auto Number should not displayed under Additional Vehicle Section$")
	public void User_verifies_Order_additional_vehicle_auto_number_not_displayed_under_additional_vehicle_section()
			throws Exception {

		ActionHandler.click(SVOAdditionalvehicleContainer.NewAdditionalVehicleBtn);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on New button after navigating to Additional vehicle section");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		Reporter.addStepLog(
				"User verifies that Order and Additional Vehicle Auto Number is not displayed on Additional Vehicle page");

		ActionHandler.click(SVOAdditionalvehicleContainer.CancelAdditionalVehicleBtn);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Cancel button on Additional vehicle section");
	}

	@And("^User adds one Vehicle Record with fields like Brand \"([^\"]*)\", Model \"([^\"]*)\", Series Name \"([^\"]*)\", Derivative \"([^\"]*)\" and Model Year \"([^\"]*)\"$")
	public void User_adds_one_Vehicle_Record_With_Fields(String Brand, String Model, String seriesName,
			String derivative, String modelYear) throws Exception {
		String b[] = Brand.split(",");
		Brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String m[] = Model.split(",");
		Model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		String series[] = seriesName.split(",");
		seriesName = CommonFunctions.readExcelMasterData(series[0], series[1], series[2]);

		String deri[] = derivative.split(",");
		derivative = CommonFunctions.readExcelMasterData(deri[0], deri[1], deri[2]);

		String my[] = modelYear.split(",");
		modelYear = CommonFunctions.readExcelMasterData(my[0], my[1], my[2]);
		ActionHandler.wait(5);
		ActionHandler.click(SVOAdditionalvehicleContainer.NewAdditionalVehicleBtn1);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on New button after navigating to Additional vehicle section");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);

		CommonFunctions.attachScreenshot();
		ActionHandler.setText(SVOAdditionalvehicleContainer.AdditionalVehicleBrand, Brand);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeBrandSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(Brand))));

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Brand for an Additional vehicle record");

		ActionHandler.setText(SVOAdditionalvehicleContainer.AdditionalVehicleModel, Model);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);

		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(Model))));

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Model for an Additional vehicle record");

		ActionHandler.setText(SVOAdditionalvehicleContainer.AdditionalVehicleSeriesName, seriesName);

		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleVehicleSeriesNameSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(seriesName))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Series Name for an Additional vehicle record");

		ActionHandler.setText(SVOAdditionalvehicleContainer.AdditionalVehicleDerivative, derivative);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleVehicleDerivativeSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(derivative))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Derivative for an Additional vehicle record");

		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleModelYear);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ModelYearSelection(modelYear))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects model year for an Additional vehicle record");

		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleWheelBase);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleWheelBaseStandard);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Wheel base as Standard Wheelbase for an Additional vehicle record");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSaveOpportunity);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("One additional vehicle record has been added successfully");

	}

	@And("^User tries to add one Vehicle Record with fields like Brand \"([^\"]*)\", Model \"([^\"]*)\", Series Name \"([^\"]*)\", Derivative \"([^\"]*)\" and Model Year \"([^\"]*)\"$")
	public void User_tries_add_one_Vehicle_Record_With_Fields(String Brand, String Model, String seriesName,
			String derivative, String modelYear) throws Exception {
		String b[] = Brand.split(",");
		Brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String m[] = Model.split(",");
		Model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		String series[] = seriesName.split(",");
		seriesName = CommonFunctions.readExcelMasterData(series[0], series[1], series[2]);

		String deri[] = derivative.split(",");
		derivative = CommonFunctions.readExcelMasterData(deri[0], deri[1], deri[2]);

		String my[] = modelYear.split(",");
		modelYear = CommonFunctions.readExcelMasterData(my[0], my[1], my[2]);

		ActionHandler.click(SVOAdditionalvehicleContainer.NewAdditionalVehicleBtn);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on New button after navigating to Additional vehicle section");

		ActionHandler.setText(SVOAdditionalvehicleContainer.AdditionalVehicleBrand, Brand);

		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeBrandSearch);

		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(Brand))));

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Brand for an Additional vehicle record");

		ActionHandler.setText(SVOAdditionalvehicleContainer.AdditionalVehicleModel, Model);

		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);

		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(Model))));

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Model for an Additional vehicle record");

		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleHandOfDrive);

		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleHODLHD);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Hand Of Drive as LHD for an Additional vehicle record");

		ActionHandler.setText(SVOAdditionalvehicleContainer.AdditionalVehicleSeriesName, seriesName);

		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleVehicleSeriesNameSearch);

		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(seriesName))));

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Series Name for an Additional vehicle record");

		ActionHandler.setText(SVOAdditionalvehicleContainer.AdditionalVehicleDerivative, derivative);

		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleVehicleDerivativeSearch);

		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(derivative))));

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Derivative for an Additional vehicle record");

		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleModelYear);

		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ModelYearSelection(modelYear))));

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects model year for an Additional vehicle record");

		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleWheelBase);

		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleWheelBaseStandard);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Wheel base as Standard Wheelbase for an Additional vehicle record");
	}

	@Then("^User navigates to Additional Vehicle hyperlink from Related Quick Links and verify necessary information displayed when mouse hovered$")
	public void User_navigates_additional_vehicle_hyperlink_necessary_information_displayed() throws Exception {

		Actions act = new Actions(driver);
		act.moveToElement(SVOAdditionalvehicleContainer.AdditionalVehicleLink);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User mouse hover to additional vehicle quick link");

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"User verifies that all necessary information should be displayed when mouse hovered to an Additional vehicle link");
	}

	@Then("^User navigates to Additional Vehicle Section of an opportunity$")
	public void User_navigates_additional_vehicle_section_opportunity() throws Exception {

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);

		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleSection);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Additional Vehicle Section of an opportunity");
	}

	@And("^User verifies that Brand, Model and Series Name is displayed in Mini page$")
	public void User_verifies_Brand_Model_Series_displayed_on_mini_page() throws Exception {

		CommonFunctions.attachScreenshot();

		VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.AdditionalVehicleSectionMiniTable);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the mini page layout is displayed as expected");

		VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.AdditionalVehicleSectionMiniTableBrand);

		VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.AdditionalVehicleSectionMiniTableModel);

		VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.AdditionalVehicleSectionMiniTableSeriesName);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"User is able to view Brand, Model and Series Name on Mini page of an Additional vehicle section");
	}

	@And("^User verifies that full vehicle information is displayed in Additional Vehicle page$")
	public void User_verifies_full_vehicle_information_displayed_Additional_vehicle_page() throws Exception {

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.AdditionalVehicleSectionViewAll);

		CommonFunctions.attachScreenshot();

		VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.AdditionalVehicleSectionBrand);

		VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.AdditionalVehicleSectionModel);

		VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.AdditionalVehicleSectionSeriesName);

		VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.AdditionalVehicleSectionDerivative);

		VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.AdditionalVehicleSectionModelYear);

		VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.AdditionalVehicleSectionHandOfDrive);

		VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.AdditionalVehicleSectionVehicleRecord);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies that all required columns should be displayed");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.AdditionalVehicleSectionFirstAVRecord);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User is able to view full additional vehicle details under Additional Vehicle section");

		ActionHandler.click(
				driver.findElement(By.xpath(SVOAdditionalvehicleContainer.BackToOpportunityFromAV(OpportunityName))));

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate back to the opportunity");
	}

	@And("^User adds one Vehicle Record with mandatory fields like Brand \"([^\"]*)\", Model \"([^\"]*)\", Series Name \"([^\"]*)\", Derivative \"([^\"]*)\" and Model Year \"([^\"]*)\"$")
	public void User_adds_one_Vehicle_Record_With_Mandatory_Fields(String Brand, String Model, String seriesName,
			String derivative, String modelYear) throws Exception {
		String b[] = Brand.split(",");
		Brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String m[] = Model.split(",");
		Model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		String series[] = seriesName.split(",");
		seriesName = CommonFunctions.readExcelMasterData(series[0], series[1], series[2]);

		String deri[] = derivative.split(",");
		derivative = CommonFunctions.readExcelMasterData(deri[0], deri[1], deri[2]);

		String my[] = modelYear.split(",");
		modelYear = CommonFunctions.readExcelMasterData(my[0], my[1], my[2]);

		ActionHandler.click(SVOAdditionalvehicleContainer.NewAdditionalVehicleBtn);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on New button after navigating to Additional vehicle section");

		ActionHandler.setText(SVOAdditionalvehicleContainer.AdditionalVehicleBrand, Brand);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeBrandSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(Brand))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Brand for an Additional vehicle record");

		ActionHandler.setText(SVOAdditionalvehicleContainer.AdditionalVehicleModel, Model);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(Model))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Model for an Additional vehicle record");

		ActionHandler.setText(SVOAdditionalvehicleContainer.AdditionalVehicleSeriesName, seriesName);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleVehicleSeriesNameSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(seriesName))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Series Name for an Additional vehicle record");

		ActionHandler.setText(SVOAdditionalvehicleContainer.AdditionalVehicleDerivative, derivative);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleVehicleDerivativeSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(derivative))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Derivative for an Additional vehicle record");

		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleModelYear);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ModelYearSelection(modelYear))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects model year for an Additional vehicle record");

		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleWheelBase);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleWheelBaseStandard);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Wheel base as Standard Wheelbase for an Additional vehicle record");

		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleOwnerlabel);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleHandOfDrive);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleHODLHD);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Hand Of Drive as LHD for an Additional vehicle record");

	}

	@Then("^User clicks on Save & New button to add one more vehicle record$")
	public void User_clicks_Save_New_button_to_add_one_vehicle_record() throws Exception {

		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleSaveAndNew);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Save and New button to add another vehicle record");
	}

	@Then("^User navigates to Additional Vehicle hyperlink from Related Quick Links$")
	public void User_navigates_additional_vehicle_hyperlink_related_quick_links() throws Exception {

		ActionHandler.wait(2);
		Actions act3 = new Actions(driver);
		act3.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act3.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act3.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);

		CommonFunctions.attachScreenshot();

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.AdditionalVehicleLink);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate to additional vehicle quick link");
	}

	@And("^User verifies that multiple vehicles are created under Additional Vehicle section$")
	public void User_verifies_multiple_vehicles_created_additional_vehicle_record() {
		String vehicles;

		List<WebElement> additionalVehicles = ActionHandler.listOfElementsByWebElement(
				SVOAdditionalvehicleContainer.AdditionalVehicleSectionFirstAVRecord, "a", "xpath");
		Reporter.addStepLog("Total Additional Vehicles that are added = " + additionalVehicles.size());

		for (int i = 0; i < additionalVehicles.size(); i++) {

			vehicles = additionalVehicles.get(i).getText();
			Reporter.addStepLog("Added Vehicle's Auto Number is = " + vehicles);
		}
		Reporter.addStepLog("User verifies that multiple vehicles are created under Additional Vehicle section");

	}

	@And("^User verifies that single vehicle is created under Additional Vehicle section$")
	public void User_verifies_single_vehicle_created_under_additional_vehicle_section() throws Exception {

		VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.AdditionalVehicleSectionFirstAVRecord);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"User verifies that single vehicle record has been created under Additional Vehicle section");

		driver.navigate().refresh();
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.AdditionalVehicleSectionFirstAVRecord);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens details of created additional vehicle");

		driver.navigate().refresh();
		CommonFunctions.attachScreenshot();
		ActionHandler.click(
				driver.findElement(By.xpath(SVOAdditionalvehicleContainer.BackToOpportunityFromAV(OpportunityName))));

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate back to the opportunity");
	}

	@Then("^User removes the vehicle record which is created and verify that it has been removed from the list$")
	public void User_removes_the_vehicle_record_created_verify_should_be_removed() throws Exception {

		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleSectionFirstAVRecord);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens details of created additional vehicle");

		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleRecordDeleteBtn);

		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleRecordConfirmDeleteBtn);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User is able to delete the vehicle record from an additional vehicle section");

		VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.AdditionalVehicleNoItemDisplayLabel);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies that no vehicle record is displayed on Additional Vehicle page");

	}

	@Then("^User edits the vehicle record which is created like Model \"([^\"]*)\" and Model Year \"([^\"]*)\"$")
	public void User_edits_vehicle_record(String Model, String modelYear) throws Exception {
		String m[] = Model.split(",");
		Model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		String my[] = modelYear.split(",");
		modelYear = CommonFunctions.readExcelMasterData(my[0], my[1], my[2]);

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.AdditionalVehicleLink);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate to additional vehicle quick link");

		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleSectionFirstAVRecord);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens details of created additional vehicle");

		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleShowMoreActionsFromOrders);

		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleEditBtnFromOrders);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requested to edit the vehicle record");

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens details of created additional vehicle");

		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleShowMoreActionsFromOrders);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleEditBtnFromOrders);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requested to edit the vehicle record");

		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleModelClearText);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clear Model text field");
		ActionHandler.setText(SVOAdditionalvehicleContainer.AdditionalVehicleModel, Model);

		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);

		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(Model))));

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects another Model for an Additional vehicle record");

		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleSeriesNameClearText);

		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleDerivativeClearText);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clear text from Series Name and Derivative fields");

		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleModelYear);

		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ModelYearSelection(modelYear))));

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects another model year for an Additional vehicle record");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSaveOpportunity);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Vehicle record has been updated successfully");
	}

	@Then("^User edits the vehicle record from Orders page which is created like Model \"([^\"]*)\" and Model Year \"([^\"]*)\"$")
	public void User_edits_vehicle_record_From_Orders_Page(String Model, String modelYear) throws Exception {
		String m[] = Model.split(",");
		Model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		String my[] = modelYear.split(",");
		modelYear = CommonFunctions.readExcelMasterData(my[0], my[1], my[2]);

		ActionHandler.wait(2);
		Actions act3 = new Actions(driver);
		act3.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);

		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleSecFirstAVRecord);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens details of created additional vehicle");

		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleShowMoreActionsFromOrders);

		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleEditBtnFromOrders);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requested to edit the vehicle record");

		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleModelClearText);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clear Model text field");
		ActionHandler.setText(SVOAdditionalvehicleContainer.AdditionalVehicleModel, Model);

		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);

		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(Model))));

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects another Model for an Additional vehicle record");

		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleSeriesNameClearText);

		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleDerivativeClearText);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clear text from Series Name and Derivative fields");

		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleModelYear);

		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ModelYearSelection(modelYear))));

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects another model year for an Additional vehicle record");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSaveOpportunity);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Vehicle record has been updated successfully");

		ActionHandler.click(
				driver.findElement(By.xpath(SVOAdditionalvehicleContainer.BackToOpportunityFromAV(OpportunityName))));

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate back to the opportunity");
	}

	@And("^User verifies that the vehicle record has been updated with Model \"([^\"]*)\"$")
	public void User_verifies_vehicle_record_updated(String Model) throws Exception {
		String m[] = Model.split(",");
		Model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		String modelName = SVOAdditionalvehicleContainer.AdditionalVehicleModelName.getText();

		if (modelName.equals(Model)) {
			Reporter.addStepLog("The vehicle record has been updated successfully");
			Reporter.addStepLog("Updated Model Name is = " + modelName);
		} else {
			Reporter.addStepLog("Vehicle record has not updated");
			Reporter.addStepLog("Updated Model Name is = " + modelName);
		}

	}

	@Then("^User navigates to Additional Vehicle Section of an opportunity from Orders Page$")
	public void User_navigates_additional_vehicle_section_from_Orders_page() throws Exception {

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);

		CommonFunctions.attachScreenshot();

		VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.AdditionalVehicleSectionFromOrders);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies that Additional vehicle section is available");
	}

	@And("^User verifies that full vehicle information is displayed in Additional Vehicle page of an Orders page$")
	public void User_verifies_full_vehicle_information_displayed_Additional_vehicle_page_Orders_Page()
			throws Exception {

		ActionHandler.wait(4);
		driver.navigate().refresh();
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.AdditionalVehicleSectionFirstAVRecord);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens details of created additional vehicle");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"User verify that full vehicle information is present on Additional Vehicle section of an orders page");
		driver.navigate().back();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate back to Orders page");
	}

	@And("^User verifies that no action can be taken on Additional Vehicle Section from Orders page$")
	public void User_verifies_no_action_taken_on_Additional_vehicle_section_orders_page() throws Exception {
		

		ActionHandler.click(SVOAdditionalvehicleContainer.AVOrdersNoActionAvailableBtn);

		CommonFunctions.attachScreenshot();

		VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.AVOrdersNoActionAvailableLabel);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies that No Action Availavle on Additional Vehicle Section on Orders Page");
	}

	@Then("^User verifies that an Additional vehicle section or hyperlink is not present$")
	public void User_verifies_an_additional_vehicle_section_not_present() throws Exception {

		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		ActionHandler.wait(1);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		ActionHandler.wait(1);

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.OpportunityShowAllLinks);

		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Actions act3 = new Actions(driver);
		act3.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act3.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act3.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);

		CommonFunctions.attachScreenshot();

		if (!VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.AdditionalVehicleLink)) {

			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Additional vehicles hyper link is not present");
		} else {
			Reporter.addStepLog("Additional vehicles hyper link is present");
		}
	}

	@Then("^User navigates to Additional Vehicle Section to add new Vehicle Record$")
	public void User_navigates_Additional_vehicle_section_to_add_new_vehicle_record() throws Exception {

		ActionHandler.scrollDown();
		ActionHandler.wait(1);

		ActionHandler.scrollDown();
		ActionHandler.wait(1);

		CommonFunctions.attachScreenshot();

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.AdditionalVehicleSection);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Additional Vehicle section");

		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleDropDown);

		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.NewVehicleBtn);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on New button to create Vehicle Record");
	}

	@Then("^User clicks on Cancel button and verify that user navigate back to Opportunity page$")
	public void User_clicks_cancel_button_verify_user_navigate_back_to_opportunity() throws Exception {

		ActionHandler.click(SVOAdditionalvehicleContainer.CancelBtn);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on cancel button");

		if (VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.OpportunityTitle)) {
			Reporter.addStepLog("User navigate back to Opportunity page on hit on cancel button");
		} else {
			Reporter.addStepLog("User does not navigate back to opportunity page");
		}
	}

	@And("^User verifies that no order is created as the quote is not accepted$")
	public void User_verifies_no_order_created_as_quote_not_accepted() throws Exception {

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.OrdersLink);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Orders link");

		CommonFunctions.attachScreenshot();
		if (VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.FirstOrder)) {
			Reporter.addStepLog("Order is created without accepting the quote");
		} else {
			Reporter.addStepLog("Order is not created as Quote is not accepted");
		}
	}

	@Then("^User verifies that user is not able to create Additional vehicle Record for an opportunity$")
	public void User_verifies_user_is_not_able_to_create_additional_vehicle_record() throws Exception {

		if (!VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.NewBtn)) {

			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Additional vehicle section is not available for an Opportunity");
		} else {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Additional vehicle section is available for an Opportunity");
		}
	}

	@And("^User opens an opportunity details for SVO Bespoke Opportunity$")
	public void User_opens_opportunity_details_SVO_Bespoke_Opportunity() throws Exception {

		ActionHandler.click(SVOAdditionalvehicleContainer.RecentlyViewedLink);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on recently viewed list drop down");

		ActionHandler.click(SVOAdditionalvehicleContainer.BespokeOpportunities);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("All Bespoke opportunities are shown on the page");

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstOpportunityLink);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens first opportunity details from the Opportunity list");
	}

	@And("^User create a SVO Bespoke Opportunity with Restricted party screening check cleared like Stage \"([^\"]*)\" Product Offering \"([^\"]*)\" Region \"([^\"]*)\" Client \"([^\"]*)\" Restricted Party Screening Stage \"([^\"]*)\" Preferred Contact \"([^\"]*)\" Account Name \"([^\"]*)\" Retailer Contact \"([^\"]*)\" Source \"([^\"]*)\" Origination Divission \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\"$")
	public void User_create_a_SVO_Bespoke_Opportunity_with_Restricted_party_screening_check_cleared(String stage,
			String productOffering, String region, String client, String Checkcleared, String preferredContact,
			String accountName, String retailerContact, String source, String originatingdivission, String brand,
			String model) throws Exception {
		String oppName = "Test_SVO_Bespoke_Opportunity_";
		double randomNumber = getRandomIntegerBetweenRange(10000, 20000);

		String closeDate = CommonFunctions.selectFutureDate();
		Reporter.addStepLog("Opportunity Close Date is = " + closeDate);

		String s[] = stage.split(",");
		stage = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String proOff[] = productOffering.split(",");
		productOffering = CommonFunctions.readExcelMasterData(proOff[0], proOff[1], proOff[2]);

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String c[] = client.split(",");
		client = CommonFunctions.readExcelMasterData(c[0], c[1], c[2]);

		String RPS[] = Checkcleared.split(",");
		Checkcleared = CommonFunctions.readExcelMasterData(RPS[0], RPS[1], RPS[2]);

		String preCon[] = preferredContact.split(",");
		preferredContact = CommonFunctions.readExcelMasterData(preCon[0], preCon[1], preCon[2]);

		String accName[] = accountName.split(",");
		accountName = CommonFunctions.readExcelMasterData(accName[0], accName[1], accName[2]);

		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		String so[] = source.split(",");
		source = CommonFunctions.readExcelMasterData(so[0], so[1], so[2]);

		String OD[] = originatingdivission.split(",");
		originatingdivission = CommonFunctions.readExcelMasterData(OD[0], OD[1], OD[2]);

		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String m[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		Reporter.addStepLog("User starts creating new SVO Bespoke Opportunity");
		ActionHandler.click(SVOAdditionalvehicleContainer.NewOpportunity);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeOppName, oppName + randomNumber);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Name");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeClosedDate, closeDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Closed date");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeStage);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.StageSelection(stage))));

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Stage for an Opportunity");
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeProductOffering);

		CommonFunctions.attachScreenshot();
		ActionHandler.click(
				driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ProOfferingSelection(productOffering))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Product Offering for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.RegionSelection(region))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Region for an Opportunity");

		VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.SVOBespokeClient);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeClient, client);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeClientNameSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(client))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Client for an Opportunity");

		VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.RestrictedPartyScreening);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.RestrictedPartyScreening);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.RPSSelection(Checkcleared))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects restricted party screening as Check cleared for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokePreferredContact, preferredContact);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokePreferredContactname);
		Reporter.addStepLog("User selects Preferred Contact for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeAccountName, accountName);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeAccountNameSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(accountName))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Account Name for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeRetailerContact, retailerContact);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokePreferredContactname);
		Reporter.addStepLog("User selects Retailer Contact for an Opportunity");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSource);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.SourceSelection(source))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Source for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOOriginatingDivission);
		CommonFunctions.attachScreenshot();
		ActionHandler
				.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.RPSSelection(originatingdivission))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Originating Divission for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeBrand, brand);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeBrandSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(brand))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Brand for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeModel, model);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(model))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Model for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSaveOpportunity);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves SVO Bespoke Opportunity");

		OpportunityName = SVOAdditionalvehicleContainer.OpportunityName.getText();
		Reporter.addStepLog("Opportunity Name is = " + OpportunityName);
	}

}
