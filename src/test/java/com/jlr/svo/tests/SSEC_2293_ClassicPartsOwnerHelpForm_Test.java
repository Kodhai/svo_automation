package com.jlr.svo.tests;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.autotest.utils.Config;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.constants.Constants;
import com.jlr.svo.containers.SSEC_1238_CaseLifecycle_Container;
import com.jlr.svo.containers.SSEC_2293_ClassicPartsOwnerHelpForm_Container;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class SSEC_2293_ClassicPartsOwnerHelpForm_Test extends TestBaseCC {

	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory.getLogger(SVOWorkOrderProgrammeField_Test.class.getName());
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	com.jlr.svo.containers.SSEC_2293_ClassicPartsOwnerHelpForm_Container SSEC_2293_ClassicPartsOwnerHelpForm_Container = PageFactory
			.initElements(driver, SSEC_2293_ClassicPartsOwnerHelpForm_Container.class);
	SSEC_1238_CaseLifecycle_Container CaseLifecycle_Container = PageFactory.initElements(driver,
			SSEC_1238_CaseLifecycle_Container.class);

	public static String verificationCode;
	public static String vCode;

	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	public void onStart() {

		setupTest("SVOTest");
		driver = getDriver();

		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		SSEC_2293_ClassicPartsOwnerHelpForm_Container = PageFactory.initElements(driver,
				SSEC_2293_ClassicPartsOwnerHelpForm_Container.class);

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

	}

	@Given("^User open the Ownerhelp portal$")
	public void User_open_the_Ownerhelp_portal() throws Throwable {

		onStart();
		driver = getDriver();
		driver.get(Constants.OwnerHelp);

		Reporter.addStepLog("User Logins to ownerhelp Portal");

	}

	@And("^User navigate to cases tab$")
	public void User_navigate_to_cases_tab() throws Throwable {
		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(CaseLifecycle_Container.CasesTab);
		ActionHandler.wait(1);
		Reporter.addStepLog("User navigates to cases tab");

	}

	@Then("^User fill all the mandatory details$")
	public void User_fill_all_the_mandatory_details() throws Throwable {
		if (VerifyHandler.verifyElementPresent(SSEC_2293_ClassicPartsOwnerHelpForm_Container.Allowall)) {

			ActionHandler.click(SSEC_2293_ClassicPartsOwnerHelpForm_Container.Allowall);
			CommonFunctions.attachScreenshot();

		}

		double randomNumber = getRandomIntegerBetweenRange(1, 1000);

		ActionHandler.click(SSEC_2293_ClassicPartsOwnerHelpForm_Container.FirstName);
		ActionHandler.wait(1);
		ActionHandler.setText(SSEC_2293_ClassicPartsOwnerHelpForm_Container.FirstName, "Test");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SSEC_2293_ClassicPartsOwnerHelpForm_Container.Lastname);
		ActionHandler.wait(1);
		ActionHandler.setText(SSEC_2293_ClassicPartsOwnerHelpForm_Container.Lastname, "SVO" + randomNumber);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SSEC_2293_ClassicPartsOwnerHelpForm_Container.Town);
		ActionHandler.wait(1);
		ActionHandler.setText(SSEC_2293_ClassicPartsOwnerHelpForm_Container.Town, "India");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SSEC_2293_ClassicPartsOwnerHelpForm_Container.Email);
		ActionHandler.wait(1);
		ActionHandler.setText(SSEC_2293_ClassicPartsOwnerHelpForm_Container.Email, "enquiryRequest2022@gmail.com");

//		ActionHandler.setText(SSEC_2293_ClassicPartsOwnerHelpForm_Container.Email,
//				"Test" + randomNumber + "@gmail.com");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SSEC_2293_ClassicPartsOwnerHelpForm_Container.PhoneNumber);
		ActionHandler.wait(1);
		ActionHandler.setText(SSEC_2293_ClassicPartsOwnerHelpForm_Container.PhoneNumber, "09" + randomNumber);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SSEC_2293_ClassicPartsOwnerHelpForm_Container.VINforOwnerHelpForm);
		ActionHandler.wait(1);
		ActionHandler.setText(SSEC_2293_ClassicPartsOwnerHelpForm_Container.VINforOwnerHelpForm, "07" + randomNumber);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SSEC_2293_ClassicPartsOwnerHelpForm_Container.Mileage);
		ActionHandler.wait(1);
		ActionHandler.setText(SSEC_2293_ClassicPartsOwnerHelpForm_Container.Mileage, "5" + randomNumber);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SSEC_2293_ClassicPartsOwnerHelpForm_Container.Symptom);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act1.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SSEC_2293_ClassicPartsOwnerHelpForm_Container.Faultlocation);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		Actions act2 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act1.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SSEC_2293_ClassicPartsOwnerHelpForm_Container.Description);
		ActionHandler.wait(1);
		ActionHandler.setText(SSEC_2293_ClassicPartsOwnerHelpForm_Container.Description, "TEST");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SSEC_2293_ClassicPartsOwnerHelpForm_Container.Submit);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
	}

	@Then("^User is getting error to not fill all the mandatory details$")
	public void User_is_getting_error_to_not_fill_all_the_mandatory_details() throws Throwable {
		if (VerifyHandler.verifyElementPresent(SSEC_2293_ClassicPartsOwnerHelpForm_Container.Allowall)) {
			ActionHandler.wait(1);
			ActionHandler.click(SSEC_2293_ClassicPartsOwnerHelpForm_Container.Allowall);
			CommonFunctions.attachScreenshot();

		}

		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		ActionHandler.click(SSEC_2293_ClassicPartsOwnerHelpForm_Container.FirstName);
		ActionHandler.wait(1);
		ActionHandler.setText(SSEC_2293_ClassicPartsOwnerHelpForm_Container.FirstName, "Test");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SSEC_2293_ClassicPartsOwnerHelpForm_Container.Lastname);
		ActionHandler.wait(1);
		ActionHandler.setText(SSEC_2293_ClassicPartsOwnerHelpForm_Container.Lastname, "SVO" + randomNumber);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SSEC_2293_ClassicPartsOwnerHelpForm_Container.Town);
		ActionHandler.wait(1);
		ActionHandler.setText(SSEC_2293_ClassicPartsOwnerHelpForm_Container.Town, "India");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SSEC_2293_ClassicPartsOwnerHelpForm_Container.Email);
		ActionHandler.wait(1);
		ActionHandler.setText(SSEC_2293_ClassicPartsOwnerHelpForm_Container.Email,
				"Test" + randomNumber + "@gmail.com");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SSEC_2293_ClassicPartsOwnerHelpForm_Container.PhoneNumber);
		ActionHandler.wait(1);
		ActionHandler.setText(SSEC_2293_ClassicPartsOwnerHelpForm_Container.PhoneNumber, "09" + randomNumber);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SSEC_2293_ClassicPartsOwnerHelpForm_Container.Symptom);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act1.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SSEC_2293_ClassicPartsOwnerHelpForm_Container.Faultlocation);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		Actions act2 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act1.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SSEC_2293_ClassicPartsOwnerHelpForm_Container.Description);
		ActionHandler.wait(1);
		ActionHandler.setText(SSEC_2293_ClassicPartsOwnerHelpForm_Container.Description, "TEST");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SSEC_2293_ClassicPartsOwnerHelpForm_Container.Submit);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		VerifyHandler.verifyElementPresent(SSEC_2293_ClassicPartsOwnerHelpForm_Container.Error);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

	}

	@And("^User close the browser$")
	public void User_close_the_browser() throws Throwable {
		ActionHandler.wait(1);
		driver.quit();
	}

	public String checkEmailForClassicPartsTechUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SSEC_2293_ClassicPartsOwnerHelpForm_Container.outlookSignin)) {

			ActionHandler.click(SSEC_2293_ClassicPartsOwnerHelpForm_Container.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SSEC_2293_ClassicPartsOwnerHelpForm_Container.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SSEC_2293_ClassicPartsOwnerHelpForm_Container.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SSEC_2293_ClassicPartsOwnerHelpForm_Container.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SSEC_2293_ClassicPartsOwnerHelpForm_Container.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SSEC_2293_ClassicPartsOwnerHelpForm_Container.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}
		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SSEC_2293_ClassicPartsOwnerHelpForm_Container.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SSEC_2293_ClassicPartsOwnerHelpForm_Container.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SSEC_2293_ClassicPartsOwnerHelpForm_Container.outlookVerificationCode.getText();
				vCode = verificationCode.substring(493, 499);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	@Given("^Laterally Access to SVO Portal as Classic Parts and technical user with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void Laterally_Access_SVO_Portal_Classic_Parts_tech_with_username_password(String userName, String password)
			throws Exception {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		Reporter.addStepLog("User access SF SVO Portal");

		driver = getDriver();
		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SSEC_2293_ClassicPartsOwnerHelpForm_Container.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SSEC_2293_ClassicPartsOwnerHelpForm_Container.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SSEC_2293_ClassicPartsOwnerHelpForm_Container.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SSEC_2293_ClassicPartsOwnerHelpForm_Container.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailForClassicPartsTechUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SSEC_2293_ClassicPartsOwnerHelpForm_Container.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SSEC_2293_ClassicPartsOwnerHelpForm_Container.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SSEC_2293_ClassicPartsOwnerHelpForm_Container.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SSEC_2293_ClassicPartsOwnerHelpForm_Container.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SSEC_2293_ClassicPartsOwnerHelpForm_Container.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SSEC_2293_ClassicPartsOwnerHelpForm_Container.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SSEC_2293_ClassicPartsOwnerHelpForm_Container.remindMeLater)) {

				ActionHandler.click(SSEC_2293_ClassicPartsOwnerHelpForm_Container.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SSEC_2293_ClassicPartsOwnerHelpForm_Container.notRegister)) {

				ActionHandler.click(SSEC_2293_ClassicPartsOwnerHelpForm_Container.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SSEC_2293_ClassicPartsOwnerHelpForm_Container.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SSEC_2293_ClassicPartsOwnerHelpForm_Container.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SSEC_2293_ClassicPartsOwnerHelpForm_Container.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

}
