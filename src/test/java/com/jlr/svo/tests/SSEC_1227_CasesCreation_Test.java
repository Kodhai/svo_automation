package com.jlr.svo.tests;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.autotest.utils.Config;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.constants.Constants;
import com.jlr.svo.containers.SSEC_1227_CasesCreationContainer;
import com.jlr.svo.containers.SSEC_1229_Case_QueuesContainer;
import com.jlr.svo.containers.SSEC_1237_CaseClosureContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SSEC_1227_CasesCreation_Test extends TestBaseCC {
	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory.getLogger(SSEC_1227_CasesCreation_Test.class.getName());
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);

	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SSEC_1227_CasesCreationContainer CasesContainer = PageFactory.initElements(driver,
			SSEC_1227_CasesCreationContainer.class);
	SSEC_1237_CaseClosureContainer CaseClosureContainer = PageFactory.initElements(driver,
			SSEC_1237_CaseClosureContainer.class);

	SSEC_1229_Case_QueuesContainer Case_QueuesContainer = PageFactory.initElements(driver, SSEC_1229_Case_QueuesContainer.class); 

//

	public static String verificationCode;
	public static String vCode;
	double randomNum = getRandomNumber(0, 10000);
	String firstName = "Test" + randomNum;
	String lastName = "tes" + randomNum;
	String VINNum = "Test";
	String Chassis = VINNum + randomNum;
	String Chassis2 = VINNum + randomNum;

	public static double getRandomNumber(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	// onStart
	public void onStart() {
		setupTest("SVOTest");
		driver = getDriver();
		CasesContainer = PageFactory.initElements(driver, SSEC_1227_CasesCreationContainer.class);
		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		Case_QueuesContainer = PageFactory.initElements(driver, SSEC_1229_Case_QueuesContainer.class); 
		verificationCode = null;

	}

	public String checkEmailForClassicPartsUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(320, 326);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	@Given("^Access to SVO Portal as Classic Parts user with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void Access_SVO_Portal_Classic_Parts_with_username_password(String userName, String password)
			throws Exception {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		Reporter.addStepLog("User access SF SVO Portal");

		onStart();
		driver = getDriver();
		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailForClassicPartsUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(2);

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	@Given("^Access to SVO Classic Operations portal with Username \"([^\"]*)\" and password \"([^\"]*)\"$")
	public void User_access_SVO_Classic_portal(String username, String password) throws Exception {
		String un[] = username.split(",");
		username = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		onStart();
		driver = getDriver();
		ActionHandler.wait(10);
		driver.get(Constants.SVOURL);
		ActionHandler.wait(15);

		ActionHandler.setText(CasesContainer.UserNameTextBox, username);
		ActionHandler.setText(CasesContainer.PasswordTextBox, password);
		ActionHandler.click(CasesContainer.LoginBtn);
		ActionHandler.wait(8);

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");
			ActionHandler.wait(10);
			vCode = SVOenquiry.checkEmail();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			ActionHandler.wait(15);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(CasesContainer.UserNameTextBox, username);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(CasesContainer.PasswordTextBox, password);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(120);

			ActionHandler.click(CasesContainer.LoginBtn);
			ActionHandler.wait(7);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(3);
			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(2);
			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {
			ActionHandler.wait(5);
			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {
				ActionHandler.wait(3);
				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {
				ActionHandler.wait(3);
				ActionHandler.click(SVOEnquiryContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();
				ActionHandler.wait(2);
			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	@When("^User searches the Case page in searchbox$")
	public void user_searches_case_page() throws Exception {

		ActionHandler.setText(CasesContainer.SearchBox, "Case");
		ActionHandler.wait(10);
	}

	@When("^User navigates to cases tab$")
	public void User_navigates_cases_tab() throws Throwable {

		javaScriptUtil.clickElementByJS(CasesContainer.CasesTab);
		ActionHandler.wait(5);
	}

	@Then("^User creates new Parts and Technical Record type case with mandatory fields like VIN$")
	public void user_creates_New_case() throws Exception {

		ActionHandler.click(CaseClosureContainer.NewBtn);
		ActionHandler.click(Case_QueuesContainer.PartandTechRecordType);
		ActionHandler.click(CasesContainer.NextBtn);
		ActionHandler.wait(5);
		ActionHandler.scrollToView(CasesContainer.VINText);
		ActionHandler.wait(5);
		ActionHandler.setText(CasesContainer.VINText, Chassis);
		ActionHandler.wait(5);
	}

	@Given("^User login to SVO Portal by Classic Customer Service user with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void user_login_to_an_SVO_portal_by_Classic_Customer_Service_user_with_Username_and_Password(String userName,
			String password) throws Throwable {

		onStart();
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailOnCustomerServiceUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			ActionHandler.wait(1);
			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				CommonFunctions.attachScreenshot();

			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				CommonFunctions.attachScreenshot();

			} else {

				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {

			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	public String checkEmailOnCustomerServiceUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(1);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(494, 500);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	@And("^User creates new Customer services case with all mandatory fields$")
	public void User_creates_new_customer() throws Exception {

		ActionHandler.click(Case_QueuesContainer.CustServiceBtn);
		ActionHandler.wait(3);
		ActionHandler.click(CasesContainer.NextBtn);
		ActionHandler.wait(3);
		ActionHandler.setText(CasesContainer.FirstNameBtn, firstName);
		ActionHandler.wait(3);
	}

	@Then("^User saves the case$")
	public void User_saves_the_customer_case() throws Exception {

		ActionHandler.click(CasesContainer.SaveBtn);
		ActionHandler.wait(3);
	}

	@And("^User verifies that the case cannot be deleted$")
	public void User_verifies_case_cannot_be_deleted() throws Exception {

		ActionHandler.click(CasesContainer.EditBtn);
		ActionHandler.wait(3);
		ActionHandler.click(CasesContainer.CancelBtn);
		ActionHandler.wait(3);
	}

	@And("^User validates the new case created with the VIN$")
	public void user_validates_new_cases() throws Exception {

		String validate = driver.findElement(By.xpath("//p[contains(@class, 'slds-truncate')])[4]")).getText();

		if (Chassis.equalsIgnoreCase(validate)) {
			System.out.println("New Case Created" + validate);
		} else {
			System.out.println("No case created!!");
		}

		ActionHandler.wait(5);
	}

	@Then("^User validates new cust case")
	public void user_validates_new_cust_case() throws Exception {

		String validateText = driver.findElement(By.xpath("(//p[contains(@class, 'slds-truncate')])[6]")).getText();

		if (firstName.equalsIgnoreCase(validateText)) {
			System.out.println("Case is successfully created");
		} else {
			System.out.println("Case Not created");
		}
	}

	@And("^User verifies that the case is editable$")
	public void User_verifies_case_editable() throws Exception {

		ActionHandler.click(CasesContainer.EditBtn);
		ActionHandler.wait(3);
		ActionHandler.setText(CasesContainer.LastNameText, lastName);
		ActionHandler.wait(3);
		ActionHandler.click(CasesContainer.SaveBtn);
		ActionHandler.wait(3);
	}

	@And("^User creates new Parts and Technical Record type case$")
	public void User_creates_new_parts_and_technical_record() throws Exception {

		ActionHandler.click(Case_QueuesContainer.PartandTechRecordType);
		ActionHandler.click(CasesContainer.NextBtn);
		ActionHandler.wait(5);
	}

	@Then("^User saves the case without filling mandatory field like VIN$")
	public void User_saves_case_without_filling_mandatory_field() throws Exception {

		ActionHandler.click(CasesContainer.SaveBtn);
		ActionHandler.wait(5);
	}

	@Then("^User saves and creates a new case with the same record type$")
	public void user_saves_and_creates_new_case() throws Exception {

		ActionHandler.click(CasesContainer.SaveNewBtn);
		ActionHandler.wait(5);
	}

	@And("^User verifies that it is unable to save$")
	public void User_verifies_that_it_unable_to_save() throws Exception {

		String VINError = "These required fields must be completed: VIN / Chassis Number";
		String validates = driver
				.findElement(By.xpath("//li[text()='These required fields must be completed: VIN / Chassis Number']"))
				.getText();

		if (VINError.equalsIgnoreCase(validates)) {
			System.out.println("It's working fine");
		} else {
			System.out.println("Something is wrong!!!");
		}

	}

	@Then("^User validates the new parts and technical case$")
	public void user_validates_new_case() throws Exception {

		ActionHandler.click(Case_QueuesContainer.PartandTechRecordType);
		ActionHandler.wait(5);
		ActionHandler.click(CasesContainer.NextBtn);
		ActionHandler.wait(5);
		ActionHandler.scrollToView(CasesContainer.VINText);
		ActionHandler.wait(5);
		ActionHandler.setText(CasesContainer.VINText, Chassis2);
		ActionHandler.wait(5);
		ActionHandler.click(CasesContainer.SaveBtn);

	}

	@And("^User logout from the portal$")
	public void User_logout_from_portal() throws Exception {

		ActionHandler.click(CasesContainer.ProfileBtn);
		ActionHandler.wait(3);
		ActionHandler.click(CasesContainer.LogoutBtn);
		ActionHandler.wait(5);
	}

}
