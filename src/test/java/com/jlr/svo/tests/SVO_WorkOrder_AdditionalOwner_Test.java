package com.jlr.svo.tests;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.autotest.utils.Config;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.constants.Constants;
import com.jlr.svo.containers.SSEC_1005_AutopoulatedFieldsOnOrderWorkOrderContainer;
import com.jlr.svo.containers.SVOAdditionalVehicleContainer;
import com.jlr.svo.containers.SVOCommentFieldAdditionContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.containers.SVOItemToApproveContainer;
import com.jlr.svo.containers.SVOOpportunityContainer;
import com.jlr.svo.containers.SVOWorkOrderContainer;
import com.jlr.svo.containers.SVO_CancelOrderAtAnyStageContainer;
import com.jlr.svo.containers.SVO_RestrictedPartyScreeningContainer;
import com.jlr.svo.containers.SVO_WorkOrder_AdditionalOwner_Container;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class SVO_WorkOrder_AdditionalOwner_Test extends TestBaseCC {
	private WebDriver driver = getDriver();
	private static final Logger LOGGER = LoggerFactory.getLogger(SVO_WorkOrder_AdditionalOwner_Test.class.getName());
	SVOCommentFieldAdditionContainer SVOCommentFieldAdditionContainer = PageFactory.initElements(driver,
			SVOCommentFieldAdditionContainer.class);
	SVOWorkOrderContainer SVOWorkOrderContainer = PageFactory.initElements(driver, SVOWorkOrderContainer.class);

	public static String WorkOrderNumber;
	public static String verificationCode;
	public static String vCode;
	public static String veriCode;
	public static String OwnerName;
	public static String AdditionalOwnerName;
	public static String OpportunityName;

	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	SVO_RestrictedPartyScreeningContainer RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
			SVO_RestrictedPartyScreeningContainer.class);

	SVOItemToApproveContainer SVOItemToapproveContainer = PageFactory.initElements(driver,
			SVOItemToApproveContainer.class);
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
	SVOAdditionalVehicleContainer SVOAdditionalvehicleContainer = PageFactory.initElements(driver,
			SVOAdditionalVehicleContainer.class);
	SVO_WorkOrder_AdditionalOwner_Container SVO_WorkOrder_AdditionalOwner_Container = PageFactory.initElements(driver,
			SVO_WorkOrder_AdditionalOwner_Container.class);
	SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SVO_CancelOrderAtAnyStageContainer SVO_Cancel_Order_Container = PageFactory.initElements(driver,
			SVO_CancelOrderAtAnyStageContainer.class);
	SSEC_1005_AutopoulatedFieldsOnOrderWorkOrderContainer AutopoulatedFieldsOnOrderWorkOrder = PageFactory
			.initElements(driver, SSEC_1005_AutopoulatedFieldsOnOrderWorkOrderContainer.class);

	@Then("^User selects an existing Work Order from All Work orders List view$")
	public void user_selects_an_existing_Work_Order_from_All_Work_orders_List_view() throws Throwable {

		WorkOrderNumber = null;

		ActionHandler.click(SVOWorkOrderContainer.RecentlyViewedLink);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on recently viewed link");

		ActionHandler.click(SVOWorkOrderContainer.AllWorkOrdersListView);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects All Work orders list from Select List view");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.FirstWorkOrder);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects First Work Order from All Work orders List view");

		WorkOrderNumber = SVOWorkOrderContainer.NewlyCreatedWorkOrderNumber.getText();
		Reporter.addStepLog("Work Order Number Name is = " + WorkOrderNumber);

	}

	public String checkEmailForClassicOpsUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(338, 344);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	@Given("^Access to SVO Portal as classic Operations user from Add Username user with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void access_to_an_SVO_portal_as_classic_operations_user_from_Add_Username_user_with_Username_and_Password(
			String userName, String password) throws Throwable {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		ActionHandler.click(SVO_OpportunityContainer.icon_image);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Logout");

		ActionHandler.click(SVOWorkOrderContainer.AddUsername);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Add Username");

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailForClassicOpsUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		}

	}

	@And("^Verify the Additional Owner field on Work Orders page$")
	public void Verify_the_Additional_Owner_field_on_Work_Orders_page() throws Throwable {

		ActionHandler.wait(5);
		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(3);

		javaScriptUtil.scrollIntoView(SVO_WorkOrder_AdditionalOwner_Container.AdditionalOwnerField);

		VerifyHandler.verifyElementPresent(SVO_WorkOrder_AdditionalOwner_Container.AdditionalOwnerField);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User is able to view Additional Owner field on Work Orders page");

	}

	@Then("^Add Classic User \"([^\"]*)\" as new Additional Owner for an existing Work order$")
	public void add_Classic_User_as_new_Additional_Owner_for_an_existing_Work_order(String AdditionalOwner)
			throws Throwable {

		String AO[] = AdditionalOwner.split(",");
		AdditionalOwner = CommonFunctions.readExcelMasterData(AO[0], AO[1], AO[2]);

		ActionHandler.wait(5);
		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(3);

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.EditAdditionalOwnerFieldIcon);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Edit Additional Owner Icon");

		if (VerifyHandler.verifyElementPresent(SVO_WorkOrder_AdditionalOwner_Container.ClearAdditionalOwnerFieldIcon)) {
			ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.ClearAdditionalOwnerFieldIcon);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User remove the existing Additional owner field");
		}

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.AdditionalOwnerFieldInputTextBox);
		ActionHandler.wait(2);
		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.AdditionalOwnerFieldInputTextBox,
				AdditionalOwner);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters classic operations user name on Additional owner text box");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.ClassicServiceUser);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects classic operations user as Additional owner");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.AdditionalOwnerFieldSaveButton);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on save button of Additional owner pop up window");

	}

	@Then("^Verify that the additional owner field is updated with the classic user \"([^\"]*)\"$")
	public void verify_that_the_additional_owner_field_is_updated_with_the_classic_user(String AdditionalOwner)
			throws Throwable {

		String AO[] = AdditionalOwner.split(",");
		AdditionalOwner = CommonFunctions.readExcelMasterData(AO[0], AO[1], AO[2]);

		VerifyHandler.verifyElementPresent(SVO_WorkOrder_AdditionalOwner_Container.AdditionalOwnerAutoPopulatedField);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Additional Owner field is autopopulated with user name : " + AdditionalOwner);

	}

	@And("^User selects the work order for which additional owner is changed to classic Operations user$")
	public void User_selects_the_work_order_for_which_additional_owner_is_changed_to_classic_Operations_user()
			throws Throwable {

		ActionHandler.click(SVOWorkOrderContainer.RecentlyViewedLink);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on recently viewed link");

		ActionHandler.click(SVOWorkOrderContainer.AllWorkOrdersListView);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects All Work orders list from Select List view");

		ActionHandler.click(SVOWorkOrderContainer.OrderNumberSearchBox);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOWorkOrderContainer.OrderNumberSearchBox, WorkOrderNumber);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters Work Order number in the search box");

		ActionHandler.wait(3);
		ActionHandler.pressEnter();
		ActionHandler.wait(3);

		ActionHandler.click(SVO_OpportunityContainer.FirstOpportunity);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects First Work Order from All Work orders List view");

		WorkOrderNumber = SVOWorkOrderContainer.NewlyCreatedWorkOrderNumber.getText();
		Reporter.addStepLog("Work Order Number Name is = " + WorkOrderNumber);

	}

	@And("^Verify that user is able to view the work order details on work orders page$")
	public void Verify_that_user_is_able_to_view_the_work_order_details_on_work_orders_page() throws Throwable {

		VerifyHandler.verifyElementPresent(SVOWorkOrderContainer.NewlyCreatedWorkOrderNumber);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User is able to view the work order details on work orders page");
	}

	@And("^Verify that user is not able to view Additional Owner field on work orders page$")
	public void Verify_that_user_is_not_able_to_view_Additional_Owner_field_on_work_orders_page() throws Throwable {

		ActionHandler.wait(5);
		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(3);

		VerifyHandler.verifyElementPresent(SVO_WorkOrder_AdditionalOwner_Container.AdditionalOwnerField);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User is not able to view Additional Owner field on work orders page");
	}

	@And("^Verify that user is not able to view Edit Additional Owner Icon on work orders page$")
	public void Verify_that_user_is_not_able_to_view_Edit_Additional_Owner_Icon_on_work_orders_page() throws Throwable {

		ActionHandler.wait(5);
		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(3);

		VerifyHandler.verifyElementPresent(SVO_WorkOrder_AdditionalOwner_Container.EditAdditionalOwnerFieldIcon);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User is not able to view Edit Additional Owner Icon on work orders page");
	}

	@Given("^Access to SVO Portal as classic operation from Add Username user with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void access_to_an_SVO_portal_as_classic_operation_from_Add_Username_user_with_Username_and_Password(
			String userName, String password) throws Throwable {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		ActionHandler.click(SVO_OpportunityContainer.icon_image);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Logout");

		ActionHandler.click(SVOWorkOrderContainer.AddUsername);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Add Username");

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailForclassicoperationFromUsernameUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		}
	}

	public String checkEmailForclassicoperationFromUsernameUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookIcon)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(338, 344);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;

	}

	@Given("^Access to SVO Portal as classic service user from Add Username user with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void access_to_an_SVO_portal_as_classic_service_user_from_Add_Username_user_with_Username_and_Password(
			String userName, String password) throws Throwable {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		ActionHandler.click(SVO_OpportunityContainer.icon_image);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Logout");

		ActionHandler.click(SVOWorkOrderContainer.AddUsername);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Add Username");

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailForclassicserviceFromUsernameUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		}
	}

	public String checkEmailForclassicserviceFromUsernameUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(342, 349);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;

	}

	@Then("^User selects All from Select List view$")
	public void user_selects_All_from_Select_List_view() throws Throwable {

		ActionHandler.click(SVOWorkOrderContainer.RecentlyViewedLink);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on recently viewed link");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.AllWorkOrderList);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects All Work orders list from Select List view");

	}

	@Then("^Verify that user is able to edit 'Additional Owner' field on work orders page Additional Owner Name \"([^\"]*)\"$")
	public void verify_that_user_is_able_to_edit_Additional_Owner_field_on_work_orders_page(String additionalowner)
			throws Throwable {

		String AO[] = additionalowner.split(",");
		additionalowner = CommonFunctions.readExcelMasterData(AO[0], AO[1], AO[2]);

		AdditionalOwnerName = null;

		javaScriptUtil.clickElementByJS(SVO_WorkOrder_AdditionalOwner_Container.SortStartForecast);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		javaScriptUtil.clickElementByJS(SVO_WorkOrder_AdditionalOwner_Container.SortStartForecast);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		javaScriptUtil.clickElementByJS(SVO_WorkOrder_AdditionalOwner_Container.SelectFirstWorkOrder);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects first work order in the list");

		VerifyHandler.verifyElementPresent(SVO_WorkOrder_AdditionalOwner_Container.AdditinalOwnerFiled);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Additional Owner field is displayed under details section of work order");

		javaScriptUtil.clickElementByJS(SVO_WorkOrder_AdditionalOwner_Container.AdditinalOwnerFiledEditBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Edit button under Additional Owner section");
		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(3);

		if (VerifyHandler.verifyElementPresent(SVO_WorkOrder_AdditionalOwner_Container.ClearAdditinalOwnerName)) {
			ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.ClearAdditinalOwnerName);
			ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.UserNameSearchBox);
			CommonFunctions.attachScreenshot();
			ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.UserNameSearchBox, additionalowner);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(3);
			ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.ShowAllUsersList);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(3);
			ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.UserFullName);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.SaveBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("user is able to edit 'Additional Owner' field on work orders page");
			CommonFunctions.attachScreenshot();
		} else {

			ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.UserNameSearchBox);
			CommonFunctions.attachScreenshot();
			ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.UserNameSearchBox, additionalowner);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.ShowAllUsersList);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.UserFullName);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.SaveBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("user is able to edit 'Additional Owner' field on work orders page");
		}

		AdditionalOwnerName = SVO_WorkOrder_AdditionalOwner_Container.AdditinalOwnerFiledName.getText();
		Reporter.addStepLog("Updated additional Owner Name is = " + AdditionalOwnerName);

		VerifyHandler.verifyElementPresent(SVO_WorkOrder_AdditionalOwner_Container.AdditinalOwnerFiledName);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verify updated additional owner name under details section of work order");
	}

	@Then("^Verify that user is able to remove 'Additional Owner' field on work orders page Additional Owner Name \"([^\"]*)\"$")
	public void verify_that_user_is_able_to_remove_Additional_Owner_field_on_work_orders_page(String additionalowner)
			throws Throwable {

		String AO[] = additionalowner.split(",");
		additionalowner = CommonFunctions.readExcelMasterData(AO[0], AO[1], AO[2]);

		driver.navigate().refresh();
		ActionHandler.wait(10);

		javaScriptUtil.clickElementByJS(SVO_WorkOrder_AdditionalOwner_Container.SortStartForecast);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		javaScriptUtil.clickElementByJS(SVO_WorkOrder_AdditionalOwner_Container.SortStartForecast);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		javaScriptUtil.clickElementByJS(SVO_WorkOrder_AdditionalOwner_Container.SelectFirstWorkOrder);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		Reporter.addStepLog("User selects first work order in the list");

		WorkOrderNumber = SVO_WorkOrder_AdditionalOwner_Container.WorkOrderNumber.getText();
		Reporter.addStepLog("Work Order Number is = " + WorkOrderNumber);
	

		VerifyHandler.verifyElementPresent(SVO_WorkOrder_AdditionalOwner_Container.AdditinalOwnerFiled);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Additional Owner field is displayed under details section of work order");

		javaScriptUtil.clickElementByJS(SVO_WorkOrder_AdditionalOwner_Container.AdditinalOwnerFiledEditBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Edit button under Additional Owner section");

		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(3);

		if (VerifyHandler.verifyElementPresent(SVO_WorkOrder_AdditionalOwner_Container.ClearAdditinalOwnerName)) {
			ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.ClearAdditinalOwnerName);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(2);

			ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.SaveBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("user is able to remove 'Additional Owner' name on work orders page");
			CommonFunctions.attachScreenshot();
		}

		else {

			ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.CancelBtn);
			ActionHandler.pageCompleteScrollDown();
			ActionHandler.wait(3);
			javaScriptUtil.clickElementByJS(SVO_WorkOrder_AdditionalOwner_Container.AdditinalOwnerFiledEditBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.UserNameSearchBox);
			CommonFunctions.attachScreenshot();
			ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.UserNameSearchBox, additionalowner);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.ShowAllUsersList);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.UserFullName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.SaveBtn);
			CommonFunctions.attachScreenshot();

			javaScriptUtil.clickElementByJS(SVO_WorkOrder_AdditionalOwner_Container.AdditinalOwnerFiledEditBtn);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.ClearAdditinalOwnerName);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.SaveBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("user is able to remove 'Additional Owner' name on work orders page");
		}

	}

	@Then("^User is not able to view the work order details on my work orders page$")
	public void user_is_not_able_to_view_the_work_order_details_on_my_work_orders_page() throws Throwable {

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.WorkOrderSearchBox);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.WorkOrderSearchBox, WorkOrderNumber);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		ActionHandler.pressEnter();
		ActionHandler.wait(3);
		javaScriptUtil.clickElementByJS(SVO_WorkOrder_AdditionalOwner_Container.SortStartForecast);
		if (!VerifyHandler.verifyElementPresent(SVO_WorkOrder_AdditionalOwner_Container.SelectFirstWorkOrder)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Verify that user is not able to view deleted work order on work orders page");
		} else {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Verify that user is able to view deleted work order on work orders page");
		}
	}

	@Then("^Verify the error message while entering same name in both owner and additional owner field$")
	public void verify_the_error_message_while_entering_same_name_in_both_owner_and_additional_owner_field()
			throws Throwable {

		javaScriptUtil.clickElementByJS(SVO_WorkOrder_AdditionalOwner_Container.SortStartForecast);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		javaScriptUtil.clickElementByJS(SVO_WorkOrder_AdditionalOwner_Container.SortStartForecast);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		javaScriptUtil.clickElementByJS(SVO_WorkOrder_AdditionalOwner_Container.SelectFirstWorkOrder);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects first work order in the list");

		VerifyHandler.verifyElementPresent(SVO_WorkOrder_AdditionalOwner_Container.AdditinalOwnerFiled);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Additional Owner field is displayed under details section of work order");

		javaScriptUtil.clickElementByJS(SVO_WorkOrder_AdditionalOwner_Container.AdditinalOwnerFiledEditBtn);
		CommonFunctions.attachScreenshot();
		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(3);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.CancelBtn);
		ActionHandler.wait(3);
		OwnerName = SVO_WorkOrder_AdditionalOwner_Container.OwnerName.getText();
		Reporter.addStepLog("Owner name is = " + OwnerName);

		javaScriptUtil.clickElementByJS(SVO_WorkOrder_AdditionalOwner_Container.AdditinalOwnerFiledEditBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Edit button under Additional Owner section");

		if (VerifyHandler.verifyElementPresent(SVO_WorkOrder_AdditionalOwner_Container.ClearAdditinalOwnerName)) {
			ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.ClearAdditinalOwnerName);
			ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.UserNameSearchBox);
			CommonFunctions.attachScreenshot();
			ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.UserNameSearchBox, OwnerName);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(3);
			ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.ShowAllUsersList);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(3);
			ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.UserFullName);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.SaveBtn);
			CommonFunctions.attachScreenshot();
			VerifyHandler.verifyElementPresent(
					SVO_WorkOrder_AdditionalOwner_Container.ErrormessageForSameAdditionalOwnerName);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog(
					"user is able to Verify the error message while entering same name in both owner and additional owner field");
			CommonFunctions.attachScreenshot();
		} else {

			ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.UserNameSearchBox);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.UserNameSearchBox, OwnerName);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.ShowAllUsersList);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.UserFullName);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.SaveBtn);
			CommonFunctions.attachScreenshot();
			VerifyHandler.verifyElementPresent(
					SVO_WorkOrder_AdditionalOwner_Container.ErrormessageForSameAdditionalOwnerName);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog(
					"user is able to Verify the error message while entering same name in both owner and additional owner field");
		}
	}

	@Then("^Verify that user is able to delete work order on work orders page$")
	public void verify_that_user_is_able_to_delete_work_order_on_work_orders_page() throws Throwable {

		javaScriptUtil.clickElementByJS(SVO_WorkOrder_AdditionalOwner_Container.SelectFirstWorkOrder);
		CommonFunctions.attachScreenshot();
		javaScriptUtil.clickElementByJS(SVO_WorkOrder_AdditionalOwner_Container.DeleteBtn);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.DeleteDoneBtn);
		CommonFunctions.attachScreenshot();
		VerifyHandler.verifyElementPresent(SVO_WorkOrder_AdditionalOwner_Container.DelecteSuccessfullMessage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user is able to delete work order successfully");
	}

	@Then("^User creates a new work order on work order page$")
	public void user_createa_a_new_work_order_on_work_order_page() throws Throwable {

		ActionHandler.click(SVOWorkOrderContainer.NewButtonForCreateNewWorkOrder);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on new button in order to create new work order");

		ActionHandler.click(SVOWorkOrderContainer.NewBuildRecordType);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Selects New Build Record Type");

		ActionHandler.click(SVOWorkOrderContainer.NextButtonForCreateNewWorkOrder);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on next button");

		ActionHandler.click(SVOWorkOrderContainer.SaveButtonForCreateNewWorkOrder);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Save button");

		WorkOrderNumber = SVOWorkOrderContainer.NewlyCreatedWorkOrderNumber.getText();
		Reporter.addStepLog("Work Order Number Name is = " + WorkOrderNumber);
	}

	@Then("^user is able to view newly created work order on 'My work orders' list view on work order page$")
	public void user_is_able_to_view_newly_created_work_order_on_My_work_orders_list_view_on_work_order_page()
			throws Throwable {

		ActionHandler.click(SVOWorkOrderContainer.WorkOrdersDropdown);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks Work Orders Dropdown button");

		ActionHandler.click(SVOWorkOrderContainer.MyWorkOrders);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Selects My Work Orders section");

		ActionHandler.wait(3);
		ActionHandler.setText(SVOWorkOrderContainer.OrderNumberSearchBox, WorkOrderNumber);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters work order number in search box");

		ActionHandler.wait(3);
		ActionHandler.pressEnter();
		ActionHandler.wait(3);
		Reporter.addStepLog("User Enters Newly created work order number in tha search box");

		VerifyHandler.verifyElementPresent(
				driver.findElement(By.xpath(SVOWorkOrderContainer.NewlyCreatedWorkOrderNumber(WorkOrderNumber))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User is able to view view newly created work order on 'My work orders' list view");

	}

	@And("^User create a Classic Reborn Opportunity with check cleared like Stage \"([^\"]*)\" Product Offering \"([^\"]*)\" Region \"([^\"]*)\" Client \"([^\"]*)\" Restricted Party Screening Stage \"([^\"]*)\" Preferred Contact \"([^\"]*)\" Account Name \"([^\"]*)\" Retailer Contact \"([^\"]*)\" Source \"([^\"]*)\" Origination Divission \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\"$")
	public void User_create_a_Classic_Reborn_Opportunity_with_check_cleared(String stage, String productOffering,
			String region, String client, String Checkcleared, String preferredContact, String accountName,
			String retailerContact, String source, String originatingdivission, String brand, String model)
			throws Exception {
		String oppName = "Test_Classic_Reborn_Opportunity_";
		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		String closeDate = CommonFunctions.selectFutureDate();
		Reporter.addStepLog("Opportunity Close Date is = " + closeDate);

		String s[] = stage.split(",");
		stage = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String proOff[] = productOffering.split(",");
		productOffering = CommonFunctions.readExcelMasterData(proOff[0], proOff[1], proOff[2]);

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String c[] = client.split(",");
		client = CommonFunctions.readExcelMasterData(c[0], c[1], c[2]);

		String RPS[] = Checkcleared.split(",");
		Checkcleared = CommonFunctions.readExcelMasterData(RPS[0], RPS[1], RPS[2]);

		String preCon[] = preferredContact.split(",");
		preferredContact = CommonFunctions.readExcelMasterData(preCon[0], preCon[1], preCon[2]);

		String accName[] = accountName.split(",");
		accountName = CommonFunctions.readExcelMasterData(accName[0], accName[1], accName[2]);

		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		String so[] = source.split(",");
		source = CommonFunctions.readExcelMasterData(so[0], so[1], so[2]);

		String OD[] = originatingdivission.split(",");
		originatingdivission = CommonFunctions.readExcelMasterData(OD[0], OD[1], OD[2]);

		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String m[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		Reporter.addStepLog("User starts creating new SVO Bespoke Opportunity");
		ActionHandler.click(SVOAdditionalvehicleContainer.NewOpportunity);
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.QuoteNextBtn)) {

			ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.ClassicRebornRecType);
			ActionHandler.wait(1);
			Reporter.addStepLog("User selects opportunity record type as Classic Reborn");

			ActionHandler.click(SVOItemToapproveContainer.QuoteNextBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User clicks on next button");

		}

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeOppName, oppName + randomNumber);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Name");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeClosedDate, closeDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Closed date");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeStage);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.StageSelection(stage))));

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Stage for an Opportunity");

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Product Offering for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		ActionHandler.wait(2);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.RegionSelection(region))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Region for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeAccountName, accountName);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeAccountNameSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(accountName))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Account Name for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokePreferredContact, preferredContact);
		ActionHandler.click(SVOItemToapproveContainer.SVOBespokePreferredContactname);
		Reporter.addStepLog("User selects Preferred Contact for an Opportunity");

		VerifyHandler.verifyElementPresent(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		CommonFunctions.attachScreenshot();
		ActionHandler
				.click(driver.findElement(By.xpath(RestrictedPartyScreeningContainer.ValueSelection(Checkcleared))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects restricted party screening as Check cleared for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeBrand, brand);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeBrandSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(brand))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Brand for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeModel, model);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(model))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Model for an Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.BodyStyleTextBox);
		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.BodyStyleTextBox, model);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.TestDefenderBodystyle);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects body style for an Opportunity");

		ActionHandler.wait(2);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.BuildColour);
		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.BuildColour, "Blue");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters build colour on Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.HandOfDrivedropDown);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.HandOfDriveLHD);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Hand of drive on Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.Speedometerdropdown);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.SpeedometerMPH);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Speedometer on Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.BuildColourShade);
		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.BuildColourShade, "brown");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters build colour shade on Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.BuildInteriorColour);
		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.BuildInteriorColour, "White");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters build interior colour on Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.AdditionalSepcDetails);
		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.AdditionalSepcDetails, "Test");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Additional spec details on Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.ClassicServiceVehicleRecord);
		ActionHandler.wait(1);
		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.ClassicServiceVehicleRecord, model);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.ClassicServiceDefenderVehicle);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Vehicle record for an Opportunity");

		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.MainVehicleRetailPrice, "250");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enter retail price for main vehicle on Opportunity page");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.VATQualifyingDropdown);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.VATyesButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses VAT qualifying of an opportunity as yes");

		ActionHandler.wait(2);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRetailerContact);
		ActionHandler.wait(2);
		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeRetailerContact, retailerContact);
		ActionHandler.click(SVOItemToapproveContainer.SVOBespokePreferredContactname);
		Reporter.addStepLog("User selects Retailer Contact for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSource);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.SourceSelection(source))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Source for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSaveOpportunity);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves Classic Reborn Opportunity");

		OpportunityName = SVOAdditionalvehicleContainer.OpportunityName.getText();
		Reporter.addStepLog("Opportunity Name is = " + OpportunityName);
	}

	@Then("^User moves an opportunity to order placed stage$")
	public void User_moves_an_opportunity_to_order_placed_stage() throws Throwable {

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.OrderPlacedButton);
		ActionHandler.wait(3);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.MarkasCurrentStatusButton);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Opportunity is moved to order placed stage");

	}

	@Then("^User Navigates to work order of an created order$")
	public void User_Navigates_to_work_order_of_an_created_order() throws Throwable {

		driver.navigate().refresh();
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.OrdersLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Orders link");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstOrder);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens Order details for the opportunity");

		ActionHandler.wait(5);
		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);

		javaScriptUtil.clickElementByJS(SVO_WorkOrder_AdditionalOwner_Container.WorkOrdernumberOnOrdersPage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens work Order details for the opportunity");

		WorkOrderNumber = SVOWorkOrderContainer.NewlyCreatedWorkOrderNumber.getText();
		Reporter.addStepLog("Work Order Number Name is = " + WorkOrderNumber);

	}

	@And("^User create a Classic Works legend Opportunity with check cleared like Stage \"([^\"]*)\" Product Offering \"([^\"]*)\" Region \"([^\"]*)\" Client \"([^\"]*)\" Restricted Party Screening Stage \"([^\"]*)\" Preferred Contact \"([^\"]*)\" Account Name \"([^\"]*)\" Retailer Contact \"([^\"]*)\" Source \"([^\"]*)\" Origination Divission \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\"$")
	public void User_create_a_Classic_Works_legend_Opportunity_with_check_cleared(String stage, String productOffering,
			String region, String client, String Checkcleared, String preferredContact, String accountName,
			String retailerContact, String source, String originatingdivission, String brand, String model)
			throws Exception {
		String oppName = "Test_Classic_WorksLegend_Opportunity_";
		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		String closeDate = CommonFunctions.selectFutureDate();
		Reporter.addStepLog("Opportunity Close Date is = " + closeDate);

		String s[] = stage.split(",");
		stage = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String proOff[] = productOffering.split(",");
		productOffering = CommonFunctions.readExcelMasterData(proOff[0], proOff[1], proOff[2]);

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String c[] = client.split(",");
		client = CommonFunctions.readExcelMasterData(c[0], c[1], c[2]);

		String RPS[] = Checkcleared.split(",");
		Checkcleared = CommonFunctions.readExcelMasterData(RPS[0], RPS[1], RPS[2]);

		String preCon[] = preferredContact.split(",");
		preferredContact = CommonFunctions.readExcelMasterData(preCon[0], preCon[1], preCon[2]);

		String accName[] = accountName.split(",");
		accountName = CommonFunctions.readExcelMasterData(accName[0], accName[1], accName[2]);

		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		String so[] = source.split(",");
		source = CommonFunctions.readExcelMasterData(so[0], so[1], so[2]);

		String OD[] = originatingdivission.split(",");
		originatingdivission = CommonFunctions.readExcelMasterData(OD[0], OD[1], OD[2]);

		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String m[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		Reporter.addStepLog("User starts creating new SVO Bespoke Opportunity");
		ActionHandler.click(SVOAdditionalvehicleContainer.NewOpportunity);
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.QuoteNextBtn)) {

			Reporter.addStepLog("User selects opportunity record type as Classic Works legend");

			ActionHandler.click(SVOItemToapproveContainer.QuoteNextBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User clicks on next button");

		}

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeOppName, oppName + randomNumber);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Name");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeClosedDate, closeDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Closed date");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeStage);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.StageSelection(stage))));

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Stage for an Opportunity");

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Product Offering for an Opportunity");

		ActionHandler.wait(2);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.RegionSelection(region))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Region for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeAccountName, accountName);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeAccountNameSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(accountName))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Account Name for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokePreferredContact, preferredContact);
		ActionHandler.click(SVOItemToapproveContainer.SVOBespokePreferredContactname);
		Reporter.addStepLog("User selects Preferred Contact for an Opportunity");

		VerifyHandler.verifyElementPresent(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		CommonFunctions.attachScreenshot();
		ActionHandler
				.click(driver.findElement(By.xpath(RestrictedPartyScreeningContainer.ValueSelection(Checkcleared))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects restricted party screening as Check cleared for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeBrand, brand);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeBrandSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(brand))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Brand for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeModel, model);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(model))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Model for an Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.ClassicServiceVehicleRecord);
		ActionHandler.wait(1);
		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.ClassicServiceVehicleRecord, model);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.ClassicServiceDefenderVehicle);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Vehicle record for an Opportunity");

		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.MainVehicleRetailPrice, "250");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enter retail price for main vehicle on Opportunity page");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.VATQualifyingDropdown);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.VATyesButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses VAT qualifying of an opportunity as yes");

		ActionHandler.wait(2);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRetailerContact);
		ActionHandler.wait(2);
		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeRetailerContact, retailerContact);
		ActionHandler.click(SVOItemToapproveContainer.SVOBespokePreferredContactname);
		Reporter.addStepLog("User selects Retailer Contact for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSource);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.SourceSelection(source))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Source for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSaveOpportunity);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves Classic Reborn Opportunity");

		OpportunityName = SVOAdditionalvehicleContainer.OpportunityName.getText();
		Reporter.addStepLog("Opportunity Name is = " + OpportunityName);
	}

	@Then("^Add non classic user \"([^\"]*)\" as new Additional owner for the selected work order$")
	public void add_non_Classic_User_as_new_Additional_Owner_for_selected_work_order(String AdditionalOwner)
			throws Throwable {

		String AO[] = AdditionalOwner.split(",");
		AdditionalOwner = CommonFunctions.readExcelMasterData(AO[0], AO[1], AO[2]);

		ActionHandler.wait(5);
		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(3);

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.EditAdditionalOwnerFieldIcon);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Edit Additional Owner Icon");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.AdditionalOwnerFieldInputTextBox);
		ActionHandler.wait(2);
		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.AdditionalOwnerFieldInputTextBox,
				AdditionalOwner);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters non classic operations user name on Additional owner text box");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.BespokeUserasAdditionalOwner);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects non classic user as Additional owner");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.AdditionalOwnerFieldSaveButton);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on save button of Additional owner pop up window");

	}

	@Then("^User selects My Work orders from Select List view$")
	public void user_selects_My_Work_orders_from_Select_List_view() throws Throwable {

		ActionHandler.click(SVOWorkOrderContainer.RecentlyViewedLink);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on recently viewed link");

		ActionHandler.click(SVOWorkOrderContainer.MyWorkOrdersListView);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects My Work orders list from Select List view");
	}

}
