package com.jlr.svo.tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.containers.SSEC_1231_ClassicParts_DefineSLAContainer;
import com.jlr.svo.containers.SSEC_1238_CaseLifecycle_Container;
import com.jlr.svo.containers.SVOAccountsContainer;
import com.jlr.svo.containers.SVOAdditionalVehicleContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.containers.SVOItemToApproveContainer;
import com.jlr.svo.containers.SVOOpportunityContainer;
import com.jlr.svo.containers.SVO_CustomerResponsesContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.Then;

public class SSEC_1231_ClassicParts_DefineSLA_Test extends TestBaseCC {

	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory.getLogger(SSEC_1238_CaseLifecycle_Test.class.getName());
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);

	SVOAccountsContainer SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
	SVOItemToApproveContainer SVOItemToapproveContainer = PageFactory.initElements(driver,
			SVOItemToApproveContainer.class);
	SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);

	SVOAdditionalVehicleContainer SVOAdditionalvehicleContainer = PageFactory.initElements(driver,
			SVOAdditionalVehicleContainer.class);
	SSEC_1238_CaseLifecycle_Container CaseLifecycle_Container = PageFactory.initElements(driver,
			SSEC_1238_CaseLifecycle_Container.class);
	SVO_CustomerResponsesContainer SVO_CustomerResponsesContainer = PageFactory.initElements(driver,
			SVO_CustomerResponsesContainer.class);
	SSEC_1231_ClassicParts_DefineSLAContainer ClassicParts_DefineSLAContainer = PageFactory.initElements(driver,
			SSEC_1231_ClassicParts_DefineSLAContainer.class);

	public static String verificationCode;
	public static String vCode;
	public static String veriCode;
	public static String CaseNumber;

	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	public void onStart() {

		setupTest("SVOTest");
		driver = getDriver();
		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		CaseNumber = null;
	}

	@Then("^Verify that SLA is breached on salesforce$")
	public void verify_that_SLA_is_breached_on_salesforce() throws Throwable {
		ActionHandler.wait(8);
		driver.navigate().refresh();
		ActionHandler.wait(10);
		VerifyHandler.verifyElementPresent(ClassicParts_DefineSLAContainer.SLAStatus);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies that SLA Status After creting new case");
	}

	@Then("^Collect case number on Friday before five PM$")
	public void Collect_case_number_on_Friday_before_five_PM() throws Throwable {

		ActionHandler.wait(4);
		CommonFunctions.setCellData("Define_SLA", "A", "2", CaseNumber);
		ActionHandler.wait(2);
	}

	@Then("^Collect case number on Friday after five PM$")
	public void Collect_case_number_on_Friday_after_five_PM() throws Throwable {

		ActionHandler.wait(4);
		CommonFunctions.setCellData("Define_SLA", "A", "3", CaseNumber);
		ActionHandler.wait(2);
	}

	@Then("^Collect case number on Saturday anytime$")
	public void Collect_case_number_on_Saturday_anytime() throws Throwable {

		ActionHandler.wait(4);
		CommonFunctions.setCellData("Define_SLA", "A", "4", CaseNumber);
		ActionHandler.wait(2);
	}

	@Then("^Collect case number on Sunday anytime$")
	public void Collect_case_number_on_Sunday_anytime() throws Throwable {

		ActionHandler.wait(4);
		CommonFunctions.setCellData("Define_SLA", "A", "5", CaseNumber);
		ActionHandler.wait(2);
	}

	@Then("^Collect case number created via web on Friday before five PM$")
	public void Collect_case_number_created_via_web_on_Friday_before_five_PM() throws Throwable {

		ActionHandler.wait(4);
		CommonFunctions.setCellData("Define_SLA", "A", "9", CaseNumber);
		ActionHandler.wait(2);
	}

	@Then("^Collect case number created via web on Friday after five PM$")
	public void Collect_case_number_created_via_web_on_Friday_after_five_PM() throws Throwable {

		ActionHandler.wait(4);
		CommonFunctions.setCellData("Define_SLA", "A", "10", CaseNumber);
		ActionHandler.wait(2);
	}

	@Then("^Collect case number on Weekday before five PM$")
	public void Collect_case_number_on_Weekday_before_five_PM() throws Throwable {

		ActionHandler.wait(4);
		CommonFunctions.setCellData("Define_SLA", "A", "6", CaseNumber);
		ActionHandler.wait(2);
	}

	@Then("^Collect case number on Weekday after five PM$")
	public void Collect_case_number_on_Weekday_after_five_PM() throws Throwable {
		ActionHandler.wait(4);
		CommonFunctions.setCellData("Define_SLA", "A", "7", CaseNumber);
		ActionHandler.wait(2);
	}

	@Then("^Collect case number which is in closed status$")
	public void Collect_case_number_which_is_in_closed_status() throws Throwable {
		ActionHandler.wait(4);
		CommonFunctions.setCellData("Define_SLA", "A", "8", CaseNumber);
		ActionHandler.wait(2);
	}

	@Then("^User create new case to define SLA with record type \"([^\"]*)\"$")
	public void user_create_new_case_to_define_SLA_with_record_type(String recordtype) throws Throwable {

		String s[] = recordtype.split(",");
		recordtype = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(CaseLifecycle_Container.CasesTab);
		ActionHandler.wait(1);
		Reporter.addStepLog("User navigates to cases tab");

		ActionHandler.click(CaseLifecycle_Container.NewBtn);
		ActionHandler.wait(2);
		ActionHandler.click(driver.findElement(By.xpath(CaseLifecycle_Container.CaseRecordType(recordtype))));
		CommonFunctions.attachScreenshot();
		ActionHandler.click(CaseLifecycle_Container.NextBtn);
		Reporter.addStepLog("User chooses record type for an case");

		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(1);
		ActionHandler.click(CaseLifecycle_Container.ChassisNumber);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseLifecycle_Container.ChassisNumber, "123456");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters chassis number for case");

		ActionHandler.click(CaseLifecycle_Container.SaveBtn);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves case");

		ActionHandler.wait(8);
		driver.navigate().refresh();
		ActionHandler.wait(10);

		CaseNumber = CaseLifecycle_Container.CaseNumber.getText();
		Reporter.addStepLog("Case number is = " + CaseNumber);

	}

}
