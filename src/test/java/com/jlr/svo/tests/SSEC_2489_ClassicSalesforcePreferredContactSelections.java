package com.jlr.svo.tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.containers.SSEC_2489_ClassicSalesforcePreferredContactSelectionsContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.containers.SVOOpportunityContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class SSEC_2489_ClassicSalesforcePreferredContactSelections extends TestBaseCC {
	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory
			.getLogger(SSEC_1228_LinkContactAndAccountTocase_Test.class.getName());
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
	SSEC_2489_ClassicSalesforcePreferredContactSelectionsContainer ClassicSalesforcePreferredContactSelectionsContainer = PageFactory.initElements(driver,  SSEC_2489_ClassicSalesforcePreferredContactSelectionsContainer.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
	
	public static String verificationCode;
	public static String vCode;
	public static String veriCode;
	public String NewWindow;
	
	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	public void onStart() {

		setupTest("SVOTest");
		driver = getDriver();

		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		SSEC_2489_ClassicSalesforcePreferredContactSelectionsContainer ClassicSalesforcePreferredContactSelectionsContainer = PageFactory.initElements(driver,  SSEC_2489_ClassicSalesforcePreferredContactSelectionsContainer.class);
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
			
		driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);

		verificationCode = null;
		vCode = null;
		veriCode = null;
		NewWindow = null;
		
	}
	
	//click on edit button of Preferred Contact
    @Then("^Click on Edit button of Preferred Contact and select the option from drop down$")
    public void Click_on_EditButton_of_PreferredContact() throws Exception{
    	
    	ActionHandler.click(ClassicSalesforcePreferredContactSelectionsContainer.EditPreferredContact);
        ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Click on Edit Preferred Contact button");
        
    	ActionHandler.click(ClassicSalesforcePreferredContactSelectionsContainer.ClearSelectionBtn);
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Click on Clear Selection button");
        
    	ActionHandler.click(ClassicSalesforcePreferredContactSelectionsContainer.PreferredContactDropDown);
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Click on Preferred Contact drop down button");
        
    }
    
    @And("^Click on Edit button of Email and fill it$")
    public void Click_on_EditButton_of_Email() throws Exception{
    	
    	ActionHandler.click(ClassicSalesforcePreferredContactSelectionsContainer.EmailAdd);
    	ActionHandler.wait(2);
    	ActionHandler.setText(ClassicSalesforcePreferredContactSelectionsContainer.EmailAdd, "test@test.com");
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Click on Email Address field");
        
    	ActionHandler.click(ClassicSalesforcePreferredContactSelectionsContainer.SaveEditBtn);
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Click on SaveEdit button");
      
    }


    @And("^User adds the preferred contact details$")
    public void User_adds_the_Preferred_contact_details() throws Exception {

        String pContact="Shifa";

        ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        ActionHandler.setText(SVOEnquiryContainer.PreferredContact, pContact);
        ActionHandler.wait(2);
        VerifyHandler.verifyElementPresent(SVOEnquiryContainer.VerifyAccount);
        Reporter.addStepLog("User is able to verify the person Account in the preferred Contact drop down");
        CommonFunctions.attachScreenshot();
        Actions act = new Actions(driver);
        act.sendKeys(Keys.ARROW_DOWN).build().perform();
        ActionHandler.pressEnter();
        Reporter.addStepLog("User adds the preferred contact details");

    }
    
    @Then("^User mark Enquiry status as complete$")
    public void user_mark_Enquiry_status_as_complete() throws Throwable {

 

        ActionHandler.pageCompleteScrollUp();
        ActionHandler.wait(2);
        ActionHandler.click(SVO_OpportunityContainer.MarkEnquiryCompleted);
        ActionHandler.wait(5);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("User mark enquiry as completed");

 

    }



}
