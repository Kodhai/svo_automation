package com.jlr.svo.tests;

import java.io.IOException;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.autotest.utils.Config;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.constants.Constants;
import com.jlr.svo.containers.SVOAccountsContainer;
import com.jlr.svo.containers.SVOAdditionalVehicleContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.containers.SVOItemToApproveContainer;
import com.jlr.svo.containers.SVOOpportunityContainer;
import com.jlr.svo.containers.SVO_CustomerResponsesContainer;
import com.jlr.svo.containers.SVO_RestrictedPartyScreeningContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SVO_RestrictedPartyScreening_Test extends TestBaseCC {

	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory.getLogger(SVOWorkOrderProgrammeField_Test.class.getName());
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);

	SVOItemToApproveContainer SVOItemToapproveContainer = PageFactory.initElements(driver,
			SVOItemToApproveContainer.class);
	SVOAccountsContainer SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
	SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	SVOAdditionalVehicleContainer SVOAdditionalvehicleContainer = PageFactory.initElements(driver,
			SVOAdditionalVehicleContainer.class);
	SVO_RestrictedPartyScreeningContainer RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
			SVO_RestrictedPartyScreeningContainer.class);
	SVO_CustomerResponsesContainer SVO_CustomerResponsesContainer = PageFactory.initElements(driver,
			SVO_CustomerResponsesContainer.class);

	public static String verificationCode;
	public static String vCode;
	public static String veriCode;
	public static String OpportunityName;
	public static String DesignBriefRequestname;
	public static String PrivateOfficeQuoteName;
	public static String ErrorMessage;
	public static String RestrictedPartyScreeningStatus;

	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	public void onStart() {

		setupTest("SVOTest");
		driver = getDriver();

		SVOItemToapproveContainer = PageFactory.initElements(driver, SVOItemToApproveContainer.class);
		SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
		SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		SVOAdditionalvehicleContainer = PageFactory.initElements(driver, SVOAdditionalVehicleContainer.class);
		RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
				SVO_RestrictedPartyScreeningContainer.class);
		SVO_CustomerResponsesContainer = PageFactory.initElements(driver, SVO_CustomerResponsesContainer.class);

		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		verificationCode = null;
		vCode = null;
		OpportunityName = null;
		DesignBriefRequestname = null;
		PrivateOfficeQuoteName = null;
		ErrorMessage = null;
		RestrictedPartyScreeningStatus = null;

	}

	@Given("^Access to SVO Portal by super uat user with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void access_to_an_SVO_portal_by_super_uat_user_with_Username_and_Password(String userName, String password)
			throws Throwable {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		onStart();
		driver = getDriver();
		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailOnSuperUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();

			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				CommonFunctions.attachScreenshot();

			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				CommonFunctions.attachScreenshot();

			} else {

				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	public String checkEmailOnSuperUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(337, 343);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	@Given("^Access to SVO Portal by bespoke uat user with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void access_to_an_SVO_portal_by_bespoke_uat_user_with_Username_and_Password(String userName, String password)
			throws Throwable {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		onStart();
		driver = getDriver();
		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailOnBespokeUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();

			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				CommonFunctions.attachScreenshot();

			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				CommonFunctions.attachScreenshot();

			} else {

				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	public String checkEmailOnBespokeUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(334, 340);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	@Given("^Access to SVO admin Portal by Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void access_to_an_SVO_admin_Portal_by_Username_and_Password(String userName, String password)
			throws Throwable {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		onStart();
		driver = getDriver();
		driver.get(Constants.SVOURL);
		Reporter.addStepLog("User Logins to SVO Portal");

		ActionHandler.setText(SVO_OpportunityContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVO_OpportunityContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVO_OpportunityContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailAdmin();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVO_OpportunityContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVO_OpportunityContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVO_OpportunityContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVO_OpportunityContainer.verifyBtn);
			ActionHandler.waitForElement(SVO_OpportunityContainer.SVOText, 180);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.remindMeLater)) {

				ActionHandler.click(SVO_OpportunityContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.notRegister)) {

				ActionHandler.click(SVO_OpportunityContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	public String checkEmailAdmin() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();
			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();
			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(337, 343);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	@Given("^Access to SVO Portal by private office user with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void access_to_an_SVO_portal_by_private_office_user_with_Username_and_Password(String userName,
			String password) throws Throwable {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		onStart();
		driver = getDriver();
		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailOnPrivateOfficeUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				CommonFunctions.attachScreenshot();

			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				CommonFunctions.attachScreenshot();

			} else {

				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {

			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	public String checkEmailOnPrivateOfficeUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(341, 347);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	@Given("^Access to SVO Portal as Super user with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void access_to_an_SVO_portal_as_Super_user_with_Username_and_Password(String userName, String password)
			throws Throwable {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		onStart();
		driver = getDriver();
		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailForSuperUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				CommonFunctions.attachScreenshot();

			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				CommonFunctions.attachScreenshot();

			} else {

				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {

			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	public String checkEmailForSuperUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(337, 343);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	@Given("^Access to SVO Portal as Classic Service user with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void access_to_an_SVO_Portal_as_classic_service_user_with_Username_and_Password(String userName,
			String password) throws Throwable {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		onStart();
		driver = getDriver();
		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailForClassicServiceUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			Reporter.addStepLog("User logins to SVO Successfully");

		}
	}

	public String checkEmailForClassicServiceUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(342, 349);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	@And("^User create a SVO Bespoke Opportunity with all fields like Stage \"([^\"]*)\" Product Offering \"([^\"]*)\" Region \"([^\"]*)\" Client \"([^\"]*)\" Preferred Contact \"([^\"]*)\" Account Name \"([^\"]*)\" Retailer Contact \"([^\"]*)\" Source \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\"$")
	public void User_create_a_SVO_Bespoke_Opportunity_with_all_fields(String stage, String productOffering,
			String region, String client, String preferredContact, String accountName, String retailerContact,
			String source, String brand, String model) throws Exception {
		String oppName = "RestrictedPartyScreening_Opportunity_";
		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		String closeDate = CommonFunctions.selectFutureDate();
		Reporter.addStepLog("Opportunity Close Date is = " + closeDate);

		String s[] = stage.split(",");
		stage = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String proOff[] = productOffering.split(",");
		productOffering = CommonFunctions.readExcelMasterData(proOff[0], proOff[1], proOff[2]);

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String c[] = client.split(",");
		client = CommonFunctions.readExcelMasterData(c[0], c[1], c[2]);

		String preCon[] = preferredContact.split(",");
		preferredContact = CommonFunctions.readExcelMasterData(preCon[0], preCon[1], preCon[2]);

		String accName[] = accountName.split(",");
		accountName = CommonFunctions.readExcelMasterData(accName[0], accName[1], accName[2]);

		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		String so[] = source.split(",");
		source = CommonFunctions.readExcelMasterData(so[0], so[1], so[2]);

		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String m[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		Reporter.addStepLog("User starts creating new SVO Bespoke Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.NewOpportunity);
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.QuoteNextBtn)) {
			ActionHandler.click(SVOItemToapproveContainer.SVOBespokeOpportunity);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects opportunity record type as SVO bespoke");

			ActionHandler.click(SVOItemToapproveContainer.QuoteNextBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User clicks on next button");

		}

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeOppName, oppName + randomNumber);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Name");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeClosedDate, closeDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Closed date");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeStage);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.StageSelection(stage))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Stage for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeProductOffering);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(
				driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ProOfferingSelection(productOffering))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Product Offering for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		ActionHandler.wait(2);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.RegionSelection(region))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Region for an Opportunity");

		VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.SVOBespokeClient);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeClient, client);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeClientNameSearch);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(client))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Client for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokePreferredContact, preferredContact);
		ActionHandler.click(SVOItemToapproveContainer.SVOBespokePreferredContactname);
		Reporter.addStepLog("User selects Preferred Contact for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeAccountName, accountName);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeAccountNameSearch);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(accountName))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Account Name for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeRetailerContact, retailerContact);
		ActionHandler.click(SVOItemToapproveContainer.SVOBespokePreferredContactname);
		Reporter.addStepLog("User selects Retailer Contact for an Opportunity");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSource);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.SourceSelection(source))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Source for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeBrand, brand);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeBrandSearch);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(brand))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Brand for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeModel, model);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(model))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Model for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSaveOpportunity);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves SVO Bespoke Opportunity");

		OpportunityName = SVOAdditionalvehicleContainer.OpportunityName.getText();
		Reporter.addStepLog("Opportunity Name is = " + OpportunityName);
	}

	@Then("^User create a Design Brief Quote with all fields like Retailer Contact \"([^\"]*)\"$")
	public void User_create_a_Design_Brief_Quote_with_all_field(String retailerContact) throws Throwable {
		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		String quoteName = "SVO Bespoke Test Quote";
		String expirtaionDate = CommonFunctions.selectFutureDate();

		ActionHandler.wait(2);
		ActionHandler.pageDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuotesLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Quotes page");

		ActionHandler.wait(1);
		ActionHandler.click(SVOAdditionalvehicleContainer.NewQuoteBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on New button to create new Quote");

		ActionHandler.wait(1);
		if (VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.QuoteNextBtn)) {
			ActionHandler.click(SVOItemToapproveContainer.QuoteNextBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User clicks on Next button to create new Quote");
		}

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteNameBox, quoteName + randomNumber);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User writes quote name for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRetailerNameBox, retailerContact);
		ActionHandler.wait(3);
		ActionHandler.click(SVOItemToapproveContainer.QuoteRetailerContactSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		ActionHandler.click(SVOItemToapproveContainer.QuoteRetailerContacts);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters retailer contact details for new quote");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteCostPrice, "123");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Cost Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteListPrice, "203");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters List Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteContribution, "23");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Contribution for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRetailPrice, "213");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Retail Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRevenue, "113");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Revenue for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteExpirationDate, expirtaionDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Expiration Date for new quote");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteSaveBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User save new quote");

		ActionHandler.wait(3);
		DesignBriefRequestname = SVOAdditionalvehicleContainer.FirstQuote.getText();
		Reporter.addStepLog("Opportunity Name is = " + DesignBriefRequestname);
		ActionHandler.wait(3);

		user_navigate_back_to_Opportunity_page();
		Reporter.addStepLog("User navigate back to the Opportunity");
	}

	@Then("^Logout from the Special vehicle operations Portal$")
	public void logout_from_the_Special_vehicle_operations_Portal() throws IOException {

		ActionHandler.click(SVOEnquiryContainer.icon_image);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Logout");

		ActionHandler.click(SVOEnquiryContainer.Logout);
		CommonFunctions.attachScreenshot();
		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.userNameTextBox)) {
			MarkupHelper.createLabel("Logged out successfully", ExtentColor.GREEN);
		} else {
			MarkupHelper.createLabel("Log out failed", ExtentColor.RED);
		}
	}

	@Then("^Verify that user is not able to view the Customer unknown/check not applicable field from Restricted Party screening$")
	public void Verify_that_user_is_not_able_to_view_the_Customer_unknown_check_not_applicable_field_from_Restricted_Party_screening()
			throws IOException {

		ActionHandler.wait(3);
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.wait(2);

		javaScriptUtil.clickElementByJS(RestrictedPartyScreeningContainer.RestrictedPartyScreeningEditBtn);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		VerifyHandler.verifyElementPresent(RestrictedPartyScreeningContainer.CustomerUnknowChecknotApplicable);

		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User is not able to view Customer unknown/check not applicable field ");

		ActionHandler.click(RestrictedPartyScreeningContainer.RestrictedPartyScreeningSaveBtn);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on save button");

		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(2);

	}

	@When("^User create a SVO Private Office Opportunity with all fields like Stage \"([^\"]*)\" Product Offering \"([^\"]*)\" Region \"([^\"]*)\" Preferred Contact \"([^\"]*)\" Account Name \"([^\"]*)\" Retailer Contact \"([^\"]*)\" Source \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\"$")
	public void user_create_a_SVO_Private_Office_Opportunity_with_all_fields(String stage, String productOffering,
			String region, String preferredContact, String accountName, String retailerContact, String source,
			String brand, String model) throws Exception {
		String oppName = "RestrictedPartyScreening_Opportunity_";
		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		String closeDate = CommonFunctions.selectFutureDate();
		Reporter.addStepLog("Opportunity Close Date is = " + closeDate);

		String s[] = stage.split(",");
		stage = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String proOff[] = productOffering.split(",");
		productOffering = CommonFunctions.readExcelMasterData(proOff[0], proOff[1], proOff[2]);

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String preCon[] = preferredContact.split(",");
		preferredContact = CommonFunctions.readExcelMasterData(preCon[0], preCon[1], preCon[2]);

		String accName[] = accountName.split(",");
		accountName = CommonFunctions.readExcelMasterData(accName[0], accName[1], accName[2]);

		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		String so[] = source.split(",");
		source = CommonFunctions.readExcelMasterData(so[0], so[1], so[2]);

		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String m[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		Reporter.addStepLog("User starts creating new SVO Private Office Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.NewOpportunity);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on New button");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeOppName, oppName + randomNumber);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Name");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeClosedDate, closeDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Closed date");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeStage);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.StageSelection(stage))));
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Stage for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeProductOffering);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(
				driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ProOfferingSelection(productOffering))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Product Offering for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.RegionSelection(region))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Region for an Opportunity");

		VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.SVOBespokePreferredContact);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokePreferredContact, preferredContact);
		ActionHandler.click(SVOItemToapproveContainer.SVOBespokePreferredContactname);
		Reporter.addStepLog("User selects Preferred Contact for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeAccountName, accountName);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeAccountNameSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(accountName))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Account Name for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeRetailerContact, retailerContact);
		ActionHandler.click(SVOItemToapproveContainer.SVOBespokePreferredContactname);
		Reporter.addStepLog("User selects Retailer Contact for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSource);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.SourceSelection(source))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Source for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeBrand, brand);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeBrandSearch);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(brand))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Brand for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeModel, model);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(model))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Model for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSaveOpportunity);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves SVO Bespoke Opportunity");

		OpportunityName = SVOAdditionalvehicleContainer.OpportunityName.getText();
		Reporter.addStepLog("Opportunity Name is = " + OpportunityName);
	}

	@And("^User moves an Oppotunity to Order Placed stage after accepting Quote for Retailer Contact \"([^\"]*)\"$")
	public void User_moves_an_opportunity_to_Order_Placed_after_accepting_quote(String retailerContact)
			throws Exception {
		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.pageDown();
		ActionHandler.wait(3);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuotesLink);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Quotes page");

		ActionHandler.wait(1);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstQuoteFromOpportunity);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens created quote details");

		ActionHandler.wait(2);
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Quote status moves to Price stage");

		ActionHandler.wait(1);
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Quote status moves to Proposal Sent stage");

		ActionHandler.wait(1);
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(1);
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDrpDwn);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeAccepted);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User mark accepeted as Quote Outcome");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDetailDrpDwn);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDetailAccepted);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDoneBtn);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User accepeted the Quote");

		ActionHandler.wait(2);
		driver.navigate().refresh();
		ActionHandler.wait(10);

		ActionHandler.wait(1);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(1);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstOpportunityLink);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate to Opportunity");

		ActionHandler.wait(2);
		driver.navigate().refresh();
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.OrdersLink);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Orders link");

		ActionHandler.wait(1);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstOrder);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens Order details for the opportunity");

		ActionHandler.wait(1);
		// ActionHandler.pageDown();
		ActionHandler.wait(2);
		ActionHandler.click(SVOAdditionalvehicleContainer.CommonOrderNumber);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		ActionHandler.setText(SVOAdditionalvehicleContainer.CommonOrderNumberBox, "123456");
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(1);
		ActionHandler.click(SVOAdditionalvehicleContainer.SaveCommonOrderNumber);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves common order number");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Move the order to committed to build stage");

		driver.navigate().refresh();
		ActionHandler.wait(20);

		ActionHandler.click(SVOAdditionalvehicleContainer.CloseOrderTab);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User closed Orders Tab");

		ActionHandler.wait(1);
		javaScriptUtil.clickElementByJS(SVO_OpportunityContainer.OpportTab);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOAdditionalvehicleContainer.OpportunitySearchBox, OpportunityName);
		ActionHandler.wait(2);
		ActionHandler.pressEnter();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstOpportunityLink);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User moves the Opportunity to Order Placed from Contract Signed");
	}

	@And("^User moves the design brief quote stage to proposal sent stage$")
	public void User_moves_the_design_brief_quote_stage_to_proposal_sent_stage() throws Exception {

		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.pageDown();
		ActionHandler.wait(3);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuotesLink);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Quotes page");

		ActionHandler.wait(1);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstQuoteFromOpportunity);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens created quote details");

		ActionHandler.wait(2);
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Quote status moves to Price stage");

		ActionHandler.wait(1);
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Quote status moves to Proposal Sent stage");

	}

	@Then("^User navigates to the Opportunity tab$")
	public void user_navigates_to_the_Opportunity_tab() throws Throwable {

		javaScriptUtil.clickElementByJS(SVO_OpportunityContainer.OpportTab);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigate to Opportunity");

	}

	@And("^User create a Classic Service Opportunity with all fields like Stage \"([^\"]*)\" Product Offering \"([^\"]*)\" Region \"([^\"]*)\" Client \"([^\"]*)\" Preferred Contact \"([^\"]*)\" Account Name \"([^\"]*)\" Retailer Contact \"([^\"]*)\" Source \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\"$")
	public void User_create_a_Classic_Service_Opportunity_with_all_fields(String stage, String productOffering,
			String region, String client, String preferredContact, String accountName, String retailerContact,
			String source, String brand, String model) throws Exception {
		String oppName = "Test_SVO_ClassicService_Opportunity_";
		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		String closeDate = CommonFunctions.selectFutureDate();
		Reporter.addStepLog("Opportunity Close Date is = " + closeDate);

		String s[] = stage.split(",");
		stage = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String proOff[] = productOffering.split(",");
		productOffering = CommonFunctions.readExcelMasterData(proOff[0], proOff[1], proOff[2]);

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String c[] = client.split(",");
		client = CommonFunctions.readExcelMasterData(c[0], c[1], c[2]);

		String preCon[] = preferredContact.split(",");
		preferredContact = CommonFunctions.readExcelMasterData(preCon[0], preCon[1], preCon[2]);

		String accName[] = accountName.split(",");
		accountName = CommonFunctions.readExcelMasterData(accName[0], accName[1], accName[2]);

		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		String so[] = source.split(",");
		source = CommonFunctions.readExcelMasterData(so[0], so[1], so[2]);

		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String m[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		Reporter.addStepLog("User starts creating new SVO Bespoke Opportunity");
		ActionHandler.click(SVOAdditionalvehicleContainer.NewOpportunity);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on New button");

		ActionHandler.click(SVOItemToapproveContainer.QuoteNextBtn);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeOppName, oppName + randomNumber);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Name");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeClosedDate, closeDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Closed date");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeStage);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.StageSelection(stage))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Stage for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeProductOffering);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(
				driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ProOfferingSelection(productOffering))));

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Product Offering for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		ActionHandler.wait(2);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.RegionSelection(region))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Region for an Opportunity");

		VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.SVOClassicClient);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOClassicClient, client);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOClassicClientNameSearch);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOClassicPreferredContact, preferredContact);
		ActionHandler.click(SVOItemToapproveContainer.SVOBespokePreferredContactname);
		Reporter.addStepLog("User selects Preferred Contact for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeBrand, brand);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeBrandSearch);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(brand))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Brand for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeModel, model);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(model))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Model for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSaveOpportunity);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves SVO Bespoke Opportunity");

		OpportunityName = SVOAdditionalvehicleContainer.OpportunityName.getText();
		Reporter.addStepLog("Opportunity Name is = " + OpportunityName);
	}

	@Then("^User moves an opportunity to Design Brief and Quote stage$")
	public void User_moves_an_Opportunity_to_Design_brief_and_Quote_Stage() throws Exception {

		VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.MarkStageAsComplete);
		ActionHandler.wait(5);

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.MarkStageAsComplete);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User moves the Opportunity to Commission from Qualified Stage");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.MarkStageAsComplete);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User tries to moves the Opportunity to Design Brief & Quote from Commission Stage");

	}

	@Then("^User tries to move an opportunity stage to Design Brief and Quote stage without clearing Restricted party screening$")
	public void User_tries_to_move_an_Opportunity_to_Design_Brief_and_Quote_Stage() throws Exception {

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.MarkStageAsComplete);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User moves the Opportunity to Commission from Qualified Stage");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.MarkStageAsComplete);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User tries to move the Opportunity to Design Brief & Quote from Commission Stage");
	}

	@And("^Verify that user is not able to move the opportunity stage to Design Brief and Quote stage$")
	public void Verify_that_user_is_not_able_to_move_the_opportunity_stage_to_Design_Brief_and_Quote_stage()
			throws Exception {

		VerifyHandler.verifyElementPresent(RestrictedPartyScreeningContainer.RestrictedPartyScreeningErrorMsg);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"An error message displayed to clear the restricted party screening before moving to Design Brief and Quote stage");
	}

	@Then("^User moves an opportunity to Contract Signed stage from Design Brief and Quote$")
	public void User_moves_an_Opportunity_to_Contract_signed_Quote_Stage_from_design_brief_and_quote()
			throws Exception {

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.MarkStageAsComplete);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User moves the Opportunity to Contract signed Stage");
	}

	@Then("^User tries to start contractiong without clearing Restricted party screening$")
	public void User_tries_to_start_Contracting_without_clearing_restricted_party_screening() throws Exception {

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.MarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User moves the Opportunity to Experiance from Qualified Stage");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.MarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User moves the Opportunity to Specification & Quotation from Experiance Stage");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.MarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User tries to move the Opportunity to Verbal commitment Stage");
	}

	@Then("^Verify that user is not able to move the opportunity stage to Contract signed$")
	public void Verify_that_user_is_not_able_to_move_the_opportunit_stage_to_Contract_signed() throws Exception {

		if (VerifyHandler.verifyElementPresent(RestrictedPartyScreeningContainer.RestrictedPartyScreeningErrorMsg)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog(
					"An error message is caught that inhibits the opportunity to move to contract signed stage");
		} else {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User is able to move to contract signed stage");
		}
	}

	@Then("^Verify that user is not able to start contracting of an opportunity$")
	public void Verify_that_user_is_not_able_to_start_contracting_of_an_opportunity() throws Exception {

		if (VerifyHandler.verifyElementPresent(RestrictedPartyScreeningContainer.RestrictedPartyScreeningErrorMsg)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("An error message is caught that inhibits the opportunity to start contracting");
		} else {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User is able to move to verbal commitment stage");
		}
	}

	@And("^User create a SVO Bespoke Opportunity with check cleared like Stage \"([^\"]*)\" Product Offering \"([^\"]*)\" Region \"([^\"]*)\" Client \"([^\"]*)\" Preferred Contact \"([^\"]*)\" Account Name \"([^\"]*)\" Retailer Contact \"([^\"]*)\" Source \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\"$")
	public void User_create_a_SVO_Bespoke_Opportunity_with_check_cleared(String stage, String productOffering,
			String region, String client, String preferredContact, String accountName, String retailerContact,
			String source, String brand, String model) throws Exception {
		String oppName = "Test_SVO_Bespoke_Opportunity_";
		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		String closeDate = CommonFunctions.selectFutureDate();
		Reporter.addStepLog("Opportunity Close Date is = " + closeDate);

		String s[] = stage.split(",");
		stage = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String proOff[] = productOffering.split(",");
		productOffering = CommonFunctions.readExcelMasterData(proOff[0], proOff[1], proOff[2]);

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String c[] = client.split(",");
		client = CommonFunctions.readExcelMasterData(c[0], c[1], c[2]);

		String preCon[] = preferredContact.split(",");
		preferredContact = CommonFunctions.readExcelMasterData(preCon[0], preCon[1], preCon[2]);

		String accName[] = accountName.split(",");
		accountName = CommonFunctions.readExcelMasterData(accName[0], accName[1], accName[2]);

		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		String so[] = source.split(",");
		source = CommonFunctions.readExcelMasterData(so[0], so[1], so[2]);

		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String m[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		Reporter.addStepLog("User starts creating new SVO Bespoke Opportunity");
		ActionHandler.click(SVOAdditionalvehicleContainer.NewOpportunity);
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.QuoteNextBtn)) {
			ActionHandler.click(SVOItemToapproveContainer.QuoteNextBtn);
		}

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeOppName, oppName + randomNumber);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Name");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeClosedDate, closeDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Closed date");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeStage);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.StageSelection(stage))));

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Stage for an Opportunity");
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeProductOffering);

		CommonFunctions.attachScreenshot();
		ActionHandler.click(
				driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ProOfferingSelection(productOffering))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Product Offering for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.RegionSelection(region))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Region for an Opportunity");

		VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.SVOBespokeClient);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeClient, client);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeClientNameSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(client))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Client for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokePreferredContact, preferredContact);
		ActionHandler.click(SVOItemToapproveContainer.SVOBespokePreferredContactname);
		Reporter.addStepLog("User selects Preferred Contact for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeAccountName, accountName);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeAccountNameSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(accountName))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Account Name for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeRetailerContact, retailerContact);
		ActionHandler.click(SVOItemToapproveContainer.SVOBespokePreferredContactname);
		Reporter.addStepLog("User selects Retailer Contact for an Opportunity");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSource);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.SourceSelection(source))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Source for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeBrand, brand);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeBrandSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(brand))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Brand for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeModel, model);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(model))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Model for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSaveOpportunity);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves SVO Bespoke Opportunity");

		OpportunityName = SVOAdditionalvehicleContainer.OpportunityName.getText();
		Reporter.addStepLog("Opportunity Name is = " + OpportunityName);
	}

	@Then("^User close the opportunity after creating a private office Quote with all fields like Retailer Contact \"([^\"]*)\"$")
	public void User_close_the_opportunity_after_creating_a_private_office_Quote_with_all_field(String retailerContact)
			throws Exception {

		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		String quoteName = "private office Test Quote";
		String expirtaionDate = CommonFunctions.selectFutureDate();

		ActionHandler.pageDown();

		CommonFunctions.attachScreenshot();
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuotesLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Quotes page");

		ActionHandler.click(SVOAdditionalvehicleContainer.NewQuoteBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on New button to create new Quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteNameBox, quoteName + randomNumber);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User writes quote name for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRetailerNameBox, retailerContact);
		ActionHandler.click(RestrictedPartyScreeningContainer.RetailerContactSearch);
		ActionHandler.click(driver
				.findElement(By.xpath(RestrictedPartyScreeningContainer.RetailserContactSelection(retailerContact))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters retailer contact details for new quote");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteCostPrice, "123");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Cost Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteListPrice, "203");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters List Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteContribution, "23");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Contribution for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRetailPrice, "213");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Retail Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRevenue, "113");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Revenue for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteExpirationDate, expirtaionDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Expiration Date for new quote");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteSaveBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User save new quote");

		ActionHandler.wait(3);
		PrivateOfficeQuoteName = RestrictedPartyScreeningContainer.FirstPrivateOfficeQuote.getText();
		Reporter.addStepLog("Opportunity Name is = " + PrivateOfficeQuoteName);

		javaScriptUtil.clickElementByJS(RestrictedPartyScreeningContainer.FirstPrivateOfficeQuote);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(4);
		Reporter.addStepLog("User opens created quote details");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(4);
		Reporter.addStepLog("Quote status moves to Price stage");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(4);
		Reporter.addStepLog("Quote status moves to Proposal Sent stage");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(4);

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDrpDwn);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeAccepted);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User mark accepeted as Quote Outcome");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDetailDrpDwn);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDetailAccepted);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDoneBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User accepeted the Quote");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuoteOpportunityLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate to Opportunity");

		driver.navigate().refresh();
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.OrdersLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Orders link");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstOrder);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens Order details for the opportunity");

		ActionHandler.click(RestrictedPartyScreeningContainer.CommonOrderNumber1);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(SVOAdditionalvehicleContainer.CommonOrderNumberBox, "123456");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(RestrictedPartyScreeningContainer.SaveCommonOrderNumber1);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User saves common order number");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(10);
		Reporter.addStepLog("User moves Order to committed to build stage");

		ActionHandler.click(SVOAdditionalvehicleContainer.CloseOrderTab);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User closed Orders Tab");

		javaScriptUtil.clickElementByJS(SVO_OpportunityContainer.OpportTab);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOAdditionalvehicleContainer.OpportunitySearchBox, OpportunityName);
		ActionHandler.wait(2);
		Actions act6 = new Actions(driver);
		act6.sendKeys(Keys.ENTER).build().perform();
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstOpportunityLink);
		CommonFunctions.attachScreenshot();

		VerifyHandler.verifyElementPresent(RestrictedPartyScreeningContainer.VerifyClosedoneStage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Opportunity is in Closed Won stage");
	}

	@And("^User create a SVO Bespoke Opportunity with restricted party screening as restricted like Stage \"([^\"]*)\" Product Offering \"([^\"]*)\" Region \"([^\"]*)\" Client \"([^\"]*)\" Preferred Contact \"([^\"]*)\" Account Name \"([^\"]*)\" Retailer Contact \"([^\"]*)\" Source \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\"$")
	public void User_create_a_SVO_Bespoke_Opportunity_with_restricted_party_screening_as_restricted(String stage,
			String productOffering, String region, String client, String preferredContact, String accountName,
			String retailerContact, String source, String brand, String model) throws Exception {
		String oppName = "Test_SVO_Bespoke_Opportunity_";
		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		String closeDate = CommonFunctions.selectFutureDate();
		Reporter.addStepLog("Opportunity Close Date is = " + closeDate);

		String s[] = stage.split(",");
		stage = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String proOff[] = productOffering.split(",");
		productOffering = CommonFunctions.readExcelMasterData(proOff[0], proOff[1], proOff[2]);

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String c[] = client.split(",");
		client = CommonFunctions.readExcelMasterData(c[0], c[1], c[2]);

		String preCon[] = preferredContact.split(",");
		preferredContact = CommonFunctions.readExcelMasterData(preCon[0], preCon[1], preCon[2]);

		String accName[] = accountName.split(",");
		accountName = CommonFunctions.readExcelMasterData(accName[0], accName[1], accName[2]);

		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		String so[] = source.split(",");
		source = CommonFunctions.readExcelMasterData(so[0], so[1], so[2]);

		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String m[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		Reporter.addStepLog("User starts creating new SVO Bespoke Opportunity");
		ActionHandler.click(SVOAdditionalvehicleContainer.NewOpportunity);
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.QuoteNextBtn)) {
			ActionHandler.click(SVOItemToapproveContainer.QuoteNextBtn);
		}

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeOppName, oppName + randomNumber);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Name");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeClosedDate, closeDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Closed date");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeStage);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.StageSelection(stage))));

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Stage for an Opportunity");
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeProductOffering);

		CommonFunctions.attachScreenshot();
		ActionHandler.click(
				driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ProOfferingSelection(productOffering))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Product Offering for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.RegionSelection(region))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Region for an Opportunity");

		VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.SVOBespokeClient);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeClient, client);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeClientNameSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(client))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Client for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokePreferredContact, preferredContact);
		ActionHandler.click(SVOItemToapproveContainer.SVOBespokePreferredContactname);
		Reporter.addStepLog("User selects Preferred Contact for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeAccountName, accountName);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeAccountNameSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(accountName))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Account Name for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeRetailerContact, retailerContact);
		ActionHandler.click(SVOItemToapproveContainer.SVOBespokePreferredContactname);
		Reporter.addStepLog("User selects Retailer Contact for an Opportunity");

		ActionHandler.pageScrollDown();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSource);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.SourceSelection(source))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Source for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeBrand, brand);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeBrandSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(brand))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Brand for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeModel, model);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(model))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Model for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSaveOpportunity);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves SVO Bespoke Opportunity");

		OpportunityName = SVOAdditionalvehicleContainer.OpportunityName.getText();
		Reporter.addStepLog("Opportunity Name is = " + OpportunityName);
	}

	@Then("^User is able to view and select the Restricted Party Screening under details section of an private office opportunity with Restricted Party Screening stage \"([^\"]*)\"$")
	public void user_is_able_to_view_and_select_the_Restricted_Party_Screening_under_details_section_of_an_private_office_opportunity_with(
			String Checkcleared) throws Throwable {

		String CC[] = Checkcleared.split(",");
		Checkcleared = CommonFunctions.readExcelMasterData(CC[0], CC[1], CC[2]);

		ActionHandler.pageDown();
		ActionHandler.wait(2);

		javaScriptUtil.clickElementByJS(RestrictedPartyScreeningContainer.RestrictedPartyScreeningEditBtn);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		ActionHandler.click(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		CommonFunctions.attachScreenshot();

		ActionHandler
				.click(driver.findElement(By.xpath(RestrictedPartyScreeningContainer.ValueSelection(Checkcleared))));
		CommonFunctions.attachScreenshot();
		ActionHandler.click(RestrictedPartyScreeningContainer.SaveOpportunityDetailsBtn);
		CommonFunctions.attachScreenshot();
		ActionHandler.pageTop();
		ActionHandler.wait(3);
		Reporter.addStepLog("User selects restricted party screening as Check cleared for an Opportunity");

	}

	@Then("^Verify the error message while moving opportunity to Design Breief and Quote stage$")
	public void verify_the_error_message_while_moving_opportunity_to_Design_Bried_and_Quote_stage() throws Throwable {

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.MarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		VerifyHandler.verifyElementPresent(RestrictedPartyScreeningContainer.VerifyRestrictedErrormessage);
		CommonFunctions.attachScreenshot();

		ErrorMessage = RestrictedPartyScreeningContainer.VerifyRestrictedErrormessage.getText();
		Reporter.addStepLog("Error mmessage is = " + ErrorMessage);
		Reporter.addStepLog(
				"User verifies the error message stating 'You cannot start contracting without Restricted Party Screening being Cleared'");
	}

	@Then("^User moves an opportunity to Specification & quote stage$")
	public void user_moves_an_opportunity_to_Specification_quote_stage() throws Throwable {

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.MarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User moves the Opportunity to Experience from Qualified Stage");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.MarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User moves the Opportunity to Specification and Quotation from Experience Stage");
	}

	@Then("^User tries to start contractiong without clearing the Restricted party screening$")
	public void User_tries_to_start_contractiong_without_clearing_the_Restricted_party_screening() throws Exception {

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.MarkStageAsComplete);
		ActionHandler.wait(6);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User moves the Opportunity to Experience from Qualified Stage");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.MarkStageAsComplete);
		ActionHandler.wait(6);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User moves the Opportunity to Specification & Quotation from Experience Stage");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.MarkStageAsComplete);
		ActionHandler.wait(6);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"User tries to move Opportunity to Verbal commitment from Specification and quotation Stage");

	}

	@And("^Verify that user is not able to start contracting an opportunity$")
	public void Verify_that_user_is_not_able_to_start_contracting_an_opportunity() throws Exception {

		VerifyHandler.verifyElementPresent(RestrictedPartyScreeningContainer.RestrictedPartyScreeningErrorMsg);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"An error message displayed to clear the restricted party screening before moving to Verbal Commitment stage");
	}

	@Then("^User selects the Restricted party screening as \"([^\"]*)\"$")
	public void User_selects_the_Restricted_party_screening_as(String RestrictedPartyScreening) throws Exception {

		String RPS[] = RestrictedPartyScreening.split(",");
		RestrictedPartyScreening = CommonFunctions.readExcelMasterData(RPS[0], RPS[1], RPS[2]);

		ActionHandler.wait(3);
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.wait(2);

		javaScriptUtil.clickElementByJS(RestrictedPartyScreeningContainer.RestrictedPartyScreeningEditBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver
				.findElement(By.xpath(RestrictedPartyScreeningContainer.ValueSelection(RestrictedPartyScreening))));
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects restricted party screening as " + RestrictedPartyScreening);

		ActionHandler.click(RestrictedPartyScreeningContainer.RestrictedPartyScreeningSaveBtn);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on save button");

		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(2);

	}

	@Then("^User selects the Originating division as \"([^\"]*)\"$")
	public void User_selects_the_Originating_division_as(String OriginatingDivision) throws Exception {

		String OD[] = OriginatingDivision.split(",");
		OriginatingDivision = CommonFunctions.readExcelMasterData(OD[0], OD[1], OD[2]);

		driver.navigate().refresh();
		ActionHandler.wait(20);
		for (int i = 0; i < 6; i++) {
			ActionHandler.scrollDown();
		}

		ActionHandler.wait(2);
		ActionHandler.click(RestrictedPartyScreeningContainer.OriginationDivisionEditBtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(RestrictedPartyScreeningContainer.OriginationDivisionDropDownList);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(
				driver.findElement(By.xpath(RestrictedPartyScreeningContainer.ValueSelection(OriginatingDivision))));
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Originating division as " + OriginatingDivision);

		ActionHandler.click(RestrictedPartyScreeningContainer.RestrictedPartyScreeningSaveBtn);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on save button");

		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(2);

	}

	@Then("^User submit Design Brief Request and return the render pack$")
	public void user_submit_Design_Brief_Request_and_return_the_render_pack() throws Throwable {

		ActionHandler.wait(2);
		driver.navigate().refresh();
		ActionHandler.wait(10);
		ActionHandler.scrollDown();
		ActionHandler.wait(2);

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuotesLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Quotes Link");

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstQuote);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects new quote");

		ActionHandler.scrollDown();
		ActionHandler.scrollDown();

		ActionHandler.wait(3);
		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Design brief");
		ActionHandler.wait(3);
		ActionHandler.click(SVOAdditionalvehicleContainer.FirstDesignBrief);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Design brief name");

		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefSubmit);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User submit Design brief successfully");

		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefSaveSubmission);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks save button");

		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefReturnRenderPack);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks return render pack button");

		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefSaveRenderPack);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks Save render pack button");

	}

	@Then("^User navigate back to Opportunity page$")
	public void user_navigate_back_to_Opportunity_page() throws Throwable {
		ActionHandler.wait(5);
		javaScriptUtil.clickElementByJS(SVO_OpportunityContainer.OpportTab);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigate to Opportunity");

		ActionHandler.click(SVOItemToapproveContainer.SelectOpportunityListView);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user searches for opportunities list to display");

		ActionHandler.click(SVOItemToapproveContainer.AllBespokeOpportunitiestab);

		Reporter.addStepLog("User chooses to view selected type opportunities");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOItemToapproveContainer.OpportunitysearchBox);
		ActionHandler.wait(2);
		ActionHandler.setText(SVOItemToapproveContainer.OpportunitysearchBox, OpportunityName);
		ActionHandler.wait(3);
		ActionHandler.pressEnter();
		ActionHandler.wait(6);

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstOpportunityLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate back to the opportunity page");

	}

	@Then("^User moves an opportunity to Contract Signed stage from Design Brief and Quote stage$")
	public void User_moves_an_Opportunity_to_Contract_signed_Quote_stage_from_design_brief_and_quote_stage()
			throws Exception {
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.MarkStageAsComplete);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User moves the Opportunity to  Design Brief and Quote Stage");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.MarkStageAsComplete);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User moves the Opportunity to  Contract signed Stage");
	}

	@And("^User create a SVO Bespoke Opportunity with check cleared like Stage \"([^\"]*)\" Product Offering \"([^\"]*)\" Region \"([^\"]*)\" Client \"([^\"]*)\" Restricted Party Screening Stage \"([^\"]*)\" Preferred Contact \"([^\"]*)\" Account Name \"([^\"]*)\" Retailer Contact \"([^\"]*)\" Source \"([^\"]*)\" Origination Divission \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\"$")
	public void User_create_a_SVO_Bespoke_Opportunity_with_check_cleared(String stage, String productOffering,
			String region, String client, String Checkcleared, String preferredContact, String accountName,
			String retailerContact, String source, String originatingdivission, String brand, String model)
			throws Exception {
		
		OpportunityName = null;
		String oppName = "Test_SVO_Bespoke_Opportunity_";
		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		String closeDate = CommonFunctions.selectFutureDate();
		Reporter.addStepLog("Opportunity Close Date is = " + closeDate);

		String s[] = stage.split(",");
		stage = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String proOff[] = productOffering.split(",");
		productOffering = CommonFunctions.readExcelMasterData(proOff[0], proOff[1], proOff[2]);

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String c[] = client.split(",");
		client = CommonFunctions.readExcelMasterData(c[0], c[1], c[2]);

		String RPS[] = Checkcleared.split(",");
		Checkcleared = CommonFunctions.readExcelMasterData(RPS[0], RPS[1], RPS[2]);

		String preCon[] = preferredContact.split(",");
		preferredContact = CommonFunctions.readExcelMasterData(preCon[0], preCon[1], preCon[2]);

		String accName[] = accountName.split(",");
		accountName = CommonFunctions.readExcelMasterData(accName[0], accName[1], accName[2]);

		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		String so[] = source.split(",");
		source = CommonFunctions.readExcelMasterData(so[0], so[1], so[2]);

		String OD[] = originatingdivission.split(",");
		originatingdivission = CommonFunctions.readExcelMasterData(OD[0], OD[1], OD[2]);

		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String m[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		Reporter.addStepLog("User starts creating new SVO Bespoke Opportunity");
		ActionHandler.click(SVOAdditionalvehicleContainer.NewOpportunity);
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.QuoteNextBtn)) {
			ActionHandler.click(SVOItemToapproveContainer.SVOBespokeOpportunity);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects opportunity record type as SVO bespoke");

			ActionHandler.click(SVOItemToapproveContainer.QuoteNextBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User clicks on next button");

		}

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeOppName, oppName + randomNumber);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Name");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeClosedDate, closeDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Closed date");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeStage);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.StageSelection(stage))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Stage for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeProductOffering);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(
				driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ProOfferingSelection(productOffering))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Product Offering for an Opportunity");

		ActionHandler.scrollToView(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		ActionHandler.wait(2);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.RegionSelection(region))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Region for an Opportunity");

		VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.SVOBespokeClient);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeClient, client);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeClientNameSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(client))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Client for an Opportunity");

		VerifyHandler.verifyElementPresent(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		CommonFunctions.attachScreenshot();
		ActionHandler
				.click(driver.findElement(By.xpath(RestrictedPartyScreeningContainer.ValueSelection(Checkcleared))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects restricted party screening as Check cleared for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokePreferredContact, preferredContact);
		ActionHandler.click(SVOItemToapproveContainer.SVOBespokePreferredContactname);
		Reporter.addStepLog("User selects Preferred Contact for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeAccountName, accountName);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeAccountNameSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(accountName))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Account Name for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeRetailerContact, retailerContact);
		ActionHandler.click(SVOItemToapproveContainer.SVOBespokePreferredContactname);
		Reporter.addStepLog("User selects Retailer Contact for an Opportunity");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSource);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.SourceSelection(source))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Source for an Opportunity");

		ActionHandler.click(RestrictedPartyScreeningContainer.SVOOriginatingDivission);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(
				driver.findElement(By.xpath(RestrictedPartyScreeningContainer.OriginatingDivisionSelection(originatingdivission))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Originating Divission for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeBrand, brand);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeBrandSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(brand))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Brand for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeModel, model);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(model))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Model for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSaveOpportunity);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves SVO Bespoke Opportunity");

		OpportunityName = SVOAdditionalvehicleContainer.OpportunityName.getText();
		Reporter.addStepLog("Opportunity Name is = " + OpportunityName);
	}

	@Then("^User is able to verify the Restricted Party Screening under details section of an opportunity$")
	public void user_is_able_to_verify_the_Restricted_Party_Screening_under_details_section_of_an_opportunity()
			throws Throwable {

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		VerifyHandler.verifyElementPresent(RestrictedPartyScreeningContainer.VerifyRestrictedPartyScreeningfield);
		CommonFunctions.attachScreenshot();

		RestrictedPartyScreeningStatus = RestrictedPartyScreeningContainer.VerifyRestrictedPartyScreeningfield
				.getText();
		Reporter.addStepLog("Restricted Party Screening Status is = " + RestrictedPartyScreeningStatus);
		CommonFunctions.attachScreenshot();
		ActionHandler.pageTop();
		Reporter.addStepLog("User verifies the restrcted party screening field set to be check cleared");

		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(2);

	}

	@And("^User create a SVO Private Office Opportunity with Restricted fields like Stage \"([^\"]*)\" Product Offering \"([^\"]*)\" Region \"([^\"]*)\" Client \"([^\"]*)\" Restricted Party Screening Stage \"([^\"]*)\" Preferred Contact \"([^\"]*)\" Account Name \"([^\"]*)\" Retailer Contact \"([^\"]*)\" Source \"([^\"]*)\" Origination Divission \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\"$")
	public void User_create_a_SVO_Private_Office_Opportunity_with_check_cleared(String stage, String productOffering,
			String region, String client, String Checkcleared, String preferredContact, String accountName,
			String retailerContact, String source, String originatingdivission, String brand, String model)
			throws Exception {
		String oppName = "Test_SVO_Private_office_Opportunity_";
		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		String closeDate = CommonFunctions.selectFutureDate();
		Reporter.addStepLog("Opportunity Close Date is = " + closeDate);

		String s[] = stage.split(",");
		stage = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String proOff[] = productOffering.split(",");
		productOffering = CommonFunctions.readExcelMasterData(proOff[0], proOff[1], proOff[2]);

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String c[] = client.split(",");
		client = CommonFunctions.readExcelMasterData(c[0], c[1], c[2]);

		String RPS[] = Checkcleared.split(",");
		Checkcleared = CommonFunctions.readExcelMasterData(RPS[0], RPS[1], RPS[2]);

		String preCon[] = preferredContact.split(",");
		preferredContact = CommonFunctions.readExcelMasterData(preCon[0], preCon[1], preCon[2]);

		String accName[] = accountName.split(",");
		accountName = CommonFunctions.readExcelMasterData(accName[0], accName[1], accName[2]);

		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		String so[] = source.split(",");
		source = CommonFunctions.readExcelMasterData(so[0], so[1], so[2]);

		String OD[] = originatingdivission.split(",");
		originatingdivission = CommonFunctions.readExcelMasterData(OD[0], OD[1], OD[2]);

		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String m[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		Reporter.addStepLog("User starts creating new SVO Bespoke Opportunity");
		ActionHandler.click(SVOAdditionalvehicleContainer.NewOpportunity);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeOppName, oppName + randomNumber);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Name");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeClosedDate, closeDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Closed date");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeStage);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.StageSelection(stage))));

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Stage for an Opportunity");
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeProductOffering);

		CommonFunctions.attachScreenshot();
		ActionHandler.click(
				driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ProOfferingSelection(productOffering))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Product Offering for an Opportunity");

		ActionHandler.wait(3);
		ActionHandler.scrollToView(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.RegionSelection(region))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Region for an Opportunity");

		VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.SVOBespokeClient);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeClient, client);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeClientNameSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(client))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Client for an Opportunity");

		VerifyHandler.verifyElementPresent(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User views restricted party screening lists for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokePreferredContact, preferredContact);
		ActionHandler.wait(4);
		ActionHandler.click(SVOItemToapproveContainer.SVOBespokePreferredContactname);
		Reporter.addStepLog("User selects Preferred Contact for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeAccountName, accountName);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeAccountNameSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(accountName))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Account Name for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeRetailerContact, retailerContact);
		ActionHandler.click(SVOItemToapproveContainer.SVOBespokePreferredContactname);
		Reporter.addStepLog("User selects Retailer Contact for an Opportunity");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSource);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.SourceSelection(source))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Source for an Opportunity");

		ActionHandler.click(RestrictedPartyScreeningContainer.SVOOriginatingDivission);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(
				driver.findElement(By.xpath(RestrictedPartyScreeningContainer.ValueSelection(originatingdivission))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Originating Divission for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeBrand, brand);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeBrandSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(brand))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Brand for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeModel, model);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(model))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Model for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSaveOpportunity);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves SVO Bespoke Opportunity");

		OpportunityName = SVOAdditionalvehicleContainer.OpportunityName.getText();
		Reporter.addStepLog("Opportunity Name is = " + OpportunityName);
	}

	@Then("^Verify the error message while moving opportunity to verbel commitment stage$")
	public void verify_the_error_message_while_moving_opportunity_to_verbel_commitment_stage() throws Throwable {
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.MarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		VerifyHandler.verifyElementPresent(RestrictedPartyScreeningContainer.VerifyCheckInProgressErrormessage);
		CommonFunctions.attachScreenshot();
		ErrorMessage = RestrictedPartyScreeningContainer.VerifyCheckInProgressErrormessage.getText();
		Reporter.addStepLog("Error mmessage is = " + ErrorMessage);
		Reporter.addStepLog(
				"User verifies the error message stating 'You cannot start contracting without Restricted Party Screening being Cleared'");
	}

	@Then("^User is able to verify the Restricted Party Screening as restricted under details section of an opportunity$")
	public void user_is_able_to_verify_the_Restricted_Party_Screening_as_restricted_under_details_section_of_an_opportunity()
			throws Throwable {
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		VerifyHandler.verifyElementPresent(
				RestrictedPartyScreeningContainer.VerifyRestrictedPartyScreeningfieldAsRestricted);
		CommonFunctions.attachScreenshot();
		RestrictedPartyScreeningStatus = RestrictedPartyScreeningContainer.VerifyRestrictedPartyScreeningfieldAsRestricted
				.getText();
		Reporter.addStepLog("Restricted Party Screening Status is = " + RestrictedPartyScreeningStatus);
		CommonFunctions.attachScreenshot();
		ActionHandler.pageTop();
		ActionHandler.wait(2);
		Reporter.addStepLog("User verifies the restrcted party screening field set to be Restricted");
	}

	@And("^User moves an Oppotunity to Order Placed stage after accepting the Quote for Retailer Contact \"([^\"]*)\"$")
	public void User_moves_an_opportunity_to_Order_Placed_after_accepting_the_quote(String retailerContact)
			throws Exception {
		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		String quoteName = "SVO Bespoke Test Quote";
		String expirtaionDate = CommonFunctions.selectFutureDate();

		ActionHandler.pageDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuotesLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Quotes page");

		ActionHandler.click(SVOAdditionalvehicleContainer.NewQuoteBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on New button to create new Quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteNameBox, quoteName);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User writes quote name for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRetailerNameBox, retailerContact);
		ActionHandler.click(SVOItemToapproveContainer.QuoteRetailerContact);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters retailer contact details for new quote");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteCostPrice, "123");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Cost Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteListPrice, "203");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters List Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteContribution, "23");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Contribution for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRetailPrice, "213");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Retail Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRevenue, "113");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Revenue for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteExpirationDate, expirtaionDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Expiration Date for new quote");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteSaveBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User save new quote");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstQuote);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens created quote details");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.DesignBriefLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Design Brief Section");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstDesignBrief);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens first design brief from the list");

		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefSubmit);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Design Brief status moves to Submitted from Draft");

		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefSaveSubmission);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves Design Brief Submission");

		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefReturnRenderPack);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User returns render pack for Design Brief");

		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefSaveRenderPack);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves Design Brief for Return Render Pack");

		javaScriptUtil.clickElementByJS(SVO_OpportunityContainer.OpportTab);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOAdditionalvehicleContainer.OpportunitySearchBox, OpportunityName);
		Actions act5 = new Actions(driver);
		act5.sendKeys(Keys.ENTER).build().perform();
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstOpportunityLink);
		CommonFunctions.attachScreenshot();

		ActionHandler.pageDown();
		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuotesLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Quotes page");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstQuoteFromOpportunity);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens created quote details");

		ActionHandler.wait(8);
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(8);
		Reporter.addStepLog("Quote status moves to Price stage");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(8);
		Reporter.addStepLog("Quote status moves to Proposal Sent stage");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(10);

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDrpDwn);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeAccepted);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User mark accepeted as Quote Outcome");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDetailDrpDwn);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDetailAccepted);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDoneBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User accepeted the Quote");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuoteOpportunityLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate to Opportunity");

		driver.navigate().refresh();
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.OrdersLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Orders link");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstOrder);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens Order details for the opportunity");

		ActionHandler.click(RestrictedPartyScreeningContainer.CommonOrderNumber1);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(SVOAdditionalvehicleContainer.CommonOrderNumberBox, "123456");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(RestrictedPartyScreeningContainer.SaveCommonOrderNumber1);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User saves common order number");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(10);
		Reporter.addStepLog("User moves the Opportunity to Committed to Build Stage");

		ActionHandler.click(SVOAdditionalvehicleContainer.CloseOrderTab);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User closed Orders Tab");

		javaScriptUtil.clickElementByJS(SVO_OpportunityContainer.OpportTab);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOAdditionalvehicleContainer.OpportunitySearchBox, OpportunityName);
		ActionHandler.wait(2);
		Actions act6 = new Actions(driver);
		act6.sendKeys(Keys.ENTER).build().perform();
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstOpportunityLink);
		CommonFunctions.attachScreenshot();

		VerifyHandler.verifyElementPresent(RestrictedPartyScreeningContainer.VerifyClosedoneStage);
		Reporter.addStepLog("Opportunity is in Closed Won stage");

	}

	@Given("^laterally Access SVO Portal by user name \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void laterally_Access_SVO_Portal_by_User_name_and_password(String UserName, String Password)
			throws Exception {
		driver = getDriver();
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);

		ActionHandler.wait(2);
		if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.userNameTextBox)) {
			ActionHandler.wait(1);
			ActionHandler.click(SVOEnquiryContainer.icon_image);
			ActionHandler.wait(6);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Click on Logout");
			ActionHandler.click(SVOEnquiryContainer.Logout);
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logout from SVO Portal");
		}

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		Reporter.addStepLog("User Logins to SVO Portal");

		String s[] = UserName.split(",");
		UserName = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String p[] = Password.split(",");
		Password = CommonFunctions.readExcelMasterData(p[0], p[1], p[2]);

		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, UserName);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, Password);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();

		ActionHandler.waitForElement(SVOEnquiryContainer.SVOText, 100);
		CommonFunctions.attachScreenshot();
		VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
		Reporter.addStepLog("User navigate to SVO Home Page successfully");
	}

	@And("^Verify the user is not able to move the opportunity stage to Design Brief and Quote without clearing Restricted Party Screening$")
	public void Verify_the_user_is_not_able_to_move_the_opportunity_stage_to_Design_Brief_and_Quote_without_clearing_Restricted_Party_Screening()
			throws Exception {

		VerifyHandler.verifyElementPresent(RestrictedPartyScreeningContainer.RestrictedPartyScreeningErrorMsg);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"An error message displayed to clear the restricted party screening while moving the opportunity stage from commission to Desigh Brie and Quote stage");
	}

}
