package com.jlr.svo.tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.containers.SSEC_1227_CasesCreationContainer;
import com.jlr.svo.containers.SSEC_1237_CaseClosureContainer;
import com.jlr.svo.containers.SSEC_1238_CaseLifecycle_Container;
import com.jlr.svo.containers.SSEC_1503_Clasic_Email_Signature_On_Salesforce_Container;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.containers.SVO_CustomerResponsesContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.Then;

public class SSEC_1826_ClassicPartsFeedbacksFromShowAndTell_Test extends TestBaseCC {

	private WebDriver driver = getDriver();
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SSEC_1227_CasesCreationContainer CasesContainer = PageFactory.initElements(driver,
			SSEC_1227_CasesCreationContainer.class);
	SSEC_1238_CaseLifecycle_Container CaseLifecycle_Container = PageFactory.initElements(driver,
			SSEC_1238_CaseLifecycle_Container.class);
	SSEC_1503_Clasic_Email_Signature_On_Salesforce_Container Clasic_Email_Signature_On_Salesforce_Container = PageFactory.initElements(driver,
			SSEC_1503_Clasic_Email_Signature_On_Salesforce_Container.class);
	SVO_CustomerResponsesContainer SVO_CustomerResponsesContainer = PageFactory.initElements(driver,
			SVO_CustomerResponsesContainer.class);
	
	public static String CaseNumber;
	
	public static double getRandomNumber(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	// onStart
	public void onStart() {
		setupTest("SVOTest");
		driver = getDriver();
		SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
		CaseLifecycle_Container = PageFactory.initElements(driver,SSEC_1238_CaseLifecycle_Container.class);
		Clasic_Email_Signature_On_Salesforce_Container = PageFactory.initElements(driver,
				SSEC_1503_Clasic_Email_Signature_On_Salesforce_Container.class);
		SVO_CustomerResponsesContainer = PageFactory.initElements(driver,
				SVO_CustomerResponsesContainer.class);
		driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
		CaseNumber = null;
	}
	
	
	@Then("^User add values to what's on your mind field on Cases page$")
	public void user_add_values_to_what_s_on_your_mind_field_on_Cases_page() throws Throwable {
	    
	}

	@Then("^User edit values of what's on your mind field on Cases page$")
	public void user_edit_values_of_what_s_on_your_mind_field_on_Cases_page() throws Throwable {
	    
	}

	@Then("^User remove the values of what's on your mind field on Cases page$")
	public void user_remove_the_values_of_what_s_on_your_mind_field_on_Cases_page() throws Throwable {
	   
	}

	@Then("^Verify that the SLA timer is running between (\\d+):(\\d+) and (\\d+):(\\d+) under milestones details on cases page$")
	public void verify_that_the_SLA_timer_is_running_between_and_under_milestones_details_on_cases_page(int arg1, int arg2, int arg3, int arg4) throws Throwable {
	   
	}
	
	@Then("^User create new case without VIN number record type \"([^\"]*)\"$")
	public void user_create_new_case_without_VIN_number_record_type(String recordtype) throws Throwable {

		String s[] = recordtype.split(",");
		recordtype = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(CaseLifecycle_Container.CasesTab);
		ActionHandler.wait(1);
		Reporter.addStepLog("User navigates to cases tab");

		ActionHandler.click(CaseLifecycle_Container.NewBtn);
		ActionHandler.wait(2);
		ActionHandler.click(driver.findElement(By.xpath(CaseLifecycle_Container.CaseRecordType(recordtype))));
		CommonFunctions.attachScreenshot();
		ActionHandler.click(CaseLifecycle_Container.NextBtn);
		Reporter.addStepLog("User chooses record type for an case");

		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(1);
		ActionHandler.click(CaseLifecycle_Container.ChassisNumber);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseLifecycle_Container.ChassisNumber, "123456");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters chassis number for case");

		ActionHandler.click(CaseLifecycle_Container.SaveBtn);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves case");
		
		ActionHandler.wait(8);
		driver.navigate().refresh();
		ActionHandler.wait(10);

		CaseNumber = CaseLifecycle_Container.CaseNumber.getText();
		Reporter.addStepLog("Case number is = " + CaseNumber);
		
	}
	
	@Then("^User verifies that email signatute while sending an email to customer \"([^\"]*)\" with Business unit \"([^\"]*)\" and subject and verify works legend stock link in email signature$")
	public void user_verifies_that_email_signature_while_sending_an_email_to_customer_with_Business_unit_and_subject_and_verify_works_legend_stock_link_in_email_signature(String Customer, String BusinessUnit)
			throws Throwable {

		String p[] = BusinessUnit.split(",");
		BusinessUnit = CommonFunctions.readExcelMasterData(p[0], p[1], p[2]);

		String s[] = Customer.split(",");
		Customer = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		ActionHandler.pageDown();
		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(SVO_CustomerResponsesContainer.EmailsSection);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigates to email section on Enquiry page");
		
		ActionHandler.wait(10);
		ActionHandler.scrollToView(SVO_CustomerResponsesContainer.e2aSendEmailButton);
		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(SVO_CustomerResponsesContainer.e2aSendEmailButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on e2a send an email button");

		ActionHandler.wait(5);
		//driver.switchTo().frame(0);
		driver.switchTo().frame(Clasic_Email_Signature_On_Salesforce_Container.EmailFrame);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user navigate to email frame");

		ActionHandler.wait(10);
		ActionHandler.click(SVO_CustomerResponsesContainer.BusinessUnitDropDown);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Business unit drop down list");

		ActionHandler.click(driver.findElement(By.xpath(SVO_CustomerResponsesContainer.BusinessUnitSelection(BusinessUnit))));
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects the " + BusinessUnit + " Business unit from drop down list");

		ActionHandler.scrollDown();
		ActionHandler.wait(2);
		
	    ActionHandler.click(SVO_CustomerResponsesContainer.EmailToTextBox);
		ActionHandler.wait(1);
		ActionHandler.setText(SVO_CustomerResponsesContainer.EmailToTextBox, Customer);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters " + Customer + " in To section of an e2a email");

		ActionHandler.click(SVO_CustomerResponsesContainer.EmailSubjectTextBox);
		ActionHandler.wait(1);
		ActionHandler.setText(SVO_CustomerResponsesContainer.EmailSubjectTextBox, "Please specify the body style?");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters text in subject section of an e2a email");

		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(2);
		
		VerifyHandler.verifyElementPresent(Clasic_Email_Signature_On_Salesforce_Container.WorksLegendsStockLink);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User verifies work legends stock link is present on salesforce");
		
		

		ActionHandler.click(SVO_CustomerResponsesContainer.EmailSendButton);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks on send an email button");

		driver.switchTo().parentFrame();

	}
	
	@Then("^Verify that default case owner as 'Glen Parkes' on cases page$")
	public void verify_that_default_case_owner_as_Glen_Parkes_on_cases_page() throws Throwable {
	    
	}

	@Then("^Verify that VIN number is empty after new parts and technical case created$")
	public void verify_that_VIN_number_is_empty_after_new_parts_and_technical_case_created() throws Throwable {
	    
	}
	
	@Then("^Verify that default case owner is not updated to 'Glen Parkes' on cases page$")
	public void verify_that_default_case_owner_is_not_updated_to_Glen_Parkes_on_cases_page() throws Throwable {
	    
	}

	@Then("^Verify that the SLA timer will not start if Classic customer service case created before (\\d+):(\\d+) AM$")
	public void verify_that_the_SLA_timer_will_not_start_if_Classic_customer_service_case_created_before_AM(int arg1, int arg2) throws Throwable {
	   
	}

	@Then("^Verify that the SLA timer will not start if Classic parts and technical case created after (\\d+):(\\d+) PM$")
	public void verify_that_the_SLA_timer_will_not_start_if_Classic_parts_and_technical_case_created_after_PM(int arg1, int arg2) throws Throwable {
	    
	}

}
