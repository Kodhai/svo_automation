package com.jlr.svo.tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.containers.SSEC_2150_Booker25AmendementsContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SSEC_2150_Booker25Amendemnets_Test extends TestBaseCC {

	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory
			.getLogger(SSEC_1039_SVOEnquiryEmailRemaindersTest.class.getName());
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
	SSEC_2150_Booker25AmendementsContainer SSEC_2150_Booker25AmendementsContainer = PageFactory.initElements(driver, SSEC_2150_Booker25AmendementsContainer.class);
	
	public static String verificationCode;
	public static String vCode;
	public static String veriCode;
	public static String EnquiryName;

	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}
	

	public void onStart() {

		setupTest("SVOTest");
		driver = getDriver();

		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		SSEC_2150_Booker25AmendementsContainer = PageFactory.initElements(driver, SSEC_2150_Booker25AmendementsContainer.class);
	    
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		

		verificationCode = null;
		vCode = null;
		EnquiryName = null;

	}

    @And("^Navigate to Commissioning Requests$")
    public void Navigate_to_Commissioning_requests() throws Exception{
    	
    	ActionHandler.click(SSEC_2150_Booker25AmendementsContainer.CommissioningRequestTab);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on Commissioning Requests Tab");
    }

    @When("^User clicks on New button$")
    public void User_clicks_on_New_button() throws Exception{
    	
    	ActionHandler.click(SSEC_2150_Booker25AmendementsContainer.NewBtn);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on New button");
    }

    @And("^Select Bespoke record type$")
    public void Select_Bespoke_record_type() throws Exception{
    	
    	ActionHandler.click(SSEC_2150_Booker25AmendementsContainer.NextBtn);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on Next button");
    }

    @And("^Fills the Start Date and End Date$")
    public void Fills_the_Start_Date_and_End_Date() throws Exception{
    	
    	ActionHandler.click(SSEC_2150_Booker25AmendementsContainer.StartingDate);
    	ActionHandler.wait(1);
    	ActionHandler.setText(SSEC_2150_Booker25AmendementsContainer.StartingDate, CommonFunctions.selectFutureDate());
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on Starting Date field");
    	
    	ActionHandler.click(SSEC_2150_Booker25AmendementsContainer.EndingDate);
    	ActionHandler.wait(1);
    	ActionHandler.setText(SSEC_2150_Booker25AmendementsContainer.EndingDate, CommonFunctions.selectFutureDate());
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on Ending Date field");
    }
    
    @Then("^Click on Save button of Commissioning request$")
    public void Click_on_Save_button_Commissioning_request() throws Exception{
    	
    	ActionHandler.click(SSEC_2150_Booker25AmendementsContainer.SaveEdit);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on SaveEdit button");
    }

    @When("^User navigates to Commissioning Request Tab$")
    public void User_navigates_to_Commissioning_request() throws Exception{
    	
    	ActionHandler.click(SSEC_2150_Booker25AmendementsContainer.CommissioningRequestTab);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on Commissioning Requests tab");
    
    }

    @And("^Select Private Office record type$")
    public void Select_Private_Office_record_type() throws Exception{
    	
    	ActionHandler.click(SSEC_2150_Booker25AmendementsContainer.PrivateOffice);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on Private Office radio button");
    	
    }
    
    @And("^Fills the Start Date and End Date Duration TourType \"([^\"]*)\"$")
    public void Fills_Start_Date_and_End_Date() throws Exception{
    	
    	ActionHandler.click(SSEC_2150_Booker25AmendementsContainer.StartingDate);
    	ActionHandler.wait(1);
    	ActionHandler.setText(SSEC_2150_Booker25AmendementsContainer.StartingDate, CommonFunctions.selectFutureDate());
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on Starting Date field");
    	
    	ActionHandler.click(SSEC_2150_Booker25AmendementsContainer.EndingDate);
    	ActionHandler.wait(1);
    	ActionHandler.setText(SSEC_2150_Booker25AmendementsContainer.EndingDate, CommonFunctions.selectFutureDate());
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on Ending Date field");
    	
    	ActionHandler.click(SSEC_2150_Booker25AmendementsContainer.DurationHours);
    	ActionHandler.wait(1);
    	ActionHandler.setText(SSEC_2150_Booker25AmendementsContainer.DurationHours, "60");
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on Duration field");
    	
    	ActionHandler.click(SSEC_2150_Booker25AmendementsContainer.EstAttendees);
    	ActionHandler.wait(1);
    	ActionHandler.setText(SSEC_2150_Booker25AmendementsContainer.EstAttendees, "10");
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on Est Attendees field");
    	
    }
    
    @And("^Check all the checkboxes of Commissioning Request form$")
    public void Check_all_the_checkboxes_of_CommissioningRequest() throws Exception{
    	
    	ActionHandler.click(SSEC_2150_Booker25AmendementsContainer.IncludesClassicVisit);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on Include Classic Visit field");
    	
    	ActionHandler.click(SSEC_2150_Booker25AmendementsContainer.CateringRequired);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click onCatering Required field");
    	
    	ActionHandler.click(SSEC_2150_Booker25AmendementsContainer.AVRequired);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on AV Required field");
    	
    	ActionHandler.click(SSEC_2150_Booker25AmendementsContainer.PPERequired);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on PPE Required field");
    	
    }

    @And("^Clicks on Submit button$")
    public void Clicks_on_Submit_button() throws Exception{
    	
    	ActionHandler.click(SSEC_2150_Booker25AmendementsContainer.SubmitBtnCommissioningReq);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on Commissioning Requests tab");
    	
    	ActionHandler.click(SSEC_2150_Booker25AmendementsContainer.SaveBtn);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on Commissioning Requests tab");
    	
    }
    
    @And("^User creates a new Reservation$")
    public void User_creats_a_new_Reservation() throws Exception{
    	
    	ActionHandler.wait(2);
    	driver.navigate().refresh();
    	ActionHandler.wait(8);
    	
    	ActionHandler.click(SSEC_2150_Booker25AmendementsContainer.ReservationDropDownBtn);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on Commissioning Requests tab");
    	
    	ActionHandler.click(SSEC_2150_Booker25AmendementsContainer.NewReservationBtn);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on Commissioning Requests tab");
    	
    	ActionHandler.click(SSEC_2150_Booker25AmendementsContainer.BespokeReservationRecordType);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on Commissioning Requests tab");
    	
    	ActionHandler.click(SSEC_2150_Booker25AmendementsContainer.NextBtnReservation);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on Commissioning Requests tab");
    	
    }

    @Then("^Fills Reservation details such as PrimaryContact \"([^\"]*)\" MeetingType \"([^\"]*)\"$")
    public void Fills_Reservation_Details(String PrimaryCont, String MeetingType) throws Exception{
    	
    	String PC[] = PrimaryCont.split(",");
    	PrimaryCont = CommonFunctions.readExcelMasterData(PC[0], PC[1], PC[2]);

    	String MeetingT[] = MeetingType.split(",");
    	MeetingType = CommonFunctions.readExcelMasterData(MeetingT[0], MeetingT[1], MeetingT[2]);

    	ActionHandler.click(SSEC_2150_Booker25AmendementsContainer.StartDateReservation);
    	ActionHandler.setText(SSEC_2150_Booker25AmendementsContainer.StartDateReservation, CommonFunctions.selectFutureDate());
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on Commissioning Requests tab");
    	
    	ActionHandler.click(SSEC_2150_Booker25AmendementsContainer.StartTimeReservation);
    	ActionHandler.setText(SSEC_2150_Booker25AmendementsContainer.StartTimeReservation, "11:00");
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on Commissioning Requests tab");
    	
    	ActionHandler.click(SSEC_2150_Booker25AmendementsContainer.EndDateReservation);
    	ActionHandler.setText(SSEC_2150_Booker25AmendementsContainer.EndDateReservation, CommonFunctions.selectFutureDate());
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on Commissioning Requests tab");
    	
    	ActionHandler.click(SSEC_2150_Booker25AmendementsContainer.PrimaryContactRservation);
    	ActionHandler.click(driver.findElement(By.xpath(SSEC_2150_Booker25AmendementsContainer.PrimaryContact(PrimaryCont))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on Commissioning Requests tab");
    	
    	ActionHandler.click(SSEC_2150_Booker25AmendementsContainer.MaxAttendeesReservation);
    	ActionHandler.clearAndSetText(SSEC_2150_Booker25AmendementsContainer.MaxAttendeesReservation, "3");
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on Commissioning Requests tab");
    	
    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	
    	ActionHandler.click(SSEC_2150_Booker25AmendementsContainer.MeetingTypeReservation);
    	ActionHandler.click(driver.findElement(By.xpath(SSEC_2150_Booker25AmendementsContainer.PrimaryContact(MeetingType))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on Commissioning Requests tab");
    	
    	ActionHandler.click(SSEC_2150_Booker25AmendementsContainer.SaveEdit);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on Commissioning Requests tab");
    	
    }
    
    @And("^User edits Status as booked \"([^\"]*)\" and Host \"([^\"]*)\" for Reservaton and Save it$")
    public void Useer_edits_status_as_booked_for_reservation(String Status, String Host) throws Exception{
    	
    	String Stat[] = Status.split(",");
    	Status = CommonFunctions.readExcelMasterData(Stat[0], Stat[1], Stat[2]);

    	String HostReser[] = Host.split(",");
    	Host = CommonFunctions.readExcelMasterData(HostReser[0], HostReser[1], HostReser[2]);

    	ActionHandler.click(SSEC_2150_Booker25AmendementsContainer.ReservationRecord);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on Commissioning Requests tab");
    	
    	ActionHandler.click(SSEC_2150_Booker25AmendementsContainer.StatusLabelReservation);
    	ActionHandler.click(driver.findElement(By.xpath(SSEC_2150_Booker25AmendementsContainer.PrimaryContact(Status))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on Commissioning Requests tab");
    	
    	ActionHandler.click(SSEC_2150_Booker25AmendementsContainer.HostReservation);
    	ActionHandler.click(driver.findElement(By.xpath(SSEC_2150_Booker25AmendementsContainer.PrimaryContact(Host))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on Commissioning Requests tab");
    	  	
    	ActionHandler.click(SSEC_2150_Booker25AmendementsContainer.SaveEdit);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on Commissioning Requests tab");
    	
    	
    }

    //Verify that once the Reservation status is changed to Booked, Commissioning Status is also changed to Booked
    @Then("^Verify that once the Reservation status is changed to Booked, Commissioning Status is also changed to Booked$")
    public void Verify_reservation_and_commissioning_status() throws Exception{
    	
    	ActionHandler.wait(2);
    	driver.navigate().back();
    	ActionHandler.wait(2);
    	
    	String StatusVerify = SSEC_2150_Booker25AmendementsContainer.StatusLabelReservation.getText();
    	ActionHandler.wait(1);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on Commissioning Requests tab");
    	
    	
    }






}