package com.jlr.svo.tests;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.autotest.utils.Config;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.constants.Constants;
import com.jlr.svo.containers.SSEC_1005_AutopoulatedFieldsOnOrderWorkOrderContainer;
import com.jlr.svo.containers.SSEC_1085_BillingAddressOnEnquiryContainer;
import com.jlr.svo.containers.SVOAccountsContainer;
import com.jlr.svo.containers.SVOAdditionalVehicleContainer;
import com.jlr.svo.containers.SVOCommentFieldAdditionContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.containers.SVOItemToApproveContainer;
import com.jlr.svo.containers.SVOOpportunityContainer;
import com.jlr.svo.containers.SVOWorkOrderContainer;
import com.jlr.svo.containers.SVO_CancelOrderAtAnyStageContainer;
import com.jlr.svo.containers.SVO_CustomerResponsesContainer;
import com.jlr.svo.containers.SVO_OpportunityInvoiceContainer;
import com.jlr.svo.containers.SVO_OpportunitySecondaryOwnerContainer;
import com.jlr.svo.containers.SVO_Pricing_LogicContainer;
import com.jlr.svo.containers.SVO_RestrictedPartyScreeningContainer;
import com.jlr.svo.containers.SVO_WorkOrder_AdditionalOwner_Container;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class SVO_OpportunitySecondaryOwner_Test extends TestBaseCC {

	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory.getLogger(SVO_OpportunitySecondaryOwner_Test.class.getName());
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);

	SVOAccountsContainer SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
	SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	SVOAdditionalVehicleContainer SVOAdditionalvehicleContainer = PageFactory.initElements(driver,
			SVOAdditionalVehicleContainer.class);
	SVO_RestrictedPartyScreeningContainer RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
			SVO_RestrictedPartyScreeningContainer.class);
	SVOItemToApproveContainer SVOItemToapproveContainer = PageFactory.initElements(driver,
			SVOItemToApproveContainer.class);
	SVO_CustomerResponsesContainer SVO_CustomerResponsesContainer = PageFactory.initElements(driver,
			SVO_CustomerResponsesContainer.class);
	SVO_OpportunitySecondaryOwnerContainer SVO_OpportunitySecondaryOwnerContainer = PageFactory.initElements(driver,
			SVO_OpportunitySecondaryOwnerContainer.class);
	SVO_OpportunityInvoiceContainer SVO_OpportunityInvoiceContainer = PageFactory.initElements(driver,
			SVO_OpportunityInvoiceContainer.class);
	SVO_Pricing_LogicContainer SVO_Pricing_LogicContainer = PageFactory.initElements(driver,
			SVO_Pricing_LogicContainer.class);
	SSEC_1085_BillingAddressOnEnquiryContainer BillingAddressOnEnquiryContainer = PageFactory.initElements(driver,
			SSEC_1085_BillingAddressOnEnquiryContainer.class);
	SVO_CancelOrderAtAnyStageContainer SVO_CancelOrderAtAnyStageContainer = PageFactory.initElements(driver,
			SVO_CancelOrderAtAnyStageContainer.class);
	SVOCommentFieldAdditionContainer SVOCommentFieldAdditionContainer = PageFactory.initElements(driver,
			SVOCommentFieldAdditionContainer.class);
	SVOWorkOrderContainer SVOWorkOrderContainer = PageFactory.initElements(driver, SVOWorkOrderContainer.class);
	SSEC_1005_AutopoulatedFieldsOnOrderWorkOrderContainer AutopoulatedFieldsOnOrderWorkOrder = PageFactory
			.initElements(driver, SSEC_1005_AutopoulatedFieldsOnOrderWorkOrderContainer.class);
	SVO_WorkOrder_AdditionalOwner_Container SVO_WorkOrder_AdditionalOwner_Container = PageFactory.initElements(driver,
			SVO_WorkOrder_AdditionalOwner_Container.class);

	public static String verificationCode;
	public static String vCode;
	public static String veriCode;
	public static String OpportunityName;
	public static String UpdatedOpportunityName;

	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	public void onStart() {

		setupTest("SVOTest");
		driver = getDriver();

		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
		SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
		SVOAdditionalvehicleContainer = PageFactory.initElements(driver, SVOAdditionalVehicleContainer.class);
		RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
				SVO_RestrictedPartyScreeningContainer.class);
		SVOItemToapproveContainer = PageFactory.initElements(driver, SVOItemToApproveContainer.class);
		SVO_CustomerResponsesContainer = PageFactory.initElements(driver, SVO_CustomerResponsesContainer.class);
		SVO_OpportunitySecondaryOwnerContainer = PageFactory.initElements(driver,
				SVO_OpportunitySecondaryOwnerContainer.class);
		SVO_OpportunityInvoiceContainer = PageFactory.initElements(driver, SVO_OpportunityInvoiceContainer.class);
		SVO_Pricing_LogicContainer = PageFactory.initElements(driver, SVO_Pricing_LogicContainer.class);
		BillingAddressOnEnquiryContainer = PageFactory.initElements(driver,
				SSEC_1085_BillingAddressOnEnquiryContainer.class);
		SVO_CancelOrderAtAnyStageContainer = PageFactory.initElements(driver, SVO_CancelOrderAtAnyStageContainer.class);
		SVOCommentFieldAdditionContainer = PageFactory.initElements(driver, SVOCommentFieldAdditionContainer.class);
		SVOWorkOrderContainer = PageFactory.initElements(driver, SVOWorkOrderContainer.class);
		AutopoulatedFieldsOnOrderWorkOrder = PageFactory.initElements(driver,
				SSEC_1005_AutopoulatedFieldsOnOrderWorkOrderContainer.class);
		SVO_WorkOrder_AdditionalOwner_Container = PageFactory.initElements(driver,
				SVO_WorkOrder_AdditionalOwner_Container.class);

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		verificationCode = null;
		vCode = null;
		veriCode = null;
		OpportunityName = null;
		UpdatedOpportunityName = null;
	}

	@Given("^Access to an SVO Portal as Classic Admin user with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void access_to_the_SVO_Portal_as_classic_admin_user_with_Username_and_Password(String userName,
			String password) throws Throwable {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		onStart();
		driver = getDriver();
		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailForClassicadminUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		}
	}

	public String checkEmailForClassicadminUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(340, 346);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	@Given("^Access to SVO Portal as classic service user by add username with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void access_to_an_SVO_portal_as_classic_service_user_by_add_username_with_Username_and_Password(
			String userName, String password) throws Throwable {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailForclassicserviceUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	public String checkEmailForclassicserviceUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(342, 348);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;

	}

	@Given("^Access to SVO Portal as classic Sales user with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void access_to_an_SVO_portal_as_classic_sales_user_with_Username_and_Password(String userName,
			String password) throws Throwable {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		onStart();
		driver = getDriver();
		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailForclassicsalesUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	public String checkEmailForclassicsalesUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(340, 346);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;

	}

	@Given("^Access to SVO Portal as classic Sales user by using add username with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void access_to_an_SVO_portal_as_classic_sales_userby_using_add_username_with_Username_and_Password(
			String userName, String password) throws Throwable {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailForclassicsalesAddUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	public String checkEmailForclassicsalesAddUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(340, 346);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;

	}

	@Given("^Access to SVO Portal as Classic Finance user by using add username with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void access_to_an_SVO_portal_as_classic_finance_user_by_using_add_username_with_Username_and_Password(
			String userName, String password) throws Throwable {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailForClassicFinanceUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	public String checkEmailForClassicFinanceUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(342, 348);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	@Given("^Access to SVO Portal as Classic Admin user by using add username with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void access_to_an_SVO_Portal_as_classic_admin_user_by_using_add_username_with_Username_and_Password(
			String userName, String password) throws Throwable {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailForClassicAdminUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		}
	}

	public String checkEmailForClassicAdminUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(340, 346);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	@And("^User creates an Classic Opportunity with mandatory fields like Stage \"([^\"]*)\" Product Offering \"([^\"]*)\" Region \"([^\"]*)\" Client \"([^\"]*)\" Restricted Party Screening Stage \"([^\"]*)\" Preferred Contact \"([^\"]*)\" SAP Account \"([^\"]*)\" Account Name \"([^\"]*)\" Retailer Contact \"([^\"]*)\" Source \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\"$")
	public void User_creates_an_Classic_Opportunity(String stage, String productOffering, String region, String client,
			String Checkcleared, String preferredContact, String SAPAccount, String accountName, String retailerContact,
			String source, String brand, String model) throws Exception {
		String oppName = "Test_Classic_Service_Opportunity_";
		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		String closeDate = CommonFunctions.selectFutureDate();
		Reporter.addStepLog("Opportunity Close Date is = " + closeDate);

		String s[] = stage.split(",");
		stage = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String proOff[] = productOffering.split(",");
		productOffering = CommonFunctions.readExcelMasterData(proOff[0], proOff[1], proOff[2]);

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String c[] = client.split(",");
		client = CommonFunctions.readExcelMasterData(c[0], c[1], c[2]);

		String RPS[] = Checkcleared.split(",");
		Checkcleared = CommonFunctions.readExcelMasterData(RPS[0], RPS[1], RPS[2]);

		String preCon[] = preferredContact.split(",");
		preferredContact = CommonFunctions.readExcelMasterData(preCon[0], preCon[1], preCon[2]);

		String SAP[] = SAPAccount.split(",");
		SAPAccount = CommonFunctions.readExcelMasterData(SAP[0], SAP[1], SAP[2]);

		String accName[] = accountName.split(",");
		accountName = CommonFunctions.readExcelMasterData(accName[0], accName[1], accName[2]);

		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		String so[] = source.split(",");
		source = CommonFunctions.readExcelMasterData(so[0], so[1], so[2]);

		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String m[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		ActionHandler.wait(3);
		Reporter.addStepLog("User starts creating new SVO Classic Service Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.NewOpportunity);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		// ActionHandler.wait(13);
		// ActionHandler.wait(50);

		if (VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.QuoteNextBtn)) {
			ActionHandler.click(SVO_OpportunityInvoiceContainer.SVOClassicServiceOpportunity);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects opportunity record type as Classic Service");

			ActionHandler.click(SVOItemToapproveContainer.QuoteNextBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User clicks on next button");

		}

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeOppName, oppName + randomNumber);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Name");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeClosedDate, closeDate);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Closed date");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeStage);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.StageSelection(stage))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Stage for an Opportunity");

		ActionHandler.scrollToView(SVOAdditionalvehicleContainer.SVOBespokeProductOffering);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeProductOffering);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(
				driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ProOfferingSelection(productOffering))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Product Offering for an Opportunity");

		ActionHandler.scrollToView(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		ActionHandler.wait(2);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.RegionSelection(region))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Region for an Opportunity");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeAccountName, accountName);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeAccountNameSearch);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(accountName))));
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Account Name for an Opportunity");

		ActionHandler.scrollToView(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		VerifyHandler.verifyElementPresent(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		CommonFunctions.attachScreenshot();
		ActionHandler
				.click(driver.findElement(By.xpath(RestrictedPartyScreeningContainer.ValueSelection(Checkcleared))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects restricted party screening as Check cleared for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokePreferredContact, preferredContact);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		// act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		act1.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(3);
		Reporter.addStepLog("User selects Preferred Contact for an Opportunity");

		ActionHandler.click(SVO_OpportunityInvoiceContainer.SAPAccountSearch);
		ActionHandler.setText(SVO_OpportunityInvoiceContainer.SAPAccountSearch, SAPAccount);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVO_OpportunityInvoiceContainer.SAPAccountNameSearch);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityInvoiceContainer.SAPAccount(SAPAccount))));
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects SAP Account for an Opportunity");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeBrand, brand);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeBrandSearch);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(brand))));
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Brand for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeModel, model);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(model))));
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Model for an Opportunity");

		ActionHandler.scrollToView(SVOAdditionalvehicleContainer.SVOBespokeSource);
		ActionHandler.wait(2);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSource);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.SourceSelection(source))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Source for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSaveOpportunity);
		ActionHandler.wait(8);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves SVO Bespoke Opportunity");

		ActionHandler.wait(4);
		OpportunityName = SVOAdditionalvehicleContainer.OpportunityName.getText();
		Reporter.addStepLog("Opportunity Name is = " + OpportunityName);
	}

	@And("^User creates Classic Opportunity with all mandatory fields like Stage \"([^\"]*)\" Record Type \"([^\"]*)\" Region \"([^\"]*)\" Client \"([^\"]*)\" Restricted Party Screening Stage \"([^\"]*)\" Preferred Contact \"([^\"]*)\" SAP Account \"([^\"]*)\" Account Name \"([^\"]*)\" Retailer Contact \"([^\"]*)\" Source \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\"$")
	public void User_creates_an_Classic_Opportunity_with_mandatory_details(String stage, String recordType,
			String region, String client, String Checkcleared, String preferredContact, String SAPAccount,
			String accountName, String retailerContact, String source, String brand, String model) throws Exception {
		String oppName = "Test_Classic_Service_Opportunity_";
		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		String closeDate = CommonFunctions.selectFutureDate();
		Reporter.addStepLog("Opportunity Close Date is = " + closeDate);

		String s[] = stage.split(",");
		stage = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String RT[] = recordType.split(",");
		recordType = CommonFunctions.readExcelMasterData(RT[0], RT[1], RT[2]);

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String c[] = client.split(",");
		client = CommonFunctions.readExcelMasterData(c[0], c[1], c[2]);

		String RPS[] = Checkcleared.split(",");
		Checkcleared = CommonFunctions.readExcelMasterData(RPS[0], RPS[1], RPS[2]);

		String preCon[] = preferredContact.split(",");
		preferredContact = CommonFunctions.readExcelMasterData(preCon[0], preCon[1], preCon[2]);

		String SAP[] = SAPAccount.split(",");
		SAPAccount = CommonFunctions.readExcelMasterData(SAP[0], SAP[1], SAP[2]);

		String accName[] = accountName.split(",");
		accountName = CommonFunctions.readExcelMasterData(accName[0], accName[1], accName[2]);

		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		String so[] = source.split(",");
		source = CommonFunctions.readExcelMasterData(so[0], so[1], so[2]);

		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String m[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		ActionHandler.wait(3);
		Reporter.addStepLog("User starts creating new SVO Classic Service Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.NewOpportunity);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.QuoteNextBtn)) {
			ActionHandler.click(driver
					.findElement(By.xpath(SVO_OpportunitySecondaryOwnerContainer.RecordTypeSelection(recordType))));
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects opportunity record type as Classic Service");

			ActionHandler.click(SVOItemToapproveContainer.QuoteNextBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User clicks on next button");

		}

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeOppName, oppName + randomNumber);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Name");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeClosedDate, closeDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Closed date");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeStage);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.StageSelection(stage))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Stage for an Opportunity");

		ActionHandler.scrollToView(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.RegionSelection(region))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Region for an Opportunity");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeAccountName, accountName);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeAccountNameSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(accountName))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Account Name for an Opportunity");

		ActionHandler.scrollToView(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		VerifyHandler.verifyElementPresent(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		CommonFunctions.attachScreenshot();
		ActionHandler
				.click(driver.findElement(By.xpath(RestrictedPartyScreeningContainer.ValueSelection(Checkcleared))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects restricted party screening as Check cleared for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokePreferredContact, preferredContact);
		CommonFunctions.attachScreenshot();
		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act1.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(3);
		Reporter.addStepLog("User selects Preferred Contact for an Opportunity");

		ActionHandler.click(SVO_OpportunityInvoiceContainer.SAPAccountSearch);
		ActionHandler.setText(SVO_OpportunityInvoiceContainer.SAPAccountSearch, SAPAccount);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVO_OpportunityInvoiceContainer.SAPAccountNameSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityInvoiceContainer.SAPAccount(SAPAccount))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects SAP Account for an Opportunity");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeBrand, brand);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeBrandSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(brand))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Brand for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeModel, model);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(model))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Model for an Opportunity");

		ActionHandler.scrollToView(SVOAdditionalvehicleContainer.SVOBespokeSource);
		ActionHandler.wait(2);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSource);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.SourceSelection(source))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Source for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSaveOpportunity);
		ActionHandler.wait(8);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves SVO Bespoke Opportunity");

		OpportunityName = SVOAdditionalvehicleContainer.OpportunityName.getText();
		Reporter.addStepLog("Opportunity Name is = " + OpportunityName);
	}

	// Verify that opportunity is in Qualified stage
	@Then("^Verify that opportunity is in Qualified stage$")
	public void verify_that_opportunity_is_in_Qualified_stage() throws Throwable {

		VerifyHandler.verifyElementPresent(SVO_OpportunitySecondaryOwnerContainer.OpportunityQualifiedStage);
		String OpportunityStatusField = SVO_OpportunitySecondaryOwnerContainer.OpportunityQualifiedStage.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"User verifies the Opportunity status displayed as " + OpportunityStatusField + " on Opportunity page");
	}

	@Then("^User adds classic user \"([^\"]*)\" as secondary owner for an classic opportunity$")
	public void user_adds_classic_user_as_secondary_owner_for_an_classic_opportunity(String secondryowner)
			throws Throwable {

		String SO[] = secondryowner.split(",");
		secondryowner = CommonFunctions.readExcelMasterData(SO[0], SO[1], SO[2]);

		ActionHandler.pageDown();
		ActionHandler.wait(3);

		javaScriptUtil.clickElementByJS(SVO_OpportunitySecondaryOwnerContainer.EditClassicOpportunityBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on edit secondary owner button");
		ActionHandler.wait(3);

		if (VerifyHandler.verifyElementPresent(SVO_OpportunitySecondaryOwnerContainer.ClearSecondaryOwner)) {
			ActionHandler.scrollToView(SVO_OpportunitySecondaryOwnerContainer.ClearSecondaryOwner);
			ActionHandler.wait(2);
			javaScriptUtil.clickElementByJS(SVO_OpportunitySecondaryOwnerContainer.ClearSecondaryOwner);
		}
		ActionHandler.click(SVO_OpportunitySecondaryOwnerContainer.SecondaryOwnerSearchBox);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVO_OpportunitySecondaryOwnerContainer.SecondaryOwnerSearchBox, secondryowner);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(4);
		ActionHandler.click(SVO_OpportunitySecondaryOwnerContainer.SecondaryOwnerNameSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver
				.findElement(By.xpath(SVO_OpportunitySecondaryOwnerContainer.SecndaryOwnerSelection(secondryowner))));
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects secondary owner for an Opportunity");

		ActionHandler.click(SVO_OpportunitySecondaryOwnerContainer.SaveBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves an opportunity");

	}

	@Then("^User views classic opportunity under secondary opportunities list$")
	public void user_views_classic_opportunity_under_secondary_opportunities_list() throws Throwable {

		ActionHandler.wait(3);
		javaScriptUtil.clickElementByJS(SVO_Pricing_LogicContainer.OpportunityDropdown);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);

		javaScriptUtil.clickElementByJS(SVO_OpportunitySecondaryOwnerContainer.SecondaryOpportunities);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Reporter.addStepLog("User selects Secondary Opportunities in the list");

		ActionHandler.setText(SVOAdditionalvehicleContainer.OpportunitySearchBox, OpportunityName);
		Actions act5 = new Actions(driver);
		act5.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstOpportunityLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User view details of classic opportunity");
	}

	@Then("^User select any existing classic opportunity$")
	public void user_select_any_existing_classic_opportunity() throws Throwable {

		ActionHandler.setText(SVOAdditionalvehicleContainer.OpportunitySearchBox, "Qualified");
		ActionHandler.wait(3);
		ActionHandler.pressEnter();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstOpportunityLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User view details of classic opportunity");
	}

	@Then("^Verify that updated secondary owner field on opportunity page$")
	public void verify_that_updated_secondary_owner_field_on_opportunity_page() throws Throwable {

		VerifyHandler.verifyElementPresent(SVO_OpportunitySecondaryOwnerContainer.SecondaryOwnerNameField);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verify that updated secondary owner name on opportunity page");

	}

	@Then("^User edit classic user \"([^\"]*)\" as secondary owner for an classic opportunity$")
	public void user_edit_classic_user_as_secondary_owner_for_an_classic_opportunity(String secondryowner)
			throws Throwable {

		String SO[] = secondryowner.split(",");
		secondryowner = CommonFunctions.readExcelMasterData(SO[0], SO[1], SO[2]);

		ActionHandler.wait(5);
		javaScriptUtil.clickElementByJS(SVO_OpportunitySecondaryOwnerContainer.EditClassicOpportunityBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on edit secondary owner button");
		ActionHandler.wait(3);

		if (VerifyHandler.verifyElementPresent(SVO_OpportunitySecondaryOwnerContainer.ClearSecondaryOwner)) {
			ActionHandler.scrollToView(SVO_OpportunitySecondaryOwnerContainer.ClearSecondaryOwner);
			ActionHandler.wait(2);
			javaScriptUtil.clickElementByJS(SVO_OpportunitySecondaryOwnerContainer.ClearSecondaryOwner);

			ActionHandler.click(SVO_OpportunitySecondaryOwnerContainer.SecondaryOwnerSearchBox);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVO_OpportunitySecondaryOwnerContainer.SecondaryOwnerSearchBox, secondryowner);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVO_OpportunitySecondaryOwnerContainer.SecondaryOwnerNameSearch);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(
					By.xpath(SVO_OpportunitySecondaryOwnerContainer.SecndaryOwnerSelection(secondryowner))));
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects secondary owner for an Opportunity");

			ActionHandler.click(SVO_OpportunitySecondaryOwnerContainer.SaveBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User saves an opportunity");
		} else {
			ActionHandler.click(SVO_OpportunitySecondaryOwnerContainer.SecondaryOwnerSearchBox);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVO_OpportunitySecondaryOwnerContainer.SecondaryOwnerSearchBox, secondryowner);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVO_OpportunitySecondaryOwnerContainer.SecondaryOwnerNameSearch);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(
					By.xpath(SVO_OpportunitySecondaryOwnerContainer.SecndaryOwnerSelection(secondryowner))));
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects secondary owner for an Opportunity");

			ActionHandler.click(SVO_OpportunitySecondaryOwnerContainer.SaveBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User saves an opportunity");
		}

	}

	@Then("^User edit opportunity details for classic opportunity$")
	public void user_edit_opportunity_details_for_classic_opportunity() throws Throwable {

		String oppName = "Test_Classic_Service_Opportunity_";
		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		String closeDate = CommonFunctions.selectFutureDate();
		Reporter.addStepLog("Opportunity Close Date is = " + closeDate);

		ActionHandler.wait(100);
		driver.navigate().refresh();
		ActionHandler.wait(7);
		javaScriptUtil.clickElementByJS(SVO_OpportunitySecondaryOwnerContainer.EditOpportunityBtn);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeOppName);
		ActionHandler.wait(2);
		ActionHandler.clearAndSetText(SVOAdditionalvehicleContainer.SVOBespokeOppName, oppName + randomNumber);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Name");

		ActionHandler.clearAndSetText(SVOAdditionalvehicleContainer.SVOBespokeClosedDate, closeDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Closed date");

		ActionHandler.click(SVO_OpportunitySecondaryOwnerContainer.SaveOpportunityBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves classic Opportunity");

	}

	@Then("^Verify that updated details on opportunity page$")
	public void verify_that_updated_details_on_opportunity_page() throws Throwable {

		VerifyHandler.verifyElementPresent(SVO_OpportunitySecondaryOwnerContainer.OpportunityName);
		UpdatedOpportunityName = SVO_OpportunitySecondaryOwnerContainer.OpportunityName.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the updated Opportunity details on Opportunity page");

	}

	@Then("^User remove classic user under secondary owner field for an opportunity$")
	public void user_remove_classic_user_under_secondary_owner_field_for_an_opportunity() throws Throwable {

		ActionHandler.wait(5);
		javaScriptUtil.clickElementByJS(SVO_OpportunitySecondaryOwnerContainer.EditClassicOpportunityBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on edit secondary owner button");
		ActionHandler.wait(3);

		ActionHandler.scrollToView(SVO_OpportunitySecondaryOwnerContainer.ClearSecondaryOwner);
		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(SVO_OpportunitySecondaryOwnerContainer.ClearSecondaryOwner);
		Reporter.addStepLog("User removes secondary owner from opportunity");

		ActionHandler.click(SVO_OpportunitySecondaryOwnerContainer.SaveBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves an opportunity");
	}

	@Then("^Verify that the opportunity is not present on opportunity page for logged in user$")
	public void verify_that_the_opportunity_is_not_present_on_opportunity_page_for_logged_in_user() throws Throwable {

		javaScriptUtil.clickElementByJS(SVO_OpportunityContainer.OpportTab);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(SVO_Pricing_LogicContainer.OpportunityDropdown);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);

		javaScriptUtil.clickElementByJS(SVO_OpportunitySecondaryOwnerContainer.SecondaryOpportunities);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Reporter.addStepLog("User selects Secondary Opportunities in the list");

		ActionHandler.setText(SVOAdditionalvehicleContainer.OpportunitySearchBox, OpportunityName);
		Actions act5 = new Actions(driver);
		act5.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"User is not able to view the classic opportunity for which the logged in user is not either main or secondary owner");
	}

	@Given("^User search and select the same classic opportunity which is created recently$")
	public void User_search_and_select_the_same_classic_opportunity_which_is_created_recently() throws Throwable {

		javaScriptUtil.clickElementByJS(SVO_OpportunityContainer.OpportTab);
		CommonFunctions.attachScreenshot();

		javaScriptUtil.clickElementByJS(SVO_Pricing_LogicContainer.OpportunityDropdown);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);

		javaScriptUtil.clickElementByJS(SVO_OpportunitySecondaryOwnerContainer.AllClassicOpportunities);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Reporter.addStepLog("User selects All Opportunities in the list");

		ActionHandler.setText(SVOAdditionalvehicleContainer.OpportunitySearchBox, OpportunityName);
		ActionHandler.wait(3);
		ActionHandler.pressEnter();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstOpportunityLink);
		CommonFunctions.attachScreenshot();

	}

	@Then("^User delete the selected classic opportunity$")
	public void user_delete_the_selected_classic_opportunity() throws Throwable {

		ActionHandler.click(SVO_OpportunitySecondaryOwnerContainer.OpportunityDeleteBtn);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunitySecondaryOwnerContainer.OpportunityDeleteDoneBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User delete the classic opportunity");
	}

	@Then("^User is not able to view deleted classic opportunity$")
	public void user_is_not_able_to_view_deleted_classic_opportunity() throws Throwable {

		javaScriptUtil.clickElementByJS(SVO_OpportunityContainer.OpportTab);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOAdditionalvehicleContainer.OpportunitySearchBox, OpportunityName);
		CommonFunctions.attachScreenshot();

		ActionHandler.pressEnter();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User is not able to view the classic opportunity which is deleted by main owner");
	}

	@Then("^Verify that secondary owner field is not present for bespoke opportunities$")
	public void verify_that_secondary_owner_field_is_not_present_for_bespoke_opportunities() throws Throwable {

		if (!VerifyHandler.verifyElementPresent(SVO_OpportunitySecondaryOwnerContainer.EditClassicOpportunityBtn)) {
			Reporter.addStepLog("secondary owner field is not present for bespoke opportunities");
		} else {
			Reporter.addStepLog("secondary owner field is present for bespoke opportunities");
		}

	}

	@Then("^Verify that opportunity tab is not present on home page$")
	public void verify_that_opportunity_tab_is_not_present_on_home_page() throws Throwable {

		if (!VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.OpportTab)) {
			Reporter.addStepLog("Verify that opportunity tab is not available on home page of SVO portal");
		} else {
			Reporter.addStepLog("Verify that opportunity tab is available on home page of SVO portal");
		}

	}

	@Then("^User not able to add non classic user \"([^\"]*)\" as secondary owner for an classic opportunity$")
	public void user_not_able_to_add_non_classic_user_as_secondary_owner_for_an_classic_opportunity(
			String secondryowner) throws Throwable {
		String SO[] = secondryowner.split(",");
		secondryowner = CommonFunctions.readExcelMasterData(SO[0], SO[1], SO[2]);

		ActionHandler.pageDown();
		ActionHandler.wait(3);

		javaScriptUtil.clickElementByJS(SVO_OpportunitySecondaryOwnerContainer.EditClassicOpportunityBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on edit secondary owner button");
		ActionHandler.wait(3);

		ActionHandler.scrollToView(SVO_OpportunitySecondaryOwnerContainer.SecondaryOwnerSearchBox);
		ActionHandler.click(SVO_OpportunitySecondaryOwnerContainer.SecondaryOwnerSearchBox);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVO_OpportunitySecondaryOwnerContainer.SecondaryOwnerSearchBox, secondryowner);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects secondary owner for an Opportunity");

		ActionHandler.click(SVO_OpportunitySecondaryOwnerContainer.SaveBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves an opportunity");

		VerifyHandler.verifyElementPresent(SVO_OpportunitySecondaryOwnerContainer.ErrorMEssageForBespokeUser);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"User is not able to add any user other than classic user as an Secondary Owner for any classic opportunity");

		ActionHandler.click(SVO_OpportunitySecondaryOwnerContainer.CancelBtn);
		ActionHandler.wait(4);
	}

	// User is not able to view secondary owner for an classic opportunity
	@Then("^User is not able to view secondary owner for an classic opportunity$")
	public void user_is_not_able_to_view_secondary_owner_for_an_classic_opportunity() throws Throwable {

		if (!VerifyHandler.verifyElementPresent(SVO_OpportunitySecondaryOwnerContainer.EditClassicOpportunityBtn)) {
			Reporter.addStepLog("secondary owner field is not present on opportunities page");
		} else {
			Reporter.addStepLog("secondary owner field is present on opportunities page");
		}
	}

	// Verify that the details of classic opportunity
	@Then("^Verify that the details of classic opportunity$")
	public void verify_that_the_details_of_classic_opportunity() throws Throwable {

		VerifyHandler.verifyElementPresent(SVO_OpportunitySecondaryOwnerContainer.ClassicOpportunityDetails);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies that details of classic opportunity");

		javaScriptUtil.clickElementByJS(SVO_CustomerResponsesContainer.EmailsSection);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigates to email section on Opportunity page");

		ActionHandler.pageDown();
		ActionHandler.wait(2);
	}

	@And("^User creates Classic Limitted Edition Opportunity with all mandatory fields like Stage \"([^\"]*)\" Record Type \"([^\"]*)\" Region \"([^\"]*)\" Client \"([^\"]*)\" Restricted Party Screening Stage \"([^\"]*)\" Preferred Contact \"([^\"]*)\" SAP Account \"([^\"]*)\" Account Name \"([^\"]*)\" Retailer Contact \"([^\"]*)\" Source \"([^\"]*)\" Programme \"([^\"]*)\" Model \"([^\"]*)\"$")
	public void User_creates_an_Classic_Limitted_Edition_Opportunity_with_mandatory_details(String stage,
			String recordType, String region, String client, String Checkcleared, String preferredContact,
			String SAPAccount, String accountName, String retailerContact, String source, String Programme,
			String model) throws Exception {
		String oppName = "Test_Classic_Opportunity_";
		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		String closeDate = CommonFunctions.selectFutureDate();
		Reporter.addStepLog("Opportunity Close Date is = " + closeDate);

		String s[] = stage.split(",");
		stage = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String RT[] = recordType.split(",");
		recordType = CommonFunctions.readExcelMasterData(RT[0], RT[1], RT[2]);

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String c[] = client.split(",");
		client = CommonFunctions.readExcelMasterData(c[0], c[1], c[2]);

		String RPS[] = Checkcleared.split(",");
		Checkcleared = CommonFunctions.readExcelMasterData(RPS[0], RPS[1], RPS[2]);

		String preCon[] = preferredContact.split(",");
		preferredContact = CommonFunctions.readExcelMasterData(preCon[0], preCon[1], preCon[2]);

		String SAP[] = SAPAccount.split(",");
		SAPAccount = CommonFunctions.readExcelMasterData(SAP[0], SAP[1], SAP[2]);

		String accName[] = accountName.split(",");
		accountName = CommonFunctions.readExcelMasterData(accName[0], accName[1], accName[2]);

		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		String so[] = source.split(",");
		source = CommonFunctions.readExcelMasterData(so[0], so[1], so[2]);

		String F[] = Programme.split(",");
		Programme = CommonFunctions.readExcelMasterData(F[0], F[1], F[2]);

		String m[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		ActionHandler.wait(3);
		Reporter.addStepLog("User starts creating new SVO Classic Service Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.NewOpportunity);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.QuoteNextBtn)) {
			ActionHandler.click(driver
					.findElement(By.xpath(SVO_OpportunitySecondaryOwnerContainer.RecordTypeSelection(recordType))));
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects opportunity record type as Classic Service");

			ActionHandler.click(SVOItemToapproveContainer.QuoteNextBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User clicks on next button");

		}

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeOppName, oppName + randomNumber);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Name");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeClosedDate, closeDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Closed date");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeStage);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.StageSelection(stage))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Stage for an Opportunity");

		ActionHandler.scrollToView(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.RegionSelection(region))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Region for an Opportunity");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeAccountName, accountName);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeAccountNameSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(accountName))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Account Name for an Opportunity");

		ActionHandler.scrollToView(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		VerifyHandler.verifyElementPresent(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		CommonFunctions.attachScreenshot();
		ActionHandler
				.click(driver.findElement(By.xpath(RestrictedPartyScreeningContainer.ValueSelection(Checkcleared))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects restricted party screening as Check cleared for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokePreferredContact, preferredContact);
		CommonFunctions.attachScreenshot();
		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(2);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		act1.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(3);
		Reporter.addStepLog("User selects Preferred Contact for an Opportunity");

		ActionHandler.click(SVO_OpportunityInvoiceContainer.SAPAccountSearch);
		ActionHandler.setText(SVO_OpportunityInvoiceContainer.SAPAccountSearch, SAPAccount);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVO_OpportunityInvoiceContainer.SAPAccountNameSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityInvoiceContainer.SAPAccount(SAPAccount))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects SAP Account for an Opportunity");

		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquiryProgrammeTextBox);
		ActionHandler.wait(1);
		ActionHandler.setText(BillingAddressOnEnquiryContainer.EnquiryProgrammeTextBox, Programme);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquiryProgrammeSearchbar);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquiryClientSelect);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Programme for an Enquiry as " + Programme);

		ActionHandler.scrollToView(SVOAdditionalvehicleContainer.SVOBespokeSource);
		ActionHandler.wait(2);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSource);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.SourceSelection(source))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Source for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSaveOpportunity);
		ActionHandler.wait(8);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves SVO Bespoke Opportunity");

		OpportunityName = SVOAdditionalvehicleContainer.OpportunityName.getText();
		Reporter.addStepLog("Opportunity Name is = " + OpportunityName);
	}

	@Then("^Move the Opportunity to closed won stage with Outcome as \"([^\"]*)\"$")
	public void move_the_Opportunity_to_closed_won_stage_with_Outcome_as(String Outcome) throws Throwable {
		String OS[] = Outcome.split(",");
		Outcome = CommonFunctions.readExcelMasterData(OS[0], OS[1], OS[2]);

		ActionHandler.wait(5);
		javaScriptUtil.clickElementByJS(SVO_OpportunitySecondaryOwnerContainer.OrderPlacedStage);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on outcome stage");

		ActionHandler.click(SVO_CancelOrderAtAnyStageContainer.MarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(7);
		Reporter.addStepLog("User clicks on Mark status");

		driver.navigate().refresh();
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.OrdersLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Orders link");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstOrder);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens Order details for the opportunity");

		javaScriptUtil.clickElementByJS(SVO_OpportunitySecondaryOwnerContainer.OrderEditBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks edit hand over details button");
		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunitySecondaryOwnerContainer.FullyPaidCheckBox);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Fully Paid CheckBox");

		ActionHandler.click(SVO_OpportunitySecondaryOwnerContainer.HeritageCertificateRequestedCheckBox);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Heritage Certificate Requested CheckBox");

		ActionHandler.click(SVO_OpportunitySecondaryOwnerContainer.FMSPassoutObtainedCheckBox);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on FMS Passout Obtained CheckBox");

		ActionHandler.click(SVO_OpportunitySecondaryOwnerContainer.FMSDisposalCompletedCheckBox);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on FMS Disposal Completed CheckBox");

		ActionHandler.click(SVO_OpportunitySecondaryOwnerContainer.RemovedFromETrackerCheckBox);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Removed From E-Tracker CheckBox");

		ActionHandler.click(SVO_OpportunitySecondaryOwnerContainer.SaveOrderBtn);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);

		javaScriptUtil.clickElementByJS(SVO_CancelOrderAtAnyStageContainer.OrderOutcome);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on outcome stage");

		ActionHandler.click(SVO_CancelOrderAtAnyStageContainer.MarkStatusAsComplete);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Mark status");

		ActionHandler.wait(10);
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDrpDwn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Order outcome stage drop down list");

		ActionHandler.click(
				driver.findElement(By.xpath(SVO_CancelOrderAtAnyStageContainer.OrderOucomeStageSelection(Outcome))));

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Outcome for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDoneBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on done button");
		Reporter.addStepLog("User retail the order on orders page");
	}

	@Then("^Verify that opportunity is in Closed Won stage$")
	public void verify_that_opportunity_is_in_Closed_Won_stage_stage() throws Throwable {

		VerifyHandler.verifyElementPresent(SVO_OpportunitySecondaryOwnerContainer.OpportunityClosedWonStage);
		String OpportunityStatusField = SVO_OpportunitySecondaryOwnerContainer.OpportunityClosedWonStage.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"User verifies the Opportunity status displayed as " + OpportunityStatusField + " on Opportunity page");
	}

	@Then("^Verify that the details of selected classic opportunity$")
	public void verify_that_the_details_of_selected_classic_opportunity() throws Throwable {

		VerifyHandler.verifyElementPresent(SVO_OpportunitySecondaryOwnerContainer.ClassicOpportunityDetails);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies that details of classic opportunity");

	}

	@Then("^User create an Classic Works Legend Opportunity with fields like Stage \"([^\"]*)\" Product Offering \"([^\"]*)\" Region \"([^\"]*)\" Client \"([^\"]*)\" Restricted Party Screening Stage \"([^\"]*)\" Preferred Contact \"([^\"]*)\" Account Name \"([^\"]*)\" Retailer Contact \"([^\"]*)\" Source \"([^\"]*)\" Origination Divission \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\"$")
	public void user_create_an_Classic_Works_Legend_Opportunity_with_fields_like_Stage_Product_Offering_Region_Client_Restricted_Party_Screening_Stage_Preferred_Contact_Account_Name_Retailer_Contact_Source_Origination_Divission_Brand_Model(
			String stage, String productOffering, String region, String client, String Checkcleared,
			String preferredContact, String accountName, String retailerContact, String source,
			String originatingdivission, String brand, String model) throws Exception {
		String oppName = "Classic_WorksLegend_Opportunity_";
		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		String closeDate = CommonFunctions.selectFutureDate();
		Reporter.addStepLog("Opportunity Close Date is = " + closeDate);

		String s[] = stage.split(",");
		stage = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String proOff[] = productOffering.split(",");
		productOffering = CommonFunctions.readExcelMasterData(proOff[0], proOff[1], proOff[2]);

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String c[] = client.split(",");
		client = CommonFunctions.readExcelMasterData(c[0], c[1], c[2]);

		String RPS[] = Checkcleared.split(",");
		Checkcleared = CommonFunctions.readExcelMasterData(RPS[0], RPS[1], RPS[2]);

		String preCon[] = preferredContact.split(",");
		preferredContact = CommonFunctions.readExcelMasterData(preCon[0], preCon[1], preCon[2]);

		String accName[] = accountName.split(",");
		accountName = CommonFunctions.readExcelMasterData(accName[0], accName[1], accName[2]);

		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		String so[] = source.split(",");
		source = CommonFunctions.readExcelMasterData(so[0], so[1], so[2]);

		String OD[] = originatingdivission.split(",");
		originatingdivission = CommonFunctions.readExcelMasterData(OD[0], OD[1], OD[2]);

		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String m[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		Reporter.addStepLog("User starts creating new Classic works legend Opportunity");
		ActionHandler.click(SVOAdditionalvehicleContainer.NewOpportunity);
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.QuoteNextBtn)) {

			ActionHandler.click(AutopoulatedFieldsOnOrderWorkOrder.ClassicWorksLegendRecType);
			ActionHandler.wait(1);
			Reporter.addStepLog("User selects opportunity record type as Classic Works legend");

			ActionHandler.click(SVOItemToapproveContainer.QuoteNextBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User clicks on next button");

		}

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeOppName, oppName + randomNumber);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Name");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeClosedDate, closeDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Closed date");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeStage);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.StageSelection(stage))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Stage for an Opportunity");

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Product Offering for an Opportunity");

		ActionHandler.scrollToView(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		ActionHandler.wait(2);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.RegionSelection(region))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Region for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeAccountName, accountName);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeAccountNameSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(accountName))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Account Name for an Opportunity");

		ActionHandler.scrollToView(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		ActionHandler.wait(2);
		VerifyHandler.verifyElementPresent(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		CommonFunctions.attachScreenshot();
		ActionHandler
				.click(driver.findElement(By.xpath(RestrictedPartyScreeningContainer.ValueSelection(Checkcleared))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects restricted party screening as Check cleared for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokePreferredContact, preferredContact);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(2);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		act1.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(3);
		Reporter.addStepLog("User selects Preferred Contact for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeBrand, brand);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeBrandSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(brand))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Brand for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeModel, model);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(model))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Model for an Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.BodyStyleTextBox);
		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.BodyStyleTextBox, model);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.TestDefenderBodystyle);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects body style for an Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.HandOfDrivedropDown);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.HandOfDriveLHD);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Hand of drive on Opportunity");

		ActionHandler.scrollToView(SVO_WorkOrder_AdditionalOwner_Container.ClassicServiceVehicleRecord);
		ActionHandler.wait(2);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.ClassicServiceVehicleRecord);
		ActionHandler.wait(1);
		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.ClassicServiceVehicleRecord, model);
		// ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.ClassicServiceDefenderVehicle);
		Actions act2 = new Actions(driver);
		act2.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(2);
		act2.sendKeys(Keys.ARROW_DOWN).build().perform();
		act2.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Vehicle record for an Opportunity");

		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.MainVehicleRetailPrice, "250");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enter retail price for main vehicle on Opportunity page");

		ActionHandler.scrollToView(SVO_WorkOrder_AdditionalOwner_Container.VATQualifyingDropdown);
		ActionHandler.wait(2);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.VATQualifyingDropdown);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.VATyesButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses VAT qualifying of an opportunity as yes");

		ActionHandler.wait(2);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSource);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.SourceSelection(source))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Source for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSaveOpportunity);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves Classic Bespoke Opportunity");

		OpportunityName = SVOAdditionalvehicleContainer.OpportunityName.getText();
		Reporter.addStepLog("Opportunity Name is = " + OpportunityName);

	}

	@Then("^User create an Classic Continuation Opportunity with all fields like Stage \"([^\"]*)\" Product Offering \"([^\"]*)\" Region \"([^\"]*)\" Client \"([^\"]*)\" Restricted Party Screening Stage \"([^\"]*)\" Preferred Contact \"([^\"]*)\" Account Name \"([^\"]*)\" Retailer Contact \"([^\"]*)\" Source \"([^\"]*)\" Origination Divission \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\"$")
	public void user_create_an_Classic_Continuation_Opportunity_with_all_fields_like_Stage_Product_Offering_Region_Client_Restricted_Party_Screening_Stage_Preferred_Contact_Account_Name_Retailer_Contact_Source_Origination_Divission_Brand_Model(
			String stage, String productOffering, String region, String client, String Checkcleared,
			String preferredContact, String accountName, String retailerContact, String source,
			String originatingdivission, String brand, String model) throws Exception {
		String oppName = "Classic_Continuation_Opportunity_";
		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		String closeDate = CommonFunctions.selectFutureDate();
		Reporter.addStepLog("Opportunity Close Date is = " + closeDate);

		String s[] = stage.split(",");
		stage = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String proOff[] = productOffering.split(",");
		productOffering = CommonFunctions.readExcelMasterData(proOff[0], proOff[1], proOff[2]);

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String c[] = client.split(",");
		client = CommonFunctions.readExcelMasterData(c[0], c[1], c[2]);

		String RPS[] = Checkcleared.split(",");
		Checkcleared = CommonFunctions.readExcelMasterData(RPS[0], RPS[1], RPS[2]);

		String preCon[] = preferredContact.split(",");
		preferredContact = CommonFunctions.readExcelMasterData(preCon[0], preCon[1], preCon[2]);

		String accName[] = accountName.split(",");
		accountName = CommonFunctions.readExcelMasterData(accName[0], accName[1], accName[2]);

		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		String so[] = source.split(",");
		source = CommonFunctions.readExcelMasterData(so[0], so[1], so[2]);

		String OD[] = originatingdivission.split(",");
		originatingdivission = CommonFunctions.readExcelMasterData(OD[0], OD[1], OD[2]);

		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String m[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		Reporter.addStepLog("User starts creating new Classic Continuation Opportunity");
		ActionHandler.click(SVOAdditionalvehicleContainer.NewOpportunity);
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.QuoteNextBtn)) {

			ActionHandler.click(AutopoulatedFieldsOnOrderWorkOrder.ClassicContinuationRecType);
			ActionHandler.wait(1);
			Reporter.addStepLog("User selects opportunity record type as Classic Continuation");

			ActionHandler.click(SVOItemToapproveContainer.QuoteNextBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User clicks on next button");

		}

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeOppName, oppName + randomNumber);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Name");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeClosedDate, closeDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Closed date");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeStage);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.StageSelection(stage))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Stage for an Opportunity");

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Product Offering for an Opportunity");

		ActionHandler.scrollToView(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		ActionHandler.wait(2);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.RegionSelection(region))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Region for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeAccountName, accountName);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeAccountNameSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(accountName))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Account Name for an Opportunity");

		ActionHandler.scrollToView(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		ActionHandler.wait(2);
		VerifyHandler.verifyElementPresent(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		CommonFunctions.attachScreenshot();
		ActionHandler
				.click(driver.findElement(By.xpath(RestrictedPartyScreeningContainer.ValueSelection(Checkcleared))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects restricted party screening as Check cleared for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokePreferredContact, preferredContact);
		ActionHandler.click(SVOItemToapproveContainer.SVOBespokePreferredContactname);
		Reporter.addStepLog("User selects Preferred Contact for an Opportunity");

		ActionHandler.click(AutopoulatedFieldsOnOrderWorkOrder.ProgrammeFieldTxtBox);
		ActionHandler.wait(1);
		ActionHandler.setText(AutopoulatedFieldsOnOrderWorkOrder.ProgrammeFieldTxtBox, model);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		ActionHandler.click(AutopoulatedFieldsOnOrderWorkOrder.ProgrammeFieldSearchBar);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(AutopoulatedFieldsOnOrderWorkOrder.DefenderProgrammeFieldValue);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Programme for an Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.BodyStyleTextBox);
		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.BodyStyleTextBox, model);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.TestDefenderBodystyle);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects body style for an Opportunity");

		ActionHandler.click(AutopoulatedFieldsOnOrderWorkOrder.BespokeInteriorRequirementTxtBox);
		ActionHandler.wait(1);
		ActionHandler.setText(AutopoulatedFieldsOnOrderWorkOrder.BespokeInteriorRequirementTxtBox, "Test Interior");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters Bespoke Interior Requirements");

		ActionHandler.click(AutopoulatedFieldsOnOrderWorkOrder.BespokeExteriorRequirementTxtBox);
		ActionHandler.wait(1);
		ActionHandler.setText(AutopoulatedFieldsOnOrderWorkOrder.BespokeExteriorRequirementTxtBox, "Test Exterior");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters Bespoke Exterior Requirements");

		ActionHandler.click(AutopoulatedFieldsOnOrderWorkOrder.BespokePaintRequirementTxtBox);
		ActionHandler.wait(1);
		ActionHandler.setText(AutopoulatedFieldsOnOrderWorkOrder.BespokePaintRequirementTxtBox, "Test Paint");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters Bespoke Paint Requirements");

		ActionHandler.wait(2);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.BuildColour);
		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.BuildColour, "Blue");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters build colour on Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.HandOfDrivedropDown);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.HandOfDriveLHD);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Hand of drive on Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.Speedometerdropdown);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.SpeedometerMPH);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Speedometer on Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.BuildColourShade);
		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.BuildColourShade, "brown");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters build colour shade on Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.BuildInteriorColour);
		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.BuildInteriorColour, "White");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters build interior colour on Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.AdditionalSepcDetails);
		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.AdditionalSepcDetails, "Test");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Additional spec details on Opportunity");

		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.MainVehicleRetailPrice, "250");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enter retail price for main vehicle on Opportunity page");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.VATQualifyingDropdown);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.VATyesButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses VAT qualifying of an opportunity as yes");

		ActionHandler.wait(2);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRetailerContact);
		ActionHandler.wait(2);
		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeRetailerContact, retailerContact);
		ActionHandler.click(SVOItemToapproveContainer.SVOBespokePreferredContactname);
		Reporter.addStepLog("User selects Retailer Contact for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSource);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.SourceSelection(source))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Source for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSaveOpportunity);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves Classic Works legend Opportunity");

		OpportunityName = SVOAdditionalvehicleContainer.OpportunityName.getText();
		Reporter.addStepLog("Opportunity Name is = " + OpportunityName);
	}

	@Then("^Verify that opportunity is in Closed Lost stage$")
	public void verify_that_opportunity_is_in_Closed_Lost_stage_stage() throws Throwable {

		ActionHandler.pageDown();
		ActionHandler.wait(3);
		javaScriptUtil.clickElementByJS(SVO_OpportunitySecondaryOwnerContainer.OpportunityLinkOnOrdersPage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates back to opportunity page");

		VerifyHandler.verifyElementPresent(SVO_OpportunitySecondaryOwnerContainer.OpportunityClosedLostStage);
		String OpportunityStatusField = SVO_OpportunitySecondaryOwnerContainer.OpportunityClosedLostStage.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"User verifies the Opportunity status displayed as " + OpportunityStatusField + " on Opportunity page");
	}

	@And("^User creates Classic Continuation Opportunity with all mandatory fields like Stage \"([^\"]*)\" Record Type \"([^\"]*)\" Region \"([^\"]*)\" Client \"([^\"]*)\" Restricted Party Screening Stage \"([^\"]*)\" Preferred Contact \"([^\"]*)\" SAP Account \"([^\"]*)\" Account Name \"([^\"]*)\" Retailer Contact \"([^\"]*)\" Source \"([^\"]*)\" Programme \"([^\"]*)\" Model \"([^\"]*)\"$")
	public void User_creates_an_Classic_Continuation_Opportunity_with_mandatory_details(String stage, String recordType,
			String region, String client, String Checkcleared, String preferredContact, String SAPAccount,
			String accountName, String retailerContact, String source, String Programme, String model)
			throws Exception {
		String oppName = "Test_Classic_Service_Opportunity_";
		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		String closeDate = CommonFunctions.selectFutureDate();
		Reporter.addStepLog("Opportunity Close Date is = " + closeDate);

		String s[] = stage.split(",");
		stage = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String RT[] = recordType.split(",");
		recordType = CommonFunctions.readExcelMasterData(RT[0], RT[1], RT[2]);

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String c[] = client.split(",");
		client = CommonFunctions.readExcelMasterData(c[0], c[1], c[2]);

		String RPS[] = Checkcleared.split(",");
		Checkcleared = CommonFunctions.readExcelMasterData(RPS[0], RPS[1], RPS[2]);

		String preCon[] = preferredContact.split(",");
		preferredContact = CommonFunctions.readExcelMasterData(preCon[0], preCon[1], preCon[2]);

		String SAP[] = SAPAccount.split(",");
		SAPAccount = CommonFunctions.readExcelMasterData(SAP[0], SAP[1], SAP[2]);

		String accName[] = accountName.split(",");
		accountName = CommonFunctions.readExcelMasterData(accName[0], accName[1], accName[2]);

		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		String so[] = source.split(",");
		source = CommonFunctions.readExcelMasterData(so[0], so[1], so[2]);

		String F[] = Programme.split(",");
		Programme = CommonFunctions.readExcelMasterData(F[0], F[1], F[2]);

		String m[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		ActionHandler.wait(3);
		Reporter.addStepLog("User starts creating new SVO Classic Service Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.NewOpportunity);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.QuoteNextBtn)) {
			ActionHandler.click(driver
					.findElement(By.xpath(SVO_OpportunitySecondaryOwnerContainer.RecordTypeSelection(recordType))));
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects opportunity record type as Classic Service");

			ActionHandler.click(SVOItemToapproveContainer.QuoteNextBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User clicks on next button");

		}

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeOppName, oppName + randomNumber);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Name");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeClosedDate, closeDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Closed date");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeStage);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.StageSelection(stage))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Stage for an Opportunity");

		ActionHandler.scrollToView(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.RegionSelection(region))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Region for an Opportunity");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeAccountName, accountName);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeAccountNameSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(accountName))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Account Name for an Opportunity");

		ActionHandler.scrollToView(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		VerifyHandler.verifyElementPresent(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		CommonFunctions.attachScreenshot();
		ActionHandler
				.click(driver.findElement(By.xpath(RestrictedPartyScreeningContainer.ValueSelection(Checkcleared))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects restricted party screening as Check cleared for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokePreferredContact, preferredContact);
		CommonFunctions.attachScreenshot();
		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act1.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(3);
		Reporter.addStepLog("User selects Preferred Contact for an Opportunity");

		ActionHandler.click(SVO_OpportunityInvoiceContainer.SAPAccountSearch);
		ActionHandler.setText(SVO_OpportunityInvoiceContainer.SAPAccountSearch, SAPAccount);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVO_OpportunityInvoiceContainer.SAPAccountNameSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityInvoiceContainer.SAPAccount(SAPAccount))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects SAP Account for an Opportunity");

		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquiryProgrammeTextBox);
		ActionHandler.wait(1);
		ActionHandler.setText(BillingAddressOnEnquiryContainer.EnquiryProgrammeTextBox, Programme);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquiryProgrammeSearchbar);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquiryClientSelect);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Programme for an Enquiry as " + Programme);

		ActionHandler.scrollToView(SVOAdditionalvehicleContainer.SVOBespokeSource);
		ActionHandler.wait(2);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSource);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.SourceSelection(source))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Source for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSaveOpportunity);
		ActionHandler.wait(8);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves SVO Bespoke Opportunity");

		OpportunityName = SVOAdditionalvehicleContainer.OpportunityName.getText();
		Reporter.addStepLog("Opportunity Name is = " + OpportunityName);
	}

	// User navigate back to Opportunities page
	@Then("^User navigate back to Opportunities page$")
	public void user_navigate_back_to_Opportunities_page() throws Throwable {

		javaScriptUtil.clickElementByJS(SVO_OpportunityContainer.OpportTab);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigate to Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.OpportunitySearchBox, OpportunityName);
		Actions act5 = new Actions(driver);
		act5.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstOpportunityLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate back to the opportunity page");

	}

}
