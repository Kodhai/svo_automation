package com.jlr.svo.tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;

import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.containers.SSEC_1227_CasesCreationContainer;
import com.jlr.svo.containers.SSEC_1532_AdditionOfMarketDataToKMIContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SSEC_1532_AddtionOfMarketDataToKMI extends TestBaseCC {

	private WebDriver driver = getDriver();
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SSEC_1227_CasesCreationContainer CasesContainer = PageFactory.initElements(driver,
			SSEC_1227_CasesCreationContainer.class);
	SSEC_1532_AdditionOfMarketDataToKMIContainer AdditionOfMarketDataToKMI = PageFactory.initElements(driver,
			SSEC_1532_AdditionOfMarketDataToKMIContainer.class);

	public static String verificationCode;
	public static String vCode;
	double randomNum = getRandomNumber(0, 10000);
	public static String productOffering;
	String enquirySource;
	String originatingDivision;
	String region;
	String salesArea;
	String market;
	String brand;
	String model;
	String retailer;

	public static double getRandomNumber(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	// onStart
	public void onStart() {
		setupTest("SVOTest");
		driver = getDriver();
		SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		CasesContainer = PageFactory.initElements(driver, SSEC_1227_CasesCreationContainer.class);
		AdditionOfMarketDataToKMI = PageFactory.initElements(driver,
				SSEC_1532_AdditionOfMarketDataToKMIContainer.class);
		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		verificationCode = null;
		driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);

	}

//	@Given("^User Access SVO Portal$")
//	public void access_SVO_Portal() throws Throwable {
//		
//		onStart();
//		driver = getDriver();
//		ActionHandler.wait(13);
//		driver.get(Constants.SVOURL);
//		ActionHandler.wait(15);
//		Reporter.addStepLog("User Logins to SVO Portal");
//
//		String UserName = Config.getPropertyValue("Admin_UserName");
//		String Password = Config.getPropertyValue("Admin_Password");
//
//		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, UserName);
//		ActionHandler.wait(3);
//		CommonFunctions.attachScreenshot();
//
//		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, Password);
//		ActionHandler.wait(3);
//		CommonFunctions.attachScreenshot();
//
//		ActionHandler.click(SVOEnquiryContainer.loginBtn);
//		ActionHandler.wait(7);
//		CommonFunctions.attachScreenshot();
//
//		ActionHandler.click(SVOEnquiryContainer.vCodeTextBox);
//
//		Reporter.addStepLog("User requires to enter Verification code received in an email");
//		ActionHandler.wait(5);
//
//		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {
//			Reporter.addStepLog("Please Open recently received email from Salesforce");
//			ActionHandler.wait(2);
//			ActionHandler.wait(10);
//			vCode = SVOenquiry.checkEmail();
//			System.out.println("Verification code is = " + vCode);
//
//			driver.get(Constants.SVOURL);
//			ActionHandler.wait(15);
//			Reporter.addStepLog("User Logins to SVO Portal");
//
//			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, UserName);
//			ActionHandler.wait(3);
//			CommonFunctions.attachScreenshot();
//
//			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, Password);
//			ActionHandler.wait(3);
//			CommonFunctions.attachScreenshot();
//
//			ActionHandler.click(SVOEnquiryContainer.loginBtn);
//			ActionHandler.wait(7);
//			CommonFunctions.attachScreenshot();
//
//			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
//			ActionHandler.wait(3);
//			CommonFunctions.attachScreenshot();
//
//			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
//			ActionHandler.wait(7);
//			CommonFunctions.attachScreenshot();
//
//		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {
//			ActionHandler.wait(5);
//			Reporter.addStepLog("User asks to enter mobile number");
//			CommonFunctions.attachScreenshot();
//			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {
//				ActionHandler.wait(3);
//				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
//				ActionHandler.wait(15);
//				CommonFunctions.attachScreenshot();
//			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {
//				ActionHandler.wait(3);
//				ActionHandler.click(SVOEnquiryContainer.notRegister);
//				ActionHandler.wait(15);
//				CommonFunctions.attachScreenshot();
//			} else {
//				ActionHandler.wait(15);
//				CommonFunctions.attachScreenshot();
//				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
//				Reporter.addStepLog("User navigate to SVO Home Page successfully");
//			}
//		} else {
//			ActionHandler.wait(15);
//			ActionHandler.wait(120);
//			ActionHandler.waitForElement(SVOEnquiryContainer.SVOText, 90);
//			CommonFunctions.attachScreenshot();
//			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
//			Reporter.addStepLog("User navigate to SVO Home Page successfully");
//		}
//	}
//	

	@When("^User navigates to Enquiries tab$")
	public void user_Navigate_to_Enquiries() throws Throwable {

		ActionHandler.click(SVOEnquiryContainer.enquiryTab);
		ActionHandler.wait(15);

	}

	@Then("^User Creates New Enquiry with Record Type \"([^\"]*)\"$")
	public void User_Create_New_Enquiry(String recordType) throws Exception {
		String rType[] = recordType.split(",");
		recordType = CommonFunctions.readExcelMasterData(rType[0], rType[1], rType[2]);

		ActionHandler.wait(4);

		ActionHandler.click(SVOEnquiryContainer.newEnquiryBtn);
		ActionHandler.wait(10);

		ActionHandler.click(AdditionOfMarketDataToKMI.ClassicSalesBtn);
		// ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.selectRecordType(recordType))));

		ActionHandler.wait(5);

		ActionHandler.click(SVOEnquiryContainer.nextRType);
		ActionHandler.wait(7);

	}

//	@Then("^Enter the Enquiry Title for the Enquiry creation$")
//	public void enter_the_Enquiry_Title_for_the_Enquiry_creation() throws Throwable {
//		String enquiryTitle = "Test_Enquiry_";
//		double randomNumber = getRandomNumber(0, 1000);
//
//		ActionHandler.wait(7);
//		ActionHandler.setText(SVOEnquiryContainer.EnquiryTitle, enquiryTitle + randomNumber);
//		ActionHandler.wait(3);
//		
//	}

	@Then("^User fills required details such as_ Product Offering \"([^\"]*)\" Enquiry Source \"([^\"]*)\" Originating Division \"([^\"]*)\" Region \"([^\"]*)\" Sales Area \"([^\"]*)\" Market \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\" and Retailer \"([^\"]*)\"$")
	public void user_fills_all_the_required_details_like_Product_Offering_Enquiry_Source_Originating_Division_Region_Sales_Area_Market_Brand_Model_and_Retailer()
			throws Throwable {
		String pOffering[] = productOffering.split(",");
		productOffering = CommonFunctions.readExcelMasterData(pOffering[0], pOffering[1], pOffering[2]);

		String eSource[] = enquirySource.split(",");
		enquirySource = CommonFunctions.readExcelMasterData(eSource[0], eSource[1], eSource[2]);

		String oDivision[] = originatingDivision.split(",");
		originatingDivision = CommonFunctions.readExcelMasterData(oDivision[0], oDivision[1], oDivision[2]);

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String sArea[] = salesArea.split(",");
		salesArea = CommonFunctions.readExcelMasterData(sArea[0], sArea[1], sArea[2]);

		String m[] = market.split(",");
		market = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String mo[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(mo[0], mo[1], mo[2]);

		String re[] = retailer.split(",");
		retailer = CommonFunctions.readExcelMasterData(re[0], re[1], re[2]);
		System.out.println("All details are fetched from excel");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.Showkeyfield)) {
			ActionHandler.wait(3);
			ActionHandler.click(SVOEnquiryContainer.Showkeyfield);
			Reporter.addStepLog("Click on show more");
			CommonFunctions.attachScreenshot();
		}

		ActionHandler.click(SVOEnquiryContainer.Vehicleinfoedit);
		ActionHandler.wait(3);
		Reporter.addStepLog("Click on edit button");
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.brand, brand);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOEnquiryContainer.brandSearch);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.brandSearch(brand))));
		ActionHandler.wait(3);
		Reporter.addStepLog("Enter brand information");
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.model, model);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOEnquiryContainer.modelSearch);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.brandSearch(model))));
		ActionHandler.wait(3);
		Reporter.addStepLog("Enter model information");
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(1);
		ActionHandler.click(SVOEnquiryContainer.proOffering);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.SourceSelection(productOffering))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Input product offering information");

		ActionHandler.click(SVOEnquiryContainer.Savechanges);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("click on save");

		ActionHandler.pageScrollDown();
		ActionHandler.pageArrowUp();
		ActionHandler.pageArrowUp();

		Reporter.addStepLog("Navigate to Details tab");
		ActionHandler.click(SVOEnquiryContainer.enqDetails);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		// ActionHandler.pageScrollDown();
		ActionHandler.wait(3);
		// javaScriptUtil.scrollIntoView(SVOEnquiryContainer.editRegion);
		ActionHandler.wait(5);
		ActionHandler.click(SVOEnquiryContainer.editRegion);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		// ActionHandler.scrollToView(SVOEnquiryContainer.ViewallDependencies);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.ViewallDependencies)) {
			ActionHandler.wait(3);
			ActionHandler.click(SVOEnquiryContainer.editRegion);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
		}

		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.ViewallDependencies);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.regionDD)) {
			ActionHandler.wait(2);
			ActionHandler.pageDown();
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(2);
			ActionHandler.click(SVOEnquiryContainer.ViewallDependencies);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
		}

		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.regionDD);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.ValueSelection(region))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter Region information");

		ActionHandler.click(SVOEnquiryContainer.BespokesalesAreaDD);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.ValueSelection(salesArea))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter Sales Area information");

		ActionHandler.click(SVOEnquiryContainer.marketDD);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.ValueSelection(market))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter market information");
		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.Applybtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click Apply");

		// ActionHandler.wait(3);
		// javaScriptUtil.scrollIntoView(SVOEnquiryContainer.retailer);
		ActionHandler.wait(3);
		ActionHandler.setText(SVOEnquiryContainer.retailer, retailer);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Input client information");

		ActionHandler.pageUp();
		ActionHandler.wait(3);
		ActionHandler.click(SVOEnquiryContainer.ViewallDependencies);
		ActionHandler.wait(5);
		ActionHandler.click(SVOEnquiryContainer.ViewallDependencies);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOEnquiryContainer.Applybtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.scrollDown();
		ActionHandler.pageDown();
		ActionHandler.wait(3);
		ActionHandler.pageDown();
		ActionHandler.wait(3);
		ActionHandler.pageDown();
		ActionHandler.wait(3);
		Reporter.addStepLog("Navigate to Source block");

		ActionHandler.wait(4);
		ActionHandler.click(SVOEnquiryContainer.originatingDiv);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.ValueSelection(originatingDivision))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Input Origination information");

		ActionHandler.click(SVOEnquiryContainer.BespokesourceDD);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.ValueSelection(enquirySource))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Input source information");

		ActionHandler.click(SVOEnquiryContainer.saveEmailBtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Save the changes");

	}

//	@When("^Save the enquiry$")
//	public void save_the_enquiry() throws Throwable {
//		
//		ActionHandler.click(SVOEnquiryContainer.SaveEnquiry);
//		ActionHandler.wait(15);
//		
//	}
//	

	@Then("^User logs a call to an Activity tab$")
	public void User_logs_call_to_activity_tab() throws Exception {

		ActionHandler.scrollToView(AdditionOfMarketDataToKMI.ActivityTab);
		ActionHandler.wait(5);

		ActionHandler.click(AdditionOfMarketDataToKMI.AddBtn);
		ActionHandler.wait(3);

		ActionHandler.click(AdditionOfMarketDataToKMI.SubjectText);
		ActionHandler.wait(2);

		ActionHandler.setText(AdditionOfMarketDataToKMI.callActivityText, "Call");
		ActionHandler.wait(5);

		ActionHandler.click(AdditionOfMarketDataToKMI.ActivitySaveBtn);
		ActionHandler.wait(8);

	}

	@Then("^From Closed Tab, click on Mark Current Enquiry Status as Close and State as Qualified-KMI$")
	public void Mark_As_Close() throws Exception {

		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(3);
		ActionHandler.pageUp();
		ActionHandler.wait(5);

		ActionHandler.click(AdditionOfMarketDataToKMI.ClosedTab);
		ActionHandler.wait(2);

		ActionHandler.click(SVOEnquiryContainer.MarkEnquiryComplete);
		ActionHandler.wait(7);

		ActionHandler.click(SVOEnquiryContainer.closedState);
		ActionHandler.wait(5);

		ActionHandler.click(AdditionOfMarketDataToKMI.qualifiedKMIBtn);
		ActionHandler.wait(3);

		ActionHandler.click(SVOEnquiryContainer.doneClosedState);
		ActionHandler.wait(10);

	}

	@Then("^User verifies that new KMI record gets created$")
	public void User_verifies_new_KMI_created() throws Exception {

		ActionHandler.pageDown();
		ActionHandler.wait(3);
		ActionHandler.click(AdditionOfMarketDataToKMI.LinkKMI);
		ActionHandler.wait(5);
		ActionHandler.click(AdditionOfMarketDataToKMI.KMIrecord);
		ActionHandler.wait(5);

	}

	@Then("^User verifies that Region, Sales Area and Market values are added to newly created KMI$")
	public void User_verifies_Region_Sales_Market_are_added() throws Exception {

		String Region = driver.findElement(By.xpath("//span[text()='Region']")).getText();
		String salesarea = driver.findElement(By.xpath("//span[text()='Sales Area']")).getText();
		String Market = driver.findElement(By.xpath("//span[text()='Market']")).getText();

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String sArea[] = salesArea.split(",");
		salesArea = CommonFunctions.readExcelMasterData(sArea[0], sArea[1], sArea[2]);

		String m[] = market.split(",");
		market = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		if (region.equalsIgnoreCase(Region)) {
			System.out.println("Matches!!");
		} else {
			System.out.println("Not Matches!!");
		}

		if (salesArea.equalsIgnoreCase(salesarea)) {
			System.out.println("Matches!!");
		} else {
			System.out.println("Not Matches!!");
		}

		if (market.equalsIgnoreCase(Market)) {
			System.out.println("Matches!!");
		} else {
			System.out.println("Not Matches!!");
		}

	}

	@Then("^User fills required details such as Region \"([^\"]*)\" Sales Area \"([^\"]*)\" Market \"([^\"]*)\"$")
	public void user_fills_details() throws Exception {

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String sArea[] = salesArea.split(",");
		salesArea = CommonFunctions.readExcelMasterData(sArea[0], sArea[1], sArea[2]);

		String m[] = market.split(",");
		market = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.regionDD);
		ActionHandler.wait(3);
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.ValueSelection(region))));
		ActionHandler.wait(3);

		ActionHandler.click(SVOEnquiryContainer.BespokesalesAreaDD);
		ActionHandler.wait(3);
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.ValueSelection(salesArea))));
		ActionHandler.wait(3);

		ActionHandler.click(SVOEnquiryContainer.marketDD);
		ActionHandler.wait(3);
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.ValueSelection(market))));
		ActionHandler.wait(3);

	}

	@Then("^User marks the current enquiry state$")
	public void user_marks_enquiry_state() throws Exception {

		ActionHandler.click(AdditionOfMarketDataToKMI.ClosedTab);
		ActionHandler.wait(2);

		ActionHandler.click(SVOEnquiryContainer.MarkEnquiryComplete);
		ActionHandler.wait(7);

		ActionHandler.click(SVOEnquiryContainer.closedState);
		ActionHandler.wait(5);

	}

	@And("^User verifies that Qualified-KMI is not available in Closed State$")
	public void user_verifies_QualifiedKMI_not_available() throws Exception {

		String ClosedState = driver.findElement(By.xpath("//button[@name='SO_Closed_State__c']")).getText();

		if (ClosedState.equalsIgnoreCase("Qualified - KMI")) {
			System.out.println("Present!!! Case failed");
		} else {
			System.out.println("Not Present!!! Case Passed");
		}
	}

	@And("^User verifies that no KMI is created$")
	public void user_verifies_no_KMI_created() throws Exception {

		boolean find = driver.getPageSource().contains("KMI");
		if (find) {
			System.out.println("Found KMI, Case Failed");
		} else {
			System.out.println("Not Found, Case Passed!!!");
		}

	}

	@Then("^User verifies that no new KMI record gets created$")
	public void user_verifies_no_KMI_record_created() throws Exception {

		ActionHandler.pageDown();
		ActionHandler.wait(3);
		ActionHandler.click(AdditionOfMarketDataToKMI.LinkKMI);
		ActionHandler.wait(5);

		String noItems = driver.findElement(By.xpath("//span[text()='No items to display.']")).getText();

		if (noItems.equalsIgnoreCase("No items to display.")) {
			System.out.println("No KMI record is created, Case Passed!!");
		} else {
			System.out.println("KMI record present, Case Failed!!");
		}
	}

	@And("^User fills Preferred Contact \"([^\"]*)\"$")
	public void User_fills_Preferred_Contact(String pContact) throws Throwable {

		String C[] = pContact.split(",");
		pContact = CommonFunctions.readExcelMasterData(C[0], C[1], C[2]);

		ActionHandler.pageDown();
		ActionHandler.click(AdditionOfMarketDataToKMI.PreferredContactBox);
		ActionHandler.wait(5);
		ActionHandler.setText(AdditionOfMarketDataToKMI.SearchContactBox, pContact);
		ActionHandler.wait(5);
		ActionHandler.click(AdditionOfMarketDataToKMI.SearchElement);
		ActionHandler.wait(5);

	}

	@Then("^From Closed Tab click on Mark Current Enquiry Status as Close and State as Qualified - Opportunity$")
	public void User_closes_with_Qualified_Opportunity() throws Throwable {

		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(3);
		ActionHandler.pageUp();
		ActionHandler.wait(5);

		ActionHandler.click(AdditionOfMarketDataToKMI.ClosedTab);
		ActionHandler.wait(2);

		ActionHandler.click(SVOEnquiryContainer.MarkEnquiryComplete);
		ActionHandler.wait(7);

		ActionHandler.click(SVOEnquiryContainer.closedState);
		ActionHandler.wait(5);

		ActionHandler.click(AdditionOfMarketDataToKMI.qualifiedOpportBtn);
		ActionHandler.wait(3);

		ActionHandler.click(SVOEnquiryContainer.doneClosedState);
		ActionHandler.wait(10);
	}

	@Then("^User verifies that no new KMI record gets created and Region, Sales Area and Market values are not added$")
	public void user_verifies_no_KMI_record_and_details_created() throws Exception {

		ActionHandler.pageDown();
		ActionHandler.wait(3);
		ActionHandler.click(AdditionOfMarketDataToKMI.LinkKMI);
		ActionHandler.wait(5);

		String noItems = driver.findElement(By.xpath("//span[text()='No items to display.']")).getText();

		if (noItems.equalsIgnoreCase("No items to display.")) {
			System.out.println("No KMI record is created, Case Passed!!");
		} else {
			System.out.println("KMI record present, Case Failed!!");
		}

	}

	@Then("^User updates Region, Sales Area and Market values in closed Classic Sales Enquiry$")
	public void User_updates_details() throws Throwable {

		ActionHandler.pageDown();
		ActionHandler.pageScrollDown();
		ActionHandler.click(SVOEnquiryContainer.editRegion);
		ActionHandler.click(AdditionOfMarketDataToKMI.editRegionChina);
		ActionHandler.wait(5);
		ActionHandler.click(AdditionOfMarketDataToKMI.editSalesArea);
		ActionHandler.wait(5);
		ActionHandler.click(AdditionOfMarketDataToKMI.editMarketChina);
		ActionHandler.wait(5);
		ActionHandler.click(AdditionOfMarketDataToKMI.SaveEditBtn);
		ActionHandler.wait(8);
	}

	@Then("^User validates that Region, Sales Area and Market values are updated in KMI record$")
	public void User_verifies_updated_Region_Sales_Market_are_added() throws Exception {

		String Region = driver.findElement(By.xpath("//span[text()='Region']")).getText();
		String salesarea = driver.findElement(By.xpath("//span[text()='Sales Area']")).getText();
		String Market = driver.findElement(By.xpath("//span[text()='Market']")).getText();

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String sArea[] = salesArea.split(",");
		salesArea = CommonFunctions.readExcelMasterData(sArea[0], sArea[1], sArea[2]);

		String m[] = market.split(",");
		market = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		if (region.equalsIgnoreCase(Region)) {
			System.out.println("Matches!!");
		} else {
			System.out.println("Not Matches!!");
		}

		if (salesArea.equalsIgnoreCase(salesarea)) {
			System.out.println("Matches!!");
		} else {
			System.out.println("Not Matches!!");
		}

		if (market.equalsIgnoreCase(Market)) {
			System.out.println("Matches!!");
		} else {
			System.out.println("Not Matches!!");
		}

	}

//    public String checkEmailForClassicAdminUser() throws Exception
//	{
//		String parentWindow = driver.getWindowHandle();
//		System.out.println("Window Id for SF SVO is = " + parentWindow);
//
//		String userName = Config.getPropertyValue("Outlook_UserName");
//		String password = Config.getPropertyValue("Outlook_Password");
//
//		driver.get(Constants.outlookURL);
//
//		Reporter.addStepLog("User need to access Outlook account to get verification code");
//		CommonFunctions.attachScreenshot();
//
//		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {
//
//			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
//			CommonFunctions.attachScreenshot();
//
//			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
//			CommonFunctions.attachScreenshot();
//
//			ActionHandler.click(SVOEnquiryContainer.outlookNext);
//			CommonFunctions.attachScreenshot();
//
//			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
//			CommonFunctions.attachScreenshot();
//
//			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
//			CommonFunctions.attachScreenshot();
//
//			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
//			CommonFunctions.attachScreenshot();
//		}
//
//		String outlookWindow = driver.getWindowHandle();
//		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
//		ActionHandler.wait(12);
//		Set<String> outlookWindows = driver.getWindowHandles();
//		Iterator<String> i = outlookWindows.iterator();
//		while (i.hasNext()) {
//			String mailWindow = i.next();
//			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
//				driver.switchTo().window(mailWindow);
//				ActionHandler.click(SVOEnquiryContainer.outlookEmail);
//
//				CommonFunctions.attachScreenshot();
//				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
//				vCode = verificationCode.substring(378, 384);
//				System.out.println("Verification Code is = " + vCode);
//				driver.close();
//				driver.switchTo().window(outlookWindow);
//			}
//		}
//		CommonFunctions.attachScreenshot();
//		return vCode;
//	}
//	
//
//	@Given("^Access to SVO Portal as Classic Ad user with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
//	public void Access_SVO_Portal_Classic_ad_with_username_password(String userName, String password) throws Exception
//	{
//		String un[] = userName.split(",");
//		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);
//
//		String pass[] = password.split(",");
//		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);
//
//		Reporter.addStepLog("User access SF SVO Portal");
//
//		onStart();
//		driver = getDriver();
//		driver.get(Constants.SVOURL);
//
//		Reporter.addStepLog("User Logins to SVO Portal");
//		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
//		CommonFunctions.attachScreenshot();
//
//		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
//		CommonFunctions.attachScreenshot();
//
//		ActionHandler.click(SVOEnquiryContainer.loginBtn);
//		CommonFunctions.attachScreenshot();
//		Reporter.addStepLog("User requires to enter Verification code received in an email");
//
//		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {
//
//			Reporter.addStepLog("Please Open recently received email from Salesforce");
//
//			vCode = checkEmailForClassicAdminUser();
//			System.out.println("Verification code is = " + vCode);
//			
//			driver.get(Constants.SVOURL);
//			Reporter.addStepLog("User Logins to SVO Portal");
//
//			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
//			CommonFunctions.attachScreenshot();
//
//			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
//			CommonFunctions.attachScreenshot();
//
//			ActionHandler.click(SVOEnquiryContainer.loginBtn);
//			CommonFunctions.attachScreenshot();
//			
//			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
//			CommonFunctions.attachScreenshot();
//
//			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
//			CommonFunctions.attachScreenshot();
//			Reporter.addStepLog("User logins to SVO Successfully");
//
//		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {
//
//			Reporter.addStepLog("User asks to enter mobile number");
//			CommonFunctions.attachScreenshot();
//			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {
//
//				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
//				ActionHandler.wait(15);
//				CommonFunctions.attachScreenshot();
//			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {
//
//				ActionHandler.click(SVOEnquiryContainer.notRegister);
//				ActionHandler.wait(15);
//				CommonFunctions.attachScreenshot();
//			} else {
//				ActionHandler.wait(15);
//				CommonFunctions.attachScreenshot();
//				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
//				Reporter.addStepLog("User navigate to SVO Home Page successfully");
//			}
//		} else {
//			ActionHandler.wait(15);
//			CommonFunctions.attachScreenshot();
//
//			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
//				SVOenquiry.navigateToSVO();
//
//			}
//			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
//			Reporter.addStepLog("User navigate to SVO Home Page successfully");
//		}
//	}

	@Then("^User deletes the enquiry created$")
	public void User_deletes_enquiry_created() throws Exception {

		ActionHandler.click(AdditionOfMarketDataToKMI.EnquiriesTab);
		ActionHandler.wait(5);
		ActionHandler.click(AdditionOfMarketDataToKMI.EnquiryDD);
		ActionHandler.wait(5);
		ActionHandler.click(AdditionOfMarketDataToKMI.DeleteEnquiryOpt);
		ActionHandler.wait(5);
		ActionHandler.click(AdditionOfMarketDataToKMI.deleteEnqBtn);
		ActionHandler.wait(10);
	}

	@And("^User checks Region, Sales Area and Market values into KMI record$")
	public void User_checks_details_into_KMI() throws Exception {

		ActionHandler.click(AdditionOfMarketDataToKMI.KMITab);
		ActionHandler.wait(5);

		ActionHandler.click(AdditionOfMarketDataToKMI.KMIrecord);
		ActionHandler.wait(5);

		WebElement SourceEnqBox = driver.findElement(By.xpath("//span[text()='Source Enquiry']"));
		String textInsideSourceEnqBox = SourceEnqBox.getAttribute("KMI");

		if (textInsideSourceEnqBox.isEmpty()) {
			System.out.println("Enquiry is deleted, Case Passed");
		} else {
			System.out.println("Enquiry is present, Case not passed");
		}

	}

}
