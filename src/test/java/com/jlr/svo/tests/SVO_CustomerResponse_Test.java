package com.jlr.svo.tests;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.autotest.utils.Config;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.constants.Constants;
import com.jlr.svo.containers.SVOAccountsContainer;
import com.jlr.svo.containers.SVOAdditionalVehicleContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.containers.SVOItemToApproveContainer;
import com.jlr.svo.containers.SVOOpportunityContainer;
import com.jlr.svo.containers.SVO_CustomerResponsesContainer;
import com.jlr.svo.containers.SVO_EnquiryLostReasonContainer;
import com.jlr.svo.containers.SVO_RestrictedPartyScreeningContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SVO_CustomerResponse_Test extends TestBaseCC {
	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory.getLogger(SVO_CustomerResponse_Test.class.getName());
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);

	SVOAccountsContainer SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
	SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	SVOAdditionalVehicleContainer SVOAdditionalvehicleContainer = PageFactory.initElements(driver,
			SVOAdditionalVehicleContainer.class);
	SVO_RestrictedPartyScreeningContainer RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
			SVO_RestrictedPartyScreeningContainer.class);
	SVOItemToApproveContainer SVOItemToapproveContainer = PageFactory.initElements(driver,
			SVOItemToApproveContainer.class);
	SVO_CustomerResponsesContainer SVO_CustomerResponsesContainer = PageFactory.initElements(driver,
			SVO_CustomerResponsesContainer.class);
	SVO_EnquiryLostReasonContainer SVO_EnquiryLostReasonContainer = PageFactory.initElements(driver,
			SVO_EnquiryLostReasonContainer.class);

	public static String verificationCode;
	public static String vCode;
	public static String veriCode;
	public static String EnquiryCreatedDate;
	public static String EnquiryName;
	public static String EnquiryTitle;
	public static String EnquiryStatus;

	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	public void onStart() {

		setupTest("SVOTest");
		driver = getDriver();

		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
		SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
		SVOAdditionalvehicleContainer = PageFactory.initElements(driver, SVOAdditionalVehicleContainer.class);

		RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
				SVO_RestrictedPartyScreeningContainer.class);
		SVOItemToapproveContainer = PageFactory.initElements(driver, SVOItemToApproveContainer.class);
		SVO_CustomerResponsesContainer = PageFactory.initElements(driver, SVO_CustomerResponsesContainer.class);
		SVO_EnquiryLostReasonContainer = PageFactory.initElements(driver, SVO_EnquiryLostReasonContainer.class);

		driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);

		verificationCode = null;
		vCode = null;
		EnquiryCreatedDate = null;
		EnquiryName = null;
		EnquiryTitle = null;
		EnquiryStatus = null;

	}

	@Given("^Access to mail account with Username \"([^\"]*)\" and password \"([^\"]*)\"$")
	public void access_to_mail_account_with_Username_and_password(String UserName, String Password) throws Throwable {

		String s[] = UserName.split(",");
		UserName = com.jlr.svo.utilities.CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String p[] = Password.split(",");
		Password = CommonFunctions.readExcelMasterData(p[0], p[1], p[2]);

		onStart();
		driver = getDriver();
		ActionHandler.wait(5);
		driver.get(Constants.gmailURL);

		Reporter.addStepLog("User tries to login to Gmail");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVO_CustomerResponsesContainer.signinbutton);
		ActionHandler.wait(1);
		ActionHandler.setText(SVO_CustomerResponsesContainer.outlookEmailId, UserName);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(1);
		ActionHandler.click(SVO_CustomerResponsesContainer.gmailNext);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVO_CustomerResponsesContainer.gmailPassword, Password);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(1);
		ActionHandler.click(SVO_CustomerResponsesContainer.gmailNext);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User logged into Gmail Account");

		VerifyHandler.verifyElementPresent(SVO_CustomerResponsesContainer.gmailText);

	}

	// Customer send an enquiry mail to user
	@When("^Customer send an enquiry mail to user \"([^\"]*)\"$")
	public void customer_send_an_enquiry_mail_to_user(String User) throws Throwable {

		String p[] = User.split(",");
		User = CommonFunctions.readExcelMasterData(p[0], p[1], p[2]);

		String EnqSubject = "Enquiry creation mail from customer_";
		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		EnquiryTitle = "Enquiry creation mail from customer";
		ActionHandler.click(SVO_CustomerResponsesContainer.NewMailCompose);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Compose email icon");

		ActionHandler.click(SVO_CustomerResponsesContainer.GmailToTextBox);
		ActionHandler.wait(1);
		ActionHandler.setText(SVO_CustomerResponsesContainer.GmailToTextBox, User);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters To address to compose an email");

		ActionHandler.click(SVO_CustomerResponsesContainer.GmailSubjectTextBox);
		ActionHandler.wait(1);
		ActionHandler.setText(SVO_CustomerResponsesContainer.GmailSubjectTextBox, EnqSubject + randomNumber);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters subject to the mail as Enquiry creation request");

		ActionHandler.click(SVO_CustomerResponsesContainer.GmailBodyTextBox);
		ActionHandler.wait(1);
		ActionHandler.setText(SVO_CustomerResponsesContainer.GmailBodyTextBox, EnquiryTitle);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Body text to the mail as Enquiry creation request");

		ActionHandler.click(SVO_CustomerResponsesContainer.SendMailBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on send email button");

		Reporter.addStepLog(User + " : sent an email to create an enquiry on salesforce");

	}

	@Then("^Verify that Customer receives an acknowledgement email$")
	public void Verify_that_customer_receives_an_acknowledgement_email() throws Throwable {
		ActionHandler.wait(40);
		driver.navigate().refresh();

		ActionHandler.click(SVO_CustomerResponsesContainer.firstEmail);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Thank you email that is received from email inbox");

		VerifyHandler.verifyElementPresent(SVO_CustomerResponsesContainer.ThankYouEmail);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Verifies the thank you email received from salesforce");

	}

	@Given("^Login to the SVO Portal by bespoke uat user with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void login_to_the_SVO_portal_by_bespoke_uat_user_Username_and_Password(String userName, String password)
			throws Throwable {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailOnBespokeUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();

			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				CommonFunctions.attachScreenshot();

			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				CommonFunctions.attachScreenshot();

			} else {

				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	public String checkEmailOnBespokeUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(334, 340);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	@Then("^User update owner as UAT test for an private office enquiry Owner Name \"([^\"]*)\"$")
	public void user_update_owner_as_UAT_test_for_an_private_office_enquiry_Owner_Name(String Owner) throws Throwable {

		String p[] = Owner.split(",");
		Owner = CommonFunctions.readExcelMasterData(p[0], p[1], p[2]);

		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);

		javaScriptUtil.clickElementByJS(SVO_CustomerResponsesContainer.EnquiryChangeOwnerButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on change owner button on enquiry page");

		ActionHandler.click(SVO_CustomerResponsesContainer.EnquiryChangeOwnerTextBox);
		ActionHandler.wait(1);
		ActionHandler.setText(SVO_CustomerResponsesContainer.EnquiryChangeOwnerTextBox, Owner);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on change owner button on enquiry page");

		ActionHandler.click(SVO_CustomerResponsesContainer.EnquiryChangeOwnerSearchbar);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User search for the owner from search bar");

		ActionHandler.click(SVO_CustomerResponsesContainer.EnquirySelectOwner);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects UAT test as a owner for an enquiry");

		ActionHandler.click(SVO_CustomerResponsesContainer.ChangeOwnerSaveBtn);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on save button of an change owner pop up window");

		driver.switchTo().alert().accept();
		ActionHandler.wait(5);

	}

	@Then("^Verify the email details of an enquiry from custom notification received$")
	public void verify_the_email_details_of_an_enquiry_from_custom_notification_received() throws Throwable {

		driver.navigate().refresh();
		ActionHandler.wait(10);
		ActionHandler.click(SVO_CustomerResponsesContainer.NotificationsIcon);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Opens custom notification");

		ActionHandler.click(SVO_CustomerResponsesContainer.CustomerResponseNotification);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Opens customer response mail on SVO portal");

		ActionHandler.wait(20);

		driver.switchTo().frame(0);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user navigate to mail frame");

		VerifyHandler.verifyElementPresent(SVO_CustomerResponsesContainer.E2AMailPage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens enquiry mail from custom notification");

		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);

		ActionHandler.click(SVO_CustomerResponsesContainer.EnquiryNameOnE2AMailpage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to enquiry from customer response mail");
		driver.switchTo().parentFrame();
	}

	@Then("^User navigated to enquiry link from the mail received from customer$")
	public void user_navigated_to_enquiry_link_from_the_mail_received_from_user() throws Throwable {

		String userName = Config.getPropertyValue("Gmail_UserName");
		String password = Config.getPropertyValue("Gmail_Password");

		ActionHandler.wait(10);
		driver.navigate().refresh();
		ActionHandler.wait(5);

		ActionHandler.click(SVO_CustomerResponsesContainer.firstEmail);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to emails section on enquiries page");

		ActionHandler.click(SVO_CustomerResponsesContainer.E2AMailLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens enquiry from link present in mail");

		String mailWindow = driver.getWindowHandle();
		Set<String> mailWindows = driver.getWindowHandles();
		Iterator<String> i = mailWindows.iterator();
		while (i.hasNext()) {
			String mailWindow1 = i.next();
			if (!mailWindow.equalsIgnoreCase(mailWindow1)) {
				driver.switchTo().window(mailWindow1);

				ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
				CommonFunctions.attachScreenshot();

				ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
				CommonFunctions.attachScreenshot();

				ActionHandler.click(SVOEnquiryContainer.loginBtn);
				CommonFunctions.attachScreenshot();
				ActionHandler.wait(10);
				driver.switchTo().frame(0);
				CommonFunctions.attachScreenshot();
				Reporter.addStepLog("user navigate to mail frame");
				ActionHandler.wait(5);
				VerifyHandler.verifyElementPresent(SVO_CustomerResponsesContainer.E2AMailPage);
				CommonFunctions.attachScreenshot();
				Reporter.addStepLog("User opens enquiry mail from link present in the mailbox");
				driver.switchTo().parentFrame();
				driver.close();
				driver.switchTo().window(mailWindow);

			}
		}

	}

	@And("^Verify automatically created private office enquiry on SVO Portal$")
	public void verify_automatically_created_private_office_enquiry_on_SVO_Portal() throws Throwable {

		ActionHandler.wait(5);
		javaScriptUtil.clickElementByJS(SVO_CustomerResponsesContainer.EnquiriesTab);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigates to Enquiries tab");

		ActionHandler.wait(2);
		ActionHandler.click(SVO_CustomerResponsesContainer.SelectOpportunityListView);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on select list view tab");

		ActionHandler.click(SVO_CustomerResponsesContainer.AllPrivateOfficeEnquiriesList);
		Reporter.addStepLog("User chooses to view selected to private office enquiries list");
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User Verifies that the new enquiry is created automatically on SVO Portal");

	}

	@Given("^Verify automatically created bespoke enquiry on SVO Portal$")
	public void Verify_automatically_created_bespoke_enquiry_on_SVO_Portal() throws Throwable {

		ActionHandler.wait(5);
		javaScriptUtil.clickElementByJS(SVO_CustomerResponsesContainer.EnquiriesTab);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigates to Enquiries tab");

		ActionHandler.wait(2);
		ActionHandler.click(SVO_CustomerResponsesContainer.SelectOpportunityListView);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on select list view tab");

		ActionHandler.click(SVO_CustomerResponsesContainer.AllBespokeEnquiriesList);
		Reporter.addStepLog("User chooses to view selected to Bespoke enquiries list");
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User Verifies that the new enquiry is created automatically on SVO Portal");
	}

	@Given("^Verify automatically created classic service enquiry on SVO Portal$")
	public void Verify_automatically_created_classic_service_enquiry_on_SVO_Portal() throws Throwable {
		ActionHandler.wait(5);
		javaScriptUtil.clickElementByJS(SVO_CustomerResponsesContainer.EnquiriesTab);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigates to Enquiries tab");

		ActionHandler.wait(2);
		ActionHandler.click(SVO_CustomerResponsesContainer.SelectOpportunityListView);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on select list view tab");

		ActionHandler.click(SVO_CustomerResponsesContainer.ClassicServiceQueueEnquiriesList);
		Reporter.addStepLog("User chooses to view selected to Bespoke enquiries list");
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User Verifies that the new enquiry is created automatically on SVO Portal");
	}

	@Given("^Access to user mail account with Username \"([^\"]*)\" and password \"([^\"]*)\"$")
	public void access_to_user_mail_account_with_Username_and_password(String UserName, String Password)
			throws Throwable {

		String s[] = UserName.split(",");
		UserName = com.jlr.svo.utilities.CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String p[] = Password.split(",");
		Password = CommonFunctions.readExcelMasterData(p[0], p[1], p[2]);

		driver.get(Constants.gmailURL);

		Reporter.addStepLog("User tries to login to Gmail");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVO_CustomerResponsesContainer.UseAnotherAccount)) {
			ActionHandler.click(SVO_CustomerResponsesContainer.UseAnotherAccount);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
		}

		ActionHandler.setText(SVO_CustomerResponsesContainer.outlookEmailId, UserName);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(1);
		ActionHandler.click(SVO_CustomerResponsesContainer.gmailNext);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVO_CustomerResponsesContainer.gmailPassword, Password);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(1);
		ActionHandler.click(SVO_CustomerResponsesContainer.gmailNext);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User logged into Gmail Account");

		VerifyHandler.verifyElementPresent(SVO_CustomerResponsesContainer.gmailText);

	}

	@Then("^User Logout from SVO Portal$")
	public void logout_from_SVO_Portal() throws Throwable {

		ActionHandler.click(SVO_OpportunityContainer.icon_image);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Logout");
		ActionHandler.click(SVO_OpportunityContainer.Logout);

		CommonFunctions.attachScreenshot();
		if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.userNameTextBox)) {
			MarkupHelper.createLabel("Logged out successfully", ExtentColor.GREEN);
		} else {
			MarkupHelper.createLabel("Log out failed", ExtentColor.RED);

		}
	}

	@Given("^Login to the SVO Portal by classic service uat user with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void Login_to_the_SVO_Portal_by_classic_service_uat_user_with_Username_and_Password(String userName,
			String password) throws Throwable {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailForClassicServiceUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	public String checkEmailForClassicServiceUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(342, 348);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	@Then("^Verify that Customer is not receives an acknowledgement email$")
	public void Verify_that_customer_is_not_receives_an_acknowledgement_email() throws Throwable {
		ActionHandler.wait(40);
		driver.navigate().refresh();

		ActionHandler.click(SVO_CustomerResponsesContainer.firstEmail);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on blocked email that is received from email inbox");

		VerifyHandler.verifyElementPresent(SVO_CustomerResponsesContainer.BlockedMessage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Verifies the blocked email received from salesforce");

	}

	@Given("^Verify that no enquiry is created automatically on enquiries page$")
	public void verify_that_no_enquiry_is_created_automatically_on_enquiries_page() throws Throwable {

		ActionHandler.wait(5);
		javaScriptUtil.clickElementByJS(SVO_CustomerResponsesContainer.EnquiriesTab);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigates to Enquiries tab");

		ActionHandler.wait(2);
		ActionHandler.click(SVO_CustomerResponsesContainer.SelectOpportunityListView);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on select list view tab");

		if (!VerifyHandler.verifyElementPresent(SVO_CustomerResponsesContainer.DataManagerQueue)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Verify that no enquiry is created automatically on enquiries page");
		} else {
			Reporter.addStepLog("Verify that new enquiry is created automatically on enquiries page");
		}

	}

	@Then("^Verify the not able to receive email of an enquiry from custom notification$")
	public void verify_the_not_able_to_receive_email_of_an_enquiry_from_custom_notification() throws Throwable {
		ActionHandler.click(SVO_CustomerResponsesContainer.NotificationsIcon);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Opens custom notification");

		if (!VerifyHandler.verifyElementPresent(SVO_CustomerResponsesContainer.NoCustomerResponseNotification)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Verify that no customer response email notification is received on SVO portal");
		} else {
			Reporter.addStepLog("Verify that new customer response email notification is received on SVO portal");
		}
	}

	@Then("^Verify that the enquiry is assigned on user's name$")
	public void verify_that_the_enquiry_is_assigned_on_user_s_name() throws Throwable {
		ActionHandler.click(SVO_CustomerResponsesContainer.NotificationsIcon);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Opens custom notification");

		ActionHandler.click(SVO_CustomerResponsesContainer.EnquiryAssignedNotification);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Opens Enquiry assigned notification mail on SVO portal");

	}

	@Then("^User send email to customer \"([^\"]*)\" with Business unit \"([^\"]*)\" and subject$")
	public void user_send_email_to_customer_with_Business_unit_and_subject(String Customer, String BusinessUnit)
			throws Throwable {

		String p[] = BusinessUnit.split(",");
		BusinessUnit = CommonFunctions.readExcelMasterData(p[0], p[1], p[2]);

		String s[] = Customer.split(",");
		Customer = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		javaScriptUtil.clickElementByJS(SVO_CustomerResponsesContainer.e2aSendEmailButton);
		ActionHandler.wait(30);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on e2a send an email button");

		driver.switchTo().frame(0);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user navigate to mail frame");

		ActionHandler.click(SVO_CustomerResponsesContainer.BusinessUnitDropDown);

		ActionHandler.scrollDown();
		ActionHandler.wait(1);
		ActionHandler.click(SVO_CustomerResponsesContainer.EmailToTextBox);
		ActionHandler.wait(1);
		ActionHandler.setText(SVO_CustomerResponsesContainer.EmailToTextBox, Customer);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters " + Customer + " in To section of an e2a email");

		ActionHandler.scrollDown();
		ActionHandler.wait(1);
		ActionHandler.scrollDown();
		ActionHandler.wait(1);

		ActionHandler.click(SVO_CustomerResponsesContainer.OwnerEmailSubjectTextBox);
		ActionHandler.wait(1);
		ActionHandler.setText(SVO_CustomerResponsesContainer.OwnerEmailSubjectTextBox,
				"Please specify the body style?");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters Subject in subject section of an e2a email");

		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(1);

		ActionHandler.click(SVO_CustomerResponsesContainer.EmailSendButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks on send an email button");

		driver.switchTo().parentFrame();

	}

	@Then("^verify that there is no link present in the mail received from customer$")
	public void verify_that_there_is_no_link_present_in_the_mail_received_from_customer() throws Throwable {
		ActionHandler.click(SVO_CustomerResponsesContainer.firstEmail);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to emails section on enquiries page");

		if (!VerifyHandler.verifyElementPresent(SVO_CustomerResponsesContainer.E2AEMailLink)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Verify that no link present in the mail received from customer");
		} else {
			Reporter.addStepLog("Verify that there is a link present in the mail received from customer");
		}

	}

	@Given("^Access to owner mail account with Username \"([^\"]*)\" and password \"([^\"]*)\"$")
	public void access_to_owner_mail_account_with_Username_and_password(String UserName, String Password)
			throws Throwable {

		String s[] = UserName.split(",");
		UserName = com.jlr.svo.utilities.CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String p[] = Password.split(",");
		Password = CommonFunctions.readExcelMasterData(p[0], p[1], p[2]);

		access_to_owner_mail_account_to_delete_verification_mail_with_Username_and_password("LoginUsers,B,16",
				"LoginUsers,C,16");
		driver.get(Constants.gmailURL);

		Reporter.addStepLog("User tries to login to Gmail");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVO_CustomerResponsesContainer.UseAnotherAccount)) {
			ActionHandler.click(SVO_CustomerResponsesContainer.UseAnotherAccount);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
		}

		ActionHandler.setText(SVO_CustomerResponsesContainer.outlookEmailId, UserName);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(1);
		ActionHandler.click(SVO_CustomerResponsesContainer.gmailNext);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVO_CustomerResponsesContainer.gmailPassword, Password);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(1);
		ActionHandler.click(SVO_CustomerResponsesContainer.gmailNext);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User logged into Gmail Account");

		VerifyHandler.verifyElementPresent(SVO_CustomerResponsesContainer.gmailText);

	}

	@Then("^Logout from users mail account$")
	public void logout_from_users_mail_account() throws Throwable {

		ActionHandler.click(SVO_CustomerResponsesContainer.GmailaccountLogo);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Gmail Account logo");

		driver.switchTo().frame(0);
		ActionHandler.click(SVO_CustomerResponsesContainer.signoutGmailIcon);
		ActionHandler.wait(5);
		driver.switchTo().parentFrame();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Logout button");

	}

	@Given("^Verify that the New Bespoke enquiry is created automatically on SVO Portal$")
	public void verify_that_the_New_Bespoke_enquiry_is_created_automatically_on_SVO_Portal() throws Throwable {

		javaScriptUtil.clickElementByJS(SVO_CustomerResponsesContainer.EnquiriesTab);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigates to Enquiries tab");

		ActionHandler.wait(2);
		ActionHandler.click(SVO_CustomerResponsesContainer.SelectOpportunityListView);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on select list view tab");

		ActionHandler.click(SVO_CustomerResponsesContainer.AllBespokeEnquiriesList);
		Reporter.addStepLog("User chooses to view selected Bespoke enquiries list");
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User Verifies that the new enquiry is created automatically on SVO Portal");

	}

	@Then("^Verify that Created date, Enquiry title and Enquiry status on Enquiries page$")
	public void verify_that_Created_date_Enquiry_title_and_Enquiry_status_on_Enquiries_page() throws Throwable {

		ActionHandler.wait(6);
		EnquiryName = SVOAdditionalvehicleContainer.FirstOpportunityLink.getText();
		Reporter.addStepLog("Enquiry name is displayed as : " + EnquiryName);

		EnquiryCreatedDate = SVO_CustomerResponsesContainer.EnquiryCreatedDate.getText();
		Reporter.addStepLog("Enquiry Created date is displayed as : " + EnquiryCreatedDate);

		EnquiryTitle = SVO_CustomerResponsesContainer.EnquiryTitle.getText();
		Reporter.addStepLog("Enquiry title is displayed as : " + EnquiryTitle);

		EnquiryStatus = SVO_CustomerResponsesContainer.EnquiryStatus.getText();
		if (EnquiryStatus.equalsIgnoreCase("Enquiry creation mail from customer")) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Enquiry status is displayed as the subject of the email sent from customer");
		}
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enquiry status is displayed as : " + EnquiryStatus);

		ActionHandler.wait(5);
		ActionHandler.click(SVOAdditionalvehicleContainer.FirstOpportunityLink);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects the enquiry that is created automatically on salesforce");

	}

	@Then("^Verify that subject name is auto-populated on emails page$")
	public void verify_that_subject_name_is_auto_populated_on_emails_page() throws Throwable {

		ActionHandler.wait(4);
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.wait(3);

		javaScriptUtil.clickElementByJS(SVO_CustomerResponsesContainer.EmailsSection);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigates to email section on Enquiry page");

		VerifyHandler.verifyElementPresent(SVO_CustomerResponsesContainer.E2ACustomerEmailSubject);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"An e2a email is displayed with subject, from name , from address details on emails section");

	}

	@Then("^User send an email to customer \"([^\"]*)\" with Business unit \"([^\"]*)\" and subject$")
	public void user_send_an_email_to_customer_with_Business_unit_and_subject(String Customer, String BusinessUnit)
			throws Throwable {

		String p[] = BusinessUnit.split(",");
		BusinessUnit = CommonFunctions.readExcelMasterData(p[0], p[1], p[2]);

		String s[] = Customer.split(",");
		Customer = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		ActionHandler.wait(10);
		ActionHandler.click(SVO_CustomerResponsesContainer.e2aSendEmailButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on e2a send an email button");

		ActionHandler.wait(5);
		driver.switchTo().frame(0);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user navigate to email frame");

		ActionHandler.wait(10);
		ActionHandler.click(SVO_CustomerResponsesContainer.BusinessUnitDropDown);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Business unit drop down list");

		ActionHandler.click(
				driver.findElement(By.xpath(SVO_CustomerResponsesContainer.BusinessUnitSelection(BusinessUnit))));
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects the " + BusinessUnit + " Business unit from drop down list");

		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.wait(1);
		ActionHandler.click(SVO_CustomerResponsesContainer.EmailToTextBox);
		ActionHandler.wait(1);
		ActionHandler.setText(SVO_CustomerResponsesContainer.EmailToTextBox, Customer);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters " + Customer + " in To section of an e2a email");

		ActionHandler.click(SVO_CustomerResponsesContainer.EmailSubjectTextBox);
		ActionHandler.wait(1);
		ActionHandler.setText(SVO_CustomerResponsesContainer.EmailSubjectTextBox, "Please specify the body style?");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters text in subject section of an e2a email");

		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(2);

		ActionHandler.click(SVO_CustomerResponsesContainer.EmailSendButton);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks on send an email button");

		driver.switchTo().parentFrame();

	}

	@Then("^Verify that enquiry status on enquiries page$")
	public void verify_that_enquiry_status_on_enquiries_page() throws Throwable {

		VerifyHandler.verifyElementPresent(SVO_EnquiryLostReasonContainer.EnquiryStatusField);
		String EnquiryStatusField = SVO_EnquiryLostReasonContainer.EnquiryStatusField.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the enquiry status displayed as " + EnquiryStatusField + " on enquiry page");

	}

	@Then("^User update owner as UAT test for an enquiry Owner Name \"([^\"]*)\"$")
	public void user_update_owner_as_UAT_test_for_an_enquiry_Owner_Name(String Owner) throws Throwable {

		String p[] = Owner.split(",");
		Owner = CommonFunctions.readExcelMasterData(p[0], p[1], p[2]);

		// driver.navigate().refresh();
		ActionHandler.click(SVO_CustomerResponsesContainer.EnquiryChangeOwnerButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on change owner button on enquiry page");

		ActionHandler.click(SVO_CustomerResponsesContainer.EnquiryChangeOwnerTextBox);
		ActionHandler.wait(1);
		ActionHandler.setText(SVO_CustomerResponsesContainer.EnquiryChangeOwnerTextBox, Owner);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters owner name in the text box");

		ActionHandler.click(SVO_CustomerResponsesContainer.EnquiryChangeOwnerSearchbar);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User search for the owner from search bar");

		ActionHandler.click(SVO_CustomerResponsesContainer.EnquirySelectOwner);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects UAT test as a owner for an enquiry");

		ActionHandler.click(SVO_CustomerResponsesContainer.ChangeOwnerSubmitBtn);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on submit button of an change owner pop up window");

	}

	@Then("^Verify that the enquiry is removed from the list view for which the owner has been changed$")
	public void Verify_that_the_enquiry_is_removed_from_the_list_view_for_which_the_owner_has_been_changed()
			throws Throwable {

		ActionHandler.wait(3);
		ActionHandler.click(SVO_EnquiryLostReasonContainer.EnquirysearchBox);
		ActionHandler.wait(2);
		ActionHandler.setText(SVO_EnquiryLostReasonContainer.EnquirysearchBox, EnquiryName);

		ActionHandler.wait(3);
		ActionHandler.pressEnter();
		ActionHandler.wait(5);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User is not able to view the enquiry for which the owner has been changed");

	}

	@Then("^Verify that Customer receives an enquiry update email from user$")
	public void verify_that_customer_receive_an_enquiry_update_email_from_user() throws Throwable {

		ActionHandler.wait(5);
		ActionHandler.click(SVO_CustomerResponsesContainer.firstEmail);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Opens an update email received from the user");

		VerifyHandler.verifyElementPresent(SVO_CustomerResponsesContainer.ReceivedRequestEmail);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Verifies the update email received from salesforce");

	}

	@Given("^Login to SVO Portal with Classic Service user with the Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void login_to_an_SVO_Portal_with_classic_service_user_with_the_Username_and_Password(String userName,
			String password) throws Throwable {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailForClassicServiceUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			Reporter.addStepLog("User logins to SVO Successfully");

		}
	}

	@Then("^Customer reply to received enquiry update request email$")
	public void Customer_reply_to_received_enquiry_update_request_email() throws Throwable {

		if (VerifyHandler.verifyElementPresent(SVO_CustomerResponsesContainer.EmailReplyBtn)) {
			ActionHandler.click(SVO_CustomerResponsesContainer.EmailReplyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User clicks on Reply button on email");
		} else if (VerifyHandler.verifyElementPresent(SVO_CustomerResponsesContainer.ClassicEmailReplyBtn)) {
			ActionHandler.click(SVO_CustomerResponsesContainer.ClassicEmailReplyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User clicks on Reply button on email");
		}

		ActionHandler.click(SVO_CustomerResponsesContainer.EmailReplyTextBox);
		ActionHandler.wait(1);
		ActionHandler.setText(SVO_CustomerResponsesContainer.EmailReplyTextBox, "Body Style : Test Defender");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters email body");

		ActionHandler.click(SVO_CustomerResponsesContainer.SendMailBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on send mail button");
	}

	@Given("^Access to SVO Portal by Owner user with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void access_to_SVO_Portal_by_Owner_user_with_Username_and_Password(String userName, String password)
			throws Throwable {

		access_to_owner_mail_account_to_delete_verification_mail_with_Username_and_password("LoginUsers,B,16",
				"LoginUsers,C,16");

		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		driver.get(Constants.SVOURL);
		Reporter.addStepLog("User Logins to SVO Portal");

		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(60);
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailForEnquiryTestUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}

	}

	public String checkEmailForEnquiryTestUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Gmail_UserName");
		String password = Config.getPropertyValue("Gmail_Password");

		driver.get(Constants.gmailURL);

		Reporter.addStepLog("User tries to login to Gmail");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVO_CustomerResponsesContainer.UseAnotherAccount)) {
			ActionHandler.click(SVO_CustomerResponsesContainer.UseAnotherAccount);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
		}

		ActionHandler.setText(SVO_CustomerResponsesContainer.outlookEmailId, userName);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(1);
		ActionHandler.click(SVO_CustomerResponsesContainer.gmailNext);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVO_CustomerResponsesContainer.gmailPassword, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(1);
		ActionHandler.click(SVO_CustomerResponsesContainer.gmailNext);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User logged into Gmail Account");

		VerifyHandler.verifyElementPresent(SVO_CustomerResponsesContainer.gmailText);
		CommonFunctions.attachScreenshot();

		javaScriptUtil.clickElementByJS(SVO_CustomerResponsesContainer.VerificationCodeMail);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens the verification code received mail to get an OTP");

		ActionHandler.wait(3);
		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(2);

		CommonFunctions.attachScreenshot();
		verificationCode = SVO_CustomerResponsesContainer.VerificationCode.getText();
		vCode = verificationCode.substring(310, 316);
		System.out.println("Verification Code is = " + vCode);

		ActionHandler.wait(2);
		ActionHandler.click(SVO_CustomerResponsesContainer.EmailDeleteButton);
		CommonFunctions.attachScreenshot();

		return vCode;
	}

	@Then("^Verify that customer response email subject is displayed under emails section of an enquiry$")
	public void verify_that_customer_response_email_subject_is_displayed_under_emails_section_of_an_enquiry()
			throws Throwable {
		VerifyHandler.verifyElementPresent(SVO_EnquiryLostReasonContainer.EnquiryStatusField);
		verify_that_subject_name_is_auto_populated_on_emails_page();

	}

	@Then("^User navigates to enquiry link from the mail received from customer$")
	public void user_navigates_to_enquiry_link_from_the_mail_received_from_user() throws Throwable {

		String userName = Config.getPropertyValue("Gmail_UserName");
		String password = Config.getPropertyValue("Gmail_Password");

		ActionHandler.wait(6);
		driver.navigate().refresh();

		ActionHandler.click(SVO_CustomerResponsesContainer.firstEmail);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to emails section on enquiries page");

		ActionHandler.click(SVO_CustomerResponsesContainer.E2AMailLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens enquiry from link present in mail");

		String mailWindow = driver.getWindowHandle();
		Set<String> mailWindows = driver.getWindowHandles();
		Iterator<String> i = mailWindows.iterator();
		while (i.hasNext()) {
			String mailWindow1 = i.next();
			if (!mailWindow.equalsIgnoreCase(mailWindow1)) {
				driver.switchTo().window(mailWindow1);

				ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
				CommonFunctions.attachScreenshot();

				ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
				CommonFunctions.attachScreenshot();

				ActionHandler.click(SVOEnquiryContainer.loginBtn);
				CommonFunctions.attachScreenshot();

				ActionHandler.wait(40);
				driver.switchTo().frame(0);
				CommonFunctions.attachScreenshot();
				Reporter.addStepLog("user navigate to mail frame");
				ActionHandler.wait(5);
				VerifyHandler.verifyElementPresent(SVO_CustomerResponsesContainer.E2AMailPage);
				CommonFunctions.attachScreenshot();
				Reporter.addStepLog("User opens enquiry mail from link present in the mailbox");
				driver.switchTo().parentFrame();
				driver.close();
				driver.switchTo().window(mailWindow);

			}
		}
	}

	@Given("^Login to SVO Portal as Classic Admin user with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void login_to_an_SVO_Portal_as_classic_admin_user_with_Username_and_Password(String userName,
			String password) throws Throwable {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailForClassicAdminUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		}
	}

	public String checkEmailForClassicAdminUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(340, 346);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	@Given("^Access to gmail account with Username \"([^\"]*)\" and password \"([^\"]*)\"$")
	public void access_to_gmail_account_with_Username_and_password(String UserName, String Password) throws Throwable {

		String s[] = UserName.split(",");
		UserName = com.jlr.svo.utilities.CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String p[] = Password.split(",");
		Password = CommonFunctions.readExcelMasterData(p[0], p[1], p[2]);

		ActionHandler.wait(8);
		driver.get(Constants.gmailURL);

		Reporter.addStepLog("User tries to login to Gmail");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVO_CustomerResponsesContainer.UseAnotherAccount)) {
			ActionHandler.click(SVO_CustomerResponsesContainer.UseAnotherAccount);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
		}

		ActionHandler.setText(SVO_CustomerResponsesContainer.outlookEmailId, UserName);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(1);
		ActionHandler.click(SVO_CustomerResponsesContainer.gmailNext);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVO_CustomerResponsesContainer.gmailPassword, Password);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(1);
		ActionHandler.click(SVO_CustomerResponsesContainer.gmailNext);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User logged into Gmail Account");

		VerifyHandler.verifyElementPresent(SVO_CustomerResponsesContainer.gmailText);
		ActionHandler.wait(5);

	}

	@Given("^Access to owner mail account to delete verification mail with Username \"([^\"]*)\" and password \"([^\"]*)\"$")
	public void access_to_owner_mail_account_to_delete_verification_mail_with_Username_and_password(String UserName,
			String Password) throws Throwable {

		String s[] = UserName.split(",");
		UserName = com.jlr.svo.utilities.CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String p[] = Password.split(",");
		Password = CommonFunctions.readExcelMasterData(p[0], p[1], p[2]);

		driver.get(Constants.gmailURL);

		Reporter.addStepLog("User tries to login to Gmail");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVO_CustomerResponsesContainer.UseAnotherAccount)) {
			ActionHandler.click(SVO_CustomerResponsesContainer.UseAnotherAccount);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
		}

		ActionHandler.setText(SVO_CustomerResponsesContainer.outlookEmailId, UserName);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(1);
		ActionHandler.click(SVO_CustomerResponsesContainer.gmailNext);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVO_CustomerResponsesContainer.gmailPassword, Password);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(1);
		ActionHandler.click(SVO_CustomerResponsesContainer.gmailNext);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User logged into Gmail Account");

		VerifyHandler.verifyElementPresent(SVO_CustomerResponsesContainer.gmailText);

		ActionHandler.wait(10);
		driver.navigate().refresh();

		if (VerifyHandler.verifyElementPresent(SVO_CustomerResponsesContainer.VerificationCodeMail)) {

			javaScriptUtil.clickElementByJS(SVO_CustomerResponsesContainer.VerificationCodeMail);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User opens the verification code received mail to get an OTP");

			ActionHandler.wait(2);
			ActionHandler.click(SVO_CustomerResponsesContainer.EmailDeleteButton);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
		}
		logout_from_users_mail_account();

	}

	@Given("^Verify that the New Classic Sales enquiry is created automatically on SVO Portal$")
	public void verify_that_the_New_classic_sales_enquiry_is_created_automatically_on_SVO_Portal() throws Throwable {

		javaScriptUtil.clickElementByJS(SVO_CustomerResponsesContainer.EnquiriesTab);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigates to Enquiries tab");

		ActionHandler.wait(2);
		ActionHandler.click(SVO_CustomerResponsesContainer.SelectOpportunityListView);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on select list view tab");

		ActionHandler.click(SVO_CustomerResponsesContainer.ClassicSalesQueueEnquiriesList);
		Reporter.addStepLog("User chooses to view Classic sales queue enquiries list");
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User Verifies that the new classic sales enquiry is created automatically on SVO Portal");

	}

	@Then("^User sends multiple emails to customer \"([^\"]*)\" with Business unit \"([^\"]*)\" and subject$")
	public void user_sends_multiple_emails_to_customer_with_Business_unit_and_subject(String Customer,
			String BusinessUnit) throws Throwable {

		String p[] = BusinessUnit.split(",");
		BusinessUnit = CommonFunctions.readExcelMasterData(p[0], p[1], p[2]);

		String s[] = Customer.split(",");
		Customer = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		user_send_an_email_to_customer_with_Business_unit_and_subject("LoginUsers,B,17", "CustomerResponse,I,2");
		VerifyHandler.verifyElementPresent(SVO_EnquiryLostReasonContainer.EnquiryStatusField);
		ActionHandler.wait(15);
		driver.navigate().refresh();

		ActionHandler.wait(10);
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.wait(3);

		javaScriptUtil.clickElementByJS(SVO_CustomerResponsesContainer.EmailsSection);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigates to email section on Enquiry page");

		VerifyHandler.verifyElementPresent(SVO_CustomerResponsesContainer.E2ACustomerEmailSubject);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"An e2a email is displayed with subject, from name , from address details on emails section");

		ActionHandler.click(SVO_CustomerResponsesContainer.e2aSendEmailButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on e2a send an email button");

		ActionHandler.wait(5);
		driver.switchTo().frame(0);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user navigate to email frame");

		ActionHandler.click(SVO_CustomerResponsesContainer.BusinessUnitDropDown);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Business unit drop down list");

		ActionHandler.click(
				driver.findElement(By.xpath(SVO_CustomerResponsesContainer.BusinessUnitSelection(BusinessUnit))));
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects the " + BusinessUnit + " Business unit from drop down list");

		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.wait(1);
		ActionHandler.click(SVO_CustomerResponsesContainer.EmailToTextBox);
		ActionHandler.wait(1);
		ActionHandler.setText(SVO_CustomerResponsesContainer.EmailToTextBox, Customer);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters " + Customer + " in To section of an e2a email");

		ActionHandler.click(SVO_CustomerResponsesContainer.EmailSubjectTextBox);
		ActionHandler.wait(1);
		ActionHandler.setText(SVO_CustomerResponsesContainer.EmailSubjectTextBox, "Please specify the body style?");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters text in subject section of an e2a email");

		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(2);

		ActionHandler.click(SVO_CustomerResponsesContainer.EmailSendButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks on send an email button");

		driver.switchTo().parentFrame();

	}

	@Then("^Verify that Customer receives multple response emails from user on a single enquiry$")
	public void verify_that_customer_receive_multiple_response_emails_from_user_on_a_single_enquiry() throws Throwable {

		ActionHandler.click(SVO_CustomerResponsesContainer.firstEmail);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Opens an first update email received from the user");

		ActionHandler.wait(3);
		ActionHandler.click(SVO_CustomerResponsesContainer.firstEmail);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Opens an second update email received from the user");

		VerifyHandler.verifyElementPresent(SVO_CustomerResponsesContainer.ReceivedRequestEmail);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Verifies the updated email received from salesforce");

	}

	@Given("^Verify that the New Classic Service enquiry is created automatically on SVO Portal$")
	public void verify_that_the_New_classic_service_enquiry_is_created_automatically_on_SVO_Portal() throws Throwable {

		javaScriptUtil.clickElementByJS(SVO_CustomerResponsesContainer.EnquiriesTab);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigates to Enquiries tab");

		ActionHandler.wait(2);
		ActionHandler.click(SVO_CustomerResponsesContainer.SelectOpportunityListView);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on select list view tab");

		ActionHandler.click(SVO_CustomerResponsesContainer.ClassicServiceQueueEnquiriesList);
		Reporter.addStepLog("User chooses to view Classic Service queue enquiries list");
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog(
				"User Verifies that the new classic service enquiry is created automatically on SVO Portal");

	}

	@Then("^User Edit the Enquiry details of an created enquiry on Enquiry page$")
	public void user_Edit_the_Enquiry_title_for_an_closed_lost_enquiry() throws Throwable {

		ActionHandler.wait(5);
		ActionHandler.click(SVO_EnquiryLostReasonContainer.EditEnquiryBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Edit Enquiry button");

		ActionHandler.clearAndSetText(SVO_EnquiryLostReasonContainer.EnquiryTitleTextBox,
				"Edited Classic service Enquiry");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Edits the enquiry title");

		ActionHandler.click(SVO_EnquiryLostReasonContainer.EnquirySaveButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks on to Enquiries Save button");

		ActionHandler.wait(5);
		Reporter.addStepLog("User is edited the enquiry details on enuiry page");

	}

	@Given("^Login to SVO Portal by private office user with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void login_to_an_SVO_portal_by_private_office_user_with_Username_and_Password(String userName,
			String password) throws Throwable {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailOnPrivateOfficeUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				CommonFunctions.attachScreenshot();

			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				CommonFunctions.attachScreenshot();

			} else {

				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {

			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	public String checkEmailOnPrivateOfficeUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(341, 347);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	@Given("^Verify that the New Private Office enquiry is created automatically on SVO Portal$")
	public void verify_that_the_New_private_office_enquiry_is_created_automatically_on_SVO_Portal() throws Throwable {

		javaScriptUtil.clickElementByJS(SVO_CustomerResponsesContainer.EnquiriesTab);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigates to Enquiries tab");

		ActionHandler.wait(2);
		ActionHandler.click(SVO_CustomerResponsesContainer.SelectOpportunityListView);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on select list view tab");

		ActionHandler.click(SVO_CustomerResponsesContainer.PrivateOfficeQueueEnquiriesList);
		Reporter.addStepLog("User chooses to view Private Office queue enquiries list");
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User Verifies that the new private office enquiry is created automatically on SVO Portal");

	}

	@Then("^Verify that the user is not able to view customer response notification on SVO Portal$")
	public void Verify_that_the_user_is_not_able_to_view_customer_response_notification_on_SVO_Portal()
			throws Throwable {

		ActionHandler.click(SVO_CustomerResponsesContainer.NotificationsIcon);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Opens custom notification");

		if (!VerifyHandler.verifyElementPresent(SVO_CustomerResponsesContainer.CustomerResponseNotification)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User is not able to view customer response notification on SVO Portal");
		} else {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User is able to view customer response notification on SVO Portal");
		}
	}

}
