package com.jlr.svo.tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.containers.SSEC_1039_SVOEnquiryEmailRemaindersContainer;
import com.jlr.svo.containers.SSEC_1503_Clasic_Email_Signature_On_Salesforce_Container;
import com.jlr.svo.containers.SSEC_2157_DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer;
import com.jlr.svo.containers.SSEC_2159_BespokeOrderForm_CreationofOrderFormRecordsContainer;
import com.jlr.svo.containers.SVOAccountsContainer;
import com.jlr.svo.containers.SVOAdditionalVehicleContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.containers.SVOItemToApproveContainer;
import com.jlr.svo.containers.SVOOpportunityContainer;
import com.jlr.svo.containers.SVO_CustomerResponsesContainer;
import com.jlr.svo.containers.SVO_EnquiryLostReasonContainer;
import com.jlr.svo.containers.SVO_RestrictedPartyScreeningContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class SSEC_2157_DesignBriefAmendments_Seatbelts_Carpets_Fridge_Headliner extends TestBaseCC {

	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory
			.getLogger(SSEC_1039_SVOEnquiryEmailRemaindersTest.class.getName());
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);

	SVOAccountsContainer SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
	SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	SVOAdditionalVehicleContainer SVOAdditionalvehicleContainer = PageFactory.initElements(driver,
			SVOAdditionalVehicleContainer.class);
	SVO_RestrictedPartyScreeningContainer RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
			SVO_RestrictedPartyScreeningContainer.class);
	SVOItemToApproveContainer SVOItemToapproveContainer = PageFactory.initElements(driver,
			SVOItemToApproveContainer.class);
	SVO_CustomerResponsesContainer SVO_CustomerResponsesContainer = PageFactory.initElements(driver,
			SVO_CustomerResponsesContainer.class);
	SVO_EnquiryLostReasonContainer SVO_EnquiryLostReasonContainer = PageFactory.initElements(driver,
			SVO_EnquiryLostReasonContainer.class);
	SSEC_1039_SVOEnquiryEmailRemaindersContainer SVOEnquiryEmailRemaindersContainer = PageFactory.initElements(driver,
			SSEC_1039_SVOEnquiryEmailRemaindersContainer.class);
	SSEC_1503_Clasic_Email_Signature_On_Salesforce_Container Clasic_Email_Signature_On_Salesforce_Container = PageFactory.initElements(driver,
			SSEC_1503_Clasic_Email_Signature_On_Salesforce_Container.class);
	SSEC_2159_BespokeOrderForm_CreationofOrderFormRecordsContainer BespokeOrderForm_CreationofOrderFormRecordsContainer = PageFactory.initElements(driver, SSEC_2159_BespokeOrderForm_CreationofOrderFormRecordsContainer.class);
	SSEC_2157_DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer = PageFactory.initElements(driver, SSEC_2157_DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer.class);
	
	public static String verificationCode;
	public static String vCode;
	public static String veriCode;
	public static String EnquiryName;

	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}
	

	public void onStart() {

		setupTest("SVOTest");
		driver = getDriver();

		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
		SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
		SVOAdditionalvehicleContainer = PageFactory.initElements(driver, SVOAdditionalVehicleContainer.class);

		RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
				SVO_RestrictedPartyScreeningContainer.class);
		SVOItemToapproveContainer = PageFactory.initElements(driver, SVOItemToApproveContainer.class);
		SVO_CustomerResponsesContainer = PageFactory.initElements(driver, SVO_CustomerResponsesContainer.class);
		SVO_EnquiryLostReasonContainer = PageFactory.initElements(driver, SVO_EnquiryLostReasonContainer.class);
		SVOEnquiryEmailRemaindersContainer = PageFactory.initElements(driver,
				SSEC_1039_SVOEnquiryEmailRemaindersContainer.class);
		Clasic_Email_Signature_On_Salesforce_Container = PageFactory.initElements(driver,
				SSEC_1503_Clasic_Email_Signature_On_Salesforce_Container.class);
		BespokeOrderForm_CreationofOrderFormRecordsContainer = PageFactory.initElements(driver, SSEC_2159_BespokeOrderForm_CreationofOrderFormRecordsContainer.class);
		DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer = PageFactory.initElements(driver, SSEC_2157_DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer.class);
		
		
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		

		verificationCode = null;
		vCode = null;
		EnquiryName = null;

	}


    @And("^Click on the new Bespoke Design Brief quote created successfully$")
    public void Click_on_new_Bespoke_Design_Brief_quote_created_successfully() throws Exception{
    	
    	ActionHandler.click(DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer.QuoteRecord);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on Quote record link");
    	
    	ActionHandler.click(DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer.DesignBriefsTab);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on Design Brief tab");
    	
    	ActionHandler.click(DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer.DesignBriefRecord);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on Design Brief record");
    	
    }
    
    @Then("^Click on Edit button of front seat belt color and select the color of frontseatbelt \"([^\"]*)\" and rearseatbelt \"([^\"]*)\"$")
    public void Click_on_Edit_button_of_front_seat_belt(String frontseatbeltcolor, String rearseatbelt) throws Exception{
    	
    	String fseatcolor[] = frontseatbeltcolor.split(",");
    	frontseatbeltcolor = CommonFunctions.readExcelMasterData(fseatcolor[0], fseatcolor[1], fseatcolor[2]);

    	String rseatcolor[] = rearseatbelt.split(",");
    	rearseatbelt = CommonFunctions.readExcelMasterData(rseatcolor[0], rseatcolor[1], rseatcolor[2]);

    	
    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	
    	ActionHandler.click(DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer.EditFrontSeatBeltColorBtn);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on edit button of front seat belt color");
    	
    	ActionHandler.scrollToView(DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer.SeatBeltDetails);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Scroll to view front seat belt details");
    	
    	ActionHandler.click(DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer.FrontSeatBeltColorOption);
    	ActionHandler.wait(1);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer.Color(frontseatbeltcolor))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User selects front seat belt color");
    	
    	ActionHandler.click(DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer.RearSeatBeltColorOption);
    	ActionHandler.wait(1);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer.Color(rearseatbelt))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User selects rear seat belt color");

    }
    
    @Then("^Click on save button of Design Brief$")
    public void Click_on_Save_button_of_Design_Brief() throws Exception{
    	
    	ActionHandler.click(DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer.SaveDesignBrief);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Save button of Design Brief");
    	
    }
    
    @Then("^Click on Edit button of cabin carpet color and select the color \"([^\"]*)\"$")
    public void Click_on_Edit_button_of_cabin_carpet_color(String cabinCarpetColor) throws Exception{
    	
    	String Ccarpetcolor[] = cabinCarpetColor.split(",");
    	cabinCarpetColor = CommonFunctions.readExcelMasterData(Ccarpetcolor[0], Ccarpetcolor[1], Ccarpetcolor[2]);

    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	
    	ActionHandler.click(DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer.EditCabinCarpetColor);
    	ActionHandler.wait(2);
    
    	ActionHandler.click(DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer.CabinColorCarpetOpt);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer.Color(cabinCarpetColor))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User selects Cabin Carpet color from drop down");
    	
    }
    
    @Then("^Click on Edit button for fridge material and select the material \"([^\"]*)\" color \"([^\"]*)\" and veneer color \"([^\"]*)\"$")
    public void Click_on_Edit_button_for_fridge_material_and_select_the_material_color(String fridgeMaterial, String fridgeColor, String veneerColor) throws Exception{
    
    	String fMaterial[] = fridgeMaterial.split(",");
    	fridgeMaterial = CommonFunctions.readExcelMasterData(fMaterial[0], fMaterial[1], fMaterial[2]);

    	String fColor[] = fridgeColor.split(",");
    	fridgeColor = CommonFunctions.readExcelMasterData(fColor[0], fColor[1], fColor[2]);

    	String vColor[] = veneerColor.split(",");
    	veneerColor = CommonFunctions.readExcelMasterData(vColor[0], vColor[1], vColor[2]);

    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	
    	ActionHandler.click(DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer.EditFridgeMaterialBtn);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on edit button of Fridge Material");
    	
    	ActionHandler.click(DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer.FridgeMaterialOption);
    	ActionHandler.wait(2);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer.Color(fridgeMaterial))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User selects the Fridge Material");

    	ActionHandler.click(DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer.VeneerColorOpt);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();    	
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer.Color(veneerColor))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User selects the veneer color");
    	
    	ActionHandler.click(DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer.FridgeColorOpt);
    	ActionHandler.wait(2);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer.Color(fridgeColor))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User selects the fridge color");
    	
    }
    
    @Then("^Click on Edit button for Headliner material and select the material \"([^\"]*)\" and color \"([^\"]*)\"$")
    public void Click_on_edit_button_for_Headliner_material_and_select_the_material(String HeadlinerMaterial, String HeadlinerColor) throws Exception{
    	
    	String HLMaterial[] = HeadlinerMaterial.split(",");
    	HeadlinerMaterial = CommonFunctions.readExcelMasterData(HLMaterial[0], HLMaterial[1], HLMaterial[2]);

    	String HLColor[] = HeadlinerColor.split(",");
    	HeadlinerColor = CommonFunctions.readExcelMasterData(HLColor[0], HLColor[1], HLColor[2]);

    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	
    	ActionHandler.scrollToView(DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer.HeadlinerDetailsText);
    	ActionHandler.wait(1);
    	
    	ActionHandler.click(DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer.EditHeadlinerMaterialBtn);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Edit Headliner material button");
    	
    	ActionHandler.click(DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer.HeadLinerMaterialText);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer.Color(HeadlinerMaterial))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User selects Headliner material");

    	ActionHandler.click(DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer.HeadlinerColorText);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer.Color(HeadlinerColor))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User selects Headliner Color");
    	
    }
    
    @Then("^Click on Edit button for front seat belt color and select the seat color as Ebony \"([^\"]*)\"$")
    public void Click_on_Edit_button_for_front_seat_belt_color(String frontseatbeltcolor) throws Exception{
    	
    	String fseatbeltcolor[] = frontseatbeltcolor.split(",");
    	frontseatbeltcolor = CommonFunctions.readExcelMasterData(fseatbeltcolor[0], fseatbeltcolor[1], fseatbeltcolor[2]);

    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	
    	ActionHandler.click(DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer.EditFrontSeatBeltColorBtn);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on edit front seat color button");
    	
    	ActionHandler.scrollToView(DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer.SeatBeltDetails);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	
    	ActionHandler.click(DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer.FrontSeatBeltColorOption);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer.Color(frontseatbeltcolor))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User selects front seat color option from drop down");
    	
    	
    }
    
    @Then("^Click on Edit button for fridge material and select the material as PU \"([^\"]*)\"$")
    public void Click_on_Edit_button_for_fridge_material(String fridgeMaterial) throws Exception{
    	
    	String fMaterial[] = fridgeMaterial.split(",");
    	fridgeMaterial = CommonFunctions.readExcelMasterData(fMaterial[0], fMaterial[1], fMaterial[2]);

    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	
    	ActionHandler.click(DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer.EditFridgeMaterialBtn);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on edit fridge material button");
    	
    	ActionHandler.click(DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer.FridgeMaterialOption);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer.Color(fridgeMaterial))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User selects fridge material from frop down");

    }
    
    @Then("^Verify the color list for fridge is changed according to the material$")
    public void Verify_the_color_list_for_fridge_is_changed() throws Exception{
    	
    	ActionHandler.click(DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer.VeneerColorOpt);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User verifies the fridge veneer color option from drop down");
    	
    	ActionHandler.click(DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer.FridgeColorOpt);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User verifies the fridge color option from drop down");
    	
    }



    






}
