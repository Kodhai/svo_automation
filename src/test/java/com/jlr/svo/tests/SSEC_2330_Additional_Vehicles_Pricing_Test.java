package com.jlr.svo.tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.containers.SSEC_1226_CaseCreationEmail_Container;
import com.jlr.svo.containers.SSEC_2119_AdditionalVehicleName_AutoNumberContainer;
import com.jlr.svo.containers.SSEC_2330_AdditionalVehiclesPricingContainer;
import com.jlr.svo.containers.SVOAccountsContainer;
import com.jlr.svo.containers.SVOAdditionalVehicleContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.containers.SVOItemToApproveContainer;
import com.jlr.svo.containers.SVOOpportunityContainer;
import com.jlr.svo.containers.SVO_CustomerResponsesContainer;
import com.jlr.svo.containers.SVO_EnquiryLostReasonContainer;
import com.jlr.svo.containers.SVO_RestrictedPartyScreeningContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class SSEC_2330_Additional_Vehicles_Pricing_Test extends TestBaseCC {
	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory.getLogger(SSEC_1226_CaseCreationEmail_Test.class.getName());
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);

	SVOAccountsContainer SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
	SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	SVOAdditionalVehicleContainer SVOAdditionalvehicleContainer = PageFactory.initElements(driver,
			SVOAdditionalVehicleContainer.class);
	SVO_RestrictedPartyScreeningContainer RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
			SVO_RestrictedPartyScreeningContainer.class);
	SVOItemToApproveContainer SVOItemToapproveContainer = PageFactory.initElements(driver,
			SVOItemToApproveContainer.class);
	SVO_CustomerResponsesContainer SVO_CustomerResponsesContainer = PageFactory.initElements(driver,
			SVO_CustomerResponsesContainer.class);
	SVO_EnquiryLostReasonContainer SVO_EnquiryLostReasonContainer = PageFactory.initElements(driver,
			SVO_EnquiryLostReasonContainer.class);
	SSEC_1226_CaseCreationEmail_Container CaseCreationEmailContainer = PageFactory.initElements(driver,
			SSEC_1226_CaseCreationEmail_Container.class);
	SSEC_2119_AdditionalVehicleName_AutoNumberContainer AdditionalVehicleName_AutoNumberContainer = PageFactory.initElements(driver, SSEC_2119_AdditionalVehicleName_AutoNumberContainer.class);
	SSEC_2330_AdditionalVehiclesPricingContainer SSEC_2330_AdditionalVehiclesPricingContainer = PageFactory.initElements(driver, SSEC_2330_AdditionalVehiclesPricingContainer.class);
	
	public static String verificationCode;
	public static String vCode;
	public static String veriCode;
	public String NewWindow;
	public static String CaseCreated;
	public static String CaseNumber;

	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	public void onStart() {

		setupTest("SVOTest");
		driver = getDriver();

		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
		SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
		SVOAdditionalvehicleContainer = PageFactory.initElements(driver, SVOAdditionalVehicleContainer.class);

		RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
				SVO_RestrictedPartyScreeningContainer.class);
		SVOItemToapproveContainer = PageFactory.initElements(driver, SVOItemToApproveContainer.class);
		SVO_CustomerResponsesContainer = PageFactory.initElements(driver, SVO_CustomerResponsesContainer.class);
		SVO_EnquiryLostReasonContainer = PageFactory.initElements(driver, SVO_EnquiryLostReasonContainer.class);
		CaseCreationEmailContainer = PageFactory.initElements(driver, SSEC_1226_CaseCreationEmail_Container.class);
		AdditionalVehicleName_AutoNumberContainer = PageFactory.initElements(driver, SSEC_2119_AdditionalVehicleName_AutoNumberContainer.class);
		SSEC_2330_AdditionalVehiclesPricingContainer = PageFactory.initElements(driver, SSEC_2330_AdditionalVehiclesPricingContainer.class);
		
		driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);

		verificationCode = null;
		vCode = null;
		veriCode = null;
		NewWindow = null;
		CaseCreated = null;
		CaseNumber = null;

	}
	
    @Then("^Clicks on Opportunity record$")
    public void Clicks_on_Opportunity_record() throws Exception{
    	
    	ActionHandler.click(SSEC_2330_AdditionalVehiclesPricingContainer.OpportunityRec);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Clicks on Opportunity record");
    }

    @And("^Verify Total Retail Price$")
    public void Verify_Total_Retail_Price() throws Exception{
    	
    	VerifyHandler.verifyElementPresent(SSEC_2330_AdditionalVehiclesPricingContainer.TotalRetailPriceText);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Verify Total Retail Price on the Opportunity tab");
    	
    }
    
    @And("^Verify Total List Price$")
    public void Verify_Total_List_Price() throws Exception{
    	
    	VerifyHandler.verifyElementPresent(SSEC_2330_AdditionalVehiclesPricingContainer.TotalListPriceText);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Verify Total List Price on the Opportunity Tab");
    	
    }
    
    @And("^Verify Total Cost Price$")
    public void Verify_Total_Cost_Price() throws Exception{
    	
    	VerifyHandler.verifyElementPresent(SSEC_2330_AdditionalVehiclesPricingContainer.TotalCostPriceText);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Verify Total Cost Price on the Opportunity Tab");
    	
    }
    
    @And("^Verify User is able to edit Retail price on the Opportunity tab$")
    public void Verify_User_is_able_to_edit_Retail_Price() throws Exception{
    	
    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	
    	ActionHandler.click(SSEC_2330_AdditionalVehiclesPricingContainer.EditRetailPriceBtn);
    	ActionHandler.wait(1);
    	ActionHandler.click(SSEC_2330_AdditionalVehiclesPricingContainer.RetailPriceLabel);
    	ActionHandler.wait(1);
    	ActionHandler.setText(SSEC_2330_AdditionalVehiclesPricingContainer.RetailPriceLabel, "900000");
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Verify User is able to edit Retail price on the Opportunity tab");
    	
    	ActionHandler.click(SSEC_2330_AdditionalVehiclesPricingContainer.SaveOpportunityBtn);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User is able to Save the Opportunity");
    	
    }
    
    @And("^Verify User is able to mark stage as Complete on Opportunity Tab$")
    public void Verify_User_is_able_to_mark_stageAs_Complete() throws Exception{
    	
    	ActionHandler.click(SSEC_2330_AdditionalVehiclesPricingContainer.MarkStageAsCompleteBtn);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User is able to Mark Stage as Complete");
    	
    }







}
