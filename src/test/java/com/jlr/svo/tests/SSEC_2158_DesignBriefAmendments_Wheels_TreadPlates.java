package com.jlr.svo.tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.containers.SSEC_1039_SVOEnquiryEmailRemaindersContainer;
import com.jlr.svo.containers.SSEC_1503_Clasic_Email_Signature_On_Salesforce_Container;
import com.jlr.svo.containers.SSEC_2157_DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer;
import com.jlr.svo.containers.SSEC_2158_DesignBriefAmendments_Wheels_TreadPlatesContainer;
import com.jlr.svo.containers.SSEC_2159_BespokeOrderForm_CreationofOrderFormRecordsContainer;
import com.jlr.svo.containers.SVOAccountsContainer;
import com.jlr.svo.containers.SVOAdditionalVehicleContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.containers.SVOItemToApproveContainer;
import com.jlr.svo.containers.SVOOpportunityContainer;
import com.jlr.svo.containers.SVO_CustomerResponsesContainer;
import com.jlr.svo.containers.SVO_EnquiryLostReasonContainer;
import com.jlr.svo.containers.SVO_RestrictedPartyScreeningContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.Then;

public class SSEC_2158_DesignBriefAmendments_Wheels_TreadPlates extends TestBaseCC {

	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory
			.getLogger(SSEC_1039_SVOEnquiryEmailRemaindersTest.class.getName());
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);

	SVOAccountsContainer SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
	SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	SVOAdditionalVehicleContainer SVOAdditionalvehicleContainer = PageFactory.initElements(driver,
			SVOAdditionalVehicleContainer.class);
	SVO_RestrictedPartyScreeningContainer RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
			SVO_RestrictedPartyScreeningContainer.class);
	SVOItemToApproveContainer SVOItemToapproveContainer = PageFactory.initElements(driver,
			SVOItemToApproveContainer.class);
	SVO_CustomerResponsesContainer SVO_CustomerResponsesContainer = PageFactory.initElements(driver,
			SVO_CustomerResponsesContainer.class);
	SVO_EnquiryLostReasonContainer SVO_EnquiryLostReasonContainer = PageFactory.initElements(driver,
			SVO_EnquiryLostReasonContainer.class);
	SSEC_1039_SVOEnquiryEmailRemaindersContainer SVOEnquiryEmailRemaindersContainer = PageFactory.initElements(driver,
			SSEC_1039_SVOEnquiryEmailRemaindersContainer.class);
	SSEC_1503_Clasic_Email_Signature_On_Salesforce_Container Clasic_Email_Signature_On_Salesforce_Container = PageFactory.initElements(driver,
			SSEC_1503_Clasic_Email_Signature_On_Salesforce_Container.class);
	SSEC_2159_BespokeOrderForm_CreationofOrderFormRecordsContainer BespokeOrderForm_CreationofOrderFormRecordsContainer = PageFactory.initElements(driver, SSEC_2159_BespokeOrderForm_CreationofOrderFormRecordsContainer.class);
	SSEC_2157_DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer = PageFactory.initElements(driver, SSEC_2157_DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer.class);
	SSEC_2158_DesignBriefAmendments_Wheels_TreadPlatesContainer DesignBriefAmendments_Wheels_TreadPlatesContainer = PageFactory.initElements(driver, SSEC_2158_DesignBriefAmendments_Wheels_TreadPlatesContainer.class);
	
	public static String verificationCode;
	public static String vCode;
	public static String veriCode;
	public static String EnquiryName;

	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}
	

	public void onStart() {

		setupTest("SVOTest");
		driver = getDriver();

		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
		SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
		SVOAdditionalvehicleContainer = PageFactory.initElements(driver, SVOAdditionalVehicleContainer.class);

		RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
				SVO_RestrictedPartyScreeningContainer.class);
		SVOItemToapproveContainer = PageFactory.initElements(driver, SVOItemToApproveContainer.class);
		SVO_CustomerResponsesContainer = PageFactory.initElements(driver, SVO_CustomerResponsesContainer.class);
		SVO_EnquiryLostReasonContainer = PageFactory.initElements(driver, SVO_EnquiryLostReasonContainer.class);
		SVOEnquiryEmailRemaindersContainer = PageFactory.initElements(driver,
				SSEC_1039_SVOEnquiryEmailRemaindersContainer.class);
		Clasic_Email_Signature_On_Salesforce_Container = PageFactory.initElements(driver,
				SSEC_1503_Clasic_Email_Signature_On_Salesforce_Container.class);
		BespokeOrderForm_CreationofOrderFormRecordsContainer = PageFactory.initElements(driver, SSEC_2159_BespokeOrderForm_CreationofOrderFormRecordsContainer.class);
		DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer = PageFactory.initElements(driver, SSEC_2157_DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer.class);
		DesignBriefAmendments_Wheels_TreadPlatesContainer = PageFactory.initElements(driver, SSEC_2158_DesignBriefAmendments_Wheels_TreadPlatesContainer.class);
		
		
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		

		verificationCode = null;
		vCode = null;
		EnquiryName = null;

	}
	
    @Then("^Click on Edit button of Wheel Inserts and select inserts \"([^\"]*)\" and color \"([^\"]*)\" from the drop down$")
    public void Click_on_Edit_button_of_Wheel_Inserts_and_select_the_details(String WheelInserts, String WheelColor) throws Exception{
    	
    	String WInserts[] = WheelInserts.split(",");
    	WheelInserts = CommonFunctions.readExcelMasterData(WInserts[0], WInserts[1], WInserts[2]);

    	String WColor[] = WheelColor.split(",");
    	WheelColor = CommonFunctions.readExcelMasterData(WColor[0], WColor[1], WColor[2]);

    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	
    	ActionHandler.click(DesignBriefAmendments_Wheels_TreadPlatesContainer.EditWheelInsertsBtn);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Edit Wheel Inserts button");
    	
    	ActionHandler.scrollToView(DesignBriefAmendments_Wheels_TreadPlatesContainer.WheelDetailsText);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User scrolls to view Wheels Details text");
    	
    	ActionHandler.click(DesignBriefAmendments_Wheels_TreadPlatesContainer.WheelInsertsDropDown);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_Wheels_TreadPlatesContainer.Wheels(WheelInserts))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Wheel Inserts Drop Down");
    	
    	ActionHandler.click(DesignBriefAmendments_Wheels_TreadPlatesContainer.WheelColorDropDown);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_Wheels_TreadPlatesContainer.Wheels(WheelColor))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Wheel Color Drop Down");
    	
    	
    }
    
    @Then("^Click on Edit button of Upper trim and select lower \"([^\"]*)\" upper trim \"([^\"]*)\" and treadplates personalisation \"([^\"]*)\"$")
    public void Click_on_Edit_button_of_treadplates_and_select_all_the_details(String lowerTrimColor, String UpperTrimColor, String TPPersonalisation) throws Exception{
    	
    	String lTrimColor[] = lowerTrimColor.split(",");
    	lowerTrimColor = CommonFunctions.readExcelMasterData(lTrimColor[0], lTrimColor[1], lTrimColor[2]);

    	String UTrimColor[] = UpperTrimColor.split(",");
    	UpperTrimColor = CommonFunctions.readExcelMasterData(UTrimColor[0], UTrimColor[1], UTrimColor[2]);

    	String TPPersonal[] = TPPersonalisation.split(",");
    	TPPersonalisation = CommonFunctions.readExcelMasterData(TPPersonal[0], TPPersonal[1], TPPersonal[2]);

    	ActionHandler.pageDown();
    	ActionHandler.pageDown();
    	ActionHandler.pageDown();
    	ActionHandler.pageDown();
    	
        ActionHandler.click(DesignBriefAmendments_Wheels_TreadPlatesContainer.EditLowerTrimColor);
        ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Edit Lower Trim Color button");
    	
    	ActionHandler.scrollToView(DesignBriefAmendments_Wheels_TreadPlatesContainer.TreadplateTrimDetailsText);
    	ActionHandler.wait(2);
    	Reporter.addStepLog("User scrolls to view Treadplate details");
    	
    	ActionHandler.click(DesignBriefAmendments_Wheels_TreadPlatesContainer.LowerTrimColorDropDown);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_Wheels_TreadPlatesContainer.Wheels(lowerTrimColor))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Lower Trim Color Drop Down");
    	
    	ActionHandler.click(DesignBriefAmendments_Wheels_TreadPlatesContainer.UpperTrimColorDropDown);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_Wheels_TreadPlatesContainer.Wheels(UpperTrimColor))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Upper Trim Color Drop Down");
    	
    	ActionHandler.click(DesignBriefAmendments_Wheels_TreadPlatesContainer.TreadPlatePersonalization);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_Wheels_TreadPlatesContainer.Wheels(TPPersonalisation))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Treadplate personalisation");
    	
    }
    
    @Then("^Click on Edit button of Headrest material and select all the fields as HeadRestMaterial \"([^\"]*)\" FrontSeatFrontSquab \"([^\"]*)\" FrontSeatRearSquab \"([^\"]*)\" RearSeatFrontSquab \"([^\"]*)\" RearSeatRearSquab \"([^\"]*)\" Perforation \"([^\"]*)\"$")
    public void Click_on_Edit_button_of_Headrest_material_and_select_all_fields(String HeadRestMaterial, String FrontFSquab, String FrontRSquab, String RearFSquab, String RearRSquab, String Perforation) throws Exception{
    	
    	String HRMaterial[] = HeadRestMaterial.split(",");
    	HeadRestMaterial = CommonFunctions.readExcelMasterData(HRMaterial[0], HRMaterial[1], HRMaterial[2]);

    	String FFSquab[] = FrontFSquab.split(",");
    	FrontFSquab = CommonFunctions.readExcelMasterData(FFSquab[0], FFSquab[1], FFSquab[2]);

    	String FRSquab[] = FrontRSquab.split(",");
    	FrontRSquab = CommonFunctions.readExcelMasterData(FRSquab[0], FRSquab[1], FRSquab[2]);

    	String RFSquab[] = RearFSquab.split(",");
    	RearFSquab = CommonFunctions.readExcelMasterData(RFSquab[0], RFSquab[1], RFSquab[2]);

    	String RRSquab[] = RearRSquab.split(",");
    	RearRSquab = CommonFunctions.readExcelMasterData(RRSquab[0], RRSquab[1], RRSquab[2]);

    	String Perf[] = Perforation.split(",");
    	Perforation = CommonFunctions.readExcelMasterData(Perf[0], Perf[1], Perf[2]);

    	ActionHandler.pageDown();
    	ActionHandler.pageDown();
    	ActionHandler.pageDown();
    	ActionHandler.pageDown();
    	
        ActionHandler.click(DesignBriefAmendments_Wheels_TreadPlatesContainer.EditHeadrestMaterial);
        ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Edit HeadRest Material");
    	
    	ActionHandler.scrollToView(DesignBriefAmendments_Wheels_TreadPlatesContainer.HeadRestDetailsText);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User scrolls to view HeadRest Details");
    	
    	ActionHandler.click(DesignBriefAmendments_Wheels_TreadPlatesContainer.HeadRestMaterialDropDown);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_Wheels_TreadPlatesContainer.Wheels(HeadRestMaterial))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on HeadRest Material Drop Down");
    	
    	ActionHandler.click(DesignBriefAmendments_Wheels_TreadPlatesContainer.FrontSeatFrontSquabDropDown);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_Wheels_TreadPlatesContainer.Wheels(FrontFSquab))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Front Seat Front Squab Drop Down");
    	
    	ActionHandler.click(DesignBriefAmendments_Wheels_TreadPlatesContainer.FrontSeatRearSquabDropDown);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_Wheels_TreadPlatesContainer.Wheels(FrontRSquab))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Front Seat Rear Squab Drop Down");
    	
    	ActionHandler.click(DesignBriefAmendments_Wheels_TreadPlatesContainer.RearSeateatFrontSquabDropDown);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_Wheels_TreadPlatesContainer.Wheels(RearFSquab))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Rear Seat Front Squab Drop Down");
    	
    	ActionHandler.click(DesignBriefAmendments_Wheels_TreadPlatesContainer.RearSeatRearSquabDropDown);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_Wheels_TreadPlatesContainer.Wheels(RearRSquab))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Rear Seat Rear Squab Drop Down");
    	
    	ActionHandler.click(DesignBriefAmendments_Wheels_TreadPlatesContainer.PerforationDropDown);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_Wheels_TreadPlatesContainer.Wheels(Perforation))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Perforation Drop Down");
    	
    	
    }
    
    @Then("^Click on Edit button of Rear center console material and select the fields as RearConsoleMaterial \"([^\"]*)\" RearConsoleArmRest \"([^\"]*)\" RearConsoleFinisher \"([^\"]*)\" RearConsoleSides \"([^\"]*)\"$")
    public void Click_on_Edit_button_of_Rear_center_console_material(String RearConsoleMaterial, String RearConsoleArmRest, String RearConsoleFinisherColor, String RearConsoleSides) throws Exception{
    	
    	String RCMaterial[] = RearConsoleMaterial.split(",");
    	RearConsoleMaterial = CommonFunctions.readExcelMasterData(RCMaterial[0], RCMaterial[1], RCMaterial[2]);

    	String RCArmRest[] = RearConsoleArmRest.split(",");
    	RearConsoleArmRest = CommonFunctions.readExcelMasterData(RCArmRest[0], RCArmRest[1], RCArmRest[2]);

    	String RCFinisherColor[] = RearConsoleFinisherColor.split(",");
    	RearConsoleFinisherColor = CommonFunctions.readExcelMasterData(RCFinisherColor[0], RCFinisherColor[1], RCFinisherColor[2]);

    	String RCSides[] = RearConsoleSides.split(",");
    	RearConsoleSides = CommonFunctions.readExcelMasterData(RCSides[0], RCSides[1], RCSides[2]);

    	ActionHandler.pageDown();
    	ActionHandler.pageDown();
    	ActionHandler.pageDown();
    	ActionHandler.pageDown();
    	
        ActionHandler.click(DesignBriefAmendments_Wheels_TreadPlatesContainer.EditRearCenterConsoleMaterial);
        ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Edit Rear Center COnsole Material button");
    	
    	ActionHandler.scrollToView(DesignBriefAmendments_Wheels_TreadPlatesContainer.RearCenterConsoleDetailsText);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User scrolls to view Rear Center Console Details");
    	
    	ActionHandler.click(DesignBriefAmendments_Wheels_TreadPlatesContainer.RearCenterConsoleMaterialDropDown);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_Wheels_TreadPlatesContainer.Wheels(RearConsoleMaterial))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Rear Center Console Material Drop Down");
    	
    	ActionHandler.click(DesignBriefAmendments_Wheels_TreadPlatesContainer.RearCenterConsoleArmRestDropDown);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_Wheels_TreadPlatesContainer.Wheels(RearConsoleArmRest))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Rear Center Console Arm Rest Drop Down");
    	
    	ActionHandler.click(DesignBriefAmendments_Wheels_TreadPlatesContainer.RearCenterFinisherColorDropDown);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_Wheels_TreadPlatesContainer.Wheels(RearConsoleFinisherColor))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Rear Center Finisher Color Drop Down");
    	
    	ActionHandler.click(DesignBriefAmendments_Wheels_TreadPlatesContainer.RearCenterConsoleSidesDropDown);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_Wheels_TreadPlatesContainer.Wheels(RearConsoleSides))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Rear Center Console Sides Drop Down");
    	
    	
    }
    
    @Then("^Click on Edit button of Wheels inserts \"([^\"]*)\" color and select the color \"([^\"]*)\"$")
    public void Click_on_Edit_button_of_Wheels_color_and_select_the_color(String WheelInserts, String WheelColor) throws Exception{
    	
    	String WInserts[] = WheelInserts.split(",");
    	WheelInserts = CommonFunctions.readExcelMasterData(WInserts[0], WInserts[1], WInserts[2]);

    	String WColor[] = WheelColor.split(",");
    	WheelColor = CommonFunctions.readExcelMasterData(WColor[0], WColor[1], WColor[2]);

    	ActionHandler.pageDown();
    	ActionHandler.pageDown();
    	ActionHandler.pageDown();
    	ActionHandler.pageDown();
    	
    	ActionHandler.click(DesignBriefAmendments_Wheels_TreadPlatesContainer.EditWheelInsertsBtn);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Edit Wheel Inserts button");
    	
    	ActionHandler.scrollToView(DesignBriefAmendments_Wheels_TreadPlatesContainer.WheelDetailsText);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User scrolls to view Wheel Details Text");
    	
    	ActionHandler.click(DesignBriefAmendments_Wheels_TreadPlatesContainer.WheelInsertsDropDown);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_Wheels_TreadPlatesContainer.Wheels(WheelInserts))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Wheel Inserts Drop Down");
    	
    	ActionHandler.click(DesignBriefAmendments_Wheels_TreadPlatesContainer.WheelColorDropDown);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_Wheels_TreadPlatesContainer.Wheels(WheelColor))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Wheel Color Drop Down");
    	
    }
    
    @Then("^Click on Edit button of Headrest matrial and select as PU \"([^\"]*)\"$")
    public void Click_on_Edit_button_of_Headrest_material_and_select_as_PU(String HeadRestMaterial) throws Exception{
    	
    	String HRMaterial[] = HeadRestMaterial.split(",");
    	HeadRestMaterial = CommonFunctions.readExcelMasterData(HRMaterial[0], HRMaterial[1], HRMaterial[2]);

    	ActionHandler.pageDown();
    	ActionHandler.pageDown();
    	ActionHandler.pageDown();
    	ActionHandler.pageDown();
    	
        ActionHandler.click(DesignBriefAmendments_Wheels_TreadPlatesContainer.EditHeadrestMaterial);
        ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Edit HeadRest Material button");
    	
    	ActionHandler.scrollToView(DesignBriefAmendments_Wheels_TreadPlatesContainer.HeadRestDetailsText);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User scrolls to view HeadRest Details");
    	
    	ActionHandler.click(DesignBriefAmendments_Wheels_TreadPlatesContainer.HeadRestMaterialDropDown);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_Wheels_TreadPlatesContainer.Wheels(HeadRestMaterial))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on HeadRest Material Drop Down");
    	
    }
    
    @Then("^Verify the color list for frontseatfrontsquab \"([^\"]*)\" and frontseatrearsquab \"([^\"]*)\" Headrest is changed according to the material$")
    public void Verify_the_color_list_for_Headrest_is_changed_accordingly(String FrontFSquab, String FrontRSquab) throws Exception{
    	
    	String FFSquab[] = FrontFSquab.split(",");
    	FrontFSquab = CommonFunctions.readExcelMasterData(FFSquab[0], FFSquab[1], FFSquab[2]);

    	String FRSquab[] = FrontRSquab.split(",");
    	FrontRSquab = CommonFunctions.readExcelMasterData(FRSquab[0], FRSquab[1], FRSquab[2]);

    	ActionHandler.click(DesignBriefAmendments_Wheels_TreadPlatesContainer.FrontSeatFrontSquabDropDown);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_Wheels_TreadPlatesContainer.Wheels(FrontFSquab))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Front Seat Front Squab Drop Down");
    	
    	ActionHandler.click(DesignBriefAmendments_Wheels_TreadPlatesContainer.FrontSeatRearSquabDropDown);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_Wheels_TreadPlatesContainer.Wheels(FrontRSquab))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Front Seat Rear Squab Drop Down");
    	
    	
    }

}
