package com.jlr.svo.tests;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.autotest.utils.Config;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.constants.Constants;
import com.jlr.svo.containers.SSEC_1226_CaseCreationEmail_Container;
import com.jlr.svo.containers.SSEC_1228_LinkContactAndAccountTocase_Container;
import com.jlr.svo.containers.SSEC_1233_CaseEmailTemplate_Container;
import com.jlr.svo.containers.SSEC_1234_CaseListViews_Container;
import com.jlr.svo.containers.SVOAccountsContainer;
import com.jlr.svo.containers.SVOAdditionalVehicleContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.containers.SVOItemToApproveContainer;
import com.jlr.svo.containers.SVOOpportunityContainer;
import com.jlr.svo.containers.SVO_CustomerResponsesContainer;
import com.jlr.svo.containers.SVO_EnquiryLostReasonContainer;
import com.jlr.svo.containers.SVO_RestrictedPartyScreeningContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class SSEC_1233_CaseEmailTemplate_Test extends TestBaseCC {
	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory.getLogger(SSEC_1233_CaseEmailTemplate_Test.class.getName());
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
///
	SVOAccountsContainer SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
	SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	SVOAdditionalVehicleContainer SVOAdditionalvehicleContainer = PageFactory.initElements(driver,
			SVOAdditionalVehicleContainer.class);
	SVO_RestrictedPartyScreeningContainer RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
			SVO_RestrictedPartyScreeningContainer.class);
	SVOItemToApproveContainer SVOItemToapproveContainer = PageFactory.initElements(driver,
			SVOItemToApproveContainer.class);
	SVO_CustomerResponsesContainer SVO_CustomerResponsesContainer = PageFactory.initElements(driver,
			SVO_CustomerResponsesContainer.class);
	SVO_EnquiryLostReasonContainer SVO_EnquiryLostReasonContainer = PageFactory.initElements(driver,
			SVO_EnquiryLostReasonContainer.class);
	SSEC_1226_CaseCreationEmail_Container CaseCreationEmailContainer = PageFactory.initElements(driver,
			SSEC_1226_CaseCreationEmail_Container.class);
	SSEC_1228_LinkContactAndAccountTocase_Container LinkContactAndAccountTocaseContainer = PageFactory
			.initElements(driver, SSEC_1228_LinkContactAndAccountTocase_Container.class);
	SSEC_1234_CaseListViews_Test CaseListViews_Test = PageFactory.initElements(driver,
			SSEC_1234_CaseListViews_Test.class);
	SSEC_1234_CaseListViews_Container CaseListViewsContainer = PageFactory.initElements(driver,
			SSEC_1234_CaseListViews_Container.class);
	SSEC_1233_CaseEmailTemplate_Container CaseEmailTemplateContainer = PageFactory.initElements(driver,
			SSEC_1233_CaseEmailTemplate_Container.class);

	public static String verificationCode;
	public static String vCode;
	public static String veriCode;
	public String NewWindow;
	public static String CaseCreated;
	public static String CaseNumber;
	public static String ContactName;

	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	public void onStart() {

		setupTest("SVOTest");
		driver = getDriver();

		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
		SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
		SVOAdditionalvehicleContainer = PageFactory.initElements(driver, SVOAdditionalVehicleContainer.class);

		RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
				SVO_RestrictedPartyScreeningContainer.class);
		SVOItemToapproveContainer = PageFactory.initElements(driver, SVOItemToApproveContainer.class);
		SVO_CustomerResponsesContainer = PageFactory.initElements(driver, SVO_CustomerResponsesContainer.class);
		SVO_EnquiryLostReasonContainer = PageFactory.initElements(driver, SVO_EnquiryLostReasonContainer.class);
		CaseCreationEmailContainer = PageFactory.initElements(driver, SSEC_1226_CaseCreationEmail_Container.class);
		LinkContactAndAccountTocaseContainer = PageFactory.initElements(driver,
				SSEC_1228_LinkContactAndAccountTocase_Container.class);
		CaseListViewsContainer = PageFactory.initElements(driver, SSEC_1234_CaseListViews_Container.class);
		CaseListViews_Test = PageFactory.initElements(driver, SSEC_1234_CaseListViews_Test.class);
		CaseEmailTemplateContainer = PageFactory.initElements(driver, SSEC_1233_CaseEmailTemplate_Container.class);
		driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);

		verificationCode = null;
		vCode = null;
		veriCode = null;
		NewWindow = null;
		CaseCreated = null;
		CaseNumber = null;
		ContactName = null;

	}

	@Given("^Access to salesforce Portal as an Classic Customer Service user with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void access_to_salesforce_SVO_portal_as_an_Classic_Customer_Service_user_with_Username_and_Password(
			String userName, String password) throws Throwable {

		onStart();
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailOnCustomerServiceUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			ActionHandler.wait(1);
			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				CommonFunctions.attachScreenshot();

			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				CommonFunctions.attachScreenshot();

			} else {

				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {

			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	public String checkEmailOnCustomerServiceUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(1);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(494, 500);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	@And("^Navigate to Cases Tab$")
	public void Navigate_to_Cases_Tab() throws Throwable {

		ActionHandler.wait(1);
		javaScriptUtil.clickElementByJS(CaseCreationEmailContainer.CasesTab);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigates to Cases tab");
	}

	@And("^Create a new case on SVO of record type \"([^\"]*)\" by linking case with existing Account \"([^\"]*)\" and Contact \"([^\"]*)\" details$")
	public void create_a_new_case_SVO_of_record_type_by_linking_case_with_existing_Account_and_Contact_details(
			String RecordType, String Account, String Contact) throws Throwable {

		String RT[] = RecordType.split(",");
		RecordType = CommonFunctions.readExcelMasterData(RT[0], RT[1], RT[2]);

		String Acc[] = Account.split(",");
		Account = CommonFunctions.readExcelMasterData(Acc[0], Acc[1], Acc[2]);

		String C[] = Contact.split(",");
		Contact = CommonFunctions.readExcelMasterData(C[0], C[1], C[2]);

		double randomNumber = getRandomIntegerBetweenRange(0, 1000);

		ActionHandler.wait(1);
		ActionHandler.click(LinkContactAndAccountTocaseContainer.NewCaseButton);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on New button to create a case");

		ActionHandler.click(
				driver.findElement(By.xpath(LinkContactAndAccountTocaseContainer.CaseRecordTypeSelection(RecordType))));
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects the case record type as " + RecordType);

		ActionHandler.click(LinkContactAndAccountTocaseContainer.CaseNextButton);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Case Next button");

		ActionHandler.setText(LinkContactAndAccountTocaseContainer.FirstNameTxtBox, RecordType + randomNumber);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters first name for the new case");

		ActionHandler.setText(LinkContactAndAccountTocaseContainer.LastNameTxtBox, "Test");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Last name for the new case");

		ActionHandler.click(LinkContactAndAccountTocaseContainer.AccountNameTxtBx);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Search and select an existing Account to link for the new case");

		ActionHandler.click(LinkContactAndAccountTocaseContainer.ContactNameTxtBx);
		ActionHandler.wait(1);
		ActionHandler.setText(LinkContactAndAccountTocaseContainer.ContactNameTxtBx, Contact);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(LinkContactAndAccountTocaseContainer.ContactSearchBar);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(LinkContactAndAccountTocaseContainer.AccountName);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Search and select an existing Contact to link with new case");

		ActionHandler.click(LinkContactAndAccountTocaseContainer.Vintxtbx);
		ActionHandler.wait(1);
		ActionHandler.setText(LinkContactAndAccountTocaseContainer.Vintxtbx, "67891762");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters VIN/chassis number");

		ActionHandler.click(LinkContactAndAccountTocaseContainer.SaveCaseButton);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Case save button");

		VerifyHandler.verifyElementHasFocus(LinkContactAndAccountTocaseContainer.CaseTitle);
		ActionHandler.wait(1);
		CaseNumber = LinkContactAndAccountTocaseContainer.CaseTitle.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Case is created successfully with case number : " + CaseNumber);

	}

	@Then("^User Logout from an SVO Portal$")
	public void User_Logout_from_an_SVO_Portal() throws Throwable {

		ActionHandler.click(SVO_OpportunityContainer.icon_image);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Logout");
		ActionHandler.click(SVO_OpportunityContainer.Logout);

		CommonFunctions.attachScreenshot();
		if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.userNameTextBox)) {
			MarkupHelper.createLabel("Logged out successfully", ExtentColor.GREEN);
		} else {
			MarkupHelper.createLabel("Log out failed", ExtentColor.RED);

		}

	}

	@Then("^User Navigate to Activity tab$")
	public void User_Navigate_to_Activity_tab() throws Throwable {
		ActionHandler.wait(1);
		ActionHandler.click(CaseEmailTemplateContainer.ActivityTab);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigates to Activity tab");

	}

	@Then("^Verify and send an email to customer \"([^\"]*)\" with template \"([^\"]*)\" on compose email section$")
	public void Verify_and_send_an_email_to_customer_with_template_on_compose_email_section(String Customer,
			String Template) throws Throwable {

		String C[] = Customer.split(",");
		Customer = CommonFunctions.readExcelMasterData(C[0], C[1], C[2]);

		String T[] = Template.split(",");
		Template = CommonFunctions.readExcelMasterData(T[0], T[1], T[2]);

		ActionHandler.wait(1);
		javaScriptUtil.clickElementByJS(CaseEmailTemplateContainer.ComposeEmailButton);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks on compose email button");

		ActionHandler.wait(1);
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.wait(1);

		javaScriptUtil.clickElementByJS(CaseEmailTemplateContainer.EmailTemplateIcon);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on email template icon");

		ActionHandler.click(CaseEmailTemplateContainer.InsertEmailTemplate);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on insert email template tab");

		ActionHandler.click(CaseEmailTemplateContainer.SearchEmailTemplateTxtBx);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseEmailTemplateContainer.SearchEmailTemplateTxtBx, Template);
		ActionHandler.wait(1);
		ActionHandler.pressEnter();
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User searched for the email template : " + Template);

		ActionHandler.click(driver.findElement(By.xpath(CaseEmailTemplateContainer.TemplateSelection(Template))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects the Email template as " + Template);

		if (VerifyHandler.verifyElementPresent(CaseEmailTemplateContainer.EmailTemplateFrame)) {
			ActionHandler.wait(1);
			ActionHandler.pageUp();
			ActionHandler.wait(1);
			driver.switchTo().frame(0);
			if (VerifyHandler.verifyElementPresent(CaseEmailTemplateContainer.EmailTemplateContent)) {
				String content = CaseEmailTemplateContainer.EmailTemplateContent.getText();
				ActionHandler.wait(1);
				CommonFunctions.attachScreenshot();
				Reporter.addStepLog("User verifies the email template that is added");
				Reporter.addStepLog("Template content : " + content);

			}
			driver.switchTo().parentFrame();
		}

		ActionHandler.wait(1);
		javaScriptUtil.clickElementByJS(CaseEmailTemplateContainer.EmailSendButton);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verified the email template and clicks on email send button");
	}

	@Given("^Login to the email account with Username \"([^\"]*)\" and password \"([^\"]*)\"$")
	public void login_to_the_email_account_with_Username_and_password(String UserName, String Password)
			throws Throwable {

		String s[] = UserName.split(",");
		UserName = com.jlr.svo.utilities.CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String p[] = Password.split(",");
		Password = CommonFunctions.readExcelMasterData(p[0], p[1], p[2]);

		driver = getDriver();
		ActionHandler.wait(5);
		driver.get(Constants.gmailURL);

		Reporter.addStepLog("User tries to login to Gmail");
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(1);
		ActionHandler.setText(SVO_CustomerResponsesContainer.outlookEmailId, UserName);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(1);
		ActionHandler.click(SVO_CustomerResponsesContainer.gmailNext);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVO_CustomerResponsesContainer.gmailPassword, Password);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(1);
		ActionHandler.click(SVO_CustomerResponsesContainer.gmailNext);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User logged into Gmail Account");

		VerifyHandler.verifyElementPresent(SVO_CustomerResponsesContainer.gmailText);

	}

	@Given("^Verify that customer receives an email with the applied template \"([^\"]*)\"$")
	public void Verify_that_customer_receives_an_email_with_the_applied_template(String Template) throws Throwable {

		String T[] = Template.split(",");
		Template = CommonFunctions.readExcelMasterData(T[0], T[1], T[2]);

		ActionHandler.wait(3);
		driver.navigate().refresh();
		ActionHandler.wait(1);

		if (VerifyHandler.verifyElementPresent(
				driver.findElement(By.xpath(CaseEmailTemplateContainer.TemplateMail(Template))))) {
			ActionHandler.wait(1);
			ActionHandler.click(driver.findElement(By.xpath(CaseEmailTemplateContainer.TemplateMail(Template))));
			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User Verifies that customer receives an email with the applied template " + Template);
		}
	}

	@Then("^User logout from the user email account$")
	public void User_logout_from_the_user_email_account() throws Throwable {

		ActionHandler.click(SVO_CustomerResponsesContainer.GmailaccountLogo);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Gmail Account logo");

		driver.switchTo().frame(SVO_CustomerResponsesContainer.mailframe);
		ActionHandler.click(SVO_CustomerResponsesContainer.signoutGmailIcon);
		ActionHandler.wait(5);
		driver.switchTo().parentFrame();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Logout button");

	}

	@Given("^Access to salesforce Portal as Classic Parts and Technical user with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void access_to_salesforce_portal_as_Classic_Parts_and_Technical_user_with_Username_and_Password(
			String userName, String password) throws Throwable {

		onStart();
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailOnClassicPartsAndTechnicalUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				CommonFunctions.attachScreenshot();

			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				CommonFunctions.attachScreenshot();

			} else {

				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {

			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	public String checkEmailOnClassicPartsAndTechnicalUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(1);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(493, 500);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	@Given("^Login to the Classic Parts \"([^\"]*)\" portal$")
	public void login_to_the_Classic_Parts_portal(String Portal) throws Throwable {
		onStart();
		String portal[] = Portal.split(",");
		Portal = CommonFunctions.readExcelMasterData(portal[0], portal[1], portal[2]);

		if (Portal.equalsIgnoreCase("Classic Parts")) {
			driver.get("https://" + "iweb_26b06a" + ":" + "PJuR7ifMuPqr" + "@" + Constants.ClassicParts);
			ActionHandler.wait(1);
			Reporter.addStepLog("User access to Classic Parts portal");
			CommonFunctions.attachScreenshot();
		} else if (Portal.equalsIgnoreCase("Accessories")) {
			driver.get("https://" + "iweb_26b06a" + ":" + "PJuR7ifMuPqr" + "@" + Constants.Accessories);
			ActionHandler.wait(1);
			Reporter.addStepLog("User access to Accesssories portal");
			CommonFunctions.attachScreenshot();
		}

		ActionHandler.click(LinkContactAndAccountTocaseContainer.AllowCookies);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User accepts cookie policies");

	}

	@And("^User Navigates to Contact Us section$")
	public void User_Navigates_to_Contact_Us_section() throws Throwable {

		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(1);
		javaScriptUtil.clickElementByJS(LinkContactAndAccountTocaseContainer.ContactUsBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Contact Us section");

	}

	@Then("^User creates an Parts and Technical Case from web form with Email \"([^\"]*)\" First name \"([^\"]*)\" Last name \"([^\"]*)\" Country \"([^\"]*)\" Phone number \"([^\"]*)\" VIN number \"([^\"]*)\"$")
	public void User_Creates_an__Parts_and_Technical_case_from_web_form_with_email(String email, String first,
			String last, String country, String phone, String vin) throws Throwable {

		String em[] = email.split(",");
		email = CommonFunctions.readExcelMasterData(em[0], em[1], em[2]);

		String fname[] = first.split(",");
		first = CommonFunctions.readExcelMasterData(fname[0], fname[1], fname[2]);

		String lname[] = last.split(",");
		last = CommonFunctions.readExcelMasterData(lname[0], lname[1], lname[2]);

		String cty[] = country.split(",");
		country = CommonFunctions.readExcelMasterData(cty[0], cty[1], cty[2]);

		String num[] = phone.split(",");
		phone = CommonFunctions.readExcelMasterData(num[0], num[1], num[2]);

		String v[] = vin.split(",");
		vin = CommonFunctions.readExcelMasterData(v[0], v[1], v[2]);

		// enter first name
		ActionHandler.setText(LinkContactAndAccountTocaseContainer.FirstNameTxtBx, first);
		ActionHandler.wait(1);
		ActionHandler.setText(LinkContactAndAccountTocaseContainer.FirstNameTxtBx, first);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters First name as : " + first);

		// enter last name
		ActionHandler.setText(LinkContactAndAccountTocaseContainer.LastNameTxtBx, last);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Last name as : " + last);

		ActionHandler.wait(1);
		ActionHandler.setText(LinkContactAndAccountTocaseContainer.InputEmailAddress, email);
		ActionHandler.wait(1);
		ActionHandler.setText(LinkContactAndAccountTocaseContainer.InputEmailAddress, email);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters email as : " + email);

		// enter phone number
		ActionHandler.setText(LinkContactAndAccountTocaseContainer.PhoneNumberTxtBx, phone);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(LinkContactAndAccountTocaseContainer.HelpSelect);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act1.sendKeys(Keys.ENTER).build().perform();
		Reporter.addStepLog("User Selects Parts and Technical Enquiry from HelpSelect Drop down list");

		ActionHandler.click(LinkContactAndAccountTocaseContainer.VINNumber);
		ActionHandler.wait(1);
		ActionHandler.setText(LinkContactAndAccountTocaseContainer.VINNumber, vin);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters the vin number");

		// enter country
		ActionHandler.setText(LinkContactAndAccountTocaseContainer.CountrySelection, country);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		ActionHandler.pressEnter();
		ActionHandler.wait(1);
		Reporter.addStepLog("User enters country as : " + country);

		String comment = "test";
		ActionHandler.setText(LinkContactAndAccountTocaseContainer.CommentText, comment);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters the comment text");

		// click on continue
		javaScriptUtil.clickElementByJS(LinkContactAndAccountTocaseContainer.SubmitBtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Submit");
		Reporter.addStepLog("User verifies that contact us form submitted successfully");
	}

	@And("^Verify that user receives an outlook email notification on Parts and Technical case creation$")
	public void Verify_that_User_receives_an_outlook_email_notification_on_Parts_and_Technical_case_creation()
			throws Throwable {

		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get Case creation email");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		} else {
			ActionHandler.click(CaseCreationEmailContainer.SignInLink);
			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(1);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(CaseCreationEmailContainer.caseoutlookEmail);
				CommonFunctions.attachScreenshot();
				Reporter.addStepLog("User receives an case creation email");

				CaseCreated = CaseCreationEmailContainer.CaseNumber.getText();
				CaseNumber = CaseCreated.substring(278, 286);
				System.out.println("Created case = " + CaseNumber);
				Reporter.addStepLog("Case created : " + CaseNumber);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
	}

	@Given("^Login to the salesforce Portal as Classic Parts and Technical user with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void login_to_the_salesforce_portal_as_Classic_Parts_and_Technical_user_with_Username_and_Password(
			String userName, String password) throws Throwable {

		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailOnClassicPartsAndTechnicalUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				CommonFunctions.attachScreenshot();

			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				CommonFunctions.attachScreenshot();

			} else {

				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {

			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	@Then("^Verify and send an email to customer \"([^\"]*)\" by editing template \"([^\"]*)\" on compose email section$")
	public void Verify_and_send_an_email_to_customer_by_editing_template_on_compose_email_section(String Customer,
			String Template) throws Throwable {

		String C[] = Customer.split(",");
		Customer = CommonFunctions.readExcelMasterData(C[0], C[1], C[2]);

		String T[] = Template.split(",");
		Template = CommonFunctions.readExcelMasterData(T[0], T[1], T[2]);

		ActionHandler.wait(1);
		ActionHandler.scrollDown();
		ActionHandler.wait(1);

		javaScriptUtil.clickElementByJS(CaseEmailTemplateContainer.ComposeEmailButton);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks on compose email button");

		ActionHandler.wait(1);
		ActionHandler.scrollDown();
		ActionHandler.wait(1);

		javaScriptUtil.clickElementByJS(CaseEmailTemplateContainer.EmailTemplateIcon);
		ActionHandler.wait(1);
		ActionHandler.click(driver.findElement(By.xpath(CaseEmailTemplateContainer.TemplateSelection(Template))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects the Email template as " + Template);

		VerifyHandler.verifyElementPresent(CaseEmailTemplateContainer.EmailTemplateFrame);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the email template that is added");

		driver.switchTo().frame(CaseEmailTemplateContainer.EmailTemplateFrame);
		ActionHandler.click(CaseEmailTemplateContainer.EditTemplate);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseEmailTemplateContainer.EditTemplate, "Updated Template");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User updates the mail template");
		driver.switchTo().parentFrame();

		ActionHandler.click(CaseEmailTemplateContainer.EmailToTxtBox);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseEmailTemplateContainer.EmailToTxtBox, Customer);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters to address of a mail as " + Customer);

		ActionHandler.click(CaseEmailTemplateContainer.EmailSubjectTxtBox);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseEmailTemplateContainer.EmailSubjectTxtBox, Template + "_Test");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters to Subject of a mail as " + Template + "_Test");

		ActionHandler.wait(1);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);

		ActionHandler.wait(1);
		javaScriptUtil.clickElementByJS(CaseEmailTemplateContainer.EmailSendButton);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on email send button");
	}

	@Then("^Send an email to customer \"([^\"]*)\" after removing email template \"([^\"]*)\" on compose email section$")
	public void Send_an_email_to_customer_after_removing_email_template_on_compose_email_section(String Customer,
			String Template) throws Throwable {

		String C[] = Customer.split(",");
		Customer = CommonFunctions.readExcelMasterData(C[0], C[1], C[2]);

		String T[] = Template.split(",");
		Template = CommonFunctions.readExcelMasterData(T[0], T[1], T[2]);

		ActionHandler.wait(1);
		ActionHandler.scrollDown();
		ActionHandler.wait(1);

		javaScriptUtil.clickElementByJS(CaseEmailTemplateContainer.ComposeEmailButton);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks on compose email button");

		ActionHandler.wait(1);
		ActionHandler.scrollDown();
		ActionHandler.wait(1);

		javaScriptUtil.clickElementByJS(CaseEmailTemplateContainer.EmailTemplateIcon);
		ActionHandler.wait(1);
		ActionHandler.click(driver.findElement(By.xpath(CaseEmailTemplateContainer.TemplateSelection(Template))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects the Email template as " + Template);

		VerifyHandler.verifyElementPresent(CaseEmailTemplateContainer.EmailTemplateFrame);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the email template that is added");

		ActionHandler.click(CaseEmailTemplateContainer.RemoveEmailTemplate);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on the remove email template icon");

		ActionHandler.click(CaseEmailTemplateContainer.DiscordTemplate);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on the Discard email template icon");

		ActionHandler.click(CaseEmailTemplateContainer.EmailToTxtBox);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseEmailTemplateContainer.EmailToTxtBox, Customer);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters to address of a mail as " + Customer);

		ActionHandler.click(CaseEmailTemplateContainer.EmailSubjectTxtBox);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseEmailTemplateContainer.EmailSubjectTxtBox, Template + "_Test");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters to Subject of a mail as " + Template + "_Test");

		ActionHandler.wait(1);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);

		ActionHandler.wait(1);
		javaScriptUtil.clickElementByJS(CaseEmailTemplateContainer.EmailSendButton);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on email send button");
	}

	@Then("^Verify user is not able to send an email without madatory fields of email to customer \"([^\"]*)\" with template \"([^\"]*)\" on compose email section$")
	public void Verify_user_is_not_able_to_send_an_email_without_madatory_fields_of_email_to_customer_with_template_on_compose_email_section(
			String Customer, String Template) throws Throwable {

		String C[] = Customer.split(",");
		Customer = CommonFunctions.readExcelMasterData(C[0], C[1], C[2]);

		String T[] = Template.split(",");
		Template = CommonFunctions.readExcelMasterData(T[0], T[1], T[2]);

		ActionHandler.wait(1);
		ActionHandler.scrollDown();
		ActionHandler.wait(1);

		javaScriptUtil.clickElementByJS(CaseEmailTemplateContainer.ComposeEmailButton);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks on compose email button");

		ActionHandler.wait(1);
		ActionHandler.scrollDown();
		ActionHandler.wait(1);

		ActionHandler.click(CaseEmailTemplateContainer.EmailToTxtBox);
		ActionHandler.wait(1);
		if (VerifyHandler.verifyElementPresent(CaseEmailTemplateContainer.EMailToClearIcon)) {
			ActionHandler.click(CaseEmailTemplateContainer.EMailToClearIcon);
			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User clears To section of an email");
		}
		if (VerifyHandler.verifyElementPresent(CaseEmailTemplateContainer.EMailBCCClearIcon)) {
			ActionHandler.click(CaseEmailTemplateContainer.EMailBCCClearIcon);
			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User clears BCC section of an email");
		}

		ActionHandler.click(CaseEmailTemplateContainer.EmailSubjectTxtBox);
		ActionHandler.wait(1);
		ActionHandler.clearText(CaseEmailTemplateContainer.EmailSubjectTxtBox);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clears subject text of an email");

		ActionHandler.wait(1);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);

		ActionHandler.wait(1);
		javaScriptUtil.clickElementByJS(CaseEmailTemplateContainer.EmailSendButton);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on email send button");

		if (VerifyHandler.verifyElementPresent(CaseEmailTemplateContainer.EmailSendError)) {
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog(
					"An error message caught when user tries to send an email without mandatory fields of an email");
		}
	}

	@And("^User Verify that Classic Parts and Technical case is automatically created on SVO Portal$")
	public void user_verify_that_Classic_Parts_and_Technical_case_is_automatically_created_on_SVO_Portal()
			throws Throwable {

		ActionHandler.click(SVO_CustomerResponsesContainer.SelectOpportunityListView);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on select list view tab");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		ActionHandler.click(CaseCreationEmailContainer.PartsAndTechnicalQueue);
		ActionHandler.wait(3);
		Reporter.addStepLog("User chooses to view selected to Parts And Technical queue list");
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(CaseCreationEmailContainer.SearchCaseTxtBox, CaseNumber);
		ActionHandler.wait(2);
		ActionHandler.pressEnter();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User searches for the Case that is created automatically");

		VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.FirstOpportunityLink);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Verifies that the new Parts and Technical Case is created on SVO Portal");

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstOpportunityLink);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects the case that is created automatically on salesforce");

	}
}