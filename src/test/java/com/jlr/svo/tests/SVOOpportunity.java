package com.jlr.svo.tests;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.AssertHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.autotest.utils.Config;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.constants.Constants;
import com.jlr.svo.containers.SVOAccountsContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.containers.SVOOpportunityContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SVOOpportunity extends TestBaseCC {
	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory.getLogger(SVOOpportunity.class.getName());
	SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
	SVOAccountsContainer SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
	public static String verificationCode;
	public static String vCode;
	public static String veriCode;
	public static String Rnumber;
	public static String oppoName;

	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	public void onStart() {
		setupTest("SVOTest");
		driver = getDriver();
		SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		verificationCode = null;
		vCode = null;
		Rnumber = null;
	}

	public void AllOpportunity() throws Exception {
		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.RecentlyViewedOpp);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.allOpportunity);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User navigates to all opportunities");
	}

	@Given("^Access to SF-SVO Portal with username \"([^\"]*)\" and password \"([^\"]*)\"$")
	public void Access_SF_SVO_Portal(String userName, String password) throws Exception {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");
		onStart();
		driver = getDriver();
		ActionHandler.wait(13);
		driver.get(Constants.SVOURL);
		ActionHandler.wait(15);
		Reporter.addStepLog("User Logins to SVO Portal");

		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(120);

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User requires to enter Verification code received in an email");
		ActionHandler.wait(5);

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");
			ActionHandler.wait(10);
			vCode = SVOenquiry.checkEmail();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			ActionHandler.wait(15);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(120);

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			ActionHandler.wait(7);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(3);
			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(2);
			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {
			ActionHandler.wait(5);
			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {
				ActionHandler.wait(3);
				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {
				ActionHandler.wait(3);
				ActionHandler.click(SVOEnquiryContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();
				ActionHandler.wait(2);
			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	// Access SVO portal
	@Given("^Access SF_SVO Portal$")
	public void access_SF_SVO_Portal() throws Throwable {
		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");
		onStart();
		driver = getDriver();
		ActionHandler.wait(6);
		driver.get(Constants.SVOURL);
		ActionHandler.wait(15);
		Reporter.addStepLog("User Logins to SVO Portal");

		String UserName = Config.getPropertyValue("Admin_UserName");
		String Password = Config.getPropertyValue("Admin_Password");

		ActionHandler.setText(SVO_OpportunityContainer.userNameTextBox, UserName);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVO_OpportunityContainer.passwordTextBox, Password);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVO_OpportunityContainer.loginBtn);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User requires to enter Verification code received in an email");
		ActionHandler.wait(5);

		if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.verificationCode)) {
			Reporter.addStepLog("Please Open recently received email from Salesforce");
			ActionHandler.wait(10);
			vCode = SVOenquiry.checkEmail();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			ActionHandler.wait(15);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, UserName);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, Password);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			ActionHandler.wait(7);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			ActionHandler.wait(7);
		} else if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.mobileNumText)) {
			ActionHandler.wait(5);
			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.remindMeLater)) {
				ActionHandler.wait(3);
				ActionHandler.click(SVO_OpportunityContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.notRegister)) {
				ActionHandler.wait(3);
				ActionHandler.click(SVO_OpportunityContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			ActionHandler.wait(120);
			CommonFunctions.attachScreenshot();
			VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	@Given("^User access SF-SVO Portal$")
	public void User_Access_SFSVO_Portal() throws Exception {
		driver = getDriver();
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);

		if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.userNameTextBox)) {
			ActionHandler.wait(3);
			ActionHandler.click(SVOEnquiryContainer.icon_image);
			ActionHandler.wait(10);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Click on Logout");
			ActionHandler.click(SVOEnquiryContainer.Logout);
			ActionHandler.wait(10);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logout from SVO Portal");
		}

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		Reporter.addStepLog("User Logins to SVO Portal");

		String UserName = Config.getPropertyValue("Admin_UserName");
		String Password = Config.getPropertyValue("Admin_Password");

		ActionHandler.wait(2);
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, UserName);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, Password);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();

		ActionHandler.waitForElement(SVOEnquiryContainer.SVOText, 30);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
		Reporter.addStepLog("User navigate to SVO Home Page successfully");

	}

	// Select SVO from Menu option
	@Then("^User Navigate to SVO$")
	public void user_Navigate_to_SVO() throws Throwable {
		// User selects SVO from Menu//
		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.Menu);
		ActionHandler.wait(5);
		Reporter.addStepLog("User click on Menu");
		CommonFunctions.attachScreenshot();

		ActionHandler.clearAndSetText(SVO_OpportunityContainer.SearchBar, "Special Vehicle Operations");
		ActionHandler.wait(5);
		Reporter.addStepLog("User searched for Special Vehicle Operations");
		javaScriptUtil.clickElementByJS(SVO_OpportunityContainer.SVO);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

	}

	// to navigate to opportunity tab
	@Then("^User Navigate to Opportunity tab$")
	public void user_Navigate_to_Opportunity_tab() throws Throwable {
		ActionHandler.wait(5);
		Reporter.addStepLog("User Navigate to Opportunity");
		javaScriptUtil.clickElementByJS(SVO_OpportunityContainer.OpportTab);
		ActionHandler.wait(15);
		CommonFunctions.attachScreenshot();
	}

	@And("^User Creates New Opportunity with \"([^\"]*)\" Record Type$")
	public void User_creates_New_Opportunity_Record_Type(String recordType) throws Exception {
		String rType[] = recordType.split(",");
		recordType = CommonFunctions.readExcelMasterData(rType[0], rType[1], rType[2]);

		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.newOpportunityBtn);
		ActionHandler.wait(3);
		Reporter.addStepLog("User clicks on new Opportunity tab");
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(driver.findElement(By.xpath("//span[text()='Classic Service']")));
		//ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.RTSelection(recordType))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVO_OpportunityContainer.Next);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User selects record type to create new Opportunity");

	}

	@Then("^User enters mandatory fields for an Opportunity like Stage \"([^\"]*)\" Product Offering \"([^\"]*)\" Region \"([^\"]*)\" Sales Area \"([^\"]*)\" Market \"([^\"]*)\" Preferred Contact \"([^\"]*)\" Account Name \"([^\"]*)\" Source \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\"$")
	public void User_enters_Mandatory_Fields_Opportunity(String stage, String proOffering, String region,
			String salesArea, String market, String pContact, String aName, String source, String brand, String model)
			throws Exception {
		String s[] = stage.split(",");
		stage = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String po[] = proOffering.split(",");
		proOffering = CommonFunctions.readExcelMasterData(po[0], po[1], po[2]);

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String sa[] = salesArea.split(",");
		salesArea = CommonFunctions.readExcelMasterData(sa[0], sa[1], sa[2]);

		String m[] = market.split(",");
		market = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		String pc[] = pContact.split(",");
		pContact = CommonFunctions.readExcelMasterData(pc[0], pc[1], pc[2]);

		String an[] = aName.split(",");
		aName = CommonFunctions.readExcelMasterData(an[0], an[1], an[2]);

		String so[] = source.split(",");
		source = CommonFunctions.readExcelMasterData(so[0], so[1], so[2]);

		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String mo[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(mo[0], mo[1], mo[2]);

		ActionHandler.wait(2);
		String opp = SVO_OpportunityContainer.oppHeading.getText();
		// String opp = opp.substring(17,26);
		Reporter.addStepLog("Selected Opportunity Record Type is = " + opp);

		String oppName = "Test_Opportunity_";
		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		String closedDate = CommonFunctions.selectFutureDate();
		String programme = "Test_Programme";

		if (opp.equals("SVO Bespoke")) {
			ActionHandler.wait(2);

			Reporter.addStepLog("User start filling mandatory fields");
			ActionHandler.wait(5);
			ActionHandler.setText(SVO_OpportunityContainer.OppName, oppName + randomNumber);
			Reporter.addStepLog("User enters an Opportunity name");
			CommonFunctions.attachScreenshot();

			ActionHandler.wait(5);
			ActionHandler.setText(SVO_OpportunityContainer.CloseDate, closedDate);
			Reporter.addStepLog("User select the close date");
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();

			// ActionHandler.pageDown();
			ActionHandler.wait(4);
			ActionHandler.click(SVO_OpportunityContainer.stageViewAllDependancies);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(7);
			ActionHandler.click(SVO_OpportunityContainer.StageDW);
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(2);
			ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.SourceSelection(stage))));
			ActionHandler.wait(3);
			ActionHandler.click(SVO_OpportunityContainer.stageApplyBtn);
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User select the stage");

			ActionHandler.wait(3);
			ActionHandler.click(SVO_OpportunityContainer.prodOffering);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.StageSelection(proOffering))));
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects Product Offering");

			ActionHandler.wait(3);
			ActionHandler.click(SVO_OpportunityContainer.RegionDW);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.RegionSelection(region))));
			ActionHandler.wait(3);
			Reporter.addStepLog("User select the Region,area and market dropdown");
			CommonFunctions.attachScreenshot();

			ActionHandler.scrollToView(SVO_OpportunityContainer.Client);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			ActionHandler.setText(SVO_OpportunityContainer.PrefContact, pContact);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(SVO_OpportunityContainer.showAllResult);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			ActionHandler
					.click(driver.findElement(By.xpath(SVO_OpportunityContainer.OppAccountSearchselect(pContact))));
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User select preferred contact");

			ActionHandler.wait(5);
			ActionHandler.setText(SVO_OpportunityContainer.AccountName, aName);
			ActionHandler.wait(5);
			ActionHandler.clearAndSetText(SVO_OpportunityContainer.AccountName, aName);
			ActionHandler.wait(5);
			Actions act = new Actions(driver);
			act.sendKeys(Keys.ARROW_DOWN).build().perform();
			act.sendKeys(Keys.ENTER).build().perform();
			ActionHandler.wait(4);
			Reporter.addStepLog("User enters Account Name");
			CommonFunctions.attachScreenshot();

			ActionHandler.wait(5);
			ActionHandler.setText(SVO_OpportunityContainer.AccountName, aName);
			ActionHandler.wait(5);
			ActionHandler.clearAndSetText(SVO_OpportunityContainer.AccountName, aName);
			ActionHandler.wait(5);
			Actions act1 = new Actions(driver);
			act1.sendKeys(Keys.ARROW_DOWN).build().perform();
			act1.sendKeys(Keys.ENTER).build().perform();
			ActionHandler.wait(4);
			Reporter.addStepLog("User enters Account Name");
			CommonFunctions.attachScreenshot();

			// javaScriptUtil.scrollIntoView(SVO_OpportunityContainer.souInfo);
			ActionHandler.wait(3);
			ActionHandler.click(SVO_OpportunityContainer.souceDD);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.StageSelection(source))));
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects Source");

			ActionHandler.wait(3);
			javaScriptUtil.scrollIntoView(SVO_OpportunityContainer.requestDetails);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			ActionHandler.setText(SVO_OpportunityContainer.brand, brand);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Actions act2 = new Actions(driver);
			act2.sendKeys(Keys.ARROW_DOWN).build().perform();
			act2.sendKeys(Keys.ENTER).build().perform();
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();
			/*
			 * ActionHandler.click(SVO_OpportunityContainer.SBbrandSearch);
			 * ActionHandler.wait(2); CommonFunctions.attachScreenshot();
			 * ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.
			 * brandSearch(brand)))); ActionHandler.wait(3);
			 * CommonFunctions.attachScreenshot();
			 */
			Reporter.addStepLog("User selects Brand");

			ActionHandler.wait(2);
			ActionHandler.setText(SVO_OpportunityContainer.model, model);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Actions act3 = new Actions(driver);
			act3.sendKeys(Keys.ARROW_DOWN).build().perform();
			act3.sendKeys(Keys.ENTER).build().perform();
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();
			/*
			 * ActionHandler.click(SVO_OpportunityContainer.SBModelSearch);
			 * ActionHandler.wait(2); CommonFunctions.attachScreenshot();
			 * ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.
			 * brandSearch(model)))); ActionHandler.wait(3);
			 * CommonFunctions.attachScreenshot();
			 */
			Reporter.addStepLog("User selects Model");

			javaScriptUtil.scrollIntoView(SVO_OpportunityContainer.pricingDetailsSB);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVO_OpportunityContainer.costPriceSB, "123");
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVO_OpportunityContainer.contributionSB, "342");
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVO_OpportunityContainer.revenueSB, "542");
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVO_OpportunityContainer.listPriceSB, "322");
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVO_OpportunityContainer.retailPriceSB, "321");
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();

			ActionHandler.wait(1);
			ActionHandler.click(SVO_OpportunityContainer.SaveButton);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User saves an Opportunity with SVO Bespoke Record Type");
		}

		else if (opp.equals("Classic Bespoke")) {
			ActionHandler.wait(2);
			Reporter.addStepLog("User start filling mandatory fields");
			ActionHandler.wait(5);
			ActionHandler.setText(SVO_OpportunityContainer.OppName, oppName + randomNumber);
			Reporter.addStepLog("User enters an Opportunity name");
			CommonFunctions.attachScreenshot();

			ActionHandler.wait(5);
			ActionHandler.setText(SVO_OpportunityContainer.CloseDate, closedDate);
			Reporter.addStepLog("User select the close date");
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVO_OpportunityContainer.StageDD);
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(2);
			ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.SourceSelection(stage))));
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User select the stage");

			/*
			 * ActionHandler.wait(3);
			 * ActionHandler.click(SVO_OpportunityContainer.prodOffering);
			 * ActionHandler.wait(3); CommonFunctions.attachScreenshot();
			 * ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.
			 * StageSelection(proOffering)))); ActionHandler.wait(2);
			 * CommonFunctions.attachScreenshot();
			 * Reporter.addStepLog("User selects Product Offering");
			 */

			ActionHandler.wait(3);
			ActionHandler.click(SVO_OpportunityContainer.RegionDW);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.RegionSelection(region))));
			ActionHandler.wait(3);
			Reporter.addStepLog("User select the Region,area and market dropdown");
			CommonFunctions.attachScreenshot();

			ActionHandler.scrollToView(SVO_OpportunityContainer.Client);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			ActionHandler.setText(SVO_OpportunityContainer.PrefContact, pContact);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Actions act1 = new Actions(driver);
			act1.sendKeys(Keys.ARROW_DOWN).build().perform();
			act1.sendKeys(Keys.ENTER).build().perform();
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User select preferred contact");

			ActionHandler.wait(5);
			ActionHandler.setText(SVO_OpportunityContainer.AccountName, aName);
			ActionHandler.wait(5);
			Actions act = new Actions(driver);
			act.sendKeys(Keys.ARROW_DOWN).build().perform();
			act.sendKeys(Keys.ENTER).build().perform();
			ActionHandler.wait(4);
			Reporter.addStepLog("User enters Account Name");
			CommonFunctions.attachScreenshot();

			ActionHandler.wait(3);
			ActionHandler.click(SVO_OpportunityContainer.CRRestrictedParty);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(2);
			ActionHandler.click(SVO_OpportunityContainer.checkCleared);
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User updates Restricted Party Screening");

			javaScriptUtil.scrollIntoView(SVO_OpportunityContainer.brand);
			ActionHandler.wait(3);
			ActionHandler.setText(SVO_OpportunityContainer.brand, brand);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Actions act2 = new Actions(driver);
			act2.sendKeys(Keys.ARROW_DOWN).build().perform();
			act2.sendKeys(Keys.ENTER).build().perform();
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects Brand");

			ActionHandler.wait(2);
			ActionHandler.setText(SVO_OpportunityContainer.model, model);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Actions act3 = new Actions(driver);
			act3.sendKeys(Keys.ARROW_DOWN).build().perform();
			act3.sendKeys(Keys.ENTER).build().perform();
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects Model");

			javaScriptUtil.scrollIntoView(SVO_OpportunityContainer.RetailPriceTextBox);
			ActionHandler.wait(3);
			ActionHandler.setText(SVO_OpportunityContainer.RetailPriceTextBox, "100");
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.wait(2);
			ActionHandler.click(SVO_OpportunityContainer.VATdropdown);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Actions act4 = new Actions(driver);
			act4.sendKeys(Keys.ARROW_DOWN).build().perform();
			act4.sendKeys(Keys.ENTER).build().perform();
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects VAT Qualifying");

			javaScriptUtil.scrollIntoView(SVO_OpportunityContainer.CBsouceDD);
			ActionHandler.wait(3);
			ActionHandler.click(SVO_OpportunityContainer.CBsouceDD);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.SourceSelection(source))));
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects Source");

			ActionHandler.wait(3);
			ActionHandler.click(SVO_OpportunityContainer.SaveButton);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User saves an Opportunity with Classic Bespoke Record Type");
		}

		else if (opp.equals("Classic Continuation")) {
			ActionHandler.wait(2);

			Reporter.addStepLog("User start filling mandatory fields");
			ActionHandler.wait(5);
			ActionHandler.setText(SVO_OpportunityContainer.OppName, oppName);
			Reporter.addStepLog("User enters an Opportunity name");
			CommonFunctions.attachScreenshot();

			ActionHandler.wait(5);
			ActionHandler.setText(SVO_OpportunityContainer.CloseDate, closedDate);
			Reporter.addStepLog("User select the close date");
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();

			ActionHandler.wait(5);
			ActionHandler.click(SVO_OpportunityContainer.StageDW);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(3);
			ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.StageSelection(stage))));
			Reporter.addStepLog("User select the stage");
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVO_OpportunityContainer.RegionDW);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.RegionSelection(region))));
			ActionHandler.wait(3);
			Reporter.addStepLog("User select the Region,area and market dropdown");
			CommonFunctions.attachScreenshot();

			ActionHandler.scrollToView(SVO_OpportunityContainer.Client);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			ActionHandler.setText(SVO_OpportunityContainer.PrefContact, pContact);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(SVO_OpportunityContainer.showAllResult);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			ActionHandler
					.click(driver.findElement(By.xpath(SVO_OpportunityContainer.OppAccountSearchselect(pContact))));
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User select preferred contact");

			ActionHandler.wait(5);
			ActionHandler.setText(SVO_OpportunityContainer.AccountName, aName);
			ActionHandler.wait(5);
			ActionHandler.clearAndSetText(SVO_OpportunityContainer.AccountName, aName);
			ActionHandler.wait(5);
			Actions act = new Actions(driver);
			act.sendKeys(Keys.ARROW_DOWN).build().perform();
			act.sendKeys(Keys.ENTER).build().perform();
			ActionHandler.wait(4);
			Reporter.addStepLog("User enters Account Name");
			CommonFunctions.attachScreenshot();

			javaScriptUtil.scrollIntoView(SVO_OpportunityContainer.brand);
			ActionHandler.wait(3);
			ActionHandler.setText(SVO_OpportunityContainer.ContinuationPrgm, programme);
			ActionHandler.wait(3);
			Actions act1 = new Actions(driver);
			act1.sendKeys(Keys.ARROW_DOWN).build().perform();
			act1.sendKeys(Keys.ENTER).build().perform();
			ActionHandler.wait(4);
			Reporter.addStepLog("User selects Programme");

			ActionHandler.wait(2);
			ActionHandler.setText(SVO_OpportunityContainer.brand, brand);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(SVO_OpportunityContainer.brandSearch);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.brandSearch(brand))));
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects Brand");

			ActionHandler.wait(2);
			ActionHandler.setText(SVO_OpportunityContainer.model, model);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(SVO_OpportunityContainer.modelSearch);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.brandSearch(model))));
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects Model");

			javaScriptUtil.scrollIntoView(SVO_OpportunityContainer.souceDD);
			ActionHandler.wait(3);
			ActionHandler.click(SVO_OpportunityContainer.souceDD);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.StageSelection(source))));
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects Source");

			ActionHandler.click(SVO_OpportunityContainer.SaveButton);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User saves an Opportunity with Classic Continuation Record Type");
		}

		else if (opp.equals("Classic Limited Edition")) {
			ActionHandler.wait(2);
			Reporter.addStepLog("User start filling mandatory fields");
			ActionHandler.wait(5);
			ActionHandler.setText(SVO_OpportunityContainer.OppName, oppName + randomNumber);
			Reporter.addStepLog("User enters an Opportunity name");
			CommonFunctions.attachScreenshot();

			ActionHandler.wait(5);
			ActionHandler.setText(SVO_OpportunityContainer.CloseDate, closedDate);
			Reporter.addStepLog("User select the close date");
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();

			ActionHandler.wait(4);
			ActionHandler.click(SVO_OpportunityContainer.stageViewAllDependancies);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(3);
			ActionHandler.click(SVO_OpportunityContainer.StageDW);
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(2);
			ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.SourceSelection(stage))));
			ActionHandler.wait(3);
			ActionHandler.click(SVO_OpportunityContainer.stageApplyBtn);
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User select the stage");

			ActionHandler.wait(2);
			ActionHandler.click(SVO_OpportunityContainer.RegionDW);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.RegionSelection(region))));
			ActionHandler.wait(3);
			Reporter.addStepLog("User select the Region,area and market dropdown");
			CommonFunctions.attachScreenshot();

			ActionHandler.scrollToView(SVO_OpportunityContainer.Client);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			ActionHandler.setText(SVO_OpportunityContainer.PrefContact, pContact);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Actions act = new Actions(driver);
			act.sendKeys(Keys.ARROW_DOWN).build().perform();
			act.sendKeys(Keys.ENTER).build().perform();
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User select preferred contact");

			ActionHandler.wait(5);
			ActionHandler.setText(SVO_OpportunityContainer.AccountName, aName);
			ActionHandler.wait(5);
			ActionHandler.clearAndSetText(SVO_OpportunityContainer.AccountName, aName);
			ActionHandler.wait(5);
			Actions act2 = new Actions(driver);
			act2.sendKeys(Keys.ARROW_DOWN).build().perform();
			act2.sendKeys(Keys.ENTER).build().perform();
			ActionHandler.wait(4);
			Reporter.addStepLog("User enters Account Name");
			CommonFunctions.attachScreenshot();

			ActionHandler.wait(4);
			ActionHandler.pageScrollDown();
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(SVO_OpportunityContainer.ContinuationPrgm);
			ActionHandler.wait(3);
			Actions act1 = new Actions(driver);
			act1.sendKeys(Keys.ARROW_DOWN).build().perform();
			act1.sendKeys(Keys.ENTER).build().perform();
			ActionHandler.wait(4);
			Reporter.addStepLog("User click on New Programme");
			ActionHandler.wait(3);
			/*
			 * ActionHandler.setText(SVO_OpportunityContainer.ClaLimEdiProgramme,
			 * programme); ActionHandler.wait(3); CommonFunctions.attachScreenshot();
			 * 
			 * ActionHandler.wait(3);
			 * ActionHandler.click(SVO_OpportunityContainer.ClaLimEdiProStatua);
			 * ActionHandler.wait(4); CommonFunctions.attachScreenshot();
			 * ActionHandler.click(SVO_OpportunityContainer.ClaLimEdiProSelectStatus);
			 * ActionHandler.wait(4); CommonFunctions.attachScreenshot();
			 * 
			 * ActionHandler.wait(3);
			 * ActionHandler.click(SVO_OpportunityContainer.ClaLimEdiProOffering);
			 * ActionHandler.wait(4); CommonFunctions.attachScreenshot();
			 * ActionHandler.click(SVO_OpportunityContainer.ClaLimEdiProOfferingValue);
			 * ActionHandler.wait(4); CommonFunctions.attachScreenshot();
			 * 
			 * ActionHandler.wait(2);
			 * ActionHandler.setText(SVO_OpportunityContainer.probrand, brand);
			 * ActionHandler.wait(3); CommonFunctions.attachScreenshot(); Actions acti = new
			 * Actions(driver); acti.sendKeys(Keys.ARROW_DOWN).build().perform();
			 * acti.sendKeys(Keys.ENTER).build().perform(); ActionHandler.wait(4);
			 * CommonFunctions.attachScreenshot();
			 * Reporter.addStepLog("User selects Brand");
			 * 
			 * ActionHandler.wait(2);
			 * ActionHandler.setText(SVO_OpportunityContainer.promodel, model);
			 * ActionHandler.wait(3); CommonFunctions.attachScreenshot(); Actions actio =
			 * new Actions(driver); actio.sendKeys(Keys.ARROW_DOWN).build().perform();
			 * actio.sendKeys(Keys.ENTER).build().perform(); ActionHandler.wait(4);
			 * CommonFunctions.attachScreenshot();
			 * Reporter.addStepLog("User selects Model");
			 * 
			 * ActionHandler.wait(2);
			 * ActionHandler.click(SVO_OpportunityContainer.SaveQuoteMike);
			 * ActionHandler.wait(4); CommonFunctions.attachScreenshot();
			 * Reporter.addStepLog("User creats new programme");
			 */

			ActionHandler.wait(2);
			ActionHandler.setText(SVO_OpportunityContainer.brand, brand);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Actions act3 = new Actions(driver);
			act3.sendKeys(Keys.ARROW_DOWN).build().perform();
			act3.sendKeys(Keys.ENTER).build().perform();
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects Brand");

			ActionHandler.wait(2);
			ActionHandler.setText(SVO_OpportunityContainer.model, model);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Actions act4 = new Actions(driver);
			act4.sendKeys(Keys.ARROW_DOWN).build().perform();
			act4.sendKeys(Keys.ENTER).build().perform();
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects Model");

			javaScriptUtil.scrollIntoView(SVO_OpportunityContainer.ClsLimiEdisouceDD);
			ActionHandler.wait(3);
			ActionHandler.click(SVO_OpportunityContainer.ClsLimiEdisouceDD);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.StageSelection(source))));
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects Source");

			ActionHandler.wait(3);
			ActionHandler.click(SVO_OpportunityContainer.SaveButton);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User saves an Opportunity with Classic Limited Edition Record Type");
		}

		else if (opp.equals("Classic Reborn")) {
			ActionHandler.wait(2);

			Reporter.addStepLog("User start filling mandatory fields");
			ActionHandler.wait(5);
			ActionHandler.setText(SVO_OpportunityContainer.OppName, oppName + randomNumber);
			Reporter.addStepLog("User enters an Opportunity name");
			CommonFunctions.attachScreenshot();

			ActionHandler.wait(5);
			ActionHandler.setText(SVO_OpportunityContainer.CloseDate, closedDate);
			Reporter.addStepLog("User select the close date");
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();

			ActionHandler.wait(4);
			ActionHandler.click(SVO_OpportunityContainer.stageViewAllDependancies);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(3);
			ActionHandler.click(SVO_OpportunityContainer.StageDW);
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(2);
			ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.SourceSelection(stage))));
			ActionHandler.wait(3);
			ActionHandler.click(SVO_OpportunityContainer.stageApplyBtn);
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User select the stage");

			ActionHandler.click(SVO_OpportunityContainer.RegionDW);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.RegionSelection(region))));
			ActionHandler.wait(3);
			Reporter.addStepLog("User select the Region,area and market dropdown");
			CommonFunctions.attachScreenshot();

			ActionHandler.scrollToView(SVO_OpportunityContainer.Client);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();

			ActionHandler.wait(3);
			ActionHandler.click(SVO_OpportunityContainer.CRRestrictedParty);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(2);
			ActionHandler.click(SVO_OpportunityContainer.checkCleared);
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User updates Restricted Party Screening");

			ActionHandler.setText(SVO_OpportunityContainer.PrefContact, pContact);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(SVO_OpportunityContainer.showAllResult);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			ActionHandler
					.click(driver.findElement(By.xpath(SVO_OpportunityContainer.OppAccountSearchselect(pContact))));
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User select preferred contact");

			ActionHandler.wait(5);
			ActionHandler.setText(SVO_OpportunityContainer.AccountName, aName);
			ActionHandler.wait(5);
			ActionHandler.clearAndSetText(SVO_OpportunityContainer.AccountName, aName);
			ActionHandler.wait(5);
			Actions act = new Actions(driver);
			act.sendKeys(Keys.ARROW_DOWN).build().perform();
			act.sendKeys(Keys.ENTER).build().perform();
			ActionHandler.wait(4);
			Reporter.addStepLog("User enters Account Name");
			CommonFunctions.attachScreenshot();

			ActionHandler.wait(3);
			javaScriptUtil.scrollIntoView(SVO_OpportunityContainer.requestDetails);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(2);
			ActionHandler.setText(SVO_OpportunityContainer.brand, brand);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Actions act2 = new Actions(driver);
			act2.sendKeys(Keys.ARROW_DOWN).build().perform();
			act2.sendKeys(Keys.ENTER).build().perform();
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects Brand");

			ActionHandler.wait(2);
			ActionHandler.setText(SVO_OpportunityContainer.model, model);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Actions act3 = new Actions(driver);
			act3.sendKeys(Keys.ARROW_DOWN).build().perform();
			act3.sendKeys(Keys.ENTER).build().perform();
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects Model");

			ActionHandler.wait(2);
			ActionHandler.click(SVO_OpportunityContainer.bodyStyle);
			ActionHandler.wait(2);
			Actions act4 = new Actions(driver);
			act4.sendKeys(Keys.ENTER).build().perform();
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(1);
			ActionHandler.setText(SVO_OpportunityContainer.name, "Test Vehicle Body Style");
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.wait(1);
			ActionHandler.click(SVO_OpportunityContainer.model);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Actions act5 = new Actions(driver);
			act5.sendKeys(Keys.ARROW_DOWN).build().perform();
			act5.sendKeys(Keys.ENTER).build().perform();
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(2);
			ActionHandler.click(SVO_OpportunityContainer.SaveOrder);
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User creates new Vehicle Body Style");

			ActionHandler.wait(2);
			ActionHandler.pageDown();
			ActionHandler.wait(2);
			ActionHandler.click(SVO_OpportunityContainer.vehicle);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			Actions act6 = new Actions(driver);
			act6.sendKeys(Keys.ENTER).build().perform();
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();

			ActionHandler.wait(2);
			ActionHandler.click(SVO_OpportunityContainer.handDrive);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Actions act7 = new Actions(driver);
			act7.sendKeys(Keys.ARROW_DOWN).build().perform();
			act7.sendKeys(Keys.ENTER).build().perform();
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();

			ActionHandler.wait(2);
			ActionHandler.click(SVO_OpportunityContainer.ExtColour);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Actions act8 = new Actions(driver);
			act8.sendKeys(Keys.ARROW_DOWN).build().perform();
			act8.sendKeys(Keys.ENTER).build().perform();
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();

			ActionHandler.wait(2);
			ActionHandler.setText(SVO_OpportunityContainer.buildColour, "Black");
			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();

			ActionHandler.wait(2);
			ActionHandler.setText(SVO_OpportunityContainer.ColourShade, "Black");
			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();

			ActionHandler.wait(2);
			ActionHandler.setText(SVO_OpportunityContainer.interiorColour, "Black");
			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();

			ActionHandler.wait(2);
			ActionHandler.setText(SVO_OpportunityContainer.specDetails, "Test Specs");
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User enters all Specifications and Options details");

			javaScriptUtil.scrollIntoView(SVO_OpportunityContainer.CRsouceDD);
			ActionHandler.wait(3);
			ActionHandler.click(SVO_OpportunityContainer.CRsouceDD);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.StageSelection(source))));
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects Source");

			ActionHandler.wait(2);
			ActionHandler.click(SVO_OpportunityContainer.SaveButton);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User saves an Opportunity with Classic Reborn Record Type");

			ActionHandler.wait(4);
			oppoName = SVO_OpportunityContainer.opporName.getText();
			System.out.println("Created Opportunity Name is = " + oppoName);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(4);

		}

		else if (opp.equals("Classic Service")) {
			ActionHandler.wait(2);

			Reporter.addStepLog("User start filling mandatory fields");
			ActionHandler.wait(5);
			ActionHandler.setText(SVO_OpportunityContainer.OppName, oppName);
			Reporter.addStepLog("User enters an Opportunity name");
			CommonFunctions.attachScreenshot();

			ActionHandler.wait(5);
			ActionHandler.setText(SVO_OpportunityContainer.CloseDate, closedDate);
			Reporter.addStepLog("User select the close date");
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();

			ActionHandler.wait(5);
			ActionHandler.click(SVO_OpportunityContainer.StageDW);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(3);
			ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.StageSelection(stage))));
			Reporter.addStepLog("User select the stage");
			CommonFunctions.attachScreenshot();

			ActionHandler.wait(3);
			ActionHandler.click(SVO_OpportunityContainer.prodOffering);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.StageSelection(proOffering))));
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects Product Offering");

			ActionHandler.click(SVO_OpportunityContainer.RegionDW);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.RegionSelection(region))));
			ActionHandler.wait(3);
			Reporter.addStepLog("User select the Region,area and market dropdown");
			CommonFunctions.attachScreenshot();

			ActionHandler.scrollToView(SVO_OpportunityContainer.Client);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			ActionHandler.setText(SVO_OpportunityContainer.PrefContact, pContact);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(SVO_OpportunityContainer.showAllResult);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			ActionHandler
					.click(driver.findElement(By.xpath(SVO_OpportunityContainer.OppAccountSearchselect(pContact))));
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User select preferred contact");

			ActionHandler.wait(5);
			ActionHandler.setText(SVO_OpportunityContainer.AccountName, aName);
			ActionHandler.wait(5);
			ActionHandler.clearAndSetText(SVO_OpportunityContainer.AccountName, aName);
			ActionHandler.wait(5);
			Actions act = new Actions(driver);
			act.sendKeys(Keys.ARROW_DOWN).build().perform();
			act.sendKeys(Keys.ENTER).build().perform();
			ActionHandler.wait(4);
			Reporter.addStepLog("User enters Account Name");
			CommonFunctions.attachScreenshot();

			javaScriptUtil.scrollIntoView(SVO_OpportunityContainer.brand);
			ActionHandler.wait(3);
			ActionHandler.setText(SVO_OpportunityContainer.brand, brand);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(SVO_OpportunityContainer.brandSearch);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.brandSearch(brand))));
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects Brand");

			ActionHandler.wait(2);
			ActionHandler.setText(SVO_OpportunityContainer.model, model);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(SVO_OpportunityContainer.modelSearch);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.brandSearch(model))));
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects Model");

			javaScriptUtil.scrollIntoView(SVO_OpportunityContainer.souceDD);
			ActionHandler.wait(3);
			ActionHandler.click(SVO_OpportunityContainer.souceDD);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.StageSelection(source))));
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects Source");

			ActionHandler.click(SVO_OpportunityContainer.SaveButton);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User saves an Opportunity with Classic Service Record Type");
		}

		else if (opp.equals("Classic Works Legend")) {
			ActionHandler.wait(2);

			Reporter.addStepLog("User start filling mandatory fields");
			ActionHandler.wait(5);
			ActionHandler.setText(SVO_OpportunityContainer.OppName, oppName);
			Reporter.addStepLog("User enters an Opportunity name");
			CommonFunctions.attachScreenshot();

			ActionHandler.wait(5);
			ActionHandler.setText(SVO_OpportunityContainer.CloseDate, closedDate);
			Reporter.addStepLog("User select the close date");
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();

			ActionHandler.wait(5);
			ActionHandler.click(SVO_OpportunityContainer.StageDW);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(3);
			ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.StageSelection(stage))));
			Reporter.addStepLog("User select the stage");
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVO_OpportunityContainer.RegionDW);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.RegionSelection(region))));
			ActionHandler.wait(3);
			Reporter.addStepLog("User select the Region,area and market dropdown");
			CommonFunctions.attachScreenshot();

			ActionHandler.scrollToView(SVO_OpportunityContainer.Client);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			ActionHandler.setText(SVO_OpportunityContainer.PrefContact, pContact);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(SVO_OpportunityContainer.showAllResult);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			ActionHandler
					.click(driver.findElement(By.xpath(SVO_OpportunityContainer.OppAccountSearchselect(pContact))));
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User select preferred contact");

			ActionHandler.wait(5);
			ActionHandler.setText(SVO_OpportunityContainer.AccountName, aName);
			ActionHandler.wait(5);
			ActionHandler.clearAndSetText(SVO_OpportunityContainer.AccountName, aName);
			ActionHandler.wait(5);
			Actions act = new Actions(driver);
			act.sendKeys(Keys.ARROW_DOWN).build().perform();
			act.sendKeys(Keys.ENTER).build().perform();
			ActionHandler.wait(4);
			Reporter.addStepLog("User enters Account Name");
			CommonFunctions.attachScreenshot();

			javaScriptUtil.scrollIntoView(SVO_OpportunityContainer.brand);
			ActionHandler.wait(3);
			ActionHandler.setText(SVO_OpportunityContainer.brand, brand);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(SVO_OpportunityContainer.brandSearch);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.brandSearch(brand))));
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects Brand");

			ActionHandler.wait(2);
			ActionHandler.setText(SVO_OpportunityContainer.model, model);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(SVO_OpportunityContainer.modelSearch);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.brandSearch(model))));
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects Model");

			javaScriptUtil.scrollIntoView(SVO_OpportunityContainer.souceDD);
			ActionHandler.wait(3);
			ActionHandler.click(SVO_OpportunityContainer.souceDD);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.StageSelection(source))));
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects Source");

			ActionHandler.click(SVO_OpportunityContainer.SaveButton);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User saves an Opportunity with Classic Works Legend Record Type");
		}

		else if (opp.equals("SVO Private Office")) {
			ActionHandler.wait(2);

			Reporter.addStepLog("User start filling mandatory fields");
			ActionHandler.wait(5);
			ActionHandler.setText(SVO_OpportunityContainer.OppName, oppName);
			Reporter.addStepLog("User enters an Opportunity name");
			CommonFunctions.attachScreenshot();

			ActionHandler.wait(5);
			ActionHandler.setText(SVO_OpportunityContainer.CloseDate, closedDate);
			Reporter.addStepLog("User select the close date");
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();

			ActionHandler.wait(5);
			ActionHandler.click(SVO_OpportunityContainer.StageDW);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(3);
			ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.StageSelection(stage))));
			Reporter.addStepLog("User select the stage");
			CommonFunctions.attachScreenshot();

			ActionHandler.wait(3);
			ActionHandler.click(SVO_OpportunityContainer.prodOffering);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.StageSelection(proOffering))));
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects Product Offering");

			ActionHandler.click(SVO_OpportunityContainer.RegionDW);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.RegionSelection(region))));
			ActionHandler.wait(3);
			Reporter.addStepLog("User select the Region,area and market dropdown");
			CommonFunctions.attachScreenshot();

			ActionHandler.scrollToView(SVO_OpportunityContainer.Client);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			ActionHandler.setText(SVO_OpportunityContainer.PrefContact, pContact);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(SVO_OpportunityContainer.showAllResult);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			ActionHandler
					.click(driver.findElement(By.xpath(SVO_OpportunityContainer.OppAccountSearchselect(pContact))));
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User select preferred contact");

			ActionHandler.wait(5);
			ActionHandler.setText(SVO_OpportunityContainer.AccountName, aName);
			ActionHandler.wait(5);
			ActionHandler.clearAndSetText(SVO_OpportunityContainer.AccountName, aName);
			ActionHandler.wait(5);
			Actions act = new Actions(driver);
			act.sendKeys(Keys.ARROW_DOWN).build().perform();
			act.sendKeys(Keys.ENTER).build().perform();
			ActionHandler.wait(4);
			Reporter.addStepLog("User enters Account Name");
			CommonFunctions.attachScreenshot();

			javaScriptUtil.scrollIntoView(SVO_OpportunityContainer.souceDD);
			ActionHandler.wait(3);
			ActionHandler.click(SVO_OpportunityContainer.souceDD);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.StageSelection(source))));
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects Source");

			javaScriptUtil.scrollIntoView(SVO_OpportunityContainer.brand);
			ActionHandler.wait(3);
			ActionHandler.setText(SVO_OpportunityContainer.brand, brand);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(SVO_OpportunityContainer.brandSearch);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.brandSearch(brand))));
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects Brand");

			ActionHandler.wait(2);
			ActionHandler.setText(SVO_OpportunityContainer.model, model);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(SVO_OpportunityContainer.modelSearch);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.brandSearch(model))));
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects Model");

			ActionHandler.click(SVO_OpportunityContainer.SaveButton);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User saves an Opportunity with SVO Private Office Record Type");
		}
	}

	@Then("^Click on Send POF button$")
	public void Click_on_Send_POF_button() throws Throwable {
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.SendPOF);
		Reporter.addStepLog("User clicks on Send POF button");
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(30);
	}

	@Then("^Click on Merge & Adobe sign button$")
	public void Click_on_Merge_Adobe_button() throws Throwable {
		driver.switchTo().frame(0);
		ActionHandler.wait(5);
		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.MergeAdobe);
		Reporter.addStepLog("User clicks on Merge and Adobe sign button");
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(30);
	}

	@Then("^Click on Send button$")
	public void Click_on_Send_button() throws Throwable {
		// driver.switchTo().frame(0);
		ActionHandler.wait(5);
		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(5);
		String parentWindow = driver.getWindowHandle();
		ActionHandler.click(SVO_OpportunityContainer.Send);
		ActionHandler.wait(10);

		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!parentWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVO_OpportunityContainer.OKButton);
				ActionHandler.wait(12);
				CommonFunctions.attachScreenshot();

				driver.switchTo().window(parentWindow);
			}
		}
	}

	@And("^Verify that the opportunity stage moves to Design Brief & Quote$")
	public void Verify_Opportunity_Stage_moves_To_Design_brief_quote() throws Throwable {
		ActionHandler.wait(4);
		VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.DesignBrief);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Opportunity moves to Design Brief Stage");
	}

	@And("^User enters Pricing Details$")
	public void User_enters_pricing_details() throws Exception {
		ActionHandler.wait(3);
		ActionHandler.pageDown();
		ActionHandler.wait(3);
		ActionHandler.pageDown();
		ActionHandler.wait(3);
		ActionHandler.pageDown();
		// javaScriptUtil.scrollIntoView(SVO_OpportunityContainer.pricingDetails);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.editPricingDetails);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on edit pricing details");

		ActionHandler.wait(3);
		ActionHandler.setText(SVO_OpportunityContainer.RetailPriceTextBox, "100");
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.VATdropdown);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects VAT Qualifying");

		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.SaveButton);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
	}

	@Then("^User enters Vehicle details$")
	public void User_enters_vehicle_details() throws Exception {
		ActionHandler.wait(3);
		javaScriptUtil.scrollIntoView(SVO_OpportunityContainer.donorDetails);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.EditVehicle);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.vehicle);
		ActionHandler.wait(3);
		Actions act = new Actions(driver);
		// act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters vehicle information");

		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.ExtColour);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		act1.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.CRSpeedometer);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Actions act2 = new Actions(driver);
		act2.sendKeys(Keys.ARROW_DOWN).build().perform();
		act2.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.setText(SVO_OpportunityContainer.buildColour, "Black");
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		ActionHandler.setText(SVO_OpportunityContainer.interiorColour, "Black");
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		ActionHandler.setText(SVO_OpportunityContainer.ColourShade, "Red");
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		ActionHandler.setText(SVO_OpportunityContainer.specDetails, "Test Car");
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Specification details");

		ActionHandler.wait(2);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.bodyStyle);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.pressEnter();
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.setText(SVO_OpportunityContainer.RecordName, "Test Body Style");
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.model);
		ActionHandler.wait(3);
		Actions act4 = new Actions(driver);
		act4.sendKeys(Keys.ARROW_DOWN).build().perform();
		act4.sendKeys(Keys.ARROW_DOWN).build().perform();
		act4.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.SaveBtn);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User creates new Body Style");

		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.SaveButton);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
	}

	@And("^User verifies Reference Number is generated$")
	public void User_verifies_Reference_number() throws Exception {
		ActionHandler.wait(2);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Reference Number is unique");
	}

	@And("^User searched for Closed opportunity$")
	public void User_searched_for_closed_opportunity() throws Exception {
		String Seatext = "Closed";

		ActionHandler.wait(2);
		AllOpportunity();
		ActionHandler.wait(3);

		ActionHandler.wait(2);
		ActionHandler.setText(SVO_OpportunityContainer.SearchOpportunity, Seatext);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.pressEnter();
		ActionHandler.wait(3);
		Reporter.addStepLog("User searched for Closed Opportunity");
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.FirstOpportunity);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens first closed opportunity");
	}

	@Then("^User tries to reopen closed Opportunity to \"([^\"]*)\" Stage$")
	public void User_tries_reopen_closed_opportunity_qualified_stage(String stage) throws Exception {
		String s[] = stage.split(",");
		stage = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		ActionHandler.wait(2);
		ActionHandler.scrollToView(SVO_OpportunityContainer.editStage);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.editStage);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.SVOOpportunityeditregion);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.ValueSelection(stage))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User tries to reopen closed opportunity");

		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.SaveVehicle);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.saveError);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.closeSaveErrorBox);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.cancelVehicle);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
	}

	// To open opportunity with classic continuation record type
	@Then("^Open an opportunity with Classic Continuation Record Type$")
	public void open_an_opportunity_with_Classic_Continuation_Record_Type() throws Throwable {
		ActionHandler.wait(5);
		String Opportunity = "Classic Continuation";
		// Search for opportunity
		ActionHandler.clearAndSetText(SVO_OpportunityContainer.OpportSearchBox, Opportunity);
		ActionHandler.wait(3);
		ActionHandler.pressEnter();
		ActionHandler.wait(8);
		ActionHandler.click(SVO_OpportunityContainer.FirstOpportunity);
		ActionHandler.wait(5);
		ActionHandler.scrollToView(SVO_OpportunityContainer.VehicleRequestDetails);
		ActionHandler.wait(3);
		// Click on programme and verify build capacity
		ActionHandler.click(SVO_OpportunityContainer.Programme);
		ActionHandler.wait(10);

	}

	@And("^User navigates to Commissioning Request$")
	public void User_navugates_Commissioning_Request() throws Exception {
		ActionHandler.wait(3);
		ActionHandler.pageDown();
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.CommissioningRequestLink);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User nevigates to Commissioning Request");
	}

	@And("^User Creates a new Commissioning Request with \"([^\"]*)\" Record type$")
	public void User_Creates_New_Commissioning_Request_with_Record_Type(String recordType) throws Exception {
		String rType[] = recordType.split(",");
		recordType = CommonFunctions.readExcelMasterData(rType[0], rType[1], rType[2]);

		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.NewCommReq);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.RTypeSelection(recordType))));
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Commissioning Request Record Type");

		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.Next);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

	}

	@And("^User enters all mandatory fields and save commissioning Request$")
	public void User_enters_mandatory_Fields_save_Comm_Request() throws Exception {
		String startDate = CommonFunctions.selectFutureDate();
		String endDate = CommonFunctions.selectFutureDate1();

		ActionHandler.wait(2);
		ActionHandler.setText(SVO_OpportunityContainer.StartDate, startDate);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.setText(SVO_OpportunityContainer.EndDate, endDate);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User enters Start Date and End date for Commissioning Request");

		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.SaveButton);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves Commissioning Request");

	}

	@Then("^User verifies that linked opportunity is displayed under Bespoke Opportunity Details$")
	public void User_verifies_Linked_Opportunity_displayed_Bespoke_Opportunity_Details() throws Exception {
		ActionHandler.wait(3);
		Reporter.addStepLog("User Opens recently created Commissioning Request");
		ActionHandler.click(SVO_OpportunityContainer.firstCommRequest);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		// ActionHandler.scrollToView(SVO_OpportunityContainer.commReqOppList);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User can view linked Opportunity under bespoke opportunity details");
	}

	@Then("^Verify that User can create New Reservation from Calendar with Primary Contact \"([^\"]*)\"$")
	public void Verify_user_creates_New_Reservation(String primaryCnt) throws Exception {
		String pCnt[] = primaryCnt.split(",");
		primaryCnt = CommonFunctions.readExcelMasterData(pCnt[0], pCnt[1], pCnt[2]);

		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.viewCalendarBtn);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Calendar");

		ActionHandler.wait(3);
		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.CalendarCell);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User creates a new Reservation from Calendar");

		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.searchCommReq);
		ActionHandler.wait(6);
		Actions select = new Actions(driver);
		select.sendKeys(Keys.ARROW_DOWN).build().perform();
		select.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects available Commissioning Request");

		ActionHandler.wait(3);
		ActionHandler.setText(SVO_OpportunityContainer.commReqPrimaryCnt, primaryCnt);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Actions selectCnt = new Actions(driver);
		selectCnt.sendKeys(Keys.ARROW_DOWN).build().perform();
		selectCnt.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects available Primary Contact");

		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.saveReservation);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User creates reservation for Commissioning Request");
	}

	// select Classic Limited Edition Opportunity
	@Then("^User selects Classic Limited Edition Opportunity$")
	public void user_selects_Classic_Limited_Edition_Opportunity() throws Throwable {

		// User clicks on New button //
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.newOpportunityBtn);
		Reporter.addStepLog("User clicks on new Opportunity tab");
		CommonFunctions.attachScreenshot();

		// User selects record type and click on next//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.ClassicLimitedEdition);
		Reporter.addStepLog("User selected recorded type");
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.nextRType);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on next button");

	}

	// create opportunity by entering mandatory fields
	@Then("^User enters fields like OpportunityName \"([^\"]*)\" CloseDate \"([^\"]*)\" Stage \"([^\"]*)\" Region \"([^\"]*)\" AccountName \"([^\"]*)\" PreferredContact \"([^\"]*)\"$")
	public void user_enters_fields_like_OpportunityName_CloseDate_Stage_Region_AccountName_PreferredContact(
			String OpportunityName, String CloseDate, String Stage, String Region, String AccountName,
			String PreferredContact) throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		String OppName[] = OpportunityName.split(",");
		OpportunityName = CommonFunctions.readExcelMasterData(OppName[0], OppName[1], OppName[2]);

		String Stg[] = Stage.split(",");
		Stage = CommonFunctions.readExcelMasterData(Stg[0], Stg[1], Stg[2]);

		String Closedate[] = CloseDate.split(",");
		CloseDate = CommonFunctions.readExcelMasterData(Closedate[0], Closedate[1], Closedate[2]);

		String r[] = Region.split(",");
		Region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String AccName[] = AccountName.split(",");
		AccountName = CommonFunctions.readExcelMasterData(AccName[0], AccName[1], AccName[2]);

		String PrefCont[] = PreferredContact.split(",");
		PreferredContact = CommonFunctions.readExcelMasterData(PrefCont[0], PrefCont[1], PrefCont[2]);

		// user enters opportunity name //
		ActionHandler.wait(5);
		ActionHandler.setText(SVO_OpportunityContainer.OppName, OpportunityName);
		Reporter.addStepLog("User enters an Opportunity name");
		CommonFunctions.attachScreenshot();

		// User enters close date//
		ActionHandler.wait(5);
		ActionHandler.setText(SVO_OpportunityContainer.CloseDate, CloseDate);
		Reporter.addStepLog("User select the close date");
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// User selects the stage//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.StageDW);
		ActionHandler.wait(5);
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.StageSelection(Stage))));
		Reporter.addStepLog("User select the stage");
		CommonFunctions.attachScreenshot();

		// User selects Region,area and market//
		ActionHandler.click(SVO_OpportunityContainer.RegionDW);
		ActionHandler.wait(5);
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.RegionSelection(Region))));
		Reporter.addStepLog("User select the Region,area and market dropdown");
		CommonFunctions.attachScreenshot();

		// User selects Account name//
		ActionHandler.scrollToView(SVO_OpportunityContainer.Client);
		ActionHandler.wait(5);
		ActionHandler.wait(5);
		ActionHandler.setText(SVO_OpportunityContainer.AccountName, AccountName);
		ActionHandler.wait(5);
		ActionHandler.clearAndSetText(SVO_OpportunityContainer.AccountName, AccountName);
		ActionHandler.wait(5);
		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);
		Reporter.addStepLog("User enters Account Name");
		CommonFunctions.attachScreenshot();

		// User selects preferred contact name//
		ActionHandler.wait(5);
		ActionHandler.setText(SVO_OpportunityContainer.PrefContact, PreferredContact);
		ActionHandler.wait(3);
		ActionHandler.clearAndSetText(SVO_OpportunityContainer.PrefContact, PreferredContact);
		ActionHandler.wait(5);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Preferred Contact");
		ActionHandler.click(SVO_OpportunityContainer.SaveOpportunity);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

	}

	// Select Classic Continuation Opportunity
	@Then("^User selects Classic Continuation Opportunity$")
	public void user_selects_Classic_Continuation_Opportunity() throws Throwable {

		// User clicks on New button //
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.newOpportunityBtn);
		Reporter.addStepLog("User clicks on new Opportunity tab");
		CommonFunctions.attachScreenshot();

		// User selects record type and click on next//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.ClassicContinuation);
		Reporter.addStepLog("User selected recorded type");
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.nextRType);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on next button");

	}

	// Access SVO portal
	@Given("^Access the SVO Portal$")
	public void access_the_SVO_Portal() throws Throwable {
		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");
		onStart();
		driver = getDriver();
		ActionHandler.wait(13);
		driver.get(Constants.SVOURL);
		ActionHandler.wait(15);
		Reporter.addStepLog("User Logins to SVO Portal");

		String UserName = Config.getPropertyValue("Admin_UserName");
		String Password = Config.getPropertyValue("Admin_Password");

		ActionHandler.setText(SVO_OpportunityContainer.userNameTextBox, UserName);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVO_OpportunityContainer.passwordTextBox, Password);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVO_OpportunityContainer.loginBtn);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User requires to enter Verification code received in an email");
		ActionHandler.wait(5);

		if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.verificationCode)) {
			Reporter.addStepLog("Please Open recently received email from Salesforce");
			ActionHandler.wait(200);// LoginSVO();
		} else if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.mobileNumText)) {
			ActionHandler.wait(5);
			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.remindMeLater)) {
				ActionHandler.wait(3);
				ActionHandler.click(SVO_OpportunityContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.notRegister)) {
				ActionHandler.wait(3);
				ActionHandler.click(SVO_OpportunityContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();
			VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	public void navigateToSVO() throws Exception {
		String SVO = "Special Vehicle Operations";
		ActionHandler.wait(3);
		Reporter.addStepLog("User tries to navigate to SVO Portal");
		if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.jlrText)) {
			ActionHandler.wait(2);
			ActionHandler.click(SVO_OpportunityContainer.menuBtn);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVO_OpportunityContainer.searchBox, SVO);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.selectMenu(SVO))));
			ActionHandler.wait(10);
			CommonFunctions.attachScreenshot();

			Reporter.addStepLog("User navigate to SVO Portal");
		}
	}

	@When("^User searches for SVO Bespoke Closed opportunity and selects it$")
	public void user_searches_for_SVO_Bespoke_Closed_opportunity_and_selects_it() throws Throwable {
		ActionHandler.click(SVO_OpportunityContainer.selectlistview);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Reporter.addStepLog("User is able to navigate to the list view");
		ActionHandler.click(SVO_OpportunityContainer.BespokeOpp);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User is able to list all bespoke Opportunities");

		// user searches for opportunity and opens it
		String Search = "Closed";
		ActionHandler.click(SVO_OpportunityContainer.SearchOpportunity);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		ActionHandler.clearAndSetText(SVO_OpportunityContainer.SearchOpportunity, Search);
		CommonFunctions.attachScreenshot();
		ActionHandler.pressEnter();
		ActionHandler.wait(10);
		Reporter.addStepLog("Search for an opportunity");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVO_OpportunityContainer.OpportunitySel1);
		ActionHandler.wait(15);
		Reporter.addStepLog("Select opportunity");
		CommonFunctions.attachScreenshot();
	}

	@Then("^Validate that user should not be able to see Send Sales Agreement button$")
	public void validate_that_user_should_not_be_able_to_see_Send_Sales_Agreement_button() throws Throwable {
		ActionHandler.wait(3);
		if (!VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.SendAgreement)) {
			Reporter.addStepLog("User does not see Send sales agreement Button");
			CommonFunctions.attachScreenshot();
		}

	}

	@Then("^Validate that user should not be able to see Send POF button$")
	public void validate_that_user_should_not_be_able_to_see_Send_POF_button() throws Throwable {
		ActionHandler.wait(3);
		if (!VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.SendPOF)) {
			Reporter.addStepLog("User does not see Send POF Button");
			CommonFunctions.attachScreenshot();
		}
	}

	public void BespokeOpportunity() throws Exception {
		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.RecentlyViewedOpp);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.AllBespokeOpportunities);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User navigates to all Bespoke opportunities");
	}

	@And("^Open opportunity with Record Type \"([^\"]*)\"$")
	public void Open_Opportunity_with_record_type(String rType) throws Exception {
		String CLE[] = rType.split(",");
		rType = CommonFunctions.readExcelMasterData(CLE[0], CLE[1], CLE[2]);
		Reporter.addStepLog("Read the record type from master data");

		ActionHandler.wait(3);
		BespokeOpportunity();
		Reporter.addStepLog("User opens All Oppotunity");

		// user search for Classic Limited Edition Opportunity//
		ActionHandler.wait(5);
		ActionHandler.setText(SVO_OpportunityContainer.OpportSearchBox, rType);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.pressEnter();
		ActionHandler.wait(8);

		// user click on first classic limited edition opportunity//
		ActionHandler.click(SVO_OpportunityContainer.FirstOpportunity);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects first opportunity for applied filter");
	}

	@And("^Navigate to Quotes section under Related lists quick links section$")
	public void Navigate_to_Quotes_section_under_Related_lists_quick_links_section() throws Exception {

		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.SelectQuote);
		ActionHandler.wait(3);
		Reporter.addStepLog("Navigate to Quotes section under Related lists quick links section");
		CommonFunctions.attachScreenshot();
	}

	// Select a Quote which is at the Design Brief Draft stage
	@And("^Select a Quote which is at the Design Brief Draft stage$")
	public void Select_a_Quote_which_is_at_the_Design_Brief_Draft_stage() throws Exception {

		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.OpportunitySel11);
		ActionHandler.wait(3);
		Reporter.addStepLog("Select a Quote which is at the Design Brief Draft stage");
		CommonFunctions.attachScreenshot();
	}

	// validate Send Sales Agreement button
	@Then("^Verify the button Send Sales Agreement$")
	public void Verify_the_button_Send_Sales_Agreement() throws Throwable {
		ActionHandler.wait(3);
		if (!VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.SendSalesAgreement)) {
			Reporter.addStepLog("User does not see Send Sales Agreement Button");
			CommonFunctions.attachScreenshot();
		}
	}

	// navigate to opportunity tab
	@When("^User Navigate to Opportunity$")
	public void user_Navigate_to_Opportunity() throws Throwable {
		ActionHandler.wait(3);
		navigateToSVO();
		ActionHandler.wait(5);
		Reporter.addStepLog("User Navigate to Opportunity tab");
		javaScriptUtil.clickElementByJS(SVO_OpportunityContainer.Opportunity);
		ActionHandler.wait(15);
		CommonFunctions.attachScreenshot();
	}

	// Search Contract signed opportunity
	@When("^User searches for Contract signed opportunity and selects it$")
	public void user_searches_for_Contract_signed_opportunity_and_selects_it() throws Throwable {
		String Search = "Contract signed";

		ActionHandler.click(SVO_OpportunityContainer.SearchOpportunity);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		ActionHandler.clearAndSetText(SVO_OpportunityContainer.SearchOpportunity, Search);
		CommonFunctions.attachScreenshot();
		ActionHandler.pressEnter();
		ActionHandler.wait(10);
		Reporter.addStepLog("Search for an opportunity");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVO_OpportunityContainer.OpportunitySel1);
		ActionHandler.wait(15);
		Reporter.addStepLog("Select opportunity");
		CommonFunctions.attachScreenshot();
	}

	// Click on search vehicle
	@Then("^User clicks on search Vehicle$")
	public void user_clicks_on_search_Vehicle() throws Throwable {
		if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.VehicleSearchbutton)) {
			ActionHandler.click(SVO_OpportunityContainer.VehicleSearchbutton);
			ActionHandler.wait(15);
			Reporter.addStepLog("Click on Vehicle Search");
			CommonFunctions.attachScreenshot();
		}

		else {
			ActionHandler.click(SVO_OpportunityContainer.showmore);
			ActionHandler.wait(15);
			Reporter.addStepLog("Click on drop down");
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVO_OpportunityContainer.VehicleSearch);
			ActionHandler.wait(15);
			Reporter.addStepLog("Select Vehicle search");
			CommonFunctions.attachScreenshot();
		}
	}

	// Select Test brand
	@Then("^select brand as test brand and click search$")
	public void select_brand_as_test_brand_and_click_search() throws Throwable {
		String Brand = "TestBrand";

		ActionHandler.clearAndSetText(SVO_OpportunityContainer.VehicleBrandSearch, Brand);
		ActionHandler.wait(3);
		ActionHandler.pressEnter();
		ActionHandler.wait(4);
		Reporter.addStepLog("Select Vehicle Brand");
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.Search);
		ActionHandler.wait(15);
		Reporter.addStepLog("Click on the Search button");
		CommonFunctions.attachScreenshot();
	}

	// Create new brand
	@When("^click on new and enter Record name \"([^\"]*)\" Full VIN \"([^\"]*)\" Brand \"([^\"]*)\" and Model \"([^\"]*)\"$")
	public void click_on_new_and_enter_Record_name_Full_VIN_Brand_and_Model(String Record, String VIN, String Brand,
			String Model) throws Throwable {
		String Rec[] = Record.split(",");
		Record = CommonFunctions.readExcelMasterData(Rec[0], Rec[1], Rec[2]);

		String V[] = VIN.split(",");
		VIN = CommonFunctions.readExcelMasterData(V[0], V[1], V[2]);

		String Br[] = Brand.split(",");
		Brand = CommonFunctions.readExcelMasterData(Br[0], Br[1], Br[2]);

		String Mod[] = Model.split(",");
		Model = CommonFunctions.readExcelMasterData(Mod[0], Mod[1], Mod[2]);

		ActionHandler.pageDown();
		ActionHandler.wait(3);
		ActionHandler.pageDown();
		ActionHandler.wait(3);

		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.NewBrand);
		ActionHandler.wait(15);
		Reporter.addStepLog("Click on the New button");
		CommonFunctions.attachScreenshot();

		// enter record details
		ActionHandler.clearAndSetText(SVO_OpportunityContainer.RecordName, Record);
		ActionHandler.wait(5);
		Reporter.addStepLog("Input record Name");
		CommonFunctions.attachScreenshot();

		ActionHandler.clearAndSetText(SVO_OpportunityContainer.VIN, VIN);
		ActionHandler.wait(5);
		Reporter.addStepLog("Input VIN information");
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVO_OpportunityContainer.brand, Brand);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Input Brand information");

		// enter model details
		ActionHandler.setText(SVO_OpportunityContainer.model, Model);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Input Model information");
	}

	// Save vehicle information
	@When("^save the vehicle information$")
	public void save_the_vehicle_information() throws Throwable {
		ActionHandler.click(SVO_OpportunityContainer.SaveRelation);
		ActionHandler.wait(15);
		Reporter.addStepLog("Click on the Save button");
		CommonFunctions.attachScreenshot();

		VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.VerRecord);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verify that the vehicle Record is added");

		// enter brand
		VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.VerBrand);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verify that the vehicle Brand is added");

		// enter VIN
		VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.VerVIN);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verify that the vehicle VIN is added");

	}

	// Select stage Design Brief and Quote
	@Then("^User selects Design Brief and Quote and clicks on mark as current stage$")
	public void user_selects_Design_Brief_and_Quote_and_clicks_on_mark_as_current_stage() throws Throwable {
		ActionHandler.click(SVO_OpportunityContainer.DesignBrief);
		ActionHandler.wait(15);
		Reporter.addStepLog("Click on the Design Brief tab button");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVO_OpportunityContainer.MarkasCurrentStage);
		ActionHandler.wait(15);
		Reporter.addStepLog("Click on mark as current stage button");
		CommonFunctions.attachScreenshot();
	}

	// Verify error message
	@Then("^verify the displayed flow error message$")
	public void verify_the_displayed_flow_error_message() throws Throwable {
		ActionHandler.wait(4);
		VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.errorFlow);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verify that the flow error message is displayed");

		// verify error displayed
		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.errorclose);
		ActionHandler.wait(5);
		Reporter.addStepLog("Click on close error button");
		CommonFunctions.attachScreenshot();
	}

	// Search closed won opportunity
	@When("^User searches for Closed won opportunity \"([^\"]*)\" and selects it$")
	public void user_searches_for_Closed_won_opportunity_and_selects_it(String Search) throws Throwable {
		String Sea[] = Search.split(",");
		Search = CommonFunctions.readExcelMasterData(Sea[0], Sea[1], Sea[2]);

		ActionHandler.click(SVO_OpportunityContainer.SearchOpportunity);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		ActionHandler.clearAndSetText(SVO_OpportunityContainer.SearchOpportunity, Search);
		CommonFunctions.attachScreenshot();
		ActionHandler.pressEnter();
		ActionHandler.wait(10);
		Reporter.addStepLog("Search for an opportunity");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.addedRelationship(Search))));
		ActionHandler.wait(15);
		Reporter.addStepLog("Select opportunity");
		CommonFunctions.attachScreenshot();
	}

	// Click clone button
	@Then("^user clicks on dropdown and clicks on the clone button$")
	public void user_clicks_on_dropdown_and_clicks_on_the_clone_button() throws Throwable {
		ActionHandler.click(SVO_OpportunityContainer.ShowMore);
		ActionHandler.wait(15);
		Reporter.addStepLog("Click on drop down");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVO_OpportunityContainer.cloneopp);
		ActionHandler.wait(15);
		Reporter.addStepLog("Select Vehicle search");
		CommonFunctions.attachScreenshot();
	}

	// Save clone opportunity
	@Then("^user saves the clone opportunity$")
	public void user_saves_the_clone_opportunity() throws Throwable {
		ActionHandler.click(SVO_OpportunityContainer.SaveVehicle);
		ActionHandler.wait(15);
		Reporter.addStepLog("User clicks on save");
		CommonFunctions.attachScreenshot();
	}

	// Search for opportunity
	@When("^User searches for an opportunity \"([^\"]*)\" and selects it$")
	public void user_searches_for_an_opportunity_and_selects_it(String Search) throws Throwable {
		String Sea[] = Search.split(",");
		Search = CommonFunctions.readExcelMasterData(Sea[0], Sea[1], Sea[2]);

		ActionHandler.click(SVO_OpportunityContainer.SearchOpportunity);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		ActionHandler.clearAndSetText(SVO_OpportunityContainer.SearchOpportunity, Search);
		CommonFunctions.attachScreenshot();
		ActionHandler.pressEnter();
		ActionHandler.wait(10);
		Reporter.addStepLog("Search for an opportunity");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.addedRelationship(Search))));
		ActionHandler.wait(15);
		Reporter.addStepLog("Select opportunity");
		CommonFunctions.attachScreenshot();
	}

	// Select closed stage
	@Then("^user selects the closed tab and selects stage as \"([^\"]*)\" and Reason \"([^\"]*)\"$")
	public void user_selects_the_closed_tab_and_selects_stage_as_and_Reason(String stage, String Reason)
			throws Throwable {
		String Sea[] = stage.split(",");
		stage = CommonFunctions.readExcelMasterData(Sea[0], Sea[1], Sea[2]);

		String Rea[] = Reason.split(",");
		Reason = CommonFunctions.readExcelMasterData(Rea[0], Rea[1], Rea[2]);

		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.oppClosed);
		ActionHandler.wait(7);
		Reporter.addStepLog("User clicks close tab");
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.oppClosedStage);
		ActionHandler.wait(7);
		Reporter.addStepLog("User clicks Select Closed Stage");
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.oppStage);
		ActionHandler.wait(7);
		Reporter.addStepLog("User clicks on stage drop down");
		CommonFunctions.attachScreenshot();

		// select stage
		ActionHandler.wait(3);
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.ValueSelection(stage))));
		ActionHandler.wait(7);
		Reporter.addStepLog("Select stage");
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.OppLostReason);
		ActionHandler.wait(7);
		Reporter.addStepLog("User clicks on Reason");
		CommonFunctions.attachScreenshot();

		// select region
		ActionHandler.wait(3);
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.ValueSelection(Reason))));
		ActionHandler.wait(7);
		Reporter.addStepLog("Select Reason");
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.DoneButton);
		ActionHandler.wait(7);
		Reporter.addStepLog("User clicks on Done");
		CommonFunctions.attachScreenshot();
	}

	// change record type of opportunity
	@Then("^user changes the record type of the opportunity$")
	public void user_changes_the_record_type_of_the_opportunity() throws Throwable {
		ActionHandler.scrollDown();
		ActionHandler.wait(3);

		ActionHandler.click(SVO_OpportunityContainer.changeRecType);
		ActionHandler.wait(15);
		Reporter.addStepLog("User clicks record type button");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVO_OpportunityContainer.ClassicReborn);
		ActionHandler.wait(15);
		Reporter.addStepLog("User clicks classic Reborn");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVO_OpportunityContainer.Next);
		ActionHandler.wait(15);
		Reporter.addStepLog("User clicks Next");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVO_OpportunityContainer.SaveVehicle);
		ActionHandler.wait(15);
		Reporter.addStepLog("User clicks on save vehicle");
		CommonFunctions.attachScreenshot();
	}

	// select stage contracting
	@Then("^user clicks on contracting and clicks on Mark as current stage$")
	public void user_clicks_on_contracting_and_clicks_on_Mark_as_current_stage() throws Throwable {
		ActionHandler.wait(4);
		ActionHandler.click(SVO_OpportunityContainer.ContractingTab);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on contracting");
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.MarkasCurrentStage);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on Mark as current stage");
		CommonFunctions.attachScreenshot();
	}

	// create new opportunity
	@Then("^User Creates a New opportunity with Record Type as \"([^\"]*)\"$")
	public void user_Creates_a_New_opportunity_with_Record_Type_as(String recordType) throws Throwable {
		String rType[] = recordType.split(",");
		recordType = CommonFunctions.readExcelMasterData(rType[0], rType[1], rType[2]);

		ActionHandler.wait(4);
		Reporter.addStepLog("User Creates new Opportunity");
		ActionHandler.click(SVO_OpportunityContainer.newOpportunityBtn);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		// user selects the opportunity type
		Reporter.addStepLog("User Needs to select any record type option");
		ActionHandler.wait(3);
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.OpportunityRecordType(recordType))));
		Reporter.addStepLog("User Select required Record Type");
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(4);
		ActionHandler.click(SVO_OpportunityContainer.nextRType);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigate to enter enquiry details page");
	}

	// Enter details for new opportunity
	@Then("^Enter Required details of New Opportunity like Stage Type \"([^\"]*)\" Account Name \"([^\"]*)\" Region \"([^\"]*)\" Preferred Contact \"([^\"]*)\"$")
	public void enter_Required_details_of_New_Opportunity_like_Stage_Type_Account_Name_Region_Preferred_Contact(
			String stage, String AccountName, String region, String preContact) throws Throwable {
		/*
		 * Below code is used to extract data from Excel Master Data and assign it to a
		 * string
		 */

		String oAccountName[] = AccountName.split(",");
		AccountName = CommonFunctions.readExcelMasterData(oAccountName[0], oAccountName[1], oAccountName[2]);

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String pc[] = preContact.split(",");
		preContact = CommonFunctions.readExcelMasterData(pc[0], pc[1], pc[2]);

		/* Below strings contains information required to fill the New Opportunity */
		double randomNumber = getRandomIntegerBetweenRange(0, 1000);
		String OpportunityName = "Test_Opportunity_";
		String requestDetails = "Create New Opportunity for Testing Perspective";
		// String Stage = "Qualified";

		Reporter.addStepLog("Start entering mandatory details");
		ActionHandler.wait(3);

		ActionHandler.wait(4);
		Reporter.addStepLog("User Creates new Opportunity");
		ActionHandler.click(SVO_OpportunityContainer.newOpportunityBtn);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
	}

	// create new programme
	@Then("^user navigated to Programme Field and clicks on new$")
	public void user_navigated_to_Programme_Field_and_clicks_on_new() throws Throwable {
		ActionHandler.pageDown();
		ActionHandler.wait(2);
		ActionHandler.pageDown();
		ActionHandler.wait(2);

		ActionHandler.click(SVO_OpportunityContainer.Programmes);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on Programme Field");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVO_OpportunityContainer.newProgrammes);
		ActionHandler.wait(15);
		Reporter.addStepLog("User clicks on new Programme Field");
		CommonFunctions.attachScreenshot();

	}

	// enter details for programme
	@Then("^enter the required information like name \"([^\"]*)\" product offering \"([^\"]*)\" build capacity \"([^\\\"]*)\" brand \"([^\"]*)\" and model \"([^\"]*)\"$")
	public void enter_the_required_information_like_name_product_offering_brand_and_model(String name, String offering,
			String build, String Brand, String Model) throws Throwable {
		String Rec[] = name.split(",");
		name = CommonFunctions.readExcelMasterData(Rec[0], Rec[1], Rec[2]);

		String V[] = offering.split(",");
		offering = CommonFunctions.readExcelMasterData(V[0], V[1], V[2]);

		String B[] = build.split(",");
		build = CommonFunctions.readExcelMasterData(B[0], B[1], B[2]);

		String Br[] = Brand.split(",");
		Brand = CommonFunctions.readExcelMasterData(Br[0], Br[1], Br[2]);

		String Mod[] = Model.split(",");
		Model = CommonFunctions.readExcelMasterData(Mod[0], Mod[1], Mod[2]);

		ActionHandler.setText(SVO_OpportunityContainer.name, name);
		ActionHandler.wait(5);
		Reporter.addStepLog("User enters a name");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVO_OpportunityContainer.prodoffer);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on product");
		CommonFunctions.attachScreenshot();

		// enter product offering
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.dropdown(offering))));
		ActionHandler.wait(5);
		Reporter.addStepLog("Select from drop down");
		CommonFunctions.attachScreenshot();

		// enter build details
		ActionHandler.setText(SVO_OpportunityContainer.BuildCap, build);
		ActionHandler.wait(5);
		Reporter.addStepLog("User enters Build capacity");
		CommonFunctions.attachScreenshot();

		ActionHandler.pageDown();
		ActionHandler.wait(5);

		// enter brand
		ActionHandler.setText(SVO_OpportunityContainer.probrand, Brand);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Input Brand information");

		// enter model
		ActionHandler.setText(SVO_OpportunityContainer.promodel, Model);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Input Model information");
	}

	// save programme
	@Then("^save the programme$")
	public void save_the_programme() throws Throwable {
		ActionHandler.click(SVO_OpportunityContainer.SaveShare);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on save");
		CommonFunctions.attachScreenshot();
	}

// cancel opportunity
	@Then("^cancel the opportunity creation$")
	public void cancel_the_opportunity_creation() throws Throwable {
		ActionHandler.click(SVO_OpportunityContainer.cancelVehicle);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on cancel");
		CommonFunctions.attachScreenshot();
	}

	// go to S-docs
	@Then("^User clicks on Send Sales Agreement button$")
	public void user_clicks_on_Send_Sales_Agreement_button() throws Throwable {
		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.sendSalesAgreement);
		ActionHandler.wait(15);
		Reporter.addStepLog("User clicks the Send Sales Agreement button");
		CommonFunctions.attachScreenshot();

		ActionHandler.waitForElement(SVO_OpportunityContainer.draftText, 90);
	}

	@And("^User verifies all the details and send the email to sign the agreement$")
	public void User_verifies_all_details_send_email_sign_agreement() throws Exception {
		ActionHandler.wait(3);

		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		String parentWindow = driver.getWindowHandle();
		ActionHandler.click(SVO_OpportunityContainer.sendESignEmail);
		ActionHandler.wait(10);

		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!parentWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVO_OpportunityContainer.OKButton);
				ActionHandler.wait(12);
				CommonFunctions.attachScreenshot();

				driver.switchTo().window(parentWindow);
			}
		}

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User sends an email to sign the agreement");
	}

	@Then("^User access to User Email address to sign the contract$")
	public void User_access_user_email_address_sign_contract() throws Exception {

		ActionHandler.wait(2);
		String userName = Config.getPropertyValue("sSignEmail_UserName");
		String password = Config.getPropertyValue("sSignEmail_Password");

		ActionHandler.wait(5);
		// onStart();
		driver.get(Constants.outlookURL);
		ActionHandler.wait(10);
		Reporter.addStepLog("User tries to login to Outlook to sign the contract");
		CommonFunctions.attachScreenshot();

		if (!VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.outlookIcon)) {
			ActionHandler.wait(2);
			ActionHandler.click(SVO_OpportunityContainer.outlookSignin);
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVO_OpportunityContainer.outlookUsername, userName);
			ActionHandler.wait(7);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVO_OpportunityContainer.outlookNext);
			ActionHandler.wait(7);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVO_OpportunityContainer.outlookPassword, password);
			ActionHandler.wait(7);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVO_OpportunityContainer.outlookSignInBtn);
			ActionHandler.wait(10);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVO_OpportunityContainer.outlookIcon);
		ActionHandler.wait(30);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVO_OpportunityContainer.signFirstEmail);
				ActionHandler.wait(4);
				CommonFunctions.attachScreenshot();
				Reporter.addStepLog("User opens very first email");

				ActionHandler.wait(1);
				ActionHandler.pageScrollDown();
				ActionHandler.wait(3);

				ActionHandler.wait(2);
				String SSignLink = driver.getWindowHandle();
				System.out.println("S Sign Link = " + SSignLink);

				ActionHandler.click(SVO_OpportunityContainer.ESignLink);
				ActionHandler.wait(15);

				Set<String> sSignWindows = driver.getWindowHandles();
				Iterator<String> i1 = sSignWindows.iterator();
				String sSignWindow1 = i1.next();
				System.out.println(driver.getTitle());

				String sSignWindow2 = i1.next();
				System.out.println(driver.getTitle());

				String sSignWindow3 = i1.next();
				System.out.println(driver.getTitle());

				if (!SSignLink.equalsIgnoreCase(sSignWindow3)) {
					driver.switchTo().window(sSignWindow3);
					ActionHandler.wait(3);
					ActionHandler.click(SVO_OpportunityContainer.startSigning);
					ActionHandler.wait(7);
					CommonFunctions.attachScreenshot();

					ActionHandler.wait(2);
					ActionHandler.click(SVO_OpportunityContainer.clickToSign);
					ActionHandler.wait(7);
					CommonFunctions.attachScreenshot();

					/*
					 * ActionHandler.wait(4); ActionHandler.pageScrollDown(); ActionHandler.wait(1);
					 * ActionHandler.pageScrollDown(); ActionHandler.wait(1);
					 * ActionHandler.pageScrollDown(); ActionHandler.wait(3);
					 * CommonFunctions.attachScreenshot();
					 */

					ActionHandler.wait(2);
					ActionHandler.click(SVO_OpportunityContainer.signHereTextBox);
					ActionHandler.wait(5);
					ActionHandler.setText(SVO_OpportunityContainer.signHereTextBox, "Mauli Soni");
					ActionHandler.wait(2);
					CommonFunctions.attachScreenshot();

					ActionHandler.wait(2);
					ActionHandler.click(SVO_OpportunityContainer.signApplyBtn);
					ActionHandler.wait(25);
					CommonFunctions.attachScreenshot();
					Reporter.addStepLog("User signed contract of S-Sign");

					ActionHandler.click(SVO_OpportunityContainer.clickToSignBtn);
					ActionHandler.wait(7);
					CommonFunctions.attachScreenshot();

					ActionHandler.wait(2);
					VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.alldoneMessage);
					ActionHandler.wait(2);
					CommonFunctions.attachScreenshot();

					driver.close();
					driver.switchTo().window(SSignLink);
				}

				CommonFunctions.attachScreenshot();
				ActionHandler.wait(10);
				ActionHandler.click(SVO_OpportunityContainer.signedEmail);
				ActionHandler.wait(7);
				CommonFunctions.attachScreenshot();
				ActionHandler.wait(4);
				CommonFunctions.attachScreenshot();

				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}

		Reporter.addStepLog("User verifies that confirmation email should be received");
	}

	@And("^User verifies that the contract saved in agreement section of an opportunity and status is signed$")
	public void User_verifies_that_the_contract_saved_agreement_section_of_an_opportunity_status_signed()
			throws Throwable {
		driver = getDriver();
		SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
		SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);

		ActionHandler.wait(3);
		driver.get(Constants.SVOURL);
		ActionHandler.wait(15);
		Reporter.addStepLog("User Logins to SVO Portal");

		String UserName = Config.getPropertyValue("Admin_UserName");
		String Password = Config.getPropertyValue("Admin_Password");

		ActionHandler.setText(SVO_OpportunityContainer.userNameTextBox, UserName);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVO_OpportunityContainer.passwordTextBox, Password);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVO_OpportunityContainer.loginBtn);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User logins back to SVO Portal");

		ActionHandler.wait(2);
		user_Navigate_to_Opportunity();
		Reporter.addStepLog("User navigate to Opportunity");

		ActionHandler.wait(1);
		ActionHandler.click(SVO_OpportunityContainer.OpportunitySel1);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens first opportunity");

		ActionHandler.wait(2);
		navigate_to_Quotes();
		Reporter.addStepLog("User navigate to Quotes Section");

		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.firstQuote);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens first Quote");

		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(SVO_OpportunityContainer.agreementsSection);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Agreements section");

		ActionHandler.wait(1);
		VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.contractStatus);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the Contract status is Signed");

		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.agreementName);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens first agreement using its name");

		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.signedPDF);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
	}

	@And("^User navigate to Agreement section of an opportunity and verify the status of contract is signed$")
	public void User_navigate_agreement_section_opportunity_verify_status_contract_signed() throws Throwable {
		driver = getDriver();
		SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
		SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);

		ActionHandler.wait(3);
		driver.get(Constants.SVOURL);
		ActionHandler.wait(15);
		Reporter.addStepLog("User Logins to SVO Portal");

		String UserName = Config.getPropertyValue("Admin_UserName");
		String Password = Config.getPropertyValue("Admin_Password");

		ActionHandler.setText(SVO_OpportunityContainer.userNameTextBox, UserName);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVO_OpportunityContainer.passwordTextBox, Password);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVO_OpportunityContainer.loginBtn);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User logins back to SVO Portal");

		ActionHandler.wait(2);
		user_Navigate_to_Opportunity();
		Reporter.addStepLog("User navigate to Opportunity");

		ActionHandler.wait(1);
		ActionHandler.click(SVO_OpportunityContainer.OpportunitySel1);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens first opportunity");

		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(SVO_OpportunityContainer.agreementsSection);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Agreements section");

		ActionHandler.wait(1);
		VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.contractStatus);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the Contract status is Signed");

		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.agreementName);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens first agreement using its name");

		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.signedPDF);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
	}

	@And("^User verifies that an error occurred when user removes added file$")
	public void User_verifies_error_message_user_removes_added_file() {
		ActionHandler.wait(3);

	}

	// select a file for s-docs
	@When("^user selects a file and clicks on next step$")
	public void user_selects_a_file_and_clicks_on_next_step() throws Throwable {
		driver.switchTo().frame(0);
		ActionHandler.wait(6);
		ActionHandler.pageDown();
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.selectFile);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on a file");
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.Nextstep);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on next step");
		CommonFunctions.attachScreenshot();
		driver.switchTo().parentFrame();

		if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.GenDoc)) {
			driver.switchTo().frame(0);
			ActionHandler.wait(7);
			ActionHandler.click(SVO_OpportunityContainer.GenDoc);
			ActionHandler.wait(5);
			Reporter.addStepLog("User clicks the Genarate Document");
			CommonFunctions.attachScreenshot();
			driver.switchTo().parentFrame();
		}
	}

	// click on E-sign in s-doc
	@Then("^user clicks on the E-sign button to send an email of the document$")
	public void user_clicks_on_the_E_sign_button_to_send_an_email_of_the_document() throws Throwable {
		driver.switchTo().frame(0);
		ActionHandler.wait(7);

		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.ESign);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on E-Sign");
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.Apply);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on the send button");
		CommonFunctions.attachScreenshot();
	}

	// Add files from files section
	@Then("^user navigates to file section and clicks on add files$")
	public void user_navigates_to_file_section_and_clicks_on_add_files() throws Throwable {
		ActionHandler.pageDown();
		ActionHandler.wait(5);
		ActionHandler.pageDown();
		ActionHandler.wait(5);

		ActionHandler.click(SVO_OpportunityContainer.Filesbutton);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks file option");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVO_OpportunityContainer.AddFiles);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on Add files option");
		CommonFunctions.attachScreenshot();
	}

	// upload file
	public void uploadFile(String file) throws IOException {
		ActionHandler.wait(10);
		Runtime.getRuntime().exec(System.getProperty("user.dir") + "/upload.exe" + " " + System.getProperty("user.dir")
				+ "\\Files\\" + file);
		CommonFunctions.attachScreenshot();
	}

	// Click on upload file button
	@Then("^select a file and click on upload Files$")
	public void select_a_file_and_click_on_upload_Files() throws Throwable {
		ActionHandler.click(SVO_OpportunityContainer.UploadFiles);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on upload button");
		CommonFunctions.attachScreenshot();

		uploadFile("SVO.txt");
		ActionHandler.wait(60);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVO_OpportunityContainer.Done);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on done button");
		CommonFunctions.attachScreenshot();
	}

	// Search closed opportunity
	@When("^User searches for Closed opportunity and selects it$")
	public void user_searches_for_Closed_opportunity_and_selects_it() throws Throwable {
		ActionHandler.click(SVO_OpportunityContainer.selectlistview);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Reporter.addStepLog("User is able to navigate to the list view");
		ActionHandler.click(SVO_OpportunityContainer.ClassicService);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User is able to list all Classic Service Opportunities");

		// user searches for opportunity and opens it
		String Search = "Closed";
		ActionHandler.click(SVO_OpportunityContainer.SearchOpportunity);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		ActionHandler.clearAndSetText(SVO_OpportunityContainer.SearchOpportunity, Search);
		CommonFunctions.attachScreenshot();
		ActionHandler.pressEnter();
		ActionHandler.wait(10);
		Reporter.addStepLog("Search for an opportunity");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVO_OpportunityContainer.OpportunitySel1);
		ActionHandler.wait(15);
		Reporter.addStepLog("Select opportunity");
		CommonFunctions.attachScreenshot();
	}

	// search for Classic Service opportunity
	@When("^User searches for Classic Service opportunity and selects it$")
	public void user_searches_for_Classic_Service_opportunity_and_selects_it() throws Throwable {
		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.recentlyViewed);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(4);
		Reporter.addStepLog("User is able to navigate to the list view");

		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.ClassicService);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User is able to list all Classic Service Opportunities");

		// user searches for opportunity and opens it
		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.OpportunitySel1);
		ActionHandler.wait(3);
		Reporter.addStepLog("Select opportunity");
		CommonFunctions.attachScreenshot();
	}

	// remove the Pricing details
	@Then("^User remove the Pricing details$")
	public void user_remove_the_Pricing_details() throws Throwable {
		ActionHandler.wait(5);
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.wait(5);

		// user clears all the pricing details from the page
		ActionHandler.click(SVO_OpportunityContainer.EditCostPrice);
		ActionHandler.wait(5);
		ActionHandler.clearText(SVO_OpportunityContainer.CostPriceTextBox);
		ActionHandler.wait(3);
		ActionHandler.clearText(SVO_OpportunityContainer.ListPriceTextBox);
		ActionHandler.wait(3);
		ActionHandler.clearText(SVO_OpportunityContainer.RetailPriceTextBox);
		ActionHandler.wait(3);
		ActionHandler.clearText(SVO_OpportunityContainer.ContributionTextBox);
		ActionHandler.wait(3);
		ActionHandler.clearText(SVO_OpportunityContainer.RevenueTextBox);
		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.SaveButton);
		Reporter.addStepLog("User clears price details and saved it");
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
	}

	// Move the stage to Order placed
	@Then("^User Move the stage to Order placed$")
	public void user_Move_the_stage_to_Order_placed() throws Throwable {
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.OrderPlaced);
		ActionHandler.wait(5);

		// user marks stage as order placed
		ActionHandler.click(SVO_OpportunityContainer.MarkAsCurrentStage);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicked on Mark as Current stage button");
		CommonFunctions.attachScreenshot();
	}

	// verify error message for pricing details
	@Then("^Verify the error message to fill the pricing details$")
	public void verify_the_error_message_to_fill_the_pricing_details() throws Throwable {
		ActionHandler.wait(5);

		// user verifies the error message for pricing details
		if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.PriceError)) {
			Reporter.addStepLog("Error message validated");
			CommonFunctions.attachScreenshot();
		} else {
			Reporter.addStepLog("No error message was caught");
			CommonFunctions.attachScreenshot();
		}
	}

	// select opportunity in Estimate stage
	@Then("^Open an opportunity which is in Estimate stage$")
	public void open_an_opportunity_which_is_in_Estimate_stage() throws Throwable {
		ActionHandler.wait(5);
		String Opportunity = "Estimate";
		ActionHandler.clearAndSetText(SVO_OpportunityContainer.OpportSearchBox, Opportunity);
		ActionHandler.wait(3);
		ActionHandler.pressEnter();
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.FirstOpportunity);
		Reporter.addStepLog("User selected opportunity which is in Estimate stage");
		CommonFunctions.attachScreenshot();
	}

	// enter production year
	@Then("^User enters production year of Vehicle$")
	public void user_enters_production_year_of_Vehicle() throws Throwable {
		ActionHandler.wait(5);
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.wait(5);

		// user enters production year and saves
		ActionHandler.click(SVO_OpportunityContainer.EditProductionYear);
		ActionHandler.wait(5);
		String Year = "2020";
		ActionHandler.clearAndSetText(SVO_OpportunityContainer.ProductionYearTextBox, Year);
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.SaveButton);
		ActionHandler.wait(5);
		Reporter.addStepLog("User entered production year of Vehicle and saved it");
		CommonFunctions.attachScreenshot();
	}

	// uncheck the Restricted party screening
	@Then("^User unchecked the Restricted party screening$")
	public void user_unchecked_the_Restricted_party_screening() throws Throwable {
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.wait(3);

		// user unchecks the restricted party screening field
		ActionHandler.click(SVO_OpportunityContainer.EditRestrictedPS);
		ActionHandler.wait(4);
		ActionHandler.click(SVO_OpportunityContainer.RestrictedDropDown);
		ActionHandler.wait(4);
		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);
		ActionHandler.click(SVO_OpportunityContainer.SaveRPS);
		ActionHandler.wait(5);
		ActionHandler.pageCompleteScrollUp();
		Reporter.addStepLog("Uncheck the Restricted Party Screening");
		CommonFunctions.attachScreenshot();
	}

	// verify error message for Restricted party screening
	@Then("^Verify the error message to clear the Restricted party screening$")
	public void verify_the_error_message_to_clear_the_Restricted_party_screening() throws Throwable {
		ActionHandler.wait(5);

		// user verifies error message to clear Restricted party screening
		if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.ErrorRPS)) {
			Reporter.addStepLog("Error message validated");
			CommonFunctions.attachScreenshot();
		} else {
			Reporter.addStepLog("No error message was caught");
			CommonFunctions.attachScreenshot();
		}
	}

	// select All Classic Bespoke opportunities
	@Then("^User selects All Classic Bespoke opportunities from select list view$")
	public void user_selects_All_Classic_Bespoke_opportunities_from_select_list_view() throws Throwable {
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.SelectListView);
		ActionHandler.wait(5);

		// user views all classic bespoke
		ActionHandler.setText(SVO_OpportunityContainer.ListViewTextBox, "All Classic Bespoke");
		ActionHandler.wait(5);
		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		Reporter.addScenarioLog("User chooses to view all Classic Bespoke opportunities");
		CommonFunctions.attachScreenshot();
	}

	// select opportunity in OrderPlaced stage
	@Then("^Open an opportunity which is in OrderPlaced stage$")
	public void open_an_opportunity_which_is_in_OrderPlaced_stage() throws Throwable {
		ActionHandler.wait(5);
		ActionHandler.clearAndSetText(SVO_OpportunityContainer.OpportSearchBox, "Order Placed");
		ActionHandler.wait(3);
		ActionHandler.pressEnter();
		ActionHandler.wait(5);

		// user opens an opportunity for order placed stage
		ActionHandler.click(SVO_OpportunityContainer.FirstOpportunity);
		ActionHandler.wait(5);
		Reporter.addStepLog("User selected classic Bespoke Opportunity with stage Order placeds");
		CommonFunctions.attachScreenshot();
	}

	// select stage as ClosedWon
	@Then("^User selects stage as ClosedWon$")
	public void user_selects_stage_as_ClosedWon() throws Throwable {
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.Donebutton);
		Reporter.addScenarioLog("User chooses closes won stage");
		CommonFunctions.attachScreenshot();
	}

	// error message to close the opportunity
	@Then("^Verify the error message to close the opportunity$")
	public void verify_the_error_message_to_close_the_opportunity() throws Throwable {
		ActionHandler.wait(5);
		// user verifies error to close opportunity
		if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.ErrorRPS)) {
			Reporter.addStepLog("Error message validated");
			CommonFunctions.attachScreenshot();
		} else {
			Reporter.addStepLog("No error message was caught");
			CommonFunctions.attachScreenshot();
		}
	}

	// error message to fill Opportunity Currency
	@Then("^Verify the error message to fill Opportunity Currency$")
	public void verify_the_error_message_to_fill_Opportunity_Currency() throws Throwable {
		ActionHandler.wait(5);

		// user verifies error to fill opportunity currency
		if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.ErrorOpportunityCurrency)) {
			Reporter.addStepLog("Error message validated");
			CommonFunctions.attachScreenshot();
		} else {
			Reporter.addStepLog("No error message was caught");
			CommonFunctions.attachScreenshot();
		}
	}

	@When("^User tries to enter invalid production year as \"([^\"]*)\"$")
	public void User_tries_to_enter_invalid_production_year(String year) throws Exception {
		String B[] = year.split(",");
		year = CommonFunctions.readExcelMasterData(B[0], B[1], B[2]);

		ActionHandler.wait(5);
		driver.navigate().refresh();
		ActionHandler.wait(15);

		for (int i = 0; i < 10; i++) {
			ActionHandler.scrollDown();
		}

		// user click on Edit Production Year pencil icon
		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.EditProductionYear);
		ActionHandler.wait(3);
		Reporter.addStepLog("user click on Edit Production Year pencil icon");
		CommonFunctions.attachScreenshot();

		// user enters an invalid production year
		ActionHandler.wait(2);
		// ActionHandler.setText(SVO_OpportunityContainer.ProductionYearTxtBx, year);
		ActionHandler.wait(2);
		Reporter.addStepLog("user enters an invalid production year");
		CommonFunctions.attachScreenshot();

		// user click on save button
		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.SaveVehicle);
		ActionHandler.wait(3);
		Reporter.addStepLog("user click on save button");
		CommonFunctions.attachScreenshot();

	}

	// create new invoice
	@Then("^User creates new invoice by entering mandatory fields$")
	public void user_creates_new_invoice_by_entering_mandatory_fields() throws Throwable {
		ActionHandler.wait(5);
		// user selects opportunity
		/*
		 * ActionHandler.setText(SVO_OpportunityContainer.OpportunityTxtBx, "test");
		 * Actions act = new Actions(driver);
		 * act.sendKeys(Keys.ARROW_DOWN).build().perform();
		 * act.sendKeys(Keys.ENTER).build().perform();
		 * Reporter.addStepLog("User selects Opportunity from search list");
		 * ActionHandler.wait(5);
		 */

		// user selects account name
		ActionHandler.wait(5);
		ActionHandler.setText(SVO_OpportunityContainer.AccountTxtBx, "Test");
		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		act1.sendKeys(Keys.ENTER).build().perform();
		Reporter.addStepLog("User selects Account name from search list");
		ActionHandler.wait(5);
		commonFunctions.attachScreenshot();

		ActionHandler.wait(5);
		ActionHandler.setText(SVO_OpportunityContainer.SAPtxtBx, "test");
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.NewSAPAcnt);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// user enters SAP ID and saves
		ActionHandler.wait(5);
		String SapId = "test_";
		double randomNumber = getRandomIntegerBetweenRange(0, 10000);
		ActionHandler.setText(SVO_OpportunityContainer.SAPid, SapId + randomNumber);
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.SaveSAPBtn);
		Reporter.addStepLog("User save the sap id");
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.SaveButton);
		CommonFunctions.attachScreenshot();
	}

	// select an Invoice linked to Order
	@Then("^User selects an Invoice linked to Order$")
	public void user_selects_an_Invoice_linked_to_Order() throws Throwable {
		ActionHandler.wait(5);
		driver.navigate().refresh();
		ActionHandler.wait(15);

		// User click on created invoice number
		ActionHandler.click(SVO_OpportunityContainer.FirstOpportunity);
		ActionHandler.wait(5);
		Reporter.addStepLog("User click on created invoice number");
		CommonFunctions.attachScreenshot();
	}

	// Change the Record type of an Invoice
	@Then("^User Change the Record type of an Invoice$")
	public void user_Change_the_Record_type_of_an_Invoice() throws Throwable {

		// User click on Invoice drop down
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.InvoiceDropDown);
		ActionHandler.wait(5);
		Reporter.addStepLog("User click on Invoice drop down");
		CommonFunctions.attachScreenshot();

		// User click on Change record type button
		ActionHandler.click(SVO_OpportunityContainer.ChangeRecordType);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Change record type button");

		// User seletc the record type
		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.CreditNoteInvoiceType);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User seletc the record type");

		// user click on Next button
		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.NextButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on Next button");

		// user saves the changes made
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.SaveButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User save the invoice");

	}

	// clear HandOver details
	@Then("^User clears HandOver details$")
	public void user_clears_HandOver_details() throws Throwable {
		ActionHandler.wait(5);
		// user clears the fields in hand over details and saves
		ActionHandler.click(SVO_OpportunityContainer.EditVehicle);
		ActionHandler.wait(5);
		ActionHandler.clearText(SVO_OpportunityContainer.HandOverDate);
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.SaveBtn);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clears the HandOver details");
	}

	// enter Payment details of an Invoice
	@Then("^User fills Payment details of an Invoice$")
	public void user_fills_Payment_details_of_an_Invoice() throws Throwable {
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.EditPaymentValue);
		ActionHandler.wait(5);
		String Date = "16/02/2021";

		// user enters all the payment details for invoice
		ActionHandler.clearAndSetText(SVO_OpportunityContainer.SendByDate, Date);
		ActionHandler.wait(5);
		ActionHandler.clearAndSetText(SVO_OpportunityContainer.PaymentMadeDate, Date);
		ActionHandler.wait(5);
		ActionHandler.clearAndSetText(SVO_OpportunityContainer.SentDate, Date);
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.SaveOpportunity);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Retailed Outcome");

	}

	// edit the Close date
	@Then("^User edits the Close date of an Opportunity$")
	public void user_edits_the_Close_date_of_an_Opportunity() throws Throwable {
		ActionHandler.wait(5);
		// user changes the close date for an opportunity
		ActionHandler.click(SVO_OpportunityContainer.EditCloseDate);
		ActionHandler.wait(5);
		ActionHandler.clearAndSetText(SVO_OpportunityContainer.CloseDateTextBox, "14/03/2021");
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.SaveOpportunity);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User edits the Close date of an Opportunity");
	}

	// close opportunity
	@Then("^User mark status as closed$")
	public void user_mark_status_as_closed() throws Throwable {
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.ClosedTab);
		ActionHandler.wait(5);
		// user marks opportunity stage as closed
		ActionHandler.click(SVO_OpportunityContainer.SelectClosedStage);
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.DoneBtn);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User mark status as closed");
	}

	// enter pinewood job card number
	@Then("^User enters pinewood job card number$")
	public void user_enters_pinewood_job_card_number() throws Throwable {
		ActionHandler.wait(5);
		// user enters all the details for pinewood card number section
		ActionHandler.click(SVO_OpportunityContainer.EditPineWoodJobCard);
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.PineWoodJobCardTxtBox);
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.BuildNumberTxtBox);
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.SaveRPS);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters pinewood job card number");

	}

	// select All Bespoke opportunities
	@Then("^User selects All Bespoke opportunities from select list view$")
	public void user_selects_All_Bespoke_opportunities_from_select_list_view() throws Throwable {
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.SelectListView);
		ActionHandler.wait(5);
		// user selects to view all bespoke opportunities
		ActionHandler.setText(SVO_OpportunityContainer.ListViewTextBox, "All Bespoke Opportunities");
		ActionHandler.wait(5);
		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		Reporter.addScenarioLog("User chooses to view all Bespoke opportunities");
		CommonFunctions.attachScreenshot();
	}

	// create Bespoke Design Brief Quote
	@Then("^User created new Bespoke Design Brief Quote$")
	public void user_created_new_Bespoke_Design_Brief_Quote() throws Throwable {
		ActionHandler.wait(5);
		// user creates a new bespoke design brief quote
		ActionHandler.click(SVO_OpportunityContainer.NewQuoteBtn);
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.BespokeDesignBrief);
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.nextRType);
		ActionHandler.wait(5);
		double randomNumber = getRandomIntegerBetweenRange(0, 1000);
		String name = "Test_";
		// user enters all the mandatory fields
		ActionHandler.setText(SVO_OpportunityContainer.QuoteNameTextBox, name + randomNumber);
		ActionHandler.wait(5);
		String RetailerContact = "";
		ActionHandler.setText(SVO_OpportunityContainer.RetailerContactTxtBx, RetailerContact);
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.SaveRPS);
		ActionHandler.wait(5);
		Reporter.addScenarioLog("User Creates new quote");
		CommonFunctions.attachScreenshot();
	}

	// clear Design brief section
	@Then("^User clears Design brief section$")
	public void user_clears_Design_brief_section() throws Throwable {
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.EditDesignBrief);
		ActionHandler.wait(5);

		// user clears design brief section fields
		ActionHandler.click(SVO_OpportunityContainer.ClearDesignBrief);
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.SaveRPS);
		ActionHandler.wait(5);
		Reporter.addScenarioLog("User clears Design brief section");
		CommonFunctions.attachScreenshot();

		// User click on Mark status as complete button of quote//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.MarkStatusAsComplete);
		ActionHandler.wait(5);
		Reporter.addScenarioLog("User clicks on Marks status as complete button");
		CommonFunctions.attachScreenshot();

	}

	// navigate to Design brief section
	@Then("^User navigate to Design brief section$")
	public void user_navigate_to_Design_brief_section() throws Throwable {
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.DesignBriefLink);
		ActionHandler.wait(5);
		Reporter.addScenarioLog("User navigate to Design brief section");
		CommonFunctions.attachScreenshot();
	}

	// uncheck the design team
	@Then("^User uncheck the design team$")
	public void user_uncheck_the_design_team() throws Throwable {
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.FirstQuote);
		ActionHandler.wait(5);
		// user opens the quote and unchecks the design checkbox
		ActionHandler.click(SVO_OpportunityContainer.EditRequiredDesign);
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.RequiredDesignCheckBx);
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.SaveRPS);
		Reporter.addScenarioLog("UnChecked the Required Design check box");
		CommonFunctions.attachScreenshot();

	}

	// search opportunity
	@When("^Search for Opportunity with stage \"([^\"]*)\" and open relevant Opportunity$")
	public void search_for_the_opportunity_with_stage_name_and_open_relevant_opportunity(String stage)
			throws Throwable {
		/* Used to enter Opportunity Name in Search box */
		ActionHandler.wait(3);
		AllOpportunity();

		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.OpportunitiesSearchBox);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		ActionHandler.clearAndSetText(SVO_OpportunityContainer.OpportunitiesSearchBox, stage);
		CommonFunctions.attachScreenshot();

		/* Used to press Enter key */
		ActionHandler.pressEnter();
		ActionHandler.wait(3);
		Reporter.addStepLog("Search for an Opportunity");
		CommonFunctions.attachScreenshot();

		/* Used to select the Opportunity */
		ActionHandler.click(SVO_OpportunityContainer.stageOpportunity);
		ActionHandler.wait(15);
		Reporter.addStepLog("open the relevant Opportunity");
		CommonFunctions.attachScreenshot();
	}

	// open compose email
	@Then("^Click on Compose email$")
	public void click_on_Compose_email() throws Throwable {
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.OppComposeEmail);
		ActionHandler.wait(30);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Compose Email button");
	}

	// attach file
	@Then("^User attaches file from different location$")
	public void user_attaches_file_from_different_location() throws Throwable {
		driver.switchTo().frame(0);
		ActionHandler.wait(3);
		// user clicks on attach file
		ActionHandler.click(SVO_OpportunityContainer.enquiryAttachFile);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Attach File Button");

		// user attaches a file from a location
		String NewWindow = driver.getWindowHandle();
		ActionHandler.wait(30);
		Set<String> attchFileWindow = driver.getWindowHandles();
		Iterator<String> i = attchFileWindow.iterator();
		while (i.hasNext()) {
			String attachWin = i.next();
			if (!NewWindow.equalsIgnoreCase(attachWin)) {
				driver.switchTo().window(attachWin);
				ActionHandler.wait(5);
				AssertHandler.assertElementPresent(SVO_OpportunityContainer.attachFileError);
				ActionHandler.wait(3);
				CommonFunctions.attachScreenshot();
				driver.switchTo().window(NewWindow);
			}
		}
	}

	// edit required information
	@Then("^edit the required information as Additional to \"([^\"]*)\" Subject \"([^\"]*)\" Body \"([^\"]*)\"$")
	public void edit_the_required_information_as_Additional_to_Subject_Body(String Additional, String Subject,
			String Body) throws Throwable {
		String add[] = Additional.split(",");
		Additional = CommonFunctions.readExcelMasterData(add[0], add[1], add[2]);

		String s[] = Subject.split(",");
		Subject = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String b[] = Body.split(",");
		Body = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		System.out.println("All details are fetched from excel");

		ActionHandler.wait(20);

		driver.switchTo().frame(0);

		ActionHandler.pageDown();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// user enters additional to field
		ActionHandler.setText(SVO_OpportunityContainer.Additionalto, Additional);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter Additional email");

		// user enters subject field
		ActionHandler.setText(SVO_OpportunityContainer.Subject, Subject);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter Subject for email");

		ActionHandler.pageDown();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// user enters text in body
		driver.switchTo().frame("thePage:theform:thePB:pbS:pbSI_body:email_body_ifr");
		ActionHandler.setText(SVO_OpportunityContainer.Body, Body);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter Body for email");
		driver.switchTo().parentFrame();
		ActionHandler.wait(3);
	}

	@Then("^User searched and select as \"([^\"]*)\" opportunities from select list view$")
	public void User_searched_select_opportunities_list_view(String opportunity) throws Exception {
		String opp[] = opportunity.split(",");
		opportunity = CommonFunctions.readExcelMasterData(opp[0], opp[1], opp[2]);

		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.SelectListView);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user searches for opportunities list to display");

		ActionHandler.wait(7);
		ActionHandler.setText(SVO_OpportunityContainer.ListViewTextBox1, opportunity);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user set text in filter text box");

		ActionHandler.wait(5);
		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		Reporter.addStepLog("User chooses to view selected type opportunities");
		CommonFunctions.attachScreenshot();
	}

	@Then("^Verify Edit comment button is not available$")
	public void Verify_edit_comment_button_not_available() throws Exception {
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.editBuildStart);
		Reporter.addStepLog("Edit comment button is not available");
	}

	// save email
	@Then("^Click on save mail$")
	public void click_on_save_mail() throws Throwable {

		// user saves the email
		ActionHandler.scrollToView(SVO_OpportunityContainer.saveemail);
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.saveemail);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on save mail");

		// user cancels the email and goes back to opportunity
		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.cancelemail);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on cancel mail");

	}

	// retrieve draft mail
	@Then("^Click on drafts and retrive draft with Subject \"([^\"]*)\"$")
	public void click_on_drafts_and_retrive_draft_with_Subject(String Subject) throws Throwable {
		String s[] = Subject.split(",");
		Subject = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		// user opens drafts
		ActionHandler.wait(10);
		driver.switchTo().frame(0);
		ActionHandler.wait(5);
		ActionHandler.scrollToView(SVO_OpportunityContainer.Drafts);
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.Drafts);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Drafts button");

		// user retrives mail from drafts
		String NewWindow = driver.getWindowHandle();
		ActionHandler.wait(30);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!NewWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);

				ActionHandler.wait(5);
				ActionHandler.click(SVO_OpportunityContainer.RetrieveDrafts);
				CommonFunctions.attachScreenshot();
				Reporter.addStepLog("Click on Retrieve Drafts button");

				ActionHandler.wait(5);
				ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.SelectDrafts(Subject))));
				CommonFunctions.attachScreenshot();
				Reporter.addStepLog("Click on selected Drafts button");

				ActionHandler.wait(20);
				CommonFunctions.attachScreenshot();
				driver.switchTo().window(NewWindow);

				ActionHandler.wait(5);
				driver.switchTo().frame(0);
				ActionHandler.wait(2);
				ActionHandler.pageCompleteScrollDown();
				ActionHandler.wait(3);
			}
		}
	}

	// send email
	@Then("^Click on sent mail$")
	public void click_on_sent_mail() throws Throwable {
		ActionHandler.scrollToView(SVO_OpportunityContainer.sendemail);
		ActionHandler.wait(3);
		// user sends an email
		ActionHandler.click(SVO_OpportunityContainer.sendemail);
		ActionHandler.wait(20);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on send mail");
	}

	// go to s-docs
	@Then("^Click on S-Docs button$")
	public void click_on_SDocs_button() throws Throwable {
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.OppSDocs);
		ActionHandler.wait(20);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on S-Docs button");
	}

	// select file
	@When("^user selects a required file and clicks on next step$")
	public void user_selects_a_required_file_and_clicks_on_next_step() throws Throwable {
		driver.switchTo().frame(0);
		ActionHandler.wait(15);
		ActionHandler.pageDown();
		ActionHandler.wait(5);
		// user selects a required file
		ActionHandler.click(SVO_OpportunityContainer.selectFile);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on a file");
		CommonFunctions.attachScreenshot();

		// user moves to next page
		ActionHandler.click(SVO_OpportunityContainer.Nextstep);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on next step");
		CommonFunctions.attachScreenshot();
		driver.switchTo().parentFrame();

		// user clicks on generate document
		if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.GenDoc)) {
			driver.switchTo().frame(0);
			ActionHandler.wait(15);
			ActionHandler.click(SVO_OpportunityContainer.GenDoc);
			ActionHandler.wait(15);
			Reporter.addStepLog("User clicks the Generate Document");
			CommonFunctions.attachScreenshot();
			driver.switchTo().parentFrame();
		}
	}

	// delete opportunity
	@Then("^Click on Delete button$")
	public void click_on_Delete_button() throws Throwable {
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		// user deletes the opportunity
		ActionHandler.click(SVO_OpportunityContainer.OppDelete);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.OppDeleteConfirm);
		ActionHandler.wait(20);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Delete button");
	}

	// go to orders
	@Then("^Navigate to Orders from Related links$")
	public void navigate_to_orders_from_related_links() throws Throwable {
		ActionHandler.pageDown();
		ActionHandler.pageArrowUp();
		ActionHandler.pageArrowUp();
		ActionHandler.pageArrowUp();
		ActionHandler.pageArrowUp();
		ActionHandler.wait(3);
		// user navigates to orders from quick links
		ActionHandler.click(SVO_OpportunityContainer.OrdersSection);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to the order section");
	}

	// edit fields
	@Then("^edit the Additional to \"([^\"]*)\"$")
	public void edit_the_Additional_to(String Additional) throws Throwable {
		String add[] = Additional.split(",");
		Additional = CommonFunctions.readExcelMasterData(add[0], add[1], add[2]);

		System.out.println("All details are fetched from excel");

		ActionHandler.wait(20);

		driver.switchTo().frame(0);

		ActionHandler.pageDown();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// user enters additional to field
		ActionHandler.setText(SVO_OpportunityContainer.Additionalto, Additional);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter Additional email");
		driver.switchTo().parentFrame();
		ActionHandler.wait(3);
	}

	@Then("^Create New Opportunity with Record Type \"([^\"]*)\"$")
	public void Create_New_Opportunity_with_Record_Type(String recordType) throws Throwable {
		String rType[] = recordType.split(",");
		recordType = CommonFunctions.readExcelMasterData(rType[0], rType[1], rType[2]);

		// click on new button
		ActionHandler.wait(3);
		Reporter.addStepLog("User Creates new Opportunity");
		ActionHandler.click(SVO_OpportunityContainer.newOpportunityBtn);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// user selects the record type
		Reporter.addStepLog("User Needs to select any record type option");
		ActionHandler.wait(3);
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.OpportunityRecordType(recordType))));
		Reporter.addStepLog("User Select required Record Type");
		CommonFunctions.attachScreenshot();

		// click on next button
		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.nextRType);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigate to enter Opportunity details page");
	}

	// save opportunity
	@Then("^Save the created Opportunity$")
	public void save_the_created_Opportunity() throws Throwable {
		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.SaveOpportunity);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves Opportunity");
	}

	// enter details for new opportunity
	@Then("^Enter Required details of New Opportunity like Product Offering \"([^\"]*)\" Source \"([^\"]*)\" Account Name \"([^\"]*)\" Region \"([^\"]*)\" Preferred Contact \"([^\"]*)\" Brand \"([^\"]*)\" and Model \"([^\"]*)\"$")
	public void enter_Required_details_of_New_Opportunity_like_Product_Offering_Source_Account_Name_Region_Preferred_Contact_Brand_and_Model(
			String productOffering, String Source, String AccountName, String region, String preContact, String brand,
			String model) throws Throwable {
		/*
		 * Below code is used to extract data from Excel Master Data and assign it to a
		 * string
		 */
		String pOffering[] = productOffering.split(",");
		productOffering = CommonFunctions.readExcelMasterData(pOffering[0], pOffering[1], pOffering[2]);

		String oSource[] = Source.split(",");
		Source = CommonFunctions.readExcelMasterData(oSource[0], oSource[1], oSource[2]);

		String oAccountName[] = AccountName.split(",");
		AccountName = CommonFunctions.readExcelMasterData(oAccountName[0], oAccountName[1], oAccountName[2]);

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String pc[] = preContact.split(",");
		preContact = CommonFunctions.readExcelMasterData(pc[0], pc[1], pc[2]);

		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String mo[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(mo[0], mo[1], mo[2]);

		/* Below strings contains information required to fill the New Opportunity */
		double randomNumber = getRandomIntegerBetweenRange(0, 1000);

		Reporter.addStepLog("Start entering mandatory details");
		ActionHandler.wait(3);

		// Below Code is updated the Opportunity Name on New Opportunity Details page.
		ActionHandler.wait(7);
		String OpportunityName = "Test_Opportunity_";
		System.out.println("User has clicked on Classic Reborn");
		ActionHandler.setText(SVO_OpportunityContainer.OpportunityName, OpportunityName + randomNumber);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// Below Code Click on Close date and Select the date
		String CDate = "12/02/2022";
		ActionHandler.setText(SVO_OpportunityContainer.OpportunityCloseDate, CDate);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// Below Code Click on Stage and Select the Qualified value from the drop down
		String Stage = "Qualified";
		ActionHandler.click(SVO_OpportunityContainer.OpportunityStage);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.SourceSelection(Stage))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// Below Code Click on ViewallDependencies and select the region value from drop
		// down
		ActionHandler.click(SVO_OpportunityContainer.Opportunityeditregion);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.ValueSelection(region))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// Below Code search for Account name and then select value for it
		ActionHandler.pageScrollDown();
		ActionHandler.setText(SVO_OpportunityContainer.OpportunityAccountName, AccountName);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// Below Code search for prferredContact details then select value for preferred
		// contact
		ActionHandler.setText(SVO_OpportunityContainer.preferredCnt, preContact);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		act1.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// user selects restricted party screening
		ActionHandler.click(SVO_OpportunityContainer.restrictedPartyScreeningDropdown);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on the dropdown.");
		ActionHandler.click(SVO_OpportunityContainer.checkCleared);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User select the value.");

		// Below Code set the request details Information in Request details field
		String requestDetails = "Create New Opportunity for Testing Perspective";
		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);
		ActionHandler.setText(SVO_OpportunityContainer.requestDetails, requestDetails);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// Below Code search for brand name then select brand value
		ActionHandler.setText(SVO_OpportunityContainer.brand, brand);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Actions act2 = new Actions(driver);
		act2.sendKeys(Keys.ARROW_DOWN).build().perform();
		act2.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// Below Code search for Model name then select brand value
		ActionHandler.setText(SVO_OpportunityContainer.model, model);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Actions act3 = new Actions(driver);
		act3.sendKeys(Keys.ARROW_DOWN).build().perform();
		act3.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// user selects body style field
		String Style = "test";
		ActionHandler.setText(SVO_OpportunityContainer.bodyStyle, Style);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Actions act4 = new Actions(driver);
		act4.sendKeys(Keys.ARROW_DOWN).build().perform();
		act4.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// user selects production year
		String year = "2000";
		ActionHandler.setText(SVO_OpportunityContainer.prodYear, year);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// user selects exterior colour
		ActionHandler.click(SVO_OpportunityContainer.ExtColour);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.colourBlue);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// user selects the vehicle
		String vehicle = "Discovery";
		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);
		ActionHandler.setText(SVO_OpportunityContainer.vehicle, vehicle);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Actions act5 = new Actions(driver);
		act5.sendKeys(Keys.ARROW_DOWN).build().perform();
		act5.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// user selects the hand of drive
		ActionHandler.click(SVO_OpportunityContainer.HandOfDrive);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.DriveLHD);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// user selects the speedometer
		ActionHandler.click(SVO_OpportunityContainer.Speedometer);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.SpeedMPH);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// user selects the colour details
		String colour = "Black";
		ActionHandler.setText(SVO_OpportunityContainer.buildColour, colour);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(SVO_OpportunityContainer.ColourShade, colour);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(SVO_OpportunityContainer.interiorColour, colour);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(SVO_OpportunityContainer.specDetails, colour);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// user enters the price details
		String price = "100";
		ActionHandler.setText(SVO_OpportunityContainer.retailPrice, price);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// user enters the VAT details
		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.VAT);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.VATYes);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
	}

	// enter details for new SVO Bespoke Opportunity
	@Then("^Enter details of New SVO Bespoke Opportunity like Opportunity name \"([^\"]*)\" Product Offering \"([^\"]*)\" Account Name \"([^\"]*)\" Region \"([^\"]*)\" Preferred Contact \"([^\"]*)\" Brand \"([^\"]*)\" and Model \"([^\"]*)\"$")
	public void enter_details_of_New_SVO_Bespoke_Opportunity_like_Opportunity_name_Product_Offering_Account_Name_Region_Preferred_Contact_Brand_and_Model(
			String OppName, String productOffering, String AccountName, String region, String preContact, String brand,
			String model) throws Throwable {
		/*
		 * Below code is used to extract data from Excel Master Data and assign it to a
		 * string
		 */
		String Oname[] = OppName.split(",");
		OppName = CommonFunctions.readExcelMasterData(Oname[0], Oname[1], Oname[2]);

		String pOffering[] = productOffering.split(",");
		productOffering = CommonFunctions.readExcelMasterData(pOffering[0], pOffering[1], pOffering[2]);

		String oAccountName[] = AccountName.split(",");
		AccountName = CommonFunctions.readExcelMasterData(oAccountName[0], oAccountName[1], oAccountName[2]);

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String pc[] = preContact.split(",");
		preContact = CommonFunctions.readExcelMasterData(pc[0], pc[1], pc[2]);

		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String mo[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(mo[0], mo[1], mo[2]);

		/* Below strings contains information required to fill the New Opportunity */
		double randomNumber = getRandomIntegerBetweenRange(0, 1000);
		String Stage = "Qualified";

		Reporter.addStepLog("Start entering mandatory details");
		ActionHandler.wait(5);

		// Below Code is updated the Opportunity Name on New Opportunity Details page.
		ActionHandler.wait(7);
		System.out.println("User has clicked on SVO Bespoke");
		ActionHandler.setText(SVO_OpportunityContainer.OpportunityName, OppName + randomNumber);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// Below Code Click on Close date and Select the date
		String CDate = "12/02/2022";
		ActionHandler.setText(SVO_OpportunityContainer.OpportunityCloseDate, CDate);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// Below Code Click on Stage and Select the Qualified value from the drop down
		ActionHandler.click(SVO_OpportunityContainer.SVOOpportunityStage);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.SourceSelection(Stage))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// Below Code is to select the product offering value from drop down
		ActionHandler.click(SVO_OpportunityContainer.SVOprodOffering);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.ValueSelection(productOffering))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// Below Code is to select the region value from drop down
		ActionHandler.click(SVO_OpportunityContainer.SVOOpportunityeditregion);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.ValueSelection(region))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// Below Code search for prferredContact details then select value for preferred
		// contact
		ActionHandler.setText(SVO_OpportunityContainer.preferredCnt, preContact);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		act1.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// Below Code search for Account name and then select value for it
		ActionHandler.pageScrollDown();
		ActionHandler.setText(SVO_OpportunityContainer.SVOAccountName, AccountName);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// Below Code search for brand name then select brand value
		ActionHandler.setText(SVO_OpportunityContainer.brand, brand);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Actions act2 = new Actions(driver);
		act2.sendKeys(Keys.ARROW_DOWN).build().perform();
		act2.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// Below Code search for Model name then select brand value
		ActionHandler.setText(SVO_OpportunityContainer.model, model);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Actions act3 = new Actions(driver);
		act3.sendKeys(Keys.ARROW_DOWN).build().perform();
		act3.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
	}

	// open opportunity
	@And("^Open an opportunity with Record Type \"([^\"]*)\"$")
	public void open_an_opportunity_with_Record_Type(String SearchText) throws Throwable {

		String CLE[] = SearchText.split(",");
		SearchText = CommonFunctions.readExcelMasterData(CLE[0], CLE[1], CLE[2]);
		Reporter.addStepLog("Read the record type from master data");

		ActionHandler.wait(3);
		AllOpportunity();
		Reporter.addStepLog("User opens All Oppotunity");

		// user search for Classic Limited Edition Opportunity//
		ActionHandler.wait(5);
		ActionHandler.setText(SVO_OpportunityContainer.OpportSearchBox, SearchText);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.pressEnter();
		ActionHandler.wait(8);

		// user click on first classic limited edition opportunity//
		ActionHandler.click(SVO_OpportunityContainer.FirstOpportunity);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects first opportunity for applied filetr");
	}

	// verify build capacity
	@Then("^Verify the Build Capacity of the Programme$")
	public void verify_the_Build_Capacity_of_the_Programme() throws Throwable {
		ActionHandler.wait(5);
		driver.navigate().refresh();
		ActionHandler.wait(15);
		ActionHandler.click(SVO_OpportunityContainer.Programme);
		ActionHandler.wait(8);
		if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.BuildCapacity)) {
			Reporter.addStepLog("Build capacity details were found");
			CommonFunctions.attachScreenshot();
		} else {
			Reporter.addStepLog("Build capacity details were not found");
			CommonFunctions.attachScreenshot();
		}
	}

	// SVO portal logout
	@Then("^Logout from SF_SVO Portal$")
	public void logout_from_SF_SVO_Portal() throws Throwable {
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.icon_image);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Logout");
		ActionHandler.click(SVO_OpportunityContainer.Logout);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.userNameTextBox)) {
			MarkupHelper.createLabel("Logged out successfully", ExtentColor.GREEN);
		} else {
			MarkupHelper.createLabel("Log out failed", ExtentColor.RED);
		}

		driver.close();
		Reporter.addStepLog("Driver closed");
	}

	// enter required fields for opportunity
	@Then("^User enters fields like OpportunityName \"([^\"]*)\" CloseDate \"([^\"]*)\" Stage \"([^\"]*)\" Region \"([^\"]*)\" AccountName \"([^\"]*)\" PreferredContact \"([^\"]*)\" Source \"([^\"]*)\"$")
	public void user_enters_fields_like_OpportunityName_CloseDate_Stage_Region_AccountName_PreferredContact_Source(

			String OpportunityName, String CloseDate, String Stage, String Region, String AccountName,
			String PreferredContact, String Source) throws Throwable {

		String OppName[] = OpportunityName.split(",");
		OpportunityName = CommonFunctions.readExcelMasterData(OppName[0], OppName[1], OppName[2]);

		String Stg[] = Stage.split(",");
		Stage = CommonFunctions.readExcelMasterData(Stg[0], Stg[1], Stg[2]);

		String Closedate[] = CloseDate.split(",");
		CloseDate = CommonFunctions.readExcelMasterData(Closedate[0], Closedate[1], Closedate[2]);

		String r[] = Region.split(",");
		Region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String s[] = Source.split(",");
		Source = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String AccName[] = AccountName.split(",");
		AccountName = CommonFunctions.readExcelMasterData(AccName[0], AccName[1], AccName[2]);

		String PrefCont[] = PreferredContact.split(",");
		PreferredContact = CommonFunctions.readExcelMasterData(PrefCont[0], PrefCont[1], PrefCont[2]);

		double randomNumber = getRandomIntegerBetweenRange(0, 1000);

		// user enters opportunity name //
		ActionHandler.wait(5);
		ActionHandler.setText(SVO_OpportunityContainer.OppName, OpportunityName + randomNumber);
		Reporter.addStepLog("User enters an Opportunity name");
		CommonFunctions.attachScreenshot();

		// User enters close date//
		ActionHandler.wait(5);
		ActionHandler.setText(SVO_OpportunityContainer.CloseDate, CloseDate);
		Reporter.addStepLog("User select the close date");
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// User selects the stage//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.StageDW);
		ActionHandler.wait(5);
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.ValueSelection(Stage))));
		Reporter.addStepLog("User select the stage");
		CommonFunctions.attachScreenshot();

		// User selects Region,area and market//
		ActionHandler.click(SVO_OpportunityContainer.RegionDW);
		ActionHandler.wait(5);
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.ValueSelection(Region))));
		Reporter.addStepLog("User select the Region,area and market dropdown");
		CommonFunctions.attachScreenshot();

		// User selects Account name//
		ActionHandler.scrollToView(SVO_OpportunityContainer.Client);
		ActionHandler.wait(5);
		ActionHandler.wait(5);
		ActionHandler.setText(SVO_OpportunityContainer.AccountName, AccountName);
		ActionHandler.wait(5);
		ActionHandler.clearAndSetText(SVO_OpportunityContainer.AccountName, AccountName);
		ActionHandler.wait(5);
		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);
		Reporter.addStepLog("User enters Account Name");
		CommonFunctions.attachScreenshot();

		// User selects preferred contact name//
		ActionHandler.wait(5);
		ActionHandler.setText(SVO_OpportunityContainer.PrefContact, PreferredContact);
		ActionHandler.wait(3);
		ActionHandler.clearAndSetText(SVO_OpportunityContainer.PrefContact, PreferredContact);
		ActionHandler.wait(5);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Preferred Contact");

		// User selects Source//
		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.SourceDW);
		ActionHandler.wait(5);
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.ValueSelection(Source))));
		Reporter.addStepLog(" User selects Source");
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.SaveOpportunity);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
	}

	// error message to fill the Programme
	@Then("^Verify the error message to fill the Programme$")
	public void verify_the_error_message_to_fill_the_Programme() throws Throwable {
		ActionHandler.wait(4);
		if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.ProgrammeError)) {
			Reporter.addStepLog("Error message validated");
			CommonFunctions.attachScreenshot();
		} else {
			Reporter.addStepLog("No error message was caught");
			CommonFunctions.attachScreenshot();
		}
		ActionHandler.wait(4);
		ActionHandler.click(SVO_OpportunityContainer.Cancel);
		ActionHandler.wait(4);
	}

	// create new opportunity
	@Then("^User Create a New Opportunity with Record Type \"([^\"]*)\"$")
	public void user_Create_a_New_Opportunity_with_Record_Type(String recordType) throws Throwable {
		String rType[] = recordType.split(",");
		recordType = CommonFunctions.readExcelMasterData(rType[0], rType[1], rType[2]);

		ActionHandler.wait(4);
		Reporter.addStepLog("User Creates new Opportunity");
		ActionHandler.click(SVO_OpportunityContainer.newOpportunityBtn);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User Needs to select any record type option");
		ActionHandler.wait(3);
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.OpportunityRecordType(recordType))));
		Reporter.addStepLog("User Select required Record Type");
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(4);
		ActionHandler.click(SVO_OpportunityContainer.nextRType);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigate to enter Opportunity details page");
	}

	// validate s-docs button
	@Then("^Validate that user should not be able to see Send Sales Agreement and Send POF button$")
	public void validate_that_user_should_not_be_able_to_see_send_sales_Send_POF_button() throws Throwable {
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User should not be able to see Send Sales Agreement and Send POF button");
	}

	// open SVO Bespoke opportunity
	@Given("^Open an opportunity with SVO Bespoke Record Type$")
	public void open_an_opportunity_with_SVO_Bespoke_Record_Type() throws Throwable {
		ActionHandler.wait(5);
		String Opportunity = "SVO Bespoke";
		ActionHandler.clearAndSetText(SVO_OpportunityContainer.OpportSearchBox, Opportunity);
		ActionHandler.wait(3);
		ActionHandler.pressEnter();
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.FirstOpportunity);
		Reporter.addStepLog("User selected SVO Bespoke Opportunity");
		CommonFunctions.attachScreenshot();
	}

	// remove pricing details
	@Then("^User remove the Pricing details \"([^\"]*)\"$")
	public void user_remove_the_Pricing_details(String Price) throws Throwable {

		String P[] = Price.split(",");
		Price = CommonFunctions.readExcelMasterData(P[0], P[1], P[2]);

		ActionHandler.wait(5);
		driver.navigate().refresh();
		ActionHandler.wait(15);

		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();

		// User click on Edit cost price pencil icon
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.EditCostPrice);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Edit cost price pencil icon");

		// user take the data from costprice, list price, contribution and revenue text
		// box//
		String CP = ActionHandler.getElementText(SVO_OpportunityContainer.CostPriceTextBox);
		ActionHandler.wait(5);

		String LP = ActionHandler.getElementText(SVO_OpportunityContainer.ListPriceTextBox);
		ActionHandler.wait(5);

		String RP = ActionHandler.getElementText(SVO_OpportunityContainer.RetailPriceTextBox);
		ActionHandler.wait(5);

		String Cont = ActionHandler.getElementText(SVO_OpportunityContainer.ContributionTextBox);
		ActionHandler.wait(5);

		String Rev = ActionHandler.getElementText(SVO_OpportunityContainer.RevenueTextBox);
		ActionHandler.wait(5);

		// compares with null value to make sure pricing details were empty
		if (CP.isEmpty()) {
			if (LP.isEmpty()) {
				if (RP.isEmpty()) {
					if (Cont.isEmpty()) {
						if (Rev.isEmpty()) {
							Reporter.addStepLog("Price details were not entered");
							CommonFunctions.attachScreenshot();
						}
					}
				}
			}

		}

		// user clicks on save button//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.SaveButton);
		Reporter.addStepLog("user clicks on save button");
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
	}

	// enter production year
	@Then("^User enters production year of Vehicle \"([^\"]*)\"$")
	public void user_enters_production_year_of_Vehicle(String Year) throws Throwable {

		String oSource[] = Year.split(",");
		Year = CommonFunctions.readExcelMasterData(oSource[0], oSource[1], oSource[2]);

		ActionHandler.wait(5);
		driver.navigate().refresh();
		ActionHandler.wait(15);

		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();

		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.EditProductionYear);
		Reporter.addStepLog("User click on Edit Production Year pencil icon");
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);

		ActionHandler.clearAndSetText(SVO_OpportunityContainer.ProductionYearTextBox, Year);
		Reporter.addStepLog("User enters production year");
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);

		ActionHandler.click(SVO_OpportunityContainer.SaveButton);
		Reporter.addStepLog("User click on save button");
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User entered production year of Vehicle and saved it");
		CommonFunctions.attachScreenshot();
	}

	// uncheck the Restricted party screening
	@Then("^User unchecked the Restricted party screening \"([^\"]*)\"$")
	public void user_unchecked_the_Restricted_party_screening(String RPS) throws Throwable {

		String R[] = RPS.split(",");
		RPS = CommonFunctions.readExcelMasterData(R[0], R[1], R[2]);

		ActionHandler.wait(5);
		driver.navigate().refresh();
		ActionHandler.wait(15);

		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		// user clicks on edit Restricted part screening pencil icon//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.EditRestrictedPS);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.RestrictedDropDown);
		ActionHandler.wait(5);
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.ValueSelection(RPS))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(4);

		// user click on save button//
		ActionHandler.click(SVO_OpportunityContainer.SaveOpportunity);
		ActionHandler.wait(5);
		ActionHandler.pageCompleteScrollUp();
		Reporter.addStepLog("Uncheck the Restricted Party Screening");
		CommonFunctions.attachScreenshot();
	}

	// Select opportunity
	@Then("^User searched and select \"([^\"]*)\" opportunities from select list view$")
	public void user_searched_and_select_opportunities_from_select_list_view(String arg1) throws Throwable {

		String s[] = arg1.split(",");
		arg1 = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		// user searches for opportunities list to display//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.SelectListView);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user searches for opportunities list to display");

		// user set text in filter text box//
		ActionHandler.wait(7);
		ActionHandler.setText(SVO_OpportunityContainer.ListViewTextBox, arg1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user set text in filter text box");

		ActionHandler.wait(5);
		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		Reporter.addStepLog("User chooses to view selected type opportunities");
		CommonFunctions.attachScreenshot();
	}

	// clear SAP Account details
	@Then("^User clears SAP Account details$")
	public void user_clears_SAP_Account_details() throws Throwable {
		ActionHandler.wait(5);
		driver.navigate().refresh();
		ActionHandler.wait(15);

		for (int i = 0; i <= 6; i++) {
			ActionHandler.scrollDown();
		}

		// User click on Edit SAP Account pencil icon
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.EditSAPaccount);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Edit SAP Account pencil icon");
		ActionHandler.wait(5);

		if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.ClearSAP)) {
			ActionHandler.wait(3);
			ActionHandler.click(SVO_OpportunityContainer.ClearSAP);
			Reporter.addStepLog("user click on cross button of sap Account");
			CommonFunctions.attachScreenshot();
		}

		// user click on save button//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.SaveOpportunity);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.pageCompleteScrollUp();

		Reporter.addStepLog("User cleared SAP Account details");
		CommonFunctions.attachScreenshot();

	}

	@Then("^User clears opportunity currency$")
	public void user_clears_opportunity_currency() throws Throwable {
		ActionHandler.wait(5);
		driver.navigate().refresh();
		ActionHandler.wait(15);

		javaScriptUtil.scrollIntoView(SVO_OpportunityContainer.PricingDetails);

		// user click on edit opportunity currency pencil icon//
		ActionHandler.wait(8);
		ActionHandler.click(SVO_OpportunityContainer.EditOpportunityCurrency);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on edit opportunity currency pencil icon");

		// user clears currency details//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.OpportunityCurrency);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user clears currency details");

		// user click on none option
		ActionHandler.wait(5);
		javaScriptUtil.clickElementByJS(SVO_OpportunityContainer.NoneOption);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on none option");

		// user click on save button//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.SaveOpportunity);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on save button to save Opportunity currency");
		CommonFunctions.attachScreenshot();

	}

	@Then("^User click on cancel button$")
	public void user_click_on_cancel_button() throws Throwable {

		// user click on cancel button
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.cancelVehicle);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on cancel button");
		CommonFunctions.attachScreenshot();
	}

	// clear VAT Qualifying of Pricing Details
	@Then("^User clears VAT Qualifying of Pricing Details$")
	public void user_clears_VAT_Qualifying_of_Pricing_Details() throws Throwable {

		ActionHandler.wait(5);
		driver.navigate().refresh();
		ActionHandler.wait(15);
		javaScriptUtil.scrollIntoView(SVO_OpportunityContainer.PricingDetails);
		ActionHandler.wait(5);

		// user clears VAT details//
		ActionHandler.click(SVO_OpportunityContainer.EditVAT);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.VATdropdown);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.NoneOption);
		CommonFunctions.attachScreenshot();

		// user click on save button//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.SaveOpportunity);
		ActionHandler.wait(5);
		Reporter.addStepLog("user click on save button to save VAT Qualifying");
		CommonFunctions.attachScreenshot();
	}

	@Then("^User clears Restricted party screening \"([^\"]*)\"$")
	public void user_clears_Restricted_party_screening(String RPS) throws Throwable {

		String R[] = RPS.split(",");
		RPS = CommonFunctions.readExcelMasterData(R[0], R[1], R[2]);

		ActionHandler.wait(5);
		driver.navigate().refresh();
		ActionHandler.wait(15);
		// javaScriptUtil.scrollIntoView(SVO_OpportunityContainer.ClientDetail);
		// ActionHandler.wait(5);

		for (int i = 0; i <= 7; i++) {
			ActionHandler.scrollDown();
		}
		// user clears Restricted party screening//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.EditRPS);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.RPSdropdown);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.ValueSelection(RPS))));
		CommonFunctions.attachScreenshot();

		// user click on save button//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.SaveOpportunity);
		ActionHandler.wait(5);
		Reporter.addStepLog("user click on save button to save VAT Qualifying");
		CommonFunctions.attachScreenshot();
	}

	// attach a file in Compose email
	@Then("^User attach a file in Compose email section$")
	public void user_attach_a_file_in_Compose_email_section() throws Throwable {

		// user click on attach file button//
		ActionHandler.wait(5);
		driver.switchTo().frame(0);
		ActionHandler.wait(6);
		ActionHandler.click(SVO_OpportunityContainer.AttachFile);
		ActionHandler.wait(10);

		String NewWindow = driver.getWindowHandle();
		ActionHandler.wait(30);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String SetUpWindow = i.next();
			if (!NewWindow.equalsIgnoreCase(SetUpWindow)) {
				driver.switchTo().window(SetUpWindow);
				ActionHandler.wait(6);
				ActionHandler.click(SVO_OpportunityContainer.DocumentListbox);
				ActionHandler.wait(6);
				CommonFunctions.attachScreenshot();
				Reporter.addStepLog("User click on document list box");

				ActionHandler.click(SVO_OpportunityContainer.ROE);
				Reporter.addStepLog("User selects documents from list box");
				CommonFunctions.attachScreenshot();
				ActionHandler.wait(5);

				// user selects first doc
				ActionHandler.wait(5);
				ActionHandler.click(SVO_OpportunityContainer.FirstDocCheckbox);
				ActionHandler.wait(5);
				Reporter.addStepLog("user selects first doc");
				CommonFunctions.attachScreenshot();

				// user attach the document//
				ActionHandler.click(SVO_OpportunityContainer.AttachBtn);
				Reporter.addStepLog("User attach the document to an email");
				CommonFunctions.attachScreenshot();
				ActionHandler.wait(5);
				ActionHandler.click(SVO_OpportunityContainer.ClickToCompleteBtn);
				ActionHandler.wait(5);

				driver.switchTo().window(NewWindow);

			}

		}

		/// user save the email//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.saveemail);
		Reporter.addStepLog("User saves the mail");
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);

		// click on cancel email
		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.cancelemail);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on cancel mail");

	}

	// click on the mark stage as complete button
	@And("^User click on the mark stage as complete button to complete the process$")
	public void user_click_on_the_mark_stage_as_complete_button_to_complete_the_process() throws Throwable {
		ActionHandler.wait(3);
		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(5);
		// driver.navigate().refresh();
		ActionHandler.wait(4);
		ActionHandler.click(SVO_OpportunityContainer.MarkStageAsComplete);
		Reporter.addStepLog("User clicks on Mark stage as complete button");
		CommonFunctions.attachScreenshot();
	}

	@Then("^Verify that user can move to \"([^\"]*)\" stage$")
	public void Verify_User_can_moved_to_Contract_Signed_Stage(String stage) throws Exception {
		ActionHandler.wait(4);
		VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.ContractSigned);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Opportunity stage moves to Contract Signed");
	}

	// select TO BCC and CC section
	@Then("^User selects TO BCC and CC section of an email$")
	public void user_selects_TO_BCC_and_CC_section_of_an_email() throws Throwable {

		ActionHandler.wait(5);
		driver.navigate().refresh();
		ActionHandler.wait(15);

		// User click on search button
		driver.switchTo().frame(0);
		ActionHandler.click(SVO_OpportunityContainer.SearchTO);
		ActionHandler.wait(5);
		Reporter.addStepLog("User click on search button");
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		String NewWindow = driver.getWindowHandle();
		ActionHandler.wait(10);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String SetUpWindow = i.next();
			if (!NewWindow.equalsIgnoreCase(SetUpWindow)) {
				driver.switchTo().window(SetUpWindow);

				driver.manage().window().maximize();
				ActionHandler.wait(5);

				ActionHandler.click(SVO_OpportunityContainer.FirstCheckbox);
				ActionHandler.wait(3);
				ActionHandler.click(SVO_OpportunityContainer.ToArrowBtn);
				Reporter.addStepLog("User selects To section of an email");
				CommonFunctions.attachScreenshot();

				// User selects CC section of an email//
				ActionHandler.wait(5);
				ActionHandler.click(SVO_OpportunityContainer.FirstCheckbox);
				ActionHandler.wait(3);
				ActionHandler.click(SVO_OpportunityContainer.CCArrowBtn);
				Reporter.addStepLog("User selects CC section of an email");
				CommonFunctions.attachScreenshot();

				// User selects BCC section of an email//
				ActionHandler.wait(5);
				ActionHandler.click(SVO_OpportunityContainer.FirstCheckbox);
				ActionHandler.wait(3);
				ActionHandler.click(SVO_OpportunityContainer.BCCArrowBtn);
				Reporter.addStepLog("User selects BCC section of an email");
				CommonFunctions.attachScreenshot();

				// User click on Ok button
				ActionHandler.wait(5);
				ActionHandler.click(SVO_OpportunityContainer.OkBtn);
				CommonFunctions.attachScreenshot();
				ActionHandler.wait(5);

				driver.switchTo().window(NewWindow);

			}

		}

		// user save the email//
		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(5);
		driver.switchTo().frame(0);

		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.saveemail);
		Reporter.addStepLog("User saves the mail");
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);

		// click on cancel email
		ActionHandler.wait(3);
		javaScriptUtil.clickElementByJS(SVO_OpportunityContainer.cancelemail);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on cancel mail");

	}

	// select an order
	@Then("^User selects an order which is linked to selected Opportunity$")
	public void user_selects_an_order_which_is_linked_to_selected_Opportunity() throws Throwable {

		ActionHandler.wait(5);
		driver.navigate().refresh();
		ActionHandler.wait(15);

		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.wait(5);

		ActionHandler.click(SVO_OpportunityContainer.OrderLink);
		ActionHandler.wait(5);
		Reporter.addStepLog("User click on order link");
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(5);
		driver.navigate().refresh();
		ActionHandler.wait(15);

		ActionHandler.click(SVO_OpportunityContainer.FirstOpportunity);
		ActionHandler.wait(5);
		Reporter.addStepLog("User selects first order");
		Reporter.addStepLog("User selects an order which is linked to selected Opportunity");
		CommonFunctions.attachScreenshot();

	}

	// User navigate to invoice section
	@Then("^User navigate to invoice section$")
	public void user_navigate_to_invoice_section() throws Throwable {

		ActionHandler.wait(5);
		driver.navigate().refresh();
		ActionHandler.wait(15);

		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.wait(5);

		// User click on Invoice link
		ActionHandler.click(SVO_OpportunityContainer.InvoiceLink);
		ActionHandler.wait(5);
		Reporter.addStepLog("User Navigate to Invoice section");
		CommonFunctions.attachScreenshot();

	}

	// select deposit type invoice
	@Then("^User selects deposit type invoice$")
	public void user_selects_deposit_type_invoice() throws Throwable {

		ActionHandler.wait(5);
		driver.navigate().refresh();
		ActionHandler.wait(5);

		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.invoiceMenu);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		// User click on New Button to create an invoice
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.InvoiceNewBtn);
		Reporter.addStepLog("User click on new invoice button");
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User click on New Button to create an invoice");
		CommonFunctions.attachScreenshot();

		// user selects deposit invoice type//
		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.DepositInvoiceType);
		CommonFunctions.attachScreenshot();

		// user click on next button//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.nextRType);
		Reporter.addStepLog("User selects deposit type of an invoice");
		CommonFunctions.attachScreenshot();
	}

	// create new invoice
	@Then("^User creates new invoice by entering mandatory fields like Account \"([^\"]*)\" and SapAccount \"([^\"]*)\"$")
	public void user_creates_new_invoice_by_entering_mandatory_fields_like_Account_and_SapAccount(String Account,
			String SapAccount) throws Throwable {

		String Ac[] = Account.split(",");
		Account = CommonFunctions.readExcelMasterData(Ac[0], Ac[1], Ac[2]);

		String sap[] = SapAccount.split(",");
		SapAccount = CommonFunctions.readExcelMasterData(sap[0], sap[1], sap[2]);

		// user selects Account name//
		ActionHandler.wait(5);
		ActionHandler.setText(SVO_OpportunityContainer.AccountTxtBx, Account);
		ActionHandler.wait(3);
		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		Reporter.addStepLog("User selects Account name from search list");
		CommonFunctions.attachScreenshot();

		// user selects sap account//
		ActionHandler.wait(5);
		ActionHandler.setText(SVO_OpportunityContainer.SAPtxtBx, SapAccount);
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.NewSAPAcnt);

		ActionHandler.wait(5);
		String SapId = "test_";
		double randomNumber = getRandomIntegerBetweenRange(0, 1000);
		ActionHandler.setText(SVO_OpportunityContainer.SAPid, SapId + randomNumber);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);

		// user selects Account name//
		ActionHandler.wait(5);
		ActionHandler.setText(SVO_OpportunityContainer.AccountTxtBx, Account);
		ActionHandler.wait(6);
		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		act1.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		Reporter.addStepLog("User selects Account name from search list");
		CommonFunctions.attachScreenshot();

		// user click on save button of sap//
		ActionHandler.click(SVO_OpportunityContainer.SaveRelation);
		ActionHandler.wait(5);
		Reporter.addStepLog(" user click on save button of sap");
		CommonFunctions.attachScreenshot();

		// user save the invoice//
		ActionHandler.click(SVO_OpportunityContainer.SaveButton);
		ActionHandler.wait(5);
		Reporter.addStepLog("User save the invoice");
		CommonFunctions.attachScreenshot();
	}

	// click on the mark status as complete button of an Order
	@Then("^User click on the mark status as complete button of an Order$")
	public void user_click_on_the_mark_status_as_complete_button_of_an_Order() throws Throwable {
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.OutcomeTab);
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.MarkAsCurrentStatus);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Mark as current status button");

		// user selects retailed from outcome drop down list//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.OutcomeList);
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.RetailedOutcome);
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.DoneBtn);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Retailed Outcome");
	}

	// fill Payment fields
	@Then("^User fills Payment fileds like SendByDate \"([^\"]*)\" PaymentMadeDate \"([^\"]*)\" SentDate \"([^\"]*)\"$")
	public void user_fills_Payment_fileds_like_SendByDate_PaymentMadeDate_SentDate(String SendByDate,
			String PaymentMadeDate, String SentDate) throws Throwable {

		String sbd[] = SendByDate.split(",");
		SendByDate = CommonFunctions.readExcelMasterData(sbd[0], sbd[1], sbd[2]);

		String pd[] = PaymentMadeDate.split(",");
		PaymentMadeDate = CommonFunctions.readExcelMasterData(pd[0], pd[1], pd[2]);

		String Sd[] = SentDate.split(",");
		SentDate = CommonFunctions.readExcelMasterData(Sd[0], Sd[1], Sd[2]);
		Reporter.addStepLog("Data retrieved from master data");

		// user click on edit payment pencil icon//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.EditPaymentValue);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// user set payment details//
		ActionHandler.clearAndSetText(SVO_OpportunityContainer.SendByDate, SendByDate);
		ActionHandler.wait(5);
		Reporter.addStepLog("User selects Retailed Outcome");

		ActionHandler.clearAndSetText(SVO_OpportunityContainer.PaymentMadeDate, PaymentMadeDate);
		ActionHandler.wait(5);
		Reporter.addStepLog("User selects Retailed Outcome");

		ActionHandler.clearAndSetText(SVO_OpportunityContainer.SentDate, SentDate);
		ActionHandler.wait(5);
		Reporter.addStepLog("User selects Retailed Outcome");

		// user click on save button//
		ActionHandler.click(SVO_OpportunityContainer.SaveOpportunity);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Retailed Outcome");

	}

	// Verify the payment due date
	@Then("^Verify the payment due date of an Invoice$")
	public void verify_the_payment_due_date_of_an_Invoice() throws Throwable {

		ActionHandler.wait(5);
		if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.PaymentDueDate)) {
			Reporter.addStepLog("payment due date is auto populated");
			CommonFunctions.attachScreenshot();
		} else {
			Reporter.addStepLog("Payment due date not found");
			CommonFunctions.attachScreenshot();
		}

	}

	// edit the Close date of an Opportunit
	@Then("^User edits the Close date of an Opportunity as \"([^\"]*)\"$")
	public void user_edits_the_Close_date_of_an_Opportunity_as(String CloseDate) throws Throwable {

		String CD[] = CloseDate.split(",");
		CloseDate = CommonFunctions.readExcelMasterData(CD[0], CD[1], CD[2]);

		// User click on edit close date pencil icon//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.EditCloseDate);
		CommonFunctions.attachScreenshot();

		// user change the close date//
		ActionHandler.wait(5);
		ActionHandler.clearAndSetText(SVO_OpportunityContainer.CloseDateTextBox, CloseDate);
		CommonFunctions.attachScreenshot();

		// User save the date//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.SaveOpportunity);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User edits the Close date of an Opportunity");
	}

	// enter pinewood job card details
	@Then("^User enters pinewood job card details like PineWoodJobCard \"([^\"]*)\" BuildNumber \"([^\"]*)\"$")
	public void user_enters_pinewood_job_card_details_like_PineWoodJobCard_BuildNumber(String PineWoodJobCard,
			String BuildNumber) throws Throwable {

		String PWJC[] = PineWoodJobCard.split(",");
		PineWoodJobCard = CommonFunctions.readExcelMasterData(PWJC[0], PWJC[1], PWJC[2]);

		String BN[] = BuildNumber.split(",");
		BuildNumber = CommonFunctions.readExcelMasterData(BN[0], BN[1], BN[2]);

		// user click on edit pencil icon of pine wood job card//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.EditPineWoodJobCard);
		CommonFunctions.attachScreenshot();

		// set text for Pine wood job card //
		ActionHandler.wait(5);
		ActionHandler.setText(SVO_OpportunityContainer.PineWoodJobCardTxtBox, PineWoodJobCard);
		CommonFunctions.attachScreenshot();

		// enters build number//
		ActionHandler.wait(5);
		ActionHandler.setText(SVO_OpportunityContainer.BuildNumberTxtBox, BuildNumber);
		CommonFunctions.attachScreenshot();

		// click on save button//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.SaveRPS);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters pinewood job card number");

	}

	// Navigate to Quote section
	@Then("^Navigate to Quote section$")
	public void navigate_to_Quote_section() throws Throwable {
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.QuoteLink);
		ActionHandler.wait(5);
		Reporter.addScenarioLog("User navigate to Quote section");
		CommonFunctions.attachScreenshot();
	}

	// Change the opportunity status
	@Then("^Change the opportunity status to Order Placed$")
	public void change_the_opportunity_status_to_order_placed() throws Throwable {
		ActionHandler.wait(10);
		ActionHandler.click(SVO_OpportunityContainer.OrderPlacedTab);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		// user changes the status of opportunity to order placed
		ActionHandler.click(SVO_OpportunityContainer.MarkasCurrentStage);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User changes status");
	}

	// navigate to Orders
	@Then("^navigate to Orders link and open the order$")
	public void navigate_to_Orders_link_and_open_the_order() throws Throwable {
		ActionHandler.wait(3);
		ActionHandler.scrollDown();
		ActionHandler.wait(2);
		ActionHandler.scrollDown();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(SVO_OpportunityContainer.OppOrder);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on orders quick link");

		// user click on first order number//
		// driver.navigate().refresh();
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.firstOrder);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on created order");
	}

	// Open the work order
	@Then("^Open the work order$")
	public void open_the_work_order() throws Throwable {
		ActionHandler.wait(3);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		ActionHandler.pageDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		// user selects and opens the work order
		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(SVO_OpportunityContainer.WorkOrder);
		ActionHandler.wait(5);
		ActionHandler.pressEnter();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on work order");
	}

	// create work order
	@Then("^Enter all the required details in work order$")
	public void enter_details_in_work_order() throws Throwable {
		String BDate = "12/06/2021";
		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.wait(10);
		// user enters details for build forecast
		ActionHandler.click(SVO_OpportunityContainer.BuildForecast);
		ActionHandler.wait(7);
		ActionHandler.setText(SVO_OpportunityContainer.BuildForecastText, BDate);
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.BuildCheckbox);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.wait(10);
		// user enters details for ABS forecast
		ActionHandler.setText(SVO_OpportunityContainer.ABSForecast, BDate);
		ActionHandler.wait(5);
		ActionHandler.setText(SVO_OpportunityContainer.ABSOriginal, BDate);
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.ABSCheckbox);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.SaveOpportunity);
		ActionHandler.wait(15);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter work order details");
	}

	// mark order status as Cancelled by customer
	@Then("^Navigate to the order and mark status as Cancelled by customer$")
	public void navigate_to_the_order_and_mark_status_as_cancelled_by_customer() throws Throwable {
		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.SelectOrder);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.outcomeStage);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.MarkAsCurrentStatus);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// user selects order and marks as outcome cancelled
		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.Outcome);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.CancByCustomer);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.OrderDone);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("change order status");
	}

	// verify status of opportunity
	@Then("^Navigate to Opportunity and verify that the status is changed to Closed Lost$")
	public void navigate_to_opportunity_and_verify_that_the_status_is_changed_to_Closed_lost() throws Throwable {
		ActionHandler.wait(3);
		ActionHandler.pageDown();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.SelectOpp);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verify opportunity status");

		ActionHandler.wait(3);
		VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.oppClosedLost);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Opportunity status moves to closed lost");
	}

	// click on send
	@Then("^Click on Send button and verify the error$")
	public void click_on_Send_button_and_verify_the_error() throws Throwable {
		driver.switchTo().frame(0);
		javaScriptUtil.clickElementByJS(SVO_OpportunityContainer.EmailSend);
		ActionHandler.wait(20);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("The email is not sent and error message is displayed");
		driver.switchTo().parentFrame();
	}

	// edit autopopulated fields
	@Then("^Edit auto populated fields like Related to \"([^\"]*)\" and Importance \"([^\"]*)\" in Compose email page$")
	public void edit_auto_populated_fields_like_Related_to_and_Importance_in_Compose_email_page(String Related,
			String importance) throws Throwable {
		String re[] = Related.split(",");
		Related = CommonFunctions.readExcelMasterData(re[0], re[1], re[2]);

		String imp[] = importance.split(",");
		importance = CommonFunctions.readExcelMasterData(imp[0], imp[1], imp[2]);

		System.out.println("The details are fetched from excel");

		ActionHandler.wait(20);

		driver.switchTo().frame(0);

		// user enters related to field details
		ActionHandler.click(SVO_OpportunityContainer.Relatedto);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.Relatedtooption(Related))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("Related to selected");

		ActionHandler.pageDown();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// user selects the importance
		ActionHandler.click(SVO_OpportunityContainer.Importance);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.Relatedtooption(importance))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Importance selected");
		ActionHandler.wait(5);
	}

	// validate and edit from and BU fields
	@Then("^Verify that the user cannot edit From and Business unit fields in Compose email page$")
	public void verify_that_the_user_cannot_edit_from_and_business_unit_fields_in_compose_email_page()
			throws Throwable {
		driver.switchTo().frame(0);
		ActionHandler.wait(3);
		// user selects the business unit
		ActionHandler.click(SVO_OpportunityContainer.BUnit);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		// user enters email from address
		ActionHandler.click(SVO_OpportunityContainer.EmailFrom);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("The email is not sent and error message is displayed");
		driver.switchTo().parentFrame();
	}

	// open calander under booker 25
	@Then("^User Navigate to \"([^\"]*)\" under \"([^\"]*)\" menu$")
	public void user_Navigate_to_under_menu(String calendar, String Booker25) throws Throwable {
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.MenuIcon);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		// user navigates to calendar under booker 25
		ActionHandler.setText(SVO_OpportunityContainer.searchMenu, Booker25);
		ActionHandler.wait(5);
		Actions act5 = new Actions(driver);
		act5.sendKeys(Keys.ARROW_DOWN).build().perform();
		act5.sendKeys(Keys.ENTER).build().perform();
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(20);
		CommonFunctions.attachScreenshot();
		javaScriptUtil.clickElementByJS(SVO_OpportunityContainer.CalendarOpt);
		ActionHandler.wait(10);
		Reporter.addStepLog("User navigates to calendars under Booker25 menu");
	}

	// edit reservation title field
	@Then("^Select and open any calendar and edit the Reservation title field as \"([^\"]*)\"$")
	public void select_and_open_any_calendar_and_edit_the_Reservation_title_field_as(String title) throws Throwable {
		ActionHandler.wait(5);
		// user selects calendar option
		ActionHandler.click(SVO_OpportunityContainer.selectCalendar);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(10);
		// user edits the title and saves
		ActionHandler.click(SVO_OpportunityContainer.editTitle);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		ActionHandler.clearAndSetText(SVO_OpportunityContainer.titleInput, title);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.SaveOpportunity);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(10);
		Reporter.addStepLog("User edits the title");
	}

	// change owner
	@Then("^Click on dropdown and navigate to change owner$")
	public void click_on_dropdown_and_navigate_to_change_owner() throws Throwable {
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.DropdownBtn);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		// user changes the owner of opportunity
		ActionHandler.click(SVO_OpportunityContainer.changeOwner);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to change owner");
	}

	// navigate to change record type
	@Then("^Click on dropdown and navigate to change record type$")
	public void click_on_dropdown_and_navigate_to_change_record_type() throws Throwable {
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.DropdownBtn);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		// user changes the record type
		ActionHandler.click(SVO_OpportunityContainer.changeRecordType);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to change record type");
	}

	// change record type
	@Then("^Change the record type to \"([^\"]*)\" and save$")
	public void change_the_record_type_to_and_save(String type) throws Throwable {
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.selectRecordType);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.nextRType);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		// user changes the record type and saves
		ActionHandler.click(SVO_OpportunityContainer.SaveOpportunity);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User changes the record type");
	}

	// Mark Strip & parts /BIW build stage as complete
	@Then("^Mark Strip & parts /BIW build stage as complete in work order and verify the other details$")
	public void mark_Strip_parts_BIW_build_stage_as_complete_in_work_order_and_verify_the_other_details()
			throws Throwable {
		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.wait(10);
		// user marks build stage complete in work order
		ActionHandler.click(SVO_OpportunityContainer.StripComplete);
		ActionHandler.wait(7);
		ActionHandler.click(SVO_OpportunityContainer.StripCheckbox);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.SaveOpportunity);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verify the other fields under Strip and Process parts are autopopulated");
	}

	// Navigate to Handover details section
	@Then("^Navigate to Handover details section and enter the required details$")
	public void navigate_to_Handover_details_section_and_enter_the_required_details() throws Throwable {
		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.wait(10);
		// user enters ABS details
		ActionHandler.click(SVO_OpportunityContainer.ABSComplete);
		ActionHandler.wait(7);
		ActionHandler.click(SVO_OpportunityContainer.ABSCompleteCheck);
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.FullyPaid);
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.HandoverDate);
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.SelectDate);
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.DVLAReq);
		ActionHandler.wait(5);
		// user enters details in hand over section
		ActionHandler.click(SVO_OpportunityContainer.Heritage);
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.FMSObt);
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.FMSCompleted);
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.RemoveEtracker);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.SaveOrder);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verify the Handover details are entered");
	}

	// Mark Status as complete for the order
	@Then("^User should be able to Mark Status as complete for the order$")
	public void user_should_be_able_to_Mark_Status_as_complete_for_the_order() throws Throwable {

		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.DBMarkStatusComplete);
		ActionHandler.wait(8);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User is able to mark status as complete");
	}

	// Enter details of New Opportunity
	@Then("^Enter details of New Opportunity like Opportunity name \"([^\"]*)\" Product Offering \"([^\"]*)\" Account Name \"([^\"]*)\" Region \"([^\"]*)\" Preferred Contact \"([^\"]*)\" Brand \"([^\"]*)\" and Model \"([^\"]*)\"$")
	public void enter_details_of_New_Opportunity_like_Opportunity_name_Product_Offering_Account_Name_Region_Preferred_Contact_Brand_and_Model(
			String OppName, String productOffering, String AccountName, String region, String preContact, String brand,
			String model) throws Throwable {
		/*
		 * Below code is used to extract data from Excel Master Data and assign it to a
		 * string
		 */
		String Oname[] = OppName.split(",");
		OppName = CommonFunctions.readExcelMasterData(Oname[0], Oname[1], Oname[2]);

		String pOffering[] = productOffering.split(",");
		productOffering = CommonFunctions.readExcelMasterData(pOffering[0], pOffering[1], pOffering[2]);

		String oAccountName[] = AccountName.split(",");
		AccountName = CommonFunctions.readExcelMasterData(oAccountName[0], oAccountName[1], oAccountName[2]);

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String pc[] = preContact.split(",");
		preContact = CommonFunctions.readExcelMasterData(pc[0], pc[1], pc[2]);

		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String mo[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(mo[0], mo[1], mo[2]);

		/* Below strings contains information required to fill the New Opportunity */
		double randomNumber = getRandomIntegerBetweenRange(0, 1000);
		String Stage = "Qualified";

		Reporter.addStepLog("Start entering mandatory details");
		ActionHandler.wait(5);

		// Below Code is updated the Opportunity Name on New Opportunity Details page.
		ActionHandler.wait(7);
		System.out.println("User has clicked on SVO Bespoke");
		ActionHandler.setText(SVO_OpportunityContainer.OpportunityName, OppName + randomNumber);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// Below Code Click on Close date and Select the date
		String CDate = "12/02/2022";
		ActionHandler.setText(SVO_OpportunityContainer.OpportunityCloseDate, CDate);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// Below Code Click on Stage and Select the Qualified value from the drop down
		ActionHandler.click(SVO_OpportunityContainer.OpportunityStage);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.ValueSelection(Stage))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// Below Code is to select the product offering value from drop down
		ActionHandler.click(SVO_OpportunityContainer.prodOffering);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.ValueSelection(productOffering))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// Below Code is to select the region value from drop down
		ActionHandler.click(SVO_OpportunityContainer.Opportunityeditregion);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.ValueSelection(region))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// Below Code search for prferredContact details then select value for preferred
		// contact
		ActionHandler.setText(SVO_OpportunityContainer.preferredCnt, preContact);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		act1.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// Below Code search for Account name and then select value for it
		ActionHandler.pageScrollDown();
		ActionHandler.setText(SVO_OpportunityContainer.SVOAccountName, AccountName);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// Below Code search for brand name then select brand value
		ActionHandler.setText(SVO_OpportunityContainer.brand, brand);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Actions act2 = new Actions(driver);
		act2.sendKeys(Keys.ARROW_DOWN).build().perform();
		act2.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// Below Code search for Model name then select brand value
		ActionHandler.setText(SVO_OpportunityContainer.model, model);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Actions act3 = new Actions(driver);
		act3.sendKeys(Keys.ARROW_DOWN).build().perform();
		act3.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
	}

	// Navigate to Quotes
	@Then("^Navigate to Quotes$")
	public void navigate_to_Quotes() throws Throwable {
		ActionHandler.wait(4);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.OppQuotes);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on quotes quick link");
	}

	// Create a new Quote
	@Then("^Create a new Quote \"([^\"]*)\" and save$")
	public void create_a_new_Quote_and_save(String contact) throws Throwable {
		String con[] = contact.split(",");
		contact = CommonFunctions.readExcelMasterData(con[0], con[1], con[2]);

		double randomNumber = getRandomIntegerBetweenRange(0, 1000);
		String quote = "Test Quote";

		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.NewQuote);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.nextRType);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		// user enters details to create quote
		ActionHandler.wait(2);
		ActionHandler.setText(SVO_OpportunityContainer.quoteName, quote + randomNumber);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.pageDown();
		ActionHandler.wait(2);
		ActionHandler.setText(SVO_OpportunityContainer.quoteContact, contact);
		ActionHandler.wait(3);
		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.SaveQuote);
		ActionHandler.wait(15);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.QuoteName);
		ActionHandler.wait(6);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User creates a new quote");
	}

	// open Design briefs
	@Then("^Navigate and open Design briefs$")
	public void navigate_and_open_Design_briefs() throws Throwable {
		// user click on design brief link//
		ActionHandler.wait(5);
		ActionHandler.scrollDown();

		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.DesignBriefs);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on design brief link");

		// user opens design brief//
		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.DesignBriefName);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Open design briefs");
	}

	// Enter the Exterior design details
	@Then("^Enter the Exterior design details and save$")
	public void enter_the_Exterior_design_details_and_save() throws Throwable {
		double randomNumber = getRandomIntegerBetweenRange(0, 1000);
		String data = "Test data";
		ActionHandler.pageScrollDown();
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.editPaint);
		ActionHandler.wait(15);
		// user enters all details for exterior design
		ActionHandler.setText(SVO_OpportunityContainer.inputPaint, data + randomNumber);
		ActionHandler.wait(5);
		ActionHandler.setText(SVO_OpportunityContainer.inputFinish, data + randomNumber);
		ActionHandler.wait(5);
		ActionHandler.setText(SVO_OpportunityContainer.inputDesignPacks, data + randomNumber);
		ActionHandler.wait(5);
		ActionHandler.setText(SVO_OpportunityContainer.inputWheelStyle, data + randomNumber);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.SaveOpportunity);
		ActionHandler.wait(10);
		Reporter.addStepLog("user enters data in exterior design details");
	}

	// verify the error on submit
	@Then("^Click on Submit and verify the error$")
	public void click_on_Submit_and_verify_the_error() throws Throwable {
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		// user submits the design
		ActionHandler.click(SVO_OpportunityContainer.SubmitDesign);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.SaveSubmit);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();

		// user verifies the error message
		VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.SubmitError);
		ActionHandler.click(SVO_OpportunityContainer.CancelSubmit);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("error is displayed while submitting");
	}

	// validate OPS Approval Status field
	@Then("^Verify the OPS Approval Status field is not editable$")
	public void verify_the_OPS_Approval_Status_field_is_not_editable() throws Throwable {
		ActionHandler.pageScrollDown();
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("OPS Approval Status field is not editable");
	}

	// cick on new opportunity
	@Then("^Click on New button$")
	public void click_on_New_button() throws Throwable {
		
		ActionHandler.wait(4);
		Reporter.addStepLog("User Creates new Opportunity");
		ActionHandler.click(SVO_OpportunityContainer.newOpportunityBtn);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
	}

	// Create a new SVO Quote
	@Then("^Create a new SVO Quote \"([^\"]*)\" and save$")
	public void create_a_new_SVO_Quote_and_save(String quote) throws Throwable {
		double randomNumber = getRandomIntegerBetweenRange(0, 1000);
		String contact = "Test Retailer contact";
		ActionHandler.click(SVO_OpportunityContainer.NewQuote);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		// user enters mandatory fields for quote creation
		ActionHandler.setText(SVO_OpportunityContainer.quoteName, quote + randomNumber);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(SVO_OpportunityContainer.quoteContact, contact);
		ActionHandler.wait(3);
		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		// user saves the quote
		ActionHandler.click(SVO_OpportunityContainer.SaveQuote);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		// ActionHandler.click(SVO_OpportunityContainer.selectQuote);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User creates a new quote");
	}

	// edit the status of new quote
	@Then("^User edit the status as \"([^\"]*)\" of saved Quote$")
	public void user_edit_the_status_as_of_saved_Quote(String QuoteStatus) throws Throwable {
		String status[] = QuoteStatus.split(",");
		QuoteStatus = CommonFunctions.readExcelMasterData(status[0], status[1], status[2]);
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.FirstQuote);
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.NewQuoteBtn);
		ActionHandler.wait(5);

		// user edits the status of quote and saves
		ActionHandler.click(SVO_OpportunityContainer.EditQuoteBtn);
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.QuoteStatusList);
		ActionHandler.wait(5);
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.StatusSelection(QuoteStatus))));
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.SaveQuote);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User edit the status as Proposal Sent");
	}

	// error message displayed while saving quote
	@Then("^Verify the error message displayed to save quote$")
	public void verify_the_error_message_displayed_to_save_quote() throws Throwable {
		ActionHandler.wait(5);
		// user verifies the error message while saving the quote
		if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.SaveQuoteError)) {
			Reporter.addStepLog("Error message validated");
			CommonFunctions.attachScreenshot();
		} else {
			Reporter.addStepLog("No error message was caught");
			CommonFunctions.attachScreenshot();
		}
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.CancelQuote);
		ActionHandler.wait(5);
	}

	// navigate to Orders
	@Then("^User navigates to Orders section from Related links of Opportunity$")
	public void user_navigates_to_Orders_section_from_Related_links_of_Opportunity() throws Exception {
		ActionHandler.wait(5);
		javaScriptUtil.clickElementByJS(SVO_OpportunityContainer.OrderWindow);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVO_OpportunityContainer.Order1fromsearch);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
	}

	@Then("^Validate that Handover details are blank and try to move to the next stage$")
	public void Validate_handover_details_blank_move_the_next_stage() throws Exception {
		ActionHandler.wait(2);
		javaScriptUtil.scrollIntoView(SVO_OpportunityContainer.handoverDetails);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies that the handover details are blank");

		ActionHandler.wait(2);
		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.MarkStatusAsComplete);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("An error occurred when user tries to move to next stage without handover details");

	}

	// Navigate to Work Order
	@Then("^Navigate to Work Order from Order window$")
	public void navigate_to_Work_Order_from_Order_window() throws Exception {
		ActionHandler.pageDown();
		ActionHandler.wait(1);
		ActionHandler.pageDown();
		ActionHandler.wait(1);
		ActionHandler.pageArrowUp();
		ActionHandler.wait(4);
		System.out.print("Click on work Order");
		ActionHandler.click(SVO_OpportunityContainer.WorkOrder1);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
	}

	// Edit Build start dates
	@Then("^Edit Build start dates and Validate start slippage days$")
	public void edit_Build_start_dates_and_Validate_start_slippage_day_s() throws Exception {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		String date = dateFormat.format(new Date());
		System.out.println(date);
		javaScriptUtil.clickElementByJS(SVO_OpportunityContainer.editBuildStart);
		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.StartOriginalDate);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(SVO_OpportunityContainer.StartOriginalDate, date);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.SaveWO);
		ActionHandler.wait(4);
		String text = ActionHandler.getElementText(SVO_OpportunityContainer.SlippingDays);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		if (!text.contains("")) {
			Reporter.addScenarioLog("Slipping days is auto updated on save");
		} else {
			Reporter.setTestRunnerOutput("Slipping days is auto updated on save");
		}
	}

	// validate error for build start date
	@Then("^Set ABS-forecast date prior to Build Start date and validate error$")
	public void set_ABS_forecast_date_prior_to_Build_Start_date_and_validate_error() throws Exception {
		javaScriptUtil.clickElementByJS(SVO_OpportunityContainer.editBuildStart);
		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.ABSForecast);
		CommonFunctions.attachScreenshot();
		ActionHandler.clearAndSetText(SVO_OpportunityContainer.ABSForecast, "16/01/2021");
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.SaveWO);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.ABS_Prior_Build_error)) {
			Reporter.addScenarioLog("Error Occured: ABS Forecast cannot be earlier than any build stage forecast.");
		} else {
			Reporter.setTestRunnerOutput(
					"Script Failed: Error Expected: ABS Forecast cannot be earlier than any build stage forecast.");
		}
	}

	// validate error for paint forecast date
	@Then("^Set Strip_Parts Forecast and Paint forecast date in a non sequential manner and validate error$")
	public void set_Strip_Parts_Forecast_and_Paint_forecast_date_in_a_non_sequential_manner_and_validate_error()
			throws Exception {
		javaScriptUtil.clickElementByJS(SVO_OpportunityContainer.editBuildStart);
		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.StripForecast);
		CommonFunctions.attachScreenshot();
		ActionHandler.clearAndSetText(SVO_OpportunityContainer.StripForecast, "16/01/2022");
		ActionHandler.wait(2);
		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.PaintForecast);
		CommonFunctions.attachScreenshot();
		ActionHandler.clearAndSetText(SVO_OpportunityContainer.PaintForecast, "16/03/2022");
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.SaveWO);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.ABS_Prior_Build_error)) {
			Reporter.addScenarioLog(
					"Error Occured: Build stages must have sequential forecast dates. Please check your forecast dates to ensure each stage does not have a forecast date before any earlier stage forecast.");
		} else {
			Reporter.setTestRunnerOutput(
					"Script Failed: Error Expected: Build stages must have sequential forecast dates. Please check your forecast dates to ensure each stage does not have a forecast date before any earlier stage forecast.");
		}
	}

	// navigates to Invoice
	@Then("^User navigates to Invoice which is in Planned Request Status$")
	public void user_navigates_to_Invoice_which_is_in_Planned_Request_Status() throws Exception {

	}

	// edit payment date
	@Then("^User edits payment date and value and validates the invoice status is moved to Payment request sent$")
	public void user_edits_payment_date_and_value_and_validates_the_invoice_status_is_moved_to_Payment_request_sent()
			throws Exception {

	}

	// edit paid amount details
	@Then("^User edits paid ammount details and validates the status is changed to Payment Request Paid$")
	public void user_edits_paid_ammount_details_and_validates_the_status_is_changed_to_Payment_Request_Paid()
			throws Exception {
		if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.SaveQuoteError)) {
			Reporter.addStepLog("Error message validated");
			CommonFunctions.attachScreenshot();
		} else {
			Reporter.addStepLog("No error message was caught");
			CommonFunctions.attachScreenshot();
		}
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.CancelQuote);
		ActionHandler.wait(5);
	}

	// Move the design brief stage
	@Then("^Move the design brief stage from Draft to Submitted$")
	public void move_the_design_brief_stage_from_Draft_to_Submitted() throws Throwable {
		// user click on submit button//
		ActionHandler.wait(7);
		ActionHandler.click(SVO_OpportunityContainer.SubmitDesignBrief);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on submit burtton");

		// User click on save button//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.SaveDesignBreif);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on save button");
		ActionHandler.wait(5);

		// User click on cancel button of second pop up//
		/*
		 * if (!VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.PopUptitle))
		 * { ActionHandler.wait(3);
		 * ActionHandler.click(SVO_OpportunityContainer.CancelDesignBrief);
		 * ActionHandler.wait(5); CommonFunctions.attachScreenshot();
		 * Reporter.addStepLog("User click on cancel button of second pop up"); }
		 */
		Reporter.addStepLog("stage is moved to submitted");
		CommonFunctions.attachScreenshot();

	}

	// validate Approval History
	@Then("^Verfify the Approval History of Design brief$")
	public void verfify_the_Approval_History_of_Design_brief() throws Throwable {
		ActionHandler.wait(5);

		// user verifies the approval history of design brief
		if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.ApprovalHistory)) {
			Reporter.addStepLog("Approval History is displayed");
			CommonFunctions.attachScreenshot();
		} else {
			Reporter.addStepLog("Approval History not found");
			CommonFunctions.attachScreenshot();
		}

	}

	// set up login
	@Then("^set up login to the user role \"([^\"]*)\"$")
	public void set_up_login_to_the_user_role(String User) throws Throwable {
		String U[] = User.split(",");
		User = CommonFunctions.readExcelMasterData(U[0], U[1], U[2]);

		// user navigates to set up bar//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.setUpBtn);
		ActionHandler.wait(3);
		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(10);

		ActionHandler.wait(5);
		String NewWindow = driver.getWindowHandle();
		ActionHandler.wait(30);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String SetUpWindow = i.next();
			if (!NewWindow.equalsIgnoreCase(SetUpWindow)) {
				driver.switchTo().window(SetUpWindow);

				// selects the user role //
				ActionHandler.wait(5);
				ActionHandler.setText(SVO_OpportunityContainer.SearchSetup, User);
				ActionHandler.wait(5);
				Actions act1 = new Actions(driver);
				act1.sendKeys(Keys.ARROW_DOWN).build().perform();
				act1.sendKeys(Keys.ENTER).build().perform();
				Reporter.addStepLog("selects the user role");
				CommonFunctions.attachScreenshot();
				ActionHandler.wait(10);

				// refresh the tab
				driver.navigate().refresh();
				ActionHandler.wait(5);

				// user clicks on login btn//
				ActionHandler.wait(10);
				driver.switchTo().frame(0);
				ActionHandler.wait(3);
				ActionHandler.click(SVO_OpportunityContainer.UserLoginBtn);
				ActionHandler.wait(10);
				Reporter.addStepLog("User is logged in successfully");
				CommonFunctions.attachScreenshot();
				// driver.switchTo().window(NewWindow);
			}
		}
	}

	// validate delete button
	@Then("^Verify that delete button is not available$")
	public void verify_that_delete_button_is_not_available() throws Throwable {
		ActionHandler.wait(5);

		// user verifies the availability of delete button
		if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.OppDelete)) {
			Reporter.addStepLog("Delete option is not available");
			CommonFunctions.attachScreenshot();
		} else {
			Reporter.addStepLog("Delete option is available");
			CommonFunctions.attachScreenshot();
		}
	}

	// Return Render pack of design brief
	@Then("^Return Render pack of design brief$")
	public void return_Render_pack_of_design_brief() throws Throwable {
		// user click on Return Render pack button//
		/*
		 * ActionHandler.wait(2);
		 * ActionHandler.click(SVO_OpportunityContainer.DesignReturned);
		 * ActionHandler.wait(5); CommonFunctions.attachScreenshot();
		 * 
		 * ActionHandler.wait(2);
		 * ActionHandler.click(SVO_OpportunityContainer.editTaskSave);
		 * ActionHandler.wait(4); CommonFunctions.attachScreenshot();
		 */

		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.EditStatus);
		ActionHandler.wait(7);

		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.OpportunityStage);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.designBriefStage);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.SaveVehicle);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.DesignBriefStatus);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("stage is moved to Returned stage");

		// user changes the status of design brief to returned
		/*
		 * ActionHandler.click(SVO_OpportunityContainer.ReturnRenderPack);
		 * ActionHandler.wait(5); CommonFunctions.attachScreenshot();
		 * Reporter.addStepLog("user click on Return Render pack button");
		 * 
		 * // user click on save button// ActionHandler.wait(3);
		 * ActionHandler.click(SVO_OpportunityContainer.SaveReturnPack);
		 * ActionHandler.wait(5);
		 * Reporter.addStepLog("stage is moved to Returned stage");
		 * CommonFunctions.attachScreenshot();
		 */
	}

	// Navigate back to Quote
	@Then("^Navigate back to Quote section$")
	public void navigate_back_to_Quote_section() throws Throwable {

		// user click on Quote link//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.QuoteBackLink);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Navigated to Quote section");
	}

	// Navigate to Orders under Quotes
	@Then("^Navigate to Orders under Quotes and click open$")
	public void navigate_to_Orders_under_Quotes_and_click_open() throws Throwable {
		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.QuoteOrder);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Navigated to Orders in Quote section");

	}

	// fill the price details of Quote
	@Then("^fill the price details of Quote like CostPrice \"([^\"]*)\" ListPrice \"([^\"]*)\" Contribution \"([^\"]*)\"  RetailPrice \"([^\"]*)\" Revenue\"([^\"]*)\"$")

	public void fill_the_price_details_of_Quote_like_CostPrice_ListPrice_Contribution_RetailPrice_Revenue(
			String CostPrice, String ListPrice, String Contribution, String RetailPrice, String Revenue)
			throws Throwable {
		String CP[] = CostPrice.split(",");
		CostPrice = CommonFunctions.readExcelMasterData(CP[0], CP[1], CP[2]);

		String LP[] = ListPrice.split(",");
		ListPrice = CommonFunctions.readExcelMasterData(LP[0], LP[1], LP[2]);

		String CB[] = Contribution.split(",");
		Contribution = CommonFunctions.readExcelMasterData(CB[0], CB[1], CB[2]);

		String RP[] = RetailPrice.split(",");
		RetailPrice = CommonFunctions.readExcelMasterData(RP[0], RP[1], RP[2]);

		String RV[] = Revenue.split(",");
		Revenue = CommonFunctions.readExcelMasterData(RV[0], RV[1], RV[2]);

		Reporter.addStepLog("data retrieved from master data");
		ActionHandler.wait(5);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		ActionHandler.pageScrollDown();
		CommonFunctions.attachScreenshot();

		// user click on Edit price pencil icon//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.EditCostPrice);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on Edit price pencil icon");

		// user enters price details//
		ActionHandler.wait(3);
		ActionHandler.setText(SVO_OpportunityContainer.CostPriceTextBox, CostPrice);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVO_OpportunityContainer.ListPriceTextBox, ListPrice);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVO_OpportunityContainer.RetailPriceTextBox, RetailPrice);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVO_OpportunityContainer.ContributionTextBox, Contribution);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVO_OpportunityContainer.RevenueTextBox, Revenue);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// user click on save button//
		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.DBSaveButton);
		Reporter.addStepLog("User clears price details and saved it");
		CommonFunctions.attachScreenshot();

		ActionHandler.pageCompleteScrollUp();

	}

	// enter Outcome fields
	@Then("^User enter Outcome fields like Outcome \"([^\"]*)\" OutcomeStage \"([^\"]*)\"$")
	public void user_enter_Outcome_fields_like_Outcome_OutcomeStage(String Outcome, String OutcomeStage)
			throws Throwable {
		String OC[] = Outcome.split(",");
		Outcome = CommonFunctions.readExcelMasterData(OC[0], OC[1], OC[2]);

		String OS[] = OutcomeStage.split(",");
		OutcomeStage = CommonFunctions.readExcelMasterData(OS[0], OS[1], OS[2]);

		Reporter.addStepLog("data retrieved from master data");

		// user selects outcome//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.OutcomeDropDown);
		ActionHandler.wait(5);
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.ValueSelection(Outcome))));
		CommonFunctions.attachScreenshot();

		// user selects outcome detail//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.OutcomeDetailDropDown);
		ActionHandler.wait(5);
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.ValueSelection(Outcome))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects outcome and outcome detail");

		// user click on done button to save the dependency//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.Donebutton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves the outcome");
	}

	// send Quote pack
	@Then("^User send Quote pack$")
	public void user_send_Quote_pack() throws Throwable {
		// User click on Send Quote pack button//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.SendQuotePack);
		CommonFunctions.attachScreenshot();

		// selects documents and click on next step button//
		ActionHandler.wait(5);
		driver.switchTo().frame(0);

		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.FirstDocCheckbox);
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.NextStepBtn);
		CommonFunctions.attachScreenshot();

		// click on E-sign Documents button//
		ActionHandler.wait(10);
		ActionHandler.click(SVO_OpportunityContainer.EsignDocBtn);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User send quote pack for an esign");
	}

	// Verify the error message
	@Then("^Verify the error message displayed$")
	public void verify_the_error_message_displayed() throws Throwable {
		ActionHandler.wait(5);
		if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.DownloadBtn)) {
			Reporter.addStepLog("Download option is available");
			CommonFunctions.attachScreenshot();
		} else {
			Reporter.addStepLog("Download option is not available");
			CommonFunctions.attachScreenshot();
		}
	}

	// Create a new order
	@Then("^Create a new order with Opportunity \"([^\"]*)\"$")
	public void create_a_new_order_with_opportunity(String Opp) throws Throwable {
		String Opportunity[] = Opp.split(",");
		Opp = CommonFunctions.readExcelMasterData(Opportunity[0], Opportunity[1], Opportunity[2]);

		// user creates a new order
		ActionHandler.click(SVO_OpportunityContainer.NewOrder);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.SVOOrder);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.nextRType);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// user enters the mandatory details to create a order
		ActionHandler.setText(SVO_OpportunityContainer.OrderOpp, Opp);
		ActionHandler.wait(5);
		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		ActionHandler.pageScrollDown();
		ActionHandler.click(SVO_OpportunityContainer.OrderOppStart);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.SelectDate);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter all the required details");
	}

	// Save the order
	@Then("^Save the order created$")
	public void save_the_order_created() throws Throwable {
		ActionHandler.click(SVO_OpportunityContainer.SaveBtn);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Save the order");
	}

	// Delete the Quote
	@Then("^Delete the Quote created$")
	public void delete_the_Quote_created() throws Throwable {
		ActionHandler.click(SVO_OpportunityContainer.QuoteDelete);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		// user confirms to delete the quote
		ActionHandler.click(SVO_OpportunityContainer.OppDeleteConfirm);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Quote should be deleted");
	}

	// Navigate to Files
	@Then("^Navigate to Files under Quotes and click open$")
	public void navigate_to_Files_under_Quotes_and_click_open() throws Throwable {
		ActionHandler.wait(3);
		ActionHandler.pageScrollDown();
		CommonFunctions.attachScreenshot();

		// user opens files under quotes section
		ActionHandler.click(SVO_OpportunityContainer.QuoteFiles);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens Files");
	}

	// Add a new file
	@Then("^Add a new file and save$")
	public void add_a_new_file_and_save() throws Throwable {
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// user adds new file in Files section
		ActionHandler.click(SVO_OpportunityContainer.AddFiles);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();

		// user selects a file
		ActionHandler.click(SVO_OpportunityContainer.FileSelect);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.AddButton);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User adds a File and verifies the details");
	}

	// submit file
	@Then("^Click on Submit and navigate back to Quote$")
	public void click_on_Submit_and_navigate_back_to_Quote() throws Throwable {
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// user changes quote status to submitted
		ActionHandler.click(SVO_OpportunityContainer.SubmitDesign);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.SaveSubmit);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		// ActionHandler.click(SVO_OpportunityContainer.QuoteSelect);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user submits the design brief");
	}

	// change quote status
	@Then("^Mark the Quote status as Outcome accepted$")
	public void mark_the_Quote_status_as_Outcome_accepted() throws Throwable {
		ActionHandler.click(SVO_OpportunityContainer.OutcomeList);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.MarkAsCurrentStatus);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// user selects outcome stage
		ActionHandler.click(SVO_OpportunityContainer.Outcome);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.OutcomeAccept);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		// user marks stage as accepted
		ActionHandler.click(SVO_OpportunityContainer.OutcomeDetail);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.OutcomeDetAccept);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.Donebutton);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Stage marked as Outcome accepted");
	}

	// change quote status
	@Then("^Mark the Quote status as Proposal Sent and verify user cannot navigate back stages$")
	public void mark_the_Quote_status_as_Proposal_Sent_and_verify_user_cannot_navigate_back_stages() throws Throwable {
		ActionHandler.click(SVO_OpportunityContainer.StageProposal);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.MarkAsCurrentStatus);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.StageError);
		Reporter.addStepLog("Error is displayed when user navigates back to proposal sent stage");
	}

	// create new filter
	@Then("^User selects New option to create New filter$")
	public void user_selects_New_option_to_create_New_filter() throws Throwable {

		// User click on List View control button//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.ListViewControlBtn);
		Reporter.addStepLog("User click on List View control button");
		CommonFunctions.attachScreenshot();

		// User selects new option to create new filter//
		ActionHandler.wait(5);
		ActionHandler.pressEnter();
		ActionHandler.wait(3);
		Reporter.addStepLog("User selects new option to create new filter");
		CommonFunctions.attachScreenshot();

	}

	// enter detials for new filter
	@Then("^User fill the details like ListName \"([^\"]*)\" and ListAPIname \"([^\"]*)\" then click on save$")
	public void user_fill_the_details_like_ListName_and_ListAPIname_then_click_on_save(String ListName,
			String ListAPIname) throws Throwable {
		String LN[] = ListName.split(",");
		ListName = CommonFunctions.readExcelMasterData(LN[0], LN[1], LN[2]);

		String API[] = ListAPIname.split(",");
		ListAPIname = CommonFunctions.readExcelMasterData(API[0], API[1], API[2]);
		Reporter.addStepLog("Read the data from Master data excel sheet");

		// User enters List name//
		ActionHandler.wait(3);
		ActionHandler.setText(SVO_OpportunityContainer.ListNameTxtBox, ListName);
		Reporter.addStepLog("User enters List name");
		CommonFunctions.attachScreenshot();

		// User enters List API name//
		double randomNumber = getRandomIntegerBetweenRange(0, 1000);

		int number = (int) randomNumber;

		ActionHandler.wait(3);
		ActionHandler.clearAndSetText(SVO_OpportunityContainer.ListAPINameTxtBox, ListAPIname + number);
		Reporter.addStepLog("User enters List API name");
		CommonFunctions.attachScreenshot();

		// User click on Save button//
		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.SaveListViewBtn);
		Reporter.addStepLog("User click on Save button");
		CommonFunctions.attachScreenshot();

	}

	// enter details
	@Then("^User Add filter by entering details like Field \"([^\"]*)\" Operator \"([^\"]*)\" Value \"([^\"]*)\"$")
	public void user_Add_filter_by_entering_details_like_Field_Operator_Value(String Field, String Operator,
			String Value) throws Throwable {

		String F[] = Field.split(",");
		Field = CommonFunctions.readExcelMasterData(F[0], F[1], F[2]);

		String Op[] = Operator.split(",");
		Operator = CommonFunctions.readExcelMasterData(Op[0], Op[1], Op[2]);

		String V[] = Value.split(",");
		Value = CommonFunctions.readExcelMasterData(V[0], V[1], V[2]);
		Reporter.addStepLog("Read the data from Master data excel sheet");

		// user click on Add filter link//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.AddFilterLink);
		Reporter.addStepLog("user click on Add filter link");
		CommonFunctions.attachScreenshot();

		// User selects Field to be filtered//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.FieldDropDown);
		ActionHandler.wait(3);
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.SourceSelection(Field))));
		Reporter.addStepLog("User selects Field to be filtered");
		CommonFunctions.attachScreenshot();

		// User selects Operator to be filtered//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.OperatorDropDown);
		ActionHandler.wait(3);
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.SourceSelection(Operator))));
		Reporter.addStepLog("User selects Operator to be filtered");
		CommonFunctions.attachScreenshot();

		// User enters Value field to be filtered//
		ActionHandler.wait(3);
		ActionHandler.setText(SVO_OpportunityContainer.ValueTxtBx, Value);
		ActionHandler.wait(3);
		Reporter.addStepLog("User enters Value field to be filtered");
		CommonFunctions.attachScreenshot();

		// user clicks on Done button//
		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.DoneFilterBtn);
		Reporter.addStepLog("User clicks on DOne Button");
		CommonFunctions.attachScreenshot();

	}

	// save filter
	@Then("^User click on Save filter$")
	public void user_click_on_Save_filter() throws Throwable {

		// user clicks on Save filter button//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.SaveFilterBtn);
		ActionHandler.wait(3);
		Reporter.addStepLog("User clicks on DOne Button");
		CommonFunctions.attachScreenshot();

	}

	// validate new filter
	@Then("^Verify that New filter is created$")
	public void verify_that_New_filter_is_created() throws Throwable {
		// verifies the filter creation//
		ActionHandler.wait(5);
		if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.FilterTitle)) {
			Reporter.addStepLog("Filter list is added Successfully");
			CommonFunctions.attachScreenshot();
		} else {
			Reporter.addStepLog("Filter is not created");
			CommonFunctions.attachScreenshot();
		}

	}

	// create SVO bespoke opportunity
	@And("^fill all mandatory fields of SVOBespoke opportunity like OpportunityName \"([^\"]*)\" Closedate \"([^\"]*)\" Stage \"([^\"]*)\" ProductOffering \"([^\"]*)\" Region \"([^\"]*)\" AccountName \"([^\"]*)\" PreferredContact \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\"$")
	public void fill_all_mandatory_fields_of_SVOBespoke_opportunity_like_OpportunityName_Closedate_Stage_ProductOffering_Region_AccountName_PreferredContact_Brand_Model(
			String OpportunityName, String Closedate, String Stage, String ProductOffering, String Region,
			String AccountName, String PreferredContact, String Brand, String Model) throws Throwable {

		double randomNumber = getRandomIntegerBetweenRange(0, 1000);
		String OpName[] = OpportunityName.split(",");
		OpportunityName = CommonFunctions.readExcelMasterData(OpName[0], OpName[1], OpName[2]);

		String Cldate[] = Closedate.split(",");
		Closedate = CommonFunctions.readExcelMasterData(Cldate[0], Cldate[1], Cldate[2]);

		String Stg[] = Stage.split(",");
		Stage = CommonFunctions.readExcelMasterData(Stg[0], Stg[1], Stg[2]);

		String ProdOfr[] = ProductOffering.split(",");
		ProductOffering = CommonFunctions.readExcelMasterData(ProdOfr[0], ProdOfr[1], ProdOfr[2]);

		String r[] = Region.split(",");
		Region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String AccName[] = AccountName.split(",");
		AccountName = CommonFunctions.readExcelMasterData(AccName[0], AccName[1], AccName[2]);

		String PrefCont[] = PreferredContact.split(",");
		PreferredContact = CommonFunctions.readExcelMasterData(PrefCont[0], PrefCont[1], PrefCont[2]);

		String b[] = Brand.split(",");
		Brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String mo[] = Model.split(",");
		Model = CommonFunctions.readExcelMasterData(mo[0], mo[1], mo[2]);

		Reporter.addStepLog("Data has been retrieved from Master Data");

		// user enters opportunity name //
		ActionHandler.wait(5);
		ActionHandler.setText(SVO_OpportunityContainer.OppName, OpportunityName + randomNumber);
		Reporter.addStepLog("User enters an Opportunity name");
		CommonFunctions.attachScreenshot();

		// User enters close date//
		ActionHandler.wait(3);
		ActionHandler.setText(SVO_OpportunityContainer.CloseDate, Closedate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters close date");

		// User selects the stage//
		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.StageDW);
		ActionHandler.wait(2);
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.ValueSelection(Stage))));
		Reporter.addStepLog("User select the stage");
		CommonFunctions.attachScreenshot();

		// User selects product offering//
		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.ProductOffering);
		ActionHandler.wait(2);
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.ValueSelection(ProductOffering))));
		Reporter.addStepLog("User select the Product Offering");
		CommonFunctions.attachScreenshot();

		// User selects Region,area and market//
		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.RegionDW);
		ActionHandler.wait(2);
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.ValueSelection(Region))));
		Reporter.addStepLog("User select the Region,area and market dropdown");
		CommonFunctions.attachScreenshot();

		// User selects preferred contact name//
		ActionHandler.wait(2);
		ActionHandler.scrollToView(SVO_OpportunityContainer.Client);
		ActionHandler.wait(2);
		ActionHandler.setText(SVO_OpportunityContainer.PrefContact, PreferredContact);
		ActionHandler.wait(5);
		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		act1.sendKeys(Keys.ENTER).build().perform();
		Reporter.addStepLog("User enters Preferred contact name");
		CommonFunctions.attachScreenshot();

		// user enters client name
		ActionHandler.wait(2);
		ActionHandler.setText(SVO_OpportunityContainer.AccountName, AccountName);
		ActionHandler.wait(5);
		Actions act4 = new Actions(driver);
		act4.sendKeys(Keys.ARROW_DOWN).build().perform();
		act4.sendKeys(Keys.ENTER).build().perform();
		Reporter.addStepLog("User enters client name");
		CommonFunctions.attachScreenshot();

		// user enters acccount name//
		ActionHandler.wait(3);
		ActionHandler.setText(SVO_OpportunityContainer.ClientName, AccountName);
		ActionHandler.wait(5);
		Actions act3 = new Actions(driver);
		act3.sendKeys(Keys.ARROW_DOWN).build().perform();
		act3.sendKeys(Keys.ENTER).build().perform();
		Reporter.addStepLog("User enters Account name");
		CommonFunctions.attachScreenshot();

		// user selects brand//
		ActionHandler.wait(3);
		ActionHandler.setText(SVO_OpportunityContainer.brand, Brand);
		ActionHandler.wait(5);
		Actions acta = new Actions(driver);
		acta.sendKeys(Keys.ARROW_DOWN).build().perform();
		acta.sendKeys(Keys.ENTER).build().perform();
		Reporter.addStepLog("User selected Brand of vehicle");
		CommonFunctions.attachScreenshot();

		// User selects Model//
		ActionHandler.wait(3);
		ActionHandler.setText(SVO_OpportunityContainer.model, Model);
		ActionHandler.wait(5);
		Actions act5 = new Actions(driver);
		act5.sendKeys(Keys.ARROW_DOWN).build().perform();
		act5.sendKeys(Keys.ENTER).build().perform();
		Reporter.addStepLog("User enters model name");
		CommonFunctions.attachScreenshot();

		// User saves the opportunity//
		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.SaveOpportunity);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

	}

	// select email template
	@And("^User selects the template of an email$")
	public void user_selects_the_template_of_an_email() throws Throwable {

		// user navigate to mail frame//
		ActionHandler.wait(5);
		driver.switchTo().frame(0);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user navigate to mail frame");

		// user click on select template button//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.SelectTemplateBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on select template button");

		// user switch to template window and selects first template//
		ActionHandler.wait(5);
		String NewWindow = driver.getWindowHandle();
		ActionHandler.wait(30);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!NewWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.wait(5);
				ActionHandler.click(SVO_OpportunityContainer.Template1);
				ActionHandler.wait(20);
				ActionHandler.click(SVO_OpportunityContainer.OkTempBtn);
				ActionHandler.wait(5);
				CommonFunctions.attachScreenshot();
				Reporter.addStepLog("User selects the first template");
				driver.switchTo().window(NewWindow);
			}
		}
		// user switch to mail frame//
		ActionHandler.wait(5);
		driver.switchTo().frame(0);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user switch to mail frame");
		ActionHandler.wait(3);

	}

	// save email
	@Then("^User save the email$")
	public void user_save_the_email() throws Throwable {
		// scroll and click on save email button//
		ActionHandler.wait(3);
		ActionHandler.scrollToView(SVO_OpportunityContainer.saveemail);
		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.saveemail);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on save mail");

		// click on save email button//
		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.cancelemail);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on cancel mail");

		driver.switchTo().frame(0);
		ActionHandler.wait(15);
		ActionHandler.pageDown();
		ActionHandler.wait(5);

		// user selects a file
		ActionHandler.click(SVO_OpportunityContainer.selectFile);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on a file");
		CommonFunctions.attachScreenshot();

		// user moves to next page
		ActionHandler.click(SVO_OpportunityContainer.Nextstep);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on next step");
		CommonFunctions.attachScreenshot();
		driver.switchTo().parentFrame();

		if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.GenDoc)) {
			driver.switchTo().frame(0);
			ActionHandler.wait(15);
			ActionHandler.click(SVO_OpportunityContainer.GenDoc);
			ActionHandler.wait(15);
			Reporter.addStepLog("User clicks the Generate Document");
			CommonFunctions.attachScreenshot();
			driver.switchTo().parentFrame();
		}

	}

	// create new opportunity
	@Given("^User click on New button to create an opportunity$")
	public void user_click_on_New_button_to_create_an_opportunity() throws Throwable {
		// user click on New Button//
		ActionHandler.wait(4);
		ActionHandler.click(SVO_OpportunityContainer.newOpportunityBtn);
		Reporter.addStepLog("user click  on New Button");
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

	}

	// select document for send quote
	@When("^User selects documents and click on Next step$")
	public void user_selects_documents_and_click_on_Next_step() throws Throwable {

		// User click on Send Quote pack button//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.SendQuotePack);
		CommonFunctions.attachScreenshot();

		// user switch to frame//
		ActionHandler.wait(5);
		driver.switchTo().frame(0);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user switch to frame");

		// user selects first two docs//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.FirstDocCheckbox);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user selects first doc");

		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.SecondDocCheckbox);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user selects second doc");

		// user click on Next step button//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.NextStepBtn);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on Next step button");

	}

	// send generated documents for e-sign
	@When("^Generated documents are sent for Electronic Signature through mail$")
	public void generated_documents_are_sent_for_Electronic_Signature_through_mail() throws Throwable {

		// user click on send documents for electronic signature button//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.ElectronicSignatureBtn);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on send documents for electronic signature button");

		// user send docs for e-sign through mail//
		ActionHandler.wait(10);
		driver.switchTo().frame(0);
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.Nextstep);
		ActionHandler.wait(10);
		Reporter.addStepLog("User clicks on next step");
		CommonFunctions.attachScreenshot();
		// user verifies the error after clicking on esign
		VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.ESignError);
		driver.switchTo().parentFrame();
	}

	// create new opportunity
	@Then("^Create New Opportunity for SVO Private Office$")
	public void create_New_Opportunity_for_SVO_Private_Office() throws Throwable {
		ActionHandler.wait(5);
		Reporter.addStepLog("User Creates new Opportunity");
		ActionHandler.click(SVO_OpportunityContainer.newOpportunityBtn);
		ActionHandler.click(SVO_OpportunityContainer.SendDocBtn);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user send docs for e-sign through mail");

	}

	// Create a new Bespoke design brief Quote
	@Then("^Create a new Bespoke design brief Quote \"([^\"]*)\"$")
	public void create_a_new_Bespoke_design_brief_Quote(String RetailerContact) throws Throwable {

		double randomNumber = getRandomIntegerBetweenRange(0, 1000);

		String RC[] = RetailerContact.split(",");
		RetailerContact = CommonFunctions.readExcelMasterData(RC[0], RC[1], RC[2]);

		// user click on New Quote button//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.NewQuote);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on New Quote button");

		// user enters Quote name//
		ActionHandler.wait(3);
		ActionHandler.setText(SVO_OpportunityContainer.quoteName, RetailerContact + randomNumber);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user enters Quote name");

		// user selects retailer contact//
		ActionHandler.setText(SVO_OpportunityContainer.quoteContact, RetailerContact);
		ActionHandler.wait(3);
		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user selects retailer contact");

	}

	// save the new Bespoke design brief Quote
	@Then("^User save the new Bespoke design brief Quote$")
	public void user_save_the_new_Bespoke_design_brief_Quote() throws Throwable {
		// user click on save button//
		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.SaveQuote);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on save button");
	}

	// save the new Bespoke design brief Mike Quote
	@Then("^User save the new Bespoke design brief Mike Quote$")
	public void user_save_the_new_Bespoke_design_brief_Mike_Quote() throws Throwable {
		// user click on save button//
		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.SaveQuoteMike);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on save button");
	}

	// selects the created Quote
	@Then("^User selects the created Quote$")
	public void user_selects_the_created_Quote() throws Throwable {
		// user selects quote that is created//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.FirstQuote);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate to Quote section");
	}

	// fill all mandatory fields of SVOBespoke Mike opportunity
	@And("^fill all mandatory fields of SVOBespoke Mike opportunity like OpportunityName \"([^\"]*)\" Closedate \"([^\"]*)\" Stage \"([^\"]*)\" ProductOffering \"([^\"]*)\" Region \"([^\"]*)\" AccountName \"([^\"]*)\" PreferredContact \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\"$")
	public void fill_all_mandatory_fields_of_SVOBespoke_Mike_opportunity_like_OpportunityName_Closedate_Stage_ProductOffering_Region_AccountName_PreferredContact_Brand_Model(
			String OpportunityName, String Closedate, String Stage, String ProductOffering, String Region,
			String AccountName, String PreferredContact, String Brand, String Model) throws Throwable {

		double randomNumber = getRandomIntegerBetweenRange(0, 1000);
		String OpName[] = OpportunityName.split(",");
		OpportunityName = CommonFunctions.readExcelMasterData(OpName[0], OpName[1], OpName[2]);

		String Cldate[] = Closedate.split(",");
		Closedate = CommonFunctions.readExcelMasterData(Cldate[0], Cldate[1], Cldate[2]);

		String Stg[] = Stage.split(",");
		Stage = CommonFunctions.readExcelMasterData(Stg[0], Stg[1], Stg[2]);

		String ProdOfr[] = ProductOffering.split(",");
		ProductOffering = CommonFunctions.readExcelMasterData(ProdOfr[0], ProdOfr[1], ProdOfr[2]);

		String r[] = Region.split(",");
		Region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String AccName[] = AccountName.split(",");
		AccountName = CommonFunctions.readExcelMasterData(AccName[0], AccName[1], AccName[2]);

		String PrefCont[] = PreferredContact.split(",");
		PreferredContact = CommonFunctions.readExcelMasterData(PrefCont[0], PrefCont[1], PrefCont[2]);

		String b[] = Brand.split(",");
		Brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String mo[] = Model.split(",");
		Model = CommonFunctions.readExcelMasterData(mo[0], mo[1], mo[2]);

		Reporter.addStepLog("Data has been retrieved from Master Data");

		// user enters opportunity name //
		ActionHandler.wait(5);
		ActionHandler.setText(SVO_OpportunityContainer.OppName, OpportunityName + randomNumber);
		Reporter.addStepLog("User enters an Opportunity name");
		CommonFunctions.attachScreenshot();

		// User enters close date//
		ActionHandler.wait(5);
		ActionHandler.setText(SVO_OpportunityContainer.CloseDate, Closedate);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// User selects the stage//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.StageDWMike);
		ActionHandler.wait(3);
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.ValueSelection(Stage))));
		ActionHandler.wait(3);
		Reporter.addStepLog("User select the stage");
		CommonFunctions.attachScreenshot();

		// User selects product offering//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.ProductOffering);
		ActionHandler.wait(3);
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.ValueSelection(ProductOffering))));
		ActionHandler.wait(5);
		Reporter.addStepLog("User select the Product Offering");
		CommonFunctions.attachScreenshot();

		// User selects Region,area and market//
		ActionHandler.click(SVO_OpportunityContainer.RegionDWMike);
		ActionHandler.wait(5);
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.ValueSelection(Region))));
		ActionHandler.wait(5);
		Reporter.addStepLog("User select the Region,area and market dropdown");
		CommonFunctions.attachScreenshot();

		// User selects preferred contact name//
		ActionHandler.scrollToView(SVO_OpportunityContainer.Client);
		ActionHandler.wait(3);
		ActionHandler.setText(SVO_OpportunityContainer.PrefContact, PreferredContact);
		ActionHandler.wait(5);
		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		act1.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		Reporter.addStepLog("User enters Preferred contact name");
		CommonFunctions.attachScreenshot();

		// user enters acccount name//
		ActionHandler.wait(5);
		ActionHandler.setText(SVO_OpportunityContainer.ClientName, AccountName);
		ActionHandler.wait(5);
		Actions act3 = new Actions(driver);
		act3.sendKeys(Keys.ARROW_DOWN).build().perform();
		act3.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		Reporter.addStepLog("User enters Account name");
		CommonFunctions.attachScreenshot();

		// user enters client name//
		ActionHandler.wait(5);
		ActionHandler.setText(SVO_OpportunityContainer.ClientName, AccountName);
		ActionHandler.wait(5);
		Actions act4 = new Actions(driver);
		act4.sendKeys(Keys.ARROW_DOWN).build().perform();
		act4.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		Reporter.addStepLog("User enters client name");
		CommonFunctions.attachScreenshot();

		// user selects brand//
		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);
		ActionHandler.setText(SVO_OpportunityContainer.brand, Brand);
		ActionHandler.wait(5);
		Actions actb = new Actions(driver);
		actb.sendKeys(Keys.ARROW_DOWN).build().perform();
		actb.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		Reporter.addStepLog("User selected Brand of vehicle");
		CommonFunctions.attachScreenshot();

		// User selects Model//
		ActionHandler.wait(5);
		ActionHandler.setText(SVO_OpportunityContainer.model, Model);
		ActionHandler.wait(5);
		Actions act5 = new Actions(driver);
		act5.sendKeys(Keys.ARROW_DOWN).build().perform();
		act5.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		Reporter.addStepLog("User enters model name");
		CommonFunctions.attachScreenshot();

		// User saves the opportunity//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.SaveOpportunity);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User created new opportunity");
		ActionHandler.wait(3);
	}

	// change order stage
	@When("^User changes order stage to \"([^\"]*)\"$")
	public void user_changes_order_stage_to(String Stage) throws Throwable {

		String Stg[] = Stage.split(",");
		Stage = CommonFunctions.readExcelMasterData(Stg[0], Stg[1], Stg[2]);

		// user click on edit order button//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.EditOrderBtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on edit order button");

		// user selects stage from stage drop downlist//
		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.OrderStageDropDown);
		ActionHandler.wait(3);
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.StatusSelection(Stage))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user selects stage from stage drop downlist");

		// user click on save button//
		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.SaveOrderBtn);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on save button");
	}

	// edit the donor details of vehicle
	@Then("^User edit the donor details of vehicle$")
	public void user_edit_the_donor_details_of_vehicle() throws Throwable {
		// scroll to view donor details//
		ActionHandler.wait(5);
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();

		// user click on edit vehicle pencil icon//
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();

		// user verifies outcome details cannot be changed
		ActionHandler.click(SVO_OpportunityContainer.CloseWindow);
		ActionHandler.click(SVO_OpportunityContainer.EditVehicle);

		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on edit vehicle pencil icon");

		// user clears the vehicle details//
		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.ClearDonorBtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user clears the vehicle details");

		// user click on save button//
		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.SaveOpportunity);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on save button");
	}

	// navigate to Activity tab
	@When("^User navigate to Activity tab$")
	public void user_navigate_to_Activity_tab() throws Throwable {

		// user click on Activity tab//
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.ActivityTab);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// user navigates to stage history page
		ActionHandler.click(SVO_OpportunityContainer.StageHistory);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.BackToOpp);
		ActionHandler.wait(10);
		Reporter.addStepLog("User cannot edit the stage history");
	}

	// navigate to New task tab
	@When("^User navigate to New task tab$")
	public void user_navigate_to_New_task_tab() throws Throwable {
		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.NewTaskTab);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate to New task tab");

	}

	// creat new task
	@Then("^User created new task with subject field as \"([^\"]*)\"$")
	public void user_created_new_task_with_subject_field_as(String Subject) throws Throwable {

		String sub[] = Subject.split(",");
		Subject = CommonFunctions.readExcelMasterData(sub[0], sub[1], sub[2]);
		Reporter.addStepLog("Data Retrieved from master data sheet");

		// User enters text in subject text box//
		ActionHandler.wait(5);
		ActionHandler.setText(SVO_OpportunityContainer.SubjectTextBox, Subject);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters text in subject text box");

	}

	// save the Task
	@Then("^User save the Task$")
	public void user_save_the_Task() throws Throwable {

		ActionHandler.wait(3);
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.SaveTask);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// user navigates to opportunity field history page
		ActionHandler.click(SVO_OpportunityContainer.OppFieldHistory);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User cannot edit the stage history");
	}

	// Mark the Quote status as Outcome rejected
	@Then("^Mark the Quote status as Outcome rejected$")
	public void mark_the_Quote_status_as_Outcome_rejected() throws Throwable {
		ActionHandler.click(SVO_OpportunityContainer.OutcomeTab);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.MarkAsCurrentStatus);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// user selects quote stage as outcome
		ActionHandler.click(SVO_OpportunityContainer.Outcome);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.OutcomeReject);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// user rejects the quote and selects outcome details
		ActionHandler.click(SVO_OpportunityContainer.OutcomeDetail);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.OutcomeDetReject);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.Donebutton);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Stage marked as Outcome rejected");
	}

	// Mark the Quote status as Design brief draft
	@Then("^Mark the Quote status as Design brief draft and verify the error message$")
	public void mark_the_Quote_status_as_Design_brief_draft_and_verify_the_error_message() throws Throwable {
		ActionHandler.click(SVO_OpportunityContainer.DesignBriefDraft);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.MarkAsCurrentStatus);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// user verifies the error message when Quote status is marked as Design brief
		// draft
		VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.StageError);
		Reporter.addStepLog("Error is displayed when user navigates back to Design brief draft stage");
	}

	// Enter details of New Opportunity
	@Then("^Enter details of New Opportunity like Opportunity name \"([^\"]*)\" Stage \"([^\"]*)\" Region \"([^\"]*)\" Account Name \"([^\"]*)\" Preferred Contact \"([^\"]*)\" Programme \"([^\"]*)\" and Source \"([^\"]*)\"$")
	public void enter_details_of_New_Opportunity_like_Opportunity_name_Stage_Region_Account_Name_Preferred_Contact_Programme_and_source(
			String OppName, String stage, String region, String AccountName, String preContact, String programme,
			String source) throws Throwable {
		/*
		 * Below code is used to extract data from Excel Master Data and assign it to a
		 * string
		 */
		String Oname[] = OppName.split(",");
		OppName = CommonFunctions.readExcelMasterData(Oname[0], Oname[1], Oname[2]);

		String stg[] = stage.split(",");
		stage = CommonFunctions.readExcelMasterData(stg[0], stg[1], stg[2]);

		String oAccountName[] = AccountName.split(",");
		AccountName = CommonFunctions.readExcelMasterData(oAccountName[0], oAccountName[1], oAccountName[2]);

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String pc[] = preContact.split(",");
		preContact = CommonFunctions.readExcelMasterData(pc[0], pc[1], pc[2]);

		String prgm[] = programme.split(",");
		programme = CommonFunctions.readExcelMasterData(prgm[0], prgm[1], prgm[2]);

		String s[] = source.split(",");
		source = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		/* Below strings contains information required to fill the New Opportunity */
		double randomNumber = getRandomIntegerBetweenRange(0, 1000);
		String Stage = "Wait List";

		Reporter.addStepLog("Start entering mandatory details");
		ActionHandler.wait(5);

		// Below Code is updated the Opportunity Name on New Opportunity Details page.
		ActionHandler.wait(7);
		System.out.println("User has clicked on Classic Continuation");
		ActionHandler.setText(SVO_OpportunityContainer.OpportunityName, OppName + randomNumber);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// Below Code Click on Close date and Select the date
		String CDate = "12/02/2023";
		ActionHandler.setText(SVO_OpportunityContainer.OpportunityCloseDate, CDate);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// Below Code Click on Stage and Select the Qualified value from the drop down
		ActionHandler.click(SVO_OpportunityContainer.ContinuationStage);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.ValueSelection(Stage))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// Below Code is to select the region value from drop down
		ActionHandler.click(SVO_OpportunityContainer.ContinuationRegion);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.ValueSelection(region))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// Below Code search for Account name and then select value for it
		ActionHandler.setText(SVO_OpportunityContainer.SVOAccountName, AccountName);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// Below Code search for preferred Contact details
		ActionHandler.setText(SVO_OpportunityContainer.preferredCnt, preContact);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		act1.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// Below Code click on Restricted Party Screening and the select value
		ActionHandler.wait(1);
		ActionHandler.click(SVO_OpportunityContainer.restrictedPS);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.Checkcleared);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// Below Code search for brand name then select programme
		ActionHandler.setText(SVO_OpportunityContainer.ContinuationPrgm, programme);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Actions act2 = new Actions(driver);
		act2.sendKeys(Keys.ARROW_DOWN).build().perform();
		act2.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// Below Code is to enter pricing details
		String RP = "100";
		ActionHandler.setText(SVO_OpportunityContainer.RetailPriceTextBox, RP);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.VATinput);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.VATYes);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// Below Code click on Source and the select value
		ActionHandler.click(SVO_OpportunityContainer.ContinuationSource);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.ValueSelection(source))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
	}

	// Edit the priority position of the opportunity
	@Then("^Edit the priority position of the opportunity as \"([^\"]*)\" and Save$")
	public void edit_the_priority_position_of_the_opportunity_as_and_save(String num) throws Throwable {
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// user edits the priority position field
		ActionHandler.click(SVO_OpportunityContainer.PriorityPositionBtn);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		ActionHandler.clearAndSetText(SVO_OpportunityContainer.PriorityPosition, num);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.SaveBtn);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
	}

	// select Contracting stage
	@Then("^Click on Contracting stage and mark as current stage$")
	public void click_on_Contracting_stage_and_mark_as_current_stage() throws Throwable {
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.ContractingTab);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.MarkasCurrentStage);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
	}

	// Select a document for e-sign
	@Then("^Select a document and move to next step to send documents for electronic signature$")
	public void select_a_document_and_move_to_next_step_to_send_documents_for_electronic_signature() throws Throwable {
		driver.switchTo().frame(0);
		ActionHandler.wait(5);

		// user selects a file
		ActionHandler.click(SVO_OpportunityContainer.selectFileEmail);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on a file");
		CommonFunctions.attachScreenshot();

		// user moves to next page
		ActionHandler.click(SVO_OpportunityContainer.Nextstep);
		ActionHandler.wait(10);
		Reporter.addStepLog("User clicks on next step");
		CommonFunctions.attachScreenshot();
		driver.switchTo().parentFrame();

		// user clicks on send documents for electronic signature
		if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.DocESign)) {
			driver.switchTo().frame(0);
			ActionHandler.wait(10);
			ActionHandler.click(SVO_OpportunityContainer.DocESign);
			ActionHandler.wait(15);
			Reporter.addStepLog("User clicks on send document for E sign");
			CommonFunctions.attachScreenshot();
			VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.ESignError);
			driver.switchTo().parentFrame();
		}
	}

	// Navigate to Orders section
	@Then("^Navigate to Orders section and open the order$")
	public void navigate_to_orders_section_and_open_the_order() throws Throwable {
		ActionHandler.scrollDown();
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.OppOrder);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		// user selects a order and opens
		ActionHandler.click(SVO_OpportunityContainer.SelectOrder);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on orders quick link and open the order");
	}

	// Navigate to Files
	@Then("^Navigate to Files section$")
	public void navigate_to_Files_section() throws Throwable {
		ActionHandler.scrollDown();
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.QuoteFiles);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on orders quick link and open the order");
	}

	// delete Files
	@Then("^Select the file added and delete$")
	public void click_on_add_files_and_select_a_file_to_add_and_save() throws Throwable {
		ActionHandler.click(SVO_OpportunityContainer.AddFiles);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// user selects file to add
		ActionHandler.click(SVO_OpportunityContainer.FileSelect);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.AddButton);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on orders quick link and open the order");
	}

	// Change closed stage
	@Then("^Click on Change closed stage$")
	public void click_on_Change_closed_stage() throws Throwable {
		ActionHandler.click(SVO_OpportunityContainer.ChangeClosedStage);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Change closed stage");
	}

	// Edit the stage of opportunity
	@Then("^Edit the stage to Contract signed and save to verify the error$")
	public void edit_the_stage_to_contract_signed_and_save_to_verify_the_error() throws Throwable {
		ActionHandler.click(SVO_OpportunityContainer.InputStage);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		// user changes the stage to contract signed and verifies the error
		ActionHandler.click(SVO_OpportunityContainer.ContractSignedStage);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.Donebutton);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.OppStageError);
		Reporter.addStepLog("Stage cannor be changed to Contract signed");
	}

	// Navigate to Commissioning Request
	@Then("^Navigate to Commissioning Request$")
	public void navigate_to_Commissioning_Request() throws Throwable {
		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// user navigates to commissioning request quick links
		ActionHandler.click(SVO_OpportunityContainer.ComRequest);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user navigates to Commissioning Request");
	}

	// Create a new Commissioning Request
	@Then("^Create a new Commissioning Request from \"([^\"]*)\" to \"([^\"]*)\" and save$")
	public void create_a_new_Commissioning_Request_from_to_and_save(String SDate, String EDate) throws Throwable {
		String Start[] = SDate.split(",");
		SDate = CommonFunctions.readExcelMasterData(Start[0], Start[1], Start[2]);

		String End[] = EDate.split(",");
		EDate = CommonFunctions.readExcelMasterData(End[0], End[1], End[2]);

		// user creates a new commissioning request
		ActionHandler.click(SVO_OpportunityContainer.NewOrder);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.nextRType);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();

		// user enters the mandatory fields
		ActionHandler.setText(SVO_OpportunityContainer.StartDate, SDate);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(SVO_OpportunityContainer.EndDate, EDate);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		// user saves the details
		ActionHandler.click(SVO_OpportunityContainer.SaveButton);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.SelectComReq);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user creates a new Commissioning Request");
	}

	// Navigate to Reservations
	@Then("^Navigate to Reservations$")
	public void navigate_to_Reservations() throws Throwable {
		ActionHandler.click(SVO_OpportunityContainer.Reservations);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user navigates to reservations");
	}

	// create new Reservations
	@Then("^Create a new \"([^\"]*)\" reservation from \"([^\"]*)\" to \"([^\"]*)\" with contact \"([^\"]*)\" and save$")
	public void create_a_new_reservation_from_to_with_contact_and_save(String bespoke, String SDate, String EDate,
			String contact) throws Throwable {
		String Start[] = SDate.split(",");
		SDate = CommonFunctions.readExcelMasterData(Start[0], Start[1], Start[2]);

		String End[] = EDate.split(",");
		EDate = CommonFunctions.readExcelMasterData(End[0], End[1], End[2]);

		String Cnt[] = contact.split(",");
		contact = CommonFunctions.readExcelMasterData(Cnt[0], Cnt[1], Cnt[2]);

		// user creates a new reservation
		ActionHandler.click(SVO_OpportunityContainer.NewOrder);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.OpportunityRecordType(bespoke))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.nextRType);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();

		// user enters mandatory fields to create reservation
		ActionHandler.setText(SVO_OpportunityContainer.StartReserv, SDate);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(SVO_OpportunityContainer.EndReserv, EDate);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(SVO_OpportunityContainer.PrefContact, contact);
		ActionHandler.wait(5);
		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		// user saves the reservation
		ActionHandler.click(SVO_OpportunityContainer.SaveButton);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("create a new reservation");
	}

	// Navigate back to Commissioning request
	@Then("^Navigate back to Commissioning request and verify the status is moved to Proposed$")
	public void navigate_back_to_Commissioning_request_and_verify_the_status_is_moved_to_Proposed() throws Throwable {
		ActionHandler.click(SVO_OpportunityContainer.BackToOpp);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user navigates to commissioning request");
	}

	// Open the reservation and change status
	@Then("^Open the reservation and change the status to Booked and host \"([^\"]*)\"$")
	public void open_the_reservation_and_change_the_status_to_booked_and_host(String host) throws Throwable {
		String Cnt[] = host.split(",");
		host = CommonFunctions.readExcelMasterData(Cnt[0], Cnt[1], Cnt[2]);

		ActionHandler.click(SVO_OpportunityContainer.SelectComReq);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		// user changes the status of reservation to booked
		ActionHandler.click(SVO_OpportunityContainer.EditStatus);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.InputStatus);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.BookedStatus);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);

		// user enters the host name
		ActionHandler.setText(SVO_OpportunityContainer.inputHost, host);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.SaveButton);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user changes reservation status to Booked");
	}

	// Navigate back to Commissioning request
	@Then("^Navigate back to Commissioning request and verify the status is moved to \"([^\"]*)\"$")
	public void navigate_back_to_Commissioning_request_and_verify_the_status_is_moved_to(String Status)
			throws Throwable {
		ActionHandler.click(SVO_OpportunityContainer.BackComReq);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("verify the status of commissioning request is changed to booked");
	}

	// Open the reservation and change status
	@Then("^Open the reservation and change the status to Closed with reason \"([^\"]*)\" and host \"([^\"]*)\"$")
	public void open_the_reservation_and_change_the_status_to_Closed_with_reason_and_host(String reason, String host)
			throws Throwable {
		String Cnt[] = host.split(",");
		host = CommonFunctions.readExcelMasterData(Cnt[0], Cnt[1], Cnt[2]);

		ActionHandler.click(SVO_OpportunityContainer.SelectComReq);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		// user changes the status of reservation to closed
		ActionHandler.click(SVO_OpportunityContainer.EditStatus);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.InputStatus);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.ClosedStatus);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// user enters closed reason
		ActionHandler.click(SVO_OpportunityContainer.InputReason);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.ValueSelection(reason))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);

		// user enters the host name
		ActionHandler.setText(SVO_OpportunityContainer.inputHost, host);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.SaveButton);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user changes reservation status to Booked");
	}

	// Navigate to invoices
	@Then("^Navigate to invoices$")
	public void navigate_to_invoices() throws Throwable {
		ActionHandler.click(SVO_OpportunityContainer.Invoices);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user navigates to invoices");
	}

	// Create a new invoice
	@Then("^Create a new invoice with Opportunity \"([^\"]*)\" Account \"([^\"]*)\" and SAP account \"([^\"]*)\" and save$")
	public void create_a_new_invoice_with_Opportunity_Account_and_SAP_account_and_save(String Opp, String Account,
			String SAP) throws Throwable {
		String Acc[] = Account.split(",");
		Account = CommonFunctions.readExcelMasterData(Acc[0], Acc[1], Acc[2]);

		String SAPAcc[] = SAP.split(",");
		SAP = CommonFunctions.readExcelMasterData(SAPAcc[0], SAPAcc[1], SAPAcc[2]);

		ActionHandler.click(SVO_OpportunityContainer.newInvoice);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.nextRType);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(7);

		// user selects opportunity
		ActionHandler.setText(SVO_OpportunityContainer.OpportunityTxtBx, Opp);
		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		Reporter.addStepLog("User selects Opportunity from search list");
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// user selects account name
		ActionHandler.wait(5);
		ActionHandler.setText(SVO_OpportunityContainer.AccountTxtBx, Account);
		ActionHandler.wait(5);
		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		act1.sendKeys(Keys.ENTER).build().perform();
		Reporter.addStepLog("User selects Account name from search list");
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// user selects SAP account
		ActionHandler.wait(5);
		ActionHandler.setText(SVO_OpportunityContainer.SAPtxtBx, SAP);
		ActionHandler.wait(5);
		Actions act2 = new Actions(driver);
		act2.sendKeys(Keys.ARROW_DOWN).build().perform();
		act2.sendKeys(Keys.ENTER).build().perform();
		Reporter.addStepLog("User selects SAP Account");
		CommonFunctions.attachScreenshot();

		// user saves the invoice
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.SaveButton);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.OpenInvoice);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(7);
	}

	// Verify that auto populated fields
	@Then("^Verify that auto populated fields like Invoice name and Status are not editable$")
	public void verify_that_auto_populated_fields_like_Invoice_name_and_Status_are_not_editable() throws Throwable {
		VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.InvoiceName);
		VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.InvoiceStatus);
		CommonFunctions.attachScreenshot();
	}

	// clear common order number
	@Then("^User clears common order number$")
	public void user_clears_common_order_number() throws Throwable {

		// user refreshes the page and click on Edit common order pencil icon//
		ActionHandler.wait(5);
		driver.navigate().refresh();
		ActionHandler.wait(10);
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.EditCommonOrder);
		ActionHandler.wait(5);
		Reporter.addStepLog("user refreshes the page and click on Edit common order pencil icon");
		CommonFunctions.attachScreenshot();

		// user clears the common order number//
		ActionHandler.wait(3);
		ActionHandler.clearText(SVO_OpportunityContainer.CommonOrderTextBox);
		ActionHandler.wait(3);
		Reporter.addStepLog("user clears the common order number");
		CommonFunctions.attachScreenshot();

		// user click on save button//
		ActionHandler.wait(3);
		ActionHandler.scrollDown();
		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.SaveBtn);
		ActionHandler.wait(5);
		Reporter.addStepLog("user click on save button");
		CommonFunctions.attachScreenshot();
		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(3);
	}

	// mark status as complete
	@Then("^User mark status as complete$")
	public void user_mark_status_as_complete() throws Throwable {

		driver.navigate().refresh();
		ActionHandler.wait(15);
		ActionHandler.click(SVO_OpportunityContainer.MarkStatusAsComplete);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Mark as current status button");

	}

	// navigate to Files
	@And("^User navigate to Files section$")
	public void user_navigate_to_Files_section() throws Throwable {

		// user refreshes the page and click on Files Link//
		ActionHandler.wait(5);
		driver.navigate().refresh();
		ActionHandler.wait(10);
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.FilesLink);
		ActionHandler.wait(5);
		Reporter.addStepLog("user refreshes the page and click on Files Link");
		CommonFunctions.attachScreenshot();
	}

	// add files to the selected order
	@When("^User add files to the selected order$")
	public void user_add_Files_to_the_selected_order() throws Throwable {

		// user click on add files button//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.AddFilesBtn);
		ActionHandler.wait(5);
		Reporter.addStepLog("user click on add files button");
		CommonFunctions.attachScreenshot();

		// user checkbox the file to be added//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.FirstFileCheckBox);
		ActionHandler.wait(5);
		Reporter.addStepLog("user checkbox the file to be added");
		CommonFunctions.attachScreenshot();

		// user click on Add button to upload the file//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.AddBtn);
		ActionHandler.wait(5);
		Reporter.addStepLog("user click on Add button to upload the file");
		CommonFunctions.attachScreenshot();

	}

	// clear Body style details
	@When("^User clears Body style details of vehicle$")
	public void user_clars_Body_style_details_of_vehicle() throws Throwable {
		// user refreshes the page and scroll to view body style details//
		ActionHandler.wait(5);
		driver.navigate().refresh();
		ActionHandler.wait(10);
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		Reporter.addStepLog("user refreshes the page and click on Files Link");
		CommonFunctions.attachScreenshot();

		// user click on Edit Body Style pencil icon//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.EditBodyStyle);
		ActionHandler.wait(5);
		Reporter.addStepLog("user refreshes the page and scroll to view body style details");
		CommonFunctions.attachScreenshot();

		// User clears the body style of vehicle//
		ActionHandler.scrollDown();
		ActionHandler.wait(5);
		if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.ClearBodyStyle)) {
			ActionHandler.click(SVO_OpportunityContainer.ClearBodyStyle);
			ActionHandler.wait(5);
			Reporter.addStepLog("user click on clear body style icon");
			CommonFunctions.attachScreenshot();
		}

		// user click on save button//
		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.SaveButton);
		ActionHandler.wait(3);
		Reporter.addStepLog("user click on save button");
		CommonFunctions.attachScreenshot();

		ActionHandler.pageCompleteScrollUp();

	}

	// Enter details of Classic Limited Edition Opportunity
	@Then("^Enter details of Classic Limited Edition Opportunity like Opportunity name \"([^\"]*)\" Stage \"([^\"]*)\" Region \"([^\"]*)\" Account Name \"([^\"]*)\" Preferred Contact \"([^\"]*)\" Programme \"([^\"]*)\" and CloseDate \"([^\"]*)\"$")
	public void enter_details_of_Classic_Limited_Edition_Opportunity_like_Opportunity_name_Stage_Region_Account_Name_Preferred_Contact_Programme_and_CloseDate(
			String OppName, String stage, String region, String AccountName, String preContact, String programme,
			String cDate) throws Throwable {
		/*
		 * Below code is used to extract data from Excel Master Data and assign it to a
		 * string
		 */
		String Oname[] = OppName.split(",");
		OppName = CommonFunctions.readExcelMasterData(Oname[0], Oname[1], Oname[2]);

		String stg[] = stage.split(",");
		stage = CommonFunctions.readExcelMasterData(stg[0], stg[1], stg[2]);

		String oAccountName[] = AccountName.split(",");
		AccountName = CommonFunctions.readExcelMasterData(oAccountName[0], oAccountName[1], oAccountName[2]);

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String pc[] = preContact.split(",");
		preContact = CommonFunctions.readExcelMasterData(pc[0], pc[1], pc[2]);

		String prgm[] = programme.split(",");
		programme = CommonFunctions.readExcelMasterData(prgm[0], prgm[1], prgm[2]);

		String s[] = cDate.split(",");
		cDate = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		/* Below strings contains information required to fill the New Opportunity */
		double randomNumber = getRandomIntegerBetweenRange(0, 1000);

		Reporter.addStepLog("Start entering mandatory details");
		ActionHandler.wait(5);

		// Below Code is updated the Opportunity Name on New Opportunity Details page.
		ActionHandler.wait(7);
		System.out.println("User has clicked on Classic Continuation");
		ActionHandler.setText(SVO_OpportunityContainer.OpportunityName, OppName + randomNumber);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// Below Code Click on Close date and Select the date
		ActionHandler.setText(SVO_OpportunityContainer.OpportunityCloseDate, cDate);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// Below Code Click on Stage and Select the Qualified value from the drop down
		ActionHandler.click(SVO_OpportunityContainer.ContinuationStage);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.ValueSelection(stage))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// Below Code is to select the region value from drop down
		ActionHandler.click(SVO_OpportunityContainer.ContinuationRegion);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.ValueSelection(region))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// Below Code search for Account name and then select value for it
		ActionHandler.setText(SVO_OpportunityContainer.SVOAccountName, AccountName);
		ActionHandler.wait(8);
		CommonFunctions.attachScreenshot();
		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// Below Code search for preferred Contact details
		ActionHandler.setText(SVO_OpportunityContainer.preferredCnt, preContact);
		ActionHandler.wait(8);
		CommonFunctions.attachScreenshot();
		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		act1.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// Below Code search for brand name then select programme
		ActionHandler.setText(SVO_OpportunityContainer.ContinuationPrgm, programme);
		ActionHandler.wait(8);
		CommonFunctions.attachScreenshot();
		Actions act2 = new Actions(driver);
		act2.sendKeys(Keys.ARROW_DOWN).build().perform();
		act2.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

	}

	@And("^fill all mandatory fields of SVO Private Office opportunity like OpportunityName \"([^\"]*)\" Closedate \"([^\"]*)\" Stage \"([^\"]*)\" ProductOffering \"([^\"]*)\" Region \"([^\"]*)\" AccountName \"([^\"]*)\" PreferredContact \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\"$")
	public void fill_all_mandatory_fields_of_SVO_Private_Office_opportunity_like_OpportunityName_Closedate_Stage_ProductOffering_Region_AccountName_PreferredContact_Brand_Model(
			String OpportunityName, String Closedate, String Stage, String ProductOffering, String Region,
			String AccountName, String PreferredContact, String Brand, String Model) throws Throwable {

		double randomNumber = getRandomIntegerBetweenRange(0, 1000);
		String OpName[] = OpportunityName.split(",");
		OpportunityName = CommonFunctions.readExcelMasterData(OpName[0], OpName[1], OpName[2]);

		String Cldate[] = Closedate.split(",");
		Closedate = CommonFunctions.readExcelMasterData(Cldate[0], Cldate[1], Cldate[2]);

		String Stg[] = Stage.split(",");
		Stage = CommonFunctions.readExcelMasterData(Stg[0], Stg[1], Stg[2]);

		String ProdOfr[] = ProductOffering.split(",");
		ProductOffering = CommonFunctions.readExcelMasterData(ProdOfr[0], ProdOfr[1], ProdOfr[2]);

		String r[] = Region.split(",");
		Region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String AccName[] = AccountName.split(",");
		AccountName = CommonFunctions.readExcelMasterData(AccName[0], AccName[1], AccName[2]);

		String PrefCont[] = PreferredContact.split(",");
		PreferredContact = CommonFunctions.readExcelMasterData(PrefCont[0], PrefCont[1], PrefCont[2]);

		String b[] = Brand.split(",");
		Brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String mo[] = Model.split(",");
		Model = CommonFunctions.readExcelMasterData(mo[0], mo[1], mo[2]);

		Reporter.addStepLog("Data has been retrieved from Master Data");

		// user enters opportunity name //
		ActionHandler.wait(5);
		ActionHandler.setText(SVO_OpportunityContainer.OppName, OpportunityName + randomNumber);
		Reporter.addStepLog("User enters an Opportunity name");
		CommonFunctions.attachScreenshot();

		// User enters close date//
		ActionHandler.wait(5);
		ActionHandler.setText(SVO_OpportunityContainer.CloseDate, Closedate);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// User selects the stage//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.ProductOffering);
		ActionHandler.wait(3);
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.ValueSelection(Stage))));
		ActionHandler.wait(3);
		Reporter.addStepLog("User select the stage");
		CommonFunctions.attachScreenshot();

		// User selects product offering//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.StageDW);
		ActionHandler.wait(3);
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.ValueSelection(ProductOffering))));
		ActionHandler.wait(5);
		Reporter.addStepLog("User select the Product Offering");
		CommonFunctions.attachScreenshot();

		// User selects Region,area and market//
		ActionHandler.click(SVO_OpportunityContainer.RegionPrivateOffice);
		ActionHandler.wait(5);
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.ValueSelection(Region))));
		ActionHandler.wait(5);
		Reporter.addStepLog("User select the Region,area and market dropdown");
		CommonFunctions.attachScreenshot();

		// User selects preferred contact name//
		ActionHandler.scrollToView(SVO_OpportunityContainer.Client);
		ActionHandler.wait(3);
		ActionHandler.setText(SVO_OpportunityContainer.PrefContact, PreferredContact);
		ActionHandler.wait(5);
		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		act1.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		Reporter.addStepLog("User enters Preferred contact name");
		CommonFunctions.attachScreenshot();

		// user enters client name
		ActionHandler.wait(5);
		ActionHandler.setText(SVO_OpportunityContainer.AccountName, AccountName);
		ActionHandler.wait(5);
		Actions act4 = new Actions(driver);
		act4.sendKeys(Keys.ARROW_DOWN).build().perform();
		act4.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		Reporter.addStepLog("User enters client name");
		CommonFunctions.attachScreenshot();

		// user enters acccount name//
		ActionHandler.wait(5);
		ActionHandler.setText(SVO_OpportunityContainer.ClientName, AccountName);
		ActionHandler.wait(5);
		Actions act3 = new Actions(driver);
		act3.sendKeys(Keys.ARROW_DOWN).build().perform();
		act3.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		Reporter.addStepLog("User enters Account name");
		CommonFunctions.attachScreenshot();

		// user selects brand//
		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);
		ActionHandler.setText(SVO_OpportunityContainer.brand, Brand);
		ActionHandler.wait(5);
		ActionHandler.clearAndSetText(SVO_OpportunityContainer.brand, Brand);
		ActionHandler.wait(5);
		Actions acta = new Actions(driver);
		acta.sendKeys(Keys.ARROW_DOWN).build().perform();
		acta.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		Reporter.addStepLog("User selected Brand of vehicle");
		CommonFunctions.attachScreenshot();

		// User selects Model//
		ActionHandler.wait(5);
		ActionHandler.setText(SVO_OpportunityContainer.model, Model);
		ActionHandler.wait(5);
		ActionHandler.clearAndSetText(SVO_OpportunityContainer.model, Model);
		ActionHandler.wait(5);
		Actions act5 = new Actions(driver);
		act5.sendKeys(Keys.ARROW_DOWN).build().perform();
		act5.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		Reporter.addStepLog("User enters model name");
		CommonFunctions.attachScreenshot();

		// User saves the opportunity//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.SaveOpportunity);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User created new opportunity");
		ActionHandler.wait(3);
	}

	// User navigate to Commissioning Requests
	@When("^User navigate to Commissioning Requests$")
	public void user_navigate_to_Commissioning_Requests() throws Throwable {

		// scroll to view Commissioning Request link
		ActionHandler.wait(5);
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();

		// click on Commissioning Request Link
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.CommissioningRequestLink);
		ActionHandler.wait(5);
		Reporter.addStepLog("click on Commissioning Request Link");
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
	}

	// User create new Commissioning Request
	@Then("^User create new Commissioning Request with Record type \"([^\"]*)\"$")
	public void user_create_new_Commissioning_Request_with_Record_type(String RecordType) throws Throwable {

		String RecType[] = RecordType.split(",");
		RecordType = CommonFunctions.readExcelMasterData(RecType[0], RecType[1], RecType[2]);

		// refresh the page
		ActionHandler.wait(2);
		driver.navigate().refresh();
		ActionHandler.wait(15);

		// click on New button
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.NewCRBtn);
		ActionHandler.wait(3);
		Reporter.addStepLog("click on New button");
		CommonFunctions.attachScreenshot();

		// selects the record type
		ActionHandler.wait(3);
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.RTypeSelection(RecordType))));
		ActionHandler.wait(3);
		Reporter.addStepLog("selects the record type");
		CommonFunctions.attachScreenshot();

		// click on next button
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.Next);
		ActionHandler.wait(3);
		Reporter.addStepLog("click on next button");
		Reporter.addStepLog("Navigate to fill details of Commissioning Request");
		CommonFunctions.attachScreenshot();
	}

	// fill all mandatory fields of Commissioning Request
	@Then("^fill all mandatory fields of Commissioning Request like Status \"([^\"]*)\" Starting \"([^\"]*)\" Ending \"([^\"]*)\" Duration \"([^\"]*)\" EstAttendees \"([^\"]*)\" TourType \"([^\"]*)\"$")
	public void fill_all_mandatory_fields_of_Commissioning_Request_like_Status_Starting_Ending_Duration_EstAttendees_TourType(
			String Status, String Starting, String Ending, String Duration, String EstAttendees, String TourType)
			throws Throwable {

		// read the data from master data excel sheet
		String S[] = Status.split(",");
		Status = CommonFunctions.readExcelMasterData(S[0], S[1], S[2]);

		String St[] = Starting.split(",");
		Starting = CommonFunctions.readExcelMasterData(St[0], St[1], St[2]);

		String E[] = Ending.split(",");
		Ending = CommonFunctions.readExcelMasterData(E[0], E[1], E[2]);

		String D[] = Duration.split(",");
		Duration = CommonFunctions.readExcelMasterData(D[0], D[1], D[2]);

		String EA[] = EstAttendees.split(",");
		EstAttendees = CommonFunctions.readExcelMasterData(EA[0], EA[1], EA[2]);

		String T[] = TourType.split(",");
		TourType = CommonFunctions.readExcelMasterData(T[0], T[1], T[2]);

		Reporter.addStepLog("read the data from master data excel sheet");

		// user selects status from status drop down list
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.StatusDW);
		ActionHandler.wait(3);
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.ValueSelection(Status))));
		ActionHandler.wait(3);
		Reporter.addStepLog("user selects status from status drop down list");
		CommonFunctions.attachScreenshot();

		// user enters starting date and time
		ActionHandler.wait(3);
		ActionHandler.setText(SVO_OpportunityContainer.Starting, Starting);
		ActionHandler.wait(3);
		Reporter.addStepLog("user enters starting date and time");
		CommonFunctions.attachScreenshot();

		// user enters ending date and time
		ActionHandler.wait(3);
		ActionHandler.setText(SVO_OpportunityContainer.Ending, Ending);
		ActionHandler.wait(3);
		Reporter.addStepLog("user enters Ending date and time");
		CommonFunctions.attachScreenshot();

		// user enters Duration
		ActionHandler.wait(3);
		ActionHandler.setText(SVO_OpportunityContainer.Duration, Duration);
		ActionHandler.wait(3);
		Reporter.addStepLog("user enters Duration");
		CommonFunctions.attachScreenshot();

		// user enters Est.Attendeess
		ActionHandler.wait(3);
		ActionHandler.setText(SVO_OpportunityContainer.EstAttendees, EstAttendees);
		ActionHandler.wait(3);
		Reporter.addStepLog("user enters EstAttendees");
		CommonFunctions.attachScreenshot();

		// user selects TourType from TourType drop down list
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.TourTypeDW);
		ActionHandler.wait(3);
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.ValueSelection(TourType))));
		ActionHandler.wait(3);
		Reporter.addStepLog("user selects TourType from TourType drop down list");
		CommonFunctions.attachScreenshot();

	}

	// User open the created commissioning Request
	@When("^User open the created commissioning Request$")
	public void user_open_the_created_commissioning_Request() throws Throwable {

		// refresh the page
		ActionHandler.wait(2);
		driver.navigate().refresh();
		ActionHandler.wait(15);

		// click on created commissioning Request
		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.FirstOpportunity);
		ActionHandler.wait(5);
		Reporter.addStepLog("click on created commissioning Request");
		CommonFunctions.attachScreenshot();

	}

	// User submit the Commissioning Request
	@When("^User submit the Commissioning Request$")
	public void user_submit_the_Commissioning_Request() throws Throwable {

		// click on submit button to submit commissioning Request
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.SubmitCRbtn);
		ActionHandler.wait(5);
		Reporter.addStepLog("click on submit button to submit commissioning Request");
		CommonFunctions.attachScreenshot();

		// refresh the page
		ActionHandler.wait(2);
		driver.navigate().refresh();
		ActionHandler.wait(15);

		// click on save button
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.SaveShare);
		ActionHandler.wait(5);
		Reporter.addStepLog("click on save button");
		CommonFunctions.attachScreenshot();

	}

	@When("^User navigate to Notes Section$")
	public void user_navigate_to_Notes_Section() throws Throwable {
		// scroll to view Commissioning Request link
		ActionHandler.wait(5);
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();

		// click on Note Link under Related Lists Quick links
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.NoteLink);
		ActionHandler.wait(5);
		Reporter.addStepLog(" click on Note Link under Related Lists Quick links");
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);

	}

	@When("^User Create New Quote with title \"([^\"]*)\"$")
	public void user_Create_New_Quote_with_title(String Subject) throws Throwable {

		String Sub[] = Subject.split(",");
		Subject = CommonFunctions.readExcelMasterData(Sub[0], Sub[1], Sub[2]);

		// refresh the page
		ActionHandler.wait(2);
		driver.navigate().refresh();
		ActionHandler.wait(15);

		// click on New button
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.NewCRBtn);
		ActionHandler.wait(3);
		Reporter.addStepLog("click on New button");
		CommonFunctions.attachScreenshot();

		// enter text in Note title Text box
		ActionHandler.wait(5);
		ActionHandler.setText(SVO_OpportunityContainer.NoteTitle, Subject);
		ActionHandler.wait(3);
		Reporter.addStepLog("enter text in Note title Text box");
		CommonFunctions.attachScreenshot();

	}

	// User Save the Note
	@Then("^User Save the Note$")
	public void user_Save_the_Note() throws Throwable {

		// click on Done button
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.Done);
		ActionHandler.wait(3);
		Reporter.addStepLog("click on Done button");
		CommonFunctions.attachScreenshot();
	}

	// User share the created note
	@Then("^User share the created note to user \"([^\"]*)\"$")
	public void user_share_the_created_note_to_user(String ShareWith) throws Throwable {

		String SW[] = ShareWith.split(",");
		ShareWith = CommonFunctions.readExcelMasterData(SW[0], SW[1], SW[2]);

		// user click on share button
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.ShareBtn);
		ActionHandler.wait(3);
		Reporter.addStepLog("user click on share button");
		CommonFunctions.attachScreenshot();

		// user enters share with user name//
		ActionHandler.wait(5);
		ActionHandler.setText(SVO_OpportunityContainer.ShareWithuser, ShareWith);
		ActionHandler.wait(5);
		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		act1.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		Reporter.addStepLog("user enters share with user name");
		CommonFunctions.attachScreenshot();

		// user click on share button
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.ShareBtn1);
		ActionHandler.wait(5);
		Reporter.addStepLog("user click on share button");
		CommonFunctions.attachScreenshot();

	}

	// User delete the created note
	@Then("^User delete the created note$")
	public void user_delete_the_created_note() throws Throwable {
		// user click on delete button
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.ConfDeleteVehicle);
		ActionHandler.wait(5);
		Reporter.addStepLog("user click on delete button");
		CommonFunctions.attachScreenshot();

		// user click on delete pop up button
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.DeleteNote);
		ActionHandler.wait(5);
		Reporter.addStepLog("user click on delete pop up button");
		CommonFunctions.attachScreenshot();
	}

	@Then("^Enter details of New Opportunity for Private Office Opportunity name \"([^\"]*)\" Product Offering \"([^\"]*)\" Account Name \"([^\"]*)\" Region \"([^\"]*)\" Preferred Contact \"([^\"]*)\" Brand \"([^\"]*)\" and Model \"([^\"]*)\"$")
	public void enter_details_of_New_Opportunity_for_Private_Office_Opportunity_name_Product_Offering_Account_Name_Region_Preferred_Contact_Brand_and_Model(
			String OppName, String productOffering, String AccountName, String region, String preContact, String brand,
			String model) throws Throwable {
		/*
		 * Below code is used to extract data from Excel Master Data and assign it to a
		 * string
		 */
		String Oname[] = OppName.split(",");
		OppName = CommonFunctions.readExcelMasterData(Oname[0], Oname[1], Oname[2]);

		String pOffering[] = productOffering.split(",");
		productOffering = CommonFunctions.readExcelMasterData(pOffering[0], pOffering[1], pOffering[2]);

		String oAccountName[] = AccountName.split(",");
		AccountName = CommonFunctions.readExcelMasterData(oAccountName[0], oAccountName[1], oAccountName[2]);

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String pc[] = preContact.split(",");
		preContact = CommonFunctions.readExcelMasterData(pc[0], pc[1], pc[2]);

		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String mo[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(mo[0], mo[1], mo[2]);

		/* Below strings contains information required to fill the New Opportunity */
		double randomNumber = getRandomIntegerBetweenRange(0, 1000);
		String Stage = "Qualified";

		Reporter.addStepLog("Start entering mandatory details");
		ActionHandler.wait(5);

		// Below Code is updated the Opportunity Name on New Opportunity Details page.
		ActionHandler.wait(7);
		System.out.println("User has clicked on SVO Bespoke");
		ActionHandler.setText(SVO_OpportunityContainer.OpportunityName, OppName + randomNumber);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// Below Code Click on Close date and Select the date
		String CDate = "12/02/2022";
		ActionHandler.setText(SVO_OpportunityContainer.OpportunityCloseDate, CDate);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// Below Code Click on Stage and Select the Qualified value from the drop down
		ActionHandler.click(SVO_OpportunityContainer.POOpportunityStage);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.SourceSelection(Stage))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// Below Code is to select the product offering value from drop down
		ActionHandler.click(SVO_OpportunityContainer.POprodOffering);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.ValueSelection(productOffering))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// Below Code is to select the region value from drop down
		ActionHandler.click(SVO_OpportunityContainer.PORegion);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.ValueSelection(region))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// Below Code search for prferredContact details then select value for preferred
		// contact
		ActionHandler.setText(SVO_OpportunityContainer.preferredCnt, preContact);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		act1.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// Below Code click on Restricted Party Screening and the select value from drop
		// down
		ActionHandler.wait(1);
		ActionHandler.click(SVO_OpportunityContainer.POrestrictedParty);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.POCheckcleared);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// Below Code search for Account name and then select value for it
		ActionHandler.pageScrollDown();
		ActionHandler.setText(SVO_OpportunityContainer.POAccountName, AccountName);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// Below Code search for brand name then select brand value
		ActionHandler.setText(SVO_OpportunityContainer.brand, brand);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Actions act2 = new Actions(driver);
		act2.sendKeys(Keys.ARROW_DOWN).build().perform();
		act2.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// Below Code search for Model name then select brand value
		ActionHandler.setText(SVO_OpportunityContainer.model, model);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Actions act3 = new Actions(driver);
		act3.sendKeys(Keys.ARROW_DOWN).build().perform();
		act3.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
	}

	// Create a new Quote
	@Then("^Create a new Quote \"([^\"]*)\" for Private Office and save$")
	public void create_a_new_Quote_for_Private_Office_and_save(String quote) throws Throwable {
		double randomNumber = getRandomIntegerBetweenRange(0, 1000);
		ActionHandler.click(SVO_OpportunityContainer.NewQuote);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(SVO_OpportunityContainer.quoteName, quote + randomNumber);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		// To enter pricing details
		String Price = "100";
		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(5);
		ActionHandler.setText(SVO_OpportunityContainer.QuoteCP, Price);
		ActionHandler.wait(3);
		ActionHandler.setText(SVO_OpportunityContainer.QuoteLP, Price);
		ActionHandler.wait(3);
		ActionHandler.setText(SVO_OpportunityContainer.QuoteContribution, Price);
		ActionHandler.wait(3);
		ActionHandler.setText(SVO_OpportunityContainer.QuoteRP, Price);
		ActionHandler.wait(3);
		ActionHandler.setText(SVO_OpportunityContainer.QuoteRevenue, Price);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// Save the quote
		ActionHandler.click(SVO_OpportunityContainer.SaveQuote);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.selectQuote);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User creates a new quote");
	}

	// Mark Status as Complete
	@And("^Mark Status as Complete$")
	public void Mark_Status_as_Complete() throws Throwable {
		ActionHandler.click(SVO_OpportunityContainer.markStatusAsComplete);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Mark stage as complete button");
	}

	// verify the outcome details
	@Then("^Click on Edit and verify the outcome details cannot be changed$")
	public void click_on_Edit_and_verify_the_outcome_details_cannot_be_changed() throws Throwable {
		ActionHandler.click(SVO_OpportunityContainer.EditOutcome);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();

		// user verifies outcome details cannot be changed
		ActionHandler.click(SVO_OpportunityContainer.CloseWindow);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User cannot edit the outcome details");
	}

	// Click on Submit
	@Then("^Click on Submit$")
	public void click_on_Submit() throws Throwable {
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.SubmitDesign);
		ActionHandler.wait(5);

		// user submits the design
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.SaveSubmit);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Design brief submitted");
	}

	// go to Return Render pack
	@Then("^Click on Return Render pack and navigate back to Quote$")
	public void click_on_Return_Render_pack_and_navigate_back_to_Quote() throws Throwable {
		ActionHandler.click(SVO_OpportunityContainer.DesignReturned);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// user changes the design brief stage as Returned
		ActionHandler.click(SVO_OpportunityContainer.SaveQuote);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.QuoteSelect);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user clicks on return render pack");
	}

	// Mark the Quote stage as Price
	@Then("^Mark the Quote stage as Price$")
	public void mark_the_Quote_stage_as_Price() throws Throwable {
		ActionHandler.click(SVO_OpportunityContainer.MarkStatusAsComplete);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user clicks on mark status as complete");
	}

	// Click on Send Quote Pack
	@Then("^Click on Send Quote Pack$")
	public void click_on_Send_Quote_Pack() throws Throwable {
		ActionHandler.click(SVO_OpportunityContainer.SendQuotePack);
		ActionHandler.wait(15);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user clicks on send quote pack");
	}

	// Select a document to e-sign
	@Then("^Select a document and move to next step to E-Sign documents in person and verify the error$")
	public void select_a_document_and_move_to_next_step_to_ESign_documents_in_person_and_verify_the_error()
			throws Throwable {
		driver.switchTo().frame(0);
		ActionHandler.wait(5);

		// user selects a file
		ActionHandler.click(SVO_OpportunityContainer.selectFile);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on a file");
		CommonFunctions.attachScreenshot();

		// user moves to next page
		ActionHandler.click(SVO_OpportunityContainer.Nextstep);
		ActionHandler.wait(10);
		Reporter.addStepLog("User clicks on next step");
		CommonFunctions.attachScreenshot();
		driver.switchTo().parentFrame();

		// user clicks on Esign document
		if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.ESignDoc)) {
			driver.switchTo().frame(0);
			ActionHandler.wait(10);
			ActionHandler.click(SVO_OpportunityContainer.ESignDoc);
			ActionHandler.wait(15);
			Reporter.addStepLog("User clicks the E sign Document");
			CommonFunctions.attachScreenshot();
			VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.ESignError);
			driver.switchTo().parentFrame();
		}
	}

	// validate error on not selecting a document
	@Then("^Click on next step without selecting a document and verify the error$")
	public void click_on_next_step_without_selecting_a_document_and_verify_the_error() throws Throwable {
		driver.switchTo().frame(0);
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.Nextstep);
		ActionHandler.wait(10);
		Reporter.addStepLog("User clicks on next step");
		CommonFunctions.attachScreenshot();
		// user verifies the error after clicking on esign
		VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.ESignError);
		driver.switchTo().parentFrame();
	}

	// Navigate to Stage History
	@Then("^Navigate to Stage History and verify user cannot edit the stage history$")
	public void navigate_to_Stage_History_and_verify_user_cannot_edit_the_stage_history() throws Throwable {
		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.ShowAllBtn);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// user navigates to stage history page
		ActionHandler.click(SVO_OpportunityContainer.StageHistory);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.BackToOpp);
		ActionHandler.wait(10);
		Reporter.addStepLog("User cannot edit the stage history");
	}

	// Navigate to Opportunity field history
	@Then("^Navigate to Opportunity field history and verify user cannot edit the Opportunity field history$")
	public void navigate_to_Opportunity_field_history_and_verify_user_cannot_edit_it() throws Throwable {
		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.ShowAllBtn);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// user navigates to opportunity field history page
		ActionHandler.click(SVO_OpportunityContainer.OppFieldHistory);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User cannot edit the stage history");
	}

	@Then("^User searches for \"([^\"]*)\" opportunities from select list view$")
	public void user_searches_for_opportunities_from_select_list_view(String arg1) throws Throwable {

		String s[] = arg1.split(",");
		arg1 = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		// user searches for opportunities list to display//
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.SelectListView);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user searches for opportunities list to display");

		// user set text in filter text box//
		ActionHandler.wait(7);
		ActionHandler.setText(SVO_OpportunityContainer.SearchListTextBox, arg1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user set text in filter text box");

		ActionHandler.wait(5);
		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		Reporter.addStepLog("User chooses to view selected type opportunities");
		CommonFunctions.attachScreenshot();
	}

	// click on Programme text box
	@When("^click on Programme text box$")
	public void click_on_Programme_text_box() throws Throwable {

		// scroll to view programme
		ActionHandler.wait(5);
		ActionHandler.scrollToView(SVO_OpportunityContainer.VehicleRequestDetail);

		// click on Programme text box
		ActionHandler.wait(5);
		ActionHandler.setText(SVO_OpportunityContainer.ContinuationPrgm, "Programme");
		ActionHandler.wait(5);
		ActionHandler.clearText(SVO_OpportunityContainer.ContinuationPrgm);
		ActionHandler.wait(3);
		Reporter.addStepLog("Click on Programme Text box");
		CommonFunctions.attachScreenshot();

	}

	// Verify that New Record button is not available
	@Then("^Verify that New Record button is not available$")
	public void verify_that_New_Record_button_is_not_available() throws Throwable {
		ActionHandler.wait(2);
		if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.NewProgrammeRecord)) {
			Reporter.addStepLog("New Record button is available");
			CommonFunctions.attachScreenshot();
		} else {
			Reporter.addStepLog("New Record button is unavailable");
			CommonFunctions.attachScreenshot();
		}
		ActionHandler.wait(4);
		ActionHandler.click(SVO_OpportunityContainer.Cancel);
		ActionHandler.wait(4);
	}

	// User click on S-Docs button
	@When("^User click on S-Docs button$")
	public void user_click_on_S_Docs_button() throws Throwable {

		// User click on S-Docs button
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.sendSalesAgreement);
		Reporter.addStepLog("User click on S-Docs button");
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);

	}

	// Verify that New Record button is not available
	@Then("^Verify the error message insufficient Privileges$")
	public void verify_the_error_message_insufficient_Privileges() throws Throwable {
		ActionHandler.wait(3);
		driver.switchTo().frame(0);
		ActionHandler.wait(10);

		if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.InsufficientPrivileges)) {
			Reporter.addStepLog("Error message caught");
			CommonFunctions.attachScreenshot();
		} else {
			Reporter.addStepLog("Error message dint caught");
			CommonFunctions.attachScreenshot();
		}

	}

	// SVO Bespoke Record Type opportunity option not displayed for classic bespoke
	// user
	@Then("^Click on New opportunity button and verify user cannot create SVO Bespoke Record Type opportunity$")
	public void Click_on_New_opportunity_button_and_verify_user_cannot_create_SVO_Bespoke_Record_Type_opportunity()
			throws Throwable {
		ActionHandler.wait(4);
		ActionHandler.click(SVO_OpportunityContainer.newOpportunityBtn);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verify SVO bespoke opportunity option is not displayed");
		ActionHandler.click(SVO_OpportunityContainer.CancelOpp);
		ActionHandler.wait(5);
	}

	// navigate to opportunity
	@Then("^Navigate back to Opportunity$")
	public void Navigate_back_to_Opportunity() throws Throwable {
		ActionHandler.pageScrollDown();
		ActionHandler.wait(4);
		ActionHandler.click(SVO_OpportunityContainer.OpenOpp);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user navigates back to opportunity");

	}

	// Verify the Delete button
	@Then("^Verify the Delete button$")
	public void verify_the_Delete_button() throws Throwable {

		ActionHandler.wait(5);
		if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.OppDelete)) {
			ActionHandler.wait(3);
			Reporter.addStepLog("User is not able to delete the opportunity as delete button is unavailble");
			CommonFunctions.attachScreenshot();
		} else {

			ActionHandler.wait(3);
			Reporter.addStepLog("User is able to find delete button to delete the opportunity");
			CommonFunctions.attachScreenshot();
		}
	}

	@When("^User click on Veicle number under donor details$")
	public void user_click_on_Veicle_number_under_donor_details() throws Throwable {
		// refresh the page
		ActionHandler.wait(5);
		driver.navigate().refresh();
		ActionHandler.wait(15);

		// scroll to view donor details
		// javaScriptUtil.scrollIntoView(SVO_OpportunityContainer.donorDetails);
		for (int i = 0; i < 10; i++) {
			ActionHandler.scrollDown();
		}
		ActionHandler.wait(5);
		Reporter.addStepLog("scroll to view donor details");
		CommonFunctions.attachScreenshot();

		// click on Vehicle number
		ActionHandler.click(SVO_OpportunityContainer.VehicleNumber);
		ActionHandler.wait(5);
		Reporter.addStepLog("click on Vehicle number");
		CommonFunctions.attachScreenshot();
	}

	@When("^User navigate to Related Tab$")
	public void user_navigate_to_Related_Tab() throws Throwable {
		// User navigate to Related Tab
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.RelatedTab);
		ActionHandler.wait(5);
		Reporter.addStepLog("User navigate to Related Tab");
		CommonFunctions.attachScreenshot();

	}

	@Then("^create an new enquiry with Record type \"([^\"]*)\"$")
	public void create_an_new_enquiry_with_Record_type(String RecordType) throws Throwable {

		String Rtype[] = RecordType.split(",");
		RecordType = CommonFunctions.readExcelMasterData(Rtype[0], Rtype[1], Rtype[2]);

		// User click on New button of Enquiry
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.NewEnqBtn);
		ActionHandler.wait(5);
		Reporter.addStepLog("User click on New button of Enquiry");
		CommonFunctions.attachScreenshot();

		// user selects Record Type of an Enquiry
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.RTypeSelection(RecordType))));
		ActionHandler.wait(2);
		Reporter.addStepLog("User selects Record Type of an Enquiry");
		CommonFunctions.attachScreenshot();

		// user click on Next Button
		ActionHandler.click(SVO_OpportunityContainer.Next);
		ActionHandler.wait(5);
		Reporter.addStepLog("User click on Next button of Enquiry");
		CommonFunctions.attachScreenshot();
	}

	@Then("^User enters Enquiry title \"([^\"]*)\" and click on save$")
	public void user_enters_Enquiry_title_and_click_on_save(String arg1) throws Throwable {

		String ET[] = arg1.split(",");
		arg1 = CommonFunctions.readExcelMasterData(ET[0], ET[1], ET[2]);

		double randomNumber = getRandomIntegerBetweenRange(0, 1000);

		// enter title of an enquiry
		ActionHandler.wait(3);
		ActionHandler.setText(SVO_OpportunityContainer.EnqTitle, arg1 + randomNumber);
		ActionHandler.wait(2);
		Reporter.addStepLog("enter title of an enquiry");
		CommonFunctions.attachScreenshot();

		// User click on Save button
		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.SaveOpportunity);
		ActionHandler.wait(5);
		Reporter.addStepLog("User click on Save button");
		CommonFunctions.attachScreenshot();
	}

	@Then("^Verify that new enquiry is linked to Opportunity$")
	public void verify_that_new_enquiry_is_linked_to_Opportunity() throws Throwable {
		ActionHandler.wait(2);
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.wait(2);
		if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.CreatedEnqTitle)) {
			Reporter.addStepLog("new enquiry is linked to Opportunity");
			CommonFunctions.attachScreenshot();
		} else {
			Reporter.addStepLog("new enquiry is not linked to Opportunity");
			CommonFunctions.attachScreenshot();
		}

	}

	// navigate to donor details
	@Then("^Navigate to Vehicle field and click on edit$")
	public void Navigate_to_vehicle_field_and_click_on_edit() throws Throwable {
		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.EditVehicle);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.ClearSelection);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.vehicle);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user navigates to vehicle field under donor details");
	}

	// edit the vehicle field
	@Then("^User adds new \"([^\"]*)\" record$")
	public void user_adds_new_record(String recordType) throws Throwable {
		ActionHandler.click(SVO_OpportunityContainer.VehicleNewRecord);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.OpportunityRecordType(recordType))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.nextRType);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user adds new record");
	}

	// enter the mandatory fields except VIN
	@Then("^Enter fields Record name \"([^\"]*)\" Brand \"([^\"]*)\" model \"([^\"]*)\" and save leaving the VIN field blank$")
	public void Enter_fields_Record_name_Brand_model_and_save_leaving_the_VIN_field_blank(String name, String Brand,
			String Model) throws Throwable {
		String Rec[] = name.split(",");
		name = CommonFunctions.readExcelMasterData(Rec[0], Rec[1], Rec[2]);

		String Br[] = Brand.split(",");
		Brand = CommonFunctions.readExcelMasterData(Br[0], Br[1], Br[2]);

		String Mod[] = Model.split(",");
		Model = CommonFunctions.readExcelMasterData(Mod[0], Mod[1], Mod[2]);

		ActionHandler.setText(SVO_OpportunityContainer.RecordName, name);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVO_OpportunityContainer.brand, Brand);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Input Brand information");

		// enter model details
		ActionHandler.setText(SVO_OpportunityContainer.model, Model);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Input Model information");

		ActionHandler.click(SVO_OpportunityContainer.SaveRelation);
		ActionHandler.wait(7);
		Reporter.addStepLog("Click on the Save button");
		CommonFunctions.attachScreenshot();
	}

	// verify the error message displayed
	@Then("^Verify the error message displayed for VIN field$")
	public void Verify_the_error_message_displayed_for_VIN_field() throws Throwable {
		VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.VINError);
		ActionHandler.click(SVO_OpportunityContainer.CancelVehicle);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.Cancel);
		ActionHandler.wait(4);
		Reporter.addStepLog("user verifies the error message");
	}

	// search for classic bespoke opportunity
	@When("^User searches for Classic bespoke opportunity and selects it$")
	public void user_searches_for_Classic_bespoke_opportunity_and_selects_it() throws Throwable {
		ActionHandler.click(SVO_OpportunityContainer.selectlistview);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(4);
		Reporter.addStepLog("User is able to navigate to the list view");
		ActionHandler.click(SVO_OpportunityContainer.ClassicBespoke);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User is able to list all Classic bespoke Opportunities");

		// user searches for opportunity and opens it
		ActionHandler.click(SVO_OpportunityContainer.OpportunitySel1);
		ActionHandler.wait(10);
		Reporter.addStepLog("Select opportunity");
		CommonFunctions.attachScreenshot();
	}

	@Then("^Mark the Quote status as Outcome$")
	public void mark_the_Quote_status_as_Outcome() throws Throwable {
		ActionHandler.click(SVO_OpportunityContainer.OutcomeTab);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.MarkAsCurrentStatus);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.Outcome);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.OutcomeAccept);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.OutcomeDetail);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.OutcomeDetAccept);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.Donebutton);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Stage marked as Outcome");
	}

	@Then("^User deletes the order$")
	public void User_deletes_the_order() throws Throwable {
		ActionHandler.click(SVO_OpportunityContainer.orderDrpodownBtn);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.QuoteDelete);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityContainer.OppDeleteConfirm);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User deletes the order");
	}

	// Navigate to Tasks tab
	@Then("^Navigate to Tasks tab$")
	public void navigate_to_Tasks_tab() throws Throwable {

		ActionHandler.wait(5);
		javaScriptUtil.clickElementByJS(SVO_OpportunityContainer.TasksTab);
		Reporter.addStepLog("User Navigate to Tasks tab");
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

	}

	// Select Tasks from select list view
	@Then("^Select \"([^\"]*)\" Tasks from select list view$")
	public void select_Tasks_from_select_list_view(String arg1) throws Throwable {

		String Rec[] = arg1.split(",");
		arg1 = CommonFunctions.readExcelMasterData(Rec[0], Rec[1], Rec[2]);

		// user click on select list view
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.SelectListView);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on select list view");

		// User selects Recently completed tasks from the list view
		ActionHandler.wait(5);
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.RTypeSelection(arg1))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects tasks from the list view");

	}

	// User select first task from the list
	@When("^User select first task from the list$")
	public void user_select_first_task_from_the_list() throws Throwable {

		// User select first task from the list
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.FirstTask);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User select first task from the list");
	}

	@When("^User change the Due Date of task to \"([^\"]*)\"$")
	public void user_change_the_Due_Date_of_task_to(String date) throws Throwable {

		String d[] = date.split(",");
		date = CommonFunctions.readExcelMasterData(d[0], d[1], d[2]);

		// click o drop down list
		ActionHandler.wait(3);
		javaScriptUtil.scrollIntoView(SVO_OpportunityContainer.EditDropDown);
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.EditDropDown);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click o drop down list");

		// User select change date option from the list
		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.ChangeDate);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User select change date option from the list");

		// Enter the due date
		ActionHandler.wait(5);
		ActionHandler.setText(SVO_OpportunityContainer.ChangeDateTxtBx, date);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter the due date");

		// click o save button
		ActionHandler.click(SVO_OpportunityContainer.SaveShare);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("click o save button");

	}

	// User Edit the selected task
	@Then("^User Edit the selected task$")
	public void user_Edit_the_selected_task() throws Throwable {

		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.EditTask);
		Reporter.addStepLog("User click on Edit button to edit the selected task");
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

	}

	// Verify the Access denied error message
	@And("^Verify the Access denied error message$")
	public void verify_the_Access_denied_error_message() throws Throwable {
		ActionHandler.wait(4);
		if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.ErrorMsg)) {
			Reporter.addStepLog("Error message validated");
			CommonFunctions.attachScreenshot();
		} else {
			Reporter.addStepLog("No error message was caught");
			CommonFunctions.attachScreenshot();
		}
		ActionHandler.wait(4);
		ActionHandler.click(SVO_OpportunityContainer.CancelError);
		ActionHandler.wait(4);
	}

	// User assign the Task to user
	@And("^User assign the Task to \"([^\"]*)\"$")
	public void user_assign_the_Task_to(String User) throws Throwable {

		String U[] = User.split(",");
		User = CommonFunctions.readExcelMasterData(U[0], U[1], U[2]);

		ActionHandler.wait(3);
		driver.navigate().refresh();
		ActionHandler.wait(15);

		// click on Edit assign to pencil icon
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.EditAssignTo);
		ActionHandler.wait(3);
		Reporter.addStepLog("click on Edit assign to pencil icon");
		CommonFunctions.attachScreenshot();

		// click on clear icon of user
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.ClearAssignTo);
		ActionHandler.wait(3);
		Reporter.addStepLog("click on clear assign to icon");
		CommonFunctions.attachScreenshot();

		// enter user into Assign To text box
		ActionHandler.wait(5);
		ActionHandler.setText(SVO_OpportunityContainer.AssignTo, User);
		ActionHandler.wait(3);
		Actions act2 = new Actions(driver);
		act2.sendKeys(Keys.ARROW_DOWN).build().perform();
		act2.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(3);
		Reporter.addStepLog("click on clear assign to icon");
		CommonFunctions.attachScreenshot();

		// click on save button
		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.SaveRPS);
		ActionHandler.wait(3);
		Reporter.addStepLog("click on save button");
		CommonFunctions.attachScreenshot();
	}

	// User Mark task as completed
	@Then("^User Mark task as completed$")
	public void user_Mark_task_as_completed() throws Throwable {

		// User click on Mark task as completed button
		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.MarkComplete);
		ActionHandler.wait(3);
		Reporter.addStepLog("User click on Mark task as completed button");
		CommonFunctions.attachScreenshot();
	}

	@And("^User tries to change the priority of completed task$")
	public void User_tries_to_change_priority_completed_task() throws Exception {
		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.EditTask);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.taskPriority);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.highTaskPriority);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.editTaskSave);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User changes priority of completed task");
	}

	// User delete the selected Task
	@When("^User delete the selected Task$")
	public void user_delete_the_selected_Task() throws Throwable {

		ActionHandler.wait(3);
		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(3);

		// click on delete button
		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.DeleteTask);
		ActionHandler.wait(3);
		Reporter.addStepLog("click on delete button");
		CommonFunctions.attachScreenshot();

		// click on delete pop up button
		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.ConfDeleteVehicle);
		ActionHandler.wait(3);
		Reporter.addStepLog("click on delete pop up button");
		CommonFunctions.attachScreenshot();
	}

	// Verify the Access denied error message to delete the Task
	@And("^Verify the Access denied error message to delete the Task$")
	public void verify_the_Access_denied_error_message_to_delete_the_Task() throws Throwable {
		ActionHandler.wait(4);
		if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.ErrorNote)) {
			Reporter.addStepLog("Error message validated");
			CommonFunctions.attachScreenshot();
		} else {
			Reporter.addStepLog("No error message was caught");
			CommonFunctions.attachScreenshot();
		}
		ActionHandler.wait(4);
		ActionHandler.click(SVO_OpportunityContainer.CancelIcon);
		ActionHandler.wait(4);
	}

	// User selects New option to create new task
	@And("^User selects New option to create new task$")
	public void user_selects_New_option_to_create_new_task() throws Throwable {

		// click on New Task drop down list
		ActionHandler.wait(5);
		ActionHandler.click(SVO_OpportunityContainer.NewDropDown);
		ActionHandler.wait(3);
		Reporter.addStepLog("click on New Task drop down list");
		CommonFunctions.attachScreenshot();

		// click on New Task Button
		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.NewTask);
		ActionHandler.wait(3);
		Reporter.addStepLog("click on New Task Button");
		CommonFunctions.attachScreenshot();

	}

	// fill all mandatory fields of Task with subject and click on Save
	@Then("^fill all mandatory fields of Task with subject \"([^\"]*)\" and click on Save$")
	public void fill_all_mandatory_fields_of_Task_with_subject_and_click_on_Save(String Subject) throws Throwable {

		String S[] = Subject.split(",");
		Subject = CommonFunctions.readExcelMasterData(S[0], S[1], S[2]);

		double randomNumber = getRandomIntegerBetweenRange(0, 1000);

		// user set text in subject text box
		ActionHandler.wait(5);
		ActionHandler.setText(SVO_OpportunityContainer.TaskSubject, Subject + randomNumber);
		ActionHandler.wait(3);
		Reporter.addStepLog("click on New Task Button");
		CommonFunctions.attachScreenshot();

		// click on save button
		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.SaveRPS);
		ActionHandler.wait(3);
		Reporter.addStepLog("click on save button");
		CommonFunctions.attachScreenshot();
	}

	@Then("^Navigate to Campaign tab \"([^\"]*)\"$")
	public void navigate_to_Campaign_tab(String Campaign) throws Throwable {

		String C[] = Campaign.split(",");
		Campaign = CommonFunctions.readExcelMasterData(C[0], C[1], C[2]);

		// User selects Campaign tab from Menu//
		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.Menu);
		ActionHandler.wait(5);
		Reporter.addStepLog("User click on Menu");
		CommonFunctions.attachScreenshot();

		ActionHandler.clearAndSetText(SVO_OpportunityContainer.SearchBar, Campaign);
		ActionHandler.wait(5);
		Reporter.addStepLog("User searched for Campaigns and select it");
		javaScriptUtil.clickElementByJS(SVO_OpportunityContainer.SVO);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

	}

	// Verify that list of Campaigns displayed
	@Then("^Verify that list of Campaigns displayed$")
	public void verify_that_list_of_Campaigns_displayed() throws Throwable {
		ActionHandler.wait(4);
		if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.SelectListView)) {
			Reporter.addStepLog("List of Recently viewd campaigns are displayed");
			CommonFunctions.attachScreenshot();
		} else {
			Reporter.addStepLog("List of Recently viewd campaigns were not displayed");
			CommonFunctions.attachScreenshot();
		}

	}

	@Then("^User create new campaign with Campaign name \"([^\"]*)\"$")
	public void user_create_new_campaign_with_Campaign_name(String Name) throws Throwable {

		String C[] = Name.split(",");
		Name = CommonFunctions.readExcelMasterData(C[0], C[1], C[2]);

		double randomNumber = getRandomIntegerBetweenRange(0, 1000);

		// click on new button to create new campaign
		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.NewCampaign);
		ActionHandler.wait(3);
		Reporter.addStepLog("click on new button to create new campaign");
		CommonFunctions.attachScreenshot();

		// enter text in campaign text box
		ActionHandler.wait(2);
		ActionHandler.setText(SVO_OpportunityContainer.CampaignName, Name + randomNumber);
		ActionHandler.wait(3);
		Reporter.addStepLog("User enters text in campaign text box");
		CommonFunctions.attachScreenshot();

		// User click on Save button
		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.SaveRelation);
		ActionHandler.wait(3);
		Reporter.addStepLog("User click on Save button");
		CommonFunctions.attachScreenshot();
	}

	// Verify that details of Campaign are displayed
	@Then("^Verify that details of Campaign are displayed$")
	public void verify_that_details_of_Campaign_are_displayed() throws Throwable {

		ActionHandler.wait(4);
		if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.CampaignDetails)) {
			Reporter.addStepLog("details of Campaign are displayed");
			CommonFunctions.attachScreenshot();
		} else {
			Reporter.addStepLog("details of Campaign are not displayed");
			CommonFunctions.attachScreenshot();
		}

	}

	@When("^User add a Parent Campaign \"([^\"]*)\"$")
	public void user_add_a_Parent_Campaign(String arg1) throws Throwable {

		String C[] = arg1.split(",");
		arg1 = CommonFunctions.readExcelMasterData(C[0], C[1], C[2]);

		ActionHandler.wait(3);
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();

		// click on Edit Parent Campaign pencil icon
		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.EditParentCampaign);
		ActionHandler.wait(5);
		Reporter.addStepLog("click on Edit Parent Campaign pencil icon");
		CommonFunctions.attachScreenshot();

		// selects Parent Campaign by adding partial text
		ActionHandler.wait(3);
		ActionHandler.setText(SVO_OpportunityContainer.SearchParentCampaign, arg1);
		ActionHandler.wait(4);
		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);
		Reporter.addStepLog("selects Parent Campaign by adding partial text");
		CommonFunctions.attachScreenshot();

		// User click on Save button
		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.SaveRelation);
		ActionHandler.wait(3);
		Reporter.addStepLog("User click on Save button");
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(2);
	}

	// User navigate to log a call tab
	@When("^User navigate to log a call tab$")
	public void user_navigate_to_log_a_call_tab() throws Throwable {

		// User navigate to log a call tab
		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.LogACallTab);
		ActionHandler.wait(3);
		Reporter.addStepLog("User navigate to log a call tab");
		CommonFunctions.attachScreenshot();

	}

	@Then("^User log a call for Campaign with Subject \"([^\"]*)\"$")
	public void user_log_a_call_for_Campaign_with_Subject(String arg1) throws Throwable {

		String C[] = arg1.split(",");
		arg1 = CommonFunctions.readExcelMasterData(C[0], C[1], C[2]);

		// click on Add button to log a call
		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.AddLogBtn);
		ActionHandler.wait(3);
		Reporter.addStepLog("click on Add button to log a call");
		CommonFunctions.attachScreenshot();

		// User set text in Subject text box of a log
		ActionHandler.wait(3);
		ActionHandler.clearAndSetText(SVO_OpportunityContainer.SubjectLogTxtBx, arg1);
		ActionHandler.wait(5);
		Reporter.addStepLog("User set text in Subject text box of a log");
		CommonFunctions.attachScreenshot();

		// User click on Save button
		ActionHandler.wait(5);
		javaScriptUtil.clickElementByJS(SVO_OpportunityContainer.SaveShare);
		ActionHandler.wait(3);
		Reporter.addStepLog("user click on Save button");
		CommonFunctions.attachScreenshot();

	}

	@When("^User navigate to New Event tab$")
	public void user_navigate_to_New_Event_tab() throws Throwable {

		ActionHandler.wait(3);
		driver.navigate().refresh();
		ActionHandler.wait(15);

		// User navigate to New Event tab
		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.NewEventTab);
		ActionHandler.wait(3);
		Reporter.addStepLog("User navigate to New Event tab");
		CommonFunctions.attachScreenshot();

	}

	@Then("^User create New Event with Subject \"([^\"]*)\"$")
	public void user_create_New_Event_with_Subject(String arg1) throws Throwable {

		String C[] = arg1.split(",");
		arg1 = CommonFunctions.readExcelMasterData(C[0], C[1], C[2]);

		// User set text in Subject text box of Event
		ActionHandler.wait(3);
		ActionHandler.setText(SVO_OpportunityContainer.SubjectEventTxtBx, arg1);
		ActionHandler.wait(5);
		Reporter.addStepLog("User set text in Subject text box of Event");
		CommonFunctions.attachScreenshot();

		// User click on Save button
		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(SVO_OpportunityContainer.SaveShare);
		ActionHandler.wait(3);
		Reporter.addStepLog("user click on Save button");
		CommonFunctions.attachScreenshot();

	}

	@Then("^User navigate to New Task tab$")
	public void user_navigate_to_New_Task_tab() throws Throwable {

		ActionHandler.wait(3);
		driver.navigate().refresh();
		ActionHandler.wait(15);

		// User navigate to New Task tab
		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.TaskTab);
		ActionHandler.wait(3);
		Reporter.addStepLog("User navigate to New Task tab");
		CommonFunctions.attachScreenshot();
	}

	@Then("^User create new task with Subject \"([^\"]*)\"$")
	public void user_create_new_task_with_Subject(String arg1) throws Throwable {

		String C[] = arg1.split(",");
		arg1 = CommonFunctions.readExcelMasterData(C[0], C[1], C[2]);

		// User set text in Subject text box of Task
		ActionHandler.wait(3);
		ActionHandler.setText(SVO_OpportunityContainer.SubjectEventTxtBx, arg1);
		ActionHandler.wait(5);
		Reporter.addStepLog("User set text in Subject text box of Task");
		CommonFunctions.attachScreenshot();

		// User click on Save button
		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(SVO_OpportunityContainer.SaveShare);
		ActionHandler.wait(3);
		Reporter.addStepLog("user click on Save button");
		CommonFunctions.attachScreenshot();

	}

	@When("^User navigate to More tab and click on Email$")
	public void user_navigate_to_More_tab_and_click_on_Email() throws Throwable {

		ActionHandler.wait(3);
		driver.navigate().refresh();
		ActionHandler.wait(15);

		// User navigate to More tab
		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.MoreTab);
		ActionHandler.wait(3);
		Reporter.addStepLog("User navigate to More tab");
		CommonFunctions.attachScreenshot();

		// User click on Email button
		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.EmailTab);
		ActionHandler.wait(3);
		Reporter.addStepLog("User click on Email button");
		CommonFunctions.attachScreenshot();

	}

	@Then("^User send an Email to \"([^\"]*)\" with subject \"([^\"]*)\" and body \"([^\"]*)\"$")
	public void user_send_an_Email_to_with_subject_and_body(String To, String subject, String body) throws Throwable {

		String to[] = To.split(",");
		To = CommonFunctions.readExcelMasterData(to[0], to[1], to[2]);

		String S[] = subject.split(",");
		subject = CommonFunctions.readExcelMasterData(S[0], S[1], S[2]);

		String B[] = body.split(",");
		body = CommonFunctions.readExcelMasterData(B[0], B[1], B[2]);

		// User set user in To section of an email
		ActionHandler.wait(3);
		ActionHandler.setText(SVO_OpportunityContainer.EmailTo, To);
		ActionHandler.pressEnter();
		ActionHandler.wait(5);
		Reporter.addStepLog("User set user in To section of an email");
		CommonFunctions.attachScreenshot();

		// User set text in Subject text box of an email
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.wait(3);
		ActionHandler.setText(SVO_OpportunityContainer.EmailSubject, subject);
		ActionHandler.wait(2);
		Reporter.addStepLog("User set text in Subject text box of an email");
		CommonFunctions.attachScreenshot();

		// User click on Save button
		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(SVO_OpportunityContainer.SendEmail);
		ActionHandler.wait(3);
		Reporter.addStepLog("user click on Save button");
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(3);
	}

}
