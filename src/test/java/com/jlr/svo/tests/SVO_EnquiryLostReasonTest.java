package com.jlr.svo.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.containers.SVOAccountsContainer;
import com.jlr.svo.containers.SVOAdditionalVehicleContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.containers.SVOItemToApproveContainer;
import com.jlr.svo.containers.SVOOpportunityContainer;
import com.jlr.svo.containers.SVO_EnquiryLostReasonContainer;
import com.jlr.svo.containers.SVO_Pricing_LogicContainer;
import com.jlr.svo.containers.SVO_RestrictedPartyScreeningContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.Then;

public class SVO_EnquiryLostReasonTest extends TestBaseCC {
	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory.getLogger(SVO_EnquiryLostReasonTest.class.getName());
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);

	SVOAccountsContainer SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
	SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	SVOAdditionalVehicleContainer SVOAdditionalvehicleContainer = PageFactory.initElements(driver,
			SVOAdditionalVehicleContainer.class);
	SVO_Pricing_LogicContainer SVO_Pricing_LogicContainer = PageFactory.initElements(driver,
			SVO_Pricing_LogicContainer.class);
	SVO_RestrictedPartyScreeningContainer RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
			SVO_RestrictedPartyScreeningContainer.class);
	SVOItemToApproveContainer SVOItemToapproveContainer = PageFactory.initElements(driver,
			SVOItemToApproveContainer.class);
	SVO_EnquiryLostReasonContainer SVO_EnquiryLostReasonContainer = PageFactory.initElements(driver,
			SVO_EnquiryLostReasonContainer.class);

	public static String verificationCode;
	public static String vCode;
	public static String veriCode;
	public static String EnquiryName;
	public static String SLAStatus;

	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	@Then("^User navigates to the Enquiries tab$")
	public void user_navigates_to_the_Enquiries_tab() throws Throwable {

		javaScriptUtil.clickElementByJS(SVO_EnquiryLostReasonContainer.EnquiriesTab);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigates to Enquiries tab");

	}

	@Then("^User creates an Enquiry with record type \"([^\"]*)\"$")
	public void user_creates_an_Enquiry_with_record_type(String RecType) throws Throwable {

		String b[] = RecType.split(",");
		RecType = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		double randomnumber = getRandomIntegerBetweenRange(0, 1000);

		ActionHandler.click(SVO_EnquiryLostReasonContainer.EnquiryNewButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks on to Enquiries new button");

		ActionHandler.wait(2);
		ActionHandler.click(driver.findElement(By.xpath(SVO_EnquiryLostReasonContainer.RecTypeSelection(RecType))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects the enquiry record type");

		ActionHandler.wait(2);
		ActionHandler.click(SVO_EnquiryLostReasonContainer.EnquiryNextButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks on to Enquiries next button");

		ActionHandler.setText(SVO_EnquiryLostReasonContainer.EnquiryTitleTextBox, RecType + randomnumber);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters enquiry title");

		ActionHandler.click(SVO_EnquiryLostReasonContainer.EnquirySaveButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks on to Enquiries Save button");

		VerifyHandler.verifyElementPresent(SVO_EnquiryLostReasonContainer.EnquiryName);

		EnquiryName = SVO_EnquiryLostReasonContainer.EnquiryName.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enquiry is created successfully with Enquiry name : " + EnquiryName);

	}

	@Then("^User verifies the SLA status of an created Enquiry$")
	public void user_verifies_the_SLA_status_of_an_created_Enquiry() throws Throwable {

		VerifyHandler.verifyElementPresent(SVO_EnquiryLostReasonContainer.SLAStatus);
		String EnquiryStatusField = SVO_EnquiryLostReasonContainer.EnquiryStatusField.getText();

		if (EnquiryStatusField.equalsIgnoreCase("New")) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("SLA status of an enquiry is displayes as In progress");
		} else if (EnquiryStatusField.equalsIgnoreCase("Contacted")) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("SLA status of an enquiry is displayes as Success");
		}

	}

	@Then("^Log a call for an enquiry activity$")
	public void log_a_call_for_an_enquiry_activity() throws Throwable {

		ActionHandler.wait(1);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);

		javaScriptUtil.clickElementByJS(SVO_EnquiryLostReasonContainer.EnquiryActivitytab);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to activity tab on enquiry page");

		//ActionHandler.wait(1);
		//ActionHandler.pageScrollDown();
		//ActionHandler.wait(2);

		javaScriptUtil.clickElementByJS(SVO_EnquiryLostReasonContainer.LogACallAddButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on add button to log a call on enquiry activity");

		ActionHandler.setText(SVO_EnquiryLostReasonContainer.LogACallSubjectTextBox, "Call");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters subject to log a call on enquiry activity");

		ActionHandler.wait(3);
		javaScriptUtil.clickElementByJS(SVO_EnquiryLostReasonContainer.EnquiryActivitySaveButton);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks on save button on activity tab");

		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(2);
		
		VerifyHandler.verifyElementPresent(SVO_EnquiryLostReasonContainer.EnquiryStatusField);
		String EnquiryStatusField = SVO_EnquiryLostReasonContainer.EnquiryStatusField.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User is able to view that enquiry status is moved to " + EnquiryStatusField + " stage automatically");
	}

	@Then("^User moves an enquiry to discovery stage by adding contact details \"([^\"]*)\"$")
	public void User_moves_an_enquiry_to_discovery_stage_by_adding_contact_details(String ContactName)
			throws Throwable {

		String b[] = ContactName.split(",");
		ContactName = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		ActionHandler.wait(1);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);

		ActionHandler.setText(SVO_EnquiryLostReasonContainer.EnquiryContactTextBox, ContactName);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters contact detail on enquiry page");

		ActionHandler.click(SVO_EnquiryLostReasonContainer.EnquiryContactSearch);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(ContactName))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects contact details on enquiry page");

		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVO_EnquiryLostReasonContainer.MarkEnquiryStatusAsCompleteBtn);
		ActionHandler.wait(6);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Move the enquiry stage to Discovery on enquiries page");
	}

	@Then("^User close the enquiry with closed state \"([^\"]*)\" and closed reason \"([^\"]*)\" on Enquiry page$")
	public void user_close_the_enquiry_with_closed_state_and_closed_reason_on_Enquiry_page(String state, String reason)
			throws Throwable {
		String s[] = state.split(",");
		state = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String r[] = reason.split(",");
		reason = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		javaScriptUtil.clickElementByJS(SVO_EnquiryLostReasonContainer.ClosedEnquiryStatusTab);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Closed enquiry tab");

		ActionHandler.click(SVO_EnquiryLostReasonContainer.MarkAsCurrentEnquiryStatusBtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Mark as current enquiry status button");

		ActionHandler.click(SVO_EnquiryLostReasonContainer.ClosedStateDropDownList);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Closed State drop down list");

		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.RPSSelection(state))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects closed state of an enquiry on enquiry page as: " + state);

		ActionHandler.click(SVO_EnquiryLostReasonContainer.ClosedReasonDropDownList);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Closed Reason drop down list");

		javaScriptUtil.clickElementByJS(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.RPSSelection(reason))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects closed reason of an enquiry on enquiry page as: " + reason);

		ActionHandler.click(SVO_EnquiryLostReasonContainer.EnquiryClosedDoneButton);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on enquiry Closed pop up window done button");

		Reporter.addStepLog("User close the enquiry with closed reason : " + reason);
	}

	@Then("^Verify the auto-populated fields of enqiry closed reason and closed state on Enquiry page$")
	public void verify_the_auto_populated_fields_of_enqiry_closed_reason_and_closed_state_on_Enquiry_page()
			throws Throwable {

		ActionHandler.wait(3);
		VerifyHandler.verifyElementPresent(SVO_EnquiryLostReasonContainer.EnquiryClosedState);
		String closedstate = SVO_EnquiryLostReasonContainer.EnquiryClosedState.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enquiry closed state is displayed as: " + closedstate + " on Enquiry page");

		VerifyHandler.verifyElementPresent(SVO_EnquiryLostReasonContainer.EnquiryClosedReason);
		String EnquiryClosedReason = SVO_EnquiryLostReasonContainer.EnquiryClosedReason.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enquiry closed reason is displayed as: " + EnquiryClosedReason + " on Enquiry page");

	}

	@Then("^User edit the closed reason of an enquiry to \"([^\"]*)\"$")
	public void user_edit_the_closed_reason_of_an_enquiry_to(String EnquiryClosedReason) throws Throwable {

		String s[] = EnquiryClosedReason.split(",");
		EnquiryClosedReason = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(SVO_EnquiryLostReasonContainer.EditEnquiryClosedreasonBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Edit Enquiry closed reason button");

		ActionHandler.wait(2);
		ActionHandler.pageUp();
		ActionHandler.wait(2);
		ActionHandler.click(SVO_EnquiryLostReasonContainer.EditClosedReasonDropDownList);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Closed Reason drop down list");

		Actions act3 = new Actions(driver);
		act3.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(2);
		act3.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(2);
		act3.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(2);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects closed reason of an enquiry on enquiry page as: " + EnquiryClosedReason);

		ActionHandler.click(SVO_EnquiryLostReasonContainer.EditEnquirySaveBtn);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Save button");
		Reporter.addStepLog("User update the closed reason of an closed lost enquiry");
	}

	@Then("^User tries to re-open the enquiry status \"([^\"]*)\"$")
	public void User_tries_to_re_open_the_enquiry_status(String EnquiryStatus) throws Throwable {

		String s[] = EnquiryStatus.split(",");
		EnquiryStatus = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		ActionHandler.wait(2);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);

		javaScriptUtil.clickElementByJS(SVO_EnquiryLostReasonContainer.EditEnquiryStatusBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Edit Enquiry status button");

		ActionHandler.wait(2);
		ActionHandler.click(SVO_EnquiryLostReasonContainer.EditEnquiryStatusDropDownlist);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Enquiry status drop down list");

		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(
				driver.findElement(By.xpath(SVO_EnquiryLostReasonContainer.EnquiryStatusSelection(EnquiryStatus))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects closed reason of an enquiry on enquiry page as: " + EnquiryStatus);

		ActionHandler.click(SVO_EnquiryLostReasonContainer.EnquirySaveButton);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Save button");
		Reporter.addStepLog("User update the Enquiry status to " + EnquiryStatus);
	}

	@Then("^Verify the error message displayed on Enquiry page$")
	public void verify_the_error_message_displayed_on_Enquiry_page() throws Throwable {

		VerifyHandler.verifyElementPresent(SVO_EnquiryLostReasonContainer.EnquiryStatusReopenErrorMsg);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("An error message displayed stating 'closed enquiry cannot be reopened'");

		ActionHandler.click(SVO_EnquiryLostReasonContainer.CancelEnquiryBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on cancel button");
	}

	@Then("^User Edit the Enquiry title for an closed lost enquiry$")
	public void user_Edit_the_Enquiry_title_for_an_closed_lost_enquiry() throws Throwable {

		ActionHandler.click(SVO_EnquiryLostReasonContainer.EditEnquiryBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Edit Enquiry button");

		ActionHandler.clearAndSetText(SVO_EnquiryLostReasonContainer.EnquiryTitleTextBox, "Edited Enquiry Title");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Edits the enquiry title");

		ActionHandler.click(SVO_EnquiryLostReasonContainer.EnquirySaveButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks on to Enquiries Save button");

		ActionHandler.wait(3);
		Reporter.addStepLog("User Edit the Enquiry title for an closed lost enquiry");

	}

	@Then("^Verify that the enquiry title is updated with the edited value on enquiry page$")
	public void verify_that_the_enquiry_title_is_updated_with_the_edited_value_on_enquiry_page() throws Throwable {

		VerifyHandler.verifyElementPresent(SVO_EnquiryLostReasonContainer.UpdatedEnquiryTitle);
		String Enquiry_title = SVO_EnquiryLostReasonContainer.UpdatedEnquiryTitle.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enquiry title is updated with the edited value : " + Enquiry_title + " On Enquiry page");
	}

	@Then("^User delete the closed lost Enquiry from Enquiry page$")
	public void user_delete_the_closed_lost_Enquiry_from_Enquiry_page() throws Throwable {

		ActionHandler.click(SVO_EnquiryLostReasonContainer.DeleteEnquiryBtn);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on delete enquiry button");

		ActionHandler.click(SVO_EnquiryLostReasonContainer.DeleteEnquiryPopUpBtn);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on delete enquiry pop up confirmation button");
		Reporter.addStepLog("User deleted the closed lost enquiry from enquiry page");
	}

	@Then("^Verify that user is not able to delete the closed lost Enquiry from Enquiry page$")
	public void verify_that_user_is_not_able_to_delete_the_closed_lost_Enquiry_from_Enquiry_page() throws Throwable {

		if (!VerifyHandler.verifyElementPresent(SVO_EnquiryLostReasonContainer.DeleteEnquiryBtn)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User is not able to view delete enquiry button on enquiry page");
		}
	}

	@Then("^Verify that user is not able to close the enquiry with closed state \"([^\"]*)\" and closed reason \"([^\"]*)\"$")
	public void verify_that_user_is_not_able_to_close_the_enquiry_with_closed_state_and_closed_reason(String state,
			String reason) throws Throwable {
		String s[] = state.split(",");
		state = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String r[] = reason.split(",");
		reason = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		ActionHandler.click(SVO_EnquiryLostReasonContainer.ClosedEnquiryStatusTab);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Closed enquiry tab");

		ActionHandler.click(SVO_EnquiryLostReasonContainer.ClosedEnquiryStatusTab);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Closed enquiry tab");

		ActionHandler.click(SVO_EnquiryLostReasonContainer.MarkAsCurrentEnquiryStatusBtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Mark as current enquiry status button");

		ActionHandler.click(SVO_EnquiryLostReasonContainer.ClosedStateDropDownList);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Closed State drop down list");

		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.RPSSelection(state))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects closed state of an enquiry on enquiry page as: " + state);

		ActionHandler.click(SVO_EnquiryLostReasonContainer.ClosedReasonDropDownList);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Closed Reason drop down list");

		if (!VerifyHandler.verifyElementPresent(SVO_EnquiryLostReasonContainer.EnquiryTransferredToEssenPicklist)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog(
					"User is not able to view the enquiry lost reason " + reason + " from closed reason picklist");
		}

		ActionHandler.click(SVO_EnquiryLostReasonContainer.EnquiryCloseCancelButton);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on enquiry Closed pop up window Cancel button");
	}

	@Then("^User select an SLA breached classic sales enquiry which is in stage New$")
	public void user_select_an_SLA_breached_classic_sales_enquiry_which_is_in_stage_New() throws Throwable {

		ActionHandler.wait(2);
		ActionHandler.click(SVOItemToapproveContainer.SelectOpportunityListView);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on select list view tab");

		ActionHandler.click(SVO_EnquiryLostReasonContainer.AllClassicSalesEnquirytab);
		Reporter.addStepLog("User chooses to view selected to classic sales enquiries list");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVO_EnquiryLostReasonContainer.SLAStatusField);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User sort the list view by SLA status");

		ActionHandler.wait(3);
		ActionHandler.click(SVO_EnquiryLostReasonContainer.EnquirysearchBox);
		ActionHandler.wait(2);
		ActionHandler.setText(SVO_EnquiryLostReasonContainer.EnquirysearchBox, "New");

		ActionHandler.wait(3);
		ActionHandler.pressEnter();
		ActionHandler.wait(5);

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstOpportunityLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects SLA breached Classic sales enquiry");

	}

	@Then("^Verify that existing closed reason picklist is not updated when record type changed on Enquiry page$")
	public void verify_that_existing_closed_reason_picklist_is_not_updated_when_record_type_changed_on_Enquiry_page()
			throws Throwable {

		ActionHandler.click(SVO_EnquiryLostReasonContainer.ChangeRecordTypeMoreActionBtn);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on More actions button to view change record type tab");

		ActionHandler.click(SVO_EnquiryLostReasonContainer.ChangeRecordTypeEnquiryBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Change Record type tab");

		VerifyHandler.verifyElementPresent(SVO_EnquiryLostReasonContainer.ChangeRecordTypeTagline);
		CommonFunctions.attachScreenshot();
		String Tagline = SVO_EnquiryLostReasonContainer.ChangeRecordTypeTagline.getText();
		Reporter.addStepLog("User is able to view the tag line stating " + Tagline + " while changing the record type");

		ActionHandler.click(SVO_EnquiryLostReasonContainer.ChangeRecordTypeNextButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Next button on Record types page");
		
		ActionHandler.click(SVO_EnquiryLostReasonContainer.EnquirySaveButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks on to Enquiries Save button");

	}
}
