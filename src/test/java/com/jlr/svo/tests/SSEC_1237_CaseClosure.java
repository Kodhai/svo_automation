package com.jlr.svo.tests;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.autotest.utils.Config;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.constants.Constants;
import com.jlr.svo.containers.SSEC_1227_CasesCreationContainer;
import com.jlr.svo.containers.SSEC_1237_CaseClosureContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.containers.SVOOpportunityContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SSEC_1237_CaseClosure extends TestBaseCC {

	private WebDriver driver = getDriver();
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SSEC_1227_CasesCreationContainer CasesContainer = PageFactory.initElements(driver,
			SSEC_1227_CasesCreationContainer.class);
	SSEC_1237_CaseClosureContainer CaseClosureContainer = PageFactory.initElements(driver,
			SSEC_1237_CaseClosureContainer.class);
	SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);

	public static String verificationCode;
	public static String vCode;
	static double randomNum = getRandomNumber(0, 10000);
	public static String firstName = "Test" + randomNum;
	public static String lastName = "S" + randomNum;
	public static String Chassis = "T" + randomNum;

	public static double getRandomNumber(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	// onStart
	public void onStart() {
		setupTest("SVOTest");
		driver = getDriver();
		SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		CasesContainer = PageFactory.initElements(driver, SSEC_1227_CasesCreationContainer.class);
		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		CaseClosureContainer = PageFactory.initElements(driver, SSEC_1237_CaseClosureContainer.class);
		SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);

		verificationCode = null;

		driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
	}

	public String checkEmailForClassicCSUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(494, 500);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	@Given("^Access to SVO Portal as Classic Customer Service user with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void Access_SVO_Portal_Classic_Parts_with_username_password(String userName, String password)
			throws Exception {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		Reporter.addStepLog("User access SF SVO Portal");

		onStart();
		driver = getDriver();
		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailForClassicCSUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	@When("^User navigates to Cases tab$")
	public void User_navigates_to_cases_tab() throws Exception {

		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(CasesContainer.CasesTab);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate to Cases tab");
	}

	@And("User creates new Customer Service record type with Case Origin as Salesforce$")
	public void User_creates_CS_with_Case_origin_Salesforce() throws Exception {

		ActionHandler.wait(1);
		Select dropdownCaseOrigin = new Select(CaseClosureContainer.CaseOrigin);
		dropdownCaseOrigin.selectByVisibleText("CRC");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User views the dropdown and select CRC");

		ActionHandler.setText(CasesContainer.FirstNameBtn, firstName);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User types the first name");

	}

	@And("^User saves the customer service case$")
	public void User_saves_customer_service_case() throws Exception {

		ActionHandler.click(CasesContainer.SaveBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves the case");

	}

	@Then("^user closes the case with reason as Question Answered$")
	public void user_closes_case_with_Question_Answered() throws Exception {

		ActionHandler.click(CaseClosureContainer.MarkCaseClose);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User nmarks the case for New");

		ActionHandler.click(CaseClosureContainer.MarkCaseClose);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User marks the case for Open");

		ActionHandler.click(CaseClosureContainer.MarkCaseClose);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User marks the case as Complete");

		Select CaseClosedropdown = new Select(CaseClosureContainer.CloseCaseReason);
		CaseClosedropdown.selectByVisibleText("Question Answered");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User views dropdown from Case Origin");

		ActionHandler.click(CaseClosureContainer.doneBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User closes the case");

	}

	public String checkEmailForClassicPartsTechUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(493, 499);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	@Given("^Access to SVO Portal as Classic Parts and technical user with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void Access_SVO_Portal_Classic_Parts_tech_with_username_password(String userName, String password)
			throws Exception {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		Reporter.addStepLog("User access SF SVO Portal");

		onStart();
		driver = getDriver();
		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailForClassicPartsTechUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	@And("^User creates new Parts and technical record type with Case Origin as Email$")
	public void User_created_new_parts_technical_with_Case_Origin_Email() throws Exception {

		String FN = "test" + randomNum;
		
		ActionHandler.click(CasesContainer.Newbtn);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on new button");
		
//		ActionHandler.click(CasesContainer.PartandTechRecordType);
//		ActionHandler.wait(1);
//		CommonFunctions.attachScreenshot();
//		Reporter.addStepLog("User clicks on Parts and Technical record type");

		ActionHandler.click(CasesContainer.NextBtn);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Next button");
		
		ActionHandler.click(CasesContainer.FirstNameBtn);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on First Name text");
		
		ActionHandler.setText(CasesContainer.FirstNameBtn, FN);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters First Name");

//		Select CaseOriginDD = new Select(CaseClosureContainer.CaseOrigin);
//		CaseOriginDD.selectByVisibleText("Email");
//
//		ActionHandler.scrollToView(CasesContainer.VINText);
//		ActionHandler.wait(2);
//		CommonFunctions.attachScreenshot();
//		Reporter.addStepLog("User scrolls to view VIN header");
//
//		ActionHandler.setText(CasesContainer.VINText, Chassis);
//		ActionHandler.wait(2);
//		CommonFunctions.attachScreenshot();
//		Reporter.addStepLog("User sets the VIN");
	}

	@And("^User saves the Parts and Technical case$")
	public void User_saves_parts_tech_case() throws Exception {

		ActionHandler.click(CasesContainer.SaveBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves the case");
	}

	@Then("^User logouts from the SF SVO$")
	public void user_logouts_from_the_SF_SVO() throws Throwable {

		ActionHandler.click(SVO_OpportunityContainer.icon_image);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Logout");
		ActionHandler.click(SVO_OpportunityContainer.Logout);

		CommonFunctions.attachScreenshot();
		if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.userNameTextBox)) {
			MarkupHelper.createLabel("Logged out successfully", ExtentColor.GREEN);
		} else {
			MarkupHelper.createLabel("Log out failed", ExtentColor.RED);

		}
		driver.quit();
	}

	@Then("^user closes the case with reason as Vin not Eligible$")
	public void User_closes_case_Vin_not_Eligible() throws Exception {

		ActionHandler.click(CaseClosureContainer.MarkCaseClose);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User marks the case for New");

		ActionHandler.click(CaseClosureContainer.MarkCaseClose);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Marks the case for Open");

		ActionHandler.click(CaseClosureContainer.MarkCaseClose);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User marks the case as complete");

		Select CaseClosureDD = new Select(CaseClosureContainer.CloseCaseReason);
		CaseClosureDD.selectByVisibleText("Vin Not Eligible");

		ActionHandler.click(CaseClosureContainer.doneBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User closes the case");
	}

	@And("^User creates new Customer Service record type with Case Origin as Website$")
	public void User_creates_Curomer_service_Case_Origin_Web() throws Exception {

		Select CaseOriginDDWeb = new Select(CaseClosureContainer.CaseOrigin);
		CaseOriginDDWeb.selectByVisibleText("Website");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User views the dropdown and select Website");

		ActionHandler.setText(CasesContainer.FirstNameBtn, firstName);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User types the first name");

	}

	@Then("^user closes the case with reason as Transferred to Another Department$")
	public void user_closes_case_Transferred_to_Another_Dept() throws Exception {

		ActionHandler.click(CaseClosureContainer.MarkCaseClose);
		ActionHandler.click(CaseClosureContainer.MarkCaseClose);
		ActionHandler.click(CaseClosureContainer.MarkCaseClose);

		Select CaseCloseTrans = new Select(CaseClosureContainer.CloseCaseReason);
		CaseCloseTrans.selectByVisibleText("Transferred to Another Department");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User views dropdown from Close Case Reason");

		ActionHandler.click(CaseClosureContainer.doneBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User closes the case");
	}

	@And("^User creates new Parts and technical record type with first name, last name and Case Origin as Website and VIN$")
	public void user_created_Parts_Technical_Case_origin_Website() throws Exception {

		ActionHandler.click(CasesContainer.PartandTechRecordType);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Parts and Technical record type");

		ActionHandler.click(CasesContainer.NextBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Next button");

		Select CaseOriginDD = new Select(CaseClosureContainer.CaseOrigin);
		CaseOriginDD.selectByVisibleText("Website");

		ActionHandler.click(CasesContainer.FirstNameBtn);
		ActionHandler.setText(CasesContainer.FirstNameBtn, firstName);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters first name");

		ActionHandler.click(CaseClosureContainer.LastName);
		ActionHandler.setText(CaseClosureContainer.LastName, lastName);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Userenters last name");

		ActionHandler.scrollToView(CasesContainer.VINText);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User scrolls to view VIN header");

		ActionHandler.setText(CasesContainer.VINText, Chassis);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User sets the VIN");
	}

	@Then("^user closes the case with reason as Part not available$")
	public void user_closes_case_Part_not_available() throws Exception {

		ActionHandler.click(CaseClosureContainer.MarkCaseClose);
		ActionHandler.click(CaseClosureContainer.MarkCaseClose);
		ActionHandler.click(CaseClosureContainer.MarkCaseClose);

		Select CaseCloseTrans = new Select(CaseClosureContainer.CloseCaseReason);
		CaseCloseTrans.selectByVisibleText("Part not Available");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects reason as Part not available");

		ActionHandler.click(CaseClosureContainer.doneBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User closes the case");

	}

	@And("^User creates new Parts and technical record type with first name, last name and Case Origin as Salesforce and VIN$")
	public void user_creates_fn_ln_and_case_origin_Salesforce() throws Exception {

		ActionHandler.click(CaseClosureContainer.NewBtn);
		ActionHandler.click(CasesContainer.PartandTechRecordType);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Parts and Technical record type");

		ActionHandler.click(CasesContainer.NextBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Next button");

		Select CaseOriginDD = new Select(CaseClosureContainer.CaseOrigin);
		CaseOriginDD.selectByVisibleText("CRC");

		ActionHandler.click(CasesContainer.FirstNameBtn);
		ActionHandler.setText(CasesContainer.FirstNameBtn, firstName);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters first name");

		ActionHandler.click(CaseClosureContainer.LastName);
		ActionHandler.setText(CaseClosureContainer.LastName, lastName);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Userenters last name");

		ActionHandler.scrollToView(CasesContainer.VINText);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User scrolls to view VIN header");

		ActionHandler.setText(CasesContainer.VINText, Chassis);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User sets the VIN");
	}

	@Then("user closes the case with reason as Other$")
	public void user_closes_case_reason_other() throws Exception {

		ActionHandler.click(CaseClosureContainer.MarkCaseClose);
		ActionHandler.click(CaseClosureContainer.MarkCaseClose);
		ActionHandler.click(CaseClosureContainer.MarkCaseClose);

		Select CaseCloseTrans = new Select(CaseClosureContainer.CloseCaseReason);
		CaseCloseTrans.selectByVisibleText("Other");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects reason as Part not available");

		ActionHandler.click(CaseClosureContainer.doneBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User closes the case");
	}

	@Then("^User removes the mandatory field details and saves the case$")
	public void user_removes_mandatory_fields() throws Exception {

		ActionHandler.scrollToView(CaseClosureContainer.VINlabel);
		ActionHandler.click(CaseClosureContainer.EditVINBtn);
		ActionHandler.click(CaseClosureContainer.EditVINinput);

		Actions act = new Actions(driver);
		act.sendKeys(CaseClosureContainer.EditVINinput, Keys.CLEAR).build().perform();

		ActionHandler.click(CaseClosureContainer.SaveEditBtn);
		ActionHandler.click(CaseClosureContainer.ErrorBtn);

		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(3);
	}

	@Then("^user closes the case$")
	public void user_closes_cases() throws Exception {

		ActionHandler.click(CaseClosureContainer.MarkCaseClose);
		ActionHandler.click(CaseClosureContainer.MarkCaseClose);
		ActionHandler.click(CaseClosureContainer.MarkCaseClose);

		ActionHandler.click(CaseClosureContainer.doneBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User closes the case");

	}

	@And("^Verifies that closed case has all the mandatory fields$")
	public void Verifies_closed_case_has_all_madatory_fields() throws Exception {

		ActionHandler.scrollToView(CaseClosureContainer.VINlabel);
		ActionHandler.click(CaseClosureContainer.EditVINBtn);

		WebElement EditVINinput = CaseClosureContainer.EditVINinput;
		EditVINinput.getText();

		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(3);

	}

	@And("^User logouts from the case$")
	public void user_logouts_from_case() throws Throwable {

		ActionHandler.click(CasesContainer.ProfileBtn);
		ActionHandler.wait(1);
		ActionHandler.click(CasesContainer.LogoutBtn);
		ActionHandler.wait(1);
	}

	@Then("^user closes the case without selecting any reason$")
	public void user_closes_without_other_reason() throws Exception {

		ActionHandler.click(CaseClosureContainer.MarkCaseClose);
		ActionHandler.click(CaseClosureContainer.MarkCaseClose);
		ActionHandler.click(CaseClosureContainer.MarkCaseClose);

		ActionHandler.click(CaseClosureContainer.doneBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User closes the case");
	}

}
