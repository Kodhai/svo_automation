package com.jlr.svo.tests;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.autotest.utils.Config;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.constants.Constants;
import com.jlr.svo.containers.SSEC_1005_AutopoulatedFieldsOnOrderWorkOrderContainer;
import com.jlr.svo.containers.SSEC_1039_SVOEnquiryEmailRemaindersContainer;
import com.jlr.svo.containers.SSEC_1085_BillingAddressOnEnquiryContainer;
import com.jlr.svo.containers.SSEC_546_EmailNotificationforContractSignedContainer;
import com.jlr.svo.containers.SVOAccountsContainer;
import com.jlr.svo.containers.SVOAdditionalVehicleContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.containers.SVOItemToApproveContainer;
import com.jlr.svo.containers.SVOOpportunityContainer;
import com.jlr.svo.containers.SVO_CustomerResponsesContainer;
import com.jlr.svo.containers.SVO_EnquiryLostReasonContainer;
import com.jlr.svo.containers.SVO_OpportunityInvoiceContainer;
import com.jlr.svo.containers.SVO_OpportunitySecondaryOwnerContainer;
import com.jlr.svo.containers.SVO_RestrictedPartyScreeningContainer;
import com.jlr.svo.containers.SVO_WorkOrder_AdditionalOwner_Container;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class SSEC_546_EmailNotificationforContractSignedTest extends TestBaseCC {
	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory
			.getLogger(SSEC_546_EmailNotificationforContractSignedTest.class.getName());
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);

	SVOAccountsContainer SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
	SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	SVOAdditionalVehicleContainer SVOAdditionalvehicleContainer = PageFactory.initElements(driver,
			SVOAdditionalVehicleContainer.class);
	SVO_RestrictedPartyScreeningContainer RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
			SVO_RestrictedPartyScreeningContainer.class);
	SVOItemToApproveContainer SVOItemToapproveContainer = PageFactory.initElements(driver,
			SVOItemToApproveContainer.class);
	SVO_CustomerResponsesContainer SVO_CustomerResponsesContainer = PageFactory.initElements(driver,
			SVO_CustomerResponsesContainer.class);
	SVO_EnquiryLostReasonContainer SVO_EnquiryLostReasonContainer = PageFactory.initElements(driver,
			SVO_EnquiryLostReasonContainer.class);
	SSEC_1039_SVOEnquiryEmailRemaindersContainer SVOEnquiryEmailRemaindersContainer = PageFactory.initElements(driver,
			SSEC_1039_SVOEnquiryEmailRemaindersContainer.class);
	SVO_OpportunitySecondaryOwnerContainer SVO_OpportunitySecondaryOwnerContainer = PageFactory.initElements(driver,
			SVO_OpportunitySecondaryOwnerContainer.class);
	SSEC_546_EmailNotificationforContractSignedContainer EmailNotificationforContractSignedContainer = PageFactory
			.initElements(driver, SSEC_546_EmailNotificationforContractSignedContainer.class);
	SVO_OpportunityInvoiceContainer SVO_OpportunityInvoiceContainer = PageFactory.initElements(driver,
			SVO_OpportunityInvoiceContainer.class);
	SSEC_1085_BillingAddressOnEnquiryContainer BillingAddressOnEnquiryContainer = PageFactory.initElements(driver,
			SSEC_1085_BillingAddressOnEnquiryContainer.class);
	SVO_WorkOrder_AdditionalOwner_Container SVO_WorkOrder_AdditionalOwner_Container = PageFactory.initElements(driver,
			SVO_WorkOrder_AdditionalOwner_Container.class);
	SSEC_1005_AutopoulatedFieldsOnOrderWorkOrderContainer AutopoulatedFieldsOnOrderWorkOrder = PageFactory
			.initElements(driver, SSEC_1005_AutopoulatedFieldsOnOrderWorkOrderContainer.class);

	public static String verificationCode;
	public static String vCode;
	public static String veriCode;
	public static String SLATimeRemaining;
	public static String OpportunityName;

	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	public void onStart() {

		setupTest("SVOTest");
		driver = getDriver();

		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
		SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
		SVOAdditionalvehicleContainer = PageFactory.initElements(driver, SVOAdditionalVehicleContainer.class);

		RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
				SVO_RestrictedPartyScreeningContainer.class);
		SVOItemToapproveContainer = PageFactory.initElements(driver, SVOItemToApproveContainer.class);
		SVO_CustomerResponsesContainer = PageFactory.initElements(driver, SVO_CustomerResponsesContainer.class);
		SVO_EnquiryLostReasonContainer = PageFactory.initElements(driver, SVO_EnquiryLostReasonContainer.class);
		SVOEnquiryEmailRemaindersContainer = PageFactory.initElements(driver,
				SSEC_1039_SVOEnquiryEmailRemaindersContainer.class);
		SVO_OpportunityInvoiceContainer = PageFactory.initElements(driver, SVO_OpportunityInvoiceContainer.class);
		SVO_OpportunitySecondaryOwnerContainer = PageFactory.initElements(driver,
				SVO_OpportunitySecondaryOwnerContainer.class);
		BillingAddressOnEnquiryContainer = PageFactory.initElements(driver,
				SSEC_1085_BillingAddressOnEnquiryContainer.class);
		SVO_WorkOrder_AdditionalOwner_Container = PageFactory.initElements(driver,
				SVO_WorkOrder_AdditionalOwner_Container.class);
		AutopoulatedFieldsOnOrderWorkOrder = PageFactory.initElements(driver,
				SSEC_1005_AutopoulatedFieldsOnOrderWorkOrderContainer.class);

		driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);

		verificationCode = null;
		vCode = null;
		SLATimeRemaining = null;
		OpportunityName = null;

	}

	@And("^User creates Classic Limitted Edition Opportunity with all fields like Stage \"([^\"]*)\" Record Type \"([^\"]*)\" Region \"([^\"]*)\" Client \"([^\"]*)\" Restricted Party Screening Stage \"([^\"]*)\" Preferred Contact \"([^\"]*)\" SAP Account \"([^\"]*)\" Account Name \"([^\"]*)\" Retailer Contact \"([^\"]*)\" Source \"([^\"]*)\" Programme \"([^\"]*)\" Model \"([^\"]*)\"$")
	public void User_creates_an_Classic_Limitted_Edition_Opportunity_with_all_fields_details(String stage,
			String recordType, String region, String client, String Checkcleared, String preferredContact,
			String SAPAccount, String accountName, String retailerContact, String source, String Programme,
			String model) throws Exception {
		String oppName = "Test_Classic_Opportunity_";
		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		String closeDate = CommonFunctions.selectFutureDate();
		Reporter.addStepLog("Opportunity Close Date is = " + closeDate);

		String s[] = stage.split(",");
		stage = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String RT[] = recordType.split(",");
		recordType = CommonFunctions.readExcelMasterData(RT[0], RT[1], RT[2]);

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String c[] = client.split(",");
		client = CommonFunctions.readExcelMasterData(c[0], c[1], c[2]);

		String RPS[] = Checkcleared.split(",");
		Checkcleared = CommonFunctions.readExcelMasterData(RPS[0], RPS[1], RPS[2]);

		String preCon[] = preferredContact.split(",");
		preferredContact = CommonFunctions.readExcelMasterData(preCon[0], preCon[1], preCon[2]);

		String SAP[] = SAPAccount.split(",");
		SAPAccount = CommonFunctions.readExcelMasterData(SAP[0], SAP[1], SAP[2]);

		String accName[] = accountName.split(",");
		accountName = CommonFunctions.readExcelMasterData(accName[0], accName[1], accName[2]);

		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		String so[] = source.split(",");
		source = CommonFunctions.readExcelMasterData(so[0], so[1], so[2]);

		String F[] = Programme.split(",");
		Programme = CommonFunctions.readExcelMasterData(F[0], F[1], F[2]);

		String m[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		ActionHandler.wait(3);
		Reporter.addStepLog("User starts creating new SVO Classic Service Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.NewOpportunity);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.QuoteNextBtn)) {
			ActionHandler.click(driver
					.findElement(By.xpath(SVO_OpportunitySecondaryOwnerContainer.RecordTypeSelection(recordType))));
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects opportunity record type as Classic Service");

			ActionHandler.click(SVOItemToapproveContainer.QuoteNextBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User clicks on next button");

		}

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeOppName, oppName + randomNumber);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Name");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeClosedDate, closeDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Closed date");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeStage);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.StageSelection(stage))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Stage for an Opportunity");

		ActionHandler.wait(1);
		javaScriptUtil.clickElementByJS(EmailNotificationforContractSignedContainer.ViewAllDependencies);
		ActionHandler.wait(1);
		ActionHandler.click(EmailNotificationforContractSignedContainer.SVOBespokeRegion);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(
				driver.findElement(By.xpath(EmailNotificationforContractSignedContainer.RegionSelection(region))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Region for an Opportunity");

		ActionHandler.wait(2);
		ActionHandler.click(EmailNotificationforContractSignedContainer.ApplyButton);

		ActionHandler.wait(4);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeAccountName);
		ActionHandler.wait(1);
		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeAccountName, accountName);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeAccountNameSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(accountName))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Account Name for an Opportunity");

		ActionHandler.wait(1);
		ActionHandler.click(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		ActionHandler
				.click(driver.findElement(By.xpath(RestrictedPartyScreeningContainer.ValueSelection(Checkcleared))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects restricted party screening as Check cleared for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokePreferredContact);
		ActionHandler.wait(1);
		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokePreferredContact, preferredContact);
		ActionHandler.wait(1);
		ActionHandler.click(SVOItemToapproveContainer.SVOBespokePreferredContactname);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		Reporter.addStepLog("User selects Preferred Contact for an Opportunity");

		ActionHandler.click(SVO_OpportunityInvoiceContainer.SAPAccountSearch);
		ActionHandler.wait(1);
		ActionHandler.setText(SVO_OpportunityInvoiceContainer.SAPAccountSearch, SAPAccount);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVO_OpportunityInvoiceContainer.SAPAccountNameSearch);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityInvoiceContainer.SAPAccount(SAPAccount))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects SAP Account for an Opportunity");

		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquiryProgrammeTextBox);
		ActionHandler.wait(1);
		ActionHandler.setText(BillingAddressOnEnquiryContainer.EnquiryProgrammeTextBox, Programme);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquiryProgrammeSearchbar);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquiryClientSelect);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Programme for an Enquiry as " + Programme);

		ActionHandler.wait(1);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.BuildColour);
		ActionHandler.wait(1);
		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.BuildColour, "Blue");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters build colour on Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.HandOfDrivedropDown);
		ActionHandler.wait(1);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.HandOfDriveLHD);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Hand of drive on Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.Speedometerdropdown);
		ActionHandler.wait(1);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.SpeedometerMPH);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Speedometer on Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.BuildColourShade);
		ActionHandler.wait(1);
		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.BuildColourShade, "brown");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters build colour shade on Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.BuildInteriorColour);
		ActionHandler.wait(1);
		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.BuildInteriorColour, "White");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters build interior colour on Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.AdditionalSepcDetails);
		ActionHandler.wait(1);
		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.AdditionalSepcDetails, "Test");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Additional spec details on Opportunity");

		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.MainVehicleRetailPrice, "250");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enter retail price for main vehicle on Opportunity page");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.VATQualifyingDropdown);
		ActionHandler.wait(1);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.VATyesButton);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses VAT qualifying of an opportunity as yes");

		ActionHandler.wait(1);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRetailerContact);
		ActionHandler.wait(2);
		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeRetailerContact, retailerContact);
		ActionHandler.click(SVOItemToapproveContainer.SVOBespokePreferredContactname);
		Reporter.addStepLog("User selects Retailer Contact for an Opportunity");

		ActionHandler.wait(1);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSource);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.SourceSelection(source))));
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Source for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSaveOpportunity);
		ActionHandler.wait(8);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves SVO Bespoke Opportunity");

		OpportunityName = SVOAdditionalvehicleContainer.OpportunityName.getText();
		Reporter.addStepLog("Opportunity Name is = " + OpportunityName);
	}

	@Then("^User moves the opportunity to Contracting stage$")
	public void user_moves_the_opportunity_to_Contracting_stage() throws Throwable {

		ActionHandler.wait(5);
		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(3);

		ActionHandler.click(EmailNotificationforContractSignedContainer.ContractSignedtab);
		ActionHandler.wait(5);
		javaScriptUtil.clickElementByJS(EmailNotificationforContractSignedContainer.ContractSignedtab);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on contract signed tab");

		ActionHandler.click(EmailNotificationforContractSignedContainer.MarkAscurrentStatusButton);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User moves the opportunity to Contracting stage");

	}

	@Then("^User sends contract for Signature using Send Sales Agreement$")
	public void user_sends_contract_for_Signature_using_Send_Sales_Agreement() throws Throwable {
		ActionHandler.wait(1);
		ActionHandler.click(EmailNotificationforContractSignedContainer.sendSalesAgreementbutton);
		ActionHandler.wait(120);
		Reporter.addStepLog("User clicks the Send Sales Agreement button");
		CommonFunctions.attachScreenshot();

		// driver.switchTo().frame(EmailNotificationforContractSignedContainer.AdobeFrame);
		driver.switchTo().frame(0);
		ActionHandler.wait(1);
		// ActionHandler.waitForElement(EmailNotificationforContractSignedContainer.AgreementdraftText,
		// 120);
		ActionHandler.wait(1);
		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(1);
		ActionHandler.click(EmailNotificationforContractSignedContainer.MergeAndSignButton);
		ActionHandler.wait(2);
		Reporter.addStepLog("User clicks the merge and adobe sign button");
		CommonFunctions.attachScreenshot();

		driver.switchTo().parentFrame();

		ActionHandler.waitForElement(SVO_OpportunityContainer.draftText, 120);
		ActionHandler.wait(1);
		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(1);
		ActionHandler.clearText(EmailNotificationforContractSignedContainer.emailCCtextbox);
		ActionHandler.wait(1);
		ActionHandler.click(SVO_OpportunityContainer.sendESignEmail);
		ActionHandler.wait(1);
		Reporter.addStepLog("User clicks the send an email button");
		CommonFunctions.attachScreenshot();

		String parentWindow = driver.getWindowHandle();
		ActionHandler.wait(10);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!parentWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(EmailNotificationforContractSignedContainer.OkConfirmationButton);
				ActionHandler.wait(10);
				CommonFunctions.attachScreenshot();

				driver.switchTo().window(parentWindow);
			}
		}

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User sends an email to sign the agreement");
	}

	@Then("^User signs the Contract by logging in UserName \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void user_signs_the_Contract_by_logging_in_UserName_and_Password(String arg1, String arg2) throws Throwable {
		ActionHandler.wait(1);
		String userName = Config.getPropertyValue("sSignEmail_UserName");
		String password = Config.getPropertyValue("sSignEmail_Password");

		ActionHandler.wait(1);
		driver.get(Constants.outlookURL);
		ActionHandler.wait(1);
		Reporter.addStepLog("User tries to login to Outlook to sign the contract");
		CommonFunctions.attachScreenshot();

		if (!VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.outlookIcon)) {
			ActionHandler.wait(2);
			ActionHandler.click(SVO_OpportunityContainer.outlookSignin);
			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVO_OpportunityContainer.outlookUsername, userName);
			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVO_OpportunityContainer.outlookNext);
			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVO_OpportunityContainer.outlookPassword, password);
			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVO_OpportunityContainer.outlookSignInBtn);
			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVO_OpportunityContainer.outlookIcon);
		ActionHandler.wait(3);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(EmailNotificationforContractSignedContainer.AdobesignFirstEmail);
				ActionHandler.wait(1);
				CommonFunctions.attachScreenshot();
				Reporter.addStepLog("User opens contract agreement mail to sign");

				ActionHandler.wait(1);
				ActionHandler.pageScrollDown();
				ActionHandler.wait(1);

				String SSignLink = driver.getWindowHandle();
				System.out.println("S Sign Link = " + SSignLink);

				ActionHandler.click(SVO_OpportunityContainer.ESignLink);
				ActionHandler.wait(10);

				Set<String> sSignWindows = driver.getWindowHandles();
				Iterator<String> i1 = sSignWindows.iterator();

				String sSignWindow3 = i1.next();
				System.out.println(driver.getTitle());

				if (!SSignLink.equalsIgnoreCase(sSignWindow3)) {
					driver.switchTo().window(sSignWindow3);

					if (VerifyHandler
							.verifyElementPresent(EmailNotificationforContractSignedContainer.ContinueButton)) {
						ActionHandler.click(EmailNotificationforContractSignedContainer.ContinueButton);
					}
					ActionHandler.wait(1);
					ActionHandler.click(SVO_OpportunityContainer.startSigning);
					ActionHandler.wait(1);
					CommonFunctions.attachScreenshot();

					ActionHandler.wait(2);
					javaScriptUtil.clickElementByJS(SVO_OpportunityContainer.clickToSign);
					ActionHandler.wait(1);
					CommonFunctions.attachScreenshot();

					ActionHandler.wait(1);
					ActionHandler.click(SVO_OpportunityContainer.signHereTextBox);
					ActionHandler.wait(2);
					ActionHandler.setText(SVO_OpportunityContainer.signHereTextBox, "Mauli Soni");
					ActionHandler.wait(2);
					CommonFunctions.attachScreenshot();

					ActionHandler.wait(1);
					ActionHandler.click(SVO_OpportunityContainer.signApplyBtn);
					ActionHandler.wait(2);
					CommonFunctions.attachScreenshot();
					Reporter.addStepLog("User signed contract of S-Sign");

					ActionHandler.click(EmailNotificationforContractSignedContainer.ConfirmCheckbox);
					ActionHandler.wait(2);
					CommonFunctions.attachScreenshot();
					Reporter.addStepLog("User check box the specifications and contract");

					ActionHandler.click(EmailNotificationforContractSignedContainer.ConfirmTermsCheckbox);
					ActionHandler.wait(2);
					CommonFunctions.attachScreenshot();
					Reporter.addStepLog("User accepts the terms and conditions of the agreement");

					ActionHandler.click(EmailNotificationforContractSignedContainer.ClickToSignButton);
					ActionHandler.wait(2);
					ActionHandler.wait(2);
					CommonFunctions.attachScreenshot();
					Reporter.addStepLog("User clicks on click to sign button");

					VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.alldoneMessage);
					ActionHandler.wait(2);
					CommonFunctions.attachScreenshot();

					driver.close();
					driver.switchTo().window(SSignLink);
				}

				CommonFunctions.attachScreenshot();
				ActionHandler.wait(10);
				ActionHandler.click(SVO_OpportunityContainer.signedEmail);
				ActionHandler.wait(7);
				CommonFunctions.attachScreenshot();
				Reporter.addStepLog("User verified the received signed confirmation email");
				CommonFunctions.attachScreenshot();
				ActionHandler.wait(4);
				CommonFunctions.attachScreenshot();

				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}

		Reporter.addStepLog("User verifies that confirmation email should be received");
	}

	@Then("^User verifies that Seconday owner receives an email stating the contract has been signed$")
	public void user_verifies_that_Seconday_owner_receives_an_email_stating_the_contract_has_been_signed()
			throws Throwable {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			Reporter.addStepLog("User clicks on Outlook signin button");
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			Reporter.addStepLog("User enters user name as " + userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			Reporter.addStepLog("User clicks on next button");
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			Reporter.addStepLog("User enters password as " + password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			Reporter.addStepLog("User clicks signin button");
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(EmailNotificationforContractSignedContainer.SignedConfirmationEmail);
				ActionHandler.wait(12);
				VerifyHandler
						.verifyElementPresent(EmailNotificationforContractSignedContainer.ContractSignedConfirmation);
				CommonFunctions.attachScreenshot();
				Reporter.addStepLog(
						"User verifies that Seconday owner receives an email stating the contract has been signed");
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
	}

	@Then("^User adds user \"([^\"]*)\" as secondary owner for an classic opportunity$")
	public void user_adds_user_as_secondary_owner_for_an_classic_opportunity(String secondryowner) throws Throwable {

		String SO[] = secondryowner.split(",");
		secondryowner = CommonFunctions.readExcelMasterData(SO[0], SO[1], SO[2]);

		ActionHandler.pageDown();
		ActionHandler.wait(3);

		javaScriptUtil.clickElementByJS(EmailNotificationforContractSignedContainer.EditClassicOpportunityBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on edit secondary owner button");
		ActionHandler.wait(3);

		ActionHandler.click(SVO_OpportunitySecondaryOwnerContainer.SecondaryOwnerSearchBox);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVO_OpportunitySecondaryOwnerContainer.SecondaryOwnerSearchBox, secondryowner);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		ActionHandler.click(EmailNotificationforContractSignedContainer.SecondaryOwnerNameSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver
				.findElement(By.xpath(SVO_OpportunitySecondaryOwnerContainer.SecndaryOwnerSelection(secondryowner))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects secondary owner for an Opportunity");

		ActionHandler.click(SVO_OpportunitySecondaryOwnerContainer.SaveBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves an opportunity");

	}

	@Then("^User sends contract for Signature using Send Sales Agreement after editing CC email address to \"([^\"]*)\"$")
	public void user_sends_contract_for_Signature_using_Send_Sales_Agreement_after_editing_CC_email_address_to(
			String User) throws Throwable {

		String us[] = User.split(",");
		User = CommonFunctions.readExcelMasterData(us[0], us[1], us[2]);

		ActionHandler.wait(1);
		ActionHandler.click(EmailNotificationforContractSignedContainer.sendSalesAgreementbutton);
		ActionHandler.wait(1);
		Reporter.addStepLog("User clicks the Send Sales Agreement button");
		CommonFunctions.attachScreenshot();

		ActionHandler.waitForElement(EmailNotificationforContractSignedContainer.AgreementdraftText, 120);
		ActionHandler.wait(1);
		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		driver.switchTo().frame(EmailNotificationforContractSignedContainer.AdobeFrame);
		ActionHandler.wait(1);
		ActionHandler.click(EmailNotificationforContractSignedContainer.MergeAndSignButton);
		ActionHandler.wait(1);
		Reporter.addStepLog("User clicks the merge and adobe sign button");
		CommonFunctions.attachScreenshot();

		driver.switchTo().parentFrame();

		ActionHandler.waitForElement(SVO_OpportunityContainer.draftText, 120);
		ActionHandler.wait(1);
		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(1);
		ActionHandler.clearAndSetText(EmailNotificationforContractSignedContainer.emailCCtextbox, User);
		ActionHandler.wait(1);
		ActionHandler.click(SVO_OpportunityContainer.sendESignEmail);
		ActionHandler.wait(1);
		Reporter.addStepLog("User clicks the send an email button");
		CommonFunctions.attachScreenshot();

		String parentWindow = driver.getWindowHandle();
		ActionHandler.wait(10);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!parentWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(EmailNotificationforContractSignedContainer.OkConfirmationButton);
				ActionHandler.wait(10);
				CommonFunctions.attachScreenshot();

				driver.switchTo().window(parentWindow);
			}
		}

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User sends an email to sign the agreement");
	}

	@Then("^User creates Classic Works Legend Opportunity with fields like Stage \"([^\"]*)\" Product Offering \"([^\"]*)\" Region \"([^\"]*)\" Client \"([^\"]*)\" Restricted Party Screening Stage \"([^\"]*)\" Preferred Contact \"([^\"]*)\" Account Name \"([^\"]*)\" Retailer Contact \"([^\"]*)\" Source \"([^\"]*)\" Origination Divission \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\"$")
	public void user_creates_Classic_Works_Legend_Opportunity_with_fields_like_Stage_Product_Offering_Region_Client_Restricted_Party_Screening_Stage_Preferred_Contact_Account_Name_Retailer_Contact_Source_Origination_Divission_Brand_Model(
			String stage, String productOffering, String region, String client, String Checkcleared,
			String preferredContact, String accountName, String retailerContact, String source,
			String originatingdivission, String brand, String model) throws Exception {
		String oppName = "Classic_WorksLegend_Opportunity_";
		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		String closeDate = CommonFunctions.selectFutureDate();
		Reporter.addStepLog("Opportunity Close Date is = " + closeDate);

		String s[] = stage.split(",");
		stage = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String proOff[] = productOffering.split(",");
		productOffering = CommonFunctions.readExcelMasterData(proOff[0], proOff[1], proOff[2]);

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String c[] = client.split(",");
		client = CommonFunctions.readExcelMasterData(c[0], c[1], c[2]);

		String RPS[] = Checkcleared.split(",");
		Checkcleared = CommonFunctions.readExcelMasterData(RPS[0], RPS[1], RPS[2]);

		String preCon[] = preferredContact.split(",");
		preferredContact = CommonFunctions.readExcelMasterData(preCon[0], preCon[1], preCon[2]);

		String accName[] = accountName.split(",");
		accountName = CommonFunctions.readExcelMasterData(accName[0], accName[1], accName[2]);

		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		String so[] = source.split(",");
		source = CommonFunctions.readExcelMasterData(so[0], so[1], so[2]);

		String OD[] = originatingdivission.split(",");
		originatingdivission = CommonFunctions.readExcelMasterData(OD[0], OD[1], OD[2]);

		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String m[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		Reporter.addStepLog("User starts creating new Classic works legend Opportunity");
		ActionHandler.click(SVOAdditionalvehicleContainer.NewOpportunity);
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.QuoteNextBtn)) {

			ActionHandler.click(AutopoulatedFieldsOnOrderWorkOrder.ClassicWorksLegendRecType);
			ActionHandler.wait(1);
			Reporter.addStepLog("User selects opportunity record type as Classic Works legend");

			ActionHandler.click(SVOItemToapproveContainer.QuoteNextBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User clicks on next button");

		}

		ActionHandler.wait(1);
		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeOppName, oppName + randomNumber);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Name");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeClosedDate, closeDate);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Closed date");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeStage);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.StageSelection(stage))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Stage for an Opportunity");

		ActionHandler.wait(1);
		javaScriptUtil.clickElementByJS(EmailNotificationforContractSignedContainer.ViewAllDependencies);
		ActionHandler.wait(1);
		ActionHandler.click(EmailNotificationforContractSignedContainer.SVOBespokeRegion);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(
				driver.findElement(By.xpath(EmailNotificationforContractSignedContainer.RegionSelection(region))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Region for an Opportunity");
		ActionHandler.wait(2);
		ActionHandler.click(EmailNotificationforContractSignedContainer.ApplyButton);

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeAccountName);
		ActionHandler.wait(1);
		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeAccountName, accountName);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeAccountNameSearch);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(accountName))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Account Name for an Opportunity");

		ActionHandler.wait(1);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokePreferredContact);
		ActionHandler.wait(1);
		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokePreferredContact, preferredContact);
		ActionHandler.wait(1);
		ActionHandler.click(SVOItemToapproveContainer.SVOBespokePreferredContactname);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Preferred Contact for an Opportunity");

		ActionHandler.wait(1);
		ActionHandler.click(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		ActionHandler
				.click(driver.findElement(By.xpath(RestrictedPartyScreeningContainer.ValueSelection(Checkcleared))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects restricted party screening as Check cleared for an Opportunity");

		ActionHandler.wait(1);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeBrand);
		ActionHandler.wait(1);
		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeBrand, brand);
		ActionHandler.wait(1);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeBrandSearch);
		ActionHandler.wait(1);
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(brand))));
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Brand for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModel);
		ActionHandler.wait(1);
		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeModel, model);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(model))));
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Model for an Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.BodyStyleTextBox);
		ActionHandler.wait(1);
		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.BodyStyleTextBox, model);
		ActionHandler.wait(1);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.TestDefenderBodystyle);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects body style for an Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.HandOfDrivedropDown);
		ActionHandler.wait(1);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.HandOfDriveLHD);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Hand of drive on Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.ClassicServiceVehicleRecord);
		ActionHandler.wait(1);
		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.ClassicServiceVehicleRecord, model);
		ActionHandler.wait(1);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.ClassicServiceDefenderVehicle);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Vehicle record for an Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.MainVehicleRetailPrice);
		ActionHandler.wait(1);
		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.MainVehicleRetailPrice, "250");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enter retail price for main vehicle on Opportunity page");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.VATQualifyingDropdown);
		ActionHandler.wait(1);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.VATyesButton);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses VAT qualifying of an opportunity as yes");

		ActionHandler.wait(2);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRetailerContact);
		ActionHandler.wait(2);
		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeRetailerContact, retailerContact);
		ActionHandler.wait(1);
		ActionHandler.click(SVOItemToapproveContainer.SVOBespokePreferredContactname);
		ActionHandler.wait(1);
		Reporter.addStepLog("User selects Retailer Contact for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSource);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.SourceSelection(source))));
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Source for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSaveOpportunity);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves Classic Bespoke Opportunity");

		OpportunityName = SVOAdditionalvehicleContainer.OpportunityName.getText();
		Reporter.addStepLog("Opportunity Name is = " + OpportunityName);

	}

	@Then("^User creates an Classic Bespoke Opportunity with all fields like Stage \"([^\"]*)\" Product Offering \"([^\"]*)\" Region \"([^\"]*)\" Client \"([^\"]*)\" Restricted Party Screening Stage \"([^\"]*)\" Preferred Contact \"([^\"]*)\" Account Name \"([^\"]*)\" Retailer Contact \"([^\"]*)\" Source \"([^\"]*)\" Origination Divission \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\"$")
	public void user_creates_an_Classic_Bespoke_Opportunity_with_all_fields_like_Stage_Product_Offering_Region_Client_Restricted_Party_Screening_Stage_Preferred_Contact_Account_Name_Retailer_Contact_Source_Origination_Divission_Brand_Model(
			String stage, String productOffering, String region, String client, String Checkcleared,
			String preferredContact, String accountName, String retailerContact, String source,
			String originatingdivission, String brand, String model) throws Exception {
		String oppName = "Classic_Bespoke_Opportunity_";
		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		String closeDate = CommonFunctions.selectFutureDate();
		Reporter.addStepLog("Opportunity Close Date is = " + closeDate);

		String s[] = stage.split(",");
		stage = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String proOff[] = productOffering.split(",");
		productOffering = CommonFunctions.readExcelMasterData(proOff[0], proOff[1], proOff[2]);

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String c[] = client.split(",");
		client = CommonFunctions.readExcelMasterData(c[0], c[1], c[2]);

		String RPS[] = Checkcleared.split(",");
		Checkcleared = CommonFunctions.readExcelMasterData(RPS[0], RPS[1], RPS[2]);

		String preCon[] = preferredContact.split(",");
		preferredContact = CommonFunctions.readExcelMasterData(preCon[0], preCon[1], preCon[2]);

		String accName[] = accountName.split(",");
		accountName = CommonFunctions.readExcelMasterData(accName[0], accName[1], accName[2]);

		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		String so[] = source.split(",");
		source = CommonFunctions.readExcelMasterData(so[0], so[1], so[2]);

		String OD[] = originatingdivission.split(",");
		originatingdivission = CommonFunctions.readExcelMasterData(OD[0], OD[1], OD[2]);

		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String m[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		Reporter.addStepLog("User starts creating new Classic Bespoke Opportunity");
		ActionHandler.click(SVOAdditionalvehicleContainer.NewOpportunity);
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.QuoteNextBtn)) {

			ActionHandler.click(AutopoulatedFieldsOnOrderWorkOrder.ClassicBespokeRecType);
			ActionHandler.wait(1);
			Reporter.addStepLog("User selects opportunity record type as Classic Bespoke");

			ActionHandler.click(SVOItemToapproveContainer.QuoteNextBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User clicks on next button");

		}

		ActionHandler.wait(1);
		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeOppName, oppName + randomNumber);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Name");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeClosedDate, closeDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Closed date");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeStage);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.StageSelection(stage))));
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Stage for an Opportunity");

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Product Offering for an Opportunity");

		ActionHandler.wait(1);
		javaScriptUtil.clickElementByJS(EmailNotificationforContractSignedContainer.ViewAllDependencies);
		ActionHandler.wait(1);
		ActionHandler.click(EmailNotificationforContractSignedContainer.SVOBespokeRegion);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(
				driver.findElement(By.xpath(EmailNotificationforContractSignedContainer.RegionSelection(region))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Region for an Opportunity");
		ActionHandler.wait(2);
		ActionHandler.click(EmailNotificationforContractSignedContainer.ApplyButton);

		ActionHandler.wait(1);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeAccountName);
		ActionHandler.wait(1);
		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeAccountName, accountName);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeAccountNameSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(accountName))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Account Name for an Opportunity");

		ActionHandler.wait(1);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokePreferredContact);
		ActionHandler.wait(1);
		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokePreferredContact, preferredContact);
		ActionHandler.wait(1);
		ActionHandler.click(SVOItemToapproveContainer.SVOBespokePreferredContactname);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Preferred Contact for an Opportunity");

		VerifyHandler.verifyElementPresent(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		CommonFunctions.attachScreenshot();
		ActionHandler
				.click(driver.findElement(By.xpath(RestrictedPartyScreeningContainer.ValueSelection(Checkcleared))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects restricted party screening as Check cleared for an Opportunity");

		ActionHandler.wait(1);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeBrand);
		ActionHandler.wait(1);
		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeBrand, brand);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeBrandSearch);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(brand))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Brand for an Opportunity");

		ActionHandler.wait(1);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModel);
		ActionHandler.wait(1);
		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeModel, model);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(model))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Model for an Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.BodyStyleTextBox);
		ActionHandler.wait(1);
		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.BodyStyleTextBox, model);
		ActionHandler.wait(1);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.TestDefenderBodystyle);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects body style for an Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.HandOfDrivedropDown);
		ActionHandler.wait(1);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.HandOfDriveLHD);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Hand of drive on Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.Speedometerdropdown);
		ActionHandler.wait(1);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.SpeedometerMPH);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Speedometer on Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.AdditionalSepcDetails);
		ActionHandler.wait(1);
		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.AdditionalSepcDetails, "Test");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Additional spec details on Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.ClassicServiceVehicleRecord);
		ActionHandler.wait(1);
		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.ClassicServiceVehicleRecord, model);
		ActionHandler.wait(1);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.ClassicServiceDefenderVehicle);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Vehicle record for an Opportunity");

		ActionHandler.click(AutopoulatedFieldsOnOrderWorkOrder.BespokeInteriorRequirementTxtBox);
		ActionHandler.wait(1);
		ActionHandler.setText(AutopoulatedFieldsOnOrderWorkOrder.BespokeInteriorRequirementTxtBox, "Test Interior");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters Bespoke Interior Requirements");

		ActionHandler.click(AutopoulatedFieldsOnOrderWorkOrder.BespokeExteriorRequirementTxtBox);
		ActionHandler.wait(1);
		ActionHandler.setText(AutopoulatedFieldsOnOrderWorkOrder.BespokeExteriorRequirementTxtBox, "Test Exterior");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters Bespoke Exterior Requirements");

		ActionHandler.click(AutopoulatedFieldsOnOrderWorkOrder.BespokePaintRequirementTxtBox);
		ActionHandler.wait(1);
		ActionHandler.setText(AutopoulatedFieldsOnOrderWorkOrder.BespokePaintRequirementTxtBox, "Test Paint");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters Bespoke Paint Requirements");

		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.MainVehicleRetailPrice, "250");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enter retail price for main vehicle on Opportunity page");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.VATQualifyingDropdown);
		ActionHandler.wait(1);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.VATyesButton);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses VAT qualifying of an opportunity as yes");

		ActionHandler.wait(2);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRetailerContact);
		ActionHandler.wait(2);
		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeRetailerContact, retailerContact);
		ActionHandler.wait(1);
		ActionHandler.click(SVOItemToapproveContainer.SVOBespokePreferredContactname);
		ActionHandler.wait(1);
		Reporter.addStepLog("User selects Retailer Contact for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSource);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.SourceSelection(source))));
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Source for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSaveOpportunity);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves Classic Bespoke Opportunity");

		OpportunityName = SVOAdditionalvehicleContainer.OpportunityName.getText();
		Reporter.addStepLog("Opportunity Name is = " + OpportunityName);

	}

	@Then("^User verifies that CC receipent receives an email stating the contract has been signed$")
	public void user_verifies_that_CC_recepient_receives_an_email_stating_the_contract_has_been_signed()
			throws Throwable {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			Reporter.addStepLog("User clicks on Outlook signin button");
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			Reporter.addStepLog("User enters user name as " + userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			Reporter.addStepLog("User clicks on next button");
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			Reporter.addStepLog("User enters password as " + password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			Reporter.addStepLog("User clicks signin button");
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(EmailNotificationforContractSignedContainer.SignedConfirmationEmail);
				ActionHandler.wait(12);
				VerifyHandler
						.verifyElementPresent(EmailNotificationforContractSignedContainer.ContractSignedConfirmation);
				CommonFunctions.attachScreenshot();
				Reporter.addStepLog(
						"User verifies that Seconday owner receives an email stating the contract has been signed");
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
	}

	@Then("^Verify that secondary owner does not receives an email stating the contract has been signed$")
	public void Verify_that_secondary_owner_does_not_receives_an_email_stating_the_contract_has_been_signed()
			throws Throwable {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			Reporter.addStepLog("User clicks on Outlook signin button");
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			Reporter.addStepLog("User enters user name as " + userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			Reporter.addStepLog("User clicks on next button");
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			Reporter.addStepLog("User enters password as " + password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			Reporter.addStepLog("User clicks signin button");
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				if (!VerifyHandler
						.verifyElementPresent(EmailNotificationforContractSignedContainer.ContractSignedConfirmation)) {
					CommonFunctions.attachScreenshot();
					Reporter.addStepLog("user does not receives an email stating the contract has been signed");
				}

				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
	}

	@Then("^User tries to send contract to Invalid email address for Signature using Send Sales Agreement$")
	public void User_tries_to_send_contract_to_Invalid_email_address_for_Signature_using_Send_Sales_Agreement()
			throws Throwable {
		ActionHandler.wait(1);
		ActionHandler.click(EmailNotificationforContractSignedContainer.sendSalesAgreementbutton);
		ActionHandler.wait(120);
		Reporter.addStepLog("User clicks the Send Sales Agreement button");
		CommonFunctions.attachScreenshot();

		driver.switchTo().frame(0);
		ActionHandler.wait(1);
		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(1);
		ActionHandler.click(EmailNotificationforContractSignedContainer.MergeAndSignButton);
		ActionHandler.wait(2);
		Reporter.addStepLog("User clicks the merge and adobe sign button");
		CommonFunctions.attachScreenshot();

		driver.switchTo().parentFrame();

		ActionHandler.waitForElement(SVO_OpportunityContainer.draftText, 120);
		ActionHandler.wait(1);
		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(1);

		ActionHandler.click(SVO_OpportunityContainer.sendESignEmail);
		ActionHandler.wait(1);
		Reporter.addStepLog("User clicks the send an email button");
		CommonFunctions.attachScreenshot();

	}

	@Then("^User is not able to add Secondary owner thus cannot validate email notification$")
	public void User_is_not_able_to_add_Secondary_owner_thus_cannot_validate_email_notification() throws Throwable {

		ActionHandler.pageDown();
		ActionHandler.wait(3);

		if (!VerifyHandler
				.verifyElementPresent(EmailNotificationforContractSignedContainer.EditClassicOpportunityBtn)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User is not able to add Secondary owner thus cannot validate email notification");
			ActionHandler.wait(3);
		}

	}

	@Then("^Verify an error occurred when user sends contract for signature$")
	public void Verify_an_error_occurred_when_user_sends_contract_for_signature() throws Throwable {

		if (VerifyHandler.verifyElementPresent(EmailNotificationforContractSignedContainer.InvalidEmailCCErrorMsg)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("An error occurred when user sends contract for signature");
			ActionHandler.wait(3);
		}
	}

}