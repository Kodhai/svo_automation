package com.jlr.svo.tests;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.autotest.utils.Config;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.constants.Constants;
import com.jlr.svo.containers.SVOAccountsContainer;
import com.jlr.svo.containers.SVOAdditionalVehicleContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.containers.SVOItemToApproveContainer;
import com.jlr.svo.containers.SVOOpportunityContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SVO_Item_to_approve_Test extends TestBaseCC {

	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory.getLogger(SVO_Item_to_approve_Test.class.getName());
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);

	SVOAccountsContainer SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
	SVOItemToApproveContainer SVOItemToapproveContainer = PageFactory.initElements(driver,
			SVOItemToApproveContainer.class);
	SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	SVOAdditionalVehicleContainer SVOAdditionalvehicleContainer = PageFactory.initElements(driver,
			SVOAdditionalVehicleContainer.class);

	public static String verificationCode;
	public static String vCode;
	public static String veriCode;
	public static String OpportunityName;
	public static String DesignBriefRequestname;
	public static String DesignBriefRequestapprovalname;
	public static String DesignBriefName;
	public static String submittedbyuser;
	public String NewWindow;

	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	public void onStart() {

		setupTest("SVOTest");
		driver = getDriver();
		SVOItemToapproveContainer = PageFactory.initElements(driver, SVOItemToApproveContainer.class);
		SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
		SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
		SVOAdditionalvehicleContainer = PageFactory.initElements(driver, SVOAdditionalVehicleContainer.class);
		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		verificationCode = null;
		vCode = null;
		OpportunityName = null;
		DesignBriefRequestname = null;
		DesignBriefRequestapprovalname = null;
		DesignBriefName = null;
		submittedbyuser = null;
		NewWindow = null;

	}

	public String checkEmail() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.outlookSignin)) {

			ActionHandler.click(SVO_OpportunityContainer.outlookSignin);
			CommonFunctions.attachScreenshot();
			ActionHandler.setText(SVO_OpportunityContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVO_OpportunityContainer.outlookNext);
			CommonFunctions.attachScreenshot();
			ActionHandler.setText(SVO_OpportunityContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVO_OpportunityContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(334, 340);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	public String checkEmailPrivateOffice() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();
			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();
			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(341, 347);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	@Given("^Access to SO data management Portal with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void access_to_an_SO_data_management_Portal_with_Username_and_Password(String userName, String password)
			throws Throwable {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");
		onStart();
		driver = getDriver();
		ActionHandler.wait(7);
		driver.get(Constants.SVOURL);
		ActionHandler.wait(8);
		Reporter.addStepLog("User Logins to SVO Portal");

		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User requires to enter Verification code received in an email");
		ActionHandler.wait(5);

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");
			ActionHandler.wait(7);
			vCode = checkEmailSOdatamanagement();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			ActionHandler.wait(7);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(3);
			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(2);
			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {
			ActionHandler.wait(5);
			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {
				ActionHandler.wait(3);
				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {
				ActionHandler.wait(3);
				ActionHandler.click(SVOEnquiryContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				ActionHandler.wait(2);
			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	public String checkEmailSOdatamanagement() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		ActionHandler.wait(5);
		driver.get(Constants.outlookURL);
		ActionHandler.wait(10);
		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {
			ActionHandler.wait(2);
			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			ActionHandler.wait(7);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			ActionHandler.wait(7);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			ActionHandler.wait(7);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			ActionHandler.wait(7);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);
				ActionHandler.wait(8);
				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(340, 346);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	public String checkEmailAdmin() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();
			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();
			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(337, 343);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	@Given("^Access to SVO classic operation Portal with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void access_to_an_SVO_classic_operation_Portal_with_Username_and_Password(String userName, String password)
			throws Throwable {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");
		onStart();
		driver = getDriver();
		ActionHandler.wait(7);
		driver.get(Constants.SVOURL);
		ActionHandler.wait(8);
		Reporter.addStepLog("User Logins to SVO Portal");

		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User requires to enter Verification code received in an email");
		ActionHandler.wait(5);

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");
			ActionHandler.wait(7);
			vCode = checkEmailclassicoperation();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			ActionHandler.wait(7);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(3);
			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(2);
			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {
			ActionHandler.wait(5);
			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {
				ActionHandler.wait(3);
				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {
				ActionHandler.wait(3);
				ActionHandler.click(SVOEnquiryContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {

				ActionHandler.wait(2);
			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	public String checkEmailclassicoperation() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);
		ActionHandler.wait(10);
		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {
			ActionHandler.wait(2);
			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			ActionHandler.wait(7);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			ActionHandler.wait(7);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			ActionHandler.wait(7);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			ActionHandler.wait(7);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);
				ActionHandler.wait(8);
				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(338, 344);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	public String checkEmailbespokeoperation() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(338, 344);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	public void navigateToSVO() throws Exception {
		String SVO = "Special Vehicle Operations";

		Reporter.addStepLog("User tries to navigate to SVO Portal");
		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.jlrText)) {

			// user click on menu button//
			ActionHandler.click(SVOEnquiryContainer.menuBtn);
			CommonFunctions.attachScreenshot();

			// user enter text in search box//
			ActionHandler.setText(SVOEnquiryContainer.searchBox, SVO);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.selectMenu(SVO))));
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User navigate to SVO Portal");
		}
	}

	@Given("^Access to SVO bespoke Portal with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void access_to_an_SVO_bespoke_Portal_with_Username_and_Password(String userName, String password)
			throws Throwable {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");
		onStart();
		driver = getDriver();

		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");
			vCode = checkEmail();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");
			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	@Given("^Login to SVO bespoke Portal with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void login_to_an_SVO_bespoke_Portal_with_Username_and_Password(String userName, String password)
			throws Throwable {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		Reporter.addStepLog("User Logins to SVO Portal");

		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");
			vCode = checkEmail();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	@Given("^Access to SVO bespoke operation Portal with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void access_to_an_SVO_bespoke_operation_Portal_with_Username_and_Password(String userName, String password)
			throws Throwable {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		onStart();
		driver = getDriver();
		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailbespokeoperation();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	@Given("^Login to SVO bespoke operation Portal with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void Login_to_an_SVO_bespoke_operation_Portal_with_Username_and_Password(String userName, String password)
			throws Throwable {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		Reporter.addStepLog("User Logins to SVO Portal");

		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailbespokeoperation();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);

			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	@Given("^Access to SVO admin Portal with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void access_to_an_SVO_admin_Portal_with_Username_and_Password(String userName, String password)
			throws Throwable {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		onStart();
		driver = getDriver();
		driver.get(Constants.SVOURL);
		Reporter.addStepLog("User Logins to SVO Portal");

		ActionHandler.setText(SVO_OpportunityContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVO_OpportunityContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVO_OpportunityContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailAdmin();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVO_OpportunityContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVO_OpportunityContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVO_OpportunityContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVO_OpportunityContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.remindMeLater)) {

				ActionHandler.click(SVO_OpportunityContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.notRegister)) {

				ActionHandler.click(SVO_OpportunityContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	@Given("^Login to SVO admin Portal with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void login_to_an_SVO_admin_Portal_with_Username_and_Password(String userName, String password)
			throws Throwable {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		Reporter.addStepLog("User Logins to SVO Portal");

		ActionHandler.setText(SVO_OpportunityContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVO_OpportunityContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVO_OpportunityContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailAdmin();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVO_OpportunityContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVO_OpportunityContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVO_OpportunityContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVO_OpportunityContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.remindMeLater)) {

				ActionHandler.click(SVO_OpportunityContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.notRegister)) {

				ActionHandler.click(SVO_OpportunityContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	@Then("^Logout from an SVO main Portal$")
	public void logout_from_an_SVO_main_Portal() throws Throwable {

		ActionHandler.click(SVO_OpportunityContainer.icon_image);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Logout");
		ActionHandler.click(SVO_OpportunityContainer.Logout);

		CommonFunctions.attachScreenshot();
		if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.userNameTextBox)) {
			MarkupHelper.createLabel("Logged out successfully", ExtentColor.GREEN);
		} else {
			MarkupHelper.createLabel("Log out failed", ExtentColor.RED);
		}
	}

	@When("^User navigate to Design Brief pending approvals section$")
	public void user_navigate_to_design_brief_pending_Approvals_section() throws Throwable {

		javaScriptUtil.clickElementByJS(SVOItemToapproveContainer.Hometab);

		ActionHandler.wait(6);
		javaScriptUtil.clickElementByJS(SVOItemToapproveContainer.ManageAllHyperLink);
		ActionHandler.wait(6);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate to Design Brief pending approvals section");

	}

	@Then("^Verify that user receives notification for Design brief approval requests$")
	public void Verify_that_user_receives_notification_for_Design_brief_approval_requests() throws Throwable {

		ActionHandler.click(SVOItemToapproveContainer.DesignBriefRequestsNotificationbar);
		if (VerifyHandler
				.verifyElementPresent(driver.findElement(By.xpath(
						SVOItemToapproveContainer.DesignBriefApprovalrequestnotification(DesignBriefRequestname))))
				&& VerifyHandler.verifyElementPresent(driver.findElement(By.xpath(SVOItemToapproveContainer
						.DesignBriefApprovalrequestnotification(DesignBriefRequestapprovalname))))) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Both the Design Brief approval requests notifications are received");
		} else {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Design Brief Request approval notification is not received");
		}
		ActionHandler.click(SVOItemToapproveContainer.CloseDesignBriefRequestsNotificationBar);
	}

	@When("^User navigates to Items to Approve section and select design brief request$")
	public void user_navigates_to_Items_to_Approve_section_and_select_design_brief_reqy() throws Throwable {

		VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.ItemsToApproveSection);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Item To Approve section is present on Home Page");

		ActionHandler.click(SVOItemToapproveContainer.ManageAllHyperLink);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Item To Approve page successfully");

		driver.navigate().refresh();
		ActionHandler.wait(15);
		ActionHandler.click(SVOItemToapproveContainer.BespokeOpsDesignBriefApprovalRequestCheckbox);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User select single design brief request to approve/reassign");

	}

	@Then("^User create a new Quote with mandatory fields like Retailer Contact \"([^\"]*)\"$")
	public void User_create_a_New_Quote_with_Mandatory_field(String retailerContact) throws Exception {
		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		String quoteName = "SVO Bespoke Test Quote";
		String expirtaionDate = CommonFunctions.selectFutureDate();

		ActionHandler.pageDown();

		CommonFunctions.attachScreenshot();

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuotesLink);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Quotes page");

		ActionHandler.click(SVOAdditionalvehicleContainer.NewQuoteBtn);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on New button to create new Quote");

		if (VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.PrivateOfficeclosed)) {
			ActionHandler.click(SVOAdditionalvehicleContainer.PrivateOfficeclosed);
		}
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOItemToapproveContainer.QuoteNextBtn);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Selects Private Office Closed record type");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteNameBox, quoteName);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User writes quote name for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRetailerNameBox, retailerContact);

		CommonFunctions.attachScreenshot();
		Actions act3 = new Actions(driver);
		act3.sendKeys(Keys.ARROW_DOWN).build().perform();
		act3.sendKeys(Keys.ENTER).build().perform();

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters retailer contact details for new quote");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteCostPrice, "123");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Cost Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteListPrice, "203");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters List Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteContribution, "23");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Contribution for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRetailPrice, "213");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Retail Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRevenue, "113");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Revenue for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteExpirationDate, expirtaionDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Expiration Date for new quote");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteSaveBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User save new quote");

		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.BackToOpportunity(OpportunityName))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate back to the Opportunity");
	}

	@Then("^User navigates to an Opportunity tab$")
	public void user_navigates_to_an_Opportunity_tab() throws Throwable {

		javaScriptUtil.clickElementByJS(SVO_OpportunityContainer.OpportTab);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigate to Opportunity");

	}

	@And("^User create a SVO Bespoke Opportunity with fields like Stage \"([^\"]*)\" Product Offering \"([^\"]*)\" Region \"([^\"]*)\" Client \"([^\"]*)\" Preferred Contact \"([^\"]*)\" Account Name \"([^\"]*)\" Retailer Contact \"([^\"]*)\" Source \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\"$")
	public void User_create_a_SVO_Bespoke_Opportunity(String stage, String productOffering, String region,
			String client, String preferredContact, String accountName, String retailerContact, String source,
			String brand, String model) throws Exception {
		String oppName = "Test_SVO_Bespoke_Opportunity_";
		double randomNumber = getRandomIntegerBetweenRange(20000, 60000);

		String closeDate = CommonFunctions.selectFutureDate();
		Reporter.addStepLog("Opportunity Close Date is = " + closeDate);

		String s[] = stage.split(",");
		stage = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String proOff[] = productOffering.split(",");
		productOffering = CommonFunctions.readExcelMasterData(proOff[0], proOff[1], proOff[2]);

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String c[] = client.split(",");
		client = CommonFunctions.readExcelMasterData(c[0], c[1], c[2]);

		String preCon[] = preferredContact.split(",");
		preferredContact = CommonFunctions.readExcelMasterData(preCon[0], preCon[1], preCon[2]);

		String accName[] = accountName.split(",");
		accountName = CommonFunctions.readExcelMasterData(accName[0], accName[1], accName[2]);

		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		String so[] = source.split(",");
		source = CommonFunctions.readExcelMasterData(so[0], so[1], so[2]);

		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String m[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		Reporter.addStepLog("User starts creating new SVO Bespoke Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.NewOpportunity);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOItemToapproveContainer.QuoteNextBtn);
		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeOppName, oppName + randomNumber);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Name");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeClosedDate, closeDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Closed date");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeStage);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.StageSelection(stage))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Stage for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeProductOffering);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(
				driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ProOfferingSelection(productOffering))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Product Offering for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.RegionSelection(region))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Region for an Opportunity");

		VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.SVOBespokeClient);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeClient, client);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeClientNameSearch);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(client))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Client for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokePreferredContact, preferredContact);
		ActionHandler.click(SVOItemToapproveContainer.SVOBespokePreferredContactname);
		Reporter.addStepLog("User selects Preferred Contact for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeAccountName, accountName);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeAccountNameSearch);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(accountName))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Account Name for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeRetailerContact, retailerContact);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOItemToapproveContainer.SVOBespokePreferredContactname);
		Reporter.addStepLog("User selects Retailer Contact for an Opportunity");

		ActionHandler.wait(2);
		ActionHandler.scrollDown();
		ActionHandler.wait(2);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSource);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.SourceSelection(source))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Source for an Opportunity");

		ActionHandler.wait(1);
		ActionHandler.click(SVOAdditionalvehicleContainer.SourceInformation);
		ActionHandler.wait(2);
		ActionHandler.scrollDown();
		ActionHandler.wait(1);
		ActionHandler.scrollDown();
		ActionHandler.wait(1);
		// javaScriptUtil.scrollIntoView(SVOAdditionalvehicleContainer.VehicleDetails);
		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeBrand, brand);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeBrandSearch);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(brand))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Brand for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeModel, model);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(model))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Model for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSaveOpportunity);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves SVO Bespoke Opportunity");

		OpportunityName = SVOAdditionalvehicleContainer.OpportunityName.getText();
		Reporter.addStepLog("Opportunity Name is = " + OpportunityName);
	}

	@Then("^User create a new Design Brief Quote with fields like Retailer Contact \"([^\"]*)\"$")
	public void User_create_a_New_Design_Brief_Quote_with_Mandatory_field(String retailerContact) throws Exception {
		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		String quoteName = "SVO Bespoke Test Quote";
		String expirtaionDate = CommonFunctions.selectFutureDate();

		driver.navigate().refresh();
		ActionHandler.wait(10);
		ActionHandler.pageDown();
		ActionHandler.wait(1);

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuotesLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Quotes page");

		ActionHandler.click(SVOAdditionalvehicleContainer.NewQuoteBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on New button to create new Quote");

		ActionHandler.wait(1);
		ActionHandler.click(SVOItemToapproveContainer.QuoteNextBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Next button to create new Quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteNameBox, quoteName + randomNumber);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User writes quote name for new quote");

		ActionHandler.wait(1);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRetailerNameBox, retailerContact);
		ActionHandler.wait(3);
		ActionHandler.click(SVOItemToapproveContainer.QuoteRetailerContactSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		ActionHandler.click(SVOItemToapproveContainer.QuoteRetailerContacts);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters retailer contact details for new quote");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteCostPrice, "123");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Cost Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteListPrice, "203");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters List Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteContribution, "23");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Contribution for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRetailPrice, "213");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Retail Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRevenue, "113");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Revenue for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteExpirationDate, expirtaionDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Expiration Date for new quote");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteSaveBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User save new quote");

		ActionHandler.wait(3);
		DesignBriefRequestname = SVOAdditionalvehicleContainer.FirstQuote.getText();
		Reporter.addStepLog("Design Brief Request id is = " + DesignBriefRequestname);
		ActionHandler.wait(3);

		ActionHandler
				.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.BackToOpportunity(OpportunityName))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate back to the Opportunity");
	}

	@Then("^User submit second Design Brief Request for an approval$")
	public void user_submit_second_Design_Brief_Request_for_an_approval() throws Throwable {

		ActionHandler.scrollDown();
		ActionHandler.wait(1);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuotesLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Quotes Link");

		driver.navigate().refresh();

		ActionHandler.click(SVOItemToapproveContainer.DesignBriefQuotename);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects new quote");

		ActionHandler.scrollDown();
		ActionHandler.scrollDown();

		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Design brief");

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstDesignBrief);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Design brief name");

		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefSubmit);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User submit Design brief successfully");

		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefSaveSubmission);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks save button");

	}

	@Then("^User submit Design Brief Request for an approval$")
	public void user_submit_Design_Brief_Request_for_an_approval() throws Throwable {

		ActionHandler.wait(2);
		driver.navigate().refresh();
		ActionHandler.wait(10);
		ActionHandler.scrollDown();

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuotesLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Quotes Link");

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstQuote);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects new quote");

		ActionHandler.scrollDown();
		ActionHandler.scrollDown();

		ActionHandler.wait(3);
		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Design brief");

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstDesignBrief);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Design brief name");

		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefSubmit);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User submit Design brief successfully");

		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefSaveSubmission);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks save button");

	}

	@And("^User create a new SVO Bespoke Opportunity with mandatory fields like Stage \"([^\"]*)\" Product Offering \"([^\"]*)\" Region \"([^\"]*)\" Client \"([^\"]*)\" Preferred Contact \"([^\"]*)\" Account Name \"([^\"]*)\" Retailer Contact \"([^\"]*)\" Source \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\"$")
	public void User_create_a_new_SVO_Bespoke_Opportunity(String stage, String productOffering, String region,
			String client, String preferredContact, String accountName, String retailerContact, String source,
			String brand, String model) throws Exception {
		String oppName = "Test_SVO_Bespoke_Opportunity_";
		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		String closeDate = CommonFunctions.selectFutureDate();
		Reporter.addStepLog("Opportunity Close Date is = " + closeDate);

		String s[] = stage.split(",");
		stage = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String proOff[] = productOffering.split(",");
		productOffering = CommonFunctions.readExcelMasterData(proOff[0], proOff[1], proOff[2]);

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String c[] = client.split(",");
		client = CommonFunctions.readExcelMasterData(c[0], c[1], c[2]);

		String preCon[] = preferredContact.split(",");
		preferredContact = CommonFunctions.readExcelMasterData(preCon[0], preCon[1], preCon[2]);

		String accName[] = accountName.split(",");
		accountName = CommonFunctions.readExcelMasterData(accName[0], accName[1], accName[2]);

		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		String so[] = source.split(",");
		source = CommonFunctions.readExcelMasterData(so[0], so[1], so[2]);

		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String m[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		Reporter.addStepLog("User starts creating new SVO Bespoke Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.NewOpportunity);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeOppName, oppName + randomNumber);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Name");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeClosedDate, closeDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Closed date");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeStage);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.StageSelection(stage))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Stage for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeProductOffering);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(
				driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ProOfferingSelection(productOffering))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Product Offering for an Opportunity");

		ActionHandler.scrollToView(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		ActionHandler.wait(1);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.RegionSelection(region))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Region for an Opportunity");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.SVOBespokeClient);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeClient, client);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeClientNameSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(client))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Client for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokePreferredContact, preferredContact);
		ActionHandler.click(SVOItemToapproveContainer.SVOBespokePreferredContactname);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Preferred Contact for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeAccountName, accountName);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeAccountNameSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(accountName))));

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Account Name for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeRetailerContact, retailerContact);
		ActionHandler.click(SVOItemToapproveContainer.SVOBespokePreferredContactname);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Retailer Contact for an Opportunity");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSource);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.SourceSelection(source))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Source for an Opportunity");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeBrand, brand);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeBrandSearch);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(brand))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Brand for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeModel, model);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(model))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Model for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSaveOpportunity);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves SVO Bespoke Opportunity");

		OpportunityName = SVOAdditionalvehicleContainer.OpportunityName.getText();
		Reporter.addStepLog("Opportunity Name is = " + OpportunityName);
	}

	@Then("^User navigates to approval history on design brief page$")
	public void user_navigates_to_approval_history_on_design_brief_page() throws Throwable {
		ActionHandler.wait(6);
		ActionHandler.click(SVOItemToapproveContainer.ApprovalHistorySection);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to approval history");

	}

	@Then("^User reassign the design brief approval to the \"([^\"]*)\" user$")
	public void user_reassign_the_design_brief_approval_to_the_user(String ReassignTo) throws Throwable {

		String reassign[] = ReassignTo.split(",");
		ReassignTo = CommonFunctions.readExcelMasterData(reassign[0], reassign[1], reassign[2]);

		ActionHandler.click(SVOItemToapproveContainer.DesignBriefReassignBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Reassign button");

		ActionHandler.click(SVOItemToapproveContainer.DesignBriefReassignUserTextbox);
		ActionHandler.setText(SVOItemToapproveContainer.DesignBriefReassignUserTextbox, ReassignTo);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOItemToapproveContainer.DesignBriefReassignSearchbar);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOItemToapproveContainer.BespokeDesignBriefApproverUsername);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Search and select the user name to be reassigned to");

		ActionHandler.wait(3);
		ActionHandler.click(SVOItemToapproveContainer.DesignBriefApproverRequestReassignBtn);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Reassign the design brief to the user" + ReassignTo);

	}

	//User navigate back to the Opportunity page
	@Then("^User navigate back to the Opportunity page$")
	public void user_navigate_back_to_the_Opportunity_page() throws Throwable {

		javaScriptUtil.clickElementByJS(SVO_OpportunityContainer.OpportTab);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigate to Opportunity");

		ActionHandler.click(SVOItemToapproveContainer.SelectOpportunityListView);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user searches for opportunities list to display");

		ActionHandler.click(SVOItemToapproveContainer.AllBespokeOpportunitiestab);

		Reporter.addStepLog("User chooses to view selected type opportunities");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOItemToapproveContainer.OpportunitysearchBox);
		ActionHandler.setText(SVOItemToapproveContainer.OpportunitysearchBox, OpportunityName);
		ActionHandler.wait(3);
		ActionHandler.pressEnter();

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstOpportunityLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate back to the opportunity page");

	}

	@Then("^User select single design brief request for an approval$")
	public void user_select_single_design_brief_request_for_an_approval() throws Throwable {

		driver.navigate().refresh();
		ActionHandler.wait(10);

		ActionHandler.click(SVOItemToapproveContainer.SortBySubmittedDate);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User sort the design brief request list by submitted date");

		ActionHandler.click(SVOItemToapproveContainer.FirstDesignBriefApprovalRequestCheckbox);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User select single design brief request to approve/reassign");

	}

	@Then("^User select multiple design brief request for reject$")
	public void user_select_multiple_design_brief_request_for_reject() throws Throwable {

		driver.navigate().refresh();
		ActionHandler.wait(10);

		ActionHandler.click(SVOItemToapproveContainer.SVOItemtoapproveSubmittedDatecolumn);
		ActionHandler.wait(3);
		ActionHandler.click(SVOItemToapproveContainer.SVOItemtoapproveSubmittedDatecolumn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User sort the design brief request list by submitted date");

		ActionHandler.click(SVOItemToapproveContainer.FirstDesignBriefApprovalRequestCheckbox);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOItemToapproveContainer.SecondDesignBriefApprovalRequestCheckbox);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User select multiple design brief requests");

	}

	@Then("^User reassign the design brief to the \"([^\"]*)\" user$")
	public void user_reassign_the_design_brief_to_the_user(String ReassignTo) throws Throwable {

		String reassign[] = ReassignTo.split(",");
		ReassignTo = CommonFunctions.readExcelMasterData(reassign[0], reassign[1], reassign[2]);

		ActionHandler.click(SVOItemToapproveContainer.BespokeDesignBriefRequestReassignBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Reassign button");

		ActionHandler.click(SVOItemToapproveContainer.BespokeDesignBriefRequestReassignTextbox);
		ActionHandler.setText(SVOItemToapproveContainer.BespokeDesignBriefRequestReassignTextbox, ReassignTo);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("enter the reassign to user name in the reassign to text box");

		ActionHandler.wait(4);
		ActionHandler.click(SVOItemToapproveContainer.BespokeDesignBriefApprover);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Search and select the user name to be reassigned to");

		ActionHandler.click(SVOItemToapproveContainer.BespokeDesignBriefReassignPopupWindowBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Reassign the design brief to the user" + ReassignTo);

	}

	@Then("^User navigates to the design brief section of an opportunity$")
	public void user_navigates_to_the_design_brief_section_of_an_opportunity() throws Throwable {

		ActionHandler.click(SVOItemToapproveContainer.SelectOpportunityListView);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user searches for opportunities list to display");

		ActionHandler.click(SVOItemToapproveContainer.AllBespokeOpportunitiestab);
		Reporter.addStepLog("User chooses to view selected type opportunities");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOItemToapproveContainer.OpportunitysearchBox);
		ActionHandler.setText(SVOItemToapproveContainer.OpportunitysearchBox, OpportunityName);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enters opportunity name in the opportunity text box");

		ActionHandler.wait(3);
		ActionHandler.pressEnter();
		ActionHandler.wait(5);

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstOpportunityLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Search and selects the opportunity");

		ActionHandler.pageDown();
		ActionHandler.wait(1);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuotesLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Quotes page");

		driver.navigate().refresh();

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstQuote);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects the quote");

		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Design brief");

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstDesignBrief);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Design brief name");
	}

	@Then("^Verify that Design brief approver is updated to reassigned user \"([^\"]*)\"$")
	public void verify_that_Design_brief_approver_is_updated_to_reassigned_user(String ReassignTo) throws Throwable {

		String reassign[] = ReassignTo.split(",");
		ReassignTo = CommonFunctions.readExcelMasterData(reassign[0], reassign[1], reassign[2]);

		if (VerifyHandler
				.verifyElementPresent(driver.findElement(By.xpath(SVOItemToapproveContainer.AssignedTo(ReassignTo))))) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Design brief is reassigned to the user" + ReassignTo);
		} else {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Design brief is not reassigned to the user" + ReassignTo);
		}
	}

	@Then("^Verify that Design brief approver is still assigned to user \"([^\"]*)\"$")
	public void verify_that_Design_brief_approver_is_still_assigned_to_user(String ReassignTo) throws Throwable {

		String reassign[] = ReassignTo.split(",");
		ReassignTo = CommonFunctions.readExcelMasterData(reassign[0], reassign[1], reassign[2]);

		if (VerifyHandler
				.verifyElementPresent(driver.findElement(By.xpath(SVOItemToapproveContainer.AssignedTo(ReassignTo))))) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Design brief is still assigned to the user" + ReassignTo);
		} else {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Design brief is reassigned to another user");
		}
	}

	@Then("^Verify that Design brief request approver is not updated to reassigned user \"([^\"]*)\"$")
	public void verify_that_Design_brief_request_approver_is_not_updated_to_reassigned_user(String ReassignTo)
			throws Throwable {

		String reassign[] = ReassignTo.split(",");
		ReassignTo = CommonFunctions.readExcelMasterData(reassign[0], reassign[1], reassign[2]);

		if (VerifyHandler.verifyElementPresent(driver
				.findElement(By.xpath(SVOItemToapproveContainer.DesignBriefRequestApprovalreassignedto(ReassignTo))))) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Design brief approval request is still assigned to user" + ReassignTo);
		} else {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Design brief is not reassigned to the user" + ReassignTo);
		}
	}

	@Then("^User cancel the reassign after entering reassignto \"([^\"]*)\" user$")
	public void user_cancel_the_reassign_after_entering_reassignTo_user(String ReassignTo) throws Throwable {

		String reassign[] = ReassignTo.split(",");
		ReassignTo = CommonFunctions.readExcelMasterData(reassign[0], reassign[1], reassign[2]);

		ActionHandler.click(SVOItemToapproveContainer.BespokeDesignBriefRequestReassignBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Reassign button");

		ActionHandler.click(SVOItemToapproveContainer.BespokeDesignBriefRequestReassignTextbox);
		ActionHandler.setText(SVOItemToapproveContainer.BespokeDesignBriefRequestReassignTextbox, ReassignTo);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		ActionHandler.click(SVOItemToapproveContainer.BespokeDesignBriefRequestApprover);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Search and select the user name to reassign the design brief request");

		ActionHandler.click(SVOItemToapproveContainer.BespokeDesignBriefCancelPopupWindowBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on cancel after entering reassign to the user" + ReassignTo);

	}

	@Then("^User reassign the design brief to the invalid \"([^\"]*)\" user$")
	public void user_reassign_the_design_brief_to_the_invalid_user(String ReassignTo) throws Throwable {

		String reassign[] = ReassignTo.split(",");
		ReassignTo = CommonFunctions.readExcelMasterData(reassign[0], reassign[1], reassign[2]);

		ActionHandler.click(SVOItemToapproveContainer.BespokeDesignBriefRequestReassignBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Reassign button");

		ActionHandler.click(SVOItemToapproveContainer.BespokeDesignBriefRequestReassignTextbox);
		ActionHandler.setText(SVOItemToapproveContainer.BespokeDesignBriefRequestReassignTextbox, ReassignTo);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Search for an invalid user name");
	}

	@Then("^Verify that user is not able to reassign the design brief approval to invalid user$")
	public void verify_that_user_is_not_able_to_reassign_design_brief_approval_to_invalid_user() throws Throwable {

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("No results found for invalid/inactive user name search");

		ActionHandler.click(SVOItemToapproveContainer.BespokeDesignBriefCancelPopupWindowBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on cancel button");
	}

	@Then("^Verify that user is not able to reassign the design brief approval without adding any user$")
	public void verify_that_user_is_not_able_to_reassign_design_brief_approval_to_without_adding_any_user()
			throws Throwable {

		if (VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.SelectAtleastOneUser)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("An error message is displayed to select atlease one user name to reassign");
		}
		ActionHandler.click(SVOItemToapproveContainer.BespokeDesignBriefCancelPopupWindowBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on cancel button");
	}

	@When("^User navigates to Items to Approve section and select multiple design brief request$")
	public void user_navigates_to_Items_to_Approve_section_and_select_multiple_design_brief_reqests() throws Throwable {

		VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.ItemsToApproveSection);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Item To Approve section is present on Home Page");

		ActionHandler.click(SVOItemToapproveContainer.ManageAllHyperLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Item To Approve page successfully");

		driver.navigate().refresh();
		ActionHandler.click(SVOItemToapproveContainer.BespokeOpsDesignBriefApprovalRequestCheckbox);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User select single design brief request to approve/reassign");
	}

	@Then("^User Reject the multiple design brief approval requests$")
	public void user_Reject_the_multiple_design_brief_approval_requests() throws Throwable {

		ActionHandler.click(SVOItemToapproveContainer.DesignBriefrRequestRejectBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Reject button");

		ActionHandler.setText(SVOItemToapproveContainer.DesignBriefRequestRejectCommentBox, "TestReject");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User added rejection reason in command text box");

		ActionHandler.click(SVOItemToapproveContainer.PopupWindowDesignBriefRequestRejectBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Reject button of Design brief request pop up window");
	}

	@Then("^Verify that Design brief approval request status is updated to Rejected status$")
	public void Verify_that_Design_brief_approval_request_status_is_updated_to_Rejected_status() throws Throwable {

		driver.navigate().refresh();
		if (VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.DesignBriefRequestRejectionStatus)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Design brief approval request status is updated to Rejected status");
		} else {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Verify that Design brief approval request status is updated to Rejected status");
		}
	}

	@Then("^User navigates to the design brief approval requests section of an opportunity$")
	public void user_navigates_to_the_design_brief_approval_requests_section_of_an_opportunity() throws Throwable {

		ActionHandler.click(SVOItemToapproveContainer.SelectOpportunityListView);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user searches for opportunities list to display");

		ActionHandler.click(SVOItemToapproveContainer.AllBespokeOpportunitiestab);
		Reporter.addStepLog("User chooses to view selected type opportunities");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOItemToapproveContainer.OpportunitysearchBox);
		ActionHandler.setText(SVOItemToapproveContainer.OpportunitysearchBox, OpportunityName);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		ActionHandler.pressEnter();

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstOpportunityLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Search and selects the opportunity");

		ActionHandler.pageDown();
		CommonFunctions.attachScreenshot();

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuotesLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Quotes page");

		driver.navigate().refresh();
		ActionHandler.click(SVOItemToapproveContainer.DesignBriefQuotename);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects the quote");

		driver.navigate().refresh();
		ActionHandler.scrollDown();

		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Design brief");

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstDesignBrief);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Design brief name");
	}

	@Then("^User submit the second Design Brief Request for approval$")
	public void user_submit_the_second_Design_Brief_Request_for_an_approval() throws Throwable {

		ActionHandler.scrollDown();
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuotesLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Quotes Link");

		driver.navigate().refresh();
		ActionHandler.click(SVOItemToapproveContainer.DesignBriefQuotename);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects new quote");

		ActionHandler.scrollDown();
		ActionHandler.scrollDown();

		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Design brief");

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstDesignBrief);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Design brief name");

		driver.navigate().refresh();
		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefSubmit);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User submit Design brief successfully");

		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefSaveSubmission);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks save button");

	}

	@Then("^User create second Design Brief Quote with fields like Retailer Contact \"([^\"]*)\"$")
	public void User_create_second_New_Design_Brief_Quote_with_Mandatory_field(String retailerContact)
			throws Exception {
		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		String quoteName = "SVO Bespoke Test Quote";
		String expirtaionDate = CommonFunctions.selectFutureDate();

		ActionHandler.pageDown();
		CommonFunctions.attachScreenshot();

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuotesLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Quotes page");

		ActionHandler.wait(1);
		ActionHandler.click(SVOAdditionalvehicleContainer.NewQuoteBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on New button to create new Quote");

		ActionHandler.wait(1);
		ActionHandler.click(SVOItemToapproveContainer.QuoteNextBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Next button to create new Quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteNameBox, quoteName + randomNumber);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User writes quote name for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRetailerNameBox, retailerContact);
		ActionHandler.wait(3);
		ActionHandler.click(SVOItemToapproveContainer.QuoteRetailerContact);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters retailer contact details for new quote");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteCostPrice, "123");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Cost Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteListPrice, "203");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters List Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteContribution, "23");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Contribution for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRetailPrice, "213");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Retail Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRevenue, "113");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Revenue for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteExpirationDate, expirtaionDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Expiration Date for new quote");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteSaveBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User save new quote");

		ActionHandler.wait(3);
		DesignBriefRequestapprovalname = SVOItemToapproveContainer.DesignBriefQuotename.getText();
		Reporter.addStepLog("Design brief request id is = " + DesignBriefRequestapprovalname);
		ActionHandler.wait(3);
		ActionHandler
				.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.BackToOpportunity(OpportunityName))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate back to the Opportunity");
	}

	@Then("^Verify that user receives notification for Design brief request$")
	public void Verify_that_user_receives_notification_for_Design_brief_request() throws Throwable {

		ActionHandler.wait(6);
		driver.navigate().refresh();
		ActionHandler.click(SVOItemToapproveContainer.DesignBriefRequestsNotificationbar);
		if (VerifyHandler.verifyElementPresent(driver.findElement(
				By.xpath(SVOItemToapproveContainer.DesignBriefApprovalrequestnotification(DesignBriefRequestname))))) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Both the Design Brief approval requests notifications are received");
		} else {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Design Brief Request approval notification is not received");
		}
		ActionHandler.click(SVOItemToapproveContainer.CloseDesignBriefRequestsNotificationBar);
	}

	@Then("^Verify that user receives notification for multiple Design brief requests on rejection$")
	public void Verify_that_user_receives_notification_for_multiple_Design_brief_on_rejection() throws Throwable {

		ActionHandler.click(SVOItemToapproveContainer.DesignBriefRequestsNotificationbar);

		if (VerifyHandler
				.verifyElementPresent(driver.findElement(By.xpath(
						SVOItemToapproveContainer.DesignBriefApprovalrequestnotification(DesignBriefRequestname))))
				&& VerifyHandler.verifyElementPresent(driver.findElement(By.xpath(SVOItemToapproveContainer
						.DesignBriefApprovalrequestnotification(DesignBriefRequestapprovalname))))) {

			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Both the Design Brief approval requests notifications are received");

		} else {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Design Brief Request approval notification is not received");
		}

		ActionHandler.click(SVOItemToapproveContainer.CloseDesignBriefRequestsNotificationBar);
		CommonFunctions.attachScreenshot();
	}

	@Then("^User Reject the design brief approval request from Items to Approve section$")
	public void user_Reject_the_design_brief_approval_requests_from_items_to_approve_section() throws Throwable {

		ActionHandler.click(SVOItemToapproveContainer.DesignBriefrRequestRejectBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Reject button");

		ActionHandler.setText(SVOItemToapproveContainer.DesignBriefRequestRejectCommentBox, "TestReject");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User added rejection reason in command text box");

		ActionHandler.click(SVOItemToapproveContainer.PopupWindowDesignBriefRequestRejectBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Reject button of Design brief request pop up window");
	}

	@Then("^User moves the selected opportunity to Design Brief & Quote stage$")
	public void User_moves_the_selected_Opportunity_to_Design_Brief_Quote_Stage() throws Exception {

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.MarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User moves the Opportunity to Commission from Qualified Stage");

		ActionHandler.wait(6);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.MarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User moves the Opportunity to Design Brief & Quote from Commission Stage");
	}

	@And("^User create and accept the Design brief Quote of an opportunity \"([^\"]*)\"$")
	public void User_create_and_accept_the_design_brief_quote_of_an_opportunity(String retailerContact)
			throws Exception {

		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		double randomNumber = getRandomIntegerBetweenRange(0, 10000);
		String quoteName = "SVO Bespoke Test Quote";
		String expirtaionDate = CommonFunctions.selectFutureDate();

		ActionHandler.wait(2);
		ActionHandler.pageDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuotesLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Quotes page");

		ActionHandler.click(SVOAdditionalvehicleContainer.NewQuoteBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on New button to create new Quote");

		ActionHandler.wait(1);
		ActionHandler.click(SVOItemToapproveContainer.QuoteNextBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Next button to create new Quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteNameBox, quoteName + randomNumber);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User writes quote name for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRetailerNameBox, retailerContact);
		ActionHandler.click(SVOItemToapproveContainer.QuoteRetailerContact);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters retailer contact details for new quote");

		ActionHandler.pageScrollDown();
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteCostPrice, "123");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Cost Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteListPrice, "203");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters List Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteContribution, "23");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Contribution for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRetailPrice, "213");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Retail Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRevenue, "113");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Revenue for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteExpirationDate, expirtaionDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Expiration Date for new quote");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteSaveBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User save new quote");

		ActionHandler.wait(1);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstQuote);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens created quote details");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.DesignBriefLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Design Brief Section");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstDesignBrief);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens first design brief from the list");

		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefSubmit);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Design Brief status moves to Submitted from Draft");

		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefSaveSubmission);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves Design Brief Submission");

		DesignBriefRequestname = SVOAdditionalvehicleContainer.DesignBriefName.getText();
		Reporter.addStepLog("Design Brief Name is = " + DesignBriefRequestname);

		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefReturnRenderPack);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User returns render pack for Design Brief");

		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefSaveRenderPack);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves Design Brief for Return Render Pack");

		javaScriptUtil.clickElementByJS(SVO_OpportunityContainer.OpportTab);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOAdditionalvehicleContainer.OpportunitySearchBox, OpportunityName);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		ActionHandler.pressEnter();

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstOpportunityLink);
		CommonFunctions.attachScreenshot();

		ActionHandler.pageDown();
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuotesLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Quotes page");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstQuoteFromOpportunity);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens created quote details");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Quote status moves to Price stage");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Quote status moves to Proposal Sent stage");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDrpDwn);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeAccepted);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User mark accepeted as Quote Outcome");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDetailDrpDwn);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDetailAccepted);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDoneBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User accepeted the Quote");

		ActionHandler.pageScrollDown();
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuoteOpportunityLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate to Opportunity");
	}

	@Then("^User Approve the design brief approval request$")
	public void user_Approve_the_design_brief_approval_request() throws Throwable {

		ActionHandler.click(SVOItemToapproveContainer.DesignBriefrRequestApproveBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Approve button");

		ActionHandler.setText(SVOItemToapproveContainer.DesignBriefRequestRejectCommentBox, "TestReject");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User added Approval command in command text box");

		ActionHandler.click(SVOItemToapproveContainer.PopupWindowDesignBriefRequestApproveBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Approve button of Design brief request pop up window");
	}

	@Then("^Verify that Design brief approval request status is updated to Approved status$")
	public void Verify_that_Design_brief_approval_request_status_is_updated_to_Approved_status() throws Throwable {

		driver.navigate().refresh();
		if (VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.DesignBriefRequestApprovedStatus)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Design brief approval request status is updated to Approved status");
		} else {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Verify that Design brief approval request status is updated to Approved status");
		}
	}

	@Then("^User select multiple design brief request for approval$")
	public void user_select_multiple_design_brief_request_for_approval() throws Throwable {

		driver.navigate().refresh();
		ActionHandler.click(SVOItemToapproveContainer.FirstDesignBriefApprovalRequestCheckbox);
		ActionHandler.click(SVOItemToapproveContainer.SecondDesignBriefApprovalRequestCheckbox);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		Reporter.addStepLog("User select multiple design brief request to reject");

	}

	@Then("^Verify that Design brief approval request status is Approved from First Approver$")
	public void Verify_that_Design_brief_approval_request_status_is_approved_from_first_approver() throws Throwable {

		driver.navigate().refresh();
		if (VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.DesignBriefRequestFirstApprovedStatus)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Design brief approval request status is approved from First approver");
		} else {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Design brief approval request status is not approved from First approver");
		}
	}

	@Then("^Verify that Design brief approval request status is Approved from all three Approvers$")
	public void Verify_that_Design_brief_approval_request_status_is_approved_from_all_three_approvers()
			throws Throwable {
		Verify_that_Design_brief_approval_request_status_is_approved_from_first_approver();
		Verify_that_Design_brief_approval_request_status_is_approved_from_second_approver();
		Verify_that_Design_brief_approval_request_status_is_approved_from_Third_approver();
	}

	@Then("^Verify that Design brief approval request status is Approved from Second Approver$")
	public void Verify_that_Design_brief_approval_request_status_is_approved_from_second_approver() throws Throwable {

		if (VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.DesignBriefRequestSecondApprovedStatus)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Design brief approval request status is approved from Second approver");
		} else {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Design brief approval request status is not approved from Second approver");
		}
	}

	@Then("^Verify that Design brief approval request status is Approved from Third Approver$")
	public void Verify_that_Design_brief_approval_request_status_is_approved_from_Third_approver() throws Throwable {

		if (VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.DesignBriefRequestThirdApprovedStatus)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Design brief approval request status is approved from third approver");
		} else {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Design brief approval request status is not Approved from third approver");
		}
	}

	@Then("^login to SVO portal as bespoke user \"([^\"]*)\" via setup login$")
	public void login_to_SVO_portal_as_bespoke_user_role_login(String User) throws Throwable {
		String U[] = User.split(",");
		User = CommonFunctions.readExcelMasterData(U[0], U[1], U[2]);

		javaScriptUtil.clickElementByJS(SVOAccountsContainer.setUpBtn);
		ActionHandler.wait(3);
		ActionHandler.click(SVOItemToapproveContainer.SetUpIcon);
		ActionHandler.wait(10);

		NewWindow = driver.getWindowHandle();

		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String SetUpWindow = i.next();
			if (!NewWindow.equalsIgnoreCase(SetUpWindow)) {
				driver.switchTo().window(SetUpWindow);

				ActionHandler.click(SVOAccountsContainer.SearchSetup);
				ActionHandler.wait(2);
				ActionHandler.setText(SVOAccountsContainer.SearchSetup, User);
				ActionHandler.wait(5);

				ActionHandler.click(SVOItemToapproveContainer.DesignBriefApprovalUser);
				Reporter.addStepLog("selects the user role");
				CommonFunctions.attachScreenshot();

				ActionHandler.wait(20);
				driver.navigate().refresh();
				ActionHandler.wait(20);
				driver.switchTo().frame(0);
				ActionHandler.click(SVOItemToapproveContainer.SetUpUserLoginBtn);
				Reporter.addStepLog("User is logged in successfully");
				CommonFunctions.attachScreenshot();

				driver.switchTo().parentFrame();

			}
		}

	}

	@Then("^User Reject the bespoke design brief approval request from design brief pending Approvals section$")
	public void user_Reject_the_bespoke_design_brief_approval_requests_from_design_brief_pending_approvals_section()
			throws Throwable {

		ActionHandler.click(SVOItemToapproveContainer.BespokeDesignBriefrRequestRejectBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Reject button");

		ActionHandler.setText(SVOItemToapproveContainer.DesignBriefRequestRejectCommentBox, "TestReject");
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User added rejection reason in command text box");

		ActionHandler.click(SVOItemToapproveContainer.BespokeRequestPopupWindowRejectBtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Reject button of Design brief request pop up window");
	}

	@Then("^Verify that user receives notification for bespoke Design brief request$")
	public void Verify_that_user_receives_notification_for_bespoke_Design_brief_request() throws Throwable {

		driver.switchTo().parentFrame();
		ActionHandler.click(SVOItemToapproveContainer.DesignBriefRequestsNotificationbar);
		if (VerifyHandler.verifyElementPresent(driver.findElement(
				By.xpath(SVOItemToapproveContainer.DesignBriefApprovalrequestnotification(DesignBriefRequestname))))) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Both the Design Brief approval requests notifications are received");
		} else {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Design Brief Request approval notification is not received");
		}
		ActionHandler.click(SVOItemToapproveContainer.ClosesNotificationBar);
		ActionHandler.wait(3);
	}

	@Then("^logout from SVO Portal as bespoke user$")
	public void logout_from_an_SVO_portal_as_bespoke_user() throws Throwable {

		driver.switchTo().parentFrame();
		ActionHandler.click(SVOItemToapproveContainer.BespokeUserLogoutlink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("logout from SVO Portal as bespoke user");

		driver.close();
		driver.switchTo().window(NewWindow);

	}

	@Then("^User reassign the bespoke design brief to the \"([^\"]*)\" user$")
	public void user_reassign_the_bespoke_design_brief_to_the_user(String ReassignTo) throws Throwable {

		String reassign[] = ReassignTo.split(",");
		ReassignTo = CommonFunctions.readExcelMasterData(reassign[0], reassign[1], reassign[2]);

		ActionHandler.click(SVOItemToapproveContainer.BespokeDesignBriefRequestReassignBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Reassign button");

		ActionHandler.click(SVOItemToapproveContainer.BespokeDesignBriefRequestReassignTextbox);
		ActionHandler.setText(SVOItemToapproveContainer.BespokeDesignBriefRequestReassignTextbox, ReassignTo);
		ActionHandler.wait(4);
		ActionHandler.click(SVOItemToapproveContainer.BespokeDesignBriefRequestApprover);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Search and select the user name to be reassigned to");

		ActionHandler.click(SVOItemToapproveContainer.BespokeDesignBriefReassignPopupWindowBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Reassign the design brief to the user" + ReassignTo);

	}

	@Then("^User cancel the reassign design brief request after entering reassignto \"([^\"]*)\" user$")
	public void user_cancel_the_reassign_design_brief_request_after_entering_reassignTo_user(String ReassignTo)
			throws Throwable {

		String reassign[] = ReassignTo.split(",");
		ReassignTo = CommonFunctions.readExcelMasterData(reassign[0], reassign[1], reassign[2]);

		ActionHandler.click(SVOItemToapproveContainer.BespokeDesignBriefRequestReassignBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Reassign button");

		ActionHandler.click(SVOItemToapproveContainer.BespokeDesignBriefRequestReassignTextbox);
		ActionHandler.setText(SVOItemToapproveContainer.BespokeDesignBriefRequestReassignTextbox, ReassignTo);
		ActionHandler.click(SVOItemToapproveContainer.BespokeDesignBriefRequestApprover);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Search and select the user name to be reassigned to");

		ActionHandler.click(SVOItemToapproveContainer.BespokeDesignBriefCancelPopupWindowBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on cancel after entering reassign to the user" + ReassignTo);

	}

	@Then("^User reassign the bespoke design brief to the invalid \"([^\"]*)\" user$")
	public void user_reassign_the_bespoke_design_brief_to_the_invalid_user(String ReassignTo) throws Throwable {

		String reassign[] = ReassignTo.split(",");
		ReassignTo = CommonFunctions.readExcelMasterData(reassign[0], reassign[1], reassign[2]);

		ActionHandler.click(SVOItemToapproveContainer.BespokeDesignBriefRequestReassignBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Reassign button");

		ActionHandler.click(SVOItemToapproveContainer.BespokeDesignBriefRequestReassignTextbox);
		ActionHandler.setText(SVOItemToapproveContainer.BespokeDesignBriefRequestReassignTextbox, ReassignTo);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Search for an invalid user name");

		ActionHandler.click(SVOItemToapproveContainer.BespokeDesignBriefUserSearchBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on search bar");
	}

	@Then("^Verify that user is not able to reassign the bespoke design brief approval to invalid user$")
	public void verify_that_user_is_not_able_to_reassign_bespoke_design_brief_approval_to_invalid_user()
			throws Throwable {

		VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.noresultsmessage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("No results found message is displayed for invalid user name");

		ActionHandler.click(SVOItemToapproveContainer.BespokeReassignWindowCancelBtn);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOItemToapproveContainer.DesignBriefCancelPopupWindowBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on cancel button");

	}

	@Then("^First Approver approves the bespoke design brief approval request$")
	public void first_approver_Approves_the_design_brief_approval_request() throws Throwable {

		user_navigate_to_design_brief_pending_Approvals_section();
		user_select_single_design_brief_request_for_an_approval();
		ActionHandler.click(SVOItemToapproveContainer.BespokeDesignBriefRequestApproveBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Approve button");

		ActionHandler.setText(SVOItemToapproveContainer.DesignBriefRequestRejectCommentBox, "TestReject");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User added Approval command in command text box");

		ActionHandler.click(SVOItemToapproveContainer.BespokeDesignBriefApprovePopupWindowBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Approve button of Design brief request pop up window");
	}

	@Then("^First Approver approves multiple bespoke design brief approval request$")
	public void first_approver_Approves_multiple_design_brief_approval_request() throws Throwable {

		user_navigate_to_design_brief_pending_Approvals_section();
		user_select_multiple_design_brief_request_for_approval();
		ActionHandler.click(SVOItemToapproveContainer.BespokeDesignBriefRequestApproveBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Approve button");

		ActionHandler.setText(SVOItemToapproveContainer.DesignBriefRequestRejectCommentBox, "TestReject");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User added Approval command in command text box");

		ActionHandler.click(SVOItemToapproveContainer.BespokeDesignBriefApprovePopupWindowBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Approve button of Design brief request pop up window");
	}

	@Then("^Second Approver approves the bespoke design brief approval request$")
	public void second_approver_Approves_the_design_brief_approval_request() throws Throwable {

		user_navigate_to_design_brief_pending_Approvals_section();
		user_select_single_design_brief_request_for_an_approval();
		ActionHandler.click(SVOItemToapproveContainer.BespokeDesignBriefRequestApproveBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Approve button");

		ActionHandler.setText(SVOItemToapproveContainer.DesignBriefRequestRejectCommentBox, "TestReject");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User added Approval command in command text box");

		ActionHandler.click(SVOItemToapproveContainer.BespokeDesignBriefApprovePopupWindowBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Approve button of Design brief request pop up window");
	}

	@Then("^Second Approver approves multiple bespoke design brief approval request$")
	public void second_approver_Approves_multiple_design_brief_approval_request() throws Throwable {

		user_navigate_to_design_brief_pending_Approvals_section();
		user_select_multiple_design_brief_request_for_approval();

		ActionHandler.click(SVOItemToapproveContainer.BespokeDesignBriefRequestApproveBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Approve button");

		ActionHandler.setText(SVOItemToapproveContainer.DesignBriefRequestRejectCommentBox, "TestReject");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User added Approval command in command text box");

		ActionHandler.click(SVOItemToapproveContainer.BespokeDesignBriefApprovePopupWindowBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Approve button of Design brief request pop up window");
	}

	@Then("^Third Approver approves the bespoke design brief approval request$")
	public void third_approver_Approves_the_design_brief_approval_request() throws Throwable {

		ActionHandler.click(SVOItemToapproveContainer.DesignBriefApprovalRequestLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Design brief request");

		ActionHandler.click(SVOItemToapproveContainer.BespokeDesignBriefApproveBtn);

		ActionHandler.setText(SVOItemToapproveContainer.DesignBriefRequestApprovetCommentBox, "TestReject");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User added Approval command in command text box");

		ActionHandler.click(SVOItemToapproveContainer.PopupDesignBriefRequestApproveBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Approve button of Design brief request pop up window");
	}

	@Then("^Third Approver approves second bespoke design brief approval request$")
	public void third_approver_Approves_second_design_brief_approval_request() throws Throwable {

		ActionHandler.click(SVOItemToapproveContainer.DesignBriefApprovalRequestLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Design brief request");

		ActionHandler.click(SVOItemToapproveContainer.BespokeDesignBriefApproveBtn);

		ActionHandler.setText(SVOItemToapproveContainer.DesignBriefRequestApprovetCommentBox, "TestReject");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User added Approval command in command text box");

		ActionHandler.click(SVOItemToapproveContainer.PopupDesignBriefRequestApproveBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User approves first design brief request");

	}

	@Then("^Third Approver approves First bespoke design brief approval request$")
	public void third_approver_Approves_First_design_brief_approval_request() throws Throwable {

		ActionHandler.click(SVOItemToapproveContainer.DesignBriefApprovalRequestLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Design brief request");

		ActionHandler.click(SVOItemToapproveContainer.BespokeDesignBriefApproveBtn);

		ActionHandler.setText(SVOItemToapproveContainer.DesignBriefRequestApprovetCommentBox, "TestReject");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User added Approval command in command text box");

		ActionHandler.click(SVOItemToapproveContainer.PopupDesignBriefRequestApproveBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User approves first design brief request");

	}

	@Then("^User navigates to the design brief request details section$")
	public void User_navigates_to_the_design_brief_request_details_section() throws Throwable {

		driver.navigate().refresh();
		ActionHandler.wait(10);
		ActionHandler.click(SVOItemToapproveContainer.DesignBriefnameLink);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to the design brief request details section");
		user_navigates_to_approval_history_on_design_brief_page();
	}

	@Then("^User hits on reassign without adding reassign user on design brief pending approvals page$")
	public void user_hits_on_reassign_without_adding_reassign_user_on_design_brief_pending_approvals_page()
			throws Throwable {

		ActionHandler.click(SVOItemToapproveContainer.BespokeDesignBriefRequestReassignBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Reassign button");

		ActionHandler.click(SVOItemToapproveContainer.BespokeDesignBriefRequestReassignTextbox);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOItemToapproveContainer.BespokeDesignBriefReassignPopupWindowBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User hits on Reassign without adding user name");

	}

	@Then("^Verify an error message displayed as to select atleast one user for reassign on hitting reassign without adding user$")
	public void Verify_an_error_message_displayed_as_to_select_atleast_one_user_for_reassign_on_hitting_reassign_without_adding_user()
			throws Throwable {

		if (VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.ErrorMessageToselectAtleastOneUser)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Error message is displayed as to select atleast one user for reassigning");
		} else {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Error message is not displayed as to select atleast one user for reassigning");
		}
		ActionHandler.click(SVOItemToapproveContainer.ReassignWindowCancelBtn);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOItemToapproveContainer.ReassignWindowCancelBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on cancel button");

	}

	@Then("^User verifies the design brief request details on opportunity page")
	public void User_verifies_the_design_brief_request_details_on_opportunity_page() throws Throwable {

		ActionHandler.click(SVOItemToapproveContainer.Opportunityhyperlink);

		ActionHandler.pageDown();

		CommonFunctions.attachScreenshot();
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuotesLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Quotes page");

		driver.navigate().refresh();

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstQuote);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects the quote");

		driver.navigate().refresh();
		ActionHandler.scrollDown();

		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Design brief");

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstDesignBrief);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Design brief name");

		user_navigates_to_approval_history_on_design_brief_page();
		submittedbyuser = SVOItemToapproveContainer.RequestSubmittedusername.getText();
		Reporter.addStepLog("Design Brief Request is submitted by  = " + submittedbyuser);

	}

	@When("^User navigate to Design Brief Pending Approvals section$")
	public void user_navigate_to_Design_Brief_Pending_Approvals_section() throws Throwable {

		VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.SVODesignBriefPendingApprovals);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Design Brief pending Approvals section is present on Home Page");

		javaScriptUtil.clickElementByJS(SVOItemToapproveContainer.ManageAll);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Design Brief pending Approvals page successfully");

	}

	@Then("^User is able to navigate to respective Opportunity Related To and Submitted By on Design Brief pending approvals page$")
	public void user_is_able_to_navigate_to_respective_Opportunity_Related_To_and_Submitted_By_on_Design_Brief_pending_approvals_page()
			throws Throwable {

		driver.navigate().refresh();
		ActionHandler.wait(10);

		/*
		 * String outlookWindow = driver.getWindowHandle();
		 * ActionHandler.click(SVOItemToapproveContainer.FirstRelatedToLink);
		 * ActionHandler.wait(12); Set<String> outlookWindows
		 * =driver.getWindowHandles(); Iterator<String> i = outlookWindows.iterator();
		 * while (i.hasNext()) { String mailWindow = i.next();
		 * if(!outlookWindow.equalsIgnoreCase(mailWindow)) {
		 * driver.switchTo().window(mailWindow);
		 * VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.
		 * VerifyDesignBriefdetails); CommonFunctions.attachScreenshot();
		 * DesignBriefRequestname=SVOItemToapproveContainer.VerifyDesignBriefdetails.
		 * getText(); Reporter.addStepLog("Design brief request Name is = " +
		 * DesignBriefRequestname); driver.close();
		 * driver.switchTo().window(outlookWindow); } }
		 */

		javaScriptUtil.clickElementByJS(SVOItemToapproveContainer.FirstRelatedToLink);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User is able to navigate to Related To link");

		VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.VerifyDesignBriefdetails);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User is able to view design brief details");

		javaScriptUtil.clickElementByJS(SVOItemToapproveContainer.Hometab);
		CommonFunctions.attachScreenshot();
		javaScriptUtil.clickElementByJS(SVOItemToapproveContainer.ManageAll);
		CommonFunctions.attachScreenshot();

		driver.navigate().refresh();
		ActionHandler.wait(10);
		javaScriptUtil.clickElementByJS(SVOItemToapproveContainer.FirstOpportunityLink);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User is able to navigate to Opportunity link");

		VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.VerifyOpportunityName);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User is able to verify opportunity name");

		javaScriptUtil.clickElementByJS(SVOItemToapproveContainer.Hometab);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		javaScriptUtil.clickElementByJS(SVOItemToapproveContainer.ManageAll);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		driver.navigate().refresh();
		ActionHandler.wait(10);
		javaScriptUtil.clickElementByJS(SVOItemToapproveContainer.FirstSubmittedByLink);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User is able to navigate to Submitted By link");

		VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.VerifySubmittedUser);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User is able to verify username whose submitted design brief request for approval");

		javaScriptUtil.clickElementByJS(SVOItemToapproveContainer.Hometab);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		javaScriptUtil.clickElementByJS(SVOItemToapproveContainer.ManageAll);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);

	}

	@Then("^User selects multiple design brief request from the list in order to approve/reject/reassign from Design Brief Pending Approvals page$")
	public void user_selects_multiple_design_brief_request_from_the_list_in_order_to_approve_reject_reassign_from_Design_Brief_Pending_Approvals_page()
			throws Throwable {
		user_select_multiple_design_brief_request_for_reject();
	}

	@Then("^User verifies the details of design brief Request from Design Brief Pending Approvals page$")
	public void user_navigates_View_Approval_Request_from_Design_Brief_Pending_Approvals_page() throws Throwable {

		ActionHandler.click(SVOItemToapproveContainer.DesignBriefApprovalrequestLink);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"User selects design brief approval request from dropdown on Design Brief pending Approvals page");

		VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.ViewApprovalrequestdetails);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Views Approval request details on Design Brief pending Approvals page");
	}

	@When("^User create a SVO Private Office Opportunity with fields like Stage \"([^\"]*)\" Product Offering \"([^\"]*)\" Region \"([^\"]*)\" Preferred Contact \"([^\"]*)\" Account Name \"([^\"]*)\" Retailer Contact \"([^\"]*)\" Source \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\"$")
	public void user_create_a_SVO_Private_Office_Opportunity(String stage, String productOffering, String region,
			String preferredContact, String accountName, String retailerContact, String source, String brand,
			String model) throws Exception {
		String oppName = "Test_SVO_Bespoke_Opportunity_";
		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		String closeDate = CommonFunctions.selectFutureDate();
		Reporter.addStepLog("Opportunity Close Date is = " + closeDate);

		String s[] = stage.split(",");
		stage = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String proOff[] = productOffering.split(",");
		productOffering = CommonFunctions.readExcelMasterData(proOff[0], proOff[1], proOff[2]);

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String preCon[] = preferredContact.split(",");
		preferredContact = CommonFunctions.readExcelMasterData(preCon[0], preCon[1], preCon[2]);

		String accName[] = accountName.split(",");
		accountName = CommonFunctions.readExcelMasterData(accName[0], accName[1], accName[2]);

		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		String so[] = source.split(",");
		source = CommonFunctions.readExcelMasterData(so[0], so[1], so[2]);

		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String m[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		Reporter.addStepLog("User starts creating new SVO Private Office Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.NewOpportunity);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOItemToapproveContainer.PrivateOfficeOpportunity);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOItemToapproveContainer.QuoteNextBtn);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeOppName, oppName + randomNumber);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Name");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeClosedDate, closeDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Closed date");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeStage);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.StageSelection(stage))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Stage for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeProductOffering);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(
				driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ProOfferingSelection(productOffering))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Product Offering for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.RegionSelection(region))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Region for an Opportunity");

		VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.SVOBespokePreferredContact);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokePreferredContact, preferredContact);
		ActionHandler.click(SVOItemToapproveContainer.SVOBespokePreferredContactname);
		Reporter.addStepLog("User selects Preferred Contact for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeAccountName, accountName);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeAccountNameSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(accountName))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Account Name for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeRetailerContact, retailerContact);
		ActionHandler.click(SVOItemToapproveContainer.SVOBespokePreferredContactname);
		Reporter.addStepLog("User selects Retailer Contact for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSource);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.SourceSelection(source))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Source for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeBrand, brand);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeBrandSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(brand))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Brand for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeModel, model);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(model))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Model for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSaveOpportunity);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves SVO Bespoke Opportunity");

		OpportunityName = SVOAdditionalvehicleContainer.OpportunityName.getText();
		Reporter.addStepLog("Opportunity Name is = " + OpportunityName);
	}

	@Then("^Verify that Approval status is set to be pending Approval on Approval history page$")
	public void verify_that_Approval_status_is_set_to_be_pending_Approval_on_Approval_history_page() throws Throwable {

		ActionHandler.click(SVOItemToapproveContainer.StepLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Design brief name");

		VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.PendingApprovalstatusonprocessinstancesteppage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"User verifies that Approval status set to be Pending approval on Process instance step page");
	}

	@Then("^User verifies that all different fields are available under Design Brief Pending Approvals section$")
	public void user_verifies_that_all_different_fields_are_available_under_Design_Brief_Pending_Approvals_section()
			throws Throwable {

		driver.navigate().refresh();
		VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.SVOItemtoapproveRelatedTocolumn);
		ActionHandler.click(SVOItemToapproveContainer.SVOItemtoapproveRelatedTocolumn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies that 'Relate To' field is present on Design Brief pending Approvals page");

		ActionHandler.click(SVOItemToapproveContainer.SVOItemtoapproveRelatedTocolumn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies that sort the data on Design Brief pending Approvals page");

		VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.SVOItemtoapproveTypecolumn);
		ActionHandler.click(SVOItemToapproveContainer.SVOItemtoapproveTypecolumn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies that 'Type' field is present on Design Brief pending Approvals page");

		ActionHandler.click(SVOItemToapproveContainer.SVOItemtoapproveTypecolumn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies that sort the data on Design Brief pending Approvals page");

		VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.SVOItemtoapproveSubmittedBycolumn);
		ActionHandler.click(SVOItemToapproveContainer.SVOItemtoapproveSubmittedBycolumn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"User verifies that 'Submitted By' field is present on Design Brief pending Approvals page");

		ActionHandler.click(SVOItemToapproveContainer.SVOItemtoapproveSubmittedBycolumn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies that sort the data on Design Brief pending Approvals page");

		VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.SVOItemtoapproveSubmittedDatecolumn);
		ActionHandler.click(SVOItemToapproveContainer.SVOItemtoapproveSubmittedDatecolumn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"User verifies that 'Submitted Date' field is present on Design Brief pending Approvals page");

		ActionHandler.click(SVOItemToapproveContainer.SVOItemtoapproveSubmittedDatecolumn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies that sort the data on Design Brief pending Approvals page");

		VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.SVOItemtoapproveCommentcolumn);
		ActionHandler.click(SVOItemToapproveContainer.SVOItemtoapproveCommentcolumn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies that 'Comment' field is present on Design Brief pending Approvals page");

		ActionHandler.click(SVOItemToapproveContainer.SVOItemtoapproveCommentcolumn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies that sort the data on Design Brief pending Approvals page");

		VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.SVOItemtoapproveDerivativeCodecolumn);
		ActionHandler.click(SVOItemToapproveContainer.SVOItemtoapproveDerivativeCodecolumn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"User verifies that 'Derivative code' field is present on Design Brief pending Approvals page");

		ActionHandler.click(SVOItemToapproveContainer.SVOItemtoapproveDerivativeCodecolumn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies that sort the data on Design Brief pending Approvals page");

		VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.SVOItemtoapproveOpportunitycolumn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"User verifies that 'Opportunity Code' field is present on Design Brief pending Approvals page");

		ActionHandler.click(SVOItemToapproveContainer.SVOItemtoapproveOpportunitycolumn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies that sort the data on Design Brief pending Approvals page");

	}

	@Then("^User Verify that Approve, Reject and Reassign buttons are present on Design Brief Pending Approvals page$")
	public void user_Verify_that_Approve_Reject_and_Reassign_buttons_are_present_on_Design_Brief_Pending_Approvals_page()
			throws Throwable {

		VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.ApproveButton);
		VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.RejectButton);
		VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.ReassignButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"User verifies that 'Approve','Reject' and 'Reassign' Buttons are present on Design Brief pending Approvals page");
	}

	@Then("^User views design brief approval requests details from Design Brief Pending Approvals page$")
	public void User_views_design_brief_approval_requests_details_from_Design_Brief_Approvals_page() throws Throwable {

		VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.SVOItemtoapproveRelatedTocolumn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies that sort the data on Design Brief pending Approvals page");

		ActionHandler.click(SVOItemToapproveContainer.DesignBriefApprovalRequest);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Design brief request on Design Brief pending Approvals page");

		VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.DesignbriefQuoteID);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Views Design Brief request details on Design Brief pending Approvals page");
	}

	@Given("^User is not able to view selected design brief's status on Items To Approval page$")
	public void user_is_not_able_to_view_selected_design_brief_s_status_on_Items_To_Approval_page() throws Throwable {

		if (!VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.SVODesignBriefStatus)) {
			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("design brief's status is not present");
		} else {
			Reporter.addStepLog("design brief's status is present");
		}
	}

	@Then("^User not be able to view created Bespoke Design Brief request on Design Brief Pending Approvals page$")
	public void user_not_be_able_to_view_created_Bespoke_Design_Brief_request_on_Items_to_Approve_page()
			throws Throwable {
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.wait(1);
		javaScriptUtil.clickElementByJS(SVOItemToapproveContainer.QuotesLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Quotes Link");
		ActionHandler.wait(1);

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstQuote);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects new quote");

		ActionHandler.scrollDown();
		ActionHandler.wait(1);

		VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.DesignBriefLink);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Design Brief quote for approval");

		javaScriptUtil.clickElementByJS(SVOItemToapproveContainer.backtoHome);
		CommonFunctions.attachScreenshot();

		VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.SVODesignBriefPendingApprovals);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);

		javaScriptUtil.clickElementByJS(SVOItemToapproveContainer.ManageAll);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Design Brief Pending approvals page");

		if (!VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.FirstQuote)) {

			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog(
					"User not be able to view created Bespoke Design Brief request on Design Brief Pending approvals page");

		} else {
			Reporter.addStepLog(
					"User able to view created Bespoke Design Brief request on Design Brief Pending approvals page");
		}

	}

	@And("^User create new SVO Private Office Opportunity with mandatory fields like Stage \"([^\"]*)\" Product Offering \"([^\"]*)\" Region \"([^\"]*)\" Client \"([^\"]*)\" Preferred Contact \"([^\"]*)\" Account Name \"([^\"]*)\" Retailer Contact \"([^\"]*)\" Source \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\"$")
	public void User_create_new_SVO_Private_Office_Opportunity(String stage, String productOffering, String region,
			String client, String preferredContact, String accountName, String retailerContact, String source,
			String brand, String model) throws Exception {
		String oppName = "Test_SVO_Bespoke_Opportunity_";
		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		String closeDate = CommonFunctions.selectFutureDate();
		Reporter.addStepLog("Opportunity Close Date is = " + closeDate);

		String s[] = stage.split(",");
		stage = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String proOff[] = productOffering.split(",");
		productOffering = CommonFunctions.readExcelMasterData(proOff[0], proOff[1], proOff[2]);

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String c[] = client.split(",");
		client = CommonFunctions.readExcelMasterData(c[0], c[1], c[2]);

		String preCon[] = preferredContact.split(",");
		preferredContact = CommonFunctions.readExcelMasterData(preCon[0], preCon[1], preCon[2]);

		String accName[] = accountName.split(",");
		accountName = CommonFunctions.readExcelMasterData(accName[0], accName[1], accName[2]);

		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		String so[] = source.split(",");
		source = CommonFunctions.readExcelMasterData(so[0], so[1], so[2]);

		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String m[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		Reporter.addStepLog("User starts creating new SVO Private Office Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.NewOpportunity);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOItemToapproveContainer.PrivateOfficeOpportunity);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOItemToapproveContainer.QuoteNextBtn);
		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeOppName, oppName + randomNumber);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Name");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeClosedDate, closeDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Closed date");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeStage);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.StageSelection(stage))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Stage for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeProductOffering);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(
				driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ProOfferingSelection(productOffering))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Product Offering for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.RegionSelection(region))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Region for an Opportunity");

		VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.SVOBespokePreferredContact);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokePreferredContact, preferredContact);
		ActionHandler.click(SVOItemToapproveContainer.SVOBespokePreferredContactname);
		Reporter.addStepLog("User selects Preferred Contact for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeAccountName, accountName);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeAccountNameSearch);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(accountName))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Account Name for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeRetailerContact, retailerContact);
		ActionHandler.click(SVOItemToapproveContainer.SVOBespokePreferredContactname);
		Reporter.addStepLog("User selects Retailer Contact for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSource);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.SourceSelection(source))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Source for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeBrand, brand);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeBrandSearch);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(brand))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Brand for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeModel, model);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(model))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Model for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSaveOpportunity);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves SVO Bespoke Opportunity");

		OpportunityName = SVOAdditionalvehicleContainer.OpportunityName.getText();
		Reporter.addStepLog("Opportunity Name is = " + OpportunityName);
	}

	@Then("^User not able to submit Private Office Design Brief for an approval$")
	public void user_not_able_to_submit_Private_Office_Design_Brief_for_an_approval() throws Throwable {

		driver.navigate().refresh();
		ActionHandler.wait(10);
		ActionHandler.scrollDown();
		ActionHandler.wait(1);
		javaScriptUtil.clickElementByJS(SVOItemToapproveContainer.QuotesLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Quotes Link");

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstQuote);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects new quote");

		ActionHandler.scrollDown();

		if (!VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.DesignBriefLink)) {
			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User not able to view design brief quote for approval");
		} else {
			Reporter.addStepLog("User able to view design brief for quote approval");
		}

		javaScriptUtil.clickElementByJS(SVOItemToapproveContainer.backtoHome);
		CommonFunctions.attachScreenshot();

		VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.SVODesignBriefPendingApprovals);
		CommonFunctions.attachScreenshot();

		ActionHandler.scrollDown();
		ActionHandler.wait(1);

		javaScriptUtil.clickElementByJS(SVOItemToapproveContainer.ManageAll);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Design Brief Pending approvals page");

		if (!VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.FirstQuote)) {

			CommonFunctions.attachScreenshot();
			Reporter.addStepLog(
					"User is not be able to view Private Office Design Brief request on Design Brief Pending approvals page");

		} else {

			CommonFunctions.attachScreenshot();
			Reporter.addStepLog(
					"User is able to view Private Office Design Brief request on Design Brief Pending approvals page");
		}
	}

	@Then("^User not be able to view deleted Bespoke Design Brief Request on Design Brief Pending Approvals page$")
	public void user_not_be_able_to_view_deleted_Bespoke_Design_Brief_Request_on_Design_Brief_Pending_Approvals_page()
			throws Throwable {
		ActionHandler.scrollDown();
		ActionHandler.wait(1);

		javaScriptUtil.clickElementByJS(SVOItemToapproveContainer.QuotesLink);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Quotes Link");

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstQuote);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects new quote");

		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.wait(1);

		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Design brief");

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstDesignBrief);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Design brief name");

		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefSubmit);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User submit Design brief successfully");

		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefSaveSubmission);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks save button");

		ActionHandler.wait(2);
		driver.navigate().refresh();
		ActionHandler.click(SVOItemToapproveContainer.DesignbriefQuoteID);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to quote");

		ActionHandler.scrollDown();
		ActionHandler.wait(1);

		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Design brief");

		ActionHandler.click(SVOItemToapproveContainer.DesignBriefDropdown);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Design brief");

		ActionHandler.click(SVOItemToapproveContainer.DesignBriefDelete);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Design brief");

		VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.SVODesignBriefPendingApprovals);
		CommonFunctions.attachScreenshot();

		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		javaScriptUtil.clickElementByJS(SVOItemToapproveContainer.ManageAll);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Design Brief Pending approvals page");

		if (!VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.FirstQuote)) {

			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog(
					"User is not able to view deleted Bespoke Design Brief request on Design Brief Pending approvals page");

		} else {
			Reporter.addStepLog(
					"User is able to view deleted Bespoke Design Brief request on Design Brief Pending approvals pagel");
		}
	}

	@Then("^User clicks on Approve/Reject/Reassign button without selecting any Design Brief request and validate error messege$")
	public void user_clicks_on_Approve_Reject_Reassign_button_without_selecting_any_Design_Brief_request_and_validate_error_messege()
			throws Throwable {

		VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.SVODesignBriefPendingApprovals);
		CommonFunctions.attachScreenshot();

		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		javaScriptUtil.clickElementByJS(SVOItemToapproveContainer.ManageAll);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Design Brief Pending approvals page");

		ActionHandler.click(SVOItemToapproveContainer.ApproveButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks Approve button");

		VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.ApprovalrequestErrormessege);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the error messege");

		ActionHandler.click(SVOItemToapproveContainer.RejectButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks Reject button");

		VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.ApprovalrequestErrormessege);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the error messege");

		ActionHandler.click(SVOItemToapproveContainer.ReassignButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks Reassign button");

		VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.ApprovalrequestErrormessege);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the error messege");
	}

	@When("^User verify that Design brief pending approval section is not present on the Home page$")
	public void user_verify_that_Design_brief_pending_approval_section_is_not_present_on_the_Home_page()
			throws Throwable {
		if (!VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.SVODesignBriefPendingApprovals)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Design brief pending approval section is not present on the Home page");
			CommonFunctions.attachScreenshot();
		} else {
			Reporter.addStepLog("Design brief pending approval section is present on the Home page");
			CommonFunctions.attachScreenshot();
		}
	}

	@When("^User verify that there is no any Design Brief request present on Design Brief Pending Approvals page$")
	public void user_verify_that_there_is_no_any_Design_Brief_request_present_on_Design_Brief_Pending_Approvals_page()
			throws Throwable {

		if (!VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.ApprovalrequestQuoteID)) {

			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog(
					"Verifies that there is no any Design Brief request present on Design Brief Pending approvals page");

		} else {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog(
					"Verifies that there is a Design Brief request present on Design Brief Pending approvals page");
		}
	}

	@Then("^User verify that Approve/Reject/Reassign button is disabled$")
	public void user_verify_that_Approve_Reject_Reassign_button_is_disabled() throws Throwable {

		if (!VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.ApproveButton)) {

			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Approve button is enabled on Design Brief Pending approvals page");

		} else {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Approve button is disabled on Design Brief Pending approvals page");
		}

		if (!VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.RejectButton)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Reject button is enabled on Design Brief Pending approvals page");
		} else {
			Reporter.addStepLog("Reject button is disabled on Design Brief Pending approvals page");
		}

		if (!VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.ReassignButton)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Reassign button is enabled on Design Brief Pending approvals page");
		} else {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Reassign button is disabled on Design Brief Pending approvals page");
		}
	}

	@And("^User is not able to view selected design brief's status on Design Brief pending Approvals page$")
	public void user_is_not_able_to_view_selected_design_brief_s_status_on_Design_Brief_pending_Approvals_page()
			throws Throwable {

		if (!VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.SVODesignBriefStatus)) {
			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("design brief's status is not present");
		} else {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("design brief's status is present");
		}
	}

	@Given("^laterally Access SVO Portal with user name \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void laterally_Access_SVO_Portal_for_specific_User(String UserName, String Password) throws Exception {
		driver = getDriver();
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);

		ActionHandler.wait(2);
		if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.userNameTextBox)) {
			ActionHandler.wait(1);
			ActionHandler.click(SVOEnquiryContainer.icon_image);
			ActionHandler.wait(6);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Click on Logout");
			ActionHandler.click(SVOEnquiryContainer.Logout);
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logout from SVO Portal");
		}

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		Reporter.addStepLog("User Logins to SVO Portal");

		String s[] = UserName.split(",");
		UserName = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String p[] = Password.split(",");
		Password = CommonFunctions.readExcelMasterData(p[0], p[1], p[2]);

		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, UserName);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, Password);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();

		ActionHandler.waitForElement(SVOEnquiryContainer.SVOText, 60);
		CommonFunctions.attachScreenshot();
		VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
		Reporter.addStepLog("User navigate to SVO Home Page successfully");
	}

	@Then("^User select design brief request for an approval$")
	public void user_select_design_brief_request_for_an_approval() throws Throwable {

		driver.navigate().refresh();
		ActionHandler.wait(10);

		ActionHandler.click(SVOItemToapproveContainer.SubmittedDateList);
		ActionHandler.wait(3);
		ActionHandler.click(SVOItemToapproveContainer.SubmittedDateList);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User sort the design brief request list by submitted date");

		ActionHandler.click(SVOItemToapproveContainer.FirstDesignBriefApprovalRequestCheckbox);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User select single design brief request to approve/reassign");

	}

}
