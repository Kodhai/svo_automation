package com.jlr.svo.tests;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.containers.SSEC_1226_CaseCreationEmail_Container;
import com.jlr.svo.containers.SSEC_2119_AdditionalVehicleName_AutoNumberContainer;
import com.jlr.svo.containers.SSEC_2221_Mass_upload_ETracker_URL_AdditionalVehicleContainer;
import com.jlr.svo.containers.SVOAccountsContainer;
import com.jlr.svo.containers.SVOAdditionalVehicleContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.containers.SVOItemToApproveContainer;
import com.jlr.svo.containers.SVOOpportunityContainer;
import com.jlr.svo.containers.SVO_CustomerResponsesContainer;
import com.jlr.svo.containers.SVO_EnquiryLostReasonContainer;
import com.jlr.svo.containers.SVO_RestrictedPartyScreeningContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class SSEC_2221_Mass_upload_ETracker_URL_AdditionalVehicle extends TestBaseCC {
	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory.getLogger(SSEC_1226_CaseCreationEmail_Test.class.getName());
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);

	SVOAccountsContainer SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
	SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	SVOAdditionalVehicleContainer SVOAdditionalvehicleContainer = PageFactory.initElements(driver,
			SVOAdditionalVehicleContainer.class);
	SVO_RestrictedPartyScreeningContainer RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
			SVO_RestrictedPartyScreeningContainer.class);
	SVOItemToApproveContainer SVOItemToapproveContainer = PageFactory.initElements(driver,
			SVOItemToApproveContainer.class);
	SVO_CustomerResponsesContainer SVO_CustomerResponsesContainer = PageFactory.initElements(driver,
			SVO_CustomerResponsesContainer.class);
	SVO_EnquiryLostReasonContainer SVO_EnquiryLostReasonContainer = PageFactory.initElements(driver,
			SVO_EnquiryLostReasonContainer.class);
	SSEC_1226_CaseCreationEmail_Container CaseCreationEmailContainer = PageFactory.initElements(driver,
			SSEC_1226_CaseCreationEmail_Container.class);
	SSEC_2119_AdditionalVehicleName_AutoNumberContainer AdditionalVehicleName_AutoNumberContainer = PageFactory.initElements(driver, SSEC_2119_AdditionalVehicleName_AutoNumberContainer.class);
	SSEC_2221_Mass_upload_ETracker_URL_AdditionalVehicleContainer Mass_upload_ETracker_URL_AdditionalVehicleContainer = PageFactory.initElements(driver, SSEC_2221_Mass_upload_ETracker_URL_AdditionalVehicleContainer.class);
	
	public static String verificationCode;
	public static String vCode;
	public static String veriCode;
	public String NewWindow;
	public static String CaseCreated;
	public static String CaseNumber;

	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	public void onStart() {

		setupTest("SVOTest");
		driver = getDriver();

		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
		SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
		SVOAdditionalvehicleContainer = PageFactory.initElements(driver, SVOAdditionalVehicleContainer.class);

		RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
				SVO_RestrictedPartyScreeningContainer.class);
		SVOItemToapproveContainer = PageFactory.initElements(driver, SVOItemToApproveContainer.class);
		SVO_CustomerResponsesContainer = PageFactory.initElements(driver, SVO_CustomerResponsesContainer.class);
		SVO_EnquiryLostReasonContainer = PageFactory.initElements(driver, SVO_EnquiryLostReasonContainer.class);
		CaseCreationEmailContainer = PageFactory.initElements(driver, SSEC_1226_CaseCreationEmail_Container.class);
		AdditionalVehicleName_AutoNumberContainer = PageFactory.initElements(driver, SSEC_2119_AdditionalVehicleName_AutoNumberContainer.class);
		Mass_upload_ETracker_URL_AdditionalVehicleContainer = PageFactory.initElements(driver, SSEC_2221_Mass_upload_ETracker_URL_AdditionalVehicleContainer.class);
		
		driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);

		verificationCode = null;
		vCode = null;
		veriCode = null;
		NewWindow = null;
		CaseCreated = null;
		CaseNumber = null;

	}

    @And("^Creates a new Additional Vehicle in Opportunity tab$")
    public void Creates_a_New_Additional_Vehicles() throws Exception{
    	
    	ActionHandler.click(AdditionalVehicleName_AutoNumberContainer.NewBtnAdditionalVehicle);
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Click on New Button of Additional Vehicle tab");
        
    	
    }
	
    @Then("^Fill Brand Model and ETracker URL$")
    public void Fill_Brand_Model_and_ETracker__URL(String BrandAdd, String ModelAdd) throws Exception{
    	
    	String Brd[] = BrandAdd.split(",");
    	BrandAdd = CommonFunctions.readExcelMasterData(Brd[0], Brd[1], Brd[2]);

    	String Mod[] = ModelAdd.split(",");
    	ModelAdd = CommonFunctions.readExcelMasterData(Mod[0], Mod[1], Mod[2]);

    	String Brand = "Land Rover";
    	String Model = "Defender";
    	String ETRackerURL = "test.test.com, test.test1.com, test.test.com, test.test1.com,"
    			+ "test.test.com, test.test1.com";
    	
    	ActionHandler.setText(Mass_upload_ETracker_URL_AdditionalVehicleContainer.BrandAdditionalVehicle, Brand);
    	ActionHandler.click(driver.findElement(By.xpath(Mass_upload_ETracker_URL_AdditionalVehicleContainer.ETracker(BrandAdd))));
    	
    	ActionHandler.setText(Mass_upload_ETracker_URL_AdditionalVehicleContainer.ModelAdditionalVehicle, Model);
    	ActionHandler.click(driver.findElement(By.xpath(Mass_upload_ETracker_URL_AdditionalVehicleContainer.ETracker(ModelAdd))));
    	
    	ActionHandler.pageCompleteScrollDown();

    	ActionHandler.setText(Mass_upload_ETracker_URL_AdditionalVehicleContainer.EtrackerURLlabel, ETRackerURL);
    	
    
    }

    @And("^Fill ETracker ID into the respective field$")
    public void Fill_ETracker_ID_into_respective_field() throws Exception{
    	
    	String ETrackerID = "1234";
    	ActionHandler.setText(Mass_upload_ETracker_URL_AdditionalVehicleContainer.ETRackerIDLabel, ETrackerID);

    	ActionHandler.click(AdditionalVehicleName_AutoNumberContainer.SaveEditAdditionalVehicle);
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Click on Save button of Additional Vehicle tab");
        
    }
    
    @And("^User navigates to Setup window and upload Mass upload csv file$")
    public void User_navigates_to_Setup_window() throws Exception{
    	
    	ActionHandler.click(Mass_upload_ETracker_URL_AdditionalVehicleContainer.SetupBtn);
    	ActionHandler.wait(2);
    	
    	Actions action = new Actions(driver);
    	action.sendKeys(Keys.ARROW_DOWN).build().perform();
    	ActionHandler.wait(1);
    	ActionHandler.pressEnter();
    	ActionHandler.wait(1);
    	
    	String NewWindow = driver.getWindowHandle();

		Set<String> NewWindows = driver.getWindowHandles();
		Iterator<String> i = NewWindows.iterator();
		while (i.hasNext()) {
			String Window = i.next();
			if (!NewWindow.equalsIgnoreCase(Window)) {
				driver.switchTo().window(Window);
			}
		}

		String User = "Mike King";

		ActionHandler.wait(10);
		ActionHandler.click(Mass_upload_ETracker_URL_AdditionalVehicleContainer.SearchSetupBox);
		ActionHandler.wait(3);
		ActionHandler.setText(Mass_upload_ETracker_URL_AdditionalVehicleContainer.SearchSetupBox, User);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(6);
		ActionHandler.click(Mass_upload_ETracker_URL_AdditionalVehicleContainer.MikeKingUser);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		
		driver.switchTo().frame(0);
		javaScriptUtil.clickElementByJS(Mass_upload_ETracker_URL_AdditionalVehicleContainer.LoginUserBtn);
		ActionHandler.wait(12);
		CommonFunctions.attachScreenshot();
		
    	action.sendKeys(Keys.ARROW_DOWN).build().perform();
    	ActionHandler.wait(1);
    	ActionHandler.pressEnter();
    	ActionHandler.wait(5);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Setup button");

    	String NewWindow1 = driver.getWindowHandle();

		Set<String> NewWindows1 = driver.getWindowHandles();
		Iterator<String> j = NewWindows1.iterator();
		while (j.hasNext()) {
			String Window = j.next();
			if (!NewWindow.equalsIgnoreCase(Window)) {
				driver.switchTo().window(Window);
			}
		}
		
		ActionHandler.click(Mass_upload_ETracker_URL_AdditionalVehicleContainer.QuickFindSearchTab);
		ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Quick Find Search tab");
    	
		ActionHandler.setText(Mass_upload_ETracker_URL_AdditionalVehicleContainer.QuickFindSearchTab, "data import");
		ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User searches for data import wizard");
    	
		ActionHandler.click(Mass_upload_ETracker_URL_AdditionalVehicleContainer.DataImportWizard);
		ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Data Import Wizard button");
    	
		ActionHandler.pageDown();
		ActionHandler.wait(1);
		
		ActionHandler.click(Mass_upload_ETracker_URL_AdditionalVehicleContainer.LaunchWizardBtn);
		ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Launch Wizard button button");
    	
		ActionHandler.wait(60);
		
		driver.quit();
		
    }
    
    @Then("^Edit the ETracker URL$")
    public void Edit_the_ETracker_URL() throws Exception{
    	
    	ActionHandler.pageCompleteScrollDown();
    	ActionHandler.click(Mass_upload_ETracker_URL_AdditionalVehicleContainer.EditETrackerURL);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Edit ETracker URL button");
    	
    	ActionHandler.clearAndSetText(Mass_upload_ETracker_URL_AdditionalVehicleContainer.EditETrackerURL, "www.stackoverflow.com");
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User edits ETracker URL text");
    	
    	ActionHandler.click(Mass_upload_ETracker_URL_AdditionalVehicleContainer.SaveEditBtn);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on SaveEdit button");
    	
    }




}
