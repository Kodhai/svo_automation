package com.jlr.svo.tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.containers.SSEC_1227_CasesCreationContainer;
import com.jlr.svo.containers.SVOAdditionalVehicleContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.containers.SVOOpportunityContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class SSEC_1568_ClassicSalesClassicService_CaseCreation_Email_to_Case extends TestBaseCC {
	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory
			.getLogger(SSEC_1228_LinkContactAndAccountTocase_Test.class.getName());
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);

	
	SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	SVOAdditionalVehicleContainer SVOAdditionalvehicleContainer = PageFactory.initElements(driver,
			SVOAdditionalVehicleContainer.class);
	SSEC_1227_CasesCreationContainer CasesContainer = PageFactory.initElements(driver,
			SSEC_1227_CasesCreationContainer.class);
	
	public static String verificationCode;
	public static String vCode;
	public static String veriCode;
	public String NewWindow;
	
	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	public void onStart() {

		setupTest("SVOTest");
		driver = getDriver();

		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
		SVOAdditionalvehicleContainer = PageFactory.initElements(driver, SVOAdditionalVehicleContainer.class);
		CasesContainer = PageFactory.initElements(driver, SSEC_1227_CasesCreationContainer.class);

		
		driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);

		verificationCode = null;
		vCode = null;
		veriCode = null;
		NewWindow = null;
		
	}
	
    @Given("^Access to Gmail account with \"([^\"]*)\" and password \"([^\"]*)\"$")
    public void Access_to_Gmail_account_with_username_and_Password() throws Exception{
    	
    }
    
    @And("^Customer sends email to Classic Sales user \"([^\"]*)\"$")
    public void Customer_sends_email_to_Classic_Sales_user() throws Exception{
    	
    }
    
    @Then("^user logouts from gmail account$")
    public void user_logouts_from_gmail_account() throws Exception{
    	
    }
    
    @Then("^User verify that Classic Parts and Technical case is automatically created$")
    public void user_verify_that_Classic_Parts_and_Technical_case_is_automatically_created() throws Exception{
    	
    }
    
    @Then("^User verify that Classic Service case is automatically created$")
    public void user_verify_that_Classic_Service_case_is_automatically_created() throws Exception{
    	
    }

    @And("^Verify that WebEmail Subject and Description is filled$")
    public void Verify_that_WebEmail_Subject_and_Description_is_filled() throws Exception{
    	
    }
    
    @And("^Verify that user is able to close the case on Cases page$")
    public void Verify_that_user_is_able_to_close_the_case_on_Cases_page() throws Exception{
    	
    }
    
    @Then("^Click on drop down button for the case created and select delete option$")
    public void Click_on_dropdown_button_for_case_created_and_select_delete_option() throws Exception{
    	
    }
    
    @And("^Click on Ok button$")
    public void Click_on_Ok_button() throws Exception{
    	
    }








	

}
