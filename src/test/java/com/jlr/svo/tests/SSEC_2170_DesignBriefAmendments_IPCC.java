package com.jlr.svo.tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.containers.SSEC_1039_SVOEnquiryEmailRemaindersContainer;
import com.jlr.svo.containers.SSEC_1503_Clasic_Email_Signature_On_Salesforce_Container;
import com.jlr.svo.containers.SSEC_2157_DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer;
import com.jlr.svo.containers.SSEC_2158_DesignBriefAmendments_Wheels_TreadPlatesContainer;
import com.jlr.svo.containers.SSEC_2159_BespokeOrderForm_CreationofOrderFormRecordsContainer;
import com.jlr.svo.containers.SSEC_2169_DesignBriefAmendments_SeatsContainer;
import com.jlr.svo.containers.SSEC_2170_DesignBriefAmendments_IPCCContainer;
import com.jlr.svo.containers.SVOAccountsContainer;
import com.jlr.svo.containers.SVOAdditionalVehicleContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.containers.SVOItemToApproveContainer;
import com.jlr.svo.containers.SVOOpportunityContainer;
import com.jlr.svo.containers.SVO_CustomerResponsesContainer;
import com.jlr.svo.containers.SVO_EnquiryLostReasonContainer;
import com.jlr.svo.containers.SVO_RestrictedPartyScreeningContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class SSEC_2170_DesignBriefAmendments_IPCC extends TestBaseCC {

	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory
			.getLogger(SSEC_1039_SVOEnquiryEmailRemaindersTest.class.getName());
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);

	SVOAccountsContainer SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
	SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	SVOAdditionalVehicleContainer SVOAdditionalvehicleContainer = PageFactory.initElements(driver,
			SVOAdditionalVehicleContainer.class);
	SVO_RestrictedPartyScreeningContainer RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
			SVO_RestrictedPartyScreeningContainer.class);
	SVOItemToApproveContainer SVOItemToapproveContainer = PageFactory.initElements(driver,
			SVOItemToApproveContainer.class);
	SVO_CustomerResponsesContainer SVO_CustomerResponsesContainer = PageFactory.initElements(driver,
			SVO_CustomerResponsesContainer.class);
	SVO_EnquiryLostReasonContainer SVO_EnquiryLostReasonContainer = PageFactory.initElements(driver,
			SVO_EnquiryLostReasonContainer.class);
	SSEC_1039_SVOEnquiryEmailRemaindersContainer SVOEnquiryEmailRemaindersContainer = PageFactory.initElements(driver,
			SSEC_1039_SVOEnquiryEmailRemaindersContainer.class);
	SSEC_1503_Clasic_Email_Signature_On_Salesforce_Container Clasic_Email_Signature_On_Salesforce_Container = PageFactory.initElements(driver,
			SSEC_1503_Clasic_Email_Signature_On_Salesforce_Container.class);
	SSEC_2159_BespokeOrderForm_CreationofOrderFormRecordsContainer BespokeOrderForm_CreationofOrderFormRecordsContainer = PageFactory.initElements(driver, SSEC_2159_BespokeOrderForm_CreationofOrderFormRecordsContainer.class);
	SSEC_2157_DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer = PageFactory.initElements(driver, SSEC_2157_DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer.class);
	SSEC_2158_DesignBriefAmendments_Wheels_TreadPlatesContainer DesignBriefAmendments_Wheels_TreadPlatesContainer = PageFactory.initElements(driver, SSEC_2158_DesignBriefAmendments_Wheels_TreadPlatesContainer.class);
	SSEC_2169_DesignBriefAmendments_SeatsContainer DesignBriefAmendments_SeatsContainer = PageFactory.initElements(driver, SSEC_2169_DesignBriefAmendments_SeatsContainer.class);
	SSEC_2170_DesignBriefAmendments_IPCCContainer DesignBriefAmendments_IPCCContainer = PageFactory.initElements(driver, SSEC_2170_DesignBriefAmendments_IPCCContainer.class);
	
	public static String verificationCode;
	public static String vCode;
	public static String veriCode;
	public static String EnquiryName;

	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}
	

	public void onStart() {

		setupTest("SVOTest");
		driver = getDriver();

		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
		SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
		SVOAdditionalvehicleContainer = PageFactory.initElements(driver, SVOAdditionalVehicleContainer.class);

		RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
				SVO_RestrictedPartyScreeningContainer.class);
		SVOItemToapproveContainer = PageFactory.initElements(driver, SVOItemToApproveContainer.class);
		SVO_CustomerResponsesContainer = PageFactory.initElements(driver, SVO_CustomerResponsesContainer.class);
		SVO_EnquiryLostReasonContainer = PageFactory.initElements(driver, SVO_EnquiryLostReasonContainer.class);
		SVOEnquiryEmailRemaindersContainer = PageFactory.initElements(driver,
				SSEC_1039_SVOEnquiryEmailRemaindersContainer.class);
		Clasic_Email_Signature_On_Salesforce_Container = PageFactory.initElements(driver,
				SSEC_1503_Clasic_Email_Signature_On_Salesforce_Container.class);
		BespokeOrderForm_CreationofOrderFormRecordsContainer = PageFactory.initElements(driver, SSEC_2159_BespokeOrderForm_CreationofOrderFormRecordsContainer.class);
		DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer = PageFactory.initElements(driver, SSEC_2157_DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer.class);
		DesignBriefAmendments_Wheels_TreadPlatesContainer = PageFactory.initElements(driver, SSEC_2158_DesignBriefAmendments_Wheels_TreadPlatesContainer.class);
		DesignBriefAmendments_SeatsContainer = PageFactory.initElements(driver, SSEC_2169_DesignBriefAmendments_SeatsContainer.class);
		DesignBriefAmendments_IPCCContainer = PageFactory.initElements(driver, SSEC_2170_DesignBriefAmendments_IPCCContainer.class);
				
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		

		verificationCode = null;
		vCode = null;
		EnquiryName = null;

	}
	

    @Then("^Click on Edit button of IP and CC Material \"([^\"]*)\"$")
    public void Click_on_Edit_button_of_Front_DoorArmrestMaterial(String IPCCMaterial) throws Exception{
    	
    	String IPCCMat[] = IPCCMaterial.split(",");
    	IPCCMaterial = CommonFunctions.readExcelMasterData(IPCCMat[0], IPCCMat[1], IPCCMat[2]);

    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	
    	ActionHandler.click(DesignBriefAmendments_IPCCContainer.EditIPCCMaterial);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Edit Monotone Duotone button");
    	
    	ActionHandler.scrollToView(DesignBriefAmendments_IPCCContainer.IPCCText);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User scrolls to view Seats Details text");
    	
    	ActionHandler.click(DesignBriefAmendments_IPCCContainer.IPCCMaterialText);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_IPCCContainer.IPCC(IPCCMaterial))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Monotone / Duotone Drop Down");
    	
    }

    @And("^Select the Column Cowl Color \"([^\"]*)\" and Topper Pad Color \"([^\"]*)\" from drop down$")
    public void Select_the_Front_DoorRearArmrestMaterial(String ColumnCowlColor, String TopperPadColor) throws Exception{
    	
    	String ColCowlColor[] = ColumnCowlColor.split(",");
    	ColumnCowlColor = CommonFunctions.readExcelMasterData(ColCowlColor[0], ColCowlColor[1], ColCowlColor[2]);

    	String TopPadColor[] = TopperPadColor.split(",");
    	TopperPadColor = CommonFunctions.readExcelMasterData(TopPadColor[0], TopPadColor[1], TopPadColor[2]);

    	ActionHandler.click(DesignBriefAmendments_IPCCContainer.ColumnCowlColor);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_IPCCContainer.IPCC(ColumnCowlColor))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Column Cowl Color Drop Down");
    	
    	ActionHandler.click(DesignBriefAmendments_IPCCContainer.TopperPadColor);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_IPCCContainer.IPCC(TopperPadColor))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on TopperPad Color Drop Down");
    	
    }
    
    @Then("^Click on Edit button of of Mid Roll Color \"([^\"]*)\"$")
    public void Click_on_Edit_button_of_MidRollColor(String MidRollColor) throws Exception{
    	
    	String MidRollCol[] = MidRollColor.split(",");
    	MidRollColor = CommonFunctions.readExcelMasterData(MidRollCol[0], MidRollCol[1], MidRollCol[2]);

    	ActionHandler.pageDown();
    	ActionHandler.pageDown();
    	ActionHandler.pageDown();
    	
    	ActionHandler.click(DesignBriefAmendments_IPCCContainer.EditMidRollColor);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Edit Mid Roll Color button");
    	
    	ActionHandler.scrollToView(DesignBriefAmendments_IPCCContainer.IPCCText);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User scrolls to view IP CC Details text");
    	
    	ActionHandler.click(DesignBriefAmendments_IPCCContainer.MidRollColor);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_IPCCContainer.IPCC(MidRollColor))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Mid Roll Color Drop Down");
    }
    
    @And("^Select the IP Lower Color \"([^\"]*)\" and Console color \"([^\"]*)\"$")
    public void Select_the_ArmRestRearColor(String IPLowerColor, String ConsoleColor, String SVCeramics) throws Exception{
    	
    	String IPLowerCol[] = IPLowerColor.split(",");
    	IPLowerColor = CommonFunctions.readExcelMasterData(IPLowerCol[0], IPLowerCol[1], IPLowerCol[2]);

    	String ConsoleCol[] = ConsoleColor.split(",");
    	ConsoleColor = CommonFunctions.readExcelMasterData(ConsoleCol[0], ConsoleCol[1], ConsoleCol[2]);

    	ActionHandler.click(DesignBriefAmendments_IPCCContainer.IPLowerColor);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_IPCCContainer.IPCC(IPLowerColor))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on IP Lower Color Drop Down");
    	
    	ActionHandler.click(DesignBriefAmendments_IPCCContainer.ConsoleColor);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_IPCCContainer.IPCC(ConsoleColor))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Console Color Drop Down");
    	
    	
    }

    @Then("^Click on Edit button of Front Door Armrest Material \"([^\"]*)\"$")
    public void Click_on_Edit_button_IPCCMaterial(String FrontDoorArmrestMaterial) throws Exception{
    	
    	String FrontDAMat[] = FrontDoorArmrestMaterial.split(",");
    	FrontDoorArmrestMaterial = CommonFunctions.readExcelMasterData(FrontDAMat[0], FrontDAMat[1], FrontDAMat[2]);

    	ActionHandler.pageDown();
    	ActionHandler.pageDown();
    	ActionHandler.pageDown();
    	
    	ActionHandler.click(DesignBriefAmendments_IPCCContainer.EditFrontDoorArmrestMaterial);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Edit Front Door ArmRest material button");
    	
    	ActionHandler.scrollToView(DesignBriefAmendments_IPCCContainer.IPCCText);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User scrolls to view IP CC Details text");
    	
    	ActionHandler.click(DesignBriefAmendments_IPCCContainer.MidRollColor);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_IPCCContainer.IPCC(FrontDoorArmrestMaterial))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks and selects Fornt Door ArmRest material Drop Down");
    
    }
    
    @And("^Select the Rear Door Armrest Material \"([^\"]*)\" Top Roll Front Color \"([^\"]*)\" and Main Carrier Front Color \"([^\"]*)\"$")
    public void Select_the_ColumnCowlColor_andTopperPadColor(String RearDoorArmrestMaterial, String TopRollFrontColor, String MainCarrierFront) throws Exception{
    	
    	String RearDAMat[] = RearDoorArmrestMaterial.split(",");
    	RearDoorArmrestMaterial = CommonFunctions.readExcelMasterData(RearDAMat[0], RearDAMat[1], RearDAMat[2]);

    	String TopRollFC[] = TopRollFrontColor.split(",");
    	TopRollFrontColor = CommonFunctions.readExcelMasterData(TopRollFC[0], TopRollFC[1], TopRollFC[2]);

    	String MainCarrierFC[] = MainCarrierFront.split(",");
    	MainCarrierFront = CommonFunctions.readExcelMasterData(MainCarrierFC[0], MainCarrierFC[1], MainCarrierFC[2]);

    	ActionHandler.click(DesignBriefAmendments_IPCCContainer.RearDoorArmrestMaterial);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_IPCCContainer.IPCC(RearDoorArmrestMaterial))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Rear Door ArmRest Material Drop Down");
    	
    	ActionHandler.click(DesignBriefAmendments_IPCCContainer.TopRollFrontColor);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_IPCCContainer.IPCC(TopRollFrontColor))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Top Roll Front Drop Down");
    	
    	ActionHandler.click(DesignBriefAmendments_IPCCContainer.MainCarrierFrontColor);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_IPCCContainer.IPCC(MainCarrierFront))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Main Carrier Front Color Drop Down");
    }

    @Then("^Click on Edit button of ArmRest FrontColorMaterial \"([^\"]*)\"$")
    public void Click_on_Edit_button_ArmRestFrontColor(String FrontDoorArmrestMaterial) throws Exception{
    	
    	String FrontDAMat[] = FrontDoorArmrestMaterial.split(",");
    	FrontDoorArmrestMaterial = CommonFunctions.readExcelMasterData(FrontDAMat[0], FrontDAMat[1], FrontDAMat[2]);

    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	
    	ActionHandler.click(DesignBriefAmendments_IPCCContainer.EditFrontDoorArmrestMaterial);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Edit Front Door ArmRest Material button");
    	
    	ActionHandler.scrollToView(DesignBriefAmendments_IPCCContainer.DoorsLabel);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User scrolls to view Doors text");
    	
    	ActionHandler.click(DesignBriefAmendments_IPCCContainer.ArmRestFrontColorLabel);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_IPCCContainer.IPCC(FrontDoorArmrestMaterial))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks and selects Front Door ArmRest material from drop down");
    
    }
    
    @And("^Select the ArmRestRearColor \"([^\"]*)\" TopRollRearColor \"([^\"]*)\" MainCarrierRearColor \"([^\"]*)\"$")
    public void Select_the_ArmRestRearColor_TopRollRearColor_MainCarrierRearColor
    (String ArmRestRearColor,String TopRollRearColor,String MainCarrierRearColor) throws Exception{

    	String ArmRestRC[] = ArmRestRearColor.split(",");
    	ArmRestRearColor = CommonFunctions.readExcelMasterData(ArmRestRC[0], ArmRestRC[1], ArmRestRC[2]);

    	String TopRollRC[] = TopRollRearColor.split(",");
    	TopRollRearColor = CommonFunctions.readExcelMasterData(TopRollRC[0], TopRollRC[1], TopRollRC[2]);

    	String MainCarrierRC[] = MainCarrierRearColor.split(",");
    	MainCarrierRearColor = CommonFunctions.readExcelMasterData(MainCarrierRC[0], MainCarrierRC[1], MainCarrierRC[2]);

    	ActionHandler.click(DesignBriefAmendments_IPCCContainer.ArmRestRearColor);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_IPCCContainer.IPCC(ArmRestRearColor))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on ArmRestRearColor drop down");
    	
    	ActionHandler.click(DesignBriefAmendments_IPCCContainer.TopRollRearColor);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_IPCCContainer.IPCC(TopRollRearColor))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Top Roll Rear Color from drop down");
    	
    	ActionHandler.click(DesignBriefAmendments_IPCCContainer.MainCarrierRearColor);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_IPCCContainer.IPCC(MainCarrierRearColor))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Main Carrier Rear Color from drop down");
    
    }
    
    @Then("^Click on Edit button of ArmRestFrontColorMaterial \"([^\"]*)\"$")
    public void Click_on_Edit_button_ArmRest_FrontColorMaterial(String FrontDoorArmrestMaterial) throws Exception{
    	
    	String FrontDAMat[] = FrontDoorArmrestMaterial.split(",");
    	FrontDoorArmrestMaterial = CommonFunctions.readExcelMasterData(FrontDAMat[0], FrontDAMat[1], FrontDAMat[2]);

    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	
    	ActionHandler.click(DesignBriefAmendments_IPCCContainer.EditFrontDoorArmrestMaterial);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Edit Front Door ArmRest material button");
    	
    	ActionHandler.scrollToView(DesignBriefAmendments_IPCCContainer.DoorsLabel);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User scrolls to view Doors text");
    	
    	ActionHandler.click(DesignBriefAmendments_IPCCContainer.ArmRestFrontColorLabel);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_IPCCContainer.IPCC(FrontDoorArmrestMaterial))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks and selects Front Door ArmRest Material Drop Down");
    
    }
    
    @And("^Verify that the ArmRestFrontColor is changed when ArmRestFrontColor Material is changed$")
    public void Verify_ArmRestFrontColor_changed_when_ArmRestFrontColorMaterial() throws Exception{
    	
    	ActionHandler.click(DesignBriefAmendments_IPCCContainer.ArmRestFrontColorLabel);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User verifies that the ArmRestFrontColor is changed when ArmRestFrontColor Material is changed");
    	
    	
    }
    







}
