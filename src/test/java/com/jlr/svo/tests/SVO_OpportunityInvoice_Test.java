package com.jlr.svo.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.containers.SVOAdditionalVehicleContainer;
import com.jlr.svo.containers.SVOCommentFieldAdditionContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.containers.SVOItemToApproveContainer;
import com.jlr.svo.containers.SVOOpportunityContainer;
import com.jlr.svo.containers.SVOWorkOrderContainer;
import com.jlr.svo.containers.SVO_OpportunityInvoiceContainer;
import com.jlr.svo.containers.SVO_Pricing_LogicContainer;
import com.jlr.svo.containers.SVO_RestrictedPartyScreeningContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SVO_OpportunityInvoice_Test extends TestBaseCC {
	private WebDriver driver = getDriver();
	private static final Logger LOGGER = LoggerFactory.getLogger(SVO_OpportunityInvoice_Test.class.getName());
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
	SVOCommentFieldAdditionContainer SVOCommentFieldAdditionContainer = PageFactory.initElements(driver,
			SVOCommentFieldAdditionContainer.class);
	SVOWorkOrderContainer SVOWorkOrderContainer = PageFactory.initElements(driver, SVOWorkOrderContainer.class);
	SVOAdditionalVehicleContainer SVOAdditionalvehicleContainer = PageFactory.initElements(driver,
			SVOAdditionalVehicleContainer.class);

	SVOItemToApproveContainer SVOItemToapproveContainer = PageFactory.initElements(driver,
			SVOItemToApproveContainer.class);
	SVO_RestrictedPartyScreeningContainer RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
			SVO_RestrictedPartyScreeningContainer.class);
	SVO_Pricing_LogicContainer SVO_Pricing_LogicContainer = PageFactory.initElements(driver,
			SVO_Pricing_LogicContainer.class);
	SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SVO_OpportunityInvoiceContainer SVO_OpportunityInvoiceContainer = PageFactory.initElements(driver,
			SVO_OpportunityInvoiceContainer.class);

	public static String verificationCode;
	public static String vCode;
	public static String veriCode;

	public static String OpportunityName;
	public static String AccountName;
	public static String OrderNumber;
	public static String SAPAccountName;
	public static String OpportunityNameOnInvoicePage;
	public static String AccountNameOnInvoicePage;
	public static String SAPAccountNameOnInvoicePage;
	public static String OrderNumberOnInvoicePage;

	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	@And("^User creates Classic Opportunity with mandatory fields like Stage \"([^\"]*)\" Product Offering \"([^\"]*)\" Region \"([^\"]*)\" Client \"([^\"]*)\" Restricted Party Screening Stage \"([^\"]*)\" Preferred Contact \"([^\"]*)\" SAP Account \"([^\"]*)\" Account Name \"([^\"]*)\" Retailer Contact \"([^\"]*)\" Source \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\"$")
	public void User_creates_Classic_Opportunity(String stage, String productOffering, String region, String client,
			String Checkcleared, String preferredContact, String SAPAccount, String accountName, String retailerContact,
			String source, String brand, String model) throws Exception {
		String oppName = "Test_Classic_Service_Opportunity_";
		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		String closeDate = CommonFunctions.selectFutureDate();
		Reporter.addStepLog("Opportunity Close Date is = " + closeDate);

		String s[] = stage.split(",");
		stage = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String proOff[] = productOffering.split(",");
		productOffering = CommonFunctions.readExcelMasterData(proOff[0], proOff[1], proOff[2]);

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String c[] = client.split(",");
		client = CommonFunctions.readExcelMasterData(c[0], c[1], c[2]);

		String RPS[] = Checkcleared.split(",");
		Checkcleared = CommonFunctions.readExcelMasterData(RPS[0], RPS[1], RPS[2]);

		String preCon[] = preferredContact.split(",");
		preferredContact = CommonFunctions.readExcelMasterData(preCon[0], preCon[1], preCon[2]);

		String SAP[] = SAPAccount.split(",");
		SAPAccount = CommonFunctions.readExcelMasterData(SAP[0], SAP[1], SAP[2]);

		String accName[] = accountName.split(",");
		accountName = CommonFunctions.readExcelMasterData(accName[0], accName[1], accName[2]);

		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		String so[] = source.split(",");
		source = CommonFunctions.readExcelMasterData(so[0], so[1], so[2]);

		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String m[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		ActionHandler.wait(3);
		Reporter.addStepLog("User starts creating new SVO Classic Service Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.NewOpportunity);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.QuoteNextBtn)) {
			ActionHandler.click(SVO_OpportunityInvoiceContainer.SVOClassicServiceOpportunity);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects opportunity record type as Classic Service");

			ActionHandler.click(SVOItemToapproveContainer.QuoteNextBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User clicks on next button");

		}

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeOppName, oppName + randomNumber);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Name");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeClosedDate, closeDate);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Closed date");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeStage);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.StageSelection(stage))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Stage for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeProductOffering);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(
				driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ProOfferingSelection(productOffering))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Product Offering for an Opportunity");

		ActionHandler.scrollToView(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		ActionHandler.wait(2);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.RegionSelection(region))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Region for an Opportunity");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeAccountName, accountName);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeAccountNameSearch);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(accountName))));
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Account Name for an Opportunity");

		VerifyHandler.verifyElementPresent(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		CommonFunctions.attachScreenshot();
		ActionHandler
				.click(driver.findElement(By.xpath(RestrictedPartyScreeningContainer.ValueSelection(Checkcleared))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects restricted party screening as Check cleared for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokePreferredContact, preferredContact);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		act1.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(3);
		Reporter.addStepLog("User selects Preferred Contact for an Opportunity");

		ActionHandler.click(SVO_OpportunityInvoiceContainer.SAPAccountSearch);
		ActionHandler.setText(SVO_OpportunityInvoiceContainer.SAPAccountSearch, SAPAccount);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVO_OpportunityInvoiceContainer.SAPAccountNameSearch);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityInvoiceContainer.SAPAccount(SAPAccount))));
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects SAP Account for an Opportunity");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeBrand, brand);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeBrandSearch);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(brand))));
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Brand for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeModel, model);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(model))));
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Model for an Opportunity");

		ActionHandler.scrollToView(SVOAdditionalvehicleContainer.SVOBespokeSource);
		ActionHandler.wait(2);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSource);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.SourceSelection(source))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Source for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSaveOpportunity);
		ActionHandler.wait(8);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves SVO Bespoke Opportunity");

		ActionHandler.wait(4);
		OpportunityName = SVOAdditionalvehicleContainer.OpportunityName.getText();
		Reporter.addStepLog("Opportunity Name is = " + OpportunityName);
	}

	@Then("^Move the Opportunity to Order placed stage Vehicle name \"([^\"]*)\"$")
	public void move_the_Opportunity_to_Order_placed_stage(String Vehicle) throws Throwable {

		String VN[] = Vehicle.split(",");
		Vehicle = CommonFunctions.readExcelMasterData(VN[0], VN[1], VN[2]);

		ActionHandler.wait(5);
		ActionHandler.click(SVOAdditionalvehicleContainer.MarkStageAsComplete);
		ActionHandler.wait(8);
		CommonFunctions.attachScreenshot();

		driver.navigate().refresh();
		ActionHandler.wait(10);

		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);

		javaScriptUtil.clickElementByJS(SVO_OpportunityInvoiceContainer.EditVehicleBtn);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityInvoiceContainer.VehicleSearch);
		ActionHandler.wait(3);

		ActionHandler.setText(SVO_OpportunityInvoiceContainer.VehicleSearch, Vehicle);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVO_OpportunityInvoiceContainer.VehicleNameSearch);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityInvoiceContainer.VehicleSelection(Vehicle))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects vehicle for an Opportunity");

		ActionHandler.click(SVO_OpportunityInvoiceContainer.ClassicRetailPrice);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(SVO_OpportunityInvoiceContainer.ClassicRetailPrice, "123");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters retail price for an Opportunity");

		ActionHandler.click(SVO_OpportunityInvoiceContainer.ClassicOppDetailsSave);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves classic Opportunity details");

		driver.navigate().refresh();
		ActionHandler.wait(10);

		ActionHandler.pageDown();
		ActionHandler.pageDown();
		ActionHandler.pageDown();
		ActionHandler.wait(1);
		ActionHandler.scrollDown();
		ActionHandler.wait(1);

		javaScriptUtil.clickElementByJS(SVO_OpportunityInvoiceContainer.ClassicOppPriceDetailsEditBtn);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityInvoiceContainer.ClassicOppVATQualifying);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityInvoiceContainer.ClassicOppVATQualifyingYes);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVO_OpportunityInvoiceContainer.ClassicOppVATQualifyingSaveBtn);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects VAT qualifying for an opportunity");

		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(3);
		ActionHandler.pageUp();
		ActionHandler.wait(3);
		ActionHandler.click(SVOAdditionalvehicleContainer.MarkStageAsComplete);
		ActionHandler.wait(8);
		CommonFunctions.attachScreenshot();

		driver.navigate().refresh();
		ActionHandler.wait(10);
		ActionHandler.click(SVOAdditionalvehicleContainer.MarkStageAsComplete);
		ActionHandler.wait(8);
		CommonFunctions.attachScreenshot();

	}

	@Then("^Verify that Account and Order details are displayed for the selected classic opportunity$")
	public void verify_that_Account_and_Order_details_are_displayed_for_the_selected_classic_opportunity()
			throws Throwable {

		AccountName = null;
		OrderNumber = null;
		SAPAccountName = null;

		ActionHandler.wait(8);
		driver.navigate().refresh();
		ActionHandler.wait(10);

		ActionHandler.pageDown();
		ActionHandler.wait(3);
		AccountName = SVO_OpportunityInvoiceContainer.AccountName.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Verifies the displayed account name as " + AccountName);

		SAPAccountName = SVO_OpportunityInvoiceContainer.SAPAccountName.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Verifies the displayed SAP account name as " + SAPAccountName);

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.OrdersLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Orders link");

		ActionHandler.wait(5);
		OrderNumber = SVOAdditionalvehicleContainer.FirstOrder.getText();
		Reporter.addStepLog("User Verifies the displayed order number as " + OrderNumber);

		driver.navigate().back();

	}

	@Then("^User create new invoice under invoice section with deposit Record Type \"([^\"]*)\"$")
	public void user_create_new_invoice_under_invoice_section_with_deposit(String recordtype) throws Throwable {

		String RT[] = recordtype.split(",");
		recordtype = CommonFunctions.readExcelMasterData(RT[0], RT[1], RT[2]);

		String closeDate = CommonFunctions.selectFutureDate();

		javaScriptUtil.clickElementByJS(SVO_OpportunityInvoiceContainer.InvoiceLink);
		ActionHandler.wait(8);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Invoice link");

		driver.navigate().refresh();
		ActionHandler.wait(10);
		ActionHandler.click(SVO_OpportunityInvoiceContainer.NewInvoiceBtn);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityInvoiceContainer.RecordType(recordtype))));
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects record type for an invoice");

		ActionHandler.click(SVO_OpportunityInvoiceContainer.InvoiceNextBtn);
		CommonFunctions.attachScreenshot();


		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.OpportunityNameOnInvoiceCreationPage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Verifies the auto-populated opportunity name on invoice page");

		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.AccountNameOnInvoiceCreationPage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the account name field on invoice creation page");

		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.SAPAccountNameOnInvoiceCreationPage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the SAP account name field on invoice creation page");

		ActionHandler.scrollToView(SVO_OpportunityInvoiceContainer.PaymentValue);
		ActionHandler.setText(SVO_OpportunityInvoiceContainer.PaymentValue, "10");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Invoice value");

		ActionHandler.setText(SVO_OpportunityInvoiceContainer.PaymentAmount, "123");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Invoice amount");

		ActionHandler.setText(SVO_OpportunityInvoiceContainer.PaymentSentDate, closeDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Payment sent date");

		ActionHandler.setText(SVO_OpportunityInvoiceContainer.PaymentMadeDate, closeDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Payment made date");

		ActionHandler.click(SVO_OpportunityInvoiceContainer.ClassicOppVATQualifyingSaveBtn);
		CommonFunctions.attachScreenshot();

	}

	@Then("^Verify the invoice details such as Account, Sap, Order and payment details of deposit invoice is displayed on Invoice page$")
	public void verify_the_invoice_details_such_as_Account_Sap_Order_and_payment_details_of_deposit_invoice_is_displayed_on_Invoice_page()
			throws Throwable {

		ActionHandler.wait(8);
		driver.navigate().refresh();
		ActionHandler.wait(10);

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstOrder);
		CommonFunctions.attachScreenshot();

		OpportunityNameOnInvoicePage = SVO_OpportunityInvoiceContainer.OpportunityNameOnInvoicePage.getText();
		Reporter.addStepLog("User Verifies the opportunity name on invoice page as " + OpportunityNameOnInvoicePage);

		AccountNameOnInvoicePage = SVO_OpportunityInvoiceContainer.AccountNameOnInvoicePage.getText();
		Reporter.addStepLog(
				"User Verifies the auto-populated account name on invoice page as " + AccountNameOnInvoicePage);

		SAPAccountNameOnInvoicePage = SVO_OpportunityInvoiceContainer.SAPAccountNameOnInvoicePage.getText();
		Reporter.addStepLog(
				"User Verifies the auto-populated SAP account name on invoice page as " + SAPAccountNameOnInvoicePage);

		OrderNumberOnInvoicePage = SVO_OpportunityInvoiceContainer.OrderNumberOnInvoicePage.getText();
		Reporter.addStepLog("User Verifies the displayed order number on invoice page as " + OrderNumberOnInvoicePage);

		if (OpportunityNameOnInvoicePage.equals(OpportunityName)) {
			Reporter.addStepLog("Opportunity name is same on both invoice and opportunity page ");
		} else {
			Reporter.addStepLog("Opportunity name is not same on both invoice and opportunity page ");
		}

		if (AccountNameOnInvoicePage.equals(AccountName)) {
			Reporter.addStepLog("Account name is same on both invoice and opportunity page ");
		} else {
			Reporter.addStepLog("Account name is not same on both invoice and opportunity page ");
		}

		if (SAPAccountNameOnInvoicePage.equals(SAPAccountName)) {
			Reporter.addStepLog("SAP account name is same on both invoice and opportunity page ");
		} else {
			Reporter.addStepLog("SAP account name is not same on both invoice and opportunity page ");
		}

		if (OrderNumberOnInvoicePage.equals(OrderNumber)) {
			Reporter.addStepLog("Order number is same on both invoice and Order page ");
		} else {
			Reporter.addStepLog("Order number is not same on both invoice and Order page ");
		}

		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);

		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.PaymentValueOnInvoicePage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the payment value on invoice page");

		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.PaymentAmountOnInvoicePage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the payment amount on invoice page");

		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.PaymentSentDateOnInvoicePage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the payment sent date on invoice page");

		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.PaymentMadeDateOnInvoicePage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the payment made date on invoice page");

		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.PaymentSendByDateOnInvoicePage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the payment send by date on invoice page");

	}

	@Then("^Verify the second invoice details such as Account, Sap, Order and payment details of an invoice is displayed on Invoice page$")
	public void verify_the_second_invoice_details_such_as_Account_Sap_Order_and_payment_details_of_an_invoice_is_displayed_on_Invoice_page()
			throws Throwable {

		ActionHandler.wait(10);

		ActionHandler.click(SVO_OpportunityInvoiceContainer.SecondInvoice);
		CommonFunctions.attachScreenshot();

		OpportunityNameOnInvoicePage = SVO_OpportunityInvoiceContainer.OpportunityNameOnInvoicePage.getText();
		Reporter.addStepLog("User Verifies the opportunity name on invoice page as " + OpportunityNameOnInvoicePage);

		AccountNameOnInvoicePage = SVO_OpportunityInvoiceContainer.AccountNameOnInvoicePage.getText();
		Reporter.addStepLog(
				"User Verifies the auto-populated account name on invoice page as " + AccountNameOnInvoicePage);

		SAPAccountNameOnInvoicePage = SVO_OpportunityInvoiceContainer.SAPAccountNameOnInvoicePage.getText();
		Reporter.addStepLog(
				"User Verifies the auto-populated SAP account name on invoice page as " + SAPAccountNameOnInvoicePage);

		OrderNumberOnInvoicePage = SVO_OpportunityInvoiceContainer.OrderNumberOnInvoicePage.getText();
		Reporter.addStepLog("User Verifies the displayed order number on invoice page as " + OrderNumberOnInvoicePage);

		if (OpportunityNameOnInvoicePage.equals(OpportunityName)) {
			Reporter.addStepLog("Opportunity name is same on both invoice and opportunity page ");
		} else {
			Reporter.addStepLog("Opportunity name is not same on both invoice and opportunity page ");
		}

		if (AccountNameOnInvoicePage.equals(AccountName)) {
			Reporter.addStepLog("Account name is same on both invoice and opportunity page ");
		} else {
			Reporter.addStepLog("Account name is not same on both invoice and opportunity page ");
		}

		if (SAPAccountNameOnInvoicePage.equals(SAPAccountName)) {
			Reporter.addStepLog("SAP account name is same on both invoice and opportunity page ");
		} else {
			Reporter.addStepLog("SAP account name is not same on both invoice and opportunity page ");
		}

		if (OrderNumberOnInvoicePage.equals(OrderNumber)) {
			Reporter.addStepLog("Order number is same on both invoice and Order page ");
		} else {
			Reporter.addStepLog("Order number is not same on both invoice and Order page ");
		}

		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);

		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.PaymentValueOnInvoicePage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the payment value on invoice page");

		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.PaymentAmountOnInvoicePage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the payment amount on invoice page");

		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.PaymentSentDateOnInvoicePage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the payment sent date on invoice page");

		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.PaymentMadeDateOnInvoicePage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the payment made date on invoice page");

		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.PaymentSendByDateOnInvoicePage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the payment send by date on invoice page");
	}

	@Then("^Verify the invoice details such as Account, Sap, Order and payment details from agreements and invoice section$")
	public void verify_the_invoice_details_such_as_Account_Sap_Order_and_payment_details_from_agreements_and_invoice_section()
			throws Throwable {

		driver.navigate().refresh();
		ActionHandler.wait(10);

		OpportunityNameOnInvoicePage = SVO_OpportunityInvoiceContainer.OpportunityNameOnInvoicePage.getText();
		Reporter.addStepLog("User Verifies the opportunity name on invoice page as" + OpportunityNameOnInvoicePage);

		AccountNameOnInvoicePage = SVO_OpportunityInvoiceContainer.SAPAccountNameOnInvoicePage.getText();
		Reporter.addStepLog(
				"User Verifies the auto-populated account name on invoice page as" + AccountNameOnInvoicePage);

		SAPAccountNameOnInvoicePage = SVO_OpportunityInvoiceContainer.SAPAccountNameOnInvoicePage.getText();
		Reporter.addStepLog(
				"User Verifies the auto-populated SAP account name on invoice page as" + SAPAccountNameOnInvoicePage);

		OrderNumberOnInvoicePage = SVO_OpportunityInvoiceContainer.OrderNumberOnInvoicePage.getText();
		Reporter.addStepLog("User Verifies the displayed order number on invoice page as" + OrderNumberOnInvoicePage);

		if (OpportunityNameOnInvoicePage.equals(OpportunityName)) {
			Reporter.addStepLog("Opportunity name is same on both invoice and opportunity page ");
		} else {
			Reporter.addStepLog("Opportunity name is not same on both invoice and opportunity page ");
		}

		if (AccountNameOnInvoicePage.equals(AccountName)) {
			Reporter.addStepLog("Account name is same on both invoice and opportunity page ");
		} else {
			Reporter.addStepLog("Account name is not same on both invoice and opportunity page ");
		}

		if (SAPAccountNameOnInvoicePage.equals(SAPAccountName)) {
			Reporter.addStepLog("SAP account name is same on both invoice and opportunity page ");
		} else {
			Reporter.addStepLog("SAP account name is not same on both invoice and opportunity page ");
		}

		if (OrderNumberOnInvoicePage.equals(OrderNumber)) {
			Reporter.addStepLog("Order number is same on both invoice and Order page ");
		} else {
			Reporter.addStepLog("Order number is not same on both invoice and Order page ");
		}

		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);

		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.PaymentValueOnInvoicePage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the payment value on invoice page");

		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.PaymentAmountOnInvoicePage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the payment amount on invoice page");

		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.PaymentSentDateOnInvoicePage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the payment sent date on invoice page");

		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.PaymentMadeDateOnInvoicePage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the payment made date on invoice page");

		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.PaymentSendByDateOnInvoicePage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the payment send by date on invoice page");

	}

	@When("^User moves the opportunity to closed won stage$")
	public void User_moves_the_opportunity_to_closed_won_stage() throws Throwable {

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.OrdersLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Orders link");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstOrder);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens Order details for the opportunity");

		driver.navigate().refresh();
		ActionHandler.wait(10);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(10);

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDrpDwn);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		Reporter.addStepLog("User clicks on Order outcome stage drop down list");

		ActionHandler.click(SVO_OpportunityInvoiceContainer.ClosedWonStage);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDoneBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on done button");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityInvoiceContainer.OpportunityNameOnOrderPage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate back to opportunity");
	}

	@Then("^User create multiple invoices under invoice section Record Type \"([^\"]*)\"$")
	public void user_create_multiple_invoices_under_invoice_section(String recordtype) throws Throwable {

		String RT[] = recordtype.split(",");
		recordtype = CommonFunctions.readExcelMasterData(RT[0], RT[1], RT[2]);

		String closeDate = CommonFunctions.selectFutureDate();

		ActionHandler.wait(4);
		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(4);
		javaScriptUtil.clickElementByJS(SVO_OpportunityInvoiceContainer.OpportunityNameOnInvoicePage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate back to opportunity page");

		javaScriptUtil.clickElementByJS(SVO_OpportunityInvoiceContainer.InvoiceLink);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Invoice link");

		ActionHandler.click(SVO_OpportunityInvoiceContainer.NewInvoiceBtn);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityInvoiceContainer.RecordType(recordtype))));
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects record type for an invoice");

		ActionHandler.click(SVO_OpportunityInvoiceContainer.InvoiceNextBtn);
		CommonFunctions.attachScreenshot();

		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.OpportunityNameOnInvoiceCreationPage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Verifies the auto-populated opportunity name on invoice page");

		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.AccountNameOnInvoiceCreationPage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the account name field on invoice creation page");

		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.SAPAccountNameOnInvoiceCreationPage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the SAP account name field on invoice creation page");

		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.OrderNumberOnInvoiceCreationPage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the Order number field on invoice creation page");

		ActionHandler.scrollToView(SVO_OpportunityInvoiceContainer.PaymentValue);
		ActionHandler.setText(SVO_OpportunityInvoiceContainer.PaymentValue, "10");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Invoice value");

		ActionHandler.setText(SVO_OpportunityInvoiceContainer.PaymentAmount, "123");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Invoice amount");

		ActionHandler.setText(SVO_OpportunityInvoiceContainer.PaymentSentDate, closeDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Payment sent date");

		ActionHandler.setText(SVO_OpportunityInvoiceContainer.PaymentMadeDate, closeDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Payment made date");

		ActionHandler.setText(SVO_OpportunityInvoiceContainer.PaymentSendByDate, closeDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Payment send by date");

		ActionHandler.click(SVO_OpportunityInvoiceContainer.ClassicOppVATQualifyingSaveBtn);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

	}

	@Then("^User navigates to invoice from agreement and invoice section$")
	public void User_navigates_to_invoice_from_agreement_and_invoice_section() throws Throwable {

		ActionHandler.wait(3);
		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(3);

		javaScriptUtil.clickElementByJS(SVO_OpportunityInvoiceContainer.OpportunityNameOnInvoicePage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates back to opportunity page");

		ActionHandler.pageDown();
		ActionHandler.wait(3);

		javaScriptUtil.clickElementByJS(SVO_OpportunityInvoiceContainer.AgreementAndInvoice);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Agreement and invoice link");

		javaScriptUtil.clickElementByJS(SVO_OpportunityInvoiceContainer.InvoiveNumber);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to newly created invoice");
	}

	@Then("^Navigate to Invoice section of an Opportunity$")
	public void navigate_to_Invoice_section_of_an_Opportunity() throws Throwable {

		ActionHandler.pageDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		javaScriptUtil.clickElementByJS(SVO_OpportunityInvoiceContainer.InvoiceLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Invoice Section");

	}

	@Then("^User tries to save the invoice after removing the Opportunity field$")
	public void user_tries_to_save_the_invoice_after_removing_the_Opportunity_field() throws Throwable {

		ActionHandler.click(SVO_OpportunityInvoiceContainer.InvoiceNewButton);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks on Invoice new button to create an invoice");

		ActionHandler.click(SVO_OpportunityInvoiceContainer.DepositeInvoiceType);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects deposit record type to create an Invoice");

		ActionHandler.click(SVO_OpportunityInvoiceContainer.InvoiceNextButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Invoice Next button");

		ActionHandler.click(SVO_OpportunityInvoiceContainer.ClearOpportunityFieldIcon);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User remove the opportunity field on invoice page");

		ActionHandler.click(SVO_OpportunityInvoiceContainer.SaveInvoiceButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User hits one save button on Invoice page");

	}

	@Then("^Verify the error message displayed to select the opportunity field$")
	public void verify_the_error_message_displayed_to_select_the_opportunity_field() throws Throwable {

		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.ErrorMsgToSelectAnOpportunity);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("An error message displayed to select the opportunity field");

		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityInvoiceContainer.CancelInvoiceButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User hits one Cancel button on Invoice page");
	}

	@Then("^User creates a Deposit type of invoice$")
	public void user_creates_a_Deposit_type_of_invoice() throws Throwable {
		ActionHandler.click(SVO_OpportunityInvoiceContainer.InvoiceNewButton);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks on Invoice new button to create an invoice");

		ActionHandler.click(SVO_OpportunityInvoiceContainer.DepositeInvoiceType);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects deposit record type to create an Invoice");

		ActionHandler.click(SVO_OpportunityInvoiceContainer.InvoiceNextButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Invoice Next button");

		ActionHandler.click(SVO_OpportunityInvoiceContainer.SaveInvoiceButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User hits one Save button on Invoice page");

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstOrder);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigates to the created Invoice page");

	}

	@Then("^Verify that the invoice stage is in Planned Request stage$")
	public void verify_that_the_invoice_stage_is_in_Planned_Request_stage() throws Throwable {

		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.InvoicePlannedRequestStage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Invoice stage is in Planned Request stage");
	}

	@Then("^Update the created Invoice with all pricing details and sap pricing details of an Invoice$")
	public void update_the_created_Invoice_with_all_pricing_details_and_sap_pricing_details_of_an_Invoice()
			throws Throwable {

		String PaymentDate = CommonFunctions.selectFutureDate();

		driver.navigate().refresh();
		ActionHandler.wait(10);

		javaScriptUtil.clickElementByJS(SVO_OpportunityInvoiceContainer.EditInvoiceButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Edit button of an created Invoice");

		ActionHandler.wait(3);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);

		ActionHandler.clearAndSetText(SVO_OpportunityInvoiceContainer.InvoicePaymentValueTextBox, "130");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters Invoice Payment Value field");

		ActionHandler.clearAndSetText(SVO_OpportunityInvoiceContainer.InvoicePaymentAmountTextBox, "140");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters Invoice Payment Amount field");

		ActionHandler.clearAndSetText(SVO_OpportunityInvoiceContainer.InvoicePaymentSentDate, PaymentDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters Invoice Payment sent date");

		ActionHandler.clearAndSetText(SVO_OpportunityInvoiceContainer.InvoicePaymentMadeDate, PaymentDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters Invoice Payment made date");

		ActionHandler.scrollToView(SVO_OpportunityInvoiceContainer.InvoiceSapNumberTextBox);
		ActionHandler.wait(2);
		ActionHandler.clearAndSetText(SVO_OpportunityInvoiceContainer.InvoiceSapNumberTextBox, "250");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters SAP Invoice Payment number field");

		ActionHandler.clearAndSetText(SVO_OpportunityInvoiceContainer.InvoiceSapValueTextBox, "330");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters SAP Invoice Payment Value field");

		ActionHandler.clearAndSetText(SVO_OpportunityInvoiceContainer.SapInvoiceRaisedDate, PaymentDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters SAP Invoice Raised date");

		ActionHandler.clearAndSetText(SVO_OpportunityInvoiceContainer.SapInvoicePaidDate, PaymentDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters SAP Invoice paid date");

		ActionHandler.wait(1);
		ActionHandler.clickElement(SVO_OpportunityInvoiceContainer.SaveInvoiceButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on save button on invoice page");
	}

	@Then("^Verify that the invoice stage is moved to SAP invoice paid stage$")
	public void verify_that_the_invoice_stage_is_moved_to_SAP_invoice_paid_stage() throws Throwable {

		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.InvoiceSAPInvoicePaidStage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Invoice stage is moved to SAP Invoice Paid stage");
	}

	@Then("^User creates a Deposit type of invoice with all pricing details$")
	public void user_creates_a_Deposit_type_of_invoice_with_all_pricing_details() throws Throwable {
		ActionHandler.click(SVO_OpportunityInvoiceContainer.InvoiceNewButton);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks on Invoice new button to create an invoice");

		ActionHandler.click(SVO_OpportunityInvoiceContainer.DepositeInvoiceType);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects deposit record type to create an Invoice");

		ActionHandler.click(SVO_OpportunityInvoiceContainer.InvoiceNextButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Invoice Next button");

		String PaymentDate = CommonFunctions.selectFutureDate();

		ActionHandler.wait(3);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);

		ActionHandler.clearAndSetText(SVO_OpportunityInvoiceContainer.InvoicePaymentValueTextBox, "100");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters Invoice Payment Value field");

		ActionHandler.clearAndSetText(SVO_OpportunityInvoiceContainer.InvoicePaymentAmountTextBox, "105");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters Invoice Payment Amount field");

		ActionHandler.clearAndSetText(SVO_OpportunityInvoiceContainer.InvoicePaymentSentDate, PaymentDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters Invoice Payment sent date");

		ActionHandler.clearAndSetText(SVO_OpportunityInvoiceContainer.InvoicePaymentMadeDate, PaymentDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters Invoice Payment made date");

		ActionHandler.scrollToView(SVO_OpportunityInvoiceContainer.InvoiceSapNumberTextBox);
		ActionHandler.wait(2);
		ActionHandler.clearAndSetText(SVO_OpportunityInvoiceContainer.InvoiceSapNumberTextBox, "150");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters SAP Invoice Payment number field");

		ActionHandler.clearAndSetText(SVO_OpportunityInvoiceContainer.InvoiceSapValueTextBox, "230");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters SAP Invoice Payment Value field");

		ActionHandler.clearAndSetText(SVO_OpportunityInvoiceContainer.SapInvoiceRaisedDate, PaymentDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters SAP Invoice Raised date");

		ActionHandler.clearAndSetText(SVO_OpportunityInvoiceContainer.SapInvoicePaidDate, PaymentDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters SAP Invoice paid date");

		ActionHandler.wait(1);
		ActionHandler.clickElement(SVO_OpportunityInvoiceContainer.SaveInvoiceButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on save button on invoice page");

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstOrder);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigates to the created Invoice page");
	}

	@Then("^Verify that user is not able to view Bespoke Opportunities from select list view$")
	public void verify_that_user_is_not_able_to_view_Bespoke_Opportunities_from_select_list_view() throws Throwable {

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstOpportunityLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user select first bespoke opportunity from available list");

		if (!VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.InvoiceLink)) {
			Reporter.addStepLog("user is not able to view invoice link for Bespoke Opportunities");
		} else {
			Reporter.addStepLog("user is able to view invoice link for Bespoke Opportunities");
		}
	}

	@Then("^Verify that user is not able to link multiple opportunities for single Invoice$")
	public void verify_that_user_is_not_able_to_link_multiple_opportunities_for_single_Invoice() throws Throwable {

		ActionHandler.click(SVO_OpportunityInvoiceContainer.InvoiceNewButton);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks on Invoice new button to create an invoice");

		ActionHandler.click(SVO_OpportunityInvoiceContainer.DepositeInvoiceType);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects deposit record type to create an Invoice");

		ActionHandler.click(SVO_OpportunityInvoiceContainer.InvoiceNextButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Invoice Next button");

		if (!VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.MultipleOpportunityField))
			;
		{
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User is not able to select multiple opportunities on a single invoice");
		}

		ActionHandler.click(SVO_OpportunityInvoiceContainer.CancelInvoiceButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on cancel button on Invoice Page");

	}

	@Then("^Move Opportunity to Order placed stage Vehicle name \"([^\"]*)\"$")
	public void move_Opportunity_to_Order_placed_stage(String Vehicle) throws Throwable {

		String VN[] = Vehicle.split(",");
		Vehicle = CommonFunctions.readExcelMasterData(VN[0], VN[1], VN[2]);

		ActionHandler.wait(5);
		ActionHandler.click(SVOAdditionalvehicleContainer.MarkStageAsComplete);
		ActionHandler.wait(8);
		CommonFunctions.attachScreenshot();

		driver.navigate().refresh();
		ActionHandler.wait(10);

		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);

		javaScriptUtil.clickElementByJS(SVO_OpportunityInvoiceContainer.EditVehicleBtn);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityInvoiceContainer.VehicleSearch);
		ActionHandler.wait(3);

		ActionHandler.setText(SVO_OpportunityInvoiceContainer.VehicleSearch, Vehicle);
		ActionHandler.wait(8);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVO_OpportunityInvoiceContainer.VehicleNameSearch);
		ActionHandler.wait(8);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityInvoiceContainer.VehicleSelection(Vehicle))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects vehicle for an Opportunity");

		ActionHandler.click(SVO_OpportunityInvoiceContainer.ClassicRetailPrice);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(SVO_OpportunityInvoiceContainer.ClassicRetailPrice, "123");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters retail price for an Opportunity");

		ActionHandler.click(SVO_OpportunityInvoiceContainer.ClassicOppDetailsSave);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves classic Opportunity details");

		driver.navigate().refresh();
		ActionHandler.wait(10);

		ActionHandler.pageDown();
		ActionHandler.wait(1);
		ActionHandler.pageDown();
		ActionHandler.wait(1);
		ActionHandler.pageDown();
		ActionHandler.wait(1);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);

		javaScriptUtil.clickElementByJS(SVO_OpportunityInvoiceContainer.ClassicOppPriceDetailsEditBtn);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityInvoiceContainer.ClassicOppVATQualifying);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityInvoiceContainer.ClassicOppVATQualifyingYes);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVO_OpportunityInvoiceContainer.ClassicOppVATQualifyingSaveBtn);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects VAT qualifying for an opportunity");

		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(5);
		ActionHandler.scrollToView(SVOAdditionalvehicleContainer.MarkStageAsComplete);
		ActionHandler.wait(3);
		ActionHandler.click(SVOAdditionalvehicleContainer.MarkStageAsComplete);
		ActionHandler.wait(8);
		CommonFunctions.attachScreenshot();

		driver.navigate().refresh();
		ActionHandler.wait(10);
		ActionHandler.click(SVOAdditionalvehicleContainer.MarkStageAsComplete);
		ActionHandler.wait(8);
		CommonFunctions.attachScreenshot();

	}

	@Then("^User create new invoice under invoice section with Credit note Record Type \"([^\"]*)\"$")
	public void user_create_new_invoice_under_invoice_section_with_Credit_note(String recordtype) throws Throwable {

		String RT[] = recordtype.split(",");
		recordtype = CommonFunctions.readExcelMasterData(RT[0], RT[1], RT[2]);

		String closeDate = CommonFunctions.selectFutureDate();

		javaScriptUtil.clickElementByJS(SVO_OpportunityInvoiceContainer.InvoiceLink);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Invoice link");

		ActionHandler.click(SVO_OpportunityInvoiceContainer.NewInvoiceBtn);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityInvoiceContainer.RecordType(recordtype))));
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects record type for an invoice");

		ActionHandler.click(SVO_OpportunityInvoiceContainer.InvoiceNextBtn);
		CommonFunctions.attachScreenshot();


		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.OpportunityNameOnInvoiceCreationPage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Verifies the auto-populated opportunity name on invoice page");

		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.AccountNameOnInvoiceCreationPage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the account name field on invoice creation page");

		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.SAPAccountNameOnInvoiceCreationPage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the SAP account name field on invoice creation page");

		ActionHandler.scrollToView(SVO_OpportunityInvoiceContainer.PaymentValue);
		ActionHandler.setText(SVO_OpportunityInvoiceContainer.PaymentValue, "10");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Invoice value");

		ActionHandler.setText(SVO_OpportunityInvoiceContainer.PaymentSentDate, closeDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Payment sent date");

		ActionHandler.click(SVO_OpportunityInvoiceContainer.ClassicOppVATQualifyingSaveBtn);
		CommonFunctions.attachScreenshot();

	}

	@Then("^Verify the invoice details such as Account, Sap, Order and payment details of credit note invoice is displayed on Invoice page$")
	public void verify_the_invoice_details_such_as_Account_Sap_Order_and_payment_details_of_credit_note_invoice_is_not_displayed_on_Invoice_page()
			throws Throwable {

		ActionHandler.wait(8);
		driver.navigate().refresh();
		ActionHandler.wait(10);

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstOrder);
		CommonFunctions.attachScreenshot();

		OpportunityNameOnInvoicePage = SVO_OpportunityInvoiceContainer.OpportunityNameOnInvoicePage.getText();
		Reporter.addStepLog("User Verifies the opportunity name on invoice page as " + OpportunityNameOnInvoicePage);

		AccountNameOnInvoicePage = SVO_OpportunityInvoiceContainer.AccountNameOnInvoicePage.getText();
		Reporter.addStepLog(
				"User Verifies the auto-populated account name on invoice page as " + AccountNameOnInvoicePage);

		SAPAccountNameOnInvoicePage = SVO_OpportunityInvoiceContainer.SAPAccountNameOnInvoicePage.getText();
		Reporter.addStepLog(
				"User Verifies the auto-populated SAP account name on invoice page as " + SAPAccountNameOnInvoicePage);

		OrderNumberOnInvoicePage = SVO_OpportunityInvoiceContainer.OrderNumberOnInvoicePage.getText();
		Reporter.addStepLog("User Verifies the displayed order number on invoice page as " + OrderNumberOnInvoicePage);

		if (OpportunityNameOnInvoicePage.equals(OpportunityName)) {
			Reporter.addStepLog("Opportunity name is same on both invoice and opportunity page ");
		} else {
			Reporter.addStepLog("Opportunity name is not same on both invoice and opportunity page ");
		}

		if (AccountNameOnInvoicePage.equals(AccountName)) {
			Reporter.addStepLog("Account name is same on both invoice and opportunity page ");
		} else {
			Reporter.addStepLog("Account name is not same on both invoice and opportunity page ");
		}

		if (SAPAccountNameOnInvoicePage.equals(SAPAccountName)) {
			Reporter.addStepLog("SAP account name is same on both invoice and opportunity page ");
		} else {
			Reporter.addStepLog("SAP account name is not same on both invoice and opportunity page ");
		}

		if (OrderNumberOnInvoicePage.equals(OrderNumber)) {
			Reporter.addStepLog("Order number is same on both invoice and Order page ");
		} else {
			Reporter.addStepLog("Order number is not same on both invoice and Order page ");
		}

		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);

		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.PaymentValueOnInvoicePage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the payment value on invoice page");

		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.PaymentSentDateOnInvoicePage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the payment sent date on invoice page");

	}

	@Then("^User create new invoice under invoice section with invoice Record Type \"([^\"]*)\"$")
	public void user_create_new_invoice_under_invoice_section_with_invoice(String recordtype) throws Throwable {

		String RT[] = recordtype.split(",");
		recordtype = CommonFunctions.readExcelMasterData(RT[0], RT[1], RT[2]);

		String closeDate = CommonFunctions.selectFutureDate();

		javaScriptUtil.clickElementByJS(SVO_OpportunityInvoiceContainer.InvoiceLink);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Invoice link");

		ActionHandler.click(SVO_OpportunityInvoiceContainer.NewInvoiceBtn);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityInvoiceContainer.RecordType(recordtype))));
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects record type for an invoice");

		ActionHandler.click(SVO_OpportunityInvoiceContainer.InvoiceNextBtn);
		CommonFunctions.attachScreenshot();

		

		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.OpportunityNameOnInvoiceCreationPage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Verifies the auto-populated opportunity name on invoice page");

		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.AccountNameOnInvoiceCreationPage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the account name field on invoice creation page");

		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.SAPAccountNameOnInvoiceCreationPage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the SAP account name field on invoice creation page");

		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.OrderNumberOnInvoiceCreationPage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the Order number field on invoice creation page");

		ActionHandler.scrollToView(SVO_OpportunityInvoiceContainer.PaymentValue);
		ActionHandler.setText(SVO_OpportunityInvoiceContainer.PaymentValue, "10");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Invoice value");

		ActionHandler.setText(SVO_OpportunityInvoiceContainer.PaymentAmount, "123");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Invoice amount");

		ActionHandler.setText(SVO_OpportunityInvoiceContainer.PaymentSentDate, closeDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Payment sent date");

		ActionHandler.setText(SVO_OpportunityInvoiceContainer.PaymentMadeDate, closeDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Payment made date");

		ActionHandler.setText(SVO_OpportunityInvoiceContainer.PaymentSendByDate, closeDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Payment send by date");

		ActionHandler.click(SVO_OpportunityInvoiceContainer.ClassicOppVATQualifyingSaveBtn);
		CommonFunctions.attachScreenshot();

	}

	@Then("^Verify the invoice details such as Account, Sap, Order and payment details of invoice is displayed on Invoice page$")
	public void verify_the_invoice_details_such_as_Account_Sap_Order_and_payment_details_of_invoice_is_not_displayed_on_Invoice_page()
			throws Throwable {

		ActionHandler.wait(8);

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstOrder);
		CommonFunctions.attachScreenshot();

		OpportunityNameOnInvoicePage = SVO_OpportunityInvoiceContainer.OpportunityNameOnInvoicePage.getText();
		Reporter.addStepLog("User Verifies the opportunity name on invoice page as " + OpportunityNameOnInvoicePage);

		AccountNameOnInvoicePage = SVO_OpportunityInvoiceContainer.AccountNameOnInvoicePage.getText();
		Reporter.addStepLog(
				"User Verifies the auto-populated account name on invoice page as " + AccountNameOnInvoicePage);

		SAPAccountNameOnInvoicePage = SVO_OpportunityInvoiceContainer.SAPAccountNameOnInvoicePage.getText();
		Reporter.addStepLog(
				"User Verifies the auto-populated SAP account name on invoice page as " + SAPAccountNameOnInvoicePage);

		OrderNumberOnInvoicePage = SVO_OpportunityInvoiceContainer.OrderNumberOnInvoicePage.getText();
		Reporter.addStepLog("User Verifies the displayed order number on invoice page as " + OrderNumberOnInvoicePage);

		if (OpportunityNameOnInvoicePage.equals(OpportunityName)) {
			Reporter.addStepLog("Opportunity name is same on both invoice and opportunity page ");
		} else {
			Reporter.addStepLog("Opportunity name is not same on both invoice and opportunity page ");
		}

		if (AccountNameOnInvoicePage.equals(AccountName)) {
			Reporter.addStepLog("Account name is same on both invoice and opportunity page ");
		} else {
			Reporter.addStepLog("Account name is not same on both invoice and opportunity page ");
		}

		if (SAPAccountNameOnInvoicePage.equals(SAPAccountName)) {
			Reporter.addStepLog("SAP account name is same on both invoice and opportunity page ");
		} else {
			Reporter.addStepLog("SAP account name is not same on both invoice and opportunity page ");
		}

		if (OrderNumberOnInvoicePage.equals(OrderNumber)) {
			Reporter.addStepLog("Order number is same on both invoice and Order page ");
		} else {
			Reporter.addStepLog("Order number is not same on both invoice and Order page ");
		}

		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);

		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.PaymentValueOnInvoicePage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the payment value on invoice page");

		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.PaymentAmountOnInvoicePage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the payment amount on invoice page");

		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.PaymentSentDateOnInvoicePage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the payment sent date on invoice page");

		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.PaymentMadeDateOnInvoicePage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the payment made date on invoice page");

		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.PaymentSendByDateOnInvoicePage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the payment send by date on invoice page");

	}

	@Then("^Verify the invoice details such as Account, Sap, Order and payment details of deposit invoice are not displayed on Invoice page$")
	public void verify_the_invoice_details_such_as_Account_Sap_Order_and_payment_details_of_deposit_invoice_are_not_displayed_on_Invoice_page()
			throws Throwable {

		ActionHandler.wait(8);
		driver.navigate().refresh();
		ActionHandler.wait(10);

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstOrder);
		CommonFunctions.attachScreenshot();

		OpportunityNameOnInvoicePage = SVO_OpportunityInvoiceContainer.OpportunityNameOnInvoicePage.getText();
		Reporter.addStepLog("User Verifies the opportunity name on invoice page as " + OpportunityNameOnInvoicePage);

		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.AccountNameOnInvoicePage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Account name is not displayed on invoice page of an opportunity");

		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.SAPAccountNameOnInvoicePage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("SAP Account name is not displayed on invoice page of an opportunity");

		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.OrderNumberOnInvoicePage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Order number is not displayed on invoice page of an opportunity");

		if (OpportunityNameOnInvoicePage.equals(OpportunityName)) {
			Reporter.addStepLog("Opportunity name is same on both invoice and opportunity page ");
		} else {
			Reporter.addStepLog("Opportunity name is not same on both invoice and opportunity page ");
		}

		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);

		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.PaymentValueOnInvoicePage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the payment value on invoice page");

		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.PaymentAmountOnInvoicePage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the payment amount on invoice page");

		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.PaymentSentDateOnInvoicePage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the payment sent date on invoice page");

		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.PaymentMadeDateOnInvoicePage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the payment made date on invoice page");

		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.PaymentSendByDateOnInvoicePage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the payment send by date on invoice page");

	}

	@When("^User moves the opportunity to closed Lost stage$")
	public void User_moves_the_opportunity_to_closed_Lost_stage() throws Throwable {

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.OrdersLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Orders link");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstOrder);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens Order details for the opportunity");

		driver.navigate().refresh();
		ActionHandler.wait(10);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(10);

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDrpDwn);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		Reporter.addStepLog("User clicks on Order outcome stage drop down list");

		ActionHandler.click(SVO_OpportunityInvoiceContainer.ClosedLostStage);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDoneBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on done button");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityInvoiceContainer.OpportunityNameOnOrderPage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate back to opportunity");
	}

	@And("^User creates Classic Opportunity without SAP Account field like Stage \"([^\"]*)\" Product Offering \"([^\"]*)\" Region \"([^\"]*)\" Client \"([^\"]*)\" Restricted Party Screening Stage \"([^\"]*)\" Preferred Contact \"([^\"]*)\" SAP Account \"([^\"]*)\" Account Name \"([^\"]*)\" Retailer Contact \"([^\"]*)\" Source \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\"$")
	public void User_create_Classic_Opportunity(String stage, String productOffering, String region, String client,
			String Checkcleared, String preferredContact, String SAPAccount, String accountName, String retailerContact,
			String source, String brand, String model) throws Exception {
		String oppName = "Test_Classic_Service_Opportunity_";
		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		String closeDate = CommonFunctions.selectFutureDate();
		Reporter.addStepLog("Opportunity Close Date is = " + closeDate);

		String s[] = stage.split(",");
		stage = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String proOff[] = productOffering.split(",");
		productOffering = CommonFunctions.readExcelMasterData(proOff[0], proOff[1], proOff[2]);

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String c[] = client.split(",");
		client = CommonFunctions.readExcelMasterData(c[0], c[1], c[2]);

		String RPS[] = Checkcleared.split(",");
		Checkcleared = CommonFunctions.readExcelMasterData(RPS[0], RPS[1], RPS[2]);

		String preCon[] = preferredContact.split(",");
		preferredContact = CommonFunctions.readExcelMasterData(preCon[0], preCon[1], preCon[2]);

		String SAP[] = SAPAccount.split(",");
		SAPAccount = CommonFunctions.readExcelMasterData(SAP[0], SAP[1], SAP[2]);

		String accName[] = accountName.split(",");
		accountName = CommonFunctions.readExcelMasterData(accName[0], accName[1], accName[2]);

		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		String so[] = source.split(",");
		source = CommonFunctions.readExcelMasterData(so[0], so[1], so[2]);

		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String m[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		ActionHandler.wait(3);
		Reporter.addStepLog("User starts creating new SVO Classic Service Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.NewOpportunity);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		

		if (VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.QuoteNextBtn)) {
			ActionHandler.click(SVO_OpportunityInvoiceContainer.SVOClassicServiceOpportunity);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects opportunity record type as Classic Service");

			ActionHandler.click(SVOItemToapproveContainer.QuoteNextBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User clicks on next button");

		}

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeOppName, oppName + randomNumber);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Name");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeClosedDate, closeDate);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Closed date");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeStage);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.StageSelection(stage))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Stage for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeProductOffering);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(
				driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ProOfferingSelection(productOffering))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Product Offering for an Opportunity");

		ActionHandler.scrollToView(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		ActionHandler.wait(2);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.RegionSelection(region))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Region for an Opportunity");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeAccountName, accountName);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeAccountNameSearch);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(accountName))));
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Account Name for an Opportunity");

		
		VerifyHandler.verifyElementPresent(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		CommonFunctions.attachScreenshot();
		ActionHandler
				.click(driver.findElement(By.xpath(RestrictedPartyScreeningContainer.ValueSelection(Checkcleared))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects restricted party screening as Check cleared for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokePreferredContact, preferredContact);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		act1.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(3);
		Reporter.addStepLog("User selects Preferred Contact for an Opportunity");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeBrand, brand);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeBrandSearch);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(brand))));
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Brand for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeModel, model);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(model))));
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Model for an Opportunity");

		ActionHandler.scrollToView(SVOAdditionalvehicleContainer.SVOBespokeSource);
		ActionHandler.wait(2);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSource);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.SourceSelection(source))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Source for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSaveOpportunity);
		ActionHandler.wait(8);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves SVO Bespoke Opportunity");

		ActionHandler.wait(4);
		OpportunityName = SVOAdditionalvehicleContainer.OpportunityName.getText();
		Reporter.addStepLog("Opportunity Name is = " + OpportunityName);
	}

	@Then("^Verify the invoice details such as Account, Order and payment details of invoice is displayed on Invoice page$")
	public void verify_the_invoice_details_such_as_Account_Order_and_payment_details_of_invoice_is_not_displayed_on_Invoice_page()
			throws Throwable {

		ActionHandler.wait(8);
		driver.navigate().refresh();
		ActionHandler.wait(10);

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstOrder);
		CommonFunctions.attachScreenshot();

		OpportunityNameOnInvoicePage = SVO_OpportunityInvoiceContainer.OpportunityNameOnInvoicePage.getText();
		Reporter.addStepLog("User Verifies the opportunity name on invoice page as " + OpportunityNameOnInvoicePage);

		AccountNameOnInvoicePage = SVO_OpportunityInvoiceContainer.AccountNameOnInvoicePage.getText();
		Reporter.addStepLog(
				"User Verifies the auto-populated account name on invoice page as " + AccountNameOnInvoicePage);

		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.SAPAccountfieldOnInvoicePage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Verifies the SAP account name is not auto-populated on invoice page");

		OrderNumberOnInvoicePage = SVO_OpportunityInvoiceContainer.OrderNumberOnInvoicePage.getText();
		Reporter.addStepLog("User Verifies the displayed order number on invoice page as " + OrderNumberOnInvoicePage);

		if (OpportunityNameOnInvoicePage.equals(OpportunityName)) {
			Reporter.addStepLog("Opportunity name is same on both invoice and opportunity page ");
		} else {
			Reporter.addStepLog("Opportunity name is not same on both invoice and opportunity page ");
		}

		if (AccountNameOnInvoicePage.equals(AccountName)) {
			Reporter.addStepLog("Account name is same on both invoice and opportunity page ");
		} else {
			Reporter.addStepLog("Account name is not same on both invoice and opportunity page ");
		}

		if (OrderNumberOnInvoicePage.equals(OrderNumber)) {
			Reporter.addStepLog("Order number is same on both invoice and Order page ");
		} else {
			Reporter.addStepLog("Order number is not same on both invoice and Order page ");
		}

		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);

		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.PaymentValueOnInvoicePage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the payment value on invoice page");

		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.PaymentAmountOnInvoicePage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the payment amount on invoice page");

		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.PaymentSentDateOnInvoicePage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the payment sent date on invoice page");

		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.PaymentMadeDateOnInvoicePage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the payment made date on invoice page");

		VerifyHandler.verifyElementPresent(SVO_OpportunityInvoiceContainer.PaymentSendByDateOnInvoicePage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the payment send by date on invoice page");

	}

	@Then("^User adds SAP account name on opportunity page SAP Account \"([^\"]*)\"$")
	public void user_adds_SAP_account_name_on_opportunity_page_SAP_Account(String SAPAccount) throws Throwable {

		String SAP[] = SAPAccount.split(",");
		SAPAccount = CommonFunctions.readExcelMasterData(SAP[0], SAP[1], SAP[2]);

		javaScriptUtil.clickElementByJS(SVO_OpportunityInvoiceContainer.OpportunityNameOnInvoicePage);
		ActionHandler.wait(8);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates back to opportunity page");

		driver.navigate().refresh();
		ActionHandler.wait(10);
		ActionHandler.pageDown();
		ActionHandler.wait(2);

		javaScriptUtil.clickElementByJS(SVO_OpportunityInvoiceContainer.EditSAPAccountBtn);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunityInvoiceContainer.SAPAccountSearch);
		ActionHandler.setText(SVO_OpportunityInvoiceContainer.SAPAccountSearch, SAPAccount);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVO_OpportunityInvoiceContainer.SAPAccountNameSearch);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityInvoiceContainer.SAPAccount(SAPAccount))));
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects SAP Account for an Opportunity");

		ActionHandler.click(SVO_OpportunityInvoiceContainer.ClassicOppVATQualifyingSaveBtn);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User adds SAP Account to an opportunity");

	}

	@Then("^Verify that Account name and Order details are displayed for the selected classic opportunity$")
	public void verify_that_Account_name_and_Order_details_are_displayed_for_the_selected_classic_opportunity()
			throws Throwable {

		AccountName = null;
		OrderNumber = null;
		SAPAccountName = null;

		ActionHandler.wait(8);
		driver.navigate().refresh();
		ActionHandler.wait(10);

		ActionHandler.pageDown();
		ActionHandler.wait(3);
		AccountName = SVO_OpportunityInvoiceContainer.AccountName.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Verifies the displayed account name as " + AccountName);

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.OrdersLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Orders link");

		ActionHandler.wait(5);
		OrderNumber = SVOAdditionalvehicleContainer.FirstOrder.getText();
		Reporter.addStepLog("User Verifies the displayed order number as " + OrderNumber);

		driver.navigate().back();

	}

}
