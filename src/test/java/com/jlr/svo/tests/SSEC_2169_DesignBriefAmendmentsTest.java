package com.jlr.svo.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.containers.SSEC_1039_SVOEnquiryEmailRemaindersContainer;
import com.jlr.svo.containers.SSEC_1503_Clasic_Email_Signature_On_Salesforce_Container;
import com.jlr.svo.containers.SSEC_2157_DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer;
import com.jlr.svo.containers.SSEC_2158_DesignBriefAmendments_Wheels_TreadPlatesContainer;
import com.jlr.svo.containers.SSEC_2159_BespokeOrderForm_CreationofOrderFormRecordsContainer;
import com.jlr.svo.containers.SSEC_2169_DesignBriefAmendments_SeatsContainer;
import com.jlr.svo.containers.SVOAccountsContainer;
import com.jlr.svo.containers.SVOAdditionalVehicleContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.containers.SVOItemToApproveContainer;
import com.jlr.svo.containers.SVOOpportunityContainer;
import com.jlr.svo.containers.SVO_CustomerResponsesContainer;
import com.jlr.svo.containers.SVO_EnquiryLostReasonContainer;
import com.jlr.svo.containers.SVO_RestrictedPartyScreeningContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class SSEC_2169_DesignBriefAmendmentsTest extends TestBaseCC {

	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory
			.getLogger(SSEC_1039_SVOEnquiryEmailRemaindersTest.class.getName());
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);

	SVOAccountsContainer SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
	SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	SVOAdditionalVehicleContainer SVOAdditionalvehicleContainer = PageFactory.initElements(driver,
			SVOAdditionalVehicleContainer.class);
	SVO_RestrictedPartyScreeningContainer RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
			SVO_RestrictedPartyScreeningContainer.class);
	SVOItemToApproveContainer SVOItemToapproveContainer = PageFactory.initElements(driver,
			SVOItemToApproveContainer.class);
	SVO_CustomerResponsesContainer SVO_CustomerResponsesContainer = PageFactory.initElements(driver,
			SVO_CustomerResponsesContainer.class);
	SVO_EnquiryLostReasonContainer SVO_EnquiryLostReasonContainer = PageFactory.initElements(driver,
			SVO_EnquiryLostReasonContainer.class);
	SSEC_1039_SVOEnquiryEmailRemaindersContainer SVOEnquiryEmailRemaindersContainer = PageFactory.initElements(driver,
			SSEC_1039_SVOEnquiryEmailRemaindersContainer.class);
	SSEC_1503_Clasic_Email_Signature_On_Salesforce_Container Clasic_Email_Signature_On_Salesforce_Container = PageFactory.initElements(driver,
			SSEC_1503_Clasic_Email_Signature_On_Salesforce_Container.class);
	SSEC_2159_BespokeOrderForm_CreationofOrderFormRecordsContainer BespokeOrderForm_CreationofOrderFormRecordsContainer = PageFactory.initElements(driver, SSEC_2159_BespokeOrderForm_CreationofOrderFormRecordsContainer.class);
	SSEC_2157_DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer = PageFactory.initElements(driver, SSEC_2157_DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer.class);
	SSEC_2158_DesignBriefAmendments_Wheels_TreadPlatesContainer DesignBriefAmendments_Wheels_TreadPlatesContainer = PageFactory.initElements(driver, SSEC_2158_DesignBriefAmendments_Wheels_TreadPlatesContainer.class);
	SSEC_2169_DesignBriefAmendments_SeatsContainer DesignBriefAmendments_SeatsContainer = PageFactory.initElements(driver, SSEC_2169_DesignBriefAmendments_SeatsContainer.class);
	
	public static String verificationCode;
	public static String vCode;
	public static String veriCode;
	public static String EnquiryName;

	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}
	

	public void onStart() {

//		setupTest("SVOTest");
//		driver = getDriver();
//
//		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
//		SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
//		SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
//		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
//		SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
//		SVOAdditionalvehicleContainer = PageFactory.initElements(driver, SVOAdditionalVehicleContainer.class);
//
//		RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
//				SVO_RestrictedPartyScreeningContainer.class);
//		SVOItemToapproveContainer = PageFactory.initElements(driver, SVOItemToApproveContainer.class);
//		SVO_CustomerResponsesContainer = PageFactory.initElements(driver, SVO_CustomerResponsesContainer.class);
//		SVO_EnquiryLostReasonContainer = PageFactory.initElements(driver, SVO_EnquiryLostReasonContainer.class);
//		SVOEnquiryEmailRemaindersContainer = PageFactory.initElements(driver,
//				SSEC_1039_SVOEnquiryEmailRemaindersContainer.class);
//		Clasic_Email_Signature_On_Salesforce_Container = PageFactory.initElements(driver,
//				SSEC_1503_Clasic_Email_Signature_On_Salesforce_Container.class);
//		BespokeOrderForm_CreationofOrderFormRecordsContainer = PageFactory.initElements(driver, SSEC_2159_BespokeOrderForm_CreationofOrderFormRecordsContainer.class);
//		DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer = PageFactory.initElements(driver, SSEC_2157_DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer.class);
//		
//		
//		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
//		
//
//		verificationCode = null;
//		vCode = null;
//		EnquiryName = null;

	}

	
    @Then("^Click on Edit button of Monotone/Duotone \"([^\"]*)\" from Seats$")
    public void Click_on_Edit_button_of_Monotone_Duotone_from_seats(String SeatsMonotoneDuo) throws Exception{
    	
    	String MonoDuo[] = SeatsMonotoneDuo.split(",");
    	SeatsMonotoneDuo = CommonFunctions.readExcelMasterData(MonoDuo[0], MonoDuo[1], MonoDuo[2]);

    	ActionHandler.pageDown();

    	ActionHandler.click(DesignBriefAmendments_SeatsContainer.EditMonotoneDuotone);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Edit Monotone Duotone button");
    	
    	ActionHandler.scrollToView(DesignBriefAmendments_SeatsContainer.SeatDetailsText);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User scrolls to view Seats Details text");
    	
    	ActionHandler.click(DesignBriefAmendments_SeatsContainer.MonotoneDuotoneLabel);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_SeatsContainer.Seats(SeatsMonotoneDuo))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Monotone / Duotone Drop Down");
    }
    
    @And("^Select the Hand of Drive \"([^\"]*)\" and front \"([^\"]*)\" and rear seat materials \"([^\"]*)\" from drop down$")
    public void Select_MonotoneDuotone_HandOfDrive_and_FrontSeat(String HandofDrive, String FrontSeatCushMaterial, String RearSeatCushMaterial) throws Exception{
    	
    	String HoD[] = HandofDrive.split(",");
    	HandofDrive = CommonFunctions.readExcelMasterData(HoD[0], HoD[1], HoD[2]);

    	String FrontSCM[] = FrontSeatCushMaterial.split(",");
    	FrontSeatCushMaterial = CommonFunctions.readExcelMasterData(FrontSCM[0], FrontSCM[1], FrontSCM[2]);

    	String RearSCM[] = RearSeatCushMaterial.split(",");
    	RearSeatCushMaterial = CommonFunctions.readExcelMasterData(RearSCM[0], RearSCM[1], RearSCM[2]);

    	ActionHandler.click(DesignBriefAmendments_SeatsContainer.HandofDriveLabel);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_SeatsContainer.Seats(HandofDrive))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Hand of Drive Drop Down");
    	
    	ActionHandler.click(DesignBriefAmendments_SeatsContainer.FrontSeatCushionMaterial);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_SeatsContainer.Seats(FrontSeatCushMaterial))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Front Sear Cushion material Drop Down");
    	
    	ActionHandler.click(DesignBriefAmendments_SeatsContainer.RearSeatMaterial);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_SeatsContainer.Seats(RearSeatCushMaterial))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Rear Seat Material Drop Down");
    }

    @Then("^Click on Edit button of Contrast Stitch Type and select Monotone \"([^\"]*)\" from Contrast Stitch Type Brief drop down$")
    public void Click_on_Edit_button_of_ContrastStitchType_and_select_Monotone(String ContrastStitchType) throws Exception{
    	
    	String ContrastST[] = ContrastStitchType.split(",");
    	ContrastStitchType = CommonFunctions.readExcelMasterData(ContrastST[0], ContrastST[1], ContrastST[2]);

    	ActionHandler.pageDown();
    	ActionHandler.wait(2);

    	ActionHandler.click(DesignBriefAmendments_SeatsContainer.EditContrastStitchTypeBtn);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Edit Contrast Stitch Type");
    	
    	ActionHandler.scrollToView(DesignBriefAmendments_SeatsContainer.SeatDetailsText);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User scrolls to view Seat Details text");
    	
    	ActionHandler.click(DesignBriefAmendments_SeatsContainer.ContrastStitchTypeLabel);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_SeatsContainer.Seats(ContrastStitchType))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Contrast Stitch Type Drop Down");
    	
    }
    
    @Then("^Select Contrast Color1 \"([^\"]*)\" from drop down$")
    public void Select_Contrast_Color1_from_dropDown(String ContrastColor1) throws Exception{

    	String ContrastC1[] = ContrastColor1.split(",");
    	ContrastColor1 = CommonFunctions.readExcelMasterData(ContrastC1[0], ContrastC1[1], ContrastC1[2]);

    	ActionHandler.click(DesignBriefAmendments_SeatsContainer.ContrastColor1);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_SeatsContainer.Seats(ContrastColor1))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Contrast Color1 Drop Down");
    }
    
    @Then("^Click on Edit button of Contrast Stitch Type and select Duotone \"([^\"]*)\" from Contrast Stitch Type drop down$")
    public void Click_on_Edit_button_of_ContrastStitchType_and_select_Duotone(String ContrastStitchType) throws Exception{
    	
    	String ContrastST[] = ContrastStitchType.split(",");
    	ContrastStitchType = CommonFunctions.readExcelMasterData(ContrastST[0], ContrastST[1], ContrastST[2]);

    	ActionHandler.pageDown();

    	ActionHandler.click(DesignBriefAmendments_SeatsContainer.EditContrastStitchTypeBtn);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Edit Contrast Stitch Type button");
    	
    	ActionHandler.scrollToView(DesignBriefAmendments_SeatsContainer.SeatDetailsText);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User scrolls to view Wheels Details text");
    	
    	ActionHandler.click(DesignBriefAmendments_SeatsContainer.ContrastStitchTypeLabel);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_SeatsContainer.Seats(ContrastStitchType))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Contrast Stitch Type Drop Down");
    	
    }
    
    @Then("^Select Contrast Color1 \"([^\"]*)\" and Color2 \"([^\"]*)\" from drop down$")
    public void Select_Contrat_Color1_Color2_from_dropDown(String ContrastColor1, String ContrastColor2) throws Exception{
    	
    	String ContrastC1[] = ContrastColor1.split(",");
    	ContrastColor1 = CommonFunctions.readExcelMasterData(ContrastC1[0], ContrastC1[1], ContrastC1[2]);

    	String ContrastC2[] = ContrastColor2.split(",");
    	ContrastColor2 = CommonFunctions.readExcelMasterData(ContrastC2[0], ContrastC2[1], ContrastC2[2]);

    	ActionHandler.click(DesignBriefAmendments_SeatsContainer.ContrastColor1);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_SeatsContainer.Seats(ContrastColor1))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Contrast Color1 Drop Down");
    	
    	ActionHandler.click(DesignBriefAmendments_SeatsContainer.ContrastColor2);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_SeatsContainer.Seats(ContrastColor2))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Contrast Color2 Drop Down");
    }
    
    @Then("^Click on Edit button of Contrast Stitch Type and select the Duotone Full contrast and Third Color option \"([^\"]*)\" from drop down$")
    public void Click_on_Edit_button_of_ContrastStitchType_and_select_DuotoneFullContrastAndThirdColorOption(String ContrastStitchType) throws Exception{
    	
    	String ContrastST[] = ContrastStitchType.split(",");
    	ContrastStitchType = CommonFunctions.readExcelMasterData(ContrastST[0], ContrastST[1], ContrastST[2]);

    	ActionHandler.pageDown();

    	ActionHandler.click(DesignBriefAmendments_SeatsContainer.EditContrastStitchTypeBtn);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Edit Contrast Stitch Type button");
    	
    	ActionHandler.scrollToView(DesignBriefAmendments_SeatsContainer.SeatDetailsText);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User scrolls to view Seat Details text");
    	
    	ActionHandler.click(DesignBriefAmendments_SeatsContainer.ContrastStitchTypeLabel);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_SeatsContainer.Seats(ContrastStitchType))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Contrast Stitch Type Drop Down");
    
    }
    
    @Then("^Select Contrast Color1 \"([^\"]*)\" and Color2 \"([^\"]*)\" and Color3 \"([^\"]*)\" from drop down$")
    public void Select_Contrast_Color1_and_Contrast2_and_Contrast3(String ContrastColor1, String ContrastColor2, String ContrastColor3) throws Exception{
    	
    	String ContrastC1[] = ContrastColor1.split(",");
    	ContrastColor1 = CommonFunctions.readExcelMasterData(ContrastC1[0], ContrastC1[1], ContrastC1[2]);

    	String ContrastC2[] = ContrastColor2.split(",");
    	ContrastColor2 = CommonFunctions.readExcelMasterData(ContrastC2[0], ContrastC2[1], ContrastC2[2]);

    	String ContrastC3[] = ContrastColor3.split(",");
    	ContrastColor3 = CommonFunctions.readExcelMasterData(ContrastC3[0], ContrastC3[1], ContrastC3[2]);

    	ActionHandler.click(DesignBriefAmendments_SeatsContainer.ContrastColor1);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_SeatsContainer.Seats(ContrastColor1))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Contrast Color1 Drop Down");
    	
    	ActionHandler.click(DesignBriefAmendments_SeatsContainer.ContrastColor2);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_SeatsContainer.Seats(ContrastColor2))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Contrast Color2 Drop Down");
    	
    	ActionHandler.click(DesignBriefAmendments_SeatsContainer.ContrastColor3);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_SeatsContainer.Seats(ContrastColor3))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Contrast Color3 Drop Down");
    }
    
    @Then("^Click on Edit button of Seat Embroidery and select Personalized option \"([^\"]*)\" from the drop down$")
    public void Click_on_Edit_button_of_Seat_Embroidery_and_select_Personalized_option(String SeatEmbroiPersonalized) throws Exception{
    	
    	String SeatEPersonalized[] = SeatEmbroiPersonalized.split(",");
    	SeatEmbroiPersonalized = CommonFunctions.readExcelMasterData(SeatEPersonalized[0], SeatEPersonalized[1], SeatEPersonalized[2]);

    	ActionHandler.pageDown();

    	ActionHandler.click(DesignBriefAmendments_SeatsContainer.EditSeatEmbroidery);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Edit Seat Embroidery button");
    	
    	ActionHandler.scrollToView(DesignBriefAmendments_SeatsContainer.SeatDetailsText);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User scrolls to view Wheels Details text");
    	
    	ActionHandler.click(DesignBriefAmendments_SeatsContainer.ContrastStitchTypeLabel);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_SeatsContainer.Seats(SeatEmbroiPersonalized))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Contrast Stitch Type Drop Down");
    	
    }
    
    @Then("^Add the additional details in the text box$")
    public void Add_the_additional_details_in_the_text_box() throws Exception{
    	
    	ActionHandler.click(DesignBriefAmendments_SeatsContainer.SeatEmbroiPersonalizationDetails);
    	ActionHandler.wait(1);
    	ActionHandler.setText(DesignBriefAmendments_SeatsContainer.SeatEmbroiPersonalizationDetails, "test");
    	ActionHandler.wait(1);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Seat Embroidery Personalization Details");
    	
    }
    
    @Then("^Click on Edit button of Rear Seat Cushion color \"([^\"]*)\"$")
    public void Click_on_Edit_button_of_Rear_Seat_Cushion_Color(String RearSeatCColor) throws Exception{
    	
    	String SeatEPersonalized[] = RearSeatCColor.split(",");
    	RearSeatCColor = CommonFunctions.readExcelMasterData(SeatEPersonalized[0], SeatEPersonalized[1], SeatEPersonalized[2]);

    	ActionHandler.pageDown();

    	ActionHandler.click(DesignBriefAmendments_SeatsContainer.EditRearSeatCushionColor);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Edit Rear Seat Cushion Color");
    	
    	ActionHandler.scrollToView(DesignBriefAmendments_SeatsContainer.SeatDetailsText);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User scrolls to view Wheels Details text");
    	
    	ActionHandler.click(DesignBriefAmendments_SeatsContainer.RearSeatCushionColorText);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_SeatsContainer.Seats(RearSeatCColor))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Rear Seat Cushion Color Drop Down");
    	
    }
    
    @And("^Select Rear seat cushion color backboard color and bulkhead colors from the drop down$")
    public void Select_RearSeatCushionColor_BackboardColor_and_BulkHeadColor_from_dropDown(String RearSeatBackboardColor, String RearSeatBackground) throws Exception{

    	String RearSBackboardColor[] = RearSeatBackboardColor.split(",");
    	RearSeatBackboardColor = CommonFunctions.readExcelMasterData(RearSBackboardColor[0], RearSBackboardColor[1], RearSBackboardColor[2]);

    	String RearSBackgroundColor[] = RearSeatBackground.split(",");
    	RearSeatBackground = CommonFunctions.readExcelMasterData(RearSBackgroundColor[0], RearSBackgroundColor[1], RearSBackgroundColor[2]);

    	ActionHandler.click(DesignBriefAmendments_SeatsContainer.RearSeatBackboardColor);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_SeatsContainer.Seats(RearSeatBackboardColor))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Rear Seat Backboard Color Drop Down");
    	
    	ActionHandler.click(DesignBriefAmendments_SeatsContainer.RearSeatBackgroundColor);
    	ActionHandler.click(driver.findElement(By.xpath(DesignBriefAmendments_SeatsContainer.Seats(RearSeatBackground))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("User clicks on Rear Seat Background Color Drop Down");
    	
    }













}
