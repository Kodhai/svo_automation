package com.jlr.svo.tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.containers.SSEC_2159_BespokeOrderForm_CreationofOrderFormRecordsContainer;
import com.jlr.svo.containers.SVOAccountsContainer;
import com.jlr.svo.containers.SVOAdditionalVehicleContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.containers.SVOItemToApproveContainer;
import com.jlr.svo.containers.SVOOpportunityContainer;
import com.jlr.svo.containers.SVOWorkOrderContainer;
import com.jlr.svo.containers.SVO_OpportunityInvoiceContainer;
import com.jlr.svo.containers.SVO_RestrictedPartyScreeningContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class SSEC_2159_BespokeOrderForm_CreationofOrderFormRecords extends TestBaseCC {

	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory.getLogger(SVOWorkOrderProgrammeField_Test.class.getName());
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);

	SVOAccountsContainer SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
	SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	SVOAdditionalVehicleContainer SVOAdditionalvehicleContainer = PageFactory.initElements(driver,
			SVOAdditionalVehicleContainer.class);
	SVOWorkOrderContainer SVOWorkOrderContainer = PageFactory.initElements(driver, SVOWorkOrderContainer.class);
	SVO_OpportunityInvoiceContainer SVO_OpportunityInvoiceContainer = PageFactory.initElements(driver,
			SVO_OpportunityInvoiceContainer.class);
	SVOItemToApproveContainer SVOItemToapproveContainer = PageFactory.initElements(driver,
			SVOItemToApproveContainer.class);
	SVO_RestrictedPartyScreeningContainer RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
			SVO_RestrictedPartyScreeningContainer.class);
	SSEC_2159_BespokeOrderForm_CreationofOrderFormRecordsContainer BespokeOrderForm_CreationofOrderFormRecordsContainer = PageFactory.initElements(driver, SSEC_2159_BespokeOrderForm_CreationofOrderFormRecordsContainer.class);
	

	public static String verificationCode;
	public static String vCode;
	public static String veriCode;
	public static String WorkOrderNumber;
	public static String OpportunityName;

	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	public void onStart() {

		setupTest("SVOTest");
		driver = getDriver();

		SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
		SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		SVOAdditionalvehicleContainer = PageFactory.initElements(driver, SVOAdditionalVehicleContainer.class);
		SVOWorkOrderContainer = PageFactory.initElements(driver, SVOWorkOrderContainer.class);
		SSEC_2159_BespokeOrderForm_CreationofOrderFormRecordsContainer BespokeOrderForm_CreationofOrderFormRecordsContainer = PageFactory.initElements(driver, SSEC_2159_BespokeOrderForm_CreationofOrderFormRecordsContainer.class);

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		verificationCode = null;
		vCode = null;
		WorkOrderNumber = null;
		
		OpportunityName = SVOAdditionalvehicleContainer.OpportunityName.getText();


	}


    @Then("^Verify that new Bespoke Design Brief record is created$")
    public void Verify_that_new_Bespoke_Design_Brief_record_is_created() throws Exception{
    	
    	ActionHandler.click(BespokeOrderForm_CreationofOrderFormRecordsContainer.QuoteCreatedLink);
    	ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigate to Quote created");

    	ActionHandler.click(BespokeOrderForm_CreationofOrderFormRecordsContainer.DesignBriefsQuickLink);
    	ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigate to Design Brief from Quick Link");
		
    	VerifyHandler.verifyElementPresent(BespokeOrderForm_CreationofOrderFormRecordsContainer.DesignBriefsText);
    	ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Design Brief text");
		
    }
    
    
    @And("^Navigate to Orders tab$")
    public void Navigate_o_orders_tab() throws Exception{
    	
    	driver.navigate().back();
    	ActionHandler.wait(4);
    	
    	driver.navigate().back();
    	ActionHandler.wait(4);
    	
		ActionHandler.click(BespokeOrderForm_CreationofOrderFormRecordsContainer.OrdersTab);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigates to Orders tab");
		
    }
    
    @And("^User fills all the mandatory details for a Order record and Status as Committed to build \"([^\"]*)\"$")
    public void User_fills_all_mandatory_details_for_Order_record(String Status) throws Exception{
    	
    	String Sts[] = Status.split(",");
    	Status = CommonFunctions.readExcelMasterData(Sts[0], Sts[1], Sts[2]);

		ActionHandler.click(BespokeOrderForm_CreationofOrderFormRecordsContainer.NewOrderBtn);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on New Order button");
		
		ActionHandler.click(BespokeOrderForm_CreationofOrderFormRecordsContainer.CommonOrderFormText);
		ActionHandler.setText(BespokeOrderForm_CreationofOrderFormRecordsContainer.CommonOrderFormText, "1234");
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User fills Common Order Form field");
		
		ActionHandler.click(BespokeOrderForm_CreationofOrderFormRecordsContainer.OpportunityText);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Opportunity field");
		ActionHandler.setText(BespokeOrderForm_CreationofOrderFormRecordsContainer.OpportunityText, OpportunityName);
		ActionHandler.click(driver.findElement(By.xpath(BespokeOrderForm_CreationofOrderFormRecordsContainer.RetailerContact(OpportunityName))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Opportunity option");
		
		ActionHandler.click(BespokeOrderForm_CreationofOrderFormRecordsContainer.StatusOrderCreatedText);
		ActionHandler.click(driver.findElement(By.xpath(BespokeOrderForm_CreationofOrderFormRecordsContainer.StatusOrder(Status))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Status from drop down");
		
		ActionHandler.pageDown();
		ActionHandler.wait(2);
		
		ActionHandler.click(BespokeOrderForm_CreationofOrderFormRecordsContainer.OrderStartDateText);
		ActionHandler.click(BespokeOrderForm_CreationofOrderFormRecordsContainer.TodayBtnOrders);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects date from Order start date");
		
		ActionHandler.click(BespokeOrderForm_CreationofOrderFormRecordsContainer.SaveOrderBtn);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Save order button");
		
    }
    
    @And("^User fills all the mandatory details for a Order record and select status other than Committed to build$")
    public void User_fills_all_mandatory_details_for_Order_record_and_change_status() throws Exception{
    	
        ActionHandler.click(BespokeOrderForm_CreationofOrderFormRecordsContainer.NewOrderBtn);
        ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on New button");
		
		ActionHandler.click(BespokeOrderForm_CreationofOrderFormRecordsContainer.CommonOrderFormText);
		ActionHandler.setText(BespokeOrderForm_CreationofOrderFormRecordsContainer.CommonOrderFormText, "1234");
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on COmmon Order form field");
		
		ActionHandler.click(BespokeOrderForm_CreationofOrderFormRecordsContainer.OpportunityText);
		ActionHandler.setText(BespokeOrderForm_CreationofOrderFormRecordsContainer.OpportunityText, OpportunityName);
		ActionHandler.click(driver.findElement(By.xpath(BespokeOrderForm_CreationofOrderFormRecordsContainer.RetailerContact(OpportunityName))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Opportunity from drop down");
		
		ActionHandler.pageDown();
		ActionHandler.wait(2);
		
		ActionHandler.click(BespokeOrderForm_CreationofOrderFormRecordsContainer.OrderStartDateText);
		ActionHandler.click(BespokeOrderForm_CreationofOrderFormRecordsContainer.TodayBtnOrders);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects date from order start date");
		
		ActionHandler.click(BespokeOrderForm_CreationofOrderFormRecordsContainer.SaveOrderBtn);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Save button");
		
    }
    
    @And("^Verify that Order is created$")
    public void Verify_that_Order_is_created() throws Exception{
    	
		ActionHandler.click(BespokeOrderForm_CreationofOrderFormRecordsContainer.OrderRecord);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Order record");
		
    }
    
    @And("^Verify that Order form is automatically created in the Orders record$")
    public void Verify_that_orderForm_is_automatically_created_in_Orders_record() throws Exception{
    	
    	ActionHandler.pageDown();
    	ActionHandler.wait(2);
		
		VerifyHandler.verifyElementPresent(BespokeOrderForm_CreationofOrderFormRecordsContainer.OrderFormRec);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Order form record");
		
    }
    
    @And("^Verify that the Order form details are populated from Design Brief details$")
    public void Verify_that_order_form_details_are_populated() throws Exception{
    	
		VerifyHandler.verifyElementPresent(BespokeOrderForm_CreationofOrderFormRecordsContainer.OwnerOrders);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Owner");
		
		VerifyHandler.verifyElementPresent(BespokeOrderForm_CreationofOrderFormRecordsContainer.MonotoneDuotoneText);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Monotone field");
		
		VerifyHandler.verifyElementPresent(BespokeOrderForm_CreationofOrderFormRecordsContainer.HandOfDrive);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Hand of Drive");
		
    }
    
    @And("^Verify that Order form is not created in the Orders record$")
    public void Verify_that_Order_form_is_not_created_in_Order_record() throws Exception{
    	
    	javaScriptUtil.clickElementByJS(SVO_OpportunityContainer.OpportTab);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigate to Opportunity");
		
		ActionHandler.click(BespokeOrderForm_CreationofOrderFormRecordsContainer.OpportunityRec);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Opportunity record");
		
		ActionHandler.pageDown();
		ActionHandler.wait(2);
		
		ActionHandler.click(BespokeOrderForm_CreationofOrderFormRecordsContainer.OrdersTab);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Orders tab");
		
		ActionHandler.click(BespokeOrderForm_CreationofOrderFormRecordsContainer.OrderRecord);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Order record");
		
		ActionHandler.pageDown();
		ActionHandler.wait(2);
		
		VerifyHandler.verifyElementPresent(BespokeOrderForm_CreationofOrderFormRecordsContainer.OrderFormsLink);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Order forms link");
		
    }
    
    @And("^User creates a Bespoke Opportunity with check cleared like Stage \"([^\"]*)\" Product Offering \"([^\"]*)\" Region \"([^\"]*)\" Client \"([^\"]*)\" Restricted Party Screening Stage \"([^\"]*)\" Preferred Contact \"([^\"]*)\" Account Name \"([^\"]*)\" Retailer Contact \"([^\"]*)\" Source \"([^\"]*)\" Origination Divission \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\"$")
	public void User_create_a_SVO_Bespoke_Opportunity_with_check_cleared(String stage, String productOffering,
			String region, String client, String Checkcleared, String preferredContact, String accountName,
			String retailerContact, String source, String originatingdivission, String brand, String model)
			throws Exception {
		
		OpportunityName = null;
		String oppName = "Test_SVO_Bespoke_Opportunity_";
		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		String closeDate = CommonFunctions.selectFutureDate();
		Reporter.addStepLog("Opportunity Close Date is = " + closeDate);

		String s[] = stage.split(",");
		stage = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String proOff[] = productOffering.split(",");
		productOffering = CommonFunctions.readExcelMasterData(proOff[0], proOff[1], proOff[2]);

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String c[] = client.split(",");
		client = CommonFunctions.readExcelMasterData(c[0], c[1], c[2]);

		String RPS[] = Checkcleared.split(",");
		Checkcleared = CommonFunctions.readExcelMasterData(RPS[0], RPS[1], RPS[2]);

		String preCon[] = preferredContact.split(",");
		preferredContact = CommonFunctions.readExcelMasterData(preCon[0], preCon[1], preCon[2]);

		String accName[] = accountName.split(",");
		accountName = CommonFunctions.readExcelMasterData(accName[0], accName[1], accName[2]);

		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		String so[] = source.split(",");
		source = CommonFunctions.readExcelMasterData(so[0], so[1], so[2]);

		String OD[] = originatingdivission.split(",");
		originatingdivission = CommonFunctions.readExcelMasterData(OD[0], OD[1], OD[2]);

		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String m[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		Reporter.addStepLog("User starts creating new SVO Bespoke Opportunity");
		ActionHandler.click(SVOAdditionalvehicleContainer.NewOpportunity);
		CommonFunctions.attachScreenshot();

//		if (VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.QuoteNextBtn)) {
//			ActionHandler.click(SVOItemToapproveContainer.SVOBespokeOpportunity);
//			CommonFunctions.attachScreenshot();
//			Reporter.addStepLog("User selects opportunity record type as SVO bespoke");
//
//			ActionHandler.click(SVOItemToapproveContainer.QuoteNextBtn);
//			CommonFunctions.attachScreenshot();
//			Reporter.addStepLog("User clicks on next button");
//
//		}

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeOppName, oppName + randomNumber);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Name");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeClosedDate, closeDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Closed date");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeStage);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.StageSelection(stage))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Stage for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeProductOffering);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(
				driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ProOfferingSelection(productOffering))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Product Offering for an Opportunity");

		ActionHandler.scrollToView(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		ActionHandler.wait(2);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.RegionSelection(region))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Region for an Opportunity");

//		VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.SVOBespokeClient);
//		CommonFunctions.attachScreenshot();
//		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeClient, client);
//		CommonFunctions.attachScreenshot();
//
//		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeClientNameSearch);
//		CommonFunctions.attachScreenshot();
//		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(client))));
//		CommonFunctions.attachScreenshot();
//		Reporter.addStepLog("User selects Client for an Opportunity");

		VerifyHandler.verifyElementPresent(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		CommonFunctions.attachScreenshot();
		ActionHandler
				.click(driver.findElement(By.xpath(RestrictedPartyScreeningContainer.ValueSelection(Checkcleared))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects restricted party screening as Check cleared for an Opportunity");

		ActionHandler.click(SVOItemToapproveContainer.PreferredContact);
		ActionHandler.wait(3);
//		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokePreferredContact, preferredContact);
//		ActionHandler.click(SVOItemToapproveContainer.SVOBespokePreferredContactname);
		ActionHandler.click(driver.findElement(By.xpath(BespokeOrderForm_CreationofOrderFormRecordsContainer.PrefContact(preferredContact))));
		ActionHandler.wait(3);
		Reporter.addStepLog("User selects Preferred Contact for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeAccountName);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
//		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeAccountNameSearch);
//		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(BespokeOrderForm_CreationofOrderFormRecordsContainer.PrefContact(accountName))));
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		Reporter.addStepLog("User selects Account Name for an Opportunity");

//		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeRetailerContact, retailerContact);
//		ActionHandler.click(SVOItemToapproveContainer.SVOBespokePreferredContactname);
//		Reporter.addStepLog("User selects Retailer Contact for an Opportunity");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
//		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSource);
//		CommonFunctions.attachScreenshot();
//		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.SourceSelection(source))));
//		CommonFunctions.attachScreenshot();
//		Reporter.addStepLog("User chooses Source for an Opportunity");
//
//		ActionHandler.click(RestrictedPartyScreeningContainer.SVOOriginatingDivission);
//		CommonFunctions.attachScreenshot();
//		ActionHandler.click(
//				driver.findElement(By.xpath(RestrictedPartyScreeningContainer.OriginatingDivisionSelection(originatingdivission))));
//		CommonFunctions.attachScreenshot();
//		Reporter.addStepLog("User chooses Originating Division for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeBrand, brand);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeBrandSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(brand))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Brand for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeModel, model);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(model))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Model for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSaveOpportunity);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves SVO Bespoke Opportunity");

		OpportunityName = SVOAdditionalvehicleContainer.OpportunityName.getText();
		Reporter.addStepLog("Opportunity Name is = " + OpportunityName);
	}





    	


    		
    		

}
