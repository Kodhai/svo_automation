package com.jlr.svo.tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.containers.SSEC_2369_DesignBriefAmendmentsContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.Then;

public class SSEC_2542_SVO_Bespoke_Quote_Design_Brief_fields_on_Related_List_Test extends TestBaseCC {

	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory.getLogger(SVOWorkOrderProgrammeField_Test.class.getName());
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
	com.jlr.svo.containers.SSEC_2369_DesignBriefAmendmentsContainer SSEC_2369_DesignBriefAmendmentsContainer = PageFactory.initElements(driver, SSEC_2369_DesignBriefAmendmentsContainer.class);

	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	public void onStart() {

		setupTest("SVOTest");
		driver = getDriver();

		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		SSEC_2369_DesignBriefAmendmentsContainer = PageFactory.initElements(driver, SSEC_2369_DesignBriefAmendmentsContainer.class);

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);


	}
	
    @Then("^User is able to view the fields under the design brief related list on quotes page$")
    public void User_is_able_to_view_the_fields_under_the_design_brief_related_list_on_quotes_page() throws Exception{
    	
    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	
    	
    }
    
    @Then("^User is able to remove the design brief related list on quotes page$")
    public void User_is_able_to_remove_the_the_design_brief_related_list_on_quotes_page() throws Exception{
    	
    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	
    
    	
    }
    
    @Then("^User is able to edit the design brief related list on quotes page$")
    public void User_is_able_to_edit_the_the_design_brief_related_list_on_quotes_page() throws Exception{
    	
    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	
    
    	
    }

}
