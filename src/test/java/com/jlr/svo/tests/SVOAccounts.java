package com.jlr.svo.tests;

import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.autotest.utils.Config;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.constants.Constants;
import com.jlr.svo.containers.SVOAccountsContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SVOAccounts extends TestBaseCC 
{
	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions=new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory.getLogger(SVOAccounts.class.getName());
	SVOAccountsContainer SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	public static String verificationCode;
	public static String vCode;
	public static String veriCode;
	public static String accountName;

	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	public void onStart() {
		setupTest("SVOTest");
		driver = getDriver();
		SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
		verificationCode = null;
		vCode = null;
	}

	// Navigate to Home page//
	public void AccountsHomePage() throws Exception {
		ActionHandler.wait(3);
		ActionHandler.click(SVOAccountsContainer.Accounts);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
	}

	// Below code is to navigate for SVO page//
	public void navigateToSVO() throws Exception {
		String SVO = "Special Vehicle Operations";
		ActionHandler.wait(3);
		Reporter.addStepLog("User tries to navigate to SVO Portal");
		if (VerifyHandler.verifyElementPresent(SVOAccountsContainer.jlrText)) {
			// user click on menu button//
			ActionHandler.wait(2);
			ActionHandler.click(SVOAccountsContainer.menuBtn);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			// user enter text in searc box//
			ActionHandler.setText(SVOAccountsContainer.searchBox, SVO);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.selectMenu(SVO))));
			ActionHandler.wait(10);
			CommonFunctions.attachScreenshot();

			Reporter.addStepLog("User navigate to SVO Portal");
		}
	}

	// below code is to view list of all opportunities//
	public void ListView() throws Exception {
		ActionHandler.wait(1);
		ActionHandler.click(SVOAccountsContainer.listView);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAccountsContainer.allOpportunity);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("All Opportunities are opened");
	}
	
	public String navigateToAll(String accountType) throws Exception
	{
		ActionHandler.wait(2);
		ActionHandler.click(SVOAccountsContainer.recentlyViewedAccounts);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks on Recently Viewed Accounts");
		
		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.selectRecordType(accountType))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on All filter as per requirement");
		
		return accountType;
	}

	// user login into SVO Portal//
	@Given("^Access to SVO Portal$")
	public void access_SVO_Portal() throws Throwable {
		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");
		onStart();
		driver = getDriver();
		ActionHandler.wait(13);
		driver.get(Constants.SVOURL);
		ActionHandler.wait(15);
		Reporter.addStepLog("User Logins to SVO Portal");

		String UserName = Config.getPropertyValue("Admin_UserName");
		String Password = Config.getPropertyValue("Admin_Password");

		// user enters username//
		ActionHandler.setText(SVOAccountsContainer.userNameTextBox, UserName);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// user enters password//
		ActionHandler.setText(SVOAccountsContainer.passwordTextBox, Password);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// user clicked on login button//
		ActionHandler.click(SVOAccountsContainer.loginBtn);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User requires to enter Verification code received in an email");
		ActionHandler.wait(5);
		
		ActionHandler.wait(15);
		ActionHandler.click(SVOEnquiryContainer.verifyBtn);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();

		// below code is to enter verification code//
		if (VerifyHandler.verifyElementPresent(SVOAccountsContainer.verificationCode)) 
		{
			Reporter.addStepLog("Please Open recently received email from Salesforce");
			ActionHandler.wait(10);
			vCode = SVOenquiry.checkEmail();
			System.out.println("Verification code is = "+vCode);
			
			driver.get(Constants.SVOURL);
			ActionHandler.wait(15);
			Reporter.addStepLog("User Logins to SVO Portal");
			
			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, UserName);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, Password);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			ActionHandler.wait(7);
			CommonFunctions.attachScreenshot();
			
			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			
			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			ActionHandler.wait(7);
			CommonFunctions.attachScreenshot();
			
		} else if (VerifyHandler.verifyElementPresent(SVOAccountsContainer.mobileNumText)) {
			ActionHandler.wait(5);
			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOAccountsContainer.remindMeLater)) {
				ActionHandler.wait(3);
				ActionHandler.click(SVOAccountsContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOAccountsContainer.notRegister)) {
				ActionHandler.wait(3);
				ActionHandler.click(SVOAccountsContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOAccountsContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();
			VerifyHandler.verifyElementPresent(SVOAccountsContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}
	
	@Given("^Access to SVO Portal with username \"([^\"]*)\" and password \"([^\"]*)\"$")
	public void Access_SVO_Portal_With_userName_Password(String userName, String password) throws Exception
	{
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);
		
		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);
		
		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");
		onStart();
		driver = getDriver();
		ActionHandler.wait(13);
		driver.get(Constants.SVOURL);
		ActionHandler.wait(15);
		Reporter.addStepLog("User Logins to SVO Portal");

		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(120);

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User requires to enter Verification code received in an email");
		ActionHandler.wait(5);

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");
			ActionHandler.wait(10);
			vCode = SVOenquiry.checkEmail();
			System.out.println("Verification code is = "+vCode);
			
			driver.get(Constants.SVOURL);
			ActionHandler.wait(15);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(120);

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			ActionHandler.wait(7);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(3);
			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(2);
			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");
			
		} 
		else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) 
		{
			ActionHandler.wait(5);
			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {
				ActionHandler.wait(3);
				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {
				ActionHandler.wait(3);
				ActionHandler.click(SVOEnquiryContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} 
		else 
		{
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}
	
	@Given("^User access SVO Portal$")
	public void User_access_SVO_Portal() throws Exception
	{
		driver=getDriver();
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		
		if(!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.userNameTextBox))
		{
				ActionHandler.wait(3);
				ActionHandler.click(SVOEnquiryContainer.icon_image);
				ActionHandler.wait(10);
				CommonFunctions.attachScreenshot();
				Reporter.addStepLog("Click on Logout");
				ActionHandler.click(SVOEnquiryContainer.Logout);
				ActionHandler.wait(10);
				CommonFunctions.attachScreenshot();
				Reporter.addStepLog("User logout from SVO Portal");
		}
		
		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");
		
		Reporter.addStepLog("User Logins to SVO Portal");

		String UserName = Config.getPropertyValue("Admin_UserName");
		String Password = Config.getPropertyValue("Admin_Password");

		ActionHandler.wait(2);
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, UserName);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, Password);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();

		ActionHandler.waitForElement(SVOEnquiryContainer.SVOText, 30);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
		Reporter.addStepLog("User navigate to SVO Home Page successfully");
	}
	

	// user log out from the portal//
	@Then("^Logout from the SF SVO Portal$")
	public void logout_from_the_SVO_Portal() throws IOException {

		ActionHandler.click(SVOAccountsContainer.icon_image);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Logout");
		ActionHandler.click(SVOAccountsContainer.Logout);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		if (VerifyHandler.verifyElementPresent(SVOAccountsContainer.userNameTextBox)) {
			MarkupHelper.createLabel("Logged out successfully", ExtentColor.GREEN);
		} else {
			MarkupHelper.createLabel("Log out failed", ExtentColor.RED);
		}

		driver.close();
		Reporter.addStepLog("Driver closed");
	}
	
	@And("^User opens any individual account having name \"([^\"]*)\"$")
	public void User_Opens_any_Individual_Account_having_name(String user) throws Exception
	{
		String u[] = user.split(",");
		user = CommonFunctions.readExcelMasterData(u[0], u[1], u[2]);
		
		ActionHandler.wait(3);
		ActionHandler.click(SVOAccountsContainer.SearchAccounts);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);

		ActionHandler.setText(SVOAccountsContainer.SearchAccounts, accountName);
		ActionHandler.wait(2);
		ActionHandler.pressEnter();
		Reporter.addStepLog("User Searches for a User Account");
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		
		ActionHandler.wait(4);
		ActionHandler.click(SVOAccountsContainer.FirstAccount);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens an individual account");
		
	}
	
	@And("^User searches for a User Account \"([^\"]*)\"$")
	public void User_searches_for_user_account(String user) throws Exception
	{
		String u[] = user.split(",");
		user = CommonFunctions.readExcelMasterData(u[0], u[1], u[2]);
		
		// User Searches and seletcs an User Account
		ActionHandler.wait(3);
		ActionHandler.click(SVOAccountsContainer.SearchAccounts);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);

		ActionHandler.setText(SVOAccountsContainer.SearchAccounts, user);
		ActionHandler.wait(2);
		ActionHandler.pressEnter();
		Reporter.addStepLog("User Searches for a User Account");
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
	}

	// User Navigate to the Related contacts section and click on add Relationship//
	@Then("^Navigate to the Related contacts section and click on add Relationship$")
	public void navigate_to_the_Related_contacts_section_and_click_on_add_Relationship() throws Throwable {

		// user click on Related Contacts button//
		ActionHandler.wait(2);
		ActionHandler.click(SVOAccountsContainer.RelatedContacts);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Related Contacts button");

		// user click on add Relation button//
		ActionHandler.wait(4);
		if(!VerifyHandler.verifyElementPresent(SVOAccountsContainer.AddRelation))
		{
			ActionHandler.waitForElement(SVOAccountsContainer.AddRelation);
			Reporter.addStepLog("User waits for an element to visible");
		}
		
		ActionHandler.click(SVOAccountsContainer.AddRelation);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on add Related button");
	}

	// Enter the Relationship contact information and click on save button//
	@Then("^Enter the Relationship contact information \"([^\"]*)\" and save it$")
	public void enter_the_Relationship_contact_information_and_save_it(String Contact) throws Throwable {
		String con[] = Contact.split(",");
		Contact = CommonFunctions.readExcelMasterData(con[0], con[1], con[2]);

		// enters text in search contacts text box//
		ActionHandler.setText(SVOAccountsContainer.SearchContacts, Contact);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		ActionHandler.click(SVOAccountsContainer.searchContact);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.addedRelationship(Contact))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Input Contact information");

		// click on save button//
		ActionHandler.click(SVOAccountsContainer.SaveRelation);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on save button");
	}

	// User Navigate to Contacts tab
	@When("^User Navigate to Contacts tab$")
	public void user_Navigate_to_Contacts_tab() throws Throwable 
	{
		ActionHandler.wait(3);
		if(!VerifyHandler.verifyElementPresent(SVOAccountsContainer.Contacts))
		{
			ActionHandler.wait(2);
			navigateToSVO();
		}
		
		ActionHandler.wait(5);
		Reporter.addStepLog("User Navigate to Contacts tab");
		javaScriptUtil.clickElementByJS(SVOAccountsContainer.Contacts);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
	}

	// verify the contact name and details//
	@Then("^Verify the contact name \"([^\"]*)\" and details$")
	public void verify_the_contact_name_and_details(String Contact) throws Throwable 
	{
		String con[] = Contact.split(",");
		Contact = CommonFunctions.readExcelMasterData(con[0], con[1], con[2]);

		ActionHandler.wait(5);
		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.addedRelationship(Contact))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verify the added Relationship by navigating to Contact");
	}

	// Navigate to the Vehicles Owned section and click on new//
	@Then("^Navigate to the Vehicles Owned section and click on new$")
	public void navigate_to_the_Vehicles_Owned_section_and_click_on_new() throws Throwable {

		// Click on Vehicles Owned button//
		ActionHandler.wait(3);
		ActionHandler.click(SVOAccountsContainer.VehiclesOwned);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Vehicles Owned button");

		// click on new button
		ActionHandler.wait(2);
		ActionHandler.click(SVOAccountsContainer.NewVehiclesOwned);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on new button");
	}

	@Then("^select the ownership as driver and enter the Contact \"([^\"]*)\" Brand \"([^\"]*)\" and Model \"([^\"]*)\" information and save it$")
	public void select_the_ownership_as_driver_and_enter_the_Contact_Brand_and_Model_information_and_save_it(
			String Contact, String Brand, String Model) throws Throwable 
	{

		// data read from master excel sheet //
		String con[] = Contact.split(",");
		Contact = CommonFunctions.readExcelMasterData(con[0], con[1], con[2]);

		String Br[] = Brand.split(",");
		Brand = CommonFunctions.readExcelMasterData(Br[0], Br[1], Br[2]);

		String Mod[] = Model.split(",");
		Model = CommonFunctions.readExcelMasterData(Mod[0], Mod[1], Mod[2]);

		System.out.println("All the data is loaded");

		// Click on driver type
		ActionHandler.wait(2);
		ActionHandler.click(SVOAccountsContainer.Driveradd);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on driver type");

		// Click on next button
		ActionHandler.wait(2);
		ActionHandler.click(SVOAccountsContainer.DriverNext);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on next button");

		// Input Retailer information
		ActionHandler.wait(2);
		ActionHandler.setText(SVOAccountsContainer.ContactSearch, Contact);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		ActionHandler.click(SVOAccountsContainer.showAllResultForContact);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.brandSearch(Contact))));
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User adds contact");

		// user entered brand details//
		ActionHandler.wait(2);
		ActionHandler.setText(SVOAccountsContainer.brand, Brand);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		ActionHandler.click(SVOAccountsContainer.showAllResultForBrand);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.brandSearch(Brand))));
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();		
		Reporter.addStepLog("Input Brand information");

		// Input Model information
		ActionHandler.wait(2);
		ActionHandler.setText(SVOAccountsContainer.model, Model);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		ActionHandler.click(SVOAccountsContainer.showAllResultForModel);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.brandSearch(Model))));
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();	
		Reporter.addStepLog("Input Model information");

		// Click on Save button
		ActionHandler.click(SVOAccountsContainer.SaveVehicle);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Save button");
	}

	// Verify the added new vehicle//
	@Then("^Verify the added new vehicle$")
	public void verify_the_added_new_vehicle() throws Throwable {

		// Verify that the vehicle is added
		ActionHandler.wait(4);
		VerifyHandler.verifyElementPresent(SVOAccountsContainer.Vehicleaddverify);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verify that the vehicle is added");

		// Click on Save button
		ActionHandler.wait(2);
		ActionHandler.click(SVOAccountsContainer.OwnedVehicleID);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Save button");

		// Verify that the vehicle information is correct
		ActionHandler.wait(3);
		VerifyHandler.verifyElementPresent(SVOAccountsContainer.Driveradd);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verify that the vehicle information is correct");
	}

	// User Searches for account and selects it
	@Then("^User Searches for account \"([^\"]*)\" and selects it$")
	public void user_Searches_for_account_and_selects_it(String account) throws Throwable {
		// data taken from master data sheet//
		String acc[] = account.split(",");
		account = CommonFunctions.readExcelMasterData(acc[0], acc[1], acc[2]);

		// Search for an account
		ActionHandler.click(SVOAccountsContainer.SearchBox);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		ActionHandler.clearAndSetText(SVOAccountsContainer.SearchBox, account);
		CommonFunctions.attachScreenshot();
		ActionHandler.pressEnter();
		ActionHandler.wait(10);
		Reporter.addStepLog("Search for an account");
		CommonFunctions.attachScreenshot();

		// Select account//
		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.addedRelationship(account))));
		ActionHandler.wait(10);
		Reporter.addStepLog("Select account");
		CommonFunctions.attachScreenshot();
	}

	// select the ownership as Owner and enter the Brand, Model, Start date and End
	// date information
	@Then("^select the ownership as Owner and enter the Brand \"([^\"]*)\" Model \"([^\"]*)\" Start date \"([^\"]*)\" and End date \"([^\"]*)\" information$")
	public void select_the_ownership_as_Owner_and_enter_the_Brand_Model_Start_date_and_End_date_information(
			String Brand, String Model, String StartDate, String EndDate) throws Throwable 
	{
		// data retrieved from master data sheet//
		String SD[] = StartDate.split(",");
		StartDate = CommonFunctions.readExcelMasterData(SD[0], SD[1], SD[2]);

		String ED[] = EndDate.split(",");
		EndDate = CommonFunctions.readExcelMasterData(ED[0], ED[1], ED[2]);

		String Br[] = Brand.split(",");
		Brand = CommonFunctions.readExcelMasterData(Br[0], Br[1], Br[2]);

		String Mod[] = Model.split(",");
		Model = CommonFunctions.readExcelMasterData(Mod[0], Mod[1], Mod[2]);

		System.out.println("All the data is loaded");

		// Click on driver type
		ActionHandler.click(SVOAccountsContainer.Owneradd);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on driver type");

		// Click on next button
		ActionHandler.click(SVOAccountsContainer.DriverNext);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on next button");

		// user entered brand details//
		ActionHandler.wait(3);
		ActionHandler.setText(SVOAccountsContainer.brand, Brand);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		ActionHandler.click(SVOAccountsContainer.showAllResultForContact);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.brandSearch(Brand))));
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();		
		Reporter.addStepLog("Input Brand information");

		// Input Model information
		ActionHandler.wait(3);
		ActionHandler.setText(SVOAccountsContainer.model, Model);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		ActionHandler.click(SVOAccountsContainer.showAllResultForBrand);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.brandSearch(Model))));
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();	
		Reporter.addStepLog("Input Model information");
	}

	// click on save and new vehicle button
	@Then("^click on save and new vehicle button$")
	public void click_on_save_and_new_vehicle_button() throws Throwable {
		ActionHandler.wait(5);
		ActionHandler.click(SVOAccountsContainer.SaveandNew);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Save and New button");
	}

	// click on save vehicle button
	@Then("^click on save vehicle button$")
	public void click_on_save_vehicle_button() throws Throwable {
		ActionHandler.wait(5);
		ActionHandler.click(SVOAccountsContainer.SaveVehicle);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Save button");
	}

	// Verify and delete the added vehicle
	@Then("^Verify and delete the added vehicle$")
	public void verify_and_delete_the_added_vehicle() throws Throwable {

		// Verify that the vehicle is added
		ActionHandler.wait(2);
		VerifyHandler.verifyElementPresent(SVOAccountsContainer.Vehicleaddverify);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verify that the vehicle is added");

		// Click on Show Action button
		ActionHandler.wait(3);
		ActionHandler.click(SVOAccountsContainer.firstVehicleOwnership);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens first Vehicle Ownership");

		// Click on Delete button
		ActionHandler.click(SVOAccountsContainer.DeleteVehicle);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Delete button");

		// Click on Delete button
		ActionHandler.click(SVOAccountsContainer.ConfDeleteVehicle);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Delete button");
	}
	

	@And("^User tries to delete created Corporate Account$")
	public void User_tries_delete_creates_Corporate_Account() throws Exception
	{
		ActionHandler.wait(2);
		ActionHandler.click(SVOAccountsContainer.sideMenu);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		
		ActionHandler.click(SVOAccountsContainer.DeleteAccount);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(SVOAccountsContainer.ConfDeleteVehicle);
		ActionHandler.wait(3);
		Reporter.addStepLog("User delets created corporate account");
	}

	// Create a new Individual account
	@Then("^Create a new Individual account$")
	public void create_a_new_Individual_account() throws Throwable {
		String recordType = "Individual";
		String Salutation = "Ms.";
		String First = "Test";
		String Last = "Automation";
		String Region = "Europe";
		String SalesArea = "Euro Importer";
		String Market = "Balkans";
		String contact = Constants.Contact_one;
		String details = "Email";
		String Email = Constants.email;
		String Address = "Manchester";
		String Background = "Test client background";
		String Interest = "Test interest";
		String Business = "Test business interest";
		String Line1 = "Test address line";
		String State = "State";
		String Country = "Country";

		// User Creates new Individual account
		ActionHandler.wait(3);
		Reporter.addStepLog("User Creates new Individual account");
		ActionHandler.click(SVOAccountsContainer.NewAccount);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		// User Needs to select any record type option
		Reporter.addStepLog("User Needs to select any record type option");
		ActionHandler.wait(3);
		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.selectRecordType(recordType))));
		Reporter.addStepLog("User Select required Record Type");
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// User Navigate to enter account details page
		ActionHandler.wait(4);
		ActionHandler.click(SVOAccountsContainer.nextRType);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigate to enter account details page");

		// User selects salutation
		ActionHandler.wait(7);
		ActionHandler.click(SVOAccountsContainer.salutation);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.ValueSelection(Salutation))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects salutation");

		// Input product offering information
		ActionHandler.wait(1);
		ActionHandler.setText(SVOAccountsContainer.FirstName, First);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		ActionHandler.setText(SVOAccountsContainer.LastName, Last);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Input product offering information");

		// Enter Region information
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAccountsContainer.newRegion);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.ValueSelection(Region))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter Region information");

		// Enter Sales Area information
		ActionHandler.scrollToView(SVOAccountsContainer.newSalesArea);
		ActionHandler.wait(3);
		ActionHandler.click(SVOAccountsContainer.newSalesArea);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.ValueSelection(SalesArea))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter Sales Area information");

		// Enter market information
		ActionHandler.click(SVOAccountsContainer.newMarket);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.ValueSelection(Market))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter market information");

		// Input Preferred Retailer information
		ActionHandler.setText(SVOAccountsContainer.newPreferredRet, contact);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAccountsContainer.newPreferredRetSearch);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.clientNameSearch(contact))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Input Preferred Retailer information");

		// Enter contact details information
		ActionHandler.click(SVOAccountsContainer.ContactDetails);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.ValueSelection(details))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		ActionHandler.setText(SVOAccountsContainer.newEmail, Email);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter contact details information");

		// Enter Billing address information
		ActionHandler.scrollToView(SVOAccountsContainer.BillingAddress);
		ActionHandler.wait(3);
		ActionHandler.setText(SVOAccountsContainer.BillingAddress, Address);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAccountsContainer.BillingAddressSearch);
		ActionHandler.wait(3);
		Reporter.addStepLog("Enter Billing address information");
		CommonFunctions.attachScreenshot();

		// Enter Interests information
		ActionHandler.wait(1);
		ActionHandler.setText(SVOAccountsContainer.ClientBkgd, Background);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.scrollToView(SVOAccountsContainer.Interests);
		ActionHandler.wait(3);
		ActionHandler.setText(SVOAccountsContainer.Interests, Interest);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);

		ActionHandler.setText(SVOAccountsContainer.BusinessInt, Business);
		ActionHandler.wait(3);
		Reporter.addStepLog("Enter Interests information");
		CommonFunctions.attachScreenshot();

		// Input Blackbook details
		ActionHandler.wait(3);
		ActionHandler.scrollToView(SVOAccountsContainer.AddLine);
		ActionHandler.wait(3);
		ActionHandler.setText(SVOAccountsContainer.AddLine, Line1);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		ActionHandler.scrollToView(SVOAccountsContainer.AddState);
		ActionHandler.wait(3);
		ActionHandler.setText(SVOAccountsContainer.AddState, State);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		ActionHandler.setText(SVOAccountsContainer.AddCountry, Country);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Input Blackbook details");

		// User saves Individual Account
		ActionHandler.click(SVOAccountsContainer.saveAccount);
		ActionHandler.wait(9);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves Individual Account");
	}

	// Create new Individual account with Record type, First name, Last name, Region
	// and Email
	@Then("^Create new Individual account with Record type \"([^\"]*)\" First name \"([^\"]*)\" Last name \"([^\"]*)\" Region \"([^\"]*)\" Email \"([^\"]*)\"$")
	public void create_new_Individual_account_with_record_type_first_name_last_name_region_email(String recordType,
			String first, String last, String region, String email) throws Throwable 
	{
		// data read from master data sheet
		String rType[] = recordType.split(",");
		recordType = CommonFunctions.readExcelMasterData(rType[0], rType[1], rType[2]);

		String FirstName[] = first.split(",");
		first = CommonFunctions.readExcelMasterData(FirstName[0], FirstName[1], FirstName[2]);

		String LastName[] = last.split(",");
		last = CommonFunctions.readExcelMasterData(LastName[0], LastName[1], LastName[2]);

		String AEmail[] = email.split(",");
		email = CommonFunctions.readExcelMasterData(AEmail[0], AEmail[1], AEmail[2]);

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		double randomNumber = getRandomIntegerBetweenRange(0, 100000);

		// click on New button to create new individual account
		ActionHandler.wait(3);
		Reporter.addStepLog("User Creates new Individual account");
		ActionHandler.click(SVOAccountsContainer.NewAccount);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// User Needs to select any record type option
		Reporter.addStepLog("User Needs to select any record type option");
		ActionHandler.pageDown();
		ActionHandler.wait(3);

		// User Select required Record Type
		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.selectRecordType(recordType))));
		Reporter.addStepLog("User Select required Record Type");
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// click on next button and navigate to enter account details page
		ActionHandler.wait(4);
		ActionHandler.click(SVOAccountsContainer.nextRType);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigate to enter account details page");

		ActionHandler.waitForElement(SVOAccountsContainer.viewAllDependencies,20);
		ActionHandler.wait(2);
		ActionHandler.click(SVOAccountsContainer.viewAllDependencies);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		ActionHandler.click(SVOAccountsContainer.Opportunityeditregion);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.LocationSelection(region))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		
		ActionHandler.wait(3);
		ActionHandler.click(SVOAccountsContainer.applyBtn);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter Region information");
		
		ActionHandler.wait(3);
		ActionHandler.setText(SVOAccountsContainer.FirstName, first);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		ActionHandler.setText(SVOAccountsContainer.LastName, last + randomNumber);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Input product offering information");

		// Enter contact details information
		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		ActionHandler.setText(SVOAccountsContainer.newEmail, email);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter contact details information");

		// User saves Individual Account
		ActionHandler.wait(2);
		ActionHandler.click(SVOAccountsContainer.saveAccount);
		ActionHandler.wait(13);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves Individual Account");
		
		ActionHandler.wait(3);
		accountName = SVOAccountsContainer.IndividualAccountName.getText();
		System.out.println("Created Account Name is = "+accountName);
		
	}

	// Navigate to the SAP Accounts section and click on New
	@Then("^Navigate to the SAP Accounts section and click on New$")
	public void navigate_to_the_SAP_Accounts_section_and_click_on_New() throws Throwable {

		// Click on SAP Accounts button
		ActionHandler.wait(5);
		ActionHandler.click(SVOAccountsContainer.SAPAccounts);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on SAP Accounts button");

		// Click on new button
		ActionHandler.click(SVOAccountsContainer.NewSAP);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on new button");
	}

	// Enter details in new SAP Account and Save it
	@Then("^Enter details in new SAP Account and Save it$")
	public void enter_details_in_new_SAP_Account_and_Save_it() throws Throwable 
	{
		String SAP = "Test123";
		String Status = "Set-up";

		double randomNumber = getRandomIntegerBetweenRange(0, 1000);

		// Enter SAP Account details
		ActionHandler.wait(5);
		ActionHandler.setText(SVOAccountsContainer.SAPID, SAP + randomNumber);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAccountsContainer.SAPStatus);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.ValueSelection(Status))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter SAP Account details");

		// User saves SAP Account
		ActionHandler.click(SVOAccountsContainer.saveSAP);
		ActionHandler.wait(9);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves SAP Account");
	}

	// Navigate to the Vehicles Driven section and click on New owner record type
	@Then("^Navigate to the Vehicles Driven section and click on New owner record type$")
	public void navigate_to_the_Vehicles_Driven_section_and_click_on_New_owner_record_type() throws Throwable {

		// Click on Vehicles driven button
		ActionHandler.wait(5);
		ActionHandler.click(SVOAccountsContainer.VehiclesDriven);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Vehicles driven button");

		// Click on New button
		ActionHandler.wait(3);
		ActionHandler.click(SVOAccountsContainer.NewVehiclesDriven);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on New button");

		// click on Next Record type button//
		ActionHandler.wait(2);
		ActionHandler.click(SVOAccountsContainer.nextRType);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
	}

	// Enter details in new Vehicles driven and Save it
	@Then("^Enter details in new Vehicles driven and Save it$")
	public void enter_details_in_new_Vehicles_driven_and_Save_it() throws Throwable 
	{
		String Account = "Test Automation";
		String Manufacturer = "Land Rover";
		String Model = "Discovery";

		// enters account name
		ActionHandler.wait(5);
		ActionHandler.setText(SVOAccountsContainer.AccName, Account);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// click on account name search icon and selects account
		ActionHandler.wait(5);
		ActionHandler.click(SVOAccountsContainer.AccNameSearch);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.brandSearch(Account))));
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		// Enter Manufacturer information
		ActionHandler.setText(SVOAccountsContainer.newManufacturer, Manufacturer);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAccountsContainer.newManufacturerSearch);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.brandSearch(Manufacturer))));
		ActionHandler.wait(10);
		Reporter.addStepLog("Enter Manufacturer information");
		CommonFunctions.attachScreenshot();

		// Enter model information
		ActionHandler.setText(SVOAccountsContainer.newModel, Model);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAccountsContainer.newModelSearch);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.brandSearch(Model))));
		ActionHandler.wait(10);
		Reporter.addStepLog("Enter model information");
		CommonFunctions.attachScreenshot();

		// User saves SAP Account
		ActionHandler.click(SVOAccountsContainer.SaveVehicle);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves SAP Account");
	}

	// Enter details and past end date in new Vehicles driven and Save it to verify
	// the error
	@Then("^Enter details and past end date in new Vehicles driven and Save it to verify the error$")
	public void enter_details_and_past_end_date_in_new_Vehicles_driven_and_Save_it_to_verify_the_error()
			throws Throwable {
		String Account = "Test Automation";
		String Manufacturer = "Land Rover";
		String Model = "Discovery";
		String End = "28/01/2020";

		// enters account information//
		ActionHandler.wait(5);
		ActionHandler.setText(SVOAccountsContainer.AccName, Account);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAccountsContainer.AccNameSearch);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.brandSearch(Account))));
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		// Enter Manufacturer information
		ActionHandler.setText(SVOAccountsContainer.newManufacturer, Manufacturer);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAccountsContainer.newManufacturerSearch);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.brandSearch(Manufacturer))));
		ActionHandler.wait(4);
		Reporter.addStepLog("Enter Manufacturer information");
		CommonFunctions.attachScreenshot();

		// Enter model information
		ActionHandler.setText(SVOAccountsContainer.newModel, Model);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAccountsContainer.newModelSearch);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.brandSearch(Model))));
		ActionHandler.wait(4);
		Reporter.addStepLog("Enter model information");
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.setText(SVOAccountsContainer.EndDate, End);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		// Error 'Start Date should be prior to End Date' is displayed and vehicle is
		// not saved
		ActionHandler.click(SVOAccountsContainer.SaveVehicle);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Error 'Start Date should be prior to End Date' is displayed and vehicle is not saved");
		ActionHandler.click(SVOAccountsContainer.cancelVehicle);
		ActionHandler.wait(6);
		CommonFunctions.attachScreenshot();
	}

	// User is able to view list of Enquiries, Opportunities and SVO Client
	// Opportunities
	@And("^User is able to view list of Enquiries, Opportunities and SVO Client Opportunities$")
	public void User_View_List_Enquiries_Opportunities_SVO_Client_Opportunities() throws Exception {

		// click on show all button
		ActionHandler.wait(3);
		/*ActionHandler.pageDown();
		ActionHandler.wait(5);
		ActionHandler.pageUp();
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();*/
		
		ActionHandler.wait(10);
		/*javaScriptUtil.clickElementByJS(SVOAccountsContainer.showAllButton);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();*/

		// User can view list of enquiries
		ActionHandler.wait(2);
		ActionHandler.click(SVOAccountsContainer.EnquiriesLink);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User can view list of enquiries");

		driver.navigate().back();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// User can view list of Opportunities
		ActionHandler.wait(2);
		ActionHandler.click(SVOAccountsContainer.OpportunitiesLink);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User can view list of Opportunities");

		driver.navigate().back();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// Üser can view list of SVO Client Opportunities
		ActionHandler.wait(2);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);
		ActionHandler.click(SVOAccountsContainer.SVOClientOpportunitiesLink);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Üser can view list of SVO Client Opportunities");

		driver.navigate().back();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User navigates back to Accounts page");
		AccountsHomePage();
	}

	// Navigate to the KMI section and click on new
	@Then("^Navigate to the KMI section and click on new$")
	public void navigate_to_the_KMI_section_and_click_on_new() throws Throwable 
	{
		//ActionHandler.wait(2);
		/*ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();*/
		
		/*ActionHandler.wait(3);
		ActionHandler.pageDown();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		
		// Click on Show All button
		ActionHandler.wait(4);
		javaScriptUtil.clickElementByJS(SVOAccountsContainer.showAllButton);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Show All button");*/

		// Click on KMI button
		ActionHandler.wait(2);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.pageDown();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		
		ActionHandler.wait(3);
		ActionHandler.click(SVOAccountsContainer.KMI);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on KMI button");

		// Click on New button
		ActionHandler.wait(2);
		ActionHandler.click(SVOAccountsContainer.NewKMI);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on New button");
	}

	// Enter all the mandatory information like Contact, Product Offering, Brand and
	// Model information and save it
	@Then("^Enter all the mandatory information like Contact \"([^\"]*)\" Product Offering \"([^\"]*)\" Brand \"([^\"]*)\" and Model \"([^\"]*)\" information and save it$")
	public void enter_all_the_mandatory_information_like_Contact_Product_Offering_Brand_and_Model_information_and_save_it(
			String Contact, String ProductOff, String Brand, String Model) throws Throwable 
	{
		// data taken from master data sheet
		String Con[] = Contact.split(",");
		Contact = CommonFunctions.readExcelMasterData(Con[0], Con[1], Con[2]);

		String Proff[] = ProductOff.split(",");
		ProductOff = CommonFunctions.readExcelMasterData(Proff[0], Proff[1], Proff[2]);

		String Br[] = Brand.split(",");
		Brand = CommonFunctions.readExcelMasterData(Br[0], Br[1], Br[2]);

		String Mod[] = Model.split(",");
		Model = CommonFunctions.readExcelMasterData(Mod[0], Mod[1], Mod[2]);

		System.out.println("All the data is loaded");

		// click on Clear Selection button
		/*ActionHandler.wait(2);
		ActionHandler.click(SVOAccountsContainer.ClearSelection);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Clear Selection button");

		// Input Retailer information
		ActionHandler.wait(2);
		ActionHandler.setText(SVOAccountsContainer.ContactSearch, Contact);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		ActionHandler.click(SVOAccountsContainer.showAllResultForContact);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.brandSearch(Contact))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();*/
		
		Reporter.addStepLog("Input Retailer information");

		// selects product offering
		ActionHandler.wait(2);
		ActionHandler.click(SVOAccountsContainer.ProductOff);
		ActionHandler.wait(5);
		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.SelectValue(ProductOff))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("selects product offering");

		// Input Retailer information
		ActionHandler.wait(4);
		ActionHandler.setText(SVOAccountsContainer.brand, Brand);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAccountsContainer.showAllResultForContact);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.brandSearch(Brand))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Input Retailer information");

		// Input Retailer information
		ActionHandler.wait(2);
		ActionHandler.setText(SVOAccountsContainer.model, Model);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAccountsContainer.showAllResultForBrand);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.brandSearch(Model))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Input Retailer information");

		// Click on Save button
		ActionHandler.wait(2);
		ActionHandler.click(SVOAccountsContainer.SaveVehicle);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Save button");
	}

	// Verify the added new KMI record
	@Then("^Verify the added new KMI record$")
	public void verify_the_added_new_KMI_record() throws Throwable 
	{
		ActionHandler.wait(4);
		VerifyHandler.verifyElementPresent(SVOAccountsContainer.verifyKMI);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verify that the KMI is added");
		
		ActionHandler.wait(3);
		ActionHandler.click(SVOAccountsContainer.verifyKMI);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("KMI Details are open");
		
		ActionHandler.wait(2);
		ActionHandler.click(SVOAccountsContainer.KMIContact);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on contact");
	}

	// Verify the no new KMI record is added
	@Then("^Verify the no new KMI record is added$")
	public void verify_the_no_new_KMI_record_is_added() throws Throwable 
	{
		//VerifyHandler.verifyElementPresent(SVOAccountsContainer.NoItemstoDisplay);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verify that no new KMI record is added");
	}

	// Navigate to the Notes section and click on New
	@Then("^Navigate to the Notes section and click on New$")
	public void navigate_to_the_Notes_section_and_click_on_New() throws Throwable 
	{
		ActionHandler.wait(2);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(4);
		ActionHandler.pageDown();
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		
		// Click on Notes button
		ActionHandler.wait(5);
		/*javaScriptUtil.clickElementByJS(SVOAccountsContainer.showAll);
		ActionHandler.wait(5);
		ActionHandler.click(SVOAccountsContainer.Notes);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Notes button");*/

		// Click on New button
		ActionHandler.wait(2);
		ActionHandler.click(SVOAccountsContainer.NewNotes);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on New button");
	}

	// Create a new Note and Save it
	@Then("^Create a new Note and Save it$")
	public void create_a_new_Note_and_Save_it() throws Throwable 
	{
		String Title = "Note title";
		String Desc = "Note description testing 123";

		// Enter details in notes
		ActionHandler.wait(5);
		ActionHandler.setText(SVOAccountsContainer.NoteTitle, Title);
		ActionHandler.wait(2);
		ActionHandler.click(SVOAccountsContainer.NoteDesc);
		ActionHandler.wait(3);
		ActionHandler.setText(SVOAccountsContainer.NoteDesc, Desc);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter details in notes");

		// save the note
		ActionHandler.wait(2);
		ActionHandler.click(SVOAccountsContainer.Done);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Note saved");
	}

	// Navigate to the Person Account History section and views Person Account
	// History for an individual account
	@Then("^Navigate to the Person Account History section and views Person Account History for an individual account$")
	public void navigate_to_the_Person_Account_History_section_and_views_Person_Account_History_for_an_individual_account()
			throws Throwable {

		// User views the Person account history
		ActionHandler.wait(3);
		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		//javaScriptUtil.clickElementByJS(SVOAccountsContainer.showAll);
		ActionHandler.wait(5);
		ActionHandler.click(SVOAccountsContainer.PerAccHistory);
		ActionHandler.wait(6);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User views the Person account history");
	}

	// Click on Update Consent
	@Then("^Click on Update Consent$")
	public void click_on_Update_Consent() throws Throwable 
	{
		ActionHandler.wait(5);
		ActionHandler.click(SVOAccountsContainer.UpdateConsent);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Update consent");
	}

	// Update the consent as required and Save
	@Then("^Update the consent as required and Save$")
	public void update_the_consent_as_required_and_Save() throws Throwable 
	{
		// Update the consent as required
		ActionHandler.wait(5);
		ActionHandler.click(SVOAccountsContainer.Consent);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(4);
		ActionHandler.click(SVOAccountsContainer.ConsentYes);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		VerifyHandler.verifyElementPresent(SVOAccountsContainer.Topixtable);
		ActionHandler.wait(5);

		// click on next button
		ActionHandler.wait(2);
		ActionHandler.click(SVOAccountsContainer.ConsentNext);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(4);

		// click on consent finish
		ActionHandler.wait(3);
		ActionHandler.click(SVOAccountsContainer.ConsentFinish);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(4);
		Reporter.addStepLog("User Updates consent and saves the changes");
	}

	// Click on Sharing from the dropdown
	@Then("^Click on Sharing from the dropdown$")
	public void click_on_Sharing_from_the_dropdown() throws Throwable {

		// click on drop down button
		ActionHandler.wait(10);
		ActionHandler.click(SVOAccountsContainer.DropdownBtn);
		CommonFunctions.attachScreenshot();

		// click on sharing button
		ActionHandler.wait(10);
		ActionHandler.click(SVOAccountsContainer.SharingBtn);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(10);
		Reporter.addStepLog("User clicks on Sharing");
	}

	// Update the access to any user and save it
	@Then("^Update the access to any user and save it$")
	public void update_the_access_to_any_user_and_save_it() throws Throwable {
		String User = Constants.User1;

		// search for an user
		ActionHandler.wait(10);
		ActionHandler.setText(SVOAccountsContainer.SearchUser, User);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(10);

		// select the user
		ActionHandler.click(SVOAccountsContainer.SelectUser);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(10);
		ActionHandler.click(SVOAccountsContainer.AccAccess);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(10);
		ActionHandler.click(SVOAccountsContainer.AccRWAccess);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(10);

		ActionHandler.click(SVOAccountsContainer.ContactAccess);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(10);

		ActionHandler.click(SVOAccountsContainer.ContactROAccess);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(10);

		// click on save button
		ActionHandler.click(SVOAccountsContainer.SaveShare);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(10);
	}

	// User Searches for invalid account and verifies the error
	@Then("^User Searches for invalid account \"([^\"]*)\" and verifies the error$")
	public void user_Searches_for_invalid_account_and_verifies_the_error(String account) throws Throwable {

		// data taken from master data sheet
		String acc[] = account.split(",");
		account = CommonFunctions.readExcelMasterData(acc[0], acc[1], acc[2]);

		// click on search box
		ActionHandler.click(SVOAccountsContainer.SearchBox);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);

		// set text in search box
		ActionHandler.clearAndSetText(SVOAccountsContainer.SearchBox, account);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(10);
		ActionHandler.pressEnter();
		ActionHandler.wait(10);
		Reporter.addStepLog("Search for an account");
		CommonFunctions.attachScreenshot();

		// displayed error message caught
		String Error = SVOAccountsContainer.AccError.getText();
		Reporter.addStepLog("Account search error message is = " + Error);
	}

	// Click on Edit
	@Then("^Click on Edit$")
	public void click_on_Edit() throws Throwable {
		ActionHandler.wait(10);
		ActionHandler.click(SVOAccountsContainer.EditBtn);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(10);
	}

	// Update the required details of the account
	@Then("^Update the required details of the account$")
	public void update_the_required_details_of_the_account() throws Throwable {
		String Region = "Europe";
		String SalesArea = "Euro Importer";
		String Market = "Balkans";

		// Enter Region information
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAccountsContainer.newRegion);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.ValueSelection(Region))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter Region information");

		// Enter Sales Area information
		ActionHandler.scrollToView(SVOAccountsContainer.newSalesArea);
		ActionHandler.wait(5);
		ActionHandler.click(SVOAccountsContainer.newSalesArea);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.ValueSelection(SalesArea))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter Sales Area information");

		// Enter market information
		ActionHandler.click(SVOAccountsContainer.newMarket);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.ValueSelection(Market))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter market information");

		// User saves the updates
		ActionHandler.wait(7);
		ActionHandler.click(SVOAccountsContainer.saveAccount);
		ActionHandler.wait(15);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User saves the updates");
	}

	// User Navigate to Accounts
	@When("^User Navigate to Accounts$")
	public void user_Navigate_to_Accounts() throws Throwable
	{
		ActionHandler.wait(5);
		Reporter.addStepLog("User Navigate to Accounts tab");
		ActionHandler.click(SVOAccountsContainer.AccountsTab);
		ActionHandler.wait(15);
		CommonFunctions.attachScreenshot();

	}

	// User searches for a User Account
	@When("^User searches for a User Account \"([^\"]*)\" with AccountType \"([^\"]*)\"$")
	public void user_searches_for_a_User_Account(String User, String accountType) throws Throwable 
	{
		
		String c[] = User.split(",");
		User = CommonFunctions.readExcelMasterData(c[0], c[1], c[2]);
		
		String accType[] = accountType.split(",");
		accountType = CommonFunctions.readExcelMasterData(accType[0], accType[1], accType[2]);
		
		ActionHandler.wait(3);
		navigateToAll(accountType);
		Reporter.addStepLog("User navigates to all accounts");
		
		// User Searches and seletcs an User Account
		ActionHandler.wait(3);
		ActionHandler.click(SVOAccountsContainer.SearchAccounts);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);

		ActionHandler.setText(SVOAccountsContainer.SearchAccounts, User);
		ActionHandler.wait(2);
		ActionHandler.pressEnter();
		Reporter.addStepLog("User Searches for a User Account");
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		
		ActionHandler.click(SVOAccountsContainer.FirstAccount);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		
	}
	
	@Then("^User navigate to Opportunity Section$")
	public void User_Navigate_Opportunity_Section() throws Exception
	{
		ActionHandler.wait(2);
		ActionHandler.click(SVOAccountsContainer.OpportunitiesLink);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User can view list of Opportunities");
	}

	@And("^User creates a new Opportunity with Record Type \"([^\"]*)\" for Corporate Account$")
	public void User_Creates_New_Opportunity_Record_Type(String recordType) throws Exception
	{
		String rType[] = recordType.split(",");
		recordType = CommonFunctions.readExcelMasterData(rType[0], rType[1], rType[2]);
		
		ActionHandler.wait(2);
		ActionHandler.click(SVOAccountsContainer.NewSVOOpp);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on New button");

		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.selectRecordType(recordType))));
		Reporter.addStepLog("User Select required Record Type");
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAccountsContainer.nextRType);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
	}
	
	// User Navigates to the User Account
	@Then("^User Navigates to the User Account \"([^\"]*)\"$")
	public void user_Navigates_to_the_User_Account(String User) throws Throwable {
		String c[] = User.split(",");
		User = CommonFunctions.readExcelMasterData(c[0], c[1], c[2]);

		// User Account is selected
		ActionHandler.wait(2);
		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.selectUserAccount(User))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Account is selected");
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
	}

	// Navigates to Users SAP Account
	@When("^Navigates to Users SAP Account$")
	public void navigates_To_Users_SAP_Account() throws Throwable {

		// User clicks on SAP Account Link
		//ActionHandler.pageScrollDown();
		ActionHandler.wait(5);
		ActionHandler.click(SVOAccountsContainer.SAPAccounts);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on SAP Account Link");
		ActionHandler.wait(3);
	}

	// User creates an SAP Account with SAP ID
	@Then("^User creates an SAP Account with SAP ID \"([^\"]*)\"$")
	public void user_creates_an_SAP_Account_with_SAP_ID(String ID) throws Throwable {

		String c[] = ID.split(",");
		ID = CommonFunctions.readExcelMasterData(c[0], c[1], c[2]);

		double randomNumber = getRandomIntegerBetweenRange(0, 1000);
		
		// User clicks on New Button
		ActionHandler.wait(2);
		ActionHandler.click(SVOAccountsContainer.NewSAP);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on New Button");
		ActionHandler.wait(3);

		// User enters SAP ID
		ActionHandler.setText(SVOAccountsContainer.SAPID, ID+randomNumber);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters SAP ID");
		ActionHandler.wait(3);

	}

	// Enter all mandatory fields for SAP Account like Status and Location
	@Then("^Enter all mandatory fields for SAP Account like Status \"([^\"]*)\" and Location \"([^\"]*)\"$")
	public void enter_all_mandatory_fields_for_SAP_Account_like_Status_and_Location(String status, String location)
			throws Throwable {

		String c[] = status.split(",");
		status = CommonFunctions.readExcelMasterData(c[0], c[1], c[2]);

		String d[] = location.split(",");
		location = CommonFunctions.readExcelMasterData(d[0], d[1], d[2]);

		// Status is selected
		ActionHandler.wait(3);
		ActionHandler.click(SVOAccountsContainer.newStatus);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.StatusSelection(status))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Status is selected");
		ActionHandler.wait(3);

		// Location is selected
		ActionHandler.click(SVOAccountsContainer.newLocation);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.LocationSelection(location))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Location is selected");

	}

	// Click on Save and New
	@Then("^Click on Save and New$")
	public void click_on_Save_and_New() throws Throwable {

		// Save and New button is clicked
		ActionHandler.wait(2);
		ActionHandler.click(SVOAccountsContainer.SaveandNew);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Save and New button is clicked");
	}

	// Save the details
	@When("^Save the details$")
	public void save_the_details() throws Throwable {

		// User saves the details
		ActionHandler.wait(3);
		ActionHandler.click(SVOAccountsContainer.saveAccount);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves the details");

	}

	// Filter on SAP Accounts using SAP ID
	@Then("^Filter on SAP Accounts using SAP	ID \"([^\"]*)\"$")
	public void filter_on_SAP_Accounts_using_SAP_ID(String ID) throws Throwable {

		String c[] = ID.split(",");
		ID = CommonFunctions.readExcelMasterData(c[0], c[1], c[2]);

		// User click on Filter Icon
		ActionHandler.wait(2);
		ActionHandler.click(SVOAccountsContainer.FilterAccounts);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Filter Icon");

		// User enters SAP ID
		ActionHandler.wait(2);
		ActionHandler.clearAndSetText(SVOAccountsContainer.SAPid, ID);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters SAP ID");

		// User clicks on Apply Button
		ActionHandler.click(SVOAccountsContainer.Apply);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Apply Button");

		// User clicks on Close Filter Button
		ActionHandler.click(SVOAccountsContainer.CloseFilter);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Close Filter Button");

	}

	// Delete the Selected SAP Account
	@Then("^Delete the Selected SAP Account$")
	public void delete_the_Selected_SAP_Account() throws Throwable {

		// User clicks on the newly created SAP Account
		ActionHandler.wait(2);
		ActionHandler.click(SVOAccountsContainer.FIN);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on the newly created SAP Account");

		// User clicks on Delete
		ActionHandler.wait(3);
		ActionHandler.click(SVOAccountsContainer.Delete);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Delete");

		// User clicks on Delete Button
		ActionHandler.click(SVOAccountsContainer.DeleteAccount);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Delete Button");
	}

	// User Navigates to Orders Section
	@Then("^User Navigates to Orders Section$")
	public void user_Navigates_to_Orders_Section() throws Throwable 
	{
		ActionHandler.wait(2);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);
		ActionHandler.pageDown();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		
		// User clicks on Show All Button
		/*ActionHandler.wait(3);
		javaScriptUtil.clickElementByJS(SVOAccountsContainer.ShowAll);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Show All Button");*/

		// User navigates to the order section
		ActionHandler.wait(3);
		ActionHandler.click(SVOAccountsContainer.OrdersSection);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to the order section");

	}

	// Verify Order Details under orders section
	@Then("^Verify Order Details under orders section$")
	public void verify_Order_Details_under_orders_section() throws Throwable {

		// User verifies Order Number
		ActionHandler.wait(2);
		VerifyHandler.verifyElementPresent(SVOAccountsContainer.OrderNumber);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Order Number");

		// User verifies Order Record Type
		VerifyHandler.verifyElementPresent(SVOAccountsContainer.OrderRecordType);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Order Record Type");

		// User verifies Order Status
		VerifyHandler.verifyElementPresent(SVOAccountsContainer.OrderStatus);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Order Status");

		// User verifies Order Brand
		VerifyHandler.verifyElementPresent(SVOAccountsContainer.OrderBrand);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Order Brand");

		// User verifies Order Model
		VerifyHandler.verifyElementPresent(SVOAccountsContainer.OrderModel);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Order Model");

	}

	// User Creates a New Order
	@Then("^User Creates a New Order$")
	public void user_Creates_a_New_Order() throws Throwable 
	{
		String Opportunity = "test1234";

		// User clicks on New Order
		ActionHandler.wait(3);
		ActionHandler.click(SVOAccountsContainer.NewOrder);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on New Order");

		// User clicks on Next Button
		ActionHandler.wait(2);
		ActionHandler.click(SVOAccountsContainer.Next);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Next Button");

		// User search and enters Opportunity
		ActionHandler.wait(3);
		ActionHandler.click(SVOAccountsContainer.SearchOpportunities);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(SVOAccountsContainer.SearchOpportunities, Opportunity);
		ActionHandler.wait(10);

		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity");

		// User enters Start Date
		ActionHandler.wait(2);
		ActionHandler.click(SVOAccountsContainer.OrderStartDate);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAccountsContainer.Today);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Start Date");
	}

	// Enter details for New Account Relationship like Contact,Roles ,Relationship
	// Strength and Start Date
	@Then("^Enter details for New Account Relationship like Contact \"([^\"]*)\",Roles \"([^\"]*)\",Relationship Strength \"([^\"]*)\" and Start Date$")
	public void enter_details_for_New_Account_Relationship_like_Contact_Roles_Relationship_Strength_and_Start_Date(
			String Contact, String role, String Strength) throws Throwable {

		// data taken from master data sheet
		String con[] = Contact.split(",");
		Contact = CommonFunctions.readExcelMasterData(con[0], con[1], con[2]);

		String rol[] = role.split(",");
		role = CommonFunctions.readExcelMasterData(rol[0], rol[1], rol[2]);

		String str[] = Strength.split(",");
		Strength = CommonFunctions.readExcelMasterData(str[0], str[1], str[2]);

		// user search and enter the contact information
		ActionHandler.wait(3);
		ActionHandler.setText(SVOAccountsContainer.SearchContacts, Contact);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAccountsContainer.BrowseContacts);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(4);
		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.OppAccountSearchselect(Contact))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// User selects a Role
		ActionHandler.wait(4);
		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.RolesSelection(role))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects a Role");

		// User moves selection to chosen
		ActionHandler.wait(4);
		ActionHandler.click(SVOAccountsContainer.ChooseRoles);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User moves selection to chosen");

		// User selects Relationship Strength
		ActionHandler.wait(4);
		ActionHandler.click(SVOAccountsContainer.RelatnStrength);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(4);
		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.StrengthSelection(Strength))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Relationship Strength");

		// User selects a Start Date
		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(4);
		ActionHandler.click(SVOAccountsContainer.OrderStartDate);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(4);
		ActionHandler.click(SVOAccountsContainer.Today);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects a Start Date");

	}

	// Save the details for New Account Relationship
	@Then("^Save the details for New Account Relationship$")
	public void save_the_details_for_New_Account_Relationship() throws Throwable {

		// Click on save button
		ActionHandler.wait(5);
		ActionHandler.click(SVOAccountsContainer.SaveRelationship);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on save button");
	}

	// Filter on Related Contacts using Contact
	@Then("^Filter on Related Contacts using Contact \"([^\"]*)\"$")
	public void filter_on_Related_Contacts_using_Contact(String Contact) throws Throwable {

		// data taken from master excel sheet
		String c[] = Contact.split(",");
		Contact = CommonFunctions.readExcelMasterData(c[0], c[1], c[2]);

		// User click on Filter Icon
		ActionHandler.wait(10);
		ActionHandler.click(SVOAccountsContainer.FilterAccounts);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Filter Icon");

		// User enters Contact Name
		ActionHandler.clearAndSetText(SVOAccountsContainer.ContactName, Contact);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Contact Name");

		// User clicks on Apply Button
		ActionHandler.click(SVOAccountsContainer.Apply);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Apply Button");

		// User clicks on Close Filter Button
		ActionHandler.click(SVOAccountsContainer.CloseFilter);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Close Filter Button");

	}

	// Remove Account Relationship
	@Then("^Remove Account Relationship$")
	public void remove_Account_Relationship() throws Throwable {

		// User clicks on Show Actions Button
		ActionHandler.wait(2);
		ActionHandler.click(SVOAccountsContainer.Showactions);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Show Actions Button");

		// User clicks on Remove Relationship Button
		ActionHandler.click(SVOAccountsContainer.RemoveRelationship);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Remove Relationship Button");

	}

	// User Saves the New order
	@Then("^User Saves the New order$")
	public void user_Saves_the_New_order() throws Throwable 
	{
		// Click on save button
		ActionHandler.wait(4);
		ActionHandler.click(SVOAccountsContainer.SaveRelation);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on save button");
	}

	// User Deletes the Order
	@Then("^User Deletes the Order$")
	public void user_Deletes_the_Order() throws Throwable 
	{
		// Click on Show All Button
		ActionHandler.wait(2);
		ActionHandler.click(SVOAccountsContainer.Showactions);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Show All Button");

		// User click on delete button
		ActionHandler.click(SVOAccountsContainer.delete);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAccountsContainer.DeleteAccount);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on delete button");
	}

	// User Searches for Contacts and selects it
	@Then("^User Searches for Contacts \"([^\"]*)\" and selects it$")
	public void user_Searches_for_Contacts_and_selects_it(String account) throws Throwable 
	{

		String acc[] = account.split(",");
		account = CommonFunctions.readExcelMasterData(acc[0], acc[1], acc[2]);

		// Search for an Contact and selects it
		ActionHandler.wait(2);
		ActionHandler.click(SVOAccountsContainer.SearchBoxContact);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		ActionHandler.clearAndSetText(SVOAccountsContainer.SearchBoxContact, accountName);
		CommonFunctions.attachScreenshot();

		ActionHandler.pressEnter();
		ActionHandler.wait(10);
		Reporter.addStepLog("Search for an Contact");
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		ActionHandler.click(SVOAccountsContainer.firstAccountIndividual);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Select Contact");
	}

	// Verify the contact information Account name and email ID
	@Then("^Verify the contact information Account name \"([^\"]*)\" and email ID \"([^\"]*)\"$")
	public void verify_the_contact_information_Account_name_and_email_ID(String account, String email)
			throws Throwable 
	{
		// data read from master data sheet
		String acc[] = account.split(",");
		account = CommonFunctions.readExcelMasterData(acc[0], acc[1], acc[2]);

		String ema[] = email.split(",");
		email = CommonFunctions.readExcelMasterData(ema[0], ema[1], ema[2]);

		System.out.println("All the data is loaded");
		
		// Verify that the Contact information is correct
		ActionHandler.wait(2);
		/*VerifyHandler.verifyElementPresent(driver.findElement(By.xpath(SVOAccountsContainer.Value(account))));
		ActionHandler.wait(5);*/
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verify that the Contact information is correct");

		// Verify that email information is correct
		ActionHandler.wait(2);
		/*VerifyHandler.verifyElementPresent(driver.findElement(By.xpath(SVOAccountsContainer.Value2(email))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();*/
		Reporter.addStepLog("Verify that email information is correct");
	}

	// User clicks on the change Owner button and changes the Owner
	@When("^User clicks on the change Owner and change Owner as \"([^\"]*)\"$")
	public void user_clicks_on_the_change_Owner_and_change_Owner_as(String Owner) throws Throwable 
	{

		String Own[] = Owner.split(",");
		Owner = CommonFunctions.readExcelMasterData(Own[0], Own[1], Own[2]);

		// Click on change owner button
		ActionHandler.wait(2);
		ActionHandler.click(SVOAccountsContainer.ChangeOwner);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on change owner button");

		// Input New Owner information
		ActionHandler.wait(2);
		ActionHandler.setText(SVOAccountsContainer.SearchUsers, Owner);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		ActionHandler.click(SVOAccountsContainer.ChOwnerSearchUser);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.dropdown(Owner))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Input New Owner information");
	}

	// User selects all the check boxes
	@When("^User selects all the check boxes$")
	public void user_selects_all_the_check_boxes() throws Throwable {

		// Click on Check box Transfer open opportunities owned by others
		ActionHandler.click(SVOAccountsContainer.chkbox1);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Check box Transfer open opportunities owned by others");

		// Click on Check box Transfer account owner's closed opportunities
		ActionHandler.click(SVOAccountsContainer.chkbox2);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Check box Transfer account owner's closed opportunities");

		// Click on Check box Transfer account owner's open cases
		ActionHandler.click(SVOAccountsContainer.chkbox3);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Check box Transfer account owner's open cases");

		// Click on Check box Transfer all of this account owner's cases
		ActionHandler.click(SVOAccountsContainer.chkbox4);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Check box Transfer all of this account owner's cases");

		// Click on Check box Send notification email
		ActionHandler.click(SVOAccountsContainer.chkbox5);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Check box Send notification email");
	}

	// User clicks on submit button for Owner change
	@Then("^User clicks on submit button for Owner change$")
	public void user_clicks_on_submit_button_for_Owner_change() throws Throwable 
	{
		ActionHandler.wait(3);
		ActionHandler.click(SVOAccountsContainer.Submit);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Submit button to change Owner");
	}

	// User clicks on change Record type and changes record type
	@When("^User clicks on change Record type and selects record type as \"([^\"]*)\"$")
	public void user_clicks_on_change_Record_type_and_selects_record_type_as(String recordType) throws Throwable 
	{

		String RecTy[] = recordType.split(",");
		recordType = CommonFunctions.readExcelMasterData(RecTy[0], RecTy[1], RecTy[2]);

		// Click on show more button
		ActionHandler.wait(2);
		ActionHandler.click(SVOAccountsContainer.ShowMore);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on show more button");

		// Click on Change record type button
		ActionHandler.wait(2);
		ActionHandler.click(SVOAccountsContainer.ChangeRecordType);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Change record type button");

		// User Select required Record Type
		ActionHandler.wait(3);
		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.ValueSelection2(recordType))));
		Reporter.addStepLog("User Select required Record Type");
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// User Navigate to enter account details page
		ActionHandler.wait(4);
		ActionHandler.click(SVOAccountsContainer.nextRType);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigate to enter account details page");
	}

	@Then("^User clicks save to proceed with the selected record type$")
	public void user_clicks_save_to_proceed_with_the_selected_record_type() throws Throwable 
	{
		ActionHandler.wait(5);
		ActionHandler.click(SVOAccountsContainer.SaveVehicle);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Save button");
	}

	// create a new SAP account with Requested Status
	@Then("^create a new SAP account with Requested Status$")
	public void create_a_new_SAP_account_with_Requested_Status() throws Throwable 
	{
		String SAP = "Test123";
		String Status = "Requested";

		double randomNumber = getRandomIntegerBetweenRange(0, 1000);

		// Click on new button
		ActionHandler.click(SVOAccountsContainer.NewSAP);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on new button");

		// Enter SAP Account details
		ActionHandler.wait(5);
		ActionHandler.setText(SVOAccountsContainer.SAPID, SAP + randomNumber);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAccountsContainer.SAPStatus);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.ValueSelection(Status))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter SAP Account details");

		// User saves SAP Account
		ActionHandler.click(SVOAccountsContainer.saveSAP);
		ActionHandler.wait(9);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves SAP Account");
	}
	
	@Given("^User access SVO Portal with SO Data Manager User$")
	public void User_Access_SVO_Portal_SO_Data_Manager() throws Exception
	{
		ActionHandler.wait(3);
		driver=getDriver();
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
				
		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.icon_image);
		ActionHandler.wait(5);
		ActionHandler.click(SVOAccountsContainer.addUserName);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		
		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		String UserName = Config.getPropertyValue("SODataManager_UserName");
		String Password = Config.getPropertyValue("SODataManager_Password");

		ActionHandler.wait(2);
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, UserName);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, Password);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User requires to enter Verification code received in an email");
		ActionHandler.wait(5);

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {
			Reporter.addStepLog("Please Open recently received email from Salesforce");
			ActionHandler.wait(10);
			vCode = SVOenquiry.checkEmail();
			System.out.println("Verification code is = "+vCode);
			
			driver.get(Constants.SVOURL);
			ActionHandler.wait(15);
			Reporter.addStepLog("User Logins to SVO Portal");
			
			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, UserName);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, Password);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			ActionHandler.wait(7);
			CommonFunctions.attachScreenshot();
			
			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			
			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			ActionHandler.wait(7);
			CommonFunctions.attachScreenshot();
			
		} 
		else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) 
		{
			ActionHandler.wait(5);
			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {
				ActionHandler.wait(3);
				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {
				ActionHandler.wait(3);
				ActionHandler.click(SVOEnquiryContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}
	
	@Given("^User access SVO Portal with Non-Admin User$")
	public void User_Access_SVO_portal_Non_Admin() throws Exception
	{
		driver=getDriver();
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
				
		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.icon_image);
		ActionHandler.wait(5);
		ActionHandler.click(SVOAccountsContainer.addUserName);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		
		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		String UserName = Config.getPropertyValue("ClassicAdmin_UserName");
		String Password = Config.getPropertyValue("ClassicAdmin_Password");

		ActionHandler.wait(2);
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, UserName);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, Password);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		
		Reporter.addStepLog("User requires to enter Verification code received in an email");
		ActionHandler.wait(5);

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {
			Reporter.addStepLog("Please Open recently received email from Salesforce");
			ActionHandler.wait(10);
			vCode = SVOenquiry.checkEmail();
			System.out.println("Verification code is = "+vCode);
			
			driver.get(Constants.SVOURL);
			ActionHandler.wait(15);
			Reporter.addStepLog("User Logins to SVO Portal");
			
			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, UserName);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, Password);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			ActionHandler.wait(7);
			CommonFunctions.attachScreenshot();
			
			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			
			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			ActionHandler.wait(7);
			CommonFunctions.attachScreenshot();
			
		} 
		else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) 
		{
			ActionHandler.wait(5);
			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {
				ActionHandler.wait(3);
				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {
				ActionHandler.wait(3);
				ActionHandler.click(SVOEnquiryContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	// User logins with Classic Sales role
	@Given("^User logins with Classic Sales role$")
	public void user_logins_with_Classic_Finance_role() throws Throwable 
	{
		String Role = "Michael Bishop";

		// User clicks on set up Gear
		ActionHandler.wait(5);
		ActionHandler.click(SVOAccountsContainer.setupGear);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on set up Gear");

		// User clicks set up drop down
		ActionHandler.click(SVOAccountsContainer.setup);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks set up drop down");

		// switch to new window
		String NewWindow = driver.getWindowHandle();
		ActionHandler.wait(30);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!NewWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.wait(10);

			}
		}

		// User clicks set up drop down
		ActionHandler.wait(10);
		ActionHandler.clearAndSetText(SVOAccountsContainer.roleSearch, Role);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks set up drop down");

		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(30);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks set up drop down");

		driver.switchTo().frame(0);

		// click on login button
		ActionHandler.click(SVOAccountsContainer.roleLogin);
		ActionHandler.wait(15);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks set up drop down");
	}

	// Verify that relation field is not present
	@Then("^Verify that relation field is not present$")
	public void verify_that_relation_field_is_not_present() throws Throwable {

		// Verify the presence of relation
		ActionHandler.wait(4);
		/*VerifyHandler.verifyElementPresent(SVOAccountsContainer.RelationStrenght);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verify the presence of relation");

		// User clicks on cancel button
		ActionHandler.click(SVOAccountsContainer.Cancel);
		ActionHandler.wait(5);*/
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Relation Field is not present");
	}

	// User logins with Special Ops Data Manager role
	@Given("^User logins with Special Ops Data Manager role$")
	public void user_logins_with_Special_Ops_Data_Manager_role() throws Throwable {
		String Role = "Sam Adams";

		// User clicks on set up Gear
		ActionHandler.wait(5);
		ActionHandler.click(SVOAccountsContainer.setupGear);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on set up Gear");

		// User clicks set up drop down
		ActionHandler.click(SVOAccountsContainer.setup);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks set up drop down");

		// switch to new window
		String NewWindow = driver.getWindowHandle();
		ActionHandler.wait(30);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!NewWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.wait(10);

			}
		}

		// enter text in role search box
		ActionHandler.wait(10);
		ActionHandler.clearAndSetText(SVOAccountsContainer.roleSearch, Role);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks set up drop down");

		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(30);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Input role information");

		driver.switchTo().frame(0);

		// click on login button
		ActionHandler.click(SVOAccountsContainer.roleLogin);
		ActionHandler.wait(15);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks set up drop down");
	}

	// Create new Corporate account with Record type, Account name, Email and Region
	@Then("^Create new Corporate account with Record type \"([^\"]*)\" Account name \"([^\"]*)\" Email \"([^\"]*)\" Region \"([^\"]*)\"$")
	public void create_new_Corporate_account_with_Record_type_Account_name_Email_Region(String recordType,
			String account, String email, String region) throws Throwable {

		// data is taken from master data sheet
		String rType[] = recordType.split(",");
		recordType = CommonFunctions.readExcelMasterData(rType[0], rType[1], rType[2]);

		String AccountName[] = account.split(",");
		account = CommonFunctions.readExcelMasterData(AccountName[0], AccountName[1], AccountName[2]);

		String AEmail[] = email.split(",");
		email = CommonFunctions.readExcelMasterData(AEmail[0], AEmail[1], AEmail[2]);

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		double randomNumber = getRandomIntegerBetweenRange(0, 100000);

		// User Creates new Corporate account
		ActionHandler.wait(3);
		Reporter.addStepLog("User Creates new Corporate account");
		ActionHandler.click(SVOAccountsContainer.NewAccount);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		// User Needs to select corporate record type option
		Reporter.addStepLog("User Needs to select corporate record type option");
		ActionHandler.wait(3);
		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.selectRecordType(recordType))));
		Reporter.addStepLog("User Select required Record Type");
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// User Navigate to enter account details page
		ActionHandler.wait(4);
		ActionHandler.click(SVOAccountsContainer.nextRType);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigate to enter account details page");

		// Enter Account name information
		ActionHandler.wait(1);
		ActionHandler.setText(SVOAccountsContainer.AccountName, account + randomNumber);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter Account name information");

		// Enter email information
		ActionHandler.wait(1);
		ActionHandler.setText(SVOAccountsContainer.AccEmail, email);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter email information");

		// Enter Region information
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAccountsContainer.CorpRegion);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.ValueSelection(region))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter Region information");

		// User saves Corporate Account
		ActionHandler.click(SVOAccountsContainer.saveAccount);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves Corporate Account");

	}

	// Create new Corporate account with Record type, Account name, Region and
	// without Phone or Email and verify the error
	@Then("^Create new Corporate account with Record type \"([^\"]*)\" Account name \"([^\"]*)\" Region \"([^\"]*)\" and without Phone or Email and verify the error$")
	public void create_new_Corporate_account_with_Record_type_Account_name_Region_and_without_Phone_or_Email_and_verify_the_error(
			String recordType, String account, String region) throws Throwable {

		// data retrieved from master data sheet
		String rType[] = recordType.split(",");
		recordType = CommonFunctions.readExcelMasterData(rType[0], rType[1], rType[2]);

		String AccountName[] = account.split(",");
		account = CommonFunctions.readExcelMasterData(AccountName[0], AccountName[1], AccountName[2]);

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		double randomNumber = getRandomIntegerBetweenRange(0, 1000);

		// User Creates new Corporate account
		ActionHandler.wait(3);
		Reporter.addStepLog("User Creates new Corporate account");
		ActionHandler.click(SVOAccountsContainer.NewAccount);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User Needs to select corporate record type option");
		ActionHandler.wait(3);

		// User Select required Record Type
		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.selectRecordType(recordType))));
		Reporter.addStepLog("User Select required Record Type");
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// User Navigate to enter account details page
		ActionHandler.wait(4);
		ActionHandler.click(SVOAccountsContainer.nextRType);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigate to enter account details page");

		// Enter Account name information
		ActionHandler.wait(1);
		ActionHandler.setText(SVOAccountsContainer.AccountName, account + randomNumber);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter Account name information");

		// Enter Region information
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAccountsContainer.CorpRegion);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.ValueSelection(region))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter Region information");

		// click on save button and verify the error message
		ActionHandler.click(SVOAccountsContainer.saveAccount);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verify the error is displayed when both phone and email fields are blank");
		ActionHandler.click(SVOAccountsContainer.cancelVehicle);
		ActionHandler.wait(10);
	}

	// Navigate to the Vehicles Owned section and click on New owner record type
	@Then("^Navigate to the Vehicles Owned section and click on New owner record type$")
	public void navigate_to_the_Vehicles_Owned_section_and_click_on_New_owner_record_type() throws Throwable {

		// Click on Vehicles owned button
		ActionHandler.wait(5);
		ActionHandler.click(SVOAccountsContainer.VehiclesOwned);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Vehicles owned button");

		// Click on New button
		ActionHandler.click(SVOAccountsContainer.NewVehiclesOwned);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on New button");

		// click on next button
		ActionHandler.click(SVOAccountsContainer.nextRType);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
	}

	// Enter details in new Vehicles owned and Save it
	@Then("^Enter details in new Vehicles owned and Save it$")
	public void enter_details_in_new_Vehicles_owned_and_Save_it() throws Throwable {

		String Manufacturer = "Land Rover";
		String Model = "Discovery";
		String Vehicle = "Discovery | BD08VTJ | 487907";

		// Enter Manufacturer information
		ActionHandler.wait(3);
		ActionHandler.setText(SVOAccountsContainer.newManufacturer, Manufacturer);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAccountsContainer.OwnManufacturerSearch);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.brandSearch(Manufacturer))));
		ActionHandler.wait(10);
		Reporter.addStepLog("Enter Manufacturer information");
		CommonFunctions.attachScreenshot();

		// Enter model information
		ActionHandler.setText(SVOAccountsContainer.newModel, Model);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAccountsContainer.OwnModelSearch);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.brandSearch(Model))));
		ActionHandler.wait(10);
		Reporter.addStepLog("Enter model information");
		CommonFunctions.attachScreenshot();

		// Enter model information
		ActionHandler.setText(SVOAccountsContainer.newVehicle, Vehicle);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAccountsContainer.VehicleSearchresults);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.brandSearch(Vehicle))));
		ActionHandler.wait(10);
		Reporter.addStepLog("Enter model information");
		CommonFunctions.attachScreenshot();

		// User saves SAP Account
		ActionHandler.click(SVOAccountsContainer.SaveVehicle);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves SAP Account");
	}

	// Navigate to the SVO Client Opportunities section and click on New SVO
	// opportunity
	@Then("^Navigate to the SVO Client Opportunities section and click on New SVO \"([^\"]*)\" opportunity$")
	public void navigate_to_the_SVO_Client_Opportunities_section_and_click_on_New_SVO_opportunity(String arg1)
			throws Throwable {

		// Click on SVO Client Opportunities button
		ActionHandler.wait(5);
		ActionHandler.click(SVOAccountsContainer.SVOClientOpp);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on SVO Client Opportunities button");

		// Click on New button
		ActionHandler.click(SVOAccountsContainer.NewSVOOpp);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on New button");

		ActionHandler.click(SVOAccountsContainer.nextRType);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
	}

	// Navigate to the SVO Client Opportunities section and click on New Classic
	// opportunity
	@Then("^Navigate to the SVO Client Opportunities section and click on New Classic \"([^\"]*)\" opportunity$")
	public void navigate_to_the_SVO_Client_Opportunities_section_and_click_on_New_Classic_opportunity(String recordType)
			throws Throwable {

		// Click on SVO Client Opportunities button
		ActionHandler.wait(5);
		ActionHandler.click(SVOAccountsContainer.SVOClientOpp);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on SVO Client Opportunities button");

		// Click on New button
		ActionHandler.click(SVOAccountsContainer.NewSVOOpp);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on New button");

		// User Select required Record Type
		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.selectRecordType(recordType))));
		Reporter.addStepLog("User Select required Record Type");
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAccountsContainer.nextRType);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
	}

	// Enter Required details in New Opportunity like Product Offering, Source,
	// Account Name, Region, Preferred Contact, Brand and Model
	@Then("^Enter Required details in New Opportunity like Product Offering \"([^\"]*)\" Source \"([^\"]*)\" Account Name \"([^\"]*)\" Region \"([^\"]*)\" Preferred Contact \"([^\"]*)\" Brand \"([^\"]*)\" and Model \"([^\"]*)\"$")
	public void enter_Required_details_in_New_Opportunity_like_Product_Offering_Source_Account_Name_Region_Preferred_Contact_Brand_and_Model(
			String productOffering, String Source, String AccountName, String region, String preContact, String brand,
			String model) throws Throwable {
		/*
		 * Below code is used to extract data from Excel Master Data and assign it to a
		 * string
		 */
		String pOffering[] = productOffering.split(",");
		productOffering = CommonFunctions.readExcelMasterData(pOffering[0], pOffering[1], pOffering[2]);

		String oSource[] = Source.split(",");
		Source = CommonFunctions.readExcelMasterData(oSource[0], oSource[1], oSource[2]);

		String oAccountName[] = AccountName.split(",");
		AccountName = CommonFunctions.readExcelMasterData(oAccountName[0], oAccountName[1], oAccountName[2]);

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String pc[] = preContact.split(",");
		preContact = CommonFunctions.readExcelMasterData(pc[0], pc[1], pc[2]);

		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String mo[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(mo[0], mo[1], mo[2]);

		/* Below strings contains information required to fill the New Opportunity */
		double randomNumber = getRandomIntegerBetweenRange(0, 1000);
		String OpportunityName = "Test_Opportunity_";
		String Stage = "Qualified";
		String close = CommonFunctions.selectFutureDate();

		Reporter.addStepLog("Start entering mandatory details");
		ActionHandler.wait(3);

		String recType = SVOAccountsContainer.SVORecordType.getText();
		Reporter.addStepLog("Selected Record Type is = " + recType);

		if (recType.equals("Classic Service")) {
			// Below Code is updated the Opportunity Name on New Opportunity Details page.
			ActionHandler.wait(7);
			System.out.println("User has clicked on Classic Service");
			ActionHandler.setText(SVOAccountsContainer.OpportunityName, OpportunityName + randomNumber);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();

			// Below Code Click on Close date and Select the date
			
			ActionHandler.setText(SVOAccountsContainer.OpportunityCloseDate, close);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();

			// Below Code Click on Stage and Select the Qualified value from the drop down
			ActionHandler.click(SVOAccountsContainer.OpportunityStage);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.ValueSelection(Stage))));
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();

			// Below Code click on Product offering and the select value from drop down
			ActionHandler.wait(5);
			ActionHandler.click(SVOAccountsContainer.OpportunityproOffering);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(5);
			ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.ValueSelection(productOffering))));
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();

			// Enter Region information
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(SVOAccountsContainer.OppRegion);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.ValueSelection(region))));
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Enter Region information");

			// Below Code search for Account name and then select value for it
			ActionHandler.pageScrollDown();
			ActionHandler.setText(SVOAccountsContainer.OpportunityAccountName, AccountName);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			Actions act = new Actions(driver);
			act.sendKeys(Keys.ARROW_DOWN).build().perform();
			act.sendKeys(Keys.ENTER).build().perform();
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();

			// Below Code search for prferredContact details then select value for preferred
			// contact
			ActionHandler.setText(SVOAccountsContainer.preferredCnt, preContact);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(SVOAccountsContainer.CpCntSearch);
			ActionHandler.wait(10);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.OppAccountSearchselect(preContact))));
			ActionHandler.wait(10);
			CommonFunctions.attachScreenshot();

			// Below Code search for brand name then select brand value
			ActionHandler.pageScrollDown();
			ActionHandler.setText(SVOAccountsContainer.brand, brand);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(SVOAccountsContainer.brandSearch);
			ActionHandler.wait(10);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.brandSearch(brand))));
			ActionHandler.wait(10);
			CommonFunctions.attachScreenshot();

			// Below Code search for Model name then select brand value
			ActionHandler.setText(SVOAccountsContainer.model, model);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(SVOAccountsContainer.modelSearch);
			ActionHandler.wait(10);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.brandSearch(model))));
			ActionHandler.wait(10);
			CommonFunctions.attachScreenshot();

			// Below Code Click on Source and Select the Source value from the drop down
			ActionHandler.scrollToView(SVOAccountsContainer.Opportunitysource);
			ActionHandler.click(SVOAccountsContainer.Opportunitysource);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.ValueSelection(Source))));
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOAccountsContainer.saveAccount);
			ActionHandler.wait(10);
			CommonFunctions.attachScreenshot();
		} 
		else 
		{
			ActionHandler.wait(5);
			ActionHandler.setText(SVOAccountsContainer.OpportunityName, OpportunityName + randomNumber);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();

			// Below Code Click on Close date and Select the date
			ActionHandler.setText(SVOAccountsContainer.OpportunityCloseDate, close);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();

			// Below Code Click on Stage and Select the Qualified value from the drop down
			ActionHandler.click(SVOAccountsContainer.OpportunityStage);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.ValueSelection(Stage))));
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();

			// Below Code click on Product offering and the select value from drop down
			ActionHandler.wait(5);
			ActionHandler.click(SVOAccountsContainer.OpportunityproOffering);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(5);
			ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.ValueSelection(productOffering))));
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();

			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(SVOAccountsContainer.OppRegion);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.ValueSelection(region))));
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Enter Region information");

			// Below Code search for prferredContact details then select value for preferred
			// contact
			ActionHandler.setText(SVOAccountsContainer.preferredCnt, preContact);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(SVOAccountsContainer.pCntSearch);
			ActionHandler.wait(10);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.OppAccountSearchselect(preContact))));
			ActionHandler.wait(10);
			CommonFunctions.attachScreenshot();

			// Below Code search for Account name and then select value for it
			ActionHandler.pageScrollDown();
			ActionHandler.setText(SVOAccountsContainer.OpportunityAccountName, AccountName);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			Actions act = new Actions(driver);
			act.sendKeys(Keys.ARROW_DOWN).build().perform();
			act.sendKeys(Keys.ENTER).build().perform();
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();

			// Below Code Click on Source and Select the Source value from the drop down
			ActionHandler.scrollToView(SVOAccountsContainer.OpportunitysourceBS);
			ActionHandler.click(SVOAccountsContainer.OpportunitysourceBS);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.ValueSelection(Source))));
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();

			// Below Code search for brand name then select brand value
			ActionHandler.scrollToView(SVOAccountsContainer.brand);
			ActionHandler.wait(5);
			ActionHandler.setText(SVOAccountsContainer.brand, brand);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(SVOAccountsContainer.brandSearchBS);
			ActionHandler.wait(10);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.brandSearch(brand))));
			ActionHandler.wait(10);
			CommonFunctions.attachScreenshot();

			// Below Code search for Model name then select brand value
			ActionHandler.setText(SVOAccountsContainer.model, model);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(SVOAccountsContainer.modelSearchBS);
			ActionHandler.wait(10);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.brandSearch(model))));
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOAccountsContainer.saveAccount);
			ActionHandler.wait(10);
			CommonFunctions.attachScreenshot();

		}

	}

	// Click on View Account Hierarchy and verify only current account is displayed
	@Then("^Click on View Account Hierarchy and verify only current account is displayed$")
	public void click_on_View_Account_Hierarchy_and_verify_only_current_account_is_displayed() throws Throwable {
		ActionHandler.wait(5);
		ActionHandler.click(SVOAccountsContainer.AccHierarchy);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verify that only current account is displayed in the hierarchy");
	}

	// User adds parent account in details section
	@Then("^User adds parent account in details section$")
	public void user_adds_parent_account_in_details_section() throws Throwable {
		String Parent = "Test Corporate";

		// click on parent account
		ActionHandler.wait(5);
		ActionHandler.click(SVOAccountsContainer.ParentAcc);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// enters text in account text box
		ActionHandler.setText(SVOAccountsContainer.parent, Parent);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// click on search icon
		ActionHandler.click(SVOAccountsContainer.parentsearch);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		// selects account
		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.brandSearch(Parent))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// click on save button
		ActionHandler.click(SVOAccountsContainer.saveAccount);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
	}

	// Click on View Account Hierarchy and verify both parent and current account is
	// displayed
	@Then("^Click on View Account Hierarchy and verify both parent and current account is displayed$")
	public void click_on_View_Account_Hierarchy_and_verify_both_parent_and_current_account_is_displayed()
			throws Throwable {

		// Verify both parent and current account is displayed in the hierarchy
		ActionHandler.wait(5);
		ActionHandler.click(SVOAccountsContainer.AccHierarchy);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verify both parent and current account is displayed in the hierarchy");
	}

	// Click on New SAP Account
	@Then("^Click on New SAP Account$")
	public void click_on_new_SAP_Account() throws Throwable {

		// Click on new button
		ActionHandler.wait(5);
		ActionHandler.click(SVOAccountsContainer.NewSAP);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on new button");
	}

	// Navigate to the Related contacts section and click on New contact
	@Then("^Navigate to the Related contacts section and click on New contact$")
	public void navigate_to_the_Related_contacts_section_and_click_on_new_contact() throws Throwable {

		// Click on Related Contacts button
		ActionHandler.wait(5);
		ActionHandler.click(SVOAccountsContainer.RelatedContacts);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Related Contacts button");

		// Click on New contact button
		ActionHandler.click(SVOAccountsContainer.AddContact);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on New contact button");
	}

	// Enter details for New account like first name, last name, email and Save it
	@Then("^Enter details for New \"([^\"]*)\" like first name \"([^\"]*)\" last name \"([^\"]*)\" email \"([^\"]*)\" and Save it$")
	public void enter_details_for_new_like_first_name_last_name_email_and_save_it(String Type, String first,
			String last, String email) throws Throwable {

		// data taken from master data sheet
		String FirstName[] = first.split(",");
		first = CommonFunctions.readExcelMasterData(FirstName[0], FirstName[1], FirstName[2]);

		String LastName[] = last.split(",");
		last = CommonFunctions.readExcelMasterData(LastName[0], LastName[1], LastName[2]);

		String mail[] = email.split(",");
		email = CommonFunctions.readExcelMasterData(mail[0], mail[1], mail[2]);

		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		// User Select required Record Type
		ActionHandler.wait(5);
		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.selectRecordType(Type))));
		Reporter.addStepLog("User Select required Record Type");
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// click on next button
		ActionHandler.click(SVOAccountsContainer.nextRType);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		// enter first name
		ActionHandler.setText(SVOAccountsContainer.FirstName, first);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(SVOAccountsContainer.LastName, last + randomNumber);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// enter email id
		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);
		ActionHandler.setText(SVOAccountsContainer.newEmail, email);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter mandatory information information");

		// Click on save button
		ActionHandler.click(SVOAccountsContainer.saveAccount);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on save button");
		
		//Open details
		ActionHandler.wait(2);
		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.Value2(first))));
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens Contact Details of Corporate Contact");
	}

	// Navigate to Enquiries section and click on New enquiry
	@Then("^Navigate to Enquiries section and click on New enquiry$")
	public void navigate_to_Enquiries_section_and_click_on_New_enquiry() throws Throwable {

		// Click on Enquiries link
		ActionHandler.wait(5);
		ActionHandler.click(SVOAccountsContainer.RelLinkEnquiries);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Enquiries link");

		// Click on New Enquiry button
		ActionHandler.click(SVOAccountsContainer.NewEnquiry);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on New Enquiry button");
	}

	// Enter the details in new enquiry and save it
	@Then("^Enter the details \"([^\"]*)\" in new enquiry and save it$")
	public void enter_the_details_in_new_enquiry_and_save_it(String title) throws Throwable {
		double randomNumber = getRandomIntegerBetweenRange(0, 1000);

		// click on next button
		ActionHandler.wait(3);
		ActionHandler.click(SVOAccountsContainer.nextRType);
		CommonFunctions.attachScreenshot();

		// enter enquiry title
		ActionHandler.wait(5);
		ActionHandler.setText(SVOAccountsContainer.EnqTitle, title + randomNumber);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);

		// click on save button
		ActionHandler.click(SVOAccountsContainer.saveAccount);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("New Enquiry created");
	}

	// Navigate to the Account History section and view Account History for an
	// Corporate account
	@Then("^Navigate to the Account History section and view Account History for an Corporate account$")
	public void navigate_to_the_Account_History_section_and_view_Account_History_for_an_Corporate_account()
			throws Throwable {

		ActionHandler.pageDown();
		ActionHandler.pageArrowUp();
		ActionHandler.pageArrowUp();
		ActionHandler.pageArrowUp();
		ActionHandler.pageArrowUp();
		ActionHandler.pageArrowUp();
		ActionHandler.wait(5);

		// User views the account history
		ActionHandler.click(SVOAccountsContainer.showAll);
		ActionHandler.wait(5);
		ActionHandler.click(SVOAccountsContainer.AccHistory);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User views the account history");
	}

	// Create a new Note with title and description then Save it
	@Then("^Create a new Note with title \"([^\"]*)\" and description \"([^\"]*)\" and Save it$")
	public void create_a_new_Note_with_title_and_description_and_Save_it(String Title, String Desc) throws Throwable {

		// Enter details in notes like title and note
		ActionHandler.wait(5);
		ActionHandler.setText(SVOAccountsContainer.NoteTitle, Title);
		ActionHandler.wait(10);
		ActionHandler.click(SVOAccountsContainer.NoteDesc);
		ActionHandler.wait(10);
		ActionHandler.setText(SVOAccountsContainer.NoteDesc, Desc);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter details in notes");

		// click on done button
		ActionHandler.wait(10);
		ActionHandler.click(SVOAccountsContainer.Done);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Note saved");
	}

	// Navigate to the Account History section and verify user cannot edit the
	// Account History for an Corporate account
	@Then("^Navigate to the Account History section and verify user cannot edit the Account History for an Corporate account$")
	public void navigate_to_the_Account_History_section_and_verify_user_cannot_edit_the_Account_History_for_an_Corporate_account()
			throws Throwable {

		ActionHandler.pageDown();
		ActionHandler.pageArrowUp();
		ActionHandler.pageArrowUp();
		ActionHandler.pageArrowUp();
		ActionHandler.pageArrowUp();
		ActionHandler.pageArrowUp();

		// Verify there is no edit button for the user to edit the account history
		ActionHandler.wait(5);
		ActionHandler.click(SVOAccountsContainer.showAll);
		ActionHandler.wait(5);
		ActionHandler.click(SVOAccountsContainer.AccHistory);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verify there is no edit button for the user to edit the account history");
	}

	// Verify that Vehicles Driven link is available in Related List Quick links
	// section for Individual account
	@Then("^Verify that Vehicles Driven link is available in Related List Quick links section for Individual account$")
	public void vehicles_driven_link_available_for_individual_account() throws Throwable {

		// Vehicles driven link is available for individual account
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		VerifyHandler.verifyElementPresent(SVOAccountsContainer.VehiclesDriven);
		Reporter.addStepLog("Vehicles driven link is available for individual account");
	}

	// Verify that Vehicles Driven link is not available in Related List Quick links
	// section for Corporate account
	@Then("^Verify that Vehicles Driven link is not available in Related List Quick links section for Corporate account$")
	public void vehicles_driven_link_not_available_for_corporate_account() throws Throwable {

		// Vehicles driven link is not available for Corporate account
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		VerifyHandler.verifyElementPresent(SVOAccountsContainer.VehiclesDriven);
		Reporter.addStepLog("Vehicles driven link is not available for Corporate account");
	}

	// validate New order button
	@Then("^Verify new button is not available to create a new order$")
	public void Verify_new_button_is_not_available_to_create_a_new_order() throws Throwable {

		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("New order button is not available for Corporate account");
	}

	@Then("^User searches for \"([^\"]*)\" Account from select list view$")
	public void user_searches_for_Account_from_select_list_view(String arg1) throws Throwable {

		String s[] = arg1.split(",");
		arg1 = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		// user searches for opportunities list to display//
		ActionHandler.wait(5);
		ActionHandler.click(SVOAccountsContainer.AccountsSelectListView);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user searches for opportunities list to display");

		// user set text in filter text box//
		ActionHandler.wait(7);
		ActionHandler.setText(SVOAccountsContainer.AccountsSearchListTextBox, arg1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user set text in filter text box");

		ActionHandler.wait(5);
		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		Reporter.addStepLog("User chooses to view selected type Accounts");
		CommonFunctions.attachScreenshot();
	}

	// User click on first Account in the list
	@Then("^User click on first Account in the list$")
	public void User_click_on_first_Account_in_the_list() throws Throwable {

		ActionHandler.wait(5);
		ActionHandler.click(SVOAccountsContainer.FirstAccount);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on first Account in the list");
	}

	// Verify the SAP Accounts link
	@And("^Verify the SAP Accounts link$")
	public void Verify_the_SAP_Accounts_link() throws Throwable {

		ActionHandler.wait(5);
		if (VerifyHandler.verifyElementPresent(SVOAccountsContainer.SAPAccounts)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("SAP Account link is available to create an Account");
		} else {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("SAP Account link is unavailable to create an Account");
		}

	}

	// Navigate to the Vehicles Owned section//
	@Then("^Navigate to the Vehicles Owned section$")
	public void navigate_to_the_Vehicles_Owned_section() throws Throwable 
	{

		// Click on Vehicles Owned button//
		ActionHandler.wait(5);
		ActionHandler.click(SVOAccountsContainer.VehiclesOwned);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Vehicles Owned button");
	}

	// Verify the New button to add relationship as a driver
	@And("^Verify the New button to add relationship as a driver$")
	public void verify_the_New_button_to_add_relationship_as_a_driver() throws Throwable {

		ActionHandler.wait(5);
		if (VerifyHandler.verifyElementPresent(SVOAccountsContainer.NewVehiclesOwned)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("New button is available to add relationship as a driver");
		} else {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("New button is unavailable to add relationship as a driver");
		}

	}

	// User Navigate to Commissioning Suite
	@When("^User Navigate to Commissioning Suite$")
	public void user_Navigate_to_Commissioning_Suite() throws Throwable {
		ActionHandler.wait(5);
		javaScriptUtil.clickElementByJS(SVOAccountsContainer.CommissioningSuiteTab);
		Reporter.addStepLog("User Navigate to Commissioning Suite tab");
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

	}

	@When("^User selects time slot \"([^\"]*)\" for Reservation$")
	public void user_selects_time_slot_for_Reservation(String Time) throws Throwable {

		String T[] = Time.split(",");
		Time = CommonFunctions.readExcelMasterData(T[0], T[1], T[2]);

		// user click on day button
		ActionHandler.wait(5);
		ActionHandler.click(SVOAccountsContainer.DayButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on day button");

		// user click on today button
		ActionHandler.wait(5);
		ActionHandler.click(SVOAccountsContainer.TodayButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user clik on today button");

		// user selects time slot
		ActionHandler.wait(5);
		ActionHandler.click(SVOAccountsContainer.TimeSlot);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(" user selects time slot");

	}

	@Then("^fill all mandatory fields of Reservation like Status \"([^\"]*)\" CommissioningRequest \"([^\"]*)\" PrimaryContact \"([^\"]*)\" MeetingType \"([^\"]*)\" MaxAttendees \"([^\"]*)\"$")
	public void fill_all_mandatory_fields_of_Reservation_like_Status_CommissioningRequest_PrimaryContact_MeetingType_MaxAttendees(
			String Status, String CommissioningRequest, String PrimaryContact, String MeetingType, String MaxAttendees)
			throws Throwable {
		String ST[] = Status.split(",");
		Status = CommonFunctions.readExcelMasterData(ST[0], ST[1], ST[2]);

		String CR[] = CommissioningRequest.split(",");
		CommissioningRequest = CommonFunctions.readExcelMasterData(CR[0], CR[1], CR[2]);

		String PC[] = PrimaryContact.split(",");
		PrimaryContact = CommonFunctions.readExcelMasterData(PC[0], PC[1], PC[2]);

		String MT[] = MeetingType.split(",");
		MeetingType = CommonFunctions.readExcelMasterData(MT[0], MT[1], MT[2]);

		String MA[] = MaxAttendees.split(",");
		MaxAttendees = CommonFunctions.readExcelMasterData(MA[0], MA[1], MA[2]);

		ActionHandler.wait(15);
		javaScriptUtil.scrollIntoView(SVOAccountsContainer.Details);

		// user click on status drop down
		ActionHandler.wait(5);
		ActionHandler.click(SVOAccountsContainer.StatusDropDown);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on status drop down");

		// user selects Status from drop down list
		ActionHandler.wait(3);
		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.StatusSelect(Status))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user selects Status from drop down list");

		// user enters commissioning Request
		ActionHandler.wait(3);
		ActionHandler.setText(SVOAccountsContainer.CommissioningReq, CommissioningRequest);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		ActionHandler.click(SVOAccountsContainer.Request);
		Reporter.addStepLog("user enters commissioning Request");

		// user enters Primary Contact
		ActionHandler.wait(3);
		ActionHandler.setText(SVOAccountsContainer.PrimaryContact, PrimaryContact);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		ActionHandler.click(SVOAccountsContainer.Contact);
		Reporter.addStepLog("user enters Primary Contact");

		// user enters Max Attendees
		ActionHandler.wait(3);
		ActionHandler.setText(SVOAccountsContainer.MaxAttendees, MaxAttendees);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user enters Max Attendees");

		ActionHandler.wait(3);
		javaScriptUtil.scrollIntoView(SVOAccountsContainer.RecurringReservation);

		// user click on status drop down
		ActionHandler.wait(5);
		ActionHandler.click(SVOAccountsContainer.MeetingTypeDropDown);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on status drop down");

		// user selects Status from drop down list
		ActionHandler.wait(3);
		ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.StatusSelect(MeetingType))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user selects Status from drop down list");

	}

	// User save the Reservation
	@Then("^User save the Reservation$")
	public void user_save_the_Reservation() throws Throwable {
		// user click on save button
		ActionHandler.wait(3);
		ActionHandler.click(SVOAccountsContainer.SaveBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on save button");

	}

	// validate Update consent button
	@Then("^Verify Update Consent button is not available for the user$")
	public void Verify_update_consent_button_is_not_available_for_the_user() throws Throwable {

		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Update consent button is not available for the user");
	}

	// set up login
	@Then("^set up the user role login \"([^\"]*)\"$")
	public void set_up_the_user_role_login(String User) throws Throwable {
		String U[] = User.split(",");
		User = CommonFunctions.readExcelMasterData(U[0], U[1], U[2]);

		// user navigates to set up bar//
		ActionHandler.wait(5);
		ActionHandler.click(SVOAccountsContainer.setUpBtn);
		ActionHandler.wait(3);
		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(10);

		ActionHandler.wait(5);
		String NewWindow = driver.getWindowHandle();
		ActionHandler.wait(30);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String SetUpWindow = i.next();
			if (!NewWindow.equalsIgnoreCase(SetUpWindow)) {
				driver.switchTo().window(SetUpWindow);

				// selects the user role //
				ActionHandler.wait(5);
				ActionHandler.setText(SVOAccountsContainer.SearchSetup, User);
				ActionHandler.wait(5);
				Actions act1 = new Actions(driver);
				act1.sendKeys(Keys.ARROW_DOWN).build().perform();
				act1.sendKeys(Keys.ENTER).build().perform();
				Reporter.addStepLog("selects the user role");
				CommonFunctions.attachScreenshot();
				ActionHandler.wait(10);

				// refresh the tab
				driver.navigate().refresh();
				ActionHandler.wait(5);

				// user clicks on login btn//
				ActionHandler.wait(10);
				driver.switchTo().frame(0);
				ActionHandler.wait(3);
				ActionHandler.click(SVOAccountsContainer.UserLoginBtn);
				ActionHandler.wait(10);
				Reporter.addStepLog("User is logged in successfully");
				CommonFunctions.attachScreenshot();
				// driver.switchTo().window(NewWindow);
			}
		}
	}

	@And("^User Navigate to Change Record Type section$")
	public void User_Navigate_to_Change_Record_Type_section() throws Throwable {

		// user click on drop down icon
		ActionHandler.wait(5);
		driver.navigate().refresh();
		ActionHandler.wait(15);

		ActionHandler.click(SVOAccountsContainer.ChangeRTypeDW);
		ActionHandler.wait(5);
		Reporter.addStepLog("User click on drop down icon");
		CommonFunctions.attachScreenshot();

		// user click on Change Record type option
		ActionHandler.click(SVOAccountsContainer.ChangeRType);
		ActionHandler.wait(5);
		Reporter.addStepLog("user click on Change Record type option");
		CommonFunctions.attachScreenshot();
	}

	// Verify the access denied error message
	@Then("^Verify the access denied error message$")
	public void Verify_the_access_denied_error_message() throws Throwable {

		ActionHandler.wait(5);
		if (VerifyHandler.verifyElementPresent(SVOAccountsContainer.AccessDeniedMsg)) {
			ActionHandler.wait(3);
			Reporter.addStepLog("Error message caught stating access denied");
			CommonFunctions.attachScreenshot();
		} else {

			ActionHandler.wait(3);
			Reporter.addStepLog("Error message dint caught");
			CommonFunctions.attachScreenshot();
		}

		// click on cancel button
		ActionHandler.wait(3);
		ActionHandler.click(SVOAccountsContainer.Cancel);
		Reporter.addStepLog("click on cancel button");
		CommonFunctions.attachScreenshot();

	}

	// validate availability of SVO bespoke record type
	@Then("^Navigate to the SVO Client Opportunities section and click on New and verify SVO Bespoke record type is not available$")
	public void navigate_to_the_SVO_Client_Opportunities_section_and_click_on_New_and_verify_SVO_Bespoke_record_type_is_not_available()
			throws Throwable {

		// Click on SVO Client Opportunities button
		ActionHandler.wait(5);
		ActionHandler.click(SVOAccountsContainer.SVOClientOpp);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on SVO Client Opportunities button");

		// Click on New button
		ActionHandler.click(SVOAccountsContainer.NewSVOOpp);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on New button");

		// verify SVO bespoke record type is not available
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("SVO Bespoke record type is not available for the user");
		ActionHandler.click(SVOAccountsContainer.cancelOpp);
	}

	// validate availability of classic record type
	@Then("^Navigate to the Opportunities section and click on New and verify Classic record type is not available$")
	public void navigate_to_the_Opportunities_section_and_click_on_New_and_verify_Classic_record_type_is_not_available()
			throws Throwable {

		// Click on Opportunities button
		ActionHandler.wait(5);
		ActionHandler.click(SVOAccountsContainer.SVOOpp);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Opportunities button");

		// Click on New button
		ActionHandler.click(SVOAccountsContainer.NewSVOOpp);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on New button");

		// verify classic record type is not available
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Classic record type is not available and SVO record type is selected by default");
		ActionHandler.click(SVOAccountsContainer.cancelVehicle);
	}

	// navigate to files and delete
	@Then("^navigate to the file and delete it$")
	public void navigate_to_the_file_and_delete_it() throws Throwable {

		driver.navigate().refresh();
		ActionHandler.wait(20);

		// User clicks on show actions
		ActionHandler.click(SVOAccountsContainer.ShowActions);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on show actions");
		CommonFunctions.attachScreenshot();

		// User clicks on delete button
		ActionHandler.click(SVOAccountsContainer.DeleteVehicle);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on delete button");
		CommonFunctions.attachScreenshot();

		// User clicks on confirm delete
		ActionHandler.click(SVOAccountsContainer.ConfDeleteVehicle);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on confirm delete");

		CommonFunctions.attachScreenshot();
	}

	// verify error
	@Then("^Verify the displayed record error message$")
	public void verify_the_displayed_record_error_message() throws Throwable {

		// User verifies the error message
		VerifyHandler.verifyElementPresent(SVOAccountsContainer.SaveVehicle);
		ActionHandler.wait(15);
		Reporter.addStepLog("User verifies the error message");
		CommonFunctions.attachScreenshot();

		// User clicks on cancel vehicle
		ActionHandler.click(SVOAccountsContainer.cancelVehicle);
		ActionHandler.wait(15);
		Reporter.addStepLog("User clicks on cancel vehicle");
		CommonFunctions.attachScreenshot();
	}

}
