//package com.jlr.svo.tests;
//
//import java.util.Iterator;
//import java.util.Set;
//import java.util.concurrent.TimeUnit;
//
//import org.openqa.selenium.By;
//import org.openqa.selenium.Keys;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.interactions.Actions;
//import org.openqa.selenium.support.PageFactory;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import com.aventstack.extentreports.ExtentTest;
//import com.cucumber.listener.Reporter;
//import com.jlr.autotest.handlers.ActionHandler;
//import com.jlr.autotest.handlers.VerifyHandler;
//import com.jlr.base.TestBaseCC;
//import com.jlr.svo.constants.Constants;
//import com.jlr.svo.containers.SSEC_2383_Classic_Team_Wide_Signature_Amendment_Container;
//import com.jlr.svo.containers.SVOAdditionalVehicleContainer;
//import com.jlr.svo.containers.SVOEnquiryContainer;
//import com.jlr.svo.containers.SVOItemToApproveContainer;
//import com.jlr.svo.containers.SVO_OpportunityInvoiceContainer;
//import com.jlr.svo.containers.SVO_OpportunitySecondaryOwnerContainer;
//import com.jlr.svo.containers.SVO_RestrictedPartyScreeningContainer;
//import com.jlr.svo.utilities.CommonFunctions;
//
//import cucumber.api.java.en.And;
//import cucumber.api.java.en.Given;
//import cucumber.api.java.en.Then;
//
//public class SSEC_2383_Classic_Team_Wide_Signature_Amendment_Test extends TestBaseCC {
//
//	public ExtentTest extentLogger;
//	private WebDriver driver = getDriver();
//	CommonFunctions commonFunctions = new CommonFunctions(driver);
//	private static final Logger LOGGER = LoggerFactory.getLogger(SVOWorkOrderProgrammeField_Test.class.getName());
//	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
////	com.jlr.svo.containers.SSEC_2383_Classic_Team_Wide_Signature_Amendment_Container SSEC_2383_Classic_Team_Wide_Signature_Amendment_Container = PageFactory
////			.initElements(driver, SSEC_2383_Classic_Team_Wide_Signature_Amendment_Container.class);
//	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
//	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
//	SVOAdditionalVehicleContainer SVOAdditionalvehicleContainer = PageFactory.initElements(driver,
//			SVOAdditionalVehicleContainer.class);
//	SVO_OpportunityInvoiceContainer SVO_OpportunityInvoiceContainer = PageFactory.initElements(driver,
//			SVO_OpportunityInvoiceContainer.class);
//	SVOItemToApproveContainer SVOItemToapproveContainer = PageFactory.initElements(driver,
//			SVOItemToApproveContainer.class);
//	SVO_RestrictedPartyScreeningContainer RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
//			SVO_RestrictedPartyScreeningContainer.class);
//	SVO_OpportunitySecondaryOwnerContainer SVO_OpportunitySecondaryOwnerContainer = PageFactory.initElements(driver,
//			SVO_OpportunitySecondaryOwnerContainer.class);
//
//	public static String OpportunityName;
//
//	public static double getRandomIntegerBetweenRange(double min, double max) {
//		double x = (int) (Math.random() * ((max - min) + 1)) + min;
//		return x;
//	}
//
//	public void onStart() {
//
//		setupTest("SVOTest");
//		driver = getDriver();
//
//		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
//		SSEC_2383_Classic_Team_Wide_Signature_Amendment_Container = PageFactory.initElements(driver,
//				SSEC_2383_Classic_Team_Wide_Signature_Amendment_Container.class);
//		RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
//				SVO_RestrictedPartyScreeningContainer.class);
//		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
//		SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
//		SVOAdditionalvehicleContainer = PageFactory.initElements(driver, SVOAdditionalVehicleContainer.class);
//		RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
//				SVO_RestrictedPartyScreeningContainer.class);
//		SVOItemToapproveContainer = PageFactory.initElements(driver, SVOItemToApproveContainer.class);
//		SVO_OpportunitySecondaryOwnerContainer = PageFactory.initElements(driver,
//				SVO_OpportunitySecondaryOwnerContainer.class);
//		SVO_OpportunityInvoiceContainer = PageFactory.initElements(driver, SVO_OpportunityInvoiceContainer.class);
//
//		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
//
//	}
//
//	@Given("^User access to SVO Portal as classic service Username user with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
//	public void user_access_to_SVO_portal_as_classic_service_Username_user_with_Username_and_Password(String userName,
//			String password) throws Throwable {
//		String un[] = userName.split(",");
//		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);
//
//		String pass[] = password.split(",");
//		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);
//
//		LOGGER.debug("Beginning scenario execution...");
//		Reporter.addStepLog("User access SF SVO Portal");
//
//		onStart();
//		driver = getDriver();
//		driver.get(Constants.SVOURL);
//
//		Reporter.addStepLog("User Logins to SVO Portal");
//		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
//		CommonFunctions.attachScreenshot();
//
//		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
//		CommonFunctions.attachScreenshot();
//
//		ActionHandler.click(SVOEnquiryContainer.loginBtn);
//		CommonFunctions.attachScreenshot();
//		Reporter.addStepLog("User requires to enter Verification code received in an email");
//
//	}
//
//	@And("^User creates Classic Opportunity with all mandatory fields like Stage \"([^\"]*)\" Record Type \"([^\"]*)\" Region \"([^\"]*)\" Client \"([^\"]*)\" Restricted Party Screening Stage \"([^\"]*)\" Preferred Contact \"([^\"]*)\" SAP Account \"([^\"]*)\" Account Name \"([^\"]*)\" Retailer Contact \"([^\"]*)\" Source \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\"$")
//	public void User_creates_an_Classic_Opportunity_with_mandatory_details(String stage, String recordType,
//			String region, String client, String Checkcleared, String preferredContact, String SAPAccount,
//			String accountName, String retailerContact, String source, String brand, String model) throws Exception {
//		String oppName = "Test_Classic_Service_Opportunity_";
//		double randomNumber = getRandomIntegerBetweenRange(0, 10000);
//
//		String productoffering = "Bespoke";
//
//		String closeDate = CommonFunctions.selectFutureDate();
//		Reporter.addStepLog("Opportunity Close Date is = " + closeDate);
//
//		String s[] = stage.split(",");
//		stage = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);
//
//		String RT[] = recordType.split(",");
//		recordType = CommonFunctions.readExcelMasterData(RT[0], RT[1], RT[2]);
//
//		String r[] = region.split(",");
//		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);
//
//		String c[] = client.split(",");
//		client = CommonFunctions.readExcelMasterData(c[0], c[1], c[2]);
//
//		String RPS[] = Checkcleared.split(",");
//		Checkcleared = CommonFunctions.readExcelMasterData(RPS[0], RPS[1], RPS[2]);
//
//		String preCon[] = preferredContact.split(",");
//		preferredContact = CommonFunctions.readExcelMasterData(preCon[0], preCon[1], preCon[2]);
//
//		String SAP[] = SAPAccount.split(",");
//		SAPAccount = CommonFunctions.readExcelMasterData(SAP[0], SAP[1], SAP[2]);
//
//		String accName[] = accountName.split(",");
//		accountName = CommonFunctions.readExcelMasterData(accName[0], accName[1], accName[2]);
//
//		String retCon[] = retailerContact.split(",");
//		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);
//
//		String so[] = source.split(",");
//		source = CommonFunctions.readExcelMasterData(so[0], so[1], so[2]);
//
//		String b[] = brand.split(",");
//		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);
//
//		String m[] = model.split(",");
//		model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);
//
//		ActionHandler.wait(3);
//		Reporter.addStepLog("User starts creating new SVO Classic Service Opportunity");
//
//		ActionHandler.click(SVOAdditionalvehicleContainer.NewOpportunity);
//		ActionHandler.wait(3);
//		CommonFunctions.attachScreenshot();
//
//		if (VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.QuoteNextBtn)) {
//			ActionHandler.click(driver
//					.findElement(By.xpath(SVO_OpportunitySecondaryOwnerContainer.RecordTypeSelection(recordType))));
//			CommonFunctions.attachScreenshot();
//			Reporter.addStepLog("User selects opportunity record type as Classic Service");
//
//			ActionHandler.click(SVOItemToapproveContainer.QuoteNextBtn);
//			CommonFunctions.attachScreenshot();
//			Reporter.addStepLog("User clicks on next button");
//
//		}
//
//		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeOppName, oppName + randomNumber);
//		CommonFunctions.attachScreenshot();
//		Reporter.addStepLog("User enters Opportunity Name");
//
//		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeClosedDate, closeDate);
//		CommonFunctions.attachScreenshot();
//		Reporter.addStepLog("User enters Opportunity Closed date");
//
//		ActionHandler.click(SVOAdditionalvehicleContainer.productoffering);
//		CommonFunctions.attachScreenshot();
//		ActionHandler.click(
//				driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ProOfferingSelection(productoffering))));
//		CommonFunctions.attachScreenshot();
//		Reporter.addStepLog("User chooses Stage for an Opportunity");
//
//		CommonFunctions.attachScreenshot();
//		Reporter.addStepLog("User chooses Product Offering for an Opportunity");
//
//		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeStage);
//		CommonFunctions.attachScreenshot();
//		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.StageSelection(stage))));
//		CommonFunctions.attachScreenshot();
//		Reporter.addStepLog("User chooses Stage for an Opportunity");
//
//		ActionHandler.scrollToView(SVOAdditionalvehicleContainer.SVOBespokeRegion);
//		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRegion);
//		CommonFunctions.attachScreenshot();
//		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.RegionSelection(region))));
//		CommonFunctions.attachScreenshot();
//		Reporter.addStepLog("User chooses Region for an Opportunity");
//
//		ActionHandler.pageScrollDown();
//		ActionHandler.wait(2);
//
//		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeAccountName, accountName);
//		ActionHandler.wait(2);
//		CommonFunctions.attachScreenshot();
//
//		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeAccountNameSearch);
//		CommonFunctions.attachScreenshot();
//		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(accountName))));
//		CommonFunctions.attachScreenshot();
//		Reporter.addStepLog("User selects Account Name for an Opportunity");
//
//		ActionHandler.scrollToView(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
//		VerifyHandler.verifyElementPresent(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
//		CommonFunctions.attachScreenshot();
//		ActionHandler.click(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
//		CommonFunctions.attachScreenshot();
//		ActionHandler
//				.click(driver.findElement(By.xpath(RestrictedPartyScreeningContainer.ValueSelection(Checkcleared))));
//		CommonFunctions.attachScreenshot();
//		Reporter.addStepLog("User selects restricted party screening as Check cleared for an Opportunity");
//
//		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokePreferredContact, preferredContact);
//		CommonFunctions.attachScreenshot();
//		Actions act1 = new Actions(driver);
//		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
//		ActionHandler.wait(1);
//		act1.sendKeys(Keys.ENTER).build().perform();
//		ActionHandler.wait(3);
//		Reporter.addStepLog("User selects Preferred Contact for an Opportunity");
//
//		ActionHandler.click(SVO_OpportunityInvoiceContainer.SAPAccountSearch);
//		ActionHandler.setText(SVO_OpportunityInvoiceContainer.SAPAccountSearch, SAPAccount);
//		CommonFunctions.attachScreenshot();
//
//		ActionHandler.click(SVO_OpportunityInvoiceContainer.SAPAccountNameSearch);
//		CommonFunctions.attachScreenshot();
//		ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityInvoiceContainer.SAPAccount(SAPAccount))));
//		CommonFunctions.attachScreenshot();
//		Reporter.addStepLog("User selects SAP Account for an Opportunity");
//
//		ActionHandler.pageScrollDown();
//		ActionHandler.wait(2);
//		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeBrand, brand);
//		CommonFunctions.attachScreenshot();
//
//		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeBrandSearch);
//		CommonFunctions.attachScreenshot();
//		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(brand))));
//		CommonFunctions.attachScreenshot();
//		Reporter.addStepLog("User selects Brand for an Opportunity");
//
//		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeModel, model);
//		CommonFunctions.attachScreenshot();
//
//		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);
//		CommonFunctions.attachScreenshot();
//		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(model))));
//		CommonFunctions.attachScreenshot();
//		Reporter.addStepLog("User selects Model for an Opportunity");
//
//		ActionHandler.scrollToView(SVOAdditionalvehicleContainer.SVOBespokeSource);
//		ActionHandler.wait(2);
//		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSource);
//		CommonFunctions.attachScreenshot();
//		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.SourceSelection(source))));
//		CommonFunctions.attachScreenshot();
//		Reporter.addStepLog("User chooses Source for an Opportunity");
//
//		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSaveOpportunity);
//		ActionHandler.wait(8);
//		CommonFunctions.attachScreenshot();
//		Reporter.addStepLog("User saves SVO Bespoke Opportunity");
//
//		OpportunityName = SVOAdditionalvehicleContainer.OpportunityName.getText();
//		Reporter.addStepLog("Opportunity Name is = " + OpportunityName);
//	}
//
//	@Then("^User click on the Emails to verify e2a emails ClassicEmail \"([^\"]*)\",eo2email \"([^\"]*)\"$")
//	public void User_click_on_the_Emails_to_verify_e2a_emails(String ClassicEmail, String eo2email) throws Throwable {
//
//		String C[] = ClassicEmail.split(",");
//		ClassicEmail = CommonFunctions.readExcelMasterData(C[0], C[1], C[2]);
//
//		String e[] = eo2email.split(",");
//		eo2email = CommonFunctions.readExcelMasterData(e[0], e[1], e[2]);
//
//		ActionHandler.scrollToView(SSEC_2383_Classic_Team_Wide_Signature_Amendment_Container.Emails);
//		ActionHandler.wait(3);
//		javaScriptUtil.clickElementByJS(SSEC_2383_Classic_Team_Wide_Signature_Amendment_Container.Emails);
//		ActionHandler.wait(3);
//		Reporter.addStepLog("User click on Email button");
//
//		ActionHandler.click(SSEC_2383_Classic_Team_Wide_Signature_Amendment_Container.e2asendanemail);
//		ActionHandler.wait(10);
//		Reporter.addStepLog("User click on Email button");
//
//		driver.switchTo().frame(SSEC_2383_Classic_Team_Wide_Signature_Amendment_Container.EmailFrame);
//		ActionHandler.wait(3);
//		CommonFunctions.attachScreenshot();
//		Reporter.addStepLog("user navigate to email frame");
//
//		ActionHandler.wait(20);
//		ActionHandler.click(SSEC_2383_Classic_Team_Wide_Signature_Amendment_Container.SelectTemplate);
//		ActionHandler.wait(3);
//		Reporter.addStepLog("User click on Email button");
//
//		ActionHandler.wait(5);
//		// driver.switchTo().frame(0);
//		String outlookWindow = driver.getWindowHandle();
////		openNewTab();
//		ActionHandler.wait(8);
//		Set<String> outlookWindows = driver.getWindowHandles();
//		Iterator<String> i = outlookWindows.iterator();
//		while (i.hasNext()) {
//			String mailWindow = i.next();
//			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
//				driver.switchTo().window(mailWindow);
//
////				driver.switchTo().f
//
//				ActionHandler.click(SSEC_2383_Classic_Team_Wide_Signature_Amendment_Container.Alllightningtemplates);
//				ActionHandler.wait(3);
//				Reporter.addStepLog("User click on all lightning templates");
//
//				ActionHandler.click(driver.findElement(
//						By.xpath(SSEC_2383_Classic_Team_Wide_Signature_Amendment_Container.selectemail(ClassicEmail))));
//
////				javaScriptUtil.clickElementByJS(SSEC_2383_Classic_Team_Wide_Signature_Amendment_Container.Classicemail);
//				ActionHandler.wait(3);
//				Reporter.addStepLog("User click on the classic Email");
//
////				ActionHandler.click(SSEC_2383_Classic_Team_Wide_Signature_Amendment_Container.Classicemail);
////				ActionHandler.wait(3);
////				Reporter.addStepLog("User click on the classic Email");
//
//				ActionHandler.click(SSEC_2383_Classic_Team_Wide_Signature_Amendment_Container.All);
//				ActionHandler.wait(3);
//				Reporter.addStepLog("User click on the classic Email");
//				ActionHandler.click(driver.findElement(
//						By.xpath(SSEC_2383_Classic_Team_Wide_Signature_Amendment_Container.selectemail(ClassicEmail))));
////				javaScriptUtil.clickElementByJS(SSEC_2383_Classic_Team_Wide_Signature_Amendment_Container.SOe2a);
//				ActionHandler.wait(3);
//				Reporter.addStepLog("User click on the classic Email");
//
//				ActionHandler.click(SSEC_2383_Classic_Team_Wide_Signature_Amendment_Container.Newemailsignature);
//				ActionHandler.wait(3);
//				Reporter.addStepLog("User click on the classic Email");
//
//				ActionHandler.click(SSEC_2383_Classic_Team_Wide_Signature_Amendment_Container.Btn);
//				ActionHandler.wait(3);
//				Reporter.addStepLog("User click on the classic Email");
//				driver.switchTo().window(outlookWindow);
////				driver.switchTo().parentFrame();
////				driver.close();
////				driver.switchTo().window(outlookWindow);
//
//			}
//		}
//		driver.switchTo().frame(SSEC_2383_Classic_Team_Wide_Signature_Amendment_Container.EmailFrame);
//		ActionHandler.wait(3);
//		CommonFunctions.attachScreenshot();
//		Reporter.addStepLog("user navigate to email frame");
//
//		ActionHandler.scrollToView(SSEC_2383_Classic_Team_Wide_Signature_Amendment_Container.Classicserviceuser);
//		ActionHandler.wait(3);
//		VerifyHandler.verifyElementPresent(SSEC_2383_Classic_Team_Wide_Signature_Amendment_Container.Vehicle);
//
//	}
//
//}
