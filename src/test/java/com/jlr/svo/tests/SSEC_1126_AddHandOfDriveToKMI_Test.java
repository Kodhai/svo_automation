package com.jlr.svo.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.containers.SVOAccountsContainer;
import com.jlr.svo.containers.SVOAdditionalVehicleContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.containers.SVOItemToApproveContainer;
import com.jlr.svo.containers.SVOOpportunityContainer;
import com.jlr.svo.containers.SVOWorkOrderContainer;
import com.jlr.svo.containers.SVO_CustomerResponsesContainer;
import com.jlr.svo.containers.SVO_OpportunityInvoiceContainer;
import com.jlr.svo.containers.SVO_OpportunitySecondaryOwnerContainer;
import com.jlr.svo.containers.SVO_Pricing_LogicContainer;
import com.jlr.svo.containers.SVO_RestrictedPartyScreeningContainer;
import com.jlr.svo.containers.SSEC_1126_AddHandOfDriveToKMIContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SSEC_1126_AddHandOfDriveToKMI_Test extends TestBaseCC {

	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory.getLogger(SSEC_1126_AddHandOfDriveToKMI_Test.class.getName());
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);

	SVOAccountsContainer SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
	SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	SVOAdditionalVehicleContainer SVOAdditionalvehicleContainer = PageFactory.initElements(driver,
			SVOAdditionalVehicleContainer.class);
	SVO_RestrictedPartyScreeningContainer RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
			SVO_RestrictedPartyScreeningContainer.class);
	SVOItemToApproveContainer SVOItemToapproveContainer = PageFactory.initElements(driver,
			SVOItemToApproveContainer.class);
	SVO_CustomerResponsesContainer SVO_CustomerResponsesContainer = PageFactory.initElements(driver,
			SVO_CustomerResponsesContainer.class);
	SVO_OpportunitySecondaryOwnerContainer SVO_OpportunitySecondaryOwnerContainer = PageFactory.initElements(driver,
			SVO_OpportunitySecondaryOwnerContainer.class);
	SVO_OpportunityInvoiceContainer SVO_OpportunityInvoiceContainer = PageFactory.initElements(driver,
			SVO_OpportunityInvoiceContainer.class);
	SVO_Pricing_LogicContainer SVO_Pricing_LogicContainer = PageFactory.initElements(driver,
			SVO_Pricing_LogicContainer.class);
	SSEC_1126_AddHandOfDriveToKMIContainer AddHandOfDriveToKMIContainer = PageFactory.initElements(driver,
			SSEC_1126_AddHandOfDriveToKMIContainer.class);
	SVOWorkOrderContainer SVOWorkOrderContainer = PageFactory.initElements(driver, SVOWorkOrderContainer.class);
	
	public static String verificationCode;
	public static String vCode;
	public static String veriCode;
	public static String OpportunityName;
	public static String KMIName; 
	

	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}
	
	@When("^User navigates to KMI section$")
	public void user_navigates_to_KMI_section() throws Throwable {
		String KMI = "KMIs";

		if (VerifyHandler.verifyElementPresent(AddHandOfDriveToKMIContainer.KMIsTab)) {
			javaScriptUtil.clickElementByJS(AddHandOfDriveToKMIContainer.KMIsTab);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User Navigate to KMIs page");
		} else {
			ActionHandler.click(SVOAccountsContainer.menuBtn);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("user click on menu button");

			ActionHandler.click(SVOAccountsContainer.searchBox);
			ActionHandler.setText(SVOAccountsContainer.searchBox, KMI);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("user enter text as KMIs in search box");

			ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.selectMenu(KMI))));
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User Navigates to KMIs page from SVO Menu bar");
			
		}
	}

	@Then("^User create new inactive KMI with all fields like Contact details \"([^\"]*)\" Hand of Drive \"([^\"]*)\" Product Offering \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\"$")
	public void user_create_new_inactive_KMI_with_all_fields_like_Active_status_Contact_details_Product_Offering_Brand_Model(String contactdetails, String handofdrive, String productOffering, String brand, String model) throws Throwable {
	    
		String CD[] = contactdetails.split(",");
		contactdetails = CommonFunctions.readExcelMasterData(CD[0], CD[1], CD[2]);

		String proOff[] = productOffering.split(",");
		productOffering = CommonFunctions.readExcelMasterData(proOff[0], proOff[1], proOff[2]);
		
		String HOD[] = handofdrive.split(",");
		handofdrive = CommonFunctions.readExcelMasterData(HOD[0], HOD[1], HOD[2]);

		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);
		
		String m[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);
		
		ActionHandler.click(AddHandOfDriveToKMIContainer.NewKMIBtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		
		ActionHandler.setText(AddHandOfDriveToKMIContainer.KMIContacts, contactdetails);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(AddHandOfDriveToKMIContainer.KMIContactNameSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(AddHandOfDriveToKMIContainer.ValueSelection(contactdetails))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Contact for an MKI");
		
		ActionHandler.click(AddHandOfDriveToKMIContainer.KMIProductoffering);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(
				driver.findElement(By.xpath(AddHandOfDriveToKMIContainer.ProdOffSelection(productOffering))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Product Offering for an KMI");
		
		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeBrand, brand);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeBrandSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(brand))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Brand for an KMI");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeModel, model);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(model))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Model for an KMI");
		
		ActionHandler.click(AddHandOfDriveToKMIContainer.SaveKMIBtn);
		ActionHandler.wait(5
				);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves an KMI");
		
		KMIName = AddHandOfDriveToKMIContainer.KMIName.getText();
		Reporter.addStepLog("KMI Name is = " + KMIName);
	}

	@Then("^User create new active KMI with all fields like Contact details \"([^\"]*)\" Hand of Drive \"([^\"]*)\" Product Offering \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\"$")
	public void user_create_new_active_KMI_with_all_fields_like_Active_status_Contact_details_Product_Offering_Brand_Model(String contactdetails, String handofdrive, String productOffering, String brand, String model) throws Throwable {
	    
		String CD[] = contactdetails.split(",");
		contactdetails = CommonFunctions.readExcelMasterData(CD[0], CD[1], CD[2]);

		String proOff[] = productOffering.split(",");
		productOffering = CommonFunctions.readExcelMasterData(proOff[0], proOff[1], proOff[2]);
		
		String HOD[] = handofdrive.split(",");
		handofdrive = CommonFunctions.readExcelMasterData(HOD[0], HOD[1], HOD[2]);

		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);
		
		String m[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);
		
		ActionHandler.click(AddHandOfDriveToKMIContainer.NewKMIBtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		
		ActionHandler.click(AddHandOfDriveToKMIContainer.ActiveStatus);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User check box the active field on KMIs page");
		
		ActionHandler.setText(AddHandOfDriveToKMIContainer.KMIContacts, contactdetails);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(AddHandOfDriveToKMIContainer.KMIContactNameSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(AddHandOfDriveToKMIContainer.ValueSelection(contactdetails))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Contact for an MKI");
		
		ActionHandler.click(AddHandOfDriveToKMIContainer.KMIProductoffering);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(AddHandOfDriveToKMIContainer.ProdOffSelection(productOffering))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Product Offering for an KMI");
		
		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeBrand, brand);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeBrandSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(brand))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Brand for an KMI");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeModel, model);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(model))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Model for an KMI");
		
		ActionHandler.click(AddHandOfDriveToKMIContainer.SaveKMIBtn);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves an KMI");
        
		
		KMIName = AddHandOfDriveToKMIContainer.KMIName.getText();
		Reporter.addStepLog("KMI Name is = " + KMIName);
	}

	
	@Then("^User views Hand of Drive details under All Active KMIs list view$")
	public void user_views_Hand_of_Drive_details_under_All_Active_KMIs_list_view() throws Throwable {
		javaScriptUtil.clickElementByJS(AddHandOfDriveToKMIContainer.KMIsTab);
		CommonFunctions.attachScreenshot();
		
		ActionHandler.click(AddHandOfDriveToKMIContainer.KMIsListViewDropdon);
		CommonFunctions.attachScreenshot();
		
		VerifyHandler.verifyElementPresent(AddHandOfDriveToKMIContainer.AllActiveKMIsListView);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Verifies 'All Active KMIs' List view on KMIs page");
		
		ActionHandler.click(AddHandOfDriveToKMIContainer.AllActiveKMIsListView);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks 'All Active KMIs' List view");
		
	}

	

	@Then("^User views Hand of Drive details under All Inactive KMIs list view$")
	public void user_views_Hand_of_Drive_details_under_All_Inactive_KMIs_list_view() throws Throwable {
		javaScriptUtil.clickElementByJS(AddHandOfDriveToKMIContainer.KMIsTab);
		CommonFunctions.attachScreenshot();
		
		ActionHandler.click(AddHandOfDriveToKMIContainer.KMIsListViewDropdon);
		CommonFunctions.attachScreenshot();
		
		VerifyHandler.verifyElementPresent(AddHandOfDriveToKMIContainer.AllInactiveKMIsListView);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Verifies 'All Inactive KMIs' List view on KMIs page");
		
		ActionHandler.click(AddHandOfDriveToKMIContainer.AllInactiveKMIsListView);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks 'All Inactive KMIs' List view");
	}

	@Then("^User sort All KMIs based on Hand of Drive Field$")
	public void user_sort_All_Inactive_KMIs_based_on_Hand_of_Drive_Field() throws Throwable {
	    
	}
	
	@Then("^User select newly created KMI under All Active KMIs list view$")
	public void user_select_newly_created_KMI_under_All_Active_KMIs_list_view() throws Throwable {
		javaScriptUtil.clickElementByJS(AddHandOfDriveToKMIContainer.KMIsTab);
		CommonFunctions.attachScreenshot();
		
		ActionHandler.click(AddHandOfDriveToKMIContainer.KMIsListViewDropdon);
		CommonFunctions.attachScreenshot();
		
		VerifyHandler.verifyElementPresent(AddHandOfDriveToKMIContainer.AllActiveKMIsListView);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Verifies 'All Active KMIs' List view on KMIs page");
		
		ActionHandler.click(AddHandOfDriveToKMIContainer.AllActiveKMIsListView);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks 'All Active KMIs' List view");
		
		ActionHandler.click(SVOAdditionalvehicleContainer.FirstOpportunityLink);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects the newly created KMI");
	}


	@Then("^Verify that KMI is updated under All Inactive KMIs list view$")
	public void verify_updated_KMI_under_All_Inactive_KMIs_list_view() throws Throwable {
	    
		javaScriptUtil.clickElementByJS(AddHandOfDriveToKMIContainer.KMIsTab);
		CommonFunctions.attachScreenshot();
		
		ActionHandler.click(AddHandOfDriveToKMIContainer.KMIsListViewDropdon);
		CommonFunctions.attachScreenshot();
		
		VerifyHandler.verifyElementPresent(AddHandOfDriveToKMIContainer.AllInactiveKMIsListView);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Verifies 'All Inactive KMIs' List view on KMIs page");
		
		ActionHandler.click(AddHandOfDriveToKMIContainer.AllInactiveKMIsListView);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks 'All Inactive KMIs' List view");
		
		ActionHandler.setText(AddHandOfDriveToKMIContainer.KMIsSearchBox, KMIName);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		ActionHandler.pressEnter();
		ActionHandler.wait(5);
		
		KMIName = AddHandOfDriveToKMIContainer.KMIName.getText();
		Reporter.addStepLog("KMI Name is = " + KMIName);
		Reporter.addStepLog("User verifies that updated KMI is present under All Inactive KMIs list view");
		
	}
	
	@Then("^User edits KMI details like Product Offering \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\"$")
	public void User_edits_KMI_details_like_Product_Offering_Brand_Model(String productOffering, String brand, String model) throws Throwable {
	    

		String proOff[] = productOffering.split(",");
		productOffering = CommonFunctions.readExcelMasterData(proOff[0], proOff[1], proOff[2]);

		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);
		
		String m[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);
		
		ActionHandler.click(AddHandOfDriveToKMIContainer.EditKMIBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks edit button on KMIs page");
		
		ActionHandler.click(AddHandOfDriveToKMIContainer.ActiveStatus);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User check box the active field on KMIs page");
	
		
		ActionHandler.click(AddHandOfDriveToKMIContainer.KMIProductoffering);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(AddHandOfDriveToKMIContainer.ProdOffSelection(productOffering))));
				
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Product Offering for an KMI");
		
		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeBrand, brand);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeBrandSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(brand))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Brand for an KMI");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeModel, model);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(model))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Model for an KMI");
		
		ActionHandler.click(AddHandOfDriveToKMIContainer.SaveKMIBtn);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves an KMI");
		
		
	}
	
	@When("^Verify that user is not able to view KMIs section on home page$")
	public void verify_that_user_is_not_able_to_view_KMIs_page_from_SVO_Menu_bar() throws Throwable {
		String KMIs = "KMIs";

		ActionHandler.click(SVOAccountsContainer.menuBtn);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on menu button");

		ActionHandler.click(SVOAccountsContainer.searchBox);
		ActionHandler.setText(SVOAccountsContainer.searchBox, KMIs);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user enter text as work orders in search box");

		if (VerifyHandler.verifyElementPresent(SVOWorkOrderContainer.NoAppOrdItemResults)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User is not able to view KMIs page from SVO menu bar");
		} else {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User is able to view KMIs page from SVO menu bar");
		}
	}

	@Then("^User delete created KMI from KMIs page$")
	public void user_delete_created_KMI_from_KMIs_page() throws Throwable {
		
		ActionHandler.click(SVO_OpportunitySecondaryOwnerContainer.OpportunityDeleteBtn);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_OpportunitySecondaryOwnerContainer.OpportunityDeleteDoneBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User delete the classic opportunity");
	}

	@Then("^Verify that KMI is not present on KMIs page$")
	public void verify_that_KMI_is_not_present_on_KMIs_page() throws Throwable {
		
		javaScriptUtil.clickElementByJS(AddHandOfDriveToKMIContainer.KMIsTab);
		CommonFunctions.attachScreenshot();
		
		ActionHandler.setText(AddHandOfDriveToKMIContainer.KMIsSearchBox, KMIName);
		ActionHandler.wait(3);
		ActionHandler.pressEnter();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User is not able to view the KMI which is already deleted");
	}
	
	@Then("^User create new active KMI without selecting Hand of drive field like Contact details \"([^\"]*)\" Product Offering \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\"$")
	public void user_create_new_active_KMI_without_selecting_Hand_of_Drive_fields_like_Active_status_Contact_details_Product_Offering_Brand_Model(String contactdetails, String handofdrive, String productOffering, String brand, String model) throws Throwable {
	    
		String CD[] = contactdetails.split(",");
		contactdetails = CommonFunctions.readExcelMasterData(CD[0], CD[1], CD[2]);

		String proOff[] = productOffering.split(",");
		productOffering = CommonFunctions.readExcelMasterData(proOff[0], proOff[1], proOff[2]);
		
		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);
		
		String m[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);
		
		ActionHandler.click(AddHandOfDriveToKMIContainer.NewKMIBtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		
		ActionHandler.click(AddHandOfDriveToKMIContainer.ActiveStatus);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User check box the active field on KMIs page");
		
		ActionHandler.setText(AddHandOfDriveToKMIContainer.KMIContacts, contactdetails);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(AddHandOfDriveToKMIContainer.KMIContactNameSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(AddHandOfDriveToKMIContainer.ValueSelection(contactdetails))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Contact for an MKI");
		
		ActionHandler.click(AddHandOfDriveToKMIContainer.KMIProductoffering);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(AddHandOfDriveToKMIContainer.ProdOffSelection(productOffering))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Product Offering for an KMI");
		
		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeBrand, brand);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeBrandSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(brand))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Brand for an KMI");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeModel, model);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(model))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Model for an KMI");
		
		ActionHandler.click(AddHandOfDriveToKMIContainer.SaveKMIBtn);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves an KMI");
		
		KMIName = AddHandOfDriveToKMIContainer.KMIName.getText();
		Reporter.addStepLog("KMI Name is = " + KMIName);
	}

	
	@Then("^User chooses source enquiry for an KMI Record Type \"([^\"]*)\"$")
	public void user_chooses_source_enquiry_for_an_KMI_Record_Type(String recordType) throws Throwable {
	    

		String rType[] = recordType.split(",");
		recordType = CommonFunctions.readExcelMasterData(rType[0], rType[1], rType[2]);

		String enquiryTitle = "Test_Enquiry_";
		double randomNumber = getRandomIntegerBetweenRange(0, 1000);

		ActionHandler.wait(4);
		Reporter.addStepLog("User Creates new enquiry");
		ActionHandler.click(AddHandOfDriveToKMIContainer.NewEnquiryBtn);
		ActionHandler.wait(8);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.selectRecordType(recordType))));
		Reporter.addStepLog("User Select Record Typefor an enquiry");
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOEnquiryContainer.nextRType);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigate to enter enquiry details page");
		
		ActionHandler.setText(SVOEnquiryContainer.enquiryTitle, enquiryTitle + randomNumber);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		System.out.println("User has entered Enquiry Title");
		
		ActionHandler.click(SVOEnquiryContainer.SaveEnquiry);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves Enquiry");
	}

	@Then("^Verify the details of enquiry from KMIs page$")
	public void verify_the_details_of_enquiry_from_KMIs_page() throws Throwable {
		
		ActionHandler.scrollToView(AddHandOfDriveToKMIContainer.SourceEnquiryLink);
		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(AddHandOfDriveToKMIContainer.SourceEnquiryLink);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		
		VerifyHandler.verifyElementPresent(AddHandOfDriveToKMIContainer.EnquiryName);
		Reporter.addStepLog("User views details of selected source enquiry on enquiry page");
	}

	@Then("^Verify that delete button is not available on KMIs page$")
	public void verify_that_delete_button_is_not_available_on_KMIs_page() throws Throwable {
		
		if (!VerifyHandler.verifyElementPresent(SVO_OpportunitySecondaryOwnerContainer.OpportunityDeleteBtn)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User is not able to view delete button on KMIs page");
		} else {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User is able to view delete button on KMIs page");
		}
	}

	@Then("^Verify that user is not able to choose source enquiry if contact details of enquiry and KMI is not matched$")
	public void verify_that_user_is_not_able_to_choose_source_enquiry_if_contact_details_of_enquiry_and_KMI_is_not_matched() throws Throwable {
		
		ActionHandler.click(AddHandOfDriveToKMIContainer.EditKMIBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks edit button on KMIs page");
		
		ActionHandler.click(AddHandOfDriveToKMIContainer.SourceEnquirySearchBox);
		CommonFunctions.attachScreenshot();
		Actions act3 = new Actions(driver);
		act3.sendKeys(Keys.ARROW_DOWN).build().perform();
		act3.sendKeys(Keys.ENTER).build().perform();
		Reporter.addStepLog("User selects source enquiry for an KMI");
		
		ActionHandler.click(AddHandOfDriveToKMIContainer.SaveKMIBtn);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves an KMI");
		
		VerifyHandler.verifyElementPresent(SVO_OpportunitySecondaryOwnerContainer.OpportunityDeleteBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verifies that user is not able to add source enquiry if conact details of enquiry and KMi is not matched");
		
		ActionHandler.click(AddHandOfDriveToKMIContainer.CancelBtn);
		CommonFunctions.attachScreenshot();
	}
	

	@Then("^Verify that edit option is not availabe for KMI name field on KMIs page$")
	public void verify_that_edit_option_is_not_availabe_for_KMI_name_field_on_KMIs_page() throws Throwable {
		if (!VerifyHandler.verifyElementPresent(AddHandOfDriveToKMIContainer.EditKMINameBtn)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User is not able to view edit option for KMI name field on KMIs page");
		} else {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User is able to view edit option for KMI name field on KMIs page");
		}
	}

	@Then("^Verify that Hand o Drive details are not present on KMis page$")
	public void verify_that_Hand_o_Drive_details_are_not_present_on_KMis_page() throws Throwable {
		javaScriptUtil.clickElementByJS(AddHandOfDriveToKMIContainer.KMIsTab);
		CommonFunctions.attachScreenshot();
		
		ActionHandler.click(AddHandOfDriveToKMIContainer.KMIsListViewDropdon);
		CommonFunctions.attachScreenshot();
		
		VerifyHandler.verifyElementPresent(AddHandOfDriveToKMIContainer.AllActiveKMIsListView);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Verifies 'All Active KMIs' List view on KMIs page");
		
		ActionHandler.click(AddHandOfDriveToKMIContainer.AllActiveKMIsListView);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks 'All Active KMIs' List view");
	}
	
	@Then("^Select an existing KMI from KMIs list$")
	public void select_an_existing_KMI_from_KMIs_list() throws Throwable {
	    
		ActionHandler.click(AddHandOfDriveToKMIContainer.KMIsListViewDropdon);
		CommonFunctions.attachScreenshot();
		
		VerifyHandler.verifyElementPresent(AddHandOfDriveToKMIContainer.AllActiveKMIsListView);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Verifies 'All Active KMIs' List view on KMIs page");
		
		ActionHandler.click(AddHandOfDriveToKMIContainer.AllActiveKMIsListView);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks 'All Active KMIs' List view");
		
		ActionHandler.click(AddHandOfDriveToKMIContainer.KMICheckBox);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects one Active KMI in the list");
	}

	@Then("^User change the KMI Onwer as classic user \"([^\"]*)\"$")
	public void user_change_the_KMI_Onwer_as_classic_user(String changeowner) throws Throwable {
		String CO[] = changeowner.split(",");
		changeowner = CommonFunctions.readExcelMasterData(CO[0], CO[1], CO[2]);
		
		ActionHandler.click(AddHandOfDriveToKMIContainer.ChangeOwnerBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on change owner button");
		
		ActionHandler.click(AddHandOfDriveToKMIContainer.OwnerSearchBox);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(AddHandOfDriveToKMIContainer.OwnerSearchBox, changeowner);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(AddHandOfDriveToKMIContainer.OwnerSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(AddHandOfDriveToKMIContainer.OwnerSelection(changeowner))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects different classic owner for an KMI");
		
		ActionHandler.click(AddHandOfDriveToKMIContainer.SaveChangeOwner);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves an KMI");
	}

	@Then("^User not able to change the KMI Onwer to other than classic user as \"([^\"]*)\"$")
	public void user_not_able_to_change_the_KMI_Onwer_to_other_than_classic_user_as(String changeowner) throws Throwable {
		String CO[] = changeowner.split(",");
		changeowner = CommonFunctions.readExcelMasterData(CO[0], CO[1], CO[2]);
		
		ActionHandler.click(AddHandOfDriveToKMIContainer.ChangeOwnerBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on change owner button");
		
		ActionHandler.click(AddHandOfDriveToKMIContainer.OwnerSearchBox);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(AddHandOfDriveToKMIContainer.OwnerSearchBox, changeowner);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(AddHandOfDriveToKMIContainer.OwnerSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(AddHandOfDriveToKMIContainer.OwnerSelection(changeowner))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects different classic owner for an KMI");
		
		ActionHandler.click(AddHandOfDriveToKMIContainer.SaveChangeOwner);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		
		VerifyHandler.verifyElementPresent(AddHandOfDriveToKMIContainer.ErrorMessage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User not able to change the owner to bespoke user for an KMI");
	}
	

	@Then("^Select an existing KMI from Active KMIs list$")
	public void select_an_existing_KMI_from_Active_KMIs_list() throws Throwable {
	    
		ActionHandler.click(AddHandOfDriveToKMIContainer.KMIsListViewDropdon);
		CommonFunctions.attachScreenshot();
		
		VerifyHandler.verifyElementPresent(AddHandOfDriveToKMIContainer.AllActiveKMIsListView);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Verifies 'All Active KMIs' List view on KMIs page");
		
		ActionHandler.click(AddHandOfDriveToKMIContainer.AllActiveKMIsListView);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks 'All Active KMIs' List view");
		
		ActionHandler.click(SVOAdditionalvehicleContainer.FirstOpportunityLink);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects the newly created KMI");
	}

}
