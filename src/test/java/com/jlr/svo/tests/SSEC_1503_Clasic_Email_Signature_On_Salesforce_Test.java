package com.jlr.svo.tests;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.containers.SSEC_1039_SVOEnquiryEmailRemaindersContainer;
import com.jlr.svo.containers.SVOAccountsContainer;
import com.jlr.svo.containers.SVOAdditionalVehicleContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.containers.SVOItemToApproveContainer;
import com.jlr.svo.containers.SVOOpportunityContainer;
import com.jlr.svo.containers.SVO_CustomerResponsesContainer;
import com.jlr.svo.containers.SVO_EnquiryLostReasonContainer;
import com.jlr.svo.containers.SVO_RestrictedPartyScreeningContainer;
import com.jlr.svo.containers.SSEC_1503_Clasic_Email_Signature_On_Salesforce_Container;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.Then;

public class SSEC_1503_Clasic_Email_Signature_On_Salesforce_Test extends TestBaseCC {

	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory
			.getLogger(SSEC_1039_SVOEnquiryEmailRemaindersTest.class.getName());
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);

	SVOAccountsContainer SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
	SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	SVOAdditionalVehicleContainer SVOAdditionalvehicleContainer = PageFactory.initElements(driver,
			SVOAdditionalVehicleContainer.class);
	SVO_RestrictedPartyScreeningContainer RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
			SVO_RestrictedPartyScreeningContainer.class);
	SVOItemToApproveContainer SVOItemToapproveContainer = PageFactory.initElements(driver,
			SVOItemToApproveContainer.class);
	SVO_CustomerResponsesContainer SVO_CustomerResponsesContainer = PageFactory.initElements(driver,
			SVO_CustomerResponsesContainer.class);
	SVO_EnquiryLostReasonContainer SVO_EnquiryLostReasonContainer = PageFactory.initElements(driver,
			SVO_EnquiryLostReasonContainer.class);
	SSEC_1039_SVOEnquiryEmailRemaindersContainer SVOEnquiryEmailRemaindersContainer = PageFactory.initElements(driver,
			SSEC_1039_SVOEnquiryEmailRemaindersContainer.class);
	SSEC_1503_Clasic_Email_Signature_On_Salesforce_Container Clasic_Email_Signature_On_Salesforce_Container = PageFactory.initElements(driver,
			SSEC_1503_Clasic_Email_Signature_On_Salesforce_Container.class);

	public static String verificationCode;
	public static String vCode;
	public static String veriCode;
	public static String EnquiryName;

	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	public void onStart() {

		setupTest("SVOTest");
		driver = getDriver();

		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
		SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
		SVOAdditionalvehicleContainer = PageFactory.initElements(driver, SVOAdditionalVehicleContainer.class);

		RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
				SVO_RestrictedPartyScreeningContainer.class);
		SVOItemToapproveContainer = PageFactory.initElements(driver, SVOItemToApproveContainer.class);
		SVO_CustomerResponsesContainer = PageFactory.initElements(driver, SVO_CustomerResponsesContainer.class);
		SVO_EnquiryLostReasonContainer = PageFactory.initElements(driver, SVO_EnquiryLostReasonContainer.class);
		SVOEnquiryEmailRemaindersContainer = PageFactory.initElements(driver,
				SSEC_1039_SVOEnquiryEmailRemaindersContainer.class);
		Clasic_Email_Signature_On_Salesforce_Container = PageFactory.initElements(driver,
				SSEC_1503_Clasic_Email_Signature_On_Salesforce_Container.class);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		verificationCode = null;
		vCode = null;
		EnquiryName = null;

	}
	
	@Then("^User send an email to customer \"([^\"]*)\" with Business unit \"([^\"]*)\" and subject and verify works legend stock link in email signature$")
	public void user_send_an_email_to_customer_with_Business_unit_and_subject_and_verify_works_legend_stock_link_in_email_signature(String Customer, String BusinessUnit)
			throws Throwable {

		String p[] = BusinessUnit.split(",");
		BusinessUnit = CommonFunctions.readExcelMasterData(p[0], p[1], p[2]);

		String s[] = Customer.split(",");
		Customer = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		ActionHandler.pageDown();
		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(SVO_CustomerResponsesContainer.EmailsSection);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigates to email section on Enquiry page");
		
		ActionHandler.wait(10);
		ActionHandler.scrollToView(SVO_CustomerResponsesContainer.e2aSendEmailButton);
		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(SVO_CustomerResponsesContainer.e2aSendEmailButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on e2a send an email button");

		ActionHandler.wait(5);
		//driver.switchTo().frame(0);
		driver.switchTo().frame(Clasic_Email_Signature_On_Salesforce_Container.EmailFrame);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user navigate to email frame");

		ActionHandler.wait(10);
		ActionHandler.click(SVO_CustomerResponsesContainer.BusinessUnitDropDown);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Business unit drop down list");

		ActionHandler.click(driver.findElement(By.xpath(SVO_CustomerResponsesContainer.BusinessUnitSelection(BusinessUnit))));
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects the " + BusinessUnit + " Business unit from drop down list");

		ActionHandler.scrollDown();
		ActionHandler.wait(2);
		
	    ActionHandler.click(SVO_CustomerResponsesContainer.EmailToTextBox);
		ActionHandler.wait(1);
		ActionHandler.setText(SVO_CustomerResponsesContainer.EmailToTextBox, Customer);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters " + Customer + " in To section of an e2a email");

		ActionHandler.click(SVO_CustomerResponsesContainer.EmailSubjectTextBox);
		ActionHandler.wait(1);
		ActionHandler.setText(SVO_CustomerResponsesContainer.EmailSubjectTextBox, "Please specify the body style?");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters text in subject section of an e2a email");

		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(2);
		
		VerifyHandler.verifyElementPresent(Clasic_Email_Signature_On_Salesforce_Container.WorksLegendsStockLink);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User verifies work legends stock link is present on salesforce");
		
		

		ActionHandler.click(SVO_CustomerResponsesContainer.EmailSendButton);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks on send an email button");

		driver.switchTo().parentFrame();

	}
	
	@Then("^User views works legend stock after clicking on the link from email signature$")
	public void user_views_works_legend_stock_after_clicking_on_the_link_from_email_signature() throws Throwable {
	    

		VerifyHandler.verifyElementPresent(Clasic_Email_Signature_On_Salesforce_Container.WorksLegendsStockLinkOnEmail);
		Reporter.addStepLog("User verifies work legends stock link is present on Email");
		
		javaScriptUtil.clickElementByJS(Clasic_Email_Signature_On_Salesforce_Container.WorksLegendsStockLinkOnEmail);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks on work legends stock link on email");
		
		String NewWindow = driver.getWindowHandle();
		Set<String> NewWindows = driver.getWindowHandles();
		Iterator<String> i = NewWindows.iterator();
		while (i.hasNext()) {
			String Window = i.next();
			if (!NewWindow.equalsIgnoreCase(Window)) {
				driver.switchTo().window(Window);
			}
		}
		

		VerifyHandler.verifyElementPresent(Clasic_Email_Signature_On_Salesforce_Container.WorksLegendsStockAvailableNumber);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies available work legends stock");
		
	}
	
	@Then("^User send an email to customer \"([^\"]*)\" with Business unit \"([^\"]*)\" and subject and verify works legend stock link is not present on email signature$")
	public void user_send_an_email_to_customer_with_Business_unit_and_subject_and_verify_works_legend_stock_link_is__nt_present_on_email_signature(String Customer, String BusinessUnit)
			throws Throwable {

		String p[] = BusinessUnit.split(",");
		BusinessUnit = CommonFunctions.readExcelMasterData(p[0], p[1], p[2]);

		String s[] = Customer.split(",");
		Customer = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		ActionHandler.pageDown();
		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(SVO_CustomerResponsesContainer.EmailsSection);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigates to email section on Enquiry page");
		
		ActionHandler.wait(10);
		ActionHandler.scrollToView(SVO_CustomerResponsesContainer.e2aSendEmailButton);
		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(SVO_CustomerResponsesContainer.e2aSendEmailButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on e2a send an email button");

		ActionHandler.wait(5);
		//driver.switchTo().frame(0);
		driver.switchTo().frame(Clasic_Email_Signature_On_Salesforce_Container.EmailFrame);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user navigate to email frame");

		ActionHandler.wait(10);
		ActionHandler.click(SVO_CustomerResponsesContainer.BusinessUnitDropDown);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Business unit drop down list");

		ActionHandler.click(driver.findElement(By.xpath(SVO_CustomerResponsesContainer.BusinessUnitSelection(BusinessUnit))));
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects the " + BusinessUnit + " Business unit from drop down list");

		ActionHandler.scrollDown();
		ActionHandler.wait(2);
		
	    ActionHandler.click(SVO_CustomerResponsesContainer.EmailToTextBox);
		ActionHandler.wait(1);
		ActionHandler.setText(SVO_CustomerResponsesContainer.EmailToTextBox, Customer);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters " + Customer + " in To section of an e2a email");

		ActionHandler.click(SVO_CustomerResponsesContainer.EmailSubjectTextBox);
		ActionHandler.wait(1);
		ActionHandler.setText(SVO_CustomerResponsesContainer.EmailSubjectTextBox, "Please specify the body style?");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters text in subject section of an e2a email");

		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(2);
		
		if(!VerifyHandler.verifyElementPresent(Clasic_Email_Signature_On_Salesforce_Container.WorksLegendsStockLink))
		{
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User verifies work legends stock link is not present on salesforce for bespoke enquiry");
		}
		else {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User verifies work legends stock link is present on salesforce");
		}
		

		ActionHandler.click(SVO_CustomerResponsesContainer.EmailSendButton);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks on send an email button");

		driver.switchTo().parentFrame();

	}
	
	@Then("^works legend stock link is not available in received email$")
	public void works_legend_stock_link_is_not_available_in_received_email() throws Throwable {
	   
		if(!VerifyHandler.verifyElementPresent(Clasic_Email_Signature_On_Salesforce_Container.WorksLegendsStockLinkOnEmail))
		{
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User verifies work legends stock link is not present on received email");
		}
		else {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User verifies work legends stock link is present on received email");
		}
		
	}
	
	@Then("^User send an email to customer \"([^\"]*)\" without Business unit and subject and verify works legend stock link is not present on email signature$")
	public void user_send_an_email_to_customer_without_Business_unit_and_subject_and_verify_works_legend_stock_link_is_not_present_on_email_signature(String Customer)
			throws Throwable {

		String s[] = Customer.split(",");
		Customer = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		ActionHandler.pageDown();
		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(SVO_CustomerResponsesContainer.EmailsSection);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigates to email section on Enquiry page");
		
		ActionHandler.wait(10);
		ActionHandler.scrollToView(SVO_CustomerResponsesContainer.e2aSendEmailButton);
		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(SVO_CustomerResponsesContainer.e2aSendEmailButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on e2a send an email button");

		ActionHandler.wait(5);
		//driver.switchTo().frame(0);
		driver.switchTo().frame(Clasic_Email_Signature_On_Salesforce_Container.EmailFrame);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user navigate to email frame");

		ActionHandler.wait(10);
		ActionHandler.scrollDown();
		ActionHandler.wait(2);
		
	    ActionHandler.click(SVO_CustomerResponsesContainer.EmailToTextBox);
		ActionHandler.wait(1);
		ActionHandler.setText(SVO_CustomerResponsesContainer.EmailToTextBox, Customer);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters " + Customer + " in To section of an e2a email");

		ActionHandler.click(Clasic_Email_Signature_On_Salesforce_Container.SubjectFieldWithotBuisinessUnit);
		ActionHandler.wait(1);
		ActionHandler.setText(Clasic_Email_Signature_On_Salesforce_Container.SubjectFieldWithotBuisinessUnit, "Please specify the body style?");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters text in subject section of an e2a email");

		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(2);
		
		if(!VerifyHandler.verifyElementPresent(Clasic_Email_Signature_On_Salesforce_Container.WorksLegendsStockLink))
		{
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User verifies work legends stock link is not present on salesforce if buisiness unit is not selected");
		}
		else {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User verifies work legends stock link is present on salesforce if buisiness unit is not selected");
		}
		

		ActionHandler.click(SVO_CustomerResponsesContainer.EmailSendButton);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks on send an email button");

		driver.switchTo().parentFrame();

	}
	
	@Then("^Logout from Outlook account$")
	public void logout_from_Outlook_account() throws Throwable {
		driver.close();
		ActionHandler.wait(2);
		driver.quit();
	}


}
