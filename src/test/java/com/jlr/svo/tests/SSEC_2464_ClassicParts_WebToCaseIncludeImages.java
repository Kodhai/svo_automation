package com.jlr.svo.tests;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.containers.SSEC_1225_CaseCreationWebtoCaseContainer;
import com.jlr.svo.containers.SSEC_1238_CaseLifecycle_Container;
import com.jlr.svo.containers.SSEC_2464_ClassicParts_WebToCaseIncludeImagesContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class SSEC_2464_ClassicParts_WebToCaseIncludeImages extends TestBaseCC {
	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory
			.getLogger(SSEC_1228_LinkContactAndAccountTocase_Test.class.getName());
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);

	
	SSEC_1225_CaseCreationWebtoCaseContainer CaseCreationWebtoCaseContainer = PageFactory.initElements(driver,
			SSEC_1225_CaseCreationWebtoCaseContainer.class);
	SSEC_2464_ClassicParts_WebToCaseIncludeImagesContainer ClassicParts_WebToCaseIncludeImagesContainer = PageFactory.initElements(driver, SSEC_2464_ClassicParts_WebToCaseIncludeImagesContainer.class);
	SSEC_1238_CaseLifecycle_Container CaseLifecycle_Container = PageFactory.initElements(driver,
			SSEC_1238_CaseLifecycle_Container.class);
	
	public static String verificationCode;
	public static String vCode;
	public static String veriCode;
	public String NewWindow;
	public static String CaseCreated;
	public static String CaseNumber;
	public static String ContactName;

	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	public void onStart() {

		setupTest("SVOTest");
		driver = getDriver();

		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		
		CaseCreationWebtoCaseContainer = PageFactory.initElements(driver,
				SSEC_1225_CaseCreationWebtoCaseContainer.class);	
		
		ClassicParts_WebToCaseIncludeImagesContainer = PageFactory.initElements(driver, SSEC_2464_ClassicParts_WebToCaseIncludeImagesContainer.class);
		CaseLifecycle_Container = PageFactory.initElements(driver, SSEC_1238_CaseLifecycle_Container.class);

		driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);

		verificationCode = null;
		vCode = null;
		veriCode = null;
		NewWindow = null;
		CaseCreated = null;
		CaseNumber = null;
		ContactName = null;

	}
	
	@Given("^Access to the Classic Parts Contact Us portal$")
	public void access_to_the_Classic_Parts_portal() throws Throwable {
		onStart();

		//driver.get("https://" + "iweb_26b06a" + ":" + "PJuR7ifMuPqr" + "@" + Constants.ClassicParts);
		driver.get("https://iweb_26b06a:PJuR7ifMuPqr@partsdev.jaguarlandroverclassic.com/contact");

		ActionHandler.wait(1);
		Reporter.addStepLog("User access to Classic Parts portal");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(CaseLifecycle_Container.AllowCookies);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User accepts cookie policies");

	}
	
    @Then("^User is able to upload images on contact us form$")
    public void User_verifies_Jaguar_LandRoverClassic_description() throws Exception{
    	
    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	
    	ActionHandler.click(ClassicParts_WebToCaseIncludeImagesContainer.ChooseFileBtn);
    	CommonFunctions.attachScreenshot();
    	ActionHandler.wait(1);
    	Reporter.addStepLog("Click on Choose File Button");
    }
    
 // upload file
    public void uploadFile(String file) throws IOException {
        ActionHandler.wait(10);
        Runtime.getRuntime().exec(System.getProperty("user.dir") + "/upload.exe" + " " + System.getProperty("user.dir")
                + "\\Files\\" + file);
        CommonFunctions.attachScreenshot();
    }
    
    @Then("^select a file and click on upload button$")
    public void select_a_file_and_click_on_upload_Files() throws Throwable {
        
        uploadFile("sampleimg.jpg");
        ActionHandler.wait(60);
        CommonFunctions.attachScreenshot();

        ActionHandler.pressEnter();
        ActionHandler.wait(6);
        Reporter.addStepLog("User clicks on done button");
        CommonFunctions.attachScreenshot();
    }
    
    @And("^Click on Delete Image in the Contact Us form$")
    public void Click_on_DeleteImage_in_the_ContactUs_form() throws Exception{
    	
    	ActionHandler.click(ClassicParts_WebToCaseIncludeImagesContainer.DeleteImageBtn);
    	ActionHandler.wait(3);
        Reporter.addStepLog("User clicks on Delete Image button");
        CommonFunctions.attachScreenshot();
        
    }
    
    @And("^Verify the captcha in the contact us form$")
    public void Verify_Captcha_in_the_ContactUs_form() throws Exception{
    	
    	driver.switchTo().frame(0);
    	
    	ActionHandler.click(ClassicParts_WebToCaseIncludeImagesContainer.Captcha);
    	ActionHandler.wait(6);
        Reporter.addStepLog("User clicks on Delete Image button");
        CommonFunctions.attachScreenshot();
        
        driver.switchTo().parentFrame();
        
    }

    @And("^User verify all four options available after the submit button on the contact form$")
    public void verify_all_four_options_available_after_the_submit_button_on_the_contact_form() throws Exception {

 


        if (VerifyHandler.verifyElementPresent(ClassicParts_WebToCaseIncludeImagesContainer.GenuineParts)) {
            CommonFunctions.attachScreenshot();
            Reporter.addStepLog("User verify the geniune parts option");
        }

 

        if (VerifyHandler.verifyElementPresent(ClassicParts_WebToCaseIncludeImagesContainer.TailorMadeandBespoke)) {
            CommonFunctions.attachScreenshot();
            Reporter.addStepLog("User verify the Tailer made and Bespoke option");
        }

        if (VerifyHandler.verifyElementPresent(ClassicParts_WebToCaseIncludeImagesContainer.PartsTechnicalAdvice)) {
            CommonFunctions.attachScreenshot();
            Reporter.addStepLog("User verify the Parts and Technical Advice option");
        }
        if (VerifyHandler.verifyElementPresent(ClassicParts_WebToCaseIncludeImagesContainer.GlobalDelivery)) {
            CommonFunctions.attachScreenshot();
            Reporter.addStepLog("User verify the Global Delivery option");
        }

 

    }


}
