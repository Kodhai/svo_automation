package com.jlr.svo.tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.containers.SSEC_1238_CaseLifecycle_Container;
import com.jlr.svo.containers.SVOAccountsContainer;
import com.jlr.svo.containers.SVOAdditionalVehicleContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.containers.SVOItemToApproveContainer;
import com.jlr.svo.containers.SVOOpportunityContainer;
import com.jlr.svo.containers.SVO_CustomerResponsesContainer;
import com.jlr.svo.containers.SSEC_1230_Case_Assignments_Rules_Container;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SSEC_1230_Case_Assignments_Rules_Test extends TestBaseCC {

	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory.getLogger(SSEC_1238_CaseLifecycle_Test.class.getName());
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);

	SVOAccountsContainer SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
	SVOItemToApproveContainer SVOItemToapproveContainer = PageFactory.initElements(driver,
			SVOItemToApproveContainer.class);
	SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	SVOAdditionalVehicleContainer SVOAdditionalvehicleContainer = PageFactory.initElements(driver,
			SVOAdditionalVehicleContainer.class);
	SSEC_1238_CaseLifecycle_Container CaseLifecycle_Container = PageFactory.initElements(driver,
			SSEC_1238_CaseLifecycle_Container.class);
	SVO_CustomerResponsesContainer SVO_CustomerResponsesContainer = PageFactory.initElements(driver,
			SVO_CustomerResponsesContainer.class);
	SSEC_1230_Case_Assignments_Rules_Container Case_Assignments_Rules_Container = PageFactory.initElements(driver,
			SSEC_1230_Case_Assignments_Rules_Container.class);

	public static String verificationCode;
	public static String vCode;
	public static String veriCode;
	public static String CaseNumber;

	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	public void onStart() {

		setupTest("SVOTest");
		driver = getDriver();
		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		CaseLifecycle_Container = PageFactory.initElements(driver, SSEC_1238_CaseLifecycle_Container.class);
		SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
		SVOItemToapproveContainer = PageFactory.initElements(driver, SVOItemToApproveContainer.class);
		SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
		SVOAdditionalvehicleContainer = PageFactory.initElements(driver, SVOAdditionalVehicleContainer.class);
		SVO_CustomerResponsesContainer = PageFactory.initElements(driver, SVO_CustomerResponsesContainer.class);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		verificationCode = null;
		vCode = null;
		veriCode = null;
		CaseNumber = null;

	}
	
	@When("^Verifies Classic parts and technical case under parts and technical queue$")
	public void erifies_Classic_parts_and_technical_case_under_parts_and_technical_queue() throws Throwable {

		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(CaseLifecycle_Container.CasesTab);
		ActionHandler.wait(1);
		Reporter.addStepLog("User navigates to cases tab");

		ActionHandler.wait(2);
		ActionHandler.click(SVO_CustomerResponsesContainer.SelectOpportunityListView);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on select list view tab");

		ActionHandler.click(Case_Assignments_Rules_Container.SelectPartsAndTechnicalCaseQueue);
		ActionHandler.wait(3);
		Reporter.addStepLog("User chooses to view selected to parts and technical case list");
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User Verifies that the new Classic parts and technical case is created automatically on SVO Portal");

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstOpportunityLink);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verifies Classic parts and technical case under parts and technical queue");
	}
	
	@When("^Verifies Classic customer service case under customer service queue$")
	public void Verifies_Classic_customer_service_case_under_customer_service_queue() throws Throwable {

		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(CaseLifecycle_Container.CasesTab);
		ActionHandler.wait(1);
		Reporter.addStepLog("User navigates to cases tab");

		ActionHandler.wait(2);
		ActionHandler.click(SVO_CustomerResponsesContainer.SelectOpportunityListView);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on select list view tab");

		ActionHandler.click(Case_Assignments_Rules_Container.SelectCustomerServiceCaseQueue);
		ActionHandler.wait(3);
		Reporter.addStepLog("User chooses to view selected to parts and technical case list");
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User Verifies that the new Classic customer service case is created automatically on SVO Portal");

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstOpportunityLink);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verifies Classic customer service case under customer service queue");
		
		
	}
	
	@When("^Verifies Classic customer service case is not present under parts and technical queue$")
	public void verifies_Classic_customer_service_case_is_not_present_under_parts_and_technical_queue() throws Throwable {

		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(CaseLifecycle_Container.CasesTab);
		ActionHandler.wait(1);
		Reporter.addStepLog("User navigates to cases tab");

		ActionHandler.wait(2);
		ActionHandler.click(SVO_CustomerResponsesContainer.SelectOpportunityListView);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on select list view tab");

		ActionHandler.click(Case_Assignments_Rules_Container.SelectPartsAndTechnicalCaseQueue);
		ActionHandler.wait(3);
		Reporter.addStepLog("User chooses to view selected to parts and technical case list");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstOpportunityLink);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		
		
		VerifyHandler.verifyElementPresent(Case_Assignments_Rules_Container.CaseRecordType);
		ActionHandler.wait(3);
		String CaseRecordType = Case_Assignments_Rules_Container.CaseRecordType.getText();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verifies Classic" + CaseRecordType + "case is not present under parts and technical queue");
		
	}
	
	@Then("^Submit Contact Us form with classic parts and technical jaguar enquiry Email \"([^\"]*)\" First name \"([^\"]*)\" Last name \"([^\"]*)\" Country \"([^\"]*)\" Phone number \"([^\"]*)\" VIN number \"([^\"]*)\"$")
	public void Submit_Contact_Us_form_with_classic_parts_and_technical_jaguar_enquiry(String email, String first, String last, String country, String phone,
			String vin) throws Throwable {

		String em[] = email.split(",");
		email = CommonFunctions.readExcelMasterData(em[0], em[1], em[2]);

		String fname[] = first.split(",");
		first = CommonFunctions.readExcelMasterData(fname[0], fname[1], fname[2]);

		String lname[] = last.split(",");
		last = CommonFunctions.readExcelMasterData(lname[0], lname[1], lname[2]);

		String cty[] = country.split(",");
		country = CommonFunctions.readExcelMasterData(cty[0], cty[1], cty[2]);

		String num[] = phone.split(",");
		phone = CommonFunctions.readExcelMasterData(num[0], num[1], num[2]);

		String v[] = vin.split(",");
		vin = CommonFunctions.readExcelMasterData(v[0], v[1], v[2]);
		
		String EnqSubject = "Enquiry submission through web_";
		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		// enter first name
		ActionHandler.setText(CaseLifecycle_Container.FirstNameTxtBx, first);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseLifecycle_Container.FirstNameTxtBx, first);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters First name as : " + first);

		// enter last name
		ActionHandler.setText(CaseLifecycle_Container.LastNameTxtBx, last);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Last name as : " + last);

		ActionHandler.wait(1);
		ActionHandler.setText(CaseLifecycle_Container.InputEmailAddress, email);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseLifecycle_Container.InputEmailAddress, email);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters email as : " + email);

		// enter phone number
		ActionHandler.setText(CaseLifecycle_Container.PhoneNumberTxtBx, phone);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		Select select = new Select(driver.findElement(By.xpath(CaseLifecycle_Container.SelectHowCanHelp())));
		select.selectByVisibleText("Parts & Technical Jaguar Enquiry");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		
		Reporter.addStepLog("User clicks on Help select");

		
		ActionHandler.setText(CaseLifecycle_Container.VINNumber, vin);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters the vin number");

		ActionHandler.setText(CaseLifecycle_Container.CommentText, EnqSubject + randomNumber);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters the comment text");

		// click on continue
		javaScriptUtil.clickElementByJS(CaseLifecycle_Container.SubmitBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Submit");

		Reporter.addStepLog("User verifies that contact us form submitted successfully");
	}
	
	@Then("^Submit Contact Us form with classic parts and technical landrover enquiry Email \"([^\"]*)\" First name \"([^\"]*)\" Last name \"([^\"]*)\" Country \"([^\"]*)\" Phone number \"([^\"]*)\" VIN number \"([^\"]*)\"$")
	public void Submit_Contact_Us_form_with_classic_parts_and_technical_landrover_enquiry(String email, String first, String last, String country, String phone,
			String vin) throws Throwable {

		String em[] = email.split(",");
		email = CommonFunctions.readExcelMasterData(em[0], em[1], em[2]);

		String fname[] = first.split(",");
		first = CommonFunctions.readExcelMasterData(fname[0], fname[1], fname[2]);

		String lname[] = last.split(",");
		last = CommonFunctions.readExcelMasterData(lname[0], lname[1], lname[2]);

		String cty[] = country.split(",");
		country = CommonFunctions.readExcelMasterData(cty[0], cty[1], cty[2]);

		String num[] = phone.split(",");
		phone = CommonFunctions.readExcelMasterData(num[0], num[1], num[2]);

		String v[] = vin.split(",");
		vin = CommonFunctions.readExcelMasterData(v[0], v[1], v[2]);
		
		String EnqSubject = "Enquiry submission through web_";
		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		// enter first name
		ActionHandler.setText(CaseLifecycle_Container.FirstNameTxtBx, first);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseLifecycle_Container.FirstNameTxtBx, first);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters First name as : " + first);

		// enter last name
		ActionHandler.setText(CaseLifecycle_Container.LastNameTxtBx, last);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Last name as : " + last);

		ActionHandler.wait(1);
		ActionHandler.setText(CaseLifecycle_Container.InputEmailAddress, email);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseLifecycle_Container.InputEmailAddress, email);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters email as : " + email);

		// enter phone number
		ActionHandler.setText(CaseLifecycle_Container.PhoneNumberTxtBx, phone);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		Select select = new Select(driver.findElement(By.xpath(CaseLifecycle_Container.SelectHowCanHelp())));
		select.selectByVisibleText("Parts & Technical Land Rover Enquiry");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Help select");

		ActionHandler.setText(CaseLifecycle_Container.VINNumber, vin);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters the vin number");

		ActionHandler.setText(CaseLifecycle_Container.CommentText, EnqSubject + randomNumber);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters the comment text");

		// click on continue
		javaScriptUtil.clickElementByJS(CaseLifecycle_Container.SubmitBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Submit");

		Reporter.addStepLog("User verifies that contact us form submitted successfully");
	}
	
	@Then("^Submit Contact Us form with classic customer service enquiry Email \"([^\"]*)\" First name \"([^\"]*)\" Last name \"([^\"]*)\" Country \"([^\"]*)\" Phone number \"([^\"]*)\" VIN number \"([^\"]*)\"$")
	public void Submit_Contact_Us_form_with_classic_customer_service_enquiry(String email, String first, String last, String country, String phone,
			String vin) throws Throwable {

		String em[] = email.split(",");
		email = CommonFunctions.readExcelMasterData(em[0], em[1], em[2]);

		String fname[] = first.split(",");
		first = CommonFunctions.readExcelMasterData(fname[0], fname[1], fname[2]);

		String lname[] = last.split(",");
		last = CommonFunctions.readExcelMasterData(lname[0], lname[1], lname[2]);

		String cty[] = country.split(",");
		country = CommonFunctions.readExcelMasterData(cty[0], cty[1], cty[2]);

		String num[] = phone.split(",");
		phone = CommonFunctions.readExcelMasterData(num[0], num[1], num[2]);

		String v[] = vin.split(",");
		vin = CommonFunctions.readExcelMasterData(v[0], v[1], v[2]);
		
		String EnqSubject = "Enquiry submission through web_";
		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		// enter first name
		ActionHandler.setText(CaseLifecycle_Container.FirstNameTxtBx, first);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseLifecycle_Container.FirstNameTxtBx, first);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters First name as : " + first);

		// enter last name
		ActionHandler.setText(CaseLifecycle_Container.LastNameTxtBx, last);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Last name as : " + last);

		ActionHandler.wait(1);
		ActionHandler.setText(CaseLifecycle_Container.InputEmailAddress, email);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseLifecycle_Container.InputEmailAddress, email);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters email as : " + email);

		// enter phone number
		ActionHandler.setText(CaseLifecycle_Container.PhoneNumberTxtBx, phone);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		Select select = new Select(driver.findElement(By.xpath(CaseLifecycle_Container.SelectHowCanHelp())));
		select.selectByVisibleText("Customer Service Enquiry");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Help select");

		
		ActionHandler.setText(CaseLifecycle_Container.VINNumber, vin);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters the vin number");

		ActionHandler.setText(CaseLifecycle_Container.CommentText, EnqSubject + randomNumber);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters the comment text");

		// click on continue
		javaScriptUtil.clickElementByJS(CaseLifecycle_Container.SubmitBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Submit");

		Reporter.addStepLog("User verifies that contact us form submitted successfully");
	}
	
	@Then("^Submit Contact Us form with Other Email \"([^\"]*)\" First name \"([^\"]*)\" Last name \"([^\"]*)\" Country \"([^\"]*)\" Phone number \"([^\"]*)\" VIN number \"([^\"]*)\"$")
	public void Submit_Contact_Us_form_with_Other(String email, String first, String last, String country, String phone,
			String vin) throws Throwable {

		String em[] = email.split(",");
		email = CommonFunctions.readExcelMasterData(em[0], em[1], em[2]);

		String fname[] = first.split(",");
		first = CommonFunctions.readExcelMasterData(fname[0], fname[1], fname[2]);

		String lname[] = last.split(",");
		last = CommonFunctions.readExcelMasterData(lname[0], lname[1], lname[2]);

		String cty[] = country.split(",");
		country = CommonFunctions.readExcelMasterData(cty[0], cty[1], cty[2]);

		String num[] = phone.split(",");
		phone = CommonFunctions.readExcelMasterData(num[0], num[1], num[2]);

		String v[] = vin.split(",");
		vin = CommonFunctions.readExcelMasterData(v[0], v[1], v[2]);
		
		String EnqSubject = "Enquiry submission through web_";
		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		// enter first name
		ActionHandler.setText(CaseLifecycle_Container.FirstNameTxtBx, first);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseLifecycle_Container.FirstNameTxtBx, first);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters First name as : " + first);

		// enter last name
		ActionHandler.setText(CaseLifecycle_Container.LastNameTxtBx, last);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Last name as : " + last);

		ActionHandler.wait(1);
		ActionHandler.setText(CaseLifecycle_Container.InputEmailAddress, email);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseLifecycle_Container.InputEmailAddress, email);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters email as : " + email);

		// enter phone number
		ActionHandler.setText(CaseLifecycle_Container.PhoneNumberTxtBx, phone);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		Select select = new Select(driver.findElement(By.xpath(CaseLifecycle_Container.SelectHowCanHelp())));
		select.selectByVisibleText("Other");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		
		Reporter.addStepLog("User clicks on Help select");

		
		ActionHandler.setText(CaseLifecycle_Container.VINNumber, vin);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters the vin number");

		ActionHandler.setText(CaseLifecycle_Container.CommentText, EnqSubject + randomNumber);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters the comment text");

		// click on continue
		javaScriptUtil.clickElementByJS(CaseLifecycle_Container.SubmitBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Submit");

		Reporter.addStepLog("User verifies that contact us form submitted successfully");
	}
	
	@When("^Verifies Classic parts and technical case is not present under customer service queue$")
	public void verifies_Classic_parts_and_technical_case_is_not_present_under_customer_service_queue() throws Throwable {

		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(CaseLifecycle_Container.CasesTab);
		ActionHandler.wait(1);
		Reporter.addStepLog("User navigates to cases tab");

		ActionHandler.wait(2);
		ActionHandler.click(SVO_CustomerResponsesContainer.SelectOpportunityListView);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on select list view tab");

		ActionHandler.click(Case_Assignments_Rules_Container.SelectCustomerServiceCaseQueue);
		ActionHandler.wait(3);
		Reporter.addStepLog("User chooses to view selected to parts and technical case list");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstOpportunityLink);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		
		
		VerifyHandler.verifyElementPresent(Case_Assignments_Rules_Container.CaseRecordType);
		ActionHandler.wait(3);
		String CaseRecordType = Case_Assignments_Rules_Container.CaseRecordType.getText();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verifies Classic" + CaseRecordType + "case is not present under customer service queue");
		
	}



}
