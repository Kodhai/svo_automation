package com.jlr.svo.tests;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.AssertHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.autotest.utils.Config;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.constants.Constants;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SVOEnquiry extends TestBaseCC {
	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory.getLogger(SVOEnquiry.class.getName());
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
	public static String verificationCode;
	public static String vCode;
	public static String veriCode;
	public static String Rnumber;

	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	// onStart
	public void onStart() {
		setupTest("SVOTest");
		driver = getDriver();
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		verificationCode = null;
		vCode = null;
		Rnumber = null;
	}

	public void getDriver_SVO() {
		driver = getDriver();
	}

	public String checkEmail() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		ActionHandler.wait(5);
		// onStart();
		driver.get(Constants.outlookURL);
		ActionHandler.wait(10);
		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookIcon)) {
			ActionHandler.wait(2);
			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			ActionHandler.wait(7);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			ActionHandler.wait(7);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			ActionHandler.wait(7);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			ActionHandler.wait(7);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			ActionHandler.wait(7);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(30);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);
				ActionHandler.wait(20);
				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(337, 343);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	// navigateToSVO
	public void navigateToSVO() throws Exception {
		String SVO = "Special Vehicle Operations";
		ActionHandler.wait(3);
		Reporter.addStepLog("User tries to navigate to SVO Portal");
		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.jlrText)) {
			ActionHandler.wait(2);
			ActionHandler.click(SVOEnquiryContainer.menuBtn);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.searchBox, SVO);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.selectMenu(SVO))));
			ActionHandler.wait(10);
			CommonFunctions.attachScreenshot();

			Reporter.addStepLog("User navigate to SVO Portal");
		}
	}

	public void homePage() throws Exception {
		driver = getDriver();
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);

		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(SVOEnquiryContainer.homePage);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Home Page");
	}

	// ListView
	public void ListView() throws Exception {
		ActionHandler.wait(1);
		ActionHandler.click(SVOEnquiryContainer.listView);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.allOpportunity);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("All Opportunities are opened");
	}

	// NewEnquiry SVO_Bespoke
	public void NewEnquiry_SVO_Bespoke() throws Exception {
		String recordType = "SVO Bespoke";
		String enquiryTitle = "New_SVO_Enquiry_";
		String prodOffering = "Full Bespoke";
		String contact = Constants.Contact_two;
		String Region = "Europe";
		String SalesArea = "Euro Importer";
		String Market = "Balkans";
		String Brand = "Land Rover";
		String Model = "Discovery";
		String Source = "Email";
		String OriginatingDiv = "Bespoke";

		ActionHandler.wait(3);
		Reporter.addStepLog("User Creates new enquiry");
		ActionHandler.click(SVOEnquiryContainer.newEnquiryBtn);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User Needs to select any record type option");
		ActionHandler.wait(3);
		// ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.selectRecordType(recordType))));
		Reporter.addStepLog("User Select required Record Type");
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(4);
		ActionHandler.click(SVOEnquiryContainer.nextRType);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigate to enter enquiry details page");

		double randomNumber = getRandomIntegerBetweenRange(0, 1000);
		ActionHandler.wait(7);
		ActionHandler.setText(SVOEnquiryContainer.enquiryTitle, enquiryTitle + randomNumber);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Enquiry Title");

		ActionHandler.wait(1);
		ActionHandler.click(SVOEnquiryContainer.newEnqProOff);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.ValueSelection(prodOffering))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Input product offering information");

		ActionHandler.scrollToView(SVOEnquiryContainer.newEnqClient);
		ActionHandler.wait(3);
		ActionHandler.setText(SVOEnquiryContainer.newEnqClient, contact);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOEnquiryContainer.newEnqClientSearch);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.clientNameSearch(contact))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Input client information");

		ActionHandler.setText(SVOEnquiryContainer.newEnqPreferredCnt, contact);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOEnquiryContainer.newEnqPreferredCntSearch);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.clientNameSearch(contact))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Input Preferred Contact information");

		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOEnquiryContainer.newEnqRegion);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.ValueSelection(Region))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter Region information");

		ActionHandler.click(SVOEnquiryContainer.newEnqSalesArea);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.ValueSelection(SalesArea))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter Sales Area information");

		ActionHandler.click(SVOEnquiryContainer.newEnqMarket);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.ValueSelection(Market))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter market information");

		/*
		 * ActionHandler.scrollToView(SVOEnquiryContainer.retailer);
		 * ActionHandler.wait(3);
		 * ActionHandler.setText(SVOEnquiryContainer.retailer,contact);
		 * ActionHandler.wait(3); CommonFunctions.attachScreenshot();
		 * ActionHandler.click(SVOEnquiryContainer.newEnqPreferredRetailerSearch);
		 * ActionHandler.wait(3); CommonFunctions.attachScreenshot();
		 * ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.
		 * brandSearch(contact)))); ActionHandler.wait(3);
		 * Reporter.addStepLog("Enter Preferred Retailer information");
		 * CommonFunctions.attachScreenshot();
		 */

		ActionHandler.scrollToView(SVOEnquiryContainer.newEnqBrand);
		ActionHandler.wait(3);
		ActionHandler.setText(SVOEnquiryContainer.newEnqBrand, Brand);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOEnquiryContainer.newEnqBrandSearch);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.brandSearch(Brand))));
		ActionHandler.wait(3);
		Reporter.addStepLog("Enter brand information");
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.newEnqModel, Model);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOEnquiryContainer.newEnqModelSearch);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.brandSearch(Model))));
		ActionHandler.wait(3);
		Reporter.addStepLog("Enter model information");
		CommonFunctions.attachScreenshot();

		ActionHandler.scrollToView(SVOEnquiryContainer.newEnqSource);
		ActionHandler.wait(3);
		ActionHandler.click(SVOEnquiryContainer.newEnqSource);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.ValueSelection(Source))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Input source information");

		ActionHandler.wait(3);
		ActionHandler.click(SVOEnquiryContainer.newEnqOriginatingDiv);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.ValueSelection(OriginatingDiv))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Input Originating Divison information");

		ActionHandler.click(SVOEnquiryContainer.saveEnquiry);
		ActionHandler.wait(9);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves Enquiry");
	}

	// newEnquiry SLA
	public void newEnquiry_SLA() throws Throwable {
		String recordType = "SVO Bespoke";
		String enquiryTitle = "Test_Enquiry_for_SLA";
		String Email = Constants.email_two;

		ActionHandler.wait(4);
		Reporter.addStepLog("User Creates new enquiry");
		ActionHandler.click(SVOEnquiryContainer.newEnquiryBtn);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User Needs to select any record type option");
		ActionHandler.wait(3);
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.selectRecordType(recordType))));
		Reporter.addStepLog("User Select required Record Type");
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(4);
		ActionHandler.click(SVOEnquiryContainer.nextRType);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigate to enter enquiry details page");

		ActionHandler.wait(7);
		ActionHandler.setText(SVOEnquiryContainer.enquiryTitle, enquiryTitle);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		System.out.println("User has entered Enquiry Title");

		ActionHandler.click(SVOEnquiryContainer.saveEnquiry);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves Enquiry");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.nextRType)) {
			ActionHandler.wait(3);
			ActionHandler.click(SVOEnquiryContainer.cancelRType);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
		}

		ActionHandler.wait(4);
	}

	public void backToHome() throws Exception {
		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.backtoHome);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate back to home");
	}

	@Given("^Access SVO Portal as a user having \"([^\"]*)\" Role and UserName \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void Access_SVO_Portal_User_having_role(String userRole, String userName, String password) throws Exception {
		String ur[] = userRole.split(",");
		userRole = CommonFunctions.readExcelMasterData(ur[0], ur[1], ur[2]);

		String uname[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(uname[0], uname[1], uname[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");
		onStart();
		driver = getDriver();
		ActionHandler.wait(13);
		driver.get(Constants.SVOURL);
		ActionHandler.wait(15);
		Reporter.addStepLog("User Logins to SVO Portal");

		if ((userRole.equals("System Admin User")) || (userRole.equals("Classic Sales User"))
				|| (userRole.equals("SO Data Manager User")) || (userRole.equals("Super User"))
				|| (userRole.equals("Classic Operation User"))
				|| (userRole.equals("Bespoke User") || (userRole.equals("Private Office User"))
						|| (userRole.equals("Bespoke Design User")) || (userRole.equals("Bespoke Operation User"))
						|| (userRole.equals("Classic Operation User")) || (userRole.equals("Classic Finance User"))
						|| (userRole.equals("Classic Service User")) || (userRole.equals("Classic Admin User")))) {
			Reporter.addStepLog("User selects logins with specific user role");
			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			ActionHandler.wait(7);
			CommonFunctions.attachScreenshot();
		}

		Reporter.addStepLog("User requires to enter Verification code received in an email");
		ActionHandler.wait(5);

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");
			ActionHandler.wait(10);
			vCode = checkEmail();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			ActionHandler.wait(15);
			Reporter.addStepLog("User Logins to SVO Portal");

			if ((userRole.equals("System Admin User")) || (userRole.equals("Classic Sales User"))
					|| (userRole.equals("SO Data Manager User")) || (userRole.equals("Super User"))
					|| (userRole.equals("Classic Operation User"))
					|| (userRole.equals("Bespoke User") || (userRole.equals("Private Office User"))
							|| (userRole.equals("Bespoke Design User")) || (userRole.equals("Bespoke Operation User"))
							|| (userRole.equals("Classic Operation User")) || (userRole.equals("Classic Finance User"))
							|| (userRole.equals("Classic Service User")) || (userRole.equals("Classic Admin User")))) {
				Reporter.addStepLog("User selects logins with specific user role");
				ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
				ActionHandler.wait(3);
				CommonFunctions.attachScreenshot();

				ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
				ActionHandler.wait(3);
				CommonFunctions.attachScreenshot();

				ActionHandler.click(SVOEnquiryContainer.loginBtn);
				ActionHandler.wait(7);
				CommonFunctions.attachScreenshot();
			}

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(2);

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	@Given("^Access SVO Portal as a user having \"([^\"]*)\"$")
	public void Access_SVO_Portal_having_User(String userRole) throws Exception {
		String userName = null;
		String password = null;

		String ur[] = userRole.split(",");
		userRole = CommonFunctions.readExcelMasterData(ur[0], ur[1], ur[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");
		onStart();
		driver = getDriver();
		ActionHandler.wait(13);
		driver.get(Constants.SVOURL);
		ActionHandler.wait(15);
		Reporter.addStepLog("User Logins to SVO Portal");

		if (userRole.equals("Admin")) {
			userName = Config.getPropertyValue("Admin_UserName");
			password = Config.getPropertyValue("Admin_Password");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			ActionHandler.wait(7);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.vCodeTextBox);

			Reporter.addStepLog("User requires to enter Verification code received in an email");
			ActionHandler.wait(5);

			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

				Reporter.addStepLog("Please Open recently received email from Salesforce");
				ActionHandler.wait(10);
				vCode = checkEmail();
				System.out.println("Verification code is = " + vCode);

				driver.get(Constants.SVOURL);
				ActionHandler.wait(15);
				Reporter.addStepLog("User Logins to SVO Portal");

				if (userRole.equals("Admin")) {
					userName = Config.getPropertyValue("Admin_UserName");
					password = Config.getPropertyValue("Admin_Password");

					ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
					ActionHandler.wait(3);
					CommonFunctions.attachScreenshot();

					ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
					ActionHandler.wait(3);
					CommonFunctions.attachScreenshot();

					ActionHandler.click(SVOEnquiryContainer.loginBtn);
					ActionHandler.wait(7);
					CommonFunctions.attachScreenshot();

					ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
					CommonFunctions.attachScreenshot();
					ActionHandler.wait(2);

					ActionHandler.click(SVOEnquiryContainer.verifyBtn);
					ActionHandler.wait(4);
					CommonFunctions.attachScreenshot();
					Reporter.addStepLog("User logins to SVO Successfully");
				}
			}
		}

		if (userRole.equals("Super User")) {
			userName = Config.getPropertyValue("SuperUser_UserName");
			password = Config.getPropertyValue("SuperUser_Password");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			ActionHandler.wait(7);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.vCodeTextBox);

			Reporter.addStepLog("User requires to enter Verification code received in an email");
			ActionHandler.wait(5);

			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

				Reporter.addStepLog("Please Open recently received email from Salesforce");
				ActionHandler.wait(10);
				vCode = checkEmail();
				System.out.println("Verification code is = " + vCode);

				driver.get(Constants.SVOURL);
				ActionHandler.wait(15);
				Reporter.addStepLog("User Logins to SVO Portal");

				if (userRole.equals("Super User")) {
					userName = Config.getPropertyValue("SuperUser_UserName");
					password = Config.getPropertyValue("SuperUser_Password");

					ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
					ActionHandler.wait(3);
					CommonFunctions.attachScreenshot();

					ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
					ActionHandler.wait(3);
					CommonFunctions.attachScreenshot();

					ActionHandler.click(SVOEnquiryContainer.loginBtn);
					ActionHandler.wait(7);
					CommonFunctions.attachScreenshot();

					ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
					CommonFunctions.attachScreenshot();
					ActionHandler.wait(2);

					ActionHandler.click(SVOEnquiryContainer.verifyBtn);
					ActionHandler.wait(4);
					CommonFunctions.attachScreenshot();
					Reporter.addStepLog("User logins to SVO Successfully");
				}
			}
		}

		if (userRole.equals("SVO Bespoke")) {
			userName = Config.getPropertyValue("BespokeUser_UserName");
			password = Config.getPropertyValue("BespokeUser_Password");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			ActionHandler.wait(7);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.vCodeTextBox);

			Reporter.addStepLog("User requires to enter Verification code received in an email");
			ActionHandler.wait(5);

			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

				Reporter.addStepLog("Please Open recently received email from Salesforce");
				ActionHandler.wait(10);
				vCode = checkEmail();
				System.out.println("Verification code is = " + vCode);

				driver.get(Constants.SVOURL);
				ActionHandler.wait(15);
				Reporter.addStepLog("User Logins to SVO Portal");

				if (userRole.equals("SVO Bespoke")) {
					userName = Config.getPropertyValue("BespokeUser_UserName");
					password = Config.getPropertyValue("BespokeUser_Password");

					ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
					ActionHandler.wait(3);
					CommonFunctions.attachScreenshot();

					ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
					ActionHandler.wait(3);
					CommonFunctions.attachScreenshot();

					ActionHandler.click(SVOEnquiryContainer.loginBtn);
					ActionHandler.wait(7);
					CommonFunctions.attachScreenshot();

					ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
					CommonFunctions.attachScreenshot();
					ActionHandler.wait(2);

					ActionHandler.click(SVOEnquiryContainer.verifyBtn);
					ActionHandler.wait(4);
					CommonFunctions.attachScreenshot();
					Reporter.addStepLog("User logins to SVO Successfully");
				}
			}
		}

		if (userRole.equals("SO Data Manager")) {
			userName = Config.getPropertyValue("SODataManager_UserName");
			password = Config.getPropertyValue("SODataManager_Password");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			ActionHandler.wait(7);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.vCodeTextBox);

			Reporter.addStepLog("User requires to enter Verification code received in an email");
			ActionHandler.wait(5);

			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

				Reporter.addStepLog("Please Open recently received email from Salesforce");
				ActionHandler.wait(10);
				vCode = checkEmail();
				System.out.println("Verification code is = " + vCode);

				driver.get(Constants.SVOURL);
				ActionHandler.wait(15);
				Reporter.addStepLog("User Logins to SVO Portal");

				if (userRole.equals("SO Data Manager")) {
					userName = Config.getPropertyValue("SODataManager_UserName");
					password = Config.getPropertyValue("SODataManager_Password");

					ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
					ActionHandler.wait(3);
					CommonFunctions.attachScreenshot();

					ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
					ActionHandler.wait(3);
					CommonFunctions.attachScreenshot();

					ActionHandler.click(SVOEnquiryContainer.loginBtn);
					ActionHandler.wait(7);
					CommonFunctions.attachScreenshot();

					ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
					CommonFunctions.attachScreenshot();
					ActionHandler.wait(2);

					ActionHandler.click(SVOEnquiryContainer.verifyBtn);
					ActionHandler.wait(4);
					CommonFunctions.attachScreenshot();
					Reporter.addStepLog("User logins to SVO Successfully");
				}
			}
		}

		if (userRole.equals("Classic Sales")) {
			userName = Config.getPropertyValue("ClassicUser_UserName");
			password = Config.getPropertyValue("ClassicUser_Password");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			ActionHandler.wait(7);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.vCodeTextBox);

			Reporter.addStepLog("User requires to enter Verification code received in an email");
			ActionHandler.wait(5);

			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

				Reporter.addStepLog("Please Open recently received email from Salesforce");
				ActionHandler.wait(10);
				vCode = checkEmail();
				System.out.println("Verification code is = " + vCode);

				driver.get(Constants.SVOURL);
				ActionHandler.wait(15);
				Reporter.addStepLog("User Logins to SVO Portal");

				if (userRole.equals("Classic Sales")) {
					userName = Config.getPropertyValue("ClassicUser_UserName");
					password = Config.getPropertyValue("ClassicUser_Password");

					ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
					ActionHandler.wait(3);
					CommonFunctions.attachScreenshot();

					ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
					ActionHandler.wait(3);
					CommonFunctions.attachScreenshot();

					ActionHandler.click(SVOEnquiryContainer.loginBtn);
					ActionHandler.wait(7);
					CommonFunctions.attachScreenshot();

					ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
					CommonFunctions.attachScreenshot();
					ActionHandler.wait(2);

					ActionHandler.click(SVOEnquiryContainer.verifyBtn);
					ActionHandler.wait(4);
					CommonFunctions.attachScreenshot();
					Reporter.addStepLog("User logins to SVO Successfully");
				}
			}
		}

	}

	// Access SVO Portal
	@Given("^Access SVO Portal$")
	public void access_SVO_Portal() throws Throwable {
		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");
		onStart();
		driver = getDriver();
		ActionHandler.wait(13);
		driver.get(Constants.SVOURL);
		ActionHandler.wait(15);
		Reporter.addStepLog("User Logins to SVO Portal");

		String UserName = Config.getPropertyValue("Admin_UserName");
		String Password = Config.getPropertyValue("Admin_Password");

		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, UserName);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, Password);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.vCodeTextBox);

		Reporter.addStepLog("User requires to enter Verification code received in an email");
		ActionHandler.wait(5);

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {
			Reporter.addStepLog("Please Open recently received email from Salesforce");
			ActionHandler.wait(2);
			ActionHandler.wait(10);
			vCode = checkEmail();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			ActionHandler.wait(15);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, UserName);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, Password);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			ActionHandler.wait(7);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			ActionHandler.wait(7);
			CommonFunctions.attachScreenshot();

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {
			ActionHandler.wait(5);
			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {
				ActionHandler.wait(3);
				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {
				ActionHandler.wait(3);
				ActionHandler.click(SVOEnquiryContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			ActionHandler.wait(120);
			ActionHandler.waitForElement(SVOEnquiryContainer.SVOText, 90);
			CommonFunctions.attachScreenshot();
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	@Given("^User Access SVO Portal$")
	public void User_Access_SVO_Portal() throws Exception {
		driver = getDriver();
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);

		ActionHandler.wait(3);
		if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.userNameTextBox)) {
			ActionHandler.wait(3);
			ActionHandler.click(SVOEnquiryContainer.icon_image);
			ActionHandler.wait(10);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Click on Logout");
			ActionHandler.click(SVOEnquiryContainer.Logout);
			ActionHandler.wait(10);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logout from SVO Portal");
		}

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		Reporter.addStepLog("User Logins to SVO Portal");

		String UserName = Config.getPropertyValue("Admin_UserName");
		String Password = Config.getPropertyValue("Admin_Password");

		ActionHandler.wait(2);
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, UserName);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, Password);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();

		ActionHandler.waitForElement(SVOEnquiryContainer.SVOText, 60);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
		Reporter.addStepLog("User navigate to SVO Home Page successfully");
	}

	// User Navigate to Enquiries
	@When("^User Navigate to Enquiries$")
	public void user_Navigate_to_Enquiries() throws Throwable {
		driver = getDriver();
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);

		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		// navigateToSVO();
		ActionHandler.wait(5);
		Reporter.addStepLog("User Navigate to Enquiry tab");
		ActionHandler.click(SVOEnquiryContainer.enquiryTab);
		ActionHandler.wait(15);
		CommonFunctions.attachScreenshot();
	}

	// User Opens an Enquiry which is in Discovery Status with Record Type and Email
	// Address
	@And("^User Opens an Enquiry which is in Discovery Status with Record Type \"([^\"]*)\", Email Address \"([^\"]*)\"$")
	public void User_Opnes_Classic_Service_Enquiry_Discovery_State(String recordType, String Email) throws Throwable {
		String rType[] = recordType.split(",");
		recordType = CommonFunctions.readExcelMasterData(rType[0], rType[1], rType[2]);

		String E[] = Email.split(",");
		Email = CommonFunctions.readExcelMasterData(E[0], E[1], E[2]);

		String client = Constants.User1;

		User_Creates_a_New_Enquiry(recordType);
		ActionHandler.wait(3);

		enter_the_required_details_for_the_Enquiry_creation();
		ActionHandler.wait(3);

		save_new_Enquiry();
		ActionHandler.wait(3);

		user_selects_Preferred_Contact_details_from_the_Potential_Matches_coming_after_entering_the_Email_Address(
				Email);
		ActionHandler.wait(3);

		log_a_call_to_the_Enquiry_with_client(client);
		ActionHandler.wait(3);

		Mark_Current_Enquiry_Status();
		ActionHandler.wait(5);

		Reporter.addStepLog("User creats a new enquiry");

	}

	// User searches a vehicle from Vehicle Search Option
	@And("^User searches a vehicle from Vehicle Search Option using Brand \"([^\"]*)\",Model \"([^\"]*)\",Full VIN \"([^\"]*)\" and Record Type \"([^\"]*)\"$")
	public void User_searches_Vehicle_Vehicle_Search_option(String brand, String model, String VIN, String recordType)
			throws Exception {
		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String m[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		String v[] = VIN.split(",");
		VIN = CommonFunctions.readExcelMasterData(v[0], v[1], v[2]);

		String rt[] = recordType.split(",");
		recordType = CommonFunctions.readExcelMasterData(rt[0], rt[1], rt[2]);

		ActionHandler.wait(3);
		ActionHandler.click(SVOEnquiryContainer.Dropdown);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.vehicleSearch);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		/*
		 * String NewWindow = driver.getWindowHandle(); ActionHandler.wait(30);
		 * Set<String> vehicleSearch = driver.getWindowHandles(); Iterator<String> i =
		 * vehicleSearch.iterator(); while (i.hasNext()) { String vSearch = i.next(); if
		 * (!NewWindow.equalsIgnoreCase(vSearch)) { driver.switchTo().window(vSearch);
		 */
		ActionHandler.wait(2);
		System.out.println("Driver moves to Vehicle search window");
		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.vSearchBrand);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		/*
		 * Actions act = new Actions(driver);
		 * act.sendKeys(Keys.ARROW_DOWN).build().perform();
		 * act.sendKeys(Keys.ARROW_DOWN).build().perform();
		 * act.sendKeys(Keys.ENTER).build().perform(); ActionHandler.wait(5);
		 */
		// CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		ActionHandler.setText(SVOEnquiryContainer.vSearchBrand, brand);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.pressEnter();
		ActionHandler.wait(1);

		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.vSearchSearchBtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.vSearchVehicleNotFound)) {
			ActionHandler.pageCompleteScrollDown();
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			Reporter.addStepLog("No record found with searched brand, so user creates a new Vehicle");
			ActionHandler.wait(2);
			ActionHandler.click(SVOEnquiryContainer.vSearchNewVehicle);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.wait(2);
			ActionHandler.setText(SVOEnquiryContainer.newVehicleRecType, recordType);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();

			ActionHandler.wait(2);
			ActionHandler.setText(SVOEnquiryContainer.brand, brand);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(SVOEnquiryContainer.brandSearch);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.brandSearch(brand))));
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.wait(2);
			ActionHandler.setText(SVOEnquiryContainer.model, model);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(SVOEnquiryContainer.modelSearch);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.brandSearch(model))));
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.wait(2);
			ActionHandler.setText(SVOEnquiryContainer.newVehicleVIN, VIN);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();

			ActionHandler.wait(2);
			ActionHandler.click(SVOEnquiryContainer.newVehicleSaveBtn);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			Reporter.addStepLog("New Vehicle got Created");

			/*
			 * driver.switchTo().window(NewWindow); } }
			 */
		}

		Reporter.addStepLog("User can view details of created Vehicle");
		// ActionHandler.pageScrollDown();
		ActionHandler.wait(3);

		Reporter.addStepLog("User Navigate to Enquiry tab");
		ActionHandler.wait(1);
		ActionHandler.click(SVOEnquiryContainer.enquiryTab);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();

		if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.RecentlyViewed)) {
			ActionHandler.wait(2);
			javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
			javaScriptUtil.clickElementByJS(SVOEnquiryContainer.back2enquiryTab);
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User navigates to Enquiry Tab");
		}

		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.firstEnquiryRecentlyViewed);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		ActionHandler.click(SVOEnquiryContainer.Dropdown);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(1);
		ActionHandler.click(SVOEnquiryContainer.vehicleSearch);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		/*
		 * String NewWindow1 = driver.getWindowHandle(); ActionHandler.wait(30);
		 * Set<String> vehicleSearch1 = driver.getWindowHandles(); Iterator<String> i1 =
		 * vehicleSearch1.iterator(); while (i.hasNext()) { String vSearch = i.next();
		 * if (!NewWindow.equalsIgnoreCase(vSearch)) {
		 * driver.switchTo().window(vSearch);
		 */
		ActionHandler.wait(2);
		System.out.println("Driver moves to Vehicle search window");
		ActionHandler.wait(2);
		ActionHandler.setText(SVOEnquiryContainer.vSearchVIN, VIN);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.vSearchSearchBtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.vSearchSelect);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		/*
		 * driver.switchTo().window(NewWindow1); } }
		 */

		Reporter.addStepLog("User creates a new Vehicle and Searches vehicle using Vehicle Search Option");
	}

	// User tries to reopen Closed Enquiry
	@And("^User tries to reopen Closed Enquiry$")
	public void User_Tries_Reopen_Closed_Enquiry() throws Exception {
		String status = "New";
		ActionHandler.wait(3);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User moved to Enquiry Status");

		ActionHandler.wait(3);
		ActionHandler.click(SVOEnquiryContainer.editEnquiryStatus);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.enquiryStatusDD);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.ValueSelection(status))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses enquiry status as New");

		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.SaveEnquiry);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		VerifyHandler.verifyElementPresent(SVOEnquiryContainer.enquirySaveError);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("An error occurred while saving a closed enquiry with New Status");

		ActionHandler.click(SVOEnquiryContainer.enquiryErrorClose);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.EnqCancel);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
	}

	// Logout from the SVO Portal
	@Then("^Logout from the SVO Portal$")
	public void logout_from_the_SVO_Portal() throws IOException {

		ActionHandler.click(SVOEnquiryContainer.icon_image);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Logout");
		ActionHandler.click(SVOEnquiryContainer.Logout);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.userNameTextBox)) {
			MarkupHelper.createLabel("Logged out successfully", ExtentColor.GREEN);
		} else {
			MarkupHelper.createLabel("Log out failed", ExtentColor.RED);
		}
	}

	// User Creates a New Enquiry with Record Type
	@Then("^User Creates a New Enquiry with Record Type \"([^\"]*)\"$")
	public void User_Creates_a_New_Enquiry(String recordType) throws Exception {
		System.out.println(recordType);

		String rType[] = recordType.split(",");
		recordType = CommonFunctions.readExcelMasterData(rType[0], rType[1], rType[2]);

		System.out.println(rType[0]);
		System.out.println(rType[1]);
		System.out.println(rType[2]);

		ActionHandler.wait(4);
		Reporter.addStepLog("User Creates new enquiry");
		ActionHandler.click(SVOEnquiryContainer.newEnquiryBtn);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		System.out.println("Selected Record type is = " + recordType);
		Reporter.addStepLog("User Needs to select any record type option");
		ActionHandler.wait(3);
		//ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.selectRecordType(recordType))));
		ActionHandler.click(SVOEnquiryContainer.ClassicSalesRecType);
		Reporter.addStepLog("User Select required Record Type");
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOEnquiryContainer.nextRType);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigate to enter enquiry details page");
	}

	// Enter the required details for the Enquiry creation
	@Then("^Enter the required details for the Enquiry creation$")
	public void enter_the_required_details_for_the_Enquiry_creation() throws Throwable {
		String enquiryTitle = "Test_Enquiry_";
		double randomNumber = getRandomIntegerBetweenRange(0, 1000);

		ActionHandler.wait(7);
		ActionHandler.setText(SVOEnquiryContainer.enquiryTitle, enquiryTitle + randomNumber);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		System.out.println("User has entered Enquiry Title");

	}

	// Save the new Enquiry
	@Then("^Save the new Enquiry$")
	public void save_new_Enquiry() throws Exception {
		ActionHandler.wait(3);
		ActionHandler.click(SVOEnquiryContainer.SaveEnquiry);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves Enquiry");
	}

	// from Contacted Tab Mark Current Enquiry Status
	@Then("^from Contacted Tab Mark Current Enquiry Status$")
	public void from_Contacted_Tab_Mark_Current_Enquiry_Status() throws Throwable {
		ActionHandler.wait(10);
		ActionHandler.click(SVOEnquiryContainer.contactedTab);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(5);
		ActionHandler.click(SVOEnquiryContainer.MarkCurrentEnquiry);
		ActionHandler.wait(15);
		CommonFunctions.attachScreenshot();
	}

	// Verify Current status cannot be marked as Contacted
	@Then("^Verify Current status cannot be marked as Contacted$")
	public void verify_Current_status_cannot_be_marked_as_Contacted() throws Throwable {
		ActionHandler.wait(3);
		VerifyHandler.verifyElementPresent(SVOEnquiryContainer.discoveryError);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		String enquiryStatus = SVOEnquiryContainer.enquiryStatus.getText();
		if (enquiryStatus.equals("New")) {
			Reporter.addStepLog("Enquiry status should be in New as call wasn't Logged");
		} else {
			Reporter.addStepLog("Enquiry status is not in New though call wasn't Logged");
		}
		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.errorClose);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("Navigate to Enquiry Tab");
		ActionHandler.click(SVOEnquiryContainer.enquiryTab);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();

		String status = SVOEnquiryContainer.status.getText();
		Reporter.addStepLog("Status Should be = " + status);
	}

	// User selects Preferred Contact details from the Potential Matches coming
	// after entering the Email Address
	@Then("^User selects Preferred Contact details from the Potential Matches coming after entering the Email Address \"([^\"]*)\"$")
	public void user_selects_Preferred_Contact_details_from_the_Potential_Matches_coming_after_entering_the_Email_Address(
			String Email) throws Throwable {
		String E[] = Email.split(",");
		Email = CommonFunctions.readExcelMasterData(E[0], E[1], E[2]);

		Reporter.addStepLog("Navigate to Details tab");
		ActionHandler.click(SVOEnquiryContainer.enqDetails);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		// ActionHandler.pageScrollDown();

		ActionHandler.wait(3);
		ActionHandler.click(SVOEnquiryContainer.editEnqTitle);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		javaScriptUtil.scrollIntoView(SVOEnquiryContainer.emailIdTextBox);
		ActionHandler.wait(2);
		ActionHandler.setText(SVOEnquiryContainer.emailIdTextBox, Email);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Input Email information");

		ActionHandler.click(SVOEnquiryContainer.saveEmailBtn);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Save");

		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(7);
		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		// javaScriptUtil.scrollIntoView(SVOEnquiryContainer.matcheslabel);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOEnquiryContainer.PotentialMatchSelect);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Choose the contact from potential matches");

		ActionHandler.pageCompleteScrollUp();
		CommonFunctions.attachScreenshot();
	}

	@Then("^User selects Preferred Contact details from the Potential Matches coming after entering the Email Address \"([^\"]*)\" for Private Office$")
	public void user_selects_Preferred_Contact_details_from_the_Potential_Matches_coming_after_entering_the_Email_Address_for_private_office(
			String Email) throws Throwable {
		String E[] = Email.split(",");
		Email = CommonFunctions.readExcelMasterData(E[0], E[1], E[2]);

		ActionHandler.wait(2);
		ActionHandler.pageDown();
		ActionHandler.wait(2);
		Reporter.addStepLog("Navigate to Details tab");
		ActionHandler.click(SVOEnquiryContainer.enqDetails);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		// ActionHandler.pageScrollDown();

		ActionHandler.pageDown();
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.POeditEmail);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("click on edit email");

		ActionHandler.wait(2);
		ActionHandler.setText(SVOEnquiryContainer.emailIdTextBox, Email);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Input Email information");

		ActionHandler.click(SVOEnquiryContainer.saveEmailBtn);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Save");

		ActionHandler.pageUp();
		ActionHandler.wait(7);
		ActionHandler.click(SVOEnquiryContainer.PotentialMatchSelect);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Choose the contact from potential matches");

		ActionHandler.pageCompleteScrollUp();
		CommonFunctions.attachScreenshot();
	}

	@Then("^User selects Preferred Contact details from the Potential Matches coming after entering the Email Address \"([^\"]*)\" for Classic Sales$")
	public void user_selects_Preferred_Contact_details_from_the_Potential_Matches_coming_after_entering_the_Email_Address_for_classic_sales(
			String Email) throws Throwable {
		String E[] = Email.split(",");
		Email = CommonFunctions.readExcelMasterData(E[0], E[1], E[2]);

		Reporter.addStepLog("Navigate to Details tab");
		ActionHandler.click(SVOEnquiryContainer.enqDetails);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		// ActionHandler.pageScrollDown();

		ActionHandler.pageDown();
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.POeditEmail);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("click on edit email");

		ActionHandler.wait(2);
		ActionHandler.setText(SVOEnquiryContainer.emailIdTextBox, Email);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Input Email information");

		ActionHandler.click(SVOEnquiryContainer.saveEmailBtn);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Save");

		ActionHandler.pageUp();
		ActionHandler.wait(7);
		ActionHandler.click(SVOEnquiryContainer.PotentialMatchSelect);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Choose the contact from potential matches");

		ActionHandler.pageCompleteScrollUp();
		CommonFunctions.attachScreenshot();
	}

	@Then("^User selects Preferred Contact details from the Potential Matches coming after entering the Email Address \"([^\"]*)\" for Classic Service$")
	public void user_selects_Preferred_Contact_details_from_the_Potential_Matches_coming_after_entering_the_Email_Address_for_classic_Service(
			String Email) throws Throwable {
		String E[] = Email.split(",");
		Email = CommonFunctions.readExcelMasterData(E[0], E[1], E[2]);

		Reporter.addStepLog("Navigate to Details tab");
		ActionHandler.click(SVOEnquiryContainer.enqDetails);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		// ActionHandler.pageScrollDown();

		ActionHandler.pageDown();
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.ClaServiceeditEmail);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("click on edit email");

		ActionHandler.wait(2);
		ActionHandler.setText(SVOEnquiryContainer.emailIdTextBox, Email);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Input Email information");

		ActionHandler.click(SVOEnquiryContainer.saveEmailBtn);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Save");

		ActionHandler.pageUp();
		ActionHandler.wait(7);
		ActionHandler.click(SVOEnquiryContainer.PotentialMatchSelect);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Choose the contact from potential matches");

		ActionHandler.pageCompleteScrollUp();
		CommonFunctions.attachScreenshot();
	}

	// Log a call to the Enquiry with client
	@Then("^Log a call to the Enquiry with client \"([^\"]*)\"$")
	public void log_a_call_to_the_Enquiry_with_client(String client) throws Throwable {
		String C[] = client.split(",");
		client = CommonFunctions.readExcelMasterData(C[0], C[1], C[2]);

		String commentText = "Customer has requested a  report";

		ActionHandler.wait(3);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		ActionHandler.pageArrowUp();
		ActionHandler.wait(2);
		ActionHandler.pageArrowUp();

		ActionHandler.wait(3);

		Reporter.addStepLog("User Navigate to Activity Tab");
		ActionHandler.click(SVOEnquiryContainer.activityTab);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Activity tab");

		Reporter.addStepLog("User clicks on Create a log call");
		ActionHandler.wait(3);
		ActionHandler.click(SVOEnquiryContainer.newLogCall);
		ActionHandler.wait(7);
		Reporter.addStepLog("click on create a log");

		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.subjectDD);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		act1.sendKeys(Keys.ENTER).build().perform();
		// ActionHandler.click(SVOEnquiryContainer.callLog);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Select subject as call");

		ActionHandler.setText(SVOEnquiryContainer.name, client);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOEnquiryContainer.nameSearch);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.clientNameSearch(client))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Input client information");

		ActionHandler.setText(SVOEnquiryContainer.commentText, commentText);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Input comment");

		ActionHandler.click(SVOEnquiryContainer.saveLog);
		ActionHandler.wait(20);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Save the log");
	}

	// from Discovery Tab click on Mark Current Enquiry Status
	@Then("^from Discovery Tab click on Mark Current Enquiry Status$")
	public void Mark_Current_Enquiry_Status() throws Exception {
		ActionHandler.wait(3);
		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(3);
		VerifyHandler.verifyElementPresent(SVOEnquiryContainer.contactedTab);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Status has updated to Contacted");

		ActionHandler.wait(3);
		ActionHandler.click(SVOEnquiryContainer.discoveryTab);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on discovery tab");

		ActionHandler.wait(5);
		ActionHandler.click(SVOEnquiryContainer.MarkCurrentEnquiry);
		ActionHandler.wait(15);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Mark current Enquiry");
	}

	// user fills the required vehicle details
	@Then("^user fills the required vehicle details$")
	public void user_fills_the_required_vehicle_details() throws Throwable {
		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.Showkeyfield)) {
			ActionHandler.wait(3);
			ActionHandler.click(SVOEnquiryContainer.Showkeyfield);
			Reporter.addStepLog("Click on show more");
			CommonFunctions.attachScreenshot();
		}

		ActionHandler.click(SVOEnquiryContainer.Vehicleinfoedit);
		ActionHandler.wait(3);
		Reporter.addStepLog("Click on edit button");
		CommonFunctions.attachScreenshot();

	}

	// user fills the required details like Product Offering, Enquiry Source,
	// Originating Division, Region, Sales Area, Market, Brand, Model, and Retailer
	@Then("^user fills the required details like Product Offering \"([^\"]*)\" Enquiry Source \"([^\"]*)\" Originating Division \"([^\"]*)\" Region \"([^\"]*)\" Sales Area \"([^\"]*)\" Market \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\" and Retailer \"([^\"]*)\"$")
	public void user_fills_the_required_details_like_Product_Offering_Enquiry_Source_Originating_Division_Region_Sales_Area_Market_Brand_Model_and_Retailer(
			String productOffering, String enquirySource, String originatingDivision, String region, String salesArea,
			String market, String brand, String model, String retailer) throws Throwable {
		String pOffering[] = productOffering.split(",");
		productOffering = CommonFunctions.readExcelMasterData(pOffering[0], pOffering[1], pOffering[2]);

		String eSource[] = enquirySource.split(",");
		enquirySource = CommonFunctions.readExcelMasterData(eSource[0], eSource[1], eSource[2]);

		String oDivision[] = originatingDivision.split(",");
		originatingDivision = CommonFunctions.readExcelMasterData(oDivision[0], oDivision[1], oDivision[2]);

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String sArea[] = salesArea.split(",");
		salesArea = CommonFunctions.readExcelMasterData(sArea[0], sArea[1], sArea[2]);

		String m[] = market.split(",");
		market = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String mo[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(mo[0], mo[1], mo[2]);

		String re[] = retailer.split(",");
		retailer = CommonFunctions.readExcelMasterData(re[0], re[1], re[2]);
		System.out.println("All details are fetched from excel");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.Showkeyfield)) {
			ActionHandler.wait(3);
			ActionHandler.click(SVOEnquiryContainer.Showkeyfield);
			Reporter.addStepLog("Click on show more");
			CommonFunctions.attachScreenshot();
		}

		ActionHandler.click(SVOEnquiryContainer.Vehicleinfoedit);
		ActionHandler.wait(3);
		Reporter.addStepLog("Click on edit button");
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.brand, brand);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOEnquiryContainer.brandSearch);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.brandSearch(brand))));
		ActionHandler.wait(3);
		Reporter.addStepLog("Enter brand information");
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.model, model);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOEnquiryContainer.modelSearch);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.brandSearch(model))));
		ActionHandler.wait(3);
		Reporter.addStepLog("Enter model information");
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(1);
		ActionHandler.click(SVOEnquiryContainer.proOffering);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.SourceSelection(productOffering))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Input product offering information");

		ActionHandler.click(SVOEnquiryContainer.Savechanges);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("click on save");

		ActionHandler.pageScrollDown();
		ActionHandler.pageArrowUp();
		ActionHandler.pageArrowUp();

		Reporter.addStepLog("Navigate to Details tab");
		ActionHandler.click(SVOEnquiryContainer.enqDetails);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		// ActionHandler.pageScrollDown();
		ActionHandler.wait(3);
		// javaScriptUtil.scrollIntoView(SVOEnquiryContainer.editRegion);
		ActionHandler.wait(5);
		ActionHandler.click(SVOEnquiryContainer.editRegion);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		// ActionHandler.scrollToView(SVOEnquiryContainer.ViewallDependencies);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.ViewallDependencies)) {
			ActionHandler.wait(3);
			ActionHandler.click(SVOEnquiryContainer.editRegion);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
		}

		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.ViewallDependencies);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.regionDD)) {
			ActionHandler.wait(2);
			ActionHandler.pageDown();
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(2);
			ActionHandler.click(SVOEnquiryContainer.ViewallDependencies);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
		}

		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.regionDD);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.ValueSelection(region))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter Region information");

		ActionHandler.click(SVOEnquiryContainer.BespokesalesAreaDD);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.ValueSelection(salesArea))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter Sales Area information");

		ActionHandler.click(SVOEnquiryContainer.marketDD);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.ValueSelection(market))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter market information");
		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.Applybtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click Apply");

		// ActionHandler.wait(3);
		// javaScriptUtil.scrollIntoView(SVOEnquiryContainer.retailer);
		ActionHandler.wait(3);
		ActionHandler.setText(SVOEnquiryContainer.retailer, retailer);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Input client information");

		ActionHandler.pageUp();
		ActionHandler.wait(3);
		ActionHandler.click(SVOEnquiryContainer.ViewallDependencies);
		ActionHandler.wait(5);
		ActionHandler.click(SVOEnquiryContainer.ViewallDependencies);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOEnquiryContainer.Applybtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.scrollDown();
		ActionHandler.pageDown();
		ActionHandler.wait(3);
		ActionHandler.pageDown();
		ActionHandler.wait(3);
		ActionHandler.pageDown();
		ActionHandler.wait(3);
		Reporter.addStepLog("Navigate to Source block");

		ActionHandler.wait(4);
		ActionHandler.click(SVOEnquiryContainer.originatingDiv);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.ValueSelection(originatingDivision))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Input Origination information");

		ActionHandler.click(SVOEnquiryContainer.BespokesourceDD);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.ValueSelection(enquirySource))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Input source information");

		ActionHandler.click(SVOEnquiryContainer.saveEmailBtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Save the changes");

	}

	// From Discovery Tab click on Mark Current Enquiry Status as Complete
	@Then("^From Discovery Tab click on Mark Current Enquiry Status as Complete$")
	public void Mark_As_Complete() throws Exception {
		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(3);
		ActionHandler.pageUp();
		ActionHandler.wait(5);
		ActionHandler.click(SVOEnquiryContainer.MarkEnquiryComplete);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.closedState);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOEnquiryContainer.selectClosedState);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOEnquiryContainer.doneClosedState);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

	}

	// Verify that a new opportunity is created after closing the Enquiry
	@Then("^Verify that a new opportunity is created after closing the Enquiry$")
	public void verify_that_a_new_opportunity_is_created_after_closing_the_Enquiry() throws Throwable {
		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		ActionHandler.click(SVOEnquiryContainer.Opportunitynav);
		Reporter.addStepLog("User clicks on Opportunity Tab to see any open Opportunity");
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		VerifyHandler.verifyElementPresent(SVOEnquiryContainer.OpporName);
		Reporter.addStepLog("User verifies the created Opportunity");
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
	}

	// Verify that user can view Recently Viewed Enquiries as default
	@Then("^Verify that user can view Recently Viewed Enquiries as default$")
	public void verify_that_user_can_view_Recently_Viewed_Enquiries_as_default() throws Throwable {
		Reporter.addStepLog("Navigate to Enquiry Tab");
		ActionHandler.click(SVOEnquiryContainer.enquiryTab);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();

		VerifyHandler.verifyElementPresent(SVOEnquiryContainer.RecentlyViewed);
		Reporter.addStepLog("User verifies the default view as recently viewed");
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
	}

	// Verify the SLA status and change the Enquiry Owner information
	@Then("^Verify the SLA status and change the Enquiry Owner information to \"([^\"]*)\"$")
	public void verify_the_SLA_status_and_change_the_Enquiry_Owner_information_to(String Owner) throws Throwable {
		String O[] = Owner.split(",");
		Owner = CommonFunctions.readExcelMasterData(O[0], O[1], O[2]);

		ActionHandler.wait(2);
		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(2);
		ActionHandler.pageUp();
		ActionHandler.wait(2);
		/*
		 * ActionHandler.pageUp(); ActionHandler.wait(2);
		 */
		CommonFunctions.attachScreenshot();
		VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SLAstatussuccess);
		ActionHandler.wait(2);
		Reporter.addStepLog("Verify SLA status");

		ActionHandler.wait(2);
		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(7);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.ChangeOwner);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on change owner");

		ActionHandler.wait(3);
		ActionHandler.setText(SVOEnquiryContainer.SearchUsers, Owner);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter new owner");
		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.ChOwnerSearch);
		ActionHandler.wait(4);
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.SourceSelection(Owner))));
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Select new owner");

		VerifyHandler.verifyElementPresent(SVOEnquiryContainer.Ownersentmail);
		ActionHandler.wait(2);
		Reporter.addStepLog("Verify sent email selected");

		ActionHandler.click(SVOEnquiryContainer.ChangeOwnerbutton);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on change owner button");

		ActionHandler.wait(1);

	}

	// Close enquiry with close type and reason
	@Then("^Close enquiry with close type as \"([^\"]*)\" and reason as \"([^\"]*)\"$")
	public void close_enquiry_with_close_type_as_and_reason_as(String Close, String Reason) throws Throwable {
		String clo[] = Close.split(",");
		Close = CommonFunctions.readExcelMasterData(clo[0], clo[1], clo[2]);

		String R[] = Reason.split(",");
		Reason = CommonFunctions.readExcelMasterData(R[0], R[1], R[2]);

		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(5);
		ActionHandler.click(SVOEnquiryContainer.MarkEnquiryComplete);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.closedState);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.ValueSelection(Close))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Updated the Closed status");

		if (Close.equals("Closed (Lost)")) {
			ActionHandler.click(SVOEnquiryContainer.Closedreason);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.ValueSelection(Reason))));
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Updated the Closed Reason");
		}

		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOEnquiryContainer.doneClosedState);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on done");
	}

	// change the Enquiry Owner information without a mail
	@When("^change the Enquiry Owner information to \"([^\"]*)\" without a mail$")
	public void change_the_Enquiry_Owner_information_to_without_a_mail(String Owner) throws Throwable {
		String O[] = Owner.split(",");
		Owner = CommonFunctions.readExcelMasterData(O[0], O[1], O[2]);

		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(7);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.ChangeOwner);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on change owner");

		ActionHandler.setText(SVOEnquiryContainer.SearchUsers, Owner);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter new owner");
		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.ChOwnerSearch);
		ActionHandler.wait(4);
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.SourceSelection(Owner))));
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Select new owner");

		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.Ownersentmail);
		ActionHandler.wait(2);
		Reporter.addStepLog("Deselect sent email button");

		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.ChangeOwnerbutton);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on change owner button");
	}

	// Enter Enquiry title
	@Then("^Enter Enquiry title \"([^\"]*)\"  for the Enquiry creation$")
	public void enter_Enquiry_title_for_the_Enquiry_creation(String enquiryTitle) throws Throwable {
		String Et[] = enquiryTitle.split(",");
		enquiryTitle = CommonFunctions.readExcelMasterData(Et[0], Et[1], Et[2]);

		ActionHandler.wait(7);
		ActionHandler.setText(SVOEnquiryContainer.enquiryTitle, enquiryTitle);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		System.out.println("User has entered Enquiry Title");
	}

	// Verify the title of the created Enquiry
	@Then("^Verify the title of the created Enquiry as \"([^\"]*)\"$")
	public void verify_the_title_of_the_created_Enquiry_as(String enquiryTitle) throws Throwable {
		String Et[] = enquiryTitle.split(",");
		enquiryTitle = CommonFunctions.readExcelMasterData(Et[0], Et[1], Et[2]);

		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		ActionHandler.pageDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		VerifyHandler
				.verifyElementPresent(driver.findElement(By.xpath(SVOEnquiryContainer.EnquiryTitle(enquiryTitle))));
		Reporter.addStepLog("Verify Enquiry title");
	}

	// user clicks on Compose Email button
	@When("^user clicks on Compose Email button$")
	public void user_clicks_on_Compose_Email_button() throws Throwable {
		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(7);
		ActionHandler.click(SVOEnquiryContainer.Dropdown);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on drop down");

		ActionHandler.click(SVOEnquiryContainer.ComposeEmail);
		ActionHandler.wait(20);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Compose Email button");
	}

	// Click on the Send button
	@Then("^Click on the Send button$")
	public void click_on_the_Send_button() throws Throwable {
		driver.switchTo().frame(0);
		javaScriptUtil.clickElementByJS(SVOEnquiryContainer.EmailSend);
		ActionHandler.wait(30);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("The email is not sent and error message is displayed");
		driver.switchTo().parentFrame();
	}

	// edit the required information for mail
	@Then("^edit the required information as to \"([^\"]*)\" Related to \"([^\"]*)\" Additional to \"([^\"]*)\" CC \"([^\"]*)\" BCC \"([^\"]*)\" Subject \"([^\"]*)\" Body \"([^\"]*)\" and Importance \"([^\"]*)\"$")
	public void edit_the_required_information_as_to_Related_to_Additional_to_CC_BCC_Subject_Body_and_Importance(
			String To, String Related, String Additional, String CC, String BCC, String Subject, String Body,
			String importance) throws Throwable {
		String t[] = To.split(",");
		To = CommonFunctions.readExcelMasterData(t[0], t[1], t[2]);

		String re[] = Related.split(",");
		Related = CommonFunctions.readExcelMasterData(re[0], re[1], re[2]);

		String add[] = Additional.split(",");
		Additional = CommonFunctions.readExcelMasterData(add[0], add[1], add[2]);

		String cc[] = CC.split(",");
		CC = CommonFunctions.readExcelMasterData(cc[0], cc[1], cc[2]);

		String bcc[] = BCC.split(",");
		BCC = CommonFunctions.readExcelMasterData(bcc[0], bcc[1], bcc[2]);

		String s[] = Subject.split(",");
		Subject = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String b[] = Body.split(",");
		Body = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String imp[] = importance.split(",");
		importance = CommonFunctions.readExcelMasterData(imp[0], imp[1], imp[2]);

		System.out.println("All details are fetched from excel");

		ActionHandler.wait(20);

		driver.switchTo().frame(0);
		ActionHandler.setText(SVOEnquiryContainer.toemail, To);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter to information");

		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.Relatedto);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.Relatedtooption(Related))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Updated Related To");

		ActionHandler.pageDown();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.Additionalto, Additional);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter Additional email");

		ActionHandler.setText(SVOEnquiryContainer.CC, CC);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter CC email");

		ActionHandler.setText(SVOEnquiryContainer.BCC, BCC);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter BCC email");

		ActionHandler.click(SVOEnquiryContainer.Importance);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.Relatedtooption(importance))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Updated the Closed status");

		ActionHandler.setText(SVOEnquiryContainer.Subject, Subject);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter Subject for email");

		ActionHandler.pageDown();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		driver.switchTo().frame("thePage:theform:thePB:pbS:pbSI_body:email_body_ifr");
		ActionHandler.setText(SVOEnquiryContainer.Body, Body);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter Body for email");
		driver.switchTo().parentFrame();
		ActionHandler.wait(3);
	}

	@Then("^Click on sent mail button$")
	public void click_on_sent_mail_button() throws Throwable {
		ActionHandler.scrollToView(SVOEnquiryContainer.sendemail);
		ActionHandler.wait(3);
		ActionHandler.click(SVOEnquiryContainer.sendemail);
		ActionHandler.wait(20);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on send mail");
	}

	// Click on save mail button
	@Then("^Click on save mail button$")
	public void click_on_save_mail_button() throws Throwable {

		ActionHandler.scrollToView(SVOEnquiryContainer.saveemail);
		ActionHandler.wait(5);
		ActionHandler.click(SVOEnquiryContainer.saveemail);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on save mail");

		ActionHandler.wait(3);
		ActionHandler.click(SVOEnquiryContainer.cancelemail);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on cancel mail");

	}

	// User search an enquiry having Closed reason type
	@And("^User search an enquiry having Closed Lost reason type as \"([^\"]*)\"$")
	public void User_search_enquiry_having_closed_lost_reason_type(String reasonType) throws Throwable {
		String EnqStatus = "Closed";
		ActionHandler.wait(2);
		user_navigates_to_All_Enquiries_list_from_Recently_Viewed_Enquiry_list();

		ActionHandler.wait(3);
		ActionHandler.click(SVOEnquiryContainer.EnquirySearchBox);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		ActionHandler.clearAndSetText(SVOEnquiryContainer.EnquirySearchBox, EnqStatus);
		CommonFunctions.attachScreenshot();
		ActionHandler.pressEnter();
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.firstEnquiryAllList);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("An enquiry with Closed[Lost] reason type has been found");
	}

	// User change closed reason type
	@Then("^User change closed reason type from \"([^\"]*)\" to \"([^\"]*)\"$")
	public void User_Change_Close_Reason_Type(String fromReasonType, String toReasonType) throws Exception {
		toReasonType = "Qualified - Opportunity";

		ActionHandler.wait(3);
		ActionHandler.scrollToView(SVOEnquiryContainer.editClosedState);
		ActionHandler.wait(4);

		ActionHandler.click(SVOEnquiryContainer.editClosedState);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Edit Close State");

		ActionHandler.click(SVOEnquiryContainer.editClosedStateDD);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.ValueSelection(toReasonType))));
		ActionHandler.wait(3);
		Reporter.addStepLog("User selects Qualified-Opportunity");

		ActionHandler.click(SVOEnquiryContainer.saveEmailBtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User closes an enquiry with Qualified-Opportunity status");

		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
	}

	// User search an enquiry having closed reason type
	@And("^User search an enquiry having closed reason type as \"([^\"]*)\"$")
	public void User_search_enquiry_closed_reason_type(String reasonType) throws Throwable {
		String qualifiedEnqStatus = "Qualified";
		ActionHandler.wait(2);
		user_navigates_to_All_Enquiries_list_from_Recently_Viewed_Enquiry_list();

		ActionHandler.wait(3);
		ActionHandler.click(SVOEnquiryContainer.EnquirySearchBox);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		ActionHandler.clearAndSetText(SVOEnquiryContainer.EnquirySearchBox, qualifiedEnqStatus);
		CommonFunctions.attachScreenshot();
		ActionHandler.pressEnter();
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.firstEnquiryAllList);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("An enquiry with Qualified-Opprotunity reason type has been found");
	}

	// User changes the closed reason type
	@Then("^User changes the closed reason type from \"([^\"]*)\" to \"([^\"]*)\"$")
	public void User_changes_closed_reason_type(String fromReasonType, String toReasonType) throws Exception {

		ActionHandler.wait(3);
		ActionHandler.click(SVOEnquiryContainer.editClosedState);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Edit Close State");

		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.enqClosedStateDD);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.enqClosedStateDD);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.cancelEditBtn);
		ActionHandler.wait(5);

		Reporter.addStepLog("User cannot change close state to Closed[Lost] status");

	}

	// Verify that an error occurred while editing closed enquiry reason type
	@And("^Verify that an error occurred while editing closed enquiry reason type$")
	public void Verify_An_Error_Occurred() throws Exception {
		ActionHandler.wait(3);
		Reporter.addStepLog("User is getting an error while closing an enquiry with closed[lost] status");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.closedStatusErrorMsg)) {
			String errorText = SVOEnquiryContainer.closedStatusErrorMsg.getText();
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Error message is = " + errorText);
		}

		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
	}

	// User opens an enquiry which is in new status since 48hrs
	@Then("^User opens an enquiry which is in new status since 48hrs$")
	public void User_Opens_An_Enquiry_New_48Hrs() throws Throwable {
		String Email = "msoni3@jaguarlandrover.com";
		ActionHandler.wait(3);
		Reporter.addStepLog("User creats a new enquiry to test");
		newEnquiry_SLA();

		Reporter.addStepLog("Navigate to Details tab");
		ActionHandler.click(SVOEnquiryContainer.enqDetails);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		// ActionHandler.pageScrollDown();

		ActionHandler.pageDown();
		ActionHandler.wait(2);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.editEmail);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("click on edit email");

		ActionHandler.wait(2);
		ActionHandler.setText(SVOEnquiryContainer.emailIdTextBox, Email);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Input Email information");

		ActionHandler.click(SVOEnquiryContainer.saveEmailBtn);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Save");

		ActionHandler.pageUp();
		ActionHandler.wait(7);
		ActionHandler.click(SVOEnquiryContainer.PotentialMatchSelect);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Choose the contact from potential matches");

		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User checks the SLA status");
		ActionHandler.wait(3);
		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(3);
		ActionHandler.pageArrowUp();
		ActionHandler.wait(3);
		// javaScriptUtil.scrollIntoView(SVOEnquiryContainer.SLAStatusSection);
		// ActionHandler.scrollToView(SVOEnquiryContainer.SLAStatusSection);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		String kickOffDate = SVOEnquiryContainer.SLAKickOffDate.getText();
		System.out.println("Enquiry Kick Off date is = " + kickOffDate);

		String time1 = kickOffDate.substring(11, 16);
		System.out.println("Time is = " + time1);

		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.editFirstContactTarget);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User tries to change First Contact Target Date");

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		String date = dateFormat.format(new Date());
		System.out.println("System Date is = " + date);

		ActionHandler.wait(3);
		ActionHandler.clearAndSetText(SVOEnquiryContainer.editFirstCntTargetDate, date);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		ActionHandler.clearAndSetText(SVOEnquiryContainer.editFirstCntTargetTime, time1);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User updates Date and Time");

		ActionHandler.wait(3);
		ActionHandler.click(SVOEnquiryContainer.saveEmailBtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves updated first contact target date");
	}

	// Verify that User receives an email notification for the same enquiry
	@And("^Verify that User receives a Custom notification for the same enquiry$")
	public void Verify_user_receives_a_Custom_notification() throws Exception {
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verify that User receives a custom notification");

		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SLAFailedStatus)) {
			Reporter.addStepLog("SLA status is failed");
			CommonFunctions.attachScreenshot();
		}
		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
	}

	// Verify that the open enquiry SLA status is failed
	@And("^Verify that the open enquiry SLA status is failed$")
	public void Verify_Open_Enquiry_SLA_Status_Failed() throws Exception {
		ActionHandler.wait(3);
		ActionHandler.wait(60);
		Reporter.addScenarioLog("User needs to wait 60 Sec to mark SLA as failed");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SLAFailedStatus)) {
			Reporter.addStepLog("SLA status is failed when user miss the first contact target date");
			CommonFunctions.attachScreenshot();
		}
		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
	}

	// User search an enquiry with non-closed status
	@And("^User search an enquiry with non-closed status$")
	public void User_Search_An_Enquiry_non_closed_status() throws Throwable {
		String qualifiedEnqStatus = "New";
		Reporter.addStepLog("User navigates to all enquiries");
		user_navigates_to_All_Enquiries_list_from_Recently_Viewed_Enquiry_list();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		ActionHandler.click(SVOEnquiryContainer.EnquirySearchBox);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		ActionHandler.clearAndSetText(SVOEnquiryContainer.EnquirySearchBox, qualifiedEnqStatus);
		CommonFunctions.attachScreenshot();
		ActionHandler.pressEnter();
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.firstEnquiryAllList);
		ActionHandler.wait(6);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("Non-closed enquiry has been opened");
	}

	// Verify that no opportunity is linked for a non-closed enquiry
	@Then("^Verify that no opportunity is linked for a non-closed enquiry$")
	public void Verify_No_Opportunity_Linked() throws Exception {
		Reporter.addStepLog("User verifies that no opportunity is linked up for an open enquiry");
		ActionHandler.wait(2);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);
		// ActionHandler.scrollToView(SVOEnquiryContainer.OpportunityLinkEnqDetail);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.OpportunityLinkEnqDetail);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		String errorText = SVOEnquiryContainer.noOpportunityErrorMsg.getText();
		Reporter.addStepLog("Opportunity search error message is = " + errorText);
		CommonFunctions.attachScreenshot();
	}

	// user clicks on select template button and selects a template and saves it
	@Then("^user clicks on select template button and selects a template and saves it$")
	public void user_clicks_on_select_template_button_and_selects_a_template() throws Throwable {
		ActionHandler.wait(10);
		driver.switchTo().frame(0);
		ActionHandler.wait(2);
		ActionHandler.scrollToView(SVOEnquiryContainer.SelectTemplate);
		ActionHandler.wait(3);
		ActionHandler.click(SVOEnquiryContainer.SelectTemplate);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Select Template button");

		String NewWindow = driver.getWindowHandle();
		ActionHandler.wait(30);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!NewWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.wait(5);
				ActionHandler.click(SVOEnquiryContainer.Template1);
				ActionHandler.wait(20);
				CommonFunctions.attachScreenshot();
				driver.switchTo().window(NewWindow);
			}
		}

		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(3);

		driver.switchTo().frame(0);
		ActionHandler.wait(3);

		ActionHandler.scrollToView(SVOEnquiryContainer.saveemail);
		ActionHandler.wait(3);
		ActionHandler.click(SVOEnquiryContainer.saveemail);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on save mail");

		ActionHandler.wait(3);
		ActionHandler.click(SVOEnquiryContainer.cancelemail);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on cancel mail");
	}

	// user clicks on drafts and retrives draft with Subject
	@Then("^user clicks on drafts and retrives draft with Subject \"([^\"]*)\"$")
	public void user_clicks_on_drafts_and_retrives_draft_with_Subject(String Subject) throws Throwable {
		String s[] = Subject.split(",");
		Subject = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		ActionHandler.wait(10);
		driver.switchTo().frame(0);
		ActionHandler.wait(5);
		ActionHandler.scrollToView(SVOEnquiryContainer.Drafts);
		ActionHandler.wait(5);
		ActionHandler.click(SVOEnquiryContainer.Drafts);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Drafts button");

		String NewWindow = driver.getWindowHandle();
		ActionHandler.wait(30);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!NewWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);

				ActionHandler.wait(5);
				ActionHandler.click(SVOEnquiryContainer.RetrieveDrafts);
				CommonFunctions.attachScreenshot();
				Reporter.addStepLog("Click on Retrieve Drafts button");

				ActionHandler.wait(5);
				ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.SelectDrafts(Subject))));
				CommonFunctions.attachScreenshot();
				Reporter.addStepLog("Click on selected Drafts button");

				ActionHandler.wait(20);
				CommonFunctions.attachScreenshot();
				driver.switchTo().window(NewWindow);

				ActionHandler.wait(5);
				driver.switchTo().frame(0);
				ActionHandler.wait(2);
				ActionHandler.pageCompleteScrollDown();
				ActionHandler.wait(3);
			}
		}

	}

	// Verify that user is able to view auto populated fields after opening Compose
	// email
	@When("^Verify that user is able to view auto populated fields after opening Compose email$")
	public void verify_that_user_is_able_to_view_auto_populated_fields_after_opening_Compose_email() throws Throwable {
		ActionHandler.wait(20);
		driver.switchTo().frame(0);

		VerifyHandler.verifyElementPresent(SVOEnquiryContainer.businessUnit);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verify business Unit information");

		VerifyHandler.verifyElementPresent(SVOEnquiryContainer.emailFormat);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verify email formate information");

		VerifyHandler.verifyElementPresent(SVOEnquiryContainer.emailFrom);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verify from email information");

		ActionHandler.pageDown();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		VerifyHandler.verifyElementPresent(SVOEnquiryContainer.Importance);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verify that importance is present");

		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(3);
		ActionHandler.click(SVOEnquiryContainer.cancelemail);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on cancel mail");
	}

	// User opens an enquiry with stages
	@And("^User opens an enquiry with \"([^\"]*)\" stage$")
	public void User_opens_an_enquiry_with_stage(String Stage) throws Throwable {
		ActionHandler.click(SVOEnquiryContainer.SearchBox);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		ActionHandler.clearAndSetText(SVOEnquiryContainer.SearchBox, Stage);
		CommonFunctions.attachScreenshot();
		ActionHandler.pressEnter();
		ActionHandler.wait(10);
		Reporter.addStepLog("Search for an enquiry");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.selectEnquiry);
		ActionHandler.wait(15);
		Reporter.addStepLog("Select enquiry");
		CommonFunctions.attachScreenshot();
	}

	// Open KMI from related links and verify there are no records available
	@And("^Open KMI from related links and verify there are no records available$")
	public void Open_KMI_from_related_links_and_verify_there_are_no_records_available() throws Throwable {
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		ActionHandler.pageArrowUp();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.KMI);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verify there are no records for KMI");
	}

	// Click on New button to create a new KMI
	@And("^Click on New button to create a new KMI$")
	public void Click_on_New_button_to_create_a_new_KMI() throws Throwable {
		ActionHandler.click(SVOEnquiryContainer.NewKMI);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on new KMI button");
	}

	// Save the KMI created
	@And("^Save the KMI created$")
	public void Save_the_KMI_created() throws Throwable {
		ActionHandler.click(SVOEnquiryContainer.SaveEnquiry);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Save the KMI");
	}

	// Enter the fields contact, product offering, brand and model
	@Then("^Enter the fields contact \"([^\"]*)\" product offering \"([^\"]*)\" brand \"([^\"]*)\" model \"([^\"]*)\"$")
	public void Enter_the_fields_contact_product_offering_brand_model(String contact, String productOffering,
			String brand, String model) throws Throwable {
		String pOffering[] = productOffering.split(",");
		productOffering = CommonFunctions.readExcelMasterData(pOffering[0], pOffering[1], pOffering[2]);

		String Contact[] = contact.split(",");
		contact = CommonFunctions.readExcelMasterData(Contact[0], Contact[1], Contact[2]);

		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String mo[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(mo[0], mo[1], mo[2]);

		System.out.println("All details are fetched from excel");

		// Below Code search for contact details then select value for contact
		ActionHandler.setText(SVOEnquiryContainer.KMIContact, contact);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		act1.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// Below Code is to select the product offering value from drop down
		ActionHandler.click(SVOEnquiryContainer.KMIproduct);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.ValueSelection(productOffering))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// Below Code search for brand name then select brand value
		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);
		ActionHandler.setText(SVOEnquiryContainer.KMIbrand, brand);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Actions act2 = new Actions(driver);
		act2.sendKeys(Keys.ARROW_DOWN).build().perform();
		act2.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// Below Code search for Model name then select brand value
		ActionHandler.setText(SVOEnquiryContainer.KMImodel, model);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Actions act3 = new Actions(driver);
		act3.sendKeys(Keys.ARROW_DOWN).build().perform();
		act3.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
	}

	// User opens an enquiry which is in new status
	@Then("^User opens an enquiry which is in new status$")
	public void User_Opens_Enquiry_New_Status() throws Throwable {
		ActionHandler.wait(3);
		Reporter.addStepLog("User creats a new enquiry to test");
		newEnquiry_SLA();

		Reporter.addStepLog("User checks the SLA status");
	}

	// Verify that the open enquiry SLA status is in progress
	@And("^Verify that the open enquiry SLA status is in progress$")
	public void Verify_Open_Enquiry_SLA_statua_In_Progress() throws Exception {
		ActionHandler.wait(3);
		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(2);
		ActionHandler.pageUp();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SLAInProgressStatus)) {
			Reporter.addStepLog("SLA status is in progress till first contact target date");
			CommonFunctions.attachScreenshot();
		}
		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

	}

	// User closes an enquiry for which SLA status is in progress
	@And("^User closes an enquiry for which SLA status is in progress$")
	public void User_closes_en_enquiry_SLA_Status_In_Progress() throws Exception {
		ActionHandler.wait(3);
		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.pageUp();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SLAInProgressStatus)) {
			Reporter.addStepLog("SLA status is in progress till first contact target date");
			CommonFunctions.attachScreenshot();
		}

		Reporter.addStepLog("SLA status is in progress, so user closes an enquiry");
		ActionHandler.wait(3);
		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.closeStageBtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.MarkCurrentEnquiry);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.closedState);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.selectClosedState);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.doneClosedState);
		ActionHandler.wait(6);
		CommonFunctions.attachScreenshot();

	}

	// Verify that an error occurred when user closes an enquiry without SLA Success
	// status
	@Then("^Verify that an error occurred when user closes an enquiry without SLA Success status$")
	public void Verify_An_Error_Occcurred_Closes_Enquiry_Without_SLA_Success() throws Exception {
		ActionHandler.wait(4);
		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SLAInProgressErrorMsg)) {
			Reporter.addStepLog("An error occurred when user closes an enquiry with SLA In progress status");
			CommonFunctions.attachScreenshot();
		}

		ActionHandler.click(SVOEnquiryContainer.errorClose);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

	}

	// Open an enquiry for which SLA is failed and reach till Final Escalation
	// Reminder Target Date
	@Then("^Open an enquiry for which SLA is failed and reach till Final Escalation Reminder Target Date$")
	public void Open_An_Enquiry_SLA_Reach_Final_Escalation_Date() throws Exception {
		ActionHandler.wait(3);
		Reporter.addStepLog("User moves to SLA Breached list");
		ActionHandler.wait(1);
		ActionHandler.click(SVOEnquiryContainer.RecentlyViewed);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.SLABreachEnquiry);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("Now user search an enquiry using filters");

		/*
		 * ActionHandler.click(SVOEnquiryContainer.filterOnSLABreach);
		 * ActionHandler.wait(3); CommonFunctions.attachScreenshot();
		 * 
		 * ActionHandler.click(SVOEnquiryContainer.filterByOwner);
		 * ActionHandler.wait(3); CommonFunctions.attachScreenshot();
		 * 
		 * ActionHandler.click(SVOEnquiryContainer.myEnquiryOption);
		 * ActionHandler.wait(4); CommonFunctions.attachScreenshot();
		 * 
		 * ActionHandler.click(SVOEnquiryContainer.filterDoneButton);
		 * ActionHandler.wait(4); CommonFunctions.attachScreenshot();
		 * 
		 * if(!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.filterSaveButton))
		 * { ActionHandler.wait(2);
		 * ActionHandler.click(SVOEnquiryContainer.FilterCloseBtn);
		 * ActionHandler.wait(4); CommonFunctions.attachScreenshot(); } else {
		 * ActionHandler.wait(2);
		 * ActionHandler.click(SVOEnquiryContainer.filterSaveButton);
		 * ActionHandler.wait(7); CommonFunctions.attachScreenshot(); }
		 */

		Reporter.addStepLog("User opens first enquiry from the available list");

		ActionHandler.click(SVOEnquiryContainer.firstEnquiryAllList);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(2);
		ActionHandler.pageUp();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User opens an enquiry for which SLA is failed and final escalation date has gone");
	}

	// User View Enquiry History
	@And("^User View Enquiry History$")
	public void User_View_Enquiry_History() throws Throwable {
		// User_search_enquiry_closed_reason_type(reasonType);

		Reporter.addStepLog("User Views an enquiry history");

		// ActionHandler.scrollToView(SVOEnquiryContainer.relatedQuickList);
		ActionHandler.wait(3);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		ActionHandler.wait(3);
		javaScriptUtil.clickElementByJS(SVOEnquiryContainer.enquiryHistoryLink);
		// ActionHandler.click(SVOEnquiryContainer.enquiryHistoryLink);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User can view Enquiry History");

		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

	}

	// User opens an enquiry which is in Discovery stage
	@Then("^User opens an enquiry which is in Discovery stage with Email \"([^\"]*)\"$")
	public void User_Opens_Enquiry_In_Discovery_Stage(String email) throws Throwable {
		String e[] = email.split(",");
		email = CommonFunctions.readExcelMasterData(e[0], e[1], e[2]);

		String client = "demo@mallinator.com";

		ActionHandler.wait(3);
		user_selects_Preferred_Contact_details_from_the_Potential_Matches_coming_after_entering_the_Email_Address(
				email);
		log_a_call_to_the_Enquiry_with_client(client);

		Reporter.addStepLog("User mark enquiry as Discovery");
		Mark_Current_Enquiry_Status();
		ActionHandler.wait(7);

		Reporter.addStepLog("User Opens an enquiry which is in Discovery stage");
	}

	// User changes the enquiry status from Discovery to Contacted stage
	@And("^User changes the enquiry status from Discovery to Contacted stage$")
	public void User_Changes_Enquiry_Status_to_Contacted() throws Exception {
		ActionHandler.wait(3);
		Reporter.addStepLog("User clicks on Contacted stage");

		ActionHandler.click(SVOEnquiryContainer.contactedTab);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.MarkCurrentEnquiry);
		ActionHandler.wait(6);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User changes enquiry status from discovery to contacted");
	}

	@And("^Verify that user is not received any email when an enquiry is contacted within 48 hours$")
	public void Verify_User_is_not_received_any_email_when_enquiry_contacted_within_48_hours() throws Exception {
		ActionHandler.wait(2);
		ActionHandler.pageUp();
		ActionHandler.wait(2);
		Reporter.addStepLog("Navigate to Details tab");
		ActionHandler.click(SVOEnquiryContainer.enqDetails);
		ActionHandler.wait(6);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(2);
		ActionHandler.pageUp();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SLAstatussuccess);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verify SLA status is success");

	}

	// User opens any open enquiry
	@Then("^User opens any open enquiry$")
	public void User_Opens_Enquiry() throws Throwable {
		String RecordType = "SVO Bespoke";
		ActionHandler.wait(3);
		Reporter.addStepLog("User creates a new Enquiry");

		User_Create_New_Enquiry(RecordType);
		enter_the_Enquiry_Title_for_the_Enquiry_creation();
		save_the_enquiry();

		ActionHandler.wait(7);

		Reporter.addStepLog("User Opens any new enquiry");
	}

	// User compose an email and attach some file from different location
	@And("^User compose an email and attach some file from different location$")
	public void User_Compose_Email_Attach_File_Different_Location() throws Throwable {
		ActionHandler.wait(2);
		user_clicks_on_Compose_Email_button();
		Reporter.addStepLog("User clicks on Compose Email Button");

		driver.switchTo().frame(0);
		ActionHandler.wait(3);

		ActionHandler.click(SVOEnquiryContainer.enquiryAttachFile);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Attach File Button");

		String NewWindow = driver.getWindowHandle();
		ActionHandler.wait(30);
		Set<String> attchFileWindow = driver.getWindowHandles();
		Iterator<String> i = attchFileWindow.iterator();
		while (i.hasNext()) {
			String attachWin = i.next();
			if (!NewWindow.equalsIgnoreCase(attachWin)) {
				driver.switchTo().window(attachWin);
				ActionHandler.wait(5);
				AssertHandler.assertElementPresent(SVOEnquiryContainer.attachFileError);
				ActionHandler.wait(3);
				CommonFunctions.attachScreenshot();
				driver.switchTo().window(NewWindow);
			}
		}

	}

	// Verify that User cannot find Reference Number
	@And("^Verify that User cannot find Reference Number for \"([^\"]*)\" enquiry$")
	public void Verify_User_cannot_Find_Reference_Number() throws Exception {
		Reporter.addStepLog("User verifies that no opportunity is linked up for closed[lost] enquiry");
		ActionHandler.wait(2);

		ActionHandler.scrollToView(SVOEnquiryContainer.OpportunityLinkEnqDetail);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.OpportunityLinkEnqDetail);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		String errorText = SVOEnquiryContainer.noOpportunityErrorMsg.getText();
		Reporter.addStepLog("Opportunity search error message is = " + errorText);
		CommonFunctions.attachScreenshot();

	}

	// Click on Drafts button
	@Then("^Click on Drafts button$")
	public void click_on_Drafts_button() throws Throwable {

		driver.switchTo().frame(0);

		ActionHandler.wait(20);

		ActionHandler.setText(SVOEnquiryContainer.Subject, "Test");
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter Subject for email");

		ActionHandler.scrollToView(SVOEnquiryContainer.saveemail);
		ActionHandler.wait(3);
		ActionHandler.click(SVOEnquiryContainer.saveemail);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on save mail");

		javaScriptUtil.clickElementByJS(SVOEnquiryContainer.EmailDrafts);
		ActionHandler.wait(15);
		CommonFunctions.attachScreenshot();

		String NewWindow = driver.getWindowHandle();
		ActionHandler.wait(30);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!NewWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);

				ActionHandler.click(SVOEnquiryContainer.closedraft);

				ActionHandler.wait(20);
				CommonFunctions.attachScreenshot();
				driver.switchTo().window(NewWindow);
			}
		}

		ActionHandler.wait(15);
		CommonFunctions.attachScreenshot();
		driver.switchTo().parentFrame();
	}

	// Click on the Cancel button
	@Then("^Click on the Cancel button$")
	public void click_on_the_Cancel_button() throws Throwable {
		ActionHandler.scrollToView(SVOEnquiryContainer.EmailCancel);
		ActionHandler.wait(5);
		javaScriptUtil.clickElementByJS(SVOEnquiryContainer.EmailCancel);
		ActionHandler.wait(20);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("The email is cancelled and user navigated to Enquiry page");
	}

	// Click on the Send button and verify the error
	@Then("^Click on the Send button and verify the error$")
	public void click_on_the_Send_button_and_verify_the_error() throws Throwable {
		driver.switchTo().frame(0);
		javaScriptUtil.clickElementByJS(SVOEnquiryContainer.EmailSend);
		ActionHandler.wait(30);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("The email is not sent and error message is displayed");
		driver.switchTo().parentFrame();
	}

	// Verify buttons on header and footer section
	@Then("^Verify buttons on header and footer section$")
	public void verify_buttons_on_header_and_footer_section() throws Throwable {
		driver.switchTo().frame(0);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		ActionHandler.scrollToView(SVOEnquiryContainer.cancelemail);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("Verify the buttons on header and footer section are same");
		driver.switchTo().parentFrame();
	}

	// Open the new opportunity created from related links
	@When("^Open the new opportunity created from related links$")
	public void open_the_new_opportunity_created_from_related_links() throws Throwable {
		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);
		ActionHandler.click(SVOEnquiryContainer.OpportunityLink);
		Reporter.addStepLog("User clicks on Opportunity link to see open Opportunity");
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(10);
		ActionHandler.click(SVOEnquiryContainer.SelectOpp);
		Reporter.addStepLog("User opens the Opportunity");
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
	}

	// Verify that the user cannot edit the Reference number field
	@Then("^Verify that the user cannot edit the Reference number field$")
	public void verify_that_the_user_cannot_edit_the_Reference_number_field() throws Throwable {
		ActionHandler.scrollToView(SVOEnquiryContainer.RefNumber);
		ActionHandler.wait(7);
		Reporter.addStepLog("User cannot edit the Reference number field");
		CommonFunctions.attachScreenshot();
	}

	// Edit auto populated fields Related
	@Then("^Edit auto populated fields Related to \"([^\"]*)\" and Importance \"([^\"]*)\" in Compose email page$")
	public void edit_auto_populated_fields_Related_to_and_Importance_in_Compose_email_page(String Related,
			String importance) throws Throwable {
		String re[] = Related.split(",");
		Related = CommonFunctions.readExcelMasterData(re[0], re[1], re[2]);

		String imp[] = importance.split(",");
		importance = CommonFunctions.readExcelMasterData(imp[0], imp[1], imp[2]);

		System.out.println("The details are fetched from excel");

		ActionHandler.wait(20);

		driver.switchTo().frame(0);

		ActionHandler.click(SVOEnquiryContainer.Relatedto);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Related to selected");

		ActionHandler.pageDown();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.Importance);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Importance selected");
		driver.switchTo().parentFrame();
		ActionHandler.wait(5);
	}

	// User opens an enquiry which is in Contacted stage
	@Then("^User opens an enquiry which is in Contacted stage with Email \"([^\"]*)\"$")
	public void User_Opens_Enquiry_in_Contacted_Stage(String email) throws Throwable {
		String e[] = email.split(",");
		email = CommonFunctions.readExcelMasterData(e[0], e[1], e[2]);

		String recordType = "SVO Bespoke";
		String client = "HR Owen";

		ActionHandler.wait(3);
		User_Create_New_Enquiry(recordType);
		enter_the_Enquiry_Title_for_the_Enquiry_creation();
		save_new_Enquiry();

		user_selects_Preferred_Contact_details_from_the_Potential_Matches_coming_after_entering_the_Email_Address(
				email);
		log_a_call_to_the_Enquiry_with_client(client);

		Reporter.addStepLog("User opens an enquiry which is in Contacted Stage");
	}

	// User changes enquiry status from Contacted to New stage
	@And("^User changes enquiry status from Contacted to New stage$")
	public void User_Changes_Enquiry_From_Contacted_To_New() throws Exception {
		ActionHandler.wait(2);
		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(1);
		ActionHandler.click(SVOEnquiryContainer.newEnquiryStatus);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.MarkCurrentEnquiry);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User changes enquiry status from Contacted to New stage");
	}

	// Verify that an error occurred when moving enquiry from Contacted to New
	@And("^Verify that an error occurred when moving enquiry from Contacted to New$")
	public void Verify_An_Error_Occurred_Moving_Enquiry_to_New() throws Exception {
		ActionHandler.wait(3);
		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SLAInProgressErrorMsg)) {
			Reporter.addStepLog("User cannot move backward from Contacted to New stage");
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
		}
	}

	// User opens a non-closed enquiry
	@Then("^User opens a non-closed enquiry$")
	public void User_Opnes_Non_Closed_Enquiry() throws Throwable {
		User_Opens_Enquiry();

		Reporter.addStepLog("User opens an enquiry which is in Contacted Stage");
	}

	// User changes the record type of a non-closed enquiry
	@And("^User changes the record type of a non-closed enquiry$")
	public void User_changes_record_type_non_closed_enquiry() throws Exception {
		String recordType = "Classic Service";

		ActionHandler.wait(3);
		ActionHandler.click(SVOEnquiryContainer.Dropdown);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(1);
		ActionHandler.click(SVOEnquiryContainer.changeRecordTypeBtn);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		/*
		 * String NewWindow = driver.getWindowHandle(); ActionHandler.wait(5);
		 * Set<String> changeRecordType = driver.getWindowHandles(); Iterator<String> i
		 * = changeRecordType.iterator(); while (i.hasNext()) { String changeRecord =
		 * i.next(); if (!NewWindow.equalsIgnoreCase(changeRecord)) {
		 * driver.switchTo().window(changeRecord); ActionHandler.wait(5);
		 * System.out.println("Driver moved to change record type window");
		 * ActionHandler.wait(2);
		 */
		ActionHandler.wait(3);
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.selectRecordType(recordType))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User changes record type");
		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.nextRType);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.saveEnquiry);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.nextRType)) {
			ActionHandler.wait(3);
			ActionHandler.click(SVOEnquiryContainer.cancelRType);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
		}

		/*
		 * driver.switchTo().window(NewWindow); } }
		 */

		ActionHandler.wait(2);
		String changeRType = SVOEnquiryContainer.recordTypeVerify.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Record type has been changed successfully = " + changeRType);

	}

	// User opens a closed enquiry
	@Then("^User opens a closed enquiry$")
	public void User_Opens_Closed_Enquiry() throws Throwable {
		String reasonType = "Qualified-Opportunity";
		ActionHandler.wait(3);

		User_search_enquiry_closed_reason_type(reasonType);

		Reporter.addStepLog("User Opens an enquiry which is in close stage");
	}

	// User changes the record type of a closed enquiry
	@And("^User changes the record type of a closed enquiry$")
	public void User_changes_Record_Type_Closed_Enquiry() throws Exception {
		ActionHandler.wait(3);
		User_changes_record_type_non_closed_enquiry();
		Reporter.addStepLog("User changes the record type of a closed enquiry");
	}

	// Verify that an error occurred when user changes record type of a closed
	// enquiry
	@Then("^Verify that an error occurred when user changes record type of a closed enquiry$")
	public void Verify_an_error_occurred_changes_record_type() throws Exception {
		ActionHandler.wait(2);
		VerifyHandler.verifyElementPresent(SVOEnquiryContainer.chRecordTypeErr);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Record type for closed enquiry cannot be changed");

		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.enquiryErrorClose);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.chRecTypeCancel);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
	}

	// User searches a vehicle from Vehicle Search Option
	@And("^User searches a vehicle from Vehicle Search Option$")
	public void User_searches_Vehicle_Vehicle_Search_option() throws Exception {
		String regiNumber = "123455";

		ActionHandler.wait(3);
		ActionHandler.click(SVOEnquiryContainer.Dropdown);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(1);
		ActionHandler.click(SVOEnquiryContainer.vehicleSearch);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		String NewWindow = driver.getWindowHandle();
		ActionHandler.wait(30);
		Set<String> vehicleSearch = driver.getWindowHandles();
		Iterator<String> i = vehicleSearch.iterator();
		while (i.hasNext()) {
			String vSearch = i.next();
			if (!NewWindow.equalsIgnoreCase(vSearch)) {
				driver.switchTo().window(vSearch);
				ActionHandler.setText(SVOEnquiryContainer.vSearchRegistrationNumber, regiNumber);
				ActionHandler.wait(2);
				CommonFunctions.attachScreenshot();

				ActionHandler.click(SVOEnquiryContainer.vSearchSearchBtn);
				ActionHandler.wait(3);
				CommonFunctions.attachScreenshot();

				ActionHandler.pageScrollDown();
				ActionHandler.wait(3);
				CommonFunctions.attachScreenshot();
				Reporter.addStepLog("Vehicle can be searched using registration number");

				ActionHandler.click(SVOEnquiryContainer.vSearchSelect);
				ActionHandler.wait(7);
				CommonFunctions.attachScreenshot();

				driver.switchTo().window(NewWindow);
			}
		}
		Reporter.addStepLog("User searches vehicle using Vehicle Search Option");
	}

	// Verify that Vehicle information get auto populated
	@Then("^Verify that Vehicle information get auto populated$")
	public void Verify_Vehicle_Information_Auto_Populated() throws Exception {
		ActionHandler.wait(2);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		ActionHandler.pageDown();
		ActionHandler.wait(2);
		ActionHandler.wait(3);
		String vehicleDetails = SVOEnquiryContainer.verifyVehicleDetails.getText();
		Reporter.addStepLog("Vehicle details are auto populated = " + vehicleDetails);
	}

	// Enter Incorrect Enquiry name
	@Then("^Enter Incorrect Enquiry name as \"([^\"]*)\" in the Enquiry search box$")
	public void enter_Incorrect_Enquiry_name_as_in_the_Enquiry_search_box(String EnquiryName) throws Throwable {
		ActionHandler.wait(3);
		ActionHandler.click(SVOEnquiryContainer.EnquirySearchBox);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		ActionHandler.clearAndSetText(SVOEnquiryContainer.EnquirySearchBox, EnquiryName);
		CommonFunctions.attachScreenshot();
		ActionHandler.pressEnter();
		ActionHandler.wait(10);
		Reporter.addStepLog("Search for an enquiry");
		CommonFunctions.attachScreenshot();
	}

	// Validate the Error Message
	@When("^Validate the Error Message$")
	public void validate_the_Error_Message() throws Throwable {
		ActionHandler.wait(5);
		VerifyHandler.verifyElementPresent(SVOEnquiryContainer.Errorencountered);
		ActionHandler.wait(15);
		Reporter.addStepLog("Error message is displayed as Please select the contact in the Contact Details section");
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOEnquiryContainer.errorClose);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("Navigate to Enquiry Tab");
		ActionHandler.click(SVOEnquiryContainer.enquiryTab);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
	}

	// User closes an enquiry
	@Then("^User closes an enquiry with \"([^\"]*)\" status$")
	public void User_closes_an_enquiry_with_status(String Status) throws Throwable {
		String Reason = "Product - no longer available";

		ActionHandler.wait(4);
		ActionHandler.click(SVOEnquiryContainer.closedBtn);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOEnquiryContainer.MarkCurrentEnquiry);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOEnquiryContainer.closedState);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOEnquiryContainer.closeState);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Closed[Lost]");

		ActionHandler.wait(3);
		ActionHandler.click(SVOEnquiryContainer.Closedreason);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.ValueSelection(Reason))));
		ActionHandler.wait(5);

		ActionHandler.click(SVOEnquiryContainer.doneClosedState);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User closes an enquiry with Closed[Lost] status");
	}

	// ^Validate error message displayed
	@Then("^Validate error message displayed$")
	public void validate_error_message_displayed() throws Throwable {
		ActionHandler.wait(3);
		VerifyHandler.verifyElementPresent(SVOEnquiryContainer.NoItems);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Error Message is displayed");
		CommonFunctions.attachScreenshot();

	}

	// User navigates to All Enquiries list from Recently Viewed Enquiry list
	@When("^User navigates to All Enquiries list from Recently Viewed Enquiry list$")
	public void user_navigates_to_All_Enquiries_list_from_Recently_Viewed_Enquiry_list() throws Throwable {
		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.listView);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.AllEnquiries);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("All Enquiries list is opened");
	}

	// Validate user can view all enquiries with status and enquiry owner
	@Then("^Validate user can view all enquiries with status and enquiry owner$")
	public void validate_user_can_view_all_enquiries_with_status_and_enquiry_owner() throws Throwable {
		ActionHandler.wait(3);
		VerifyHandler.verifyElementPresent(SVOEnquiryContainer.Owner);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Owner list is validated");

		VerifyHandler.verifyElementPresent(SVOEnquiryContainer.Status);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Status list is validated");

		ActionHandler.wait(2);
	}

	// User Create New Enquiry with Record Type
	@Then("^User Create New Enquiry with Record Type \"([^\"]*)\"$")
	public void User_Create_New_Enquiry(String recordType) throws Exception {
		String rType[] = recordType.split(",");
		recordType = CommonFunctions.readExcelMasterData(rType[0], rType[1], rType[2]);

		ActionHandler.wait(4);
		Reporter.addStepLog("User Creates new enquiry");
		ActionHandler.click(SVOEnquiryContainer.newEnquiryBtn);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User Needs to select any record type option");
		ActionHandler.wait(3);
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.selectRecordType(recordType))));
		Reporter.addStepLog("User Select required Record Type");
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(4);
		ActionHandler.click(SVOEnquiryContainer.nextRType);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigate to enter enquiry details page");
	}

	// Enter the Enquiry Title for the Enquiry creation
	@Then("^Enter the Enquiry Title for the Enquiry creation$")
	public void enter_the_Enquiry_Title_for_the_Enquiry_creation() throws Throwable {
		String enquiryTitle = "Test_Enquiry_";
		double randomNumber = getRandomIntegerBetweenRange(0, 1000);

		ActionHandler.wait(7);
		ActionHandler.setText(SVOEnquiryContainer.EnquiryTitle, enquiryTitle + randomNumber);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		System.out.println("User has entered Enquiry Title");
	}

	public void NewEnquiryCreationforSVOBespoke() throws Exception {
		ActionHandler.wait(4);
		Reporter.addStepLog("User Creates new enquiry");
		ActionHandler.click(SVOEnquiryContainer.newEnquiryBtn);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects SVO Bespoke Record Type");
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(4);
		ActionHandler.click(SVOEnquiryContainer.nextRType);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigate to enter enquiry details page");
		ActionHandler.wait(3);
		String enquiryTitle = "Test_Enquiry_";
		double randomNumber = getRandomIntegerBetweenRange(0, 1000);
		ActionHandler.wait(7);
		ActionHandler.setText(SVOEnquiryContainer.EnquiryTitle, enquiryTitle + randomNumber);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		System.out.println("User has entered Enquiry Title");
		ActionHandler.wait(3);
		ActionHandler.click(SVOEnquiryContainer.SaveEnquiry);
		ActionHandler.wait(20);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves Enquiry");

	}

	// User Edits an enquiry and Enter Client
	@When("^User Edits an enquiry and Enter Client \"([^\"]*)\" and Preferred Contact \"([^\"]*)\"$")
	public void user_Edits_an_enquiry_and_Enter_Client_and_Preferred_Contact(String client, String pContact)
			throws Throwable {
		String c[] = client.split(",");
		client = CommonFunctions.readExcelMasterData(c[0], c[1], c[2]);

		String pc[] = pContact.split(",");
		pContact = CommonFunctions.readExcelMasterData(pc[0], pc[1], pc[2]);

		ActionHandler.wait(4);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(4);
		ActionHandler.pageScrollDown();
		// javaScriptUtil.scrollIntoView(SVOEnquiryContainer.clientLabel);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.EditClient);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.Client, client);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOEnquiryContainer.CSpCntSearch);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.clientNameSearch(client))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(SVOEnquiryContainer.PreferredContact, pContact);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOEnquiryContainer.PCntSearch);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.retailerAccountSearch(pContact))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User can add Client and Preferred contact without entering an email");
	}

	// Save the enquiry
	@When("^Save the enquiry$")
	public void save_the_enquiry() throws Throwable {
		ActionHandler.wait(3);
		ActionHandler.click(SVOEnquiryContainer.SaveEnquiry);
		ActionHandler.wait(20);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves Enquiry");

	}

	// Log a call to that Enquiry with Client
	@When("^Log a call to that Enquiry with Client \"([^\"]*)\"$")
	public void log_a_call_to_that_Enquiry_with_Client(String Client) throws Throwable {
		String c[] = Client.split(",");
		Client = CommonFunctions.readExcelMasterData(c[0], c[1], c[2]);

		String commentText = "Customer has requested a  report";

		ActionHandler.wait(3);

		Reporter.addStepLog("User Navigate to Activity Tab");
		ActionHandler.click(SVOEnquiryContainer.activityTab);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User clicks on Create a log call");
		ActionHandler.wait(3);
		ActionHandler.click(SVOEnquiryContainer.newLogCall);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.subjectDD);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOEnquiryContainer.callLog);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(SVOEnquiryContainer.name, Client);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOEnquiryContainer.nameSearch);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.clientNameSearch(Client))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(SVOEnquiryContainer.commentText, commentText);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.saveLog);
		ActionHandler.wait(15);
		CommonFunctions.attachScreenshot();
	}

	// From the Contacted Stage Mark Enquiry Status as Complete
	@When("^From the Contacted Stage Mark Enquiry Status as Complete$")
	public void from_the_Contacted_Stage_Mark_Enquiry_Status_as_Complete() throws Throwable {
		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(5);
		ActionHandler.click(SVOEnquiryContainer.MarkEnquiryComplete);
		ActionHandler.wait(15);
		Reporter.addStepLog("User clicks on Mark Enquiry Status as Complete");
		CommonFunctions.attachScreenshot();
	}

	// Mark the Enquiry Status as Complete with Qualified-Opportunity state
	@Then("^Mark the Enquiry Status as Complete with Qualified-Opportunity state$")
	public void mark_the_Enquiry_Status_as_Complete_with_Qualified_Opportunity_state() throws Throwable {
		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(5);
		ActionHandler.click(SVOEnquiryContainer.MarkEnquiryComplete);
		ActionHandler.wait(15);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.closedState);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOEnquiryContainer.selectClosedState);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOEnquiryContainer.doneClosedState);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

	}

	// Validate the Error Message Displayed
	@Then("^Validate the Error Message	Displayed$")
	public void validate_the_Error_Message_Displayed() throws Throwable {
		ActionHandler.wait(5);
		VerifyHandler.verifyElementPresent(SVOEnquiryContainer.CSCloseStateError);
		ActionHandler.wait(4);
		ActionHandler.click(SVOEnquiryContainer.errorClose);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("Navigate to Enquiry Tab");
		ActionHandler.click(SVOEnquiryContainer.enquiryTab);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
	}

	// Use Creates a New Enquiry
	@When("^Use Creates a New Enquiry$")
	public void use_Creates_a_New_Enquiry() throws Throwable {
		NewEnquiryCreationforSVOBespoke();
	}

	// ^User adds Additional Related
	@Then("^User adds Additional Related to \"([^\"]*)\" as \"([^\"]*)\"$")
	public void user_adds_Additional_Related_to_as(String RelatedTo, String ContactName) throws Throwable {
		String c[] = ContactName.split(",");
		ContactName = CommonFunctions.readExcelMasterData(c[0], c[1], c[2]);
		driver.switchTo().frame(0);
		ActionHandler.wait(2);
		ActionHandler.pageDown();
		ActionHandler.click(SVOEnquiryContainer.AdditionalRelated);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Additional Related To");
		// ActionHandler.click(SVOEnquiryContainer.RelatedTo);
		ActionHandler.setText(SVOEnquiryContainer.RelatedTo, RelatedTo);
		ActionHandler.wait(2);
		ActionHandler.pressEnter();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Adds Contact from the dropdown");
		// ActionHandler.setText(SVOEnquiryContainer.RelatedTo,"Contact");
		// ActionHandler.pressEnter();
		ActionHandler.wait(2);
		// CommonFunctions.attachScreenshot();
		// Reporter.addStepLog("User Adds Contact from the dropdown");
		ActionHandler.clearAndSetText(SVOEnquiryContainer.AddContact, ContactName);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Adds Contact Name from the dropdown");

	}

	// User Saves the Email
	@Then("^User Saves the Email$")
	public void user_Saves_the_Email() throws Throwable {
		ActionHandler.click(SVOEnquiryContainer.SaveEmail);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Saves the Email");
		driver.switchTo().parentFrame();
		ActionHandler.wait(2);

	}

	// User Navigates to Opportunities link with Enquiry Title
	@Then("^User Navigates to Opportunities link	with Enquiry Title \"([^\"]*)\"$")
	public void user_Navigates_to_Opportunities_link_with_Enquiry_Title(String enquiryTitle) throws Throwable {
		String Et[] = enquiryTitle.split(",");
		enquiryTitle = CommonFunctions.readExcelMasterData(Et[0], Et[1], Et[2]);
		ActionHandler.pageDown();
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.OppSearch(enquiryTitle))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks on Opportunities Link");
	}

	// User Validates the Reference Number in the Details Section
	@Then("^User Validates the Reference Number in the Details Section$")
	public void user_Validates_the_Reference_Number_in_the_Details_Section() throws Throwable {
		ActionHandler.pageDown();
		// verificationCode = SVOtestContainer.outlookVerificationCode.getText();
		// vCode = verificationCode.substring(510, 515);
		// System.out.println("Verification Code is = "+vCode);

		Rnumber = SVOEnquiryContainer.ReferenceNumber.getText();
		String referenceNumber[] = Rnumber.split("-");
		String ref1 = referenceNumber[0];
		String ref2 = referenceNumber[1];
		String ref3 = referenceNumber[2];

		String RecType = "CLS";
		String Year = "2021";
		// SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy");
		// String date = dateFormat.format(new Date());
		// System.out.println(date);
		// String brands = "L";

		if (Year.contains(ref2)) {
			System.out.println("Record Type format is validated successfully");
		}
		if (RecType.contains(ref1)) {
			System.out.println("Year format is validated successfully");
		}
		// if (brands.contains(ref3)) {
		// System.out.println("Brand format is not validated successfully");
		// }

	}

	// Enter Contact Details
	@Then("^Enter Contact Details for \"([^\"]*)\"$")
	public void enter_Contact_Details_for(String pContact) throws Throwable {
		String pc[] = pContact.split(",");
		pContact = CommonFunctions.readExcelMasterData(pc[0], pc[1], pc[2]);
		ActionHandler.wait(4);
		// ActionHandler.click(SVOEnquiryContainer.EditPreferredContact);
		// ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(SVOEnquiryContainer.ContactDetails, pContact);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOEnquiryContainer.nameSearch);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.retailerAccountSearch(pContact))));
		ActionHandler.wait(5);
		Reporter.addStepLog("User adds the Contact Details");
		CommonFunctions.attachScreenshot();
	}

	// Log a email to the Enquiry with client
	@Then("^Log a email to the Enquiry with client \"([^\"]*)\"$")
	public void log_a_email_to_the_Enquiry_with_client(String client) throws Throwable {
		String C[] = client.split(",");
		client = CommonFunctions.readExcelMasterData(C[0], C[1], C[2]);

		String commentText = "Customer has requested a  report";
		String Subject = "EMAIL";

		ActionHandler.wait(3);
		ActionHandler.pageUp();
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(5);
		Reporter.addStepLog("User Navigate to Activity Tab");
		ActionHandler.click(SVOEnquiryContainer.activityTab);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Activity tab");

		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User clicks on log a call");
		ActionHandler.wait(3);
		ActionHandler.click(SVOEnquiryContainer.LogCall);

		Reporter.addStepLog("User clicks on Create a log call");
		ActionHandler.wait(3);
		ActionHandler.click(SVOEnquiryContainer.newLogCall);
		ActionHandler.wait(7);
		Reporter.addStepLog("click on create a log");

		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.subjectDD, Subject);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Input subject as Email");

		ActionHandler.setText(SVOEnquiryContainer.name, client);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOEnquiryContainer.nameSearch);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.clientNameSearch(client))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Input client information");

		ActionHandler.setText(SVOEnquiryContainer.commentText, commentText);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Input comment");

		ActionHandler.click(SVOEnquiryContainer.saveLog);
		ActionHandler.wait(20);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Save the log");
	}

	// Log an email to that Enquiry with Client
	@When("^Log an email to that Enquiry with Client \"([^\"]*)\"$")
	public void log_an_email_to_that_Enquiry_with_Client(String Client) throws Throwable {
		String c[] = Client.split(",");
		Client = CommonFunctions.readExcelMasterData(c[0], c[1], c[2]);
		String commentText = "Customer has requested an email log";
		String subject = "Email";
		/*
		 * ActionHandler.wait(3); ActionHandler.pageScrollDown();
		 * ActionHandler.pageArrowUp(); ActionHandler.pageArrowUp();
		 * Reporter.addStepLog("User Navigate to Activity Tab");
		 * ActionHandler.click(SVOEnquiryContainer.activityTab); ActionHandler.wait(7);
		 * CommonFunctions.attachScreenshot();
		 */

		Reporter.addStepLog("User clicks on Create a log call");
		ActionHandler.wait(3);
		ActionHandler.click(SVOEnquiryContainer.newLogCall);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.subjectDD, subject);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(SVOEnquiryContainer.name, Client);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOEnquiryContainer.nameSearch);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.clientNameSearch(Client))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(SVOEnquiryContainer.commentText, commentText);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.saveLog);
		ActionHandler.wait(15);
		CommonFunctions.attachScreenshot();
	}

	// User clicks on the change Owner and enter Owner
	@When("^User clicks on the change Owner and enter Owner as \"([^\"]*)\"$")
	public void user_clicks_on_the_change_Owner_and_enter_Owner_as(String Owner) throws Throwable {
		String O[] = Owner.split(",");
		Owner = CommonFunctions.readExcelMasterData(O[0], O[1], O[2]);

		ActionHandler.wait(3);
		ActionHandler.click(SVOEnquiryContainer.EnqChangeOwner);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on change owner");

		ActionHandler.wait(2);
		ActionHandler.setText(SVOEnquiryContainer.SearchUsers, Owner);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter new owner");
		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.ChOwnerSearch);
		ActionHandler.wait(4);
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.ownerSelection(Owner))));
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Select new owner");

		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.Ownersentmail);
		ActionHandler.wait(2);
		Reporter.addStepLog("Deselect sent email button");

		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.SubmitOwnerBtn);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on change owner button");

	}

	// Log a email to the Enquiry
	@Then("^Log a email to the Enquiry$")
	public void log_a_email_to_the_Enquiry() throws Throwable {
		ActionHandler.wait(3);
		ActionHandler.pageUp();
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(5);
		Reporter.addStepLog("User Navigate to Emails Tab");
		ActionHandler.click(SVOEnquiryContainer.EmailTab);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Email tab");

		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(3);
		Reporter.addStepLog("Click on Send an Email");
		ActionHandler.click(SVOEnquiryContainer.SendanEmail);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
	}

	// User Creates a new Programme with Status
	@Then("^User Creates a new Programme with Status \"([^\"]*)\", Product offering \"([^\"]*)\", Brand \"([^\"]*)\", Model \"([^\"]*)\"$")
	public void User_creates_New_Programme(String Status, String ProOffering, String brand, String model)
			throws Exception {
		String client = "Jaguar Land Rover Internal Use";

		String s[] = Status.split(",");
		Status = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String po[] = ProOffering.split(",");
		ProOffering = CommonFunctions.readExcelMasterData(po[0], po[1], po[2]);

		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String m[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		ActionHandler.wait(3);
		ActionHandler.pageScrollDown();
		// javaScriptUtil.scrollIntoView(SVOEnquiryContainer.VehicleDetailsSection);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOEnquiryContainer.editProgrammeField);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.programmeField)) {
			String ProgrammeName = "Test_Programme";
			ActionHandler.wait(3);
			ActionHandler.click(SVOEnquiryContainer.programmeField);
			ActionHandler.wait(3);

			Actions act1 = new Actions(driver);
			act1.sendKeys(Keys.ENTER).build().perform();
			ActionHandler.wait(6);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("One pop window opens for creating a new Programme");

			ActionHandler.wait(2);
			ActionHandler.setText(SVOEnquiryContainer.newVehicleRecType, ProgrammeName);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.wait(2);
			ActionHandler.click(SVOEnquiryContainer.NewProgrammeStatus);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.SourceSelection(Status))));
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.wait(2);
			ActionHandler.click(SVOEnquiryContainer.NewProgrammeProOffering);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.SourceSelection(ProOffering))));
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.wait(2);
			ActionHandler.setText(SVOEnquiryContainer.NewProgrammeBuildCapacity, "10");
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();

			ActionHandler.wait(2);
			ActionHandler.pageScrollDown();
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			ActionHandler.setText(SVOEnquiryContainer.brand, brand);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(SVOEnquiryContainer.brandSearch);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.brandSearch(brand))));
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.wait(2);
			ActionHandler.setText(SVOEnquiryContainer.model, model);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(SVOEnquiryContainer.modelSearch);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(2);
			ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.NewProModelSearch(model))));
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.wait(2);
			ActionHandler.click(SVOEnquiryContainer.newVehicleSaveBtn);
			ActionHandler.wait(6);
			CommonFunctions.attachScreenshot();
		}

		ActionHandler.wait(3);
		ActionHandler.pageUp();
		ActionHandler.wait(3);
		ActionHandler.pageUp();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.newEnqClient, client);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters client information");

		ActionHandler.wait(3);
		ActionHandler.click(SVOEnquiryContainer.saveEnquiry);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User creates a new programme");
		ActionHandler.wait(2);
		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
	}

	// Validate that preferred contact details is blank
	@Then("^Validate that preferred contact details is blank$")
	public void validate_that_preferred_contact_details_is_blank() throws Exception {
		ActionHandler.wait(5);
		ActionHandler.pageDown();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(5);
	}

	// Navigate to KMI section
	@Then("^Navigate to KMI section$")
	public void navigate_to_KMI_section() throws Throwable {
		ActionHandler.wait(5);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);
		javaScriptUtil.clickElementByJS(SVOEnquiryContainer.KMILink);
		ActionHandler.wait(3);
		Reporter.addStepLog("User navigate to KMI section");
		CommonFunctions.attachScreenshot();

	}

	// User created New KMI with fields like Contact
	@Then("^User created New KMI with fields like Contact \"([^\"]*)\" ProductOffering \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\"$")
	public void user_created_New_KMI_with_fields_like_Contact_ProductOffering_Brand_Model(String Contact,
			String ProductOffering, String brand, String model) throws Throwable {

		String cnt[] = Contact.split(",");
		Contact = CommonFunctions.readExcelMasterData(cnt[0], cnt[1], cnt[2]);

		String PrdOfr[] = ProductOffering.split(",");
		ProductOffering = CommonFunctions.readExcelMasterData(PrdOfr[0], PrdOfr[1], PrdOfr[2]);

		String m[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		// click on New button//
		ActionHandler.wait(2);
		ActionHandler.click(SVOEnquiryContainer.NewKMIbtn);

		// user selects contact//
		ActionHandler.wait(4);
		ActionHandler.setText(SVOEnquiryContainer.Contacttxtbx, Contact);
		ActionHandler.wait(4);
		Actions acts = new Actions(driver);
		acts.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(3);
		acts.sendKeys(Keys.ENTER).build().perform();
		CommonFunctions.attachScreenshot();

		// User selects product offering//
		ActionHandler.wait(4);
		ActionHandler.click(SVOEnquiryContainer.ProductOffering);
		ActionHandler.wait(4);
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.PrOfSelection(ProductOffering))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// user selects brand//
		ActionHandler.pageCompleteScrollDown();
		ActionHandler.setText(SVOEnquiryContainer.Brandtxtbx, brand);
		ActionHandler.wait(4);
		Actions Bnd = new Actions(driver);
		Bnd.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(3);
		Bnd.sendKeys(Keys.ENTER).build().perform();
		CommonFunctions.attachScreenshot();

		// user selects model//
		ActionHandler.wait(3);
		ActionHandler.setText(SVOEnquiryContainer.Modeltxtbx, model);
		ActionHandler.wait(4);
		Actions mdl = new Actions(driver);
		mdl.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(3);
		mdl.sendKeys(Keys.ENTER).build().perform();
		CommonFunctions.attachScreenshot();

		// user click on save button//
		ActionHandler.wait(3);
		ActionHandler.click(SVOEnquiryContainer.SaveKMI);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

	}

	// User changes the Product offering of KMI
	@Then("^User changes the Product offering of KMI to \"([^\"]*)\"$")
	public void user_changes_the_Product_offering_of_KMI_to(String ProductOffering) throws Throwable {
		String PrdOfr[] = ProductOffering.split(",");
		ProductOffering = CommonFunctions.readExcelMasterData(PrdOfr[0], PrdOfr[1], PrdOfr[2]);

		// user click on edit button//
		ActionHandler.wait(5);
		ActionHandler.click(SVOEnquiryContainer.EditKMIdrpdn);
		ActionHandler.wait(5);
		ActionHandler.click(SVOEnquiryContainer.EditKMIBtn);
		ActionHandler.wait(5);

		// user changes the product offering//
		ActionHandler.wait(4);
		ActionHandler.click(SVOEnquiryContainer.ProductOffering);
		ActionHandler.wait(4);
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.PrOfSelection(ProductOffering))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

	}

	// User save the KMI Record
	@Then("^User save the KMI Record$")
	public void user_save_the_KMI_Record() throws Throwable {
		// user click on save button//
		ActionHandler.wait(3);
		ActionHandler.click(SVOEnquiryContainer.SaveKMI);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
	}

	// User change the owner of KMI
	@Then("^User change the owner of KMI to \"([^\"]*)\"$")
	public void user_change_the_owner_of_KMI_to(String Owner) throws Throwable {
		String O[] = Owner.split(",");
		Owner = CommonFunctions.readExcelMasterData(O[0], O[1], O[2]);

		// selects created KMI//
		ActionHandler.wait(3);
		ActionHandler.click(SVOEnquiryContainer.KMIcheckbx);

		// user click on change owner button//
		ActionHandler.wait(3);
		ActionHandler.click(SVOEnquiryContainer.ChangeOwnerBtn);

		// user selects new owner //
		ActionHandler.wait(5);
		ActionHandler.setText(SVOEnquiryContainer.SearchUsersTxtbx, Owner);
		ActionHandler.wait(3);
		Actions user = new Actions(driver);
		user.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(3);
		user.sendKeys(Keys.ENTER).build().perform();
		CommonFunctions.attachScreenshot();

		// user click on submit button//
		ActionHandler.wait(3);
		ActionHandler.click(SVOEnquiryContainer.SubmitOwnerBtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
	}

	// User delete the KMI Record
	@Then("^User delete the KMI Record$")
	public void user_delete_the_KMI_Record() throws Throwable {
		ActionHandler.wait(3);
		ActionHandler.click(SVOEnquiryContainer.DeleteKMIBtn);
		ActionHandler.wait(5);
		ActionHandler.click(SVOEnquiryContainer.DeletePopUp);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User delete the KMI Record");
	}

	// User click on newly created KMI
	@Then("^User click on newly created KMI$")
	public void user_click_on_newly_created_KMI() throws Throwable {
		// user click on first KMI Record//
		ActionHandler.wait(5);
		// javaScriptUtil.clickElementByJS(SVOEnquiryContainer.FirstKMI);
		ActionHandler.click(SVOEnquiryContainer.FirstKMI);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on newly created KMI");
	}

	// User navigate to Related tab of KMI
	@Then("^User navigate to Related tab of KMI$")
	public void user_navigate_to_Related_tab_of_KMI() throws Throwable {
		ActionHandler.wait(5);
		ActionHandler.click(SVOEnquiryContainer.RelatedTab);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate to Related tab of KMI");
	}

	// User creates new enquiry and link to KMI
	@Then("^User creates new enquiry and link to KMI$")
	public void user_creates_new_enquiry_and_link_to_KMI() throws Throwable {
		// user click on New button//
		ActionHandler.wait(5);
		ActionHandler.click(SVOEnquiryContainer.NewBtn);

		// user selects SVO Bespoke enquiry and click on next button//
		ActionHandler.wait(5);
		ActionHandler.click(SVOEnquiryContainer.nextRType);
		String enquiryTitle = "Test_Enquiry_";
		double randomNumber = getRandomIntegerBetweenRange(0, 1000);

		ActionHandler.wait(7);
		ActionHandler.setText(SVOEnquiryContainer.enquiryTitle, enquiryTitle + randomNumber);
		ActionHandler.wait(3);
		ActionHandler.click(SVOEnquiryContainer.SaveKMIenq);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user creates new enquiry");
	}

	// User navigate to Enquiry
	@Then("^User navigate to Enquiry$")
	public void user_navigate_to_Enquiry() throws Throwable {
		ActionHandler.wait(5);
		javaScriptUtil.clickElementByJS(SVOEnquiryContainer.FirstEnqKMI);
		ActionHandler.wait(5);
		Reporter.addStepLog("User navigate to Enquiry");
		CommonFunctions.attachScreenshot();
	}

	// Change the owner of Enquiry
	@Then("^Change the owner of Enquiry to \"([^\"]*)\"$")
	public void change_the_owner_of_Enquiry_to(String Owner) throws Throwable {
		String O[] = Owner.split(",");
		Owner = CommonFunctions.readExcelMasterData(O[0], O[1], O[2]);
		// user click on change owner button//
		ActionHandler.wait(3);
		ActionHandler.click(SVOEnquiryContainer.ChangeOwnerEnq);

		// user selects new owner //
		ActionHandler.wait(5);
		ActionHandler.setText(SVOEnquiryContainer.SearchUsersTxtbx, Owner);
		ActionHandler.wait(3);
		Actions user = new Actions(driver);
		user.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(3);
		user.sendKeys(Keys.ENTER).build().perform();
		CommonFunctions.attachScreenshot();

		// user click on submit button//
		ActionHandler.wait(3);
		ActionHandler.click(SVOEnquiryContainer.SubmitOwnerBtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

	}

	// Create a new enquiry with title
	@Then("^Create a new enquiry with title \"([^\"]*)\" source KMI \"([^\"]*)\"$")
	public void create_a_new_enquiry_with_title_source_KMI(String enquiryTitle, String sourceKMI) throws Throwable {
		String Et[] = enquiryTitle.split(",");
		enquiryTitle = CommonFunctions.readExcelMasterData(Et[0], Et[1], Et[2]);

		String KMI[] = sourceKMI.split(",");
		sourceKMI = CommonFunctions.readExcelMasterData(KMI[0], KMI[1], KMI[2]);

		double randomNumber = getRandomIntegerBetweenRange(0, 1000);
		ActionHandler.wait(5);
		ActionHandler.setText(SVOEnquiryContainer.enquiryTitle, enquiryTitle + randomNumber);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.scrollToView(SVOEnquiryContainer.sourceKMI);
		ActionHandler.wait(5);
		ActionHandler.setText(SVOEnquiryContainer.sourceKMI, sourceKMI);
		ActionHandler.wait(5);
		Actions act3 = new Actions(driver);
		act3.sendKeys(Keys.ARROW_DOWN).build().perform();
		act3.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		System.out.println("User has entered Enquiry Title");
	}
	
	//testing

	// Delete the KMI from the list
	@And("^Delete the KMI from the list$")
	public void delete_the_KMI_from_the_list() throws Throwable {
		ActionHandler.click(SVOEnquiryContainer.KMIdropdown);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOEnquiryContainer.KMIdelete);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOEnquiryContainer.KMIdeleteconfirm);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user deletes the KMI");
	}

	// Open the KMI created
	@And("^Open the KMI created$")
	public void open_the_KMI_created() throws Throwable {
		ActionHandler.wait(5);
		ActionHandler.click(SVOEnquiryContainer.SelectKMI);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Open the KMI created");
	}

	// Navigate to Related tab
	@And("^Navigate to Related tab$")
	public void navigate_to_related_tab() throws Throwable {
		ActionHandler.click(SVOEnquiryContainer.KMIRelated);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user navigates to related tab");
	}

	// Create a new enquiry of type and title
	@Then("^Create a new enquiry of type \"([^\"]*)\" and title \"([^\"]*)\"$")
	public void create_a_new_enquiry_of_type(String recordType, String enquiryTitle) throws Throwable {
		String rType[] = recordType.split(",");
		recordType = CommonFunctions.readExcelMasterData(rType[0], rType[1], rType[2]);

		String Et[] = enquiryTitle.split(",");
		enquiryTitle = CommonFunctions.readExcelMasterData(Et[0], Et[1], Et[2]);

		double randomNumber = getRandomIntegerBetweenRange(0, 1000);
		ActionHandler.wait(4);
		Reporter.addStepLog("User Creates new enquiry");
		ActionHandler.click(SVOEnquiryContainer.KMIEnquiry);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User Needs to select any record type option");
		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.selectRecordType(recordType))));
		Reporter.addStepLog("User Select required Record Type");
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOEnquiryContainer.nextRType);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.KMIEnquiryTitle, enquiryTitle + randomNumber);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
	}

	// Click on Closed state and mark as current enquiry status
	@Then("^Click on Closed state and mark as current enquiry status$")
	public void click_on_closed_state_and_mark_as_current_enquiry_status() throws Exception {
		ActionHandler.wait(3);
		ActionHandler.click(SVOEnquiryContainer.EnqClosed);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOEnquiryContainer.MarkCurrentEnquiry);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user clicks on closed state and mark as current enquiry status");
	}

	// Click on Closed state dropdown and verify Qualified - KMI option is not
	// available for the user
	@Then("^Click on Closed state dropdown and verify Qualified - KMI option is not available for the user$")
	public void click_on_closed_state_dropdown_and_verify_Qualified_KMI_option_is_not_available_for_the_user()
			throws Exception {
		ActionHandler.click(SVOEnquiryContainer.closedState);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies that Qualified - KMI option is not available");
	}

	// Click on Cancel
	@Then("^Click on Cancel$")
	public void click_on_cancel() throws Exception {
		ActionHandler.click(SVOEnquiryContainer.EnqCancel);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User cancels Enquiry");
	}

	// Navigate to Emails tab
	@Then("^Navigate to Emails tab$")
	public void navigate_to_emails_tab() throws Exception {

		ActionHandler.wait(5);
		driver.navigate().refresh();
		ActionHandler.wait(15);

		ActionHandler.pageDown();
		ActionHandler.wait(5);
		ActionHandler.click(SVOEnquiryContainer.EnqEmails);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Emails tab");
	}

	// Click on e2a send an email
	@Then("^Click on e2a send an email$")
	public void click_on_e2a_send_an_email() throws Exception {
		ActionHandler.wait(3);
		ActionHandler.click(SVOEnquiryContainer.e2aEmail);
		ActionHandler.wait(20);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens email page");
	}

	// edit the required information with Subject and click on Send
	@Then("^edit the required information as to \"([^\"]*)\" Subject \"([^\"]*)\" and click on Send$")
	public void edit_the_required_information_as_to_Subject_and_click_on_send(String To, String Subject)
			throws Throwable {
		String t[] = To.split(",");
		To = CommonFunctions.readExcelMasterData(t[0], t[1], t[2]);

		String s[] = Subject.split(",");
		Subject = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		System.out.println("All details are fetched from excel");

		ActionHandler.wait(5);

		driver.switchTo().frame(0);

		ActionHandler.setText(SVOEnquiryContainer.Additionalto, To);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter To field");

		ActionHandler.pageDown();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.Subject, Subject);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter Subject for email");

		ActionHandler.scrollToView(SVOEnquiryContainer.sendemail);
		ActionHandler.wait(3);
		ActionHandler.click(SVOEnquiryContainer.sendemail);
		ActionHandler.wait(20);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on send mail");
	}

	// Click on e2a email and open the sent email
	@Then("^Click on e2a email and open the sent email$")
	public void click_on_e2a_email_and_open_the_sent_email() throws Exception {
		ActionHandler.click(SVOEnquiryContainer.Opene2aEmail);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		// ActionHandler.click(SVOEnquiryContainer.OpenEmail);
		ActionHandler.wait(15);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens the sent email");
	}

	// Click on Reply button
	@Then("^Click on Reply button$")
	public void Click_on_Reply_button() throws Exception {
		driver.switchTo().frame(0);
		ActionHandler.wait(5);
		ActionHandler.click(SVOEnquiryContainer.ReplyEmail);
		driver.switchTo().parentFrame();
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on reply");
	}

	// Click on Forward button
	@Then("^Click on Forward button$")
	public void Click_on_Forward_button() throws Exception {
		driver.switchTo().frame(0);
		ActionHandler.wait(5);
		ActionHandler.click(SVOEnquiryContainer.ForwardEmail);
		driver.switchTo().parentFrame();
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on forward");

	}

	// Edit the to field and click on send
	@Then("^Edit the to field as \"([^\"]*)\" and click on send$")
	public void edit_the_to_field_as_and_click_on_send(String To) throws Throwable {
		String t[] = To.split(",");
		To = CommonFunctions.readExcelMasterData(t[0], t[1], t[2]);

		System.out.println("All details are fetched from excel");

		ActionHandler.wait(5);

		driver.switchTo().frame(0);

		ActionHandler.wait(5);
		ActionHandler.clearAndSetText(SVOEnquiryContainer.Additionalto, To);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter To field");

		ActionHandler.scrollToView(SVOEnquiryContainer.sendemail);
		ActionHandler.wait(3);
		ActionHandler.click(SVOEnquiryContainer.sendemail);
		driver.switchTo().parentFrame();
		ActionHandler.wait(20);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on send mail");

	}

	// edit the required information with Subject and click on Send
	@Given("^Add multiple users in To section as \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\" subject \"([^\"]*)\" and click on send$")
	public void add_multiple_users_in_To_section_as_and_and_subject_and_click_on_send(String User1, String User2,
			String User3, String Subject) throws Throwable {
		String u1[] = User1.split(",");
		User1 = CommonFunctions.readExcelMasterData(u1[0], u1[1], u1[2]);

		String u2[] = User2.split(",");
		User2 = CommonFunctions.readExcelMasterData(u2[0], u2[1], u2[2]);

		String u3[] = User3.split(",");
		User3 = CommonFunctions.readExcelMasterData(u3[0], u3[1], u3[2]);

		String s[] = Subject.split(",");
		Subject = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		System.out.println("All details are fetched from excel");

		ActionHandler.wait(5);

		ActionHandler.wait(5);
		driver.navigate().refresh();
		ActionHandler.wait(15);

		// User click on search button
		driver.switchTo().frame(0);
		ActionHandler.click(SVOEnquiryContainer.SearchTO);
		ActionHandler.wait(5);
		Reporter.addStepLog("User click on search button");
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		String NewWindow = driver.getWindowHandle();
		ActionHandler.wait(10);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String SetUpWindow = i.next();
			if (!NewWindow.equalsIgnoreCase(SetUpWindow)) {
				driver.switchTo().window(SetUpWindow);

				driver.manage().window().maximize();
				ActionHandler.wait(5);

				ActionHandler.click(SVOEnquiryContainer.FirstCheckbox);
				ActionHandler.wait(3);
				ActionHandler.click(SVOEnquiryContainer.ToArrowBtn);
				Reporter.addStepLog("User selects user1 ");
				CommonFunctions.attachScreenshot();

				ActionHandler.wait(5);
				ActionHandler.click(SVOEnquiryContainer.FirstCheckbox);
				ActionHandler.wait(3);
				ActionHandler.click(SVOEnquiryContainer.ToArrowBtn);
				Reporter.addStepLog("User selects user2 ");
				CommonFunctions.attachScreenshot();

				ActionHandler.wait(5);
				ActionHandler.click(SVOEnquiryContainer.FirstCheckbox);
				ActionHandler.wait(3);
				ActionHandler.click(SVOEnquiryContainer.ToArrowBtn);
				Reporter.addStepLog("User selects user3");
				CommonFunctions.attachScreenshot();

				// User click on Ok button
				ActionHandler.wait(7);
				ActionHandler.click(SVOEnquiryContainer.OkBtn);
				CommonFunctions.attachScreenshot();
				ActionHandler.wait(5);

				driver.switchTo().window(NewWindow);

			}

		}

		driver.switchTo().frame(0);
		ActionHandler.wait(5);
		ActionHandler.setText(SVOEnquiryContainer.Subject, Subject);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter Subject for email");

		ActionHandler.scrollToView(SVOEnquiryContainer.sendemail);
		ActionHandler.wait(3);
		ActionHandler.click(SVOEnquiryContainer.sendemail);
		ActionHandler.wait(20);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on send mail");

	}

	@Given("^log into jira with issueKey \"([^\"]*)\"$")
	public void Login_JIRA(String key) throws Exception {
		String k[] = key.split(",");
		key = CommonFunctions.readExcelMasterData(k[0], k[1], k[2]);

		onStart();
		driver.get(Constants.JIRA);
		ActionHandler.wait(7);
		Reporter.addStepLog("User Logins to OUV Portal");

		ActionHandler.clearAndSetText(SVOEnquiryContainer.jirauser, "msoni3");
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();

		ActionHandler.clearAndSetText(SVOEnquiryContainer.jirauserpass, "JLR2021Feb");
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.jirauserlogin);
		ActionHandler.wait(20);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.setText(SVOEnquiryContainer.jiraSearchBox, key);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		ActionHandler.pressEnter();
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigates to designated Issue Key");

	}

	@Then("^update the execution status in JIRA for issueKey \"([^\"]*)\"$")
	public void update_the_execution_status_in_JIRA(String key) throws Throwable {

		String status = null;

		String k[] = key.split(",");
		key = CommonFunctions.readExcelMasterData(k[0], k[1], k[2]);

		for (int i = 2; i < 252; i++) {
			String row = String.valueOf(i);
			status = CommonFunctions.readExcelMasterData("ScriptResults", "C", row);

			try {

				if (status == "") {
					System.out.println("The cell " + row + " is empty ");
				}

				else {
					ActionHandler.pageDown();
					ActionHandler.pageDown();
					ActionHandler.wait(5);

					String keyissue = CommonFunctions.readExcelMasterData("ScriptResults", "A", row);

					try {
						WebElement script = driver.findElement(By.xpath("//a[text()='" + keyissue + "']"));
					} catch (NoSuchElementException e) {

						javaScriptUtil
								.scrollIntoView(driver.findElement(By.xpath("//a[@class='next paginate_button']")));
						ActionHandler.wait(5);
						ActionHandler.click(driver.findElement(By.xpath("//a[@class='next paginate_button']")));
						CommonFunctions.attachScreenshot();
						Reporter.addStepLog("Navigate to the next page");
						ActionHandler.wait(10);

					}

					try {

						WebElement script = driver.findElement(By.xpath("//a[text()='" + keyissue + "']"));
					} catch (NoSuchElementException e) {

						javaScriptUtil
								.scrollIntoView(driver.findElement(By.xpath("//a[@class='next paginate_button']")));
						ActionHandler.wait(5);
						ActionHandler.click(driver.findElement(By.xpath("//a[@class='next paginate_button']")));
						CommonFunctions.attachScreenshot();
						Reporter.addStepLog("Navigate to the next page");
						ActionHandler.wait(10);

					}

					javaScriptUtil.scrollIntoView(driver.findElement(By.xpath("//a[text()='" + keyissue + "']")));
					ActionHandler.wait(5);
					ActionHandler.click(driver.findElement(By.xpath("//a[text()='" + keyissue + "']")));
					CommonFunctions.attachScreenshot();
					Reporter.addStepLog("Navigate to the script");
					ActionHandler.wait(10);

					ActionHandler.pageDown();
					ActionHandler.click(driver.findElement(By.xpath("//div[@class='aui-item issue-main-column']")));
					ActionHandler.wait(5);
					ActionHandler.pageCompleteScrollUp();
					ActionHandler.wait(3);
					CommonFunctions.attachScreenshot();
					javaScriptUtil.scrollIntoView(SVOEnquiryContainer.filterText);
					ActionHandler.wait(5);

					Reporter.addStepLog("User wants to filter the search");
					ActionHandler.wait(2);
					ActionHandler.click(SVOEnquiryContainer.filterAllProjects);
					ActionHandler.wait(2);
					CommonFunctions.attachScreenshot();

					ActionHandler.wait(2);
					ActionHandler.clearAndSetText(SVOEnquiryContainer.filterAllProjects, "SFGO");
					ActionHandler.wait(2);
					CommonFunctions.attachScreenshot();
					ActionHandler.pressEnter();
					ActionHandler.wait(3);
					CommonFunctions.attachScreenshot();
					Reporter.addStepLog("User chose SFGO-SVO Project");

					ActionHandler.wait(2);
					ActionHandler.click(SVOEnquiryContainer.statusCol);
					ActionHandler.wait(3);
					CommonFunctions.attachScreenshot();

					ActionHandler.wait(2);
					ActionHandler.setText(SVOEnquiryContainer.statusCol, "TODO");
					ActionHandler.wait(2);
					CommonFunctions.attachScreenshot();
					ActionHandler.pressEnter();
					ActionHandler.wait(2);
					CommonFunctions.attachScreenshot();
					Reporter.addStepLog("User chose Status as TODO");

					if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.toDoStatus)) {
						ActionHandler.wait(3);
						ActionHandler.click(driver.findElement(By.xpath("(//button[@title='Execute'])[2]")));
						CommonFunctions.attachScreenshot();
						Reporter.addStepLog("click on the execution button");

						ActionHandler.wait(1);
						ActionHandler.click(driver.findElement(By.xpath("(//a[text()='" + status + "'])[2]")));
						ActionHandler.wait(5);
						CommonFunctions.attachScreenshot();
						Reporter.addStepLog("Update the status");
						System.out.println("Update the status for " + keyissue);
					}

					else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.failedStatus)) {
						ActionHandler.wait(2);
						ActionHandler.click(SVOEnquiryContainer.statusCol);
						ActionHandler.wait(3);
						CommonFunctions.attachScreenshot();

						ActionHandler.wait(2);
						ActionHandler.setText(SVOEnquiryContainer.statusCol, "FAIL");
						ActionHandler.wait(2);
						CommonFunctions.attachScreenshot();
						ActionHandler.pressEnter();
						ActionHandler.wait(2);
						CommonFunctions.attachScreenshot();
						Reporter.addStepLog("User chose Status as Fail");

						ActionHandler.wait(3);
						ActionHandler.click(driver.findElement(By.xpath("//button[@title='Execute']")));
						CommonFunctions.attachScreenshot();
						Reporter.addStepLog("click on the execution button");

						ActionHandler.wait(1);
						ActionHandler.click(driver.findElement(By.xpath("//a[text()='" + status + "']")));
						ActionHandler.wait(5);
						CommonFunctions.attachScreenshot();
						Reporter.addStepLog("Update the status");
						System.out.println("Update the status for " + keyissue);
					}

					else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.abortedStatus)) {
						ActionHandler.wait(2);
						ActionHandler.click(SVOEnquiryContainer.statusCol);
						ActionHandler.wait(3);
						CommonFunctions.attachScreenshot();

						ActionHandler.wait(2);
						ActionHandler.setText(SVOEnquiryContainer.statusCol, "ABORTED");
						ActionHandler.wait(2);
						CommonFunctions.attachScreenshot();
						ActionHandler.pressEnter();
						ActionHandler.wait(2);
						CommonFunctions.attachScreenshot();
						Reporter.addStepLog("User chose Status as Fail");

						ActionHandler.wait(3);
						ActionHandler.click(driver.findElement(By.xpath("//button[@title='Execute']")));
						CommonFunctions.attachScreenshot();
						Reporter.addStepLog("click on the execution button");

						ActionHandler.wait(1);
						ActionHandler.click(driver.findElement(By.xpath("//a[text()='" + status + "']")));
						ActionHandler.wait(5);
						CommonFunctions.attachScreenshot();
						Reporter.addStepLog("Update the status");
						System.out.println("Update the status for " + keyissue);
					}

					else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.executingStatus)) {
						ActionHandler.wait(2);
						ActionHandler.click(SVOEnquiryContainer.statusCol);
						ActionHandler.wait(3);
						CommonFunctions.attachScreenshot();

						ActionHandler.wait(2);
						ActionHandler.setText(SVOEnquiryContainer.statusCol, "EXECUTING");
						ActionHandler.wait(2);
						CommonFunctions.attachScreenshot();
						ActionHandler.pressEnter();
						ActionHandler.wait(2);
						CommonFunctions.attachScreenshot();
						Reporter.addStepLog("User chose Status as Fail");

						ActionHandler.wait(3);
						ActionHandler.click(driver.findElement(By.xpath("//button[@title='Execute']")));
						CommonFunctions.attachScreenshot();
						Reporter.addStepLog("click on the execution button");

						ActionHandler.wait(1);
						ActionHandler.click(driver.findElement(By.xpath("//a[text()='" + status + "']")));
						ActionHandler.wait(5);
						CommonFunctions.attachScreenshot();
						Reporter.addStepLog("Update the status");
						System.out.println("Update the status for " + keyissue);
					}

					else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.pendingValidationStatus)) {
						ActionHandler.wait(2);
						ActionHandler.click(SVOEnquiryContainer.statusCol);
						ActionHandler.wait(3);
						CommonFunctions.attachScreenshot();

						ActionHandler.wait(2);
						ActionHandler.setText(SVOEnquiryContainer.statusCol, "Pending-Validation");
						ActionHandler.wait(2);
						CommonFunctions.attachScreenshot();
						ActionHandler.pressEnter();
						ActionHandler.wait(2);
						CommonFunctions.attachScreenshot();
						Reporter.addStepLog("User chose Status as Fail");

						ActionHandler.wait(3);
						ActionHandler.click(driver.findElement(By.xpath("//button[@title='Execute']")));
						CommonFunctions.attachScreenshot();
						Reporter.addStepLog("click on the execution button");

						ActionHandler.wait(1);
						ActionHandler.click(driver.findElement(By.xpath("//a[text()='" + status + "']")));
						ActionHandler.wait(5);
						CommonFunctions.attachScreenshot();
						Reporter.addStepLog("Update the status");
						System.out.println("Update the status for " + keyissue);
					} else {
						Reporter.addStepLog("The status is passed.No need to change");
						CommonFunctions.attachScreenshot();
					}

					driver.navigate().back();
					ActionHandler.wait(5);
					CommonFunctions.attachScreenshot();
					Reporter.addStepLog("Update the status");

					driver.navigate().refresh();
					ActionHandler.wait(15);
				}

			} catch (NoSuchElementException e) {
				driver.get(Constants.JIRA);
				ActionHandler.wait(7);
				ActionHandler.setText(SVOEnquiryContainer.jiraSearchBox, key);
				ActionHandler.wait(1);
				CommonFunctions.attachScreenshot();
				ActionHandler.pressEnter();
				ActionHandler.wait(7);
				CommonFunctions.attachScreenshot();
				Reporter.addStepLog("Unable to find Issue Key");
			}

		}
		ActionHandler.pageDown();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		driver.close();
	}

	@And("^mark this script status in \"([^\"]*)\"$")
	public void mark_Script_status(String status) throws Exception {
		String u1[] = status.split(",");
		CommonFunctions.setCellData(u1[0], u1[1], u1[2], "PASS");

		Reporter.addStepLog("Update the script status in excel");

	}

	@And("^mark test script status in \"([^\"]*)\"$")
	public void mark_test_script_status(String status) throws Exception {
		String u1[] = status.split(",");
		CommonFunctions.setCellData(u1[0], u1[1], u1[2], "EXECUTING");
		Reporter.addStepLog("Update the script status in excel");
	}

	@Given("^User convert JIRA Status as per JUNIT Test Status$")
	public void User_convert_JIRA_Status_as_Per_JUNIT_test_status() throws Exception {
		ActionHandler.wait(2);

		String status = null;

		for (int i = 2; i < 252; i++) {
			String row = String.valueOf(i);
			status = CommonFunctions.readExcelMasterData("ScriptResults", "C", row);
			System.out.println("Status of all Tests = " + status);

			String row1 = String.valueOf(i - 1);

			try {

				if (status.equals("EXECUTING")) {
					CommonFunctions.setCellData("ScriptResults", "C", row1, "FAIL");
					Reporter.addStepLog("Update the script status in excel");
				}
			} catch (NoSuchElementException e) {
				driver.get("https://jira.devops.jlr-apps.com/browse/SFGO-152");
				ActionHandler.wait(7);
				driver.navigate().refresh();
				ActionHandler.wait(15);
				System.out.println("unable to find the key");

			}
		}
	}
}
