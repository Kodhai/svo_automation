package com.jlr.svo.tests;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.autotest.utils.Config;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.constants.Constants;
import com.jlr.svo.containers.SSEC_1005_AutopoulatedFieldsOnOrderWorkOrderContainer;
import com.jlr.svo.containers.SVOAdditionalVehicleContainer;
import com.jlr.svo.containers.SVOCommentFieldAdditionContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.containers.SVOItemToApproveContainer;
import com.jlr.svo.containers.SVOOpportunityContainer;
import com.jlr.svo.containers.SVOWorkOrderContainer;
import com.jlr.svo.containers.SVO_CancelOrderAtAnyStageContainer;
import com.jlr.svo.containers.SVO_RestrictedPartyScreeningContainer;
import com.jlr.svo.containers.SVO_WorkOrder_AdditionalOwner_Container;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class SSEC_1005_AutopoulatedFieldsOnOrderWorkOrder_Test extends TestBaseCC {
	private WebDriver driver = getDriver();
	private static final Logger LOGGER = LoggerFactory
			.getLogger(SSEC_1005_AutopoulatedFieldsOnOrderWorkOrder_Test.class.getName());
	SVOCommentFieldAdditionContainer SVOCommentFieldAdditionContainer = PageFactory.initElements(driver,
			SVOCommentFieldAdditionContainer.class);
	SVOWorkOrderContainer SVOWorkOrderContainer = PageFactory.initElements(driver, SVOWorkOrderContainer.class);

	public static String WorkOrderNumber;
	public static String verificationCode;
	public static String vCode;
	public static String veriCode;
	public static String OpportunityName;
	public static String BespokeInteriorRequirement;
	public static String BespokeExteriorRequirement;
	public static String BespokePaintRequirement;

	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	SVO_RestrictedPartyScreeningContainer RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
			SVO_RestrictedPartyScreeningContainer.class);

	SVOItemToApproveContainer SVOItemToapproveContainer = PageFactory.initElements(driver,
			SVOItemToApproveContainer.class);
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
	SVOAdditionalVehicleContainer SVOAdditionalvehicleContainer = PageFactory.initElements(driver,
			SVOAdditionalVehicleContainer.class);
	SVO_WorkOrder_AdditionalOwner_Container SVO_WorkOrder_AdditionalOwner_Container = PageFactory.initElements(driver,
			SVO_WorkOrder_AdditionalOwner_Container.class);
	SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SVO_CancelOrderAtAnyStageContainer SVO_Cancel_Order_Container = PageFactory.initElements(driver,
			SVO_CancelOrderAtAnyStageContainer.class);
	SSEC_1005_AutopoulatedFieldsOnOrderWorkOrderContainer AutopoulatedFieldsOnOrderWorkOrder = PageFactory
			.initElements(driver, SSEC_1005_AutopoulatedFieldsOnOrderWorkOrderContainer.class);

	@Given("^Access to SVO Portal with Classic Sales user with the Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void access_to_an_SVO_Portal_with_classic_sales_user_with_the_Username_and_Password(String userName,
			String password) throws Throwable {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		onStart();
		driver = getDriver();
		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailForClassicSalesUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			Reporter.addStepLog("User logins to SVO Successfully");

		}
	}

	public String checkEmailForClassicSalesUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(342, 349);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	@Then("^Verify that user is able to view bespoke requirement fields on Opportunity page$")
	public void verify_that_user_is_able_to_view_bespoke_requirement_fields_on_Opportunity_page() throws Throwable {

		ActionHandler.wait(3);
		javaScriptUtil.scrollIntoView(AutopoulatedFieldsOnOrderWorkOrder.BespokeSpecificationsSection);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Scroll the opportunity page to view Bespoke Specification section");

		BespokeInteriorRequirement = AutopoulatedFieldsOnOrderWorkOrder.BespokeInteriorRequirementFieldValue.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"Bespoke Interior Requirement is displayed as " + BespokeInteriorRequirement + " on Opportunity Page");

		BespokeExteriorRequirement = AutopoulatedFieldsOnOrderWorkOrder.BespokeExteriorRequirementFieldValue.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"Bespoke Exterior Requirement is displayed as " + BespokeExteriorRequirement + " on Opportunity Page");

		BespokePaintRequirement = AutopoulatedFieldsOnOrderWorkOrder.BespokePaintRequirementFieldValue.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"Bespoke Paint Requirement is displayed as " + BespokePaintRequirement + " on Opportunity Page");

		ActionHandler.wait(2);
		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(2);
	}

	@Then("^User Navigates to Orders page of an Opportunity$")
	public void user_Navigates_to_Orders_page_of_an_Opportunity() throws Throwable {
		driver.navigate().refresh();
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.OrdersLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Orders link");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstOrder);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens Order details for the opportunity");

	}

	@Then("^Verify that user is able to view bespoke requirement fields on Order page$")
	public void verify_that_user_is_able_to_view_bespoke_requirement_fields_on_Order_page() throws Throwable {
		ActionHandler.wait(3);
		javaScriptUtil.scrollIntoView(AutopoulatedFieldsOnOrderWorkOrder.BespokeRequestSection);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Scroll the opportunity page to view Bespoke Request Information section");

		BespokeInteriorRequirement = AutopoulatedFieldsOnOrderWorkOrder.BespokeInteriorRequirementOrderField.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"Bespoke Interior Requirement is displayed as " + BespokeInteriorRequirement + " on Order Page");

		BespokeExteriorRequirement = AutopoulatedFieldsOnOrderWorkOrder.BespokeExteriorRequirementOrderField.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"Bespoke Exterior Requirement is displayed as " + BespokeExteriorRequirement + " on Order Page");

		BespokePaintRequirement = AutopoulatedFieldsOnOrderWorkOrder.BespokePaintRequirementOrderField.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Bespoke Paint Requirement is displayed as " + BespokePaintRequirement + " on Order Page");

		ActionHandler.wait(2);
		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(2);
	}

	@Then("^User Navigates to work order page of an created order$")
	public void user_Navigates_to_work_order_page_of_an_created_order() throws Throwable {
		ActionHandler.wait(5);
		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);

		javaScriptUtil.clickElementByJS(SVO_WorkOrder_AdditionalOwner_Container.WorkOrdernumberOnOrdersPage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens work Order details for the opportunity");

		WorkOrderNumber = SVOWorkOrderContainer.NewlyCreatedWorkOrderNumber.getText();
		Reporter.addStepLog("Work Order Number Name is = " + WorkOrderNumber);
	}

	@Then("^Verify that user is able to view bespoke requirement fields on Work Order page$")
	public void verify_that_user_is_able_to_view_bespoke_requirement_fields_on_Work_Order_page() throws Throwable {
		ActionHandler.wait(3);
		javaScriptUtil.scrollIntoView(AutopoulatedFieldsOnOrderWorkOrder.BespokeSpecificationsSection);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Scroll the opportunity page to view Bespoke Specification section");

		BespokeInteriorRequirement = AutopoulatedFieldsOnOrderWorkOrder.BespokeInteriorRequirementFieldValue.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"Bespoke Interior Requirement is displayed as " + BespokeInteriorRequirement + " on Work Order Page");

		BespokeExteriorRequirement = AutopoulatedFieldsOnOrderWorkOrder.BespokeExteriorRequirementFieldValue.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"Bespoke Exterior Requirement is displayed as " + BespokeExteriorRequirement + " on Work Order Page");

		BespokePaintRequirement = AutopoulatedFieldsOnOrderWorkOrder.BespokePaintRequirementFieldValue.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"Bespoke Paint Requirement is displayed as " + BespokePaintRequirement + " on Work Order Page");

		ActionHandler.wait(2);
		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(2);
	}

	@And("^User create a Classic Reborn Opportunity with all fields like Stage \"([^\"]*)\" Product Offering \"([^\"]*)\" Region \"([^\"]*)\" Client \"([^\"]*)\" Restricted Party Screening Stage \"([^\"]*)\" Preferred Contact \"([^\"]*)\" Account Name \"([^\"]*)\" Retailer Contact \"([^\"]*)\" Source \"([^\"]*)\" Origination Divission \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\"$")
	public void User_create_a_Classic_Reborn_Opportunity_with_all_fields_like_stage(String stage,
			String productOffering, String region, String client, String Checkcleared, String preferredContact,
			String accountName, String retailerContact, String source, String originatingdivission, String brand,
			String model) throws Exception {
		String oppName = "Classic_Reborn_Opportunity_";
		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		String closeDate = CommonFunctions.selectFutureDate();
		Reporter.addStepLog("Opportunity Close Date is = " + closeDate);

		String s[] = stage.split(",");
		stage = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String proOff[] = productOffering.split(",");
		productOffering = CommonFunctions.readExcelMasterData(proOff[0], proOff[1], proOff[2]);

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String c[] = client.split(",");
		client = CommonFunctions.readExcelMasterData(c[0], c[1], c[2]);

		String RPS[] = Checkcleared.split(",");
		Checkcleared = CommonFunctions.readExcelMasterData(RPS[0], RPS[1], RPS[2]);

		String preCon[] = preferredContact.split(",");
		preferredContact = CommonFunctions.readExcelMasterData(preCon[0], preCon[1], preCon[2]);

		String accName[] = accountName.split(",");
		accountName = CommonFunctions.readExcelMasterData(accName[0], accName[1], accName[2]);

		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		String so[] = source.split(",");
		source = CommonFunctions.readExcelMasterData(so[0], so[1], so[2]);

		String OD[] = originatingdivission.split(",");
		originatingdivission = CommonFunctions.readExcelMasterData(OD[0], OD[1], OD[2]);

		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String m[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		Reporter.addStepLog("User starts creating new Classic Reborn Opportunity");
		ActionHandler.click(SVOAdditionalvehicleContainer.NewOpportunity);
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.QuoteNextBtn)) {

			ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.ClassicRebornRecType);
			ActionHandler.wait(1);
			Reporter.addStepLog("User selects opportunity record type as Classic Reborn");

			ActionHandler.click(SVOItemToapproveContainer.QuoteNextBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User clicks on next button");

		}

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeOppName, oppName + randomNumber);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Name");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeClosedDate, closeDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Closed date");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeStage);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.StageSelection(stage))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Stage for an Opportunity");

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Product Offering for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		ActionHandler.wait(2);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.RegionSelection(region))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Region for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeAccountName, accountName);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeAccountNameSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(accountName))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Account Name for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokePreferredContact, preferredContact);
		ActionHandler.click(SVOItemToapproveContainer.SVOBespokePreferredContactname);
		Reporter.addStepLog("User selects Preferred Contact for an Opportunity");

		VerifyHandler.verifyElementPresent(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		CommonFunctions.attachScreenshot();
		ActionHandler
				.click(driver.findElement(By.xpath(RestrictedPartyScreeningContainer.ValueSelection(Checkcleared))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects restricted party screening as Check cleared for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeBrand, brand);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeBrandSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(brand))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Brand for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeModel, model);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(model))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Model for an Opportunity");

		ActionHandler.click(AutopoulatedFieldsOnOrderWorkOrder.BespokeInteriorRequirementTxtBox);
		ActionHandler.wait(1);
		ActionHandler.setText(AutopoulatedFieldsOnOrderWorkOrder.BespokeInteriorRequirementTxtBox, "Test Interior");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters Bespoke Interior Requirements");

		ActionHandler.click(AutopoulatedFieldsOnOrderWorkOrder.BespokeExteriorRequirementTxtBox);
		ActionHandler.wait(1);
		ActionHandler.setText(AutopoulatedFieldsOnOrderWorkOrder.BespokeExteriorRequirementTxtBox, "Test Exterior");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters Bespoke Exterior Requirements");

		ActionHandler.click(AutopoulatedFieldsOnOrderWorkOrder.BespokePaintRequirementTxtBox);
		ActionHandler.wait(1);
		ActionHandler.setText(AutopoulatedFieldsOnOrderWorkOrder.BespokePaintRequirementTxtBox, "Test Paint");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters Bespoke Paint Requirements");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.BodyStyleTextBox);
		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.BodyStyleTextBox, model);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.TestDefenderBodystyle);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects body style for an Opportunity");

		ActionHandler.wait(2);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.BuildColour);
		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.BuildColour, "Blue");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters build colour on Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.HandOfDrivedropDown);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.HandOfDriveLHD);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Hand of drive on Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.Speedometerdropdown);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.SpeedometerMPH);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Speedometer on Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.BuildColourShade);
		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.BuildColourShade, "brown");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters build colour shade on Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.BuildInteriorColour);
		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.BuildInteriorColour, "White");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters build interior colour on Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.AdditionalSepcDetails);
		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.AdditionalSepcDetails, "Test");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Additional spec details on Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.ClassicServiceVehicleRecord);
		ActionHandler.wait(1);
		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.ClassicServiceVehicleRecord, model);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.ClassicServiceDefenderVehicle);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Vehicle record for an Opportunity");

		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.MainVehicleRetailPrice, "250");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enter retail price for main vehicle on Opportunity page");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.VATQualifyingDropdown);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.VATyesButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses VAT qualifying of an opportunity as yes");

		ActionHandler.wait(2);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRetailerContact);
		ActionHandler.wait(2);
		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeRetailerContact, retailerContact);
		ActionHandler.click(SVOItemToapproveContainer.SVOBespokePreferredContactname);
		Reporter.addStepLog("User selects Retailer Contact for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSource);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.SourceSelection(source))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Source for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSaveOpportunity);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves Classic Reborn Opportunity");

		OpportunityName = SVOAdditionalvehicleContainer.OpportunityName.getText();
		Reporter.addStepLog("Opportunity Name is = " + OpportunityName);
	}

	@And("^User create a Classic Reborn Opportunity with fields like Stage \"([^\"]*)\" Product Offering \"([^\"]*)\" Region \"([^\"]*)\" Client \"([^\"]*)\" Restricted Party Screening Stage \"([^\"]*)\" Preferred Contact \"([^\"]*)\" Account Name \"([^\"]*)\" Retailer Contact \"([^\"]*)\" Source \"([^\"]*)\" Origination Divission \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\"$")
	public void User_create_a_Classic_Reborn_Opportunity_with_fields_like_stage(String stage, String productOffering,
			String region, String client, String Checkcleared, String preferredContact, String accountName,
			String retailerContact, String source, String originatingdivission, String brand, String model)
			throws Exception {
		String oppName = "Classic_Reborn_Opportunity_";
		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		String closeDate = CommonFunctions.selectFutureDate();
		Reporter.addStepLog("Opportunity Close Date is = " + closeDate);

		String s[] = stage.split(",");
		stage = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String proOff[] = productOffering.split(",");
		productOffering = CommonFunctions.readExcelMasterData(proOff[0], proOff[1], proOff[2]);

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String c[] = client.split(",");
		client = CommonFunctions.readExcelMasterData(c[0], c[1], c[2]);

		String RPS[] = Checkcleared.split(",");
		Checkcleared = CommonFunctions.readExcelMasterData(RPS[0], RPS[1], RPS[2]);

		String preCon[] = preferredContact.split(",");
		preferredContact = CommonFunctions.readExcelMasterData(preCon[0], preCon[1], preCon[2]);

		String accName[] = accountName.split(",");
		accountName = CommonFunctions.readExcelMasterData(accName[0], accName[1], accName[2]);

		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		String so[] = source.split(",");
		source = CommonFunctions.readExcelMasterData(so[0], so[1], so[2]);

		String OD[] = originatingdivission.split(",");
		originatingdivission = CommonFunctions.readExcelMasterData(OD[0], OD[1], OD[2]);

		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String m[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		Reporter.addStepLog("User starts creating new Classic Reborn Opportunity");
		ActionHandler.click(SVOAdditionalvehicleContainer.NewOpportunity);
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.QuoteNextBtn)) {

			ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.ClassicRebornRecType);
			ActionHandler.wait(1);
			Reporter.addStepLog("User selects opportunity record type as Classic Reborn");

			ActionHandler.click(SVOItemToapproveContainer.QuoteNextBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User clicks on next button");

		}

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeOppName, oppName + randomNumber);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Name");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeClosedDate, closeDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Closed date");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeStage);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.StageSelection(stage))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Stage for an Opportunity");

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Product Offering for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		ActionHandler.wait(2);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.RegionSelection(region))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Region for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeAccountName, accountName);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeAccountNameSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(accountName))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Account Name for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokePreferredContact, preferredContact);
		ActionHandler.click(SVOItemToapproveContainer.SVOBespokePreferredContactname);
		Reporter.addStepLog("User selects Preferred Contact for an Opportunity");

		VerifyHandler.verifyElementPresent(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		CommonFunctions.attachScreenshot();
		ActionHandler
				.click(driver.findElement(By.xpath(RestrictedPartyScreeningContainer.ValueSelection(Checkcleared))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects restricted party screening as Check cleared for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeBrand, brand);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeBrandSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(brand))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Brand for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeModel, model);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(model))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Model for an Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.BodyStyleTextBox);
		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.BodyStyleTextBox, model);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.TestDefenderBodystyle);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects body style for an Opportunity");

		ActionHandler.wait(2);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.BuildColour);
		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.BuildColour, "Blue");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters build colour on Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.HandOfDrivedropDown);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.HandOfDriveLHD);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Hand of drive on Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.Speedometerdropdown);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.SpeedometerMPH);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Speedometer on Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.BuildColourShade);
		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.BuildColourShade, "brown");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters build colour shade on Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.BuildInteriorColour);
		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.BuildInteriorColour, "White");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters build interior colour on Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.AdditionalSepcDetails);
		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.AdditionalSepcDetails, "Test");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Additional spec details on Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.ClassicServiceVehicleRecord);
		ActionHandler.wait(1);
		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.ClassicServiceVehicleRecord, model);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.ClassicServiceDefenderVehicle);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Vehicle record for an Opportunity");

		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.MainVehicleRetailPrice, "250");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enter retail price for main vehicle on Opportunity page");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.VATQualifyingDropdown);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.VATyesButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses VAT qualifying of an opportunity as yes");

		ActionHandler.wait(2);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRetailerContact);
		ActionHandler.wait(2);
		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeRetailerContact, retailerContact);
		ActionHandler.click(SVOItemToapproveContainer.SVOBespokePreferredContactname);
		Reporter.addStepLog("User selects Retailer Contact for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSource);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.SourceSelection(source))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Source for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSaveOpportunity);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves Classic Reborn Opportunity");

		OpportunityName = SVOAdditionalvehicleContainer.OpportunityName.getText();
		Reporter.addStepLog("Opportunity Name is = " + OpportunityName);
	}

	@Then("^User create a Classic Bespoke Opportunity with all fields like Stage \"([^\"]*)\" Product Offering \"([^\"]*)\" Region \"([^\"]*)\" Client \"([^\"]*)\" Restricted Party Screening Stage \"([^\"]*)\" Preferred Contact \"([^\"]*)\" Account Name \"([^\"]*)\" Retailer Contact \"([^\"]*)\" Source \"([^\"]*)\" Origination Divission \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\"$")
	public void user_create_a_Classic_Bespoke_Opportunity_with_all_fields_like_Stage_Product_Offering_Region_Client_Restricted_Party_Screening_Stage_Preferred_Contact_Account_Name_Retailer_Contact_Source_Origination_Divission_Brand_Model(
			String stage, String productOffering, String region, String client, String Checkcleared,
			String preferredContact, String accountName, String retailerContact, String source,
			String originatingdivission, String brand, String model) throws Exception {
		String oppName = "Classic_Bespoke_Opportunity_";
		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		String closeDate = CommonFunctions.selectFutureDate();
		Reporter.addStepLog("Opportunity Close Date is = " + closeDate);

		String s[] = stage.split(",");
		stage = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String proOff[] = productOffering.split(",");
		productOffering = CommonFunctions.readExcelMasterData(proOff[0], proOff[1], proOff[2]);

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String c[] = client.split(",");
		client = CommonFunctions.readExcelMasterData(c[0], c[1], c[2]);

		String RPS[] = Checkcleared.split(",");
		Checkcleared = CommonFunctions.readExcelMasterData(RPS[0], RPS[1], RPS[2]);

		String preCon[] = preferredContact.split(",");
		preferredContact = CommonFunctions.readExcelMasterData(preCon[0], preCon[1], preCon[2]);

		String accName[] = accountName.split(",");
		accountName = CommonFunctions.readExcelMasterData(accName[0], accName[1], accName[2]);

		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		String so[] = source.split(",");
		source = CommonFunctions.readExcelMasterData(so[0], so[1], so[2]);

		String OD[] = originatingdivission.split(",");
		originatingdivission = CommonFunctions.readExcelMasterData(OD[0], OD[1], OD[2]);

		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String m[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		Reporter.addStepLog("User starts creating new Classic Bespoke Opportunity");
		ActionHandler.click(SVOAdditionalvehicleContainer.NewOpportunity);
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.QuoteNextBtn)) {

			ActionHandler.click(AutopoulatedFieldsOnOrderWorkOrder.ClassicBespokeRecType);
			ActionHandler.wait(1);
			Reporter.addStepLog("User selects opportunity record type as Classic Bespoke");

			ActionHandler.click(SVOItemToapproveContainer.QuoteNextBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User clicks on next button");

		}

		ActionHandler.wait(1);
		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeOppName, oppName + randomNumber);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Name");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeClosedDate, closeDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Closed date");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeStage);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.StageSelection(stage))));
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Stage for an Opportunity");

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Product Offering for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		ActionHandler.wait(2);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.RegionSelection(region))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Region for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeAccountName, accountName);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeAccountNameSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(accountName))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Account Name for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokePreferredContact, preferredContact);
		ActionHandler.click(SVOItemToapproveContainer.SVOBespokePreferredContactname);
		Reporter.addStepLog("User selects Preferred Contact for an Opportunity");

		VerifyHandler.verifyElementPresent(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		CommonFunctions.attachScreenshot();
		ActionHandler
				.click(driver.findElement(By.xpath(RestrictedPartyScreeningContainer.ValueSelection(Checkcleared))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects restricted party screening as Check cleared for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeBrand, brand);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeBrandSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(brand))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Brand for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeModel, model);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(model))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Model for an Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.BodyStyleTextBox);
		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.BodyStyleTextBox, model);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.TestDefenderBodystyle);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects body style for an Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.HandOfDrivedropDown);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.HandOfDriveLHD);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Hand of drive on Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.Speedometerdropdown);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.SpeedometerMPH);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Speedometer on Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.AdditionalSepcDetails);
		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.AdditionalSepcDetails, "Test");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Additional spec details on Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.ClassicServiceVehicleRecord);
		ActionHandler.wait(1);
		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.ClassicServiceVehicleRecord, model);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.ClassicServiceDefenderVehicle);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Vehicle record for an Opportunity");

		ActionHandler.click(AutopoulatedFieldsOnOrderWorkOrder.BespokeInteriorRequirementTxtBox);
		ActionHandler.wait(1);
		ActionHandler.setText(AutopoulatedFieldsOnOrderWorkOrder.BespokeInteriorRequirementTxtBox, "Test Interior");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters Bespoke Interior Requirements");

		ActionHandler.click(AutopoulatedFieldsOnOrderWorkOrder.BespokeExteriorRequirementTxtBox);
		ActionHandler.wait(1);
		ActionHandler.setText(AutopoulatedFieldsOnOrderWorkOrder.BespokeExteriorRequirementTxtBox, "Test Exterior");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters Bespoke Exterior Requirements");

		ActionHandler.click(AutopoulatedFieldsOnOrderWorkOrder.BespokePaintRequirementTxtBox);
		ActionHandler.wait(1);
		ActionHandler.setText(AutopoulatedFieldsOnOrderWorkOrder.BespokePaintRequirementTxtBox, "Test Paint");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters Bespoke Paint Requirements");

		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.MainVehicleRetailPrice, "250");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enter retail price for main vehicle on Opportunity page");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.VATQualifyingDropdown);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.VATyesButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses VAT qualifying of an opportunity as yes");

		ActionHandler.wait(2);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRetailerContact);
		ActionHandler.wait(2);
		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeRetailerContact, retailerContact);
		ActionHandler.click(SVOItemToapproveContainer.SVOBespokePreferredContactname);
		Reporter.addStepLog("User selects Retailer Contact for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSource);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.SourceSelection(source))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Source for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSaveOpportunity);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves Classic Bespoke Opportunity");

		OpportunityName = SVOAdditionalvehicleContainer.OpportunityName.getText();
		Reporter.addStepLog("Opportunity Name is = " + OpportunityName);

	}

	@Then("^User create a Classic Works Legend Opportunity with fields like Stage \"([^\"]*)\" Product Offering \"([^\"]*)\" Region \"([^\"]*)\" Client \"([^\"]*)\" Restricted Party Screening Stage \"([^\"]*)\" Preferred Contact \"([^\"]*)\" Account Name \"([^\"]*)\" Retailer Contact \"([^\"]*)\" Source \"([^\"]*)\" Origination Divission \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\"$")
	public void user_create_a_Classic_Works_Legend_Opportunity_with_fields_like_Stage_Product_Offering_Region_Client_Restricted_Party_Screening_Stage_Preferred_Contact_Account_Name_Retailer_Contact_Source_Origination_Divission_Brand_Model(
			String stage, String productOffering, String region, String client, String Checkcleared,
			String preferredContact, String accountName, String retailerContact, String source,
			String originatingdivission, String brand, String model) throws Exception {
		String oppName = "Classic_WorksLegend_Opportunity_";
		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		String closeDate = CommonFunctions.selectFutureDate();
		Reporter.addStepLog("Opportunity Close Date is = " + closeDate);

		String s[] = stage.split(",");
		stage = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String proOff[] = productOffering.split(",");
		productOffering = CommonFunctions.readExcelMasterData(proOff[0], proOff[1], proOff[2]);

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String c[] = client.split(",");
		client = CommonFunctions.readExcelMasterData(c[0], c[1], c[2]);

		String RPS[] = Checkcleared.split(",");
		Checkcleared = CommonFunctions.readExcelMasterData(RPS[0], RPS[1], RPS[2]);

		String preCon[] = preferredContact.split(",");
		preferredContact = CommonFunctions.readExcelMasterData(preCon[0], preCon[1], preCon[2]);

		String accName[] = accountName.split(",");
		accountName = CommonFunctions.readExcelMasterData(accName[0], accName[1], accName[2]);

		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		String so[] = source.split(",");
		source = CommonFunctions.readExcelMasterData(so[0], so[1], so[2]);

		String OD[] = originatingdivission.split(",");
		originatingdivission = CommonFunctions.readExcelMasterData(OD[0], OD[1], OD[2]);

		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String m[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		Reporter.addStepLog("User starts creating new Classic works legend Opportunity");
		ActionHandler.click(SVOAdditionalvehicleContainer.NewOpportunity);
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.QuoteNextBtn)) {

			ActionHandler.click(AutopoulatedFieldsOnOrderWorkOrder.ClassicWorksLegendRecType);
			ActionHandler.wait(1);
			Reporter.addStepLog("User selects opportunity record type as Classic Works legend");

			ActionHandler.click(SVOItemToapproveContainer.QuoteNextBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User clicks on next button");

		}

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeOppName, oppName + randomNumber);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Name");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeClosedDate, closeDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Closed date");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeStage);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.StageSelection(stage))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Stage for an Opportunity");

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Product Offering for an Opportunity");

		ActionHandler.scrollToView(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		ActionHandler.wait(2);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.RegionSelection(region))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Region for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeAccountName, accountName);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeAccountNameSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(accountName))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Account Name for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokePreferredContact, preferredContact);
		ActionHandler.click(SVOItemToapproveContainer.SVOBespokePreferredContactname);
		Reporter.addStepLog("User selects Preferred Contact for an Opportunity");

		ActionHandler.scrollToView(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		ActionHandler.wait(2);
		VerifyHandler.verifyElementPresent(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		CommonFunctions.attachScreenshot();
		ActionHandler
				.click(driver.findElement(By.xpath(RestrictedPartyScreeningContainer.ValueSelection(Checkcleared))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects restricted party screening as Check cleared for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeBrand, brand);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeBrandSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(brand))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Brand for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeModel, model);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(model))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Model for an Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.BodyStyleTextBox);
		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.BodyStyleTextBox, model);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.TestDefenderBodystyle);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects body style for an Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.HandOfDrivedropDown);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.HandOfDriveLHD);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Hand of drive on Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.ClassicServiceVehicleRecord);
		ActionHandler.wait(1);
		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.ClassicServiceVehicleRecord, model);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.ClassicServiceDefenderVehicle);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Vehicle record for an Opportunity");

		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.MainVehicleRetailPrice, "250");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enter retail price for main vehicle on Opportunity page");

		ActionHandler.scrollToView(SVO_WorkOrder_AdditionalOwner_Container.VATQualifyingDropdown);
		ActionHandler.wait(2);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.VATQualifyingDropdown);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.VATyesButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses VAT qualifying of an opportunity as yes");

		ActionHandler.wait(2);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRetailerContact);
		ActionHandler.wait(2);
		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeRetailerContact, retailerContact);
		ActionHandler.click(SVOItemToapproveContainer.SVOBespokePreferredContactname);
		Reporter.addStepLog("User selects Retailer Contact for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSource);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.SourceSelection(source))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Source for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSaveOpportunity);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves Classic Bespoke Opportunity");

		OpportunityName = SVOAdditionalvehicleContainer.OpportunityName.getText();
		Reporter.addStepLog("Opportunity Name is = " + OpportunityName);

	}

	@Then("^User is able to enter all bespoke requirement field values on Opportunity page$")
	public void user_is_able_to_enter_all_bespoke_requirement_field_values_on_Opportunity_page() throws Throwable {

		ActionHandler.wait(3);
		javaScriptUtil.scrollIntoView(AutopoulatedFieldsOnOrderWorkOrder.BespokeSpecificationsSection);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Scroll the opportunity page to view Bespoke Specification section");

		javaScriptUtil.clickElementByJS(AutopoulatedFieldsOnOrderWorkOrder.EditBespokeExteriorRequirementFieldValue);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on edit bespoke requirements icon on Opportunity page");

		ActionHandler.click(AutopoulatedFieldsOnOrderWorkOrder.BespokeInteriorRequirementTxtBox);
		ActionHandler.wait(1);
		ActionHandler.clearAndSetText(AutopoulatedFieldsOnOrderWorkOrder.BespokeInteriorRequirementTxtBox,
				"Test Interior");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters Bespoke Interior Requirements");

		ActionHandler.click(AutopoulatedFieldsOnOrderWorkOrder.BespokeExteriorRequirementTxtBox);
		ActionHandler.wait(1);
		ActionHandler.clearAndSetText(AutopoulatedFieldsOnOrderWorkOrder.BespokeExteriorRequirementTxtBox,
				"Test Exterior");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters Bespoke Exterior Requirements");

		ActionHandler.click(AutopoulatedFieldsOnOrderWorkOrder.BespokePaintRequirementTxtBox);
		ActionHandler.wait(1);
		ActionHandler.clearAndSetText(AutopoulatedFieldsOnOrderWorkOrder.BespokePaintRequirementTxtBox, "Test Paint");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters Bespoke Paint Requirements");

		ActionHandler.click(AutopoulatedFieldsOnOrderWorkOrder.SaveBespokeExteriorRequirementBtn);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on save Bespoke Requirements Button");

		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(2);

	}

	@Then("^User create a Classic Continuation Opportunity with all fields like Stage \"([^\"]*)\" Product Offering \"([^\"]*)\" Region \"([^\"]*)\" Client \"([^\"]*)\" Restricted Party Screening Stage \"([^\"]*)\" Preferred Contact \"([^\"]*)\" Account Name \"([^\"]*)\" Retailer Contact \"([^\"]*)\" Source \"([^\"]*)\" Origination Divission \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\"$")
	public void user_create_a_Classic_Continuation_Opportunity_with_all_fields_like_Stage_Product_Offering_Region_Client_Restricted_Party_Screening_Stage_Preferred_Contact_Account_Name_Retailer_Contact_Source_Origination_Divission_Brand_Model(
			String stage, String productOffering, String region, String client, String Checkcleared,
			String preferredContact, String accountName, String retailerContact, String source,
			String originatingdivission, String brand, String model) throws Exception {
		String oppName = "Classic_Continuation_Opportunity_";
		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		String closeDate = CommonFunctions.selectFutureDate();
		Reporter.addStepLog("Opportunity Close Date is = " + closeDate);

		String s[] = stage.split(",");
		stage = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String proOff[] = productOffering.split(",");
		productOffering = CommonFunctions.readExcelMasterData(proOff[0], proOff[1], proOff[2]);

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String c[] = client.split(",");
		client = CommonFunctions.readExcelMasterData(c[0], c[1], c[2]);

		String RPS[] = Checkcleared.split(",");
		Checkcleared = CommonFunctions.readExcelMasterData(RPS[0], RPS[1], RPS[2]);

		String preCon[] = preferredContact.split(",");
		preferredContact = CommonFunctions.readExcelMasterData(preCon[0], preCon[1], preCon[2]);

		String accName[] = accountName.split(",");
		accountName = CommonFunctions.readExcelMasterData(accName[0], accName[1], accName[2]);

		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		String so[] = source.split(",");
		source = CommonFunctions.readExcelMasterData(so[0], so[1], so[2]);

		String OD[] = originatingdivission.split(",");
		originatingdivission = CommonFunctions.readExcelMasterData(OD[0], OD[1], OD[2]);

		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String m[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		Reporter.addStepLog("User starts creating new Classic Continuation Opportunity");
		ActionHandler.click(SVOAdditionalvehicleContainer.NewOpportunity);
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.QuoteNextBtn)) {

			ActionHandler.click(AutopoulatedFieldsOnOrderWorkOrder.ClassicContinuationRecType);
			ActionHandler.wait(1);
			Reporter.addStepLog("User selects opportunity record type as Classic Continuation");

			ActionHandler.click(SVOItemToapproveContainer.QuoteNextBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User clicks on next button");

		}

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeOppName, oppName + randomNumber);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Name");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeClosedDate, closeDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Closed date");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeStage);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.StageSelection(stage))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Stage for an Opportunity");

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Product Offering for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		ActionHandler.wait(2);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.RegionSelection(region))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Region for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeAccountName, accountName);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeAccountNameSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(accountName))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Account Name for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokePreferredContact, preferredContact);
		ActionHandler.click(SVOItemToapproveContainer.SVOBespokePreferredContactname);
		Reporter.addStepLog("User selects Preferred Contact for an Opportunity");

		VerifyHandler.verifyElementPresent(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		CommonFunctions.attachScreenshot();
		ActionHandler
				.click(driver.findElement(By.xpath(RestrictedPartyScreeningContainer.ValueSelection(Checkcleared))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects restricted party screening as Check cleared for an Opportunity");

		ActionHandler.click(AutopoulatedFieldsOnOrderWorkOrder.ProgrammeFieldTxtBox);
		ActionHandler.wait(1);
		ActionHandler.setText(AutopoulatedFieldsOnOrderWorkOrder.ProgrammeFieldTxtBox, model);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		ActionHandler.click(AutopoulatedFieldsOnOrderWorkOrder.ProgrammeFieldSearchBar);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(AutopoulatedFieldsOnOrderWorkOrder.DefenderProgrammeFieldValue);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Programme for an Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.BodyStyleTextBox);
		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.BodyStyleTextBox, model);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.TestDefenderBodystyle);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects body style for an Opportunity");

		ActionHandler.click(AutopoulatedFieldsOnOrderWorkOrder.BespokeInteriorRequirementTxtBox);
		ActionHandler.wait(1);
		ActionHandler.setText(AutopoulatedFieldsOnOrderWorkOrder.BespokeInteriorRequirementTxtBox, "Test Interior");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters Bespoke Interior Requirements");

		ActionHandler.click(AutopoulatedFieldsOnOrderWorkOrder.BespokeExteriorRequirementTxtBox);
		ActionHandler.wait(1);
		ActionHandler.setText(AutopoulatedFieldsOnOrderWorkOrder.BespokeExteriorRequirementTxtBox, "Test Exterior");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters Bespoke Exterior Requirements");

		ActionHandler.click(AutopoulatedFieldsOnOrderWorkOrder.BespokePaintRequirementTxtBox);
		ActionHandler.wait(1);
		ActionHandler.setText(AutopoulatedFieldsOnOrderWorkOrder.BespokePaintRequirementTxtBox, "Test Paint");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters Bespoke Paint Requirements");

		ActionHandler.wait(2);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.BuildColour);
		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.BuildColour, "Blue");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters build colour on Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.HandOfDrivedropDown);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.HandOfDriveLHD);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Hand of drive on Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.Speedometerdropdown);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.SpeedometerMPH);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Speedometer on Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.BuildColourShade);
		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.BuildColourShade, "brown");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters build colour shade on Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.BuildInteriorColour);
		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.BuildInteriorColour, "White");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters build interior colour on Opportunity");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.AdditionalSepcDetails);
		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.AdditionalSepcDetails, "Test");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Additional spec details on Opportunity");

		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.MainVehicleRetailPrice, "250");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enter retail price for main vehicle on Opportunity page");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.VATQualifyingDropdown);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.VATyesButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses VAT qualifying of an opportunity as yes");

		ActionHandler.wait(2);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRetailerContact);
		ActionHandler.wait(2);
		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeRetailerContact, retailerContact);
		ActionHandler.click(SVOItemToapproveContainer.SVOBespokePreferredContactname);
		Reporter.addStepLog("User selects Retailer Contact for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSource);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.SourceSelection(source))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Source for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSaveOpportunity);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves Classic Works legend Opportunity");

		OpportunityName = SVOAdditionalvehicleContainer.OpportunityName.getText();
		Reporter.addStepLog("Opportunity Name is = " + OpportunityName);
	}

	@Then("^User Navigate Back to Opportunity Page$")
	public void user_Navigate_Back_to_Opportunity_Page() throws Throwable {

		javaScriptUtil.clickElementByJS(SVO_OpportunityContainer.OpportTab);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigate to Opportunity");

		ActionHandler.click(SVOItemToapproveContainer.OpportunitysearchBox);
		ActionHandler.wait(2);
		ActionHandler.setText(SVOItemToapproveContainer.OpportunitysearchBox, OpportunityName);
		ActionHandler.wait(3);
		ActionHandler.pressEnter();
		ActionHandler.wait(6);

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstOpportunityLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate back to the opportunity page");

	}

	@Then("^User moves an opportunity to closed lost stage$")
	public void user_moves_an_opportunity_to_closed_lost_stage() throws Throwable {

		ActionHandler.click(AutopoulatedFieldsOnOrderWorkOrder.ClosedStageButton);
		ActionHandler.wait(3);
		ActionHandler.click(AutopoulatedFieldsOnOrderWorkOrder.SelectClosedStageButton);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Closed stage Button");

		ActionHandler.click(AutopoulatedFieldsOnOrderWorkOrder.ClosedStageDropDown);
		ActionHandler.wait(1);
		ActionHandler.click(AutopoulatedFieldsOnOrderWorkOrder.ClosedLostPicklist);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Selects Closed lost as an opportunity stage");

		ActionHandler.wait(1);
		ActionHandler.click(AutopoulatedFieldsOnOrderWorkOrder.LostReasonDropDown);
		ActionHandler.wait(1);
		ActionHandler.click(AutopoulatedFieldsOnOrderWorkOrder.ClosedLostPicklist);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Selects Lost Reason for an opportunity");

		ActionHandler.click(AutopoulatedFieldsOnOrderWorkOrder.DoneButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on done button");

		ActionHandler.wait(5);
		Reporter.addStepLog("User Moves an opportunity to closed lost stage");

	}

	@Then("^User moves an opportunity to closed Won stage$")
	public void user_moves_an_opportunity_to_closed_Won_stage() throws Throwable {

		ActionHandler.click(AutopoulatedFieldsOnOrderWorkOrder.ClosedStageButton);
		ActionHandler.wait(3);
		ActionHandler.click(AutopoulatedFieldsOnOrderWorkOrder.SelectClosedStageButton);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Closed stage and selects closed won picklist");

		ActionHandler.click(AutopoulatedFieldsOnOrderWorkOrder.DoneButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on done button");

		ActionHandler.wait(5);
		Reporter.addStepLog("User Moves an opportunity to closed won stage");

	}

	@Then("^Verify that user is not able to view bespoke requirement fields on Order page$")
	public void verify_that_user_is_not_able_to_view_bespoke_requirement_fields_on_Order_page() throws Throwable {

		if (!VerifyHandler.verifyElementPresent(AutopoulatedFieldsOnOrderWorkOrder.BespokeInteriorRequirementOrderField)
				&& !VerifyHandler
						.verifyElementPresent(AutopoulatedFieldsOnOrderWorkOrder.BespokeExteriorRequirementOrderField)
				&& !VerifyHandler
						.verifyElementPresent(AutopoulatedFieldsOnOrderWorkOrder.BespokePaintRequirementOrderField)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User is not able to view bespoke requirement fields on Order page");
		}

	}

	@Then("^Verify that user is not able to edit bespoke requirement fields on Order page$")
	public void verify_that_user_is_not_able_to_edit_bespoke_requirement_fields_on_Order_page() throws Throwable {

		if (!VerifyHandler
				.verifyElementPresent(AutopoulatedFieldsOnOrderWorkOrder.EditBespokeExteriorRequirementFieldValue)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User is not able to edit bespoke requirement fields on Order page");
		}
	}

	@Then("^Verify that user is not able to edit bespoke requirement fields on Work Order page$")
	public void verify_that_user_is_not_able_to_edit_bespoke_requirement_fields_on_Work_Order_page() throws Throwable {

		if (!VerifyHandler
				.verifyElementPresent(AutopoulatedFieldsOnOrderWorkOrder.EditBespokeExteriorRequirementFieldValue)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User is not able to edit bespoke requirement fields on Work Order page");
		}

	}

	@Then("^Verify that user is not able to view any Order on orders section of an opportunity$")
	public void verify_that_user_is_not_able_to_view_any_Order_on_orders_section_of_an_opportunity() throws Throwable {

		if (!VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.FirstOrder)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User is not able to view any Order on orders section of an opportunity");
		}
	}

}
