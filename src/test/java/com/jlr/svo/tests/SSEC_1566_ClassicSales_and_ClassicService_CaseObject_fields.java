package com.jlr.svo.tests;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.autotest.utils.Config;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.constants.Constants;
import com.jlr.svo.containers.SSEC_1226_CaseCreationEmail_Container;
import com.jlr.svo.containers.SSEC_1228_LinkContactAndAccountTocase_Container;
import com.jlr.svo.containers.SSEC_1234_CaseListViews_Container;
import com.jlr.svo.containers.SSEC_1566_ClassicSalesAndClassicService_CaseObject_fields_Container;
import com.jlr.svo.containers.SVOAccountsContainer;
import com.jlr.svo.containers.SVOAdditionalVehicleContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.containers.SVOItemToApproveContainer;
import com.jlr.svo.containers.SVOOpportunityContainer;
import com.jlr.svo.containers.SVO_CustomerResponsesContainer;
import com.jlr.svo.containers.SVO_EnquiryLostReasonContainer;
import com.jlr.svo.containers.SVO_RestrictedPartyScreeningContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class SSEC_1566_ClassicSales_and_ClassicService_CaseObject_fields extends TestBaseCC {
	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory
			.getLogger(SSEC_1566_ClassicSales_and_ClassicService_CaseObject_fields.class.getName());
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);

	SVOAccountsContainer SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
	SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	SVOAdditionalVehicleContainer SVOAdditionalvehicleContainer = PageFactory.initElements(driver,
			SVOAdditionalVehicleContainer.class);
	SVO_RestrictedPartyScreeningContainer RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
			SVO_RestrictedPartyScreeningContainer.class);
	SVOItemToApproveContainer SVOItemToapproveContainer = PageFactory.initElements(driver,
			SVOItemToApproveContainer.class);
	SVO_CustomerResponsesContainer SVO_CustomerResponsesContainer = PageFactory.initElements(driver,
			SVO_CustomerResponsesContainer.class);
	SVO_EnquiryLostReasonContainer SVO_EnquiryLostReasonContainer = PageFactory.initElements(driver,
			SVO_EnquiryLostReasonContainer.class);
	SSEC_1226_CaseCreationEmail_Container CaseCreationEmailContainer = PageFactory.initElements(driver,
			SSEC_1226_CaseCreationEmail_Container.class);
	SSEC_1228_LinkContactAndAccountTocase_Container LinkContactAndAccountTocaseContainer = PageFactory
			.initElements(driver, SSEC_1228_LinkContactAndAccountTocase_Container.class);
	SSEC_1234_CaseListViews_Test CaseListViews_Test = PageFactory.initElements(driver,
			SSEC_1234_CaseListViews_Test.class);
	SSEC_1234_CaseListViews_Container CaseListViewsContainer = PageFactory.initElements(driver,
			SSEC_1234_CaseListViews_Container.class);
	SSEC_1566_ClassicSalesAndClassicService_CaseObject_fields_Container CaseObjectfieldsContainer = PageFactory
			.initElements(driver, SSEC_1566_ClassicSalesAndClassicService_CaseObject_fields_Container.class);

	public static String verificationCode;
	public static String vCode;
	public static String veriCode;
	public String NewWindow;
	public static String CaseCreated;
	public static String CaseNumber;
	public static String ContactName;

	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	public void onStart() {

		setupTest("SVOTest");
		driver = getDriver();

		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
		SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
		SVOAdditionalvehicleContainer = PageFactory.initElements(driver, SVOAdditionalVehicleContainer.class);

		RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
				SVO_RestrictedPartyScreeningContainer.class);
		SVOItemToapproveContainer = PageFactory.initElements(driver, SVOItemToApproveContainer.class);
		SVO_CustomerResponsesContainer = PageFactory.initElements(driver, SVO_CustomerResponsesContainer.class);
		SVO_EnquiryLostReasonContainer = PageFactory.initElements(driver, SVO_EnquiryLostReasonContainer.class);
		CaseCreationEmailContainer = PageFactory.initElements(driver, SSEC_1226_CaseCreationEmail_Container.class);
		LinkContactAndAccountTocaseContainer = PageFactory.initElements(driver,
				SSEC_1228_LinkContactAndAccountTocase_Container.class);
		CaseListViewsContainer = PageFactory.initElements(driver, SSEC_1234_CaseListViews_Container.class);
		CaseListViews_Test = PageFactory.initElements(driver, SSEC_1234_CaseListViews_Test.class);
		CaseObjectfieldsContainer = PageFactory.initElements(driver,
				SSEC_1566_ClassicSalesAndClassicService_CaseObject_fields_Container.class);

		driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);

		verificationCode = null;
		vCode = null;
		veriCode = null;
		NewWindow = null;
		CaseCreated = null;
		CaseNumber = null;
		ContactName = null;

	}

	@Given("^vcode$")
	public void vcode() throws Throwable {
		onStart();
		checkEmailForClassicServiceUser();
	}

	@Given("^Access to SVO portal for Classic Sales User using Username \"([^\"]*)\" and password \"([^\"]*)\"$")
	public void Login_to_the_SVO_Portal_for_classic_Sales(String userName, String password) throws Throwable {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		onStart();
		driver = getDriver();

		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailForClassicSalesUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verifyBtn)) {
				ActionHandler.click(SVOEnquiryContainer.verifyBtn);
				CommonFunctions.attachScreenshot();
				Reporter.addStepLog("User logins to SVO Successfully");
			}

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	public String checkEmailForClassicSalesUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(513, 520);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	@And("^User navigates to the Cases tab$")
	public void user_navigates_to_Cases_tab() throws Exception {
		ActionHandler.wait(1);
		javaScriptUtil.clickElementByJS(CaseCreationEmailContainer.CasesTab);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigates to Cases tab");
	}

	@Then("^User logouts from SVO portal$")
	public void user_logouts_from_SVO_portal() throws Exception {

		ActionHandler.click(SVO_OpportunityContainer.icon_image);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Logout");
		ActionHandler.click(SVO_OpportunityContainer.Logout);

		CommonFunctions.attachScreenshot();
		if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.userNameTextBox)) {
			MarkupHelper.createLabel("Logged out successfully", ExtentColor.GREEN);
		} else {
			MarkupHelper.createLabel("Log out failed", ExtentColor.RED);

		}
		// driver.quit();
	}

	@Given("^Access to SVO portal for Classic Services User with UserName \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void Login_to_the_SVO_Portal_for_classic_Service(String userName, String password) throws Throwable {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		onStart();

		driver = getDriver();

		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailForClassicServiceUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	public String checkEmailForClassicServiceUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(515, 521);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	@Then("^User creates classic sales case with case details like Record Type \"([^\"]*)\" Product Offering \"([^\"]*)\" Rating \"([^\"]*)\" and Case Origin \"([^\"]*)\"$")
	public void user_creates_classic_sales_case_case_details_like_Record_Type_Product_Offering_Rating_and_Case_Origin(
			String RecordType, String ProductOffering, String Rating, String CaseOrigin) throws Throwable {

	}
    @Then("^User fills Preferred Retailer \"([^\"]*)\"$")
    public void fill_Preferred_retailer() throws Exception{
	
    	
    }
    
    @Then("^User fills required details as Product Offering \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\"$")
   	public void user_fills_details_like_Product_Offering_Brand_Model(String productOffering, String brand, String model)
   			throws Throwable {
    	
    	
    }
    
    @Then("^Verifies Created by and Modified by on the enquiry page$")
    public void Verifies_Created_by_and_ModifiedBy() throws Exception{
    	
    }
    
    @Then("^Verifies SLA status on the enquiry page$")
    public void Verifies_SLA_status_on_enquiry_page() throws Exception{
    	
    	
    }
    
    @Then("^User mark the enquiry as Complete$")
    public void user_marks_enquiry_as_Complete() throws Exception{
    	
    	
    }
    
    @Then("^User logs a call to the Activity tab$")
    public void User_logs_call_to_Activity_tab() throws Exception{
    	
    	
    }
    
    @And("^Closes the enquiry with Qualified KMI$")
    public void Closes_enquiry_with_Qualified_KMI() throws Exception{
    	
    	
    }
    
    @And("^Verify that user is unable to close the enquiry$")
    public void Verify_user_is_unable_to_close_enquiry() throws Exception{
    	
    	
    }


    @And("^Verifies the SLA time remaining is zero$")
    public void Verifies_SLA_time_remaining() throws Exception{
    	
    }


	@Then("^Verify all the details of Case Information Section for newly created case$")
	public void verify_all_the_details_of_Case_Information_Section_for_newly_created_case() throws Throwable {

		ActionHandler.wait(2);
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.wait(1);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyProdOff);
		String ProductOffering = CaseObjectfieldsContainer.VerifyProdOff.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies product offering field with value displayed as " + ProductOffering);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyStatus);
		String CaseStatus = CaseObjectfieldsContainer.VerifyStatus.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Case Status field with value displayed as " + CaseStatus);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyRating);
		String Rating = CaseObjectfieldsContainer.VerifyRating.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Rating field with value displayed as " + Rating);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyOwner);
		String CaseOwner = CaseObjectfieldsContainer.VerifyOwner.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Case Owner field with value displayed as " + CaseOwner);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyRecord);
		String RecordType = CaseObjectfieldsContainer.VerifyRecord.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Case Record Type field with value displayed as " + RecordType);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyCaseOrigin);
		String CaseOrigin = CaseObjectfieldsContainer.VerifyCaseOrigin.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Case Origin field with value displayed as " + CaseOrigin);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyPriority);
		String CasePriority = CaseObjectfieldsContainer.VerifyPriority.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Priority field with value displayed as " + CasePriority);
	}

	@Then("^User creates Classic Service case with client details like record type \"([^\"]*)\" Account Name \"([^\"]*)\" Contact Name \"([^\"]*)\" Region \"([^\"]*)\" Sales Area \"([^\"]*)\" Country \"([^\"]*)\" FirstName \"([^\"]*)\" LastName \"([^\"]*)\" WebEmail \"([^\"]*)\" Customer Contact \"([^\"]*)\" WebPhone \"([^\"]*)\"$")
	public void user_creates_Classic_Service_case_with_client_details_like_record_type_Account_Name_Contact_Name_FirstName_LastName_WebEmail_WebPhone(
			String RecordType, String Account, String Contact, String Region, String SalesArea, String Country,
			String FirstName, String LastName, String WebEmail, String CustomerContact, String WebPhone)
			throws Throwable {

		String RecT[] = RecordType.split(",");
		RecordType = CommonFunctions.readExcelMasterData(RecT[0], RecT[1], RecT[2]);

		String A[] = Account.split(",");
		Account = CommonFunctions.readExcelMasterData(A[0], A[1], A[2]);

		String C[] = Contact.split(",");
		Contact = CommonFunctions.readExcelMasterData(C[0], C[1], C[2]);

		String Reg[] = Region.split(",");
		Region = CommonFunctions.readExcelMasterData(Reg[0], Reg[1], Reg[2]);

		String Sa[] = SalesArea.split(",");
		SalesArea = CommonFunctions.readExcelMasterData(Sa[0], Sa[1], Sa[2]);

		String Co[] = Country.split(",");
		Country = CommonFunctions.readExcelMasterData(Co[0], Co[1], Co[2]);

		String F[] = FirstName.split(",");
		FirstName = CommonFunctions.readExcelMasterData(F[0], F[1], F[2]);

		String L[] = LastName.split(",");
		LastName = CommonFunctions.readExcelMasterData(L[0], L[1], L[2]);

		String W[] = WebEmail.split(",");
		WebEmail = CommonFunctions.readExcelMasterData(W[0], W[1], W[2]);

		String Cc[] = CustomerContact.split(",");
		CustomerContact = CommonFunctions.readExcelMasterData(Cc[0], Cc[1], Cc[2]);

		String P[] = WebPhone.split(",");
		WebPhone = CommonFunctions.readExcelMasterData(P[0], P[1], P[2]);

		ActionHandler.click(CaseObjectfieldsContainer.NewEnquiry);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on new button to create a Case");

		ActionHandler
				.click(driver.findElement(By.xpath(CaseObjectfieldsContainer.CaseRecordTypeSelection(RecordType))));
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Selects Case Record Type as " + RecordType);

		ActionHandler.click(CaseObjectfieldsContainer.NextEnquiry);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Next button to create " + RecordType + " Case");

		ActionHandler.click(CaseObjectfieldsContainer.AccountNameCase);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseObjectfieldsContainer.AccountNameCase, Account);
		ActionHandler.wait(1);
		ActionHandler.click(CaseObjectfieldsContainer.AccountSearchBar);
		ActionHandler.wait(1);
		ActionHandler.click(driver.findElement(By.xpath(CaseObjectfieldsContainer.AccountSelection(Account))));
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Select Account Name as " + Account);

		ActionHandler.click(CaseObjectfieldsContainer.ContactNameCase);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseObjectfieldsContainer.ContactNameCase, Contact);
		ActionHandler.wait(1);
		ActionHandler.click(CaseObjectfieldsContainer.ContactSearchBar);
		ActionHandler.wait(1);
		ActionHandler.click(driver.findElement(By.xpath(CaseObjectfieldsContainer.AccountSelection(Contact))));
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Select Contact Name as " + Contact);

		ActionHandler.click(CaseObjectfieldsContainer.CaseRegion);
		ActionHandler.wait(1);
		ActionHandler.click(driver.findElement(By.xpath(CaseObjectfieldsContainer.ValueSelection(Region))));
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Select Region Name as " + Region);

		ActionHandler.click(CaseObjectfieldsContainer.SalesArea);
		ActionHandler.wait(1);
		ActionHandler.click(driver.findElement(By.xpath(CaseObjectfieldsContainer.ValueSelection(SalesArea))));
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Select Sales Area as " + SalesArea);

		ActionHandler.click(CaseObjectfieldsContainer.Country);
		ActionHandler.wait(1);
		ActionHandler.click(driver.findElement(By.xpath(CaseObjectfieldsContainer.ValueSelection(Country))));
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Select Country Name as " + Country);

		ActionHandler.click(CaseObjectfieldsContainer.FirstNameCase);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseObjectfieldsContainer.FirstNameCase, FirstName);
		ActionHandler.wait(1);
		Reporter.addStepLog("User Enter First Name as " + FirstName);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(CaseObjectfieldsContainer.LastNameCase);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseObjectfieldsContainer.LastNameCase, LastName);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enter Last Name " + LastName);

		ActionHandler.click(CaseObjectfieldsContainer.WebEmailCase);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseObjectfieldsContainer.WebEmailCase, WebEmail);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enter Web Email Id as " + WebEmail);

		ActionHandler.click(CaseObjectfieldsContainer.CustomerContact);
		ActionHandler.wait(1);
		ActionHandler.click(driver.findElement(By.xpath(CaseObjectfieldsContainer.ValueSelection(CustomerContact))));
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Select Customer Contact as " + CustomerContact);

		ActionHandler.click(CaseObjectfieldsContainer.WebPhoneCase);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseObjectfieldsContainer.WebPhoneCase, WebPhone);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enter Web Phone as " + WebPhone);

		ActionHandler.click(CaseObjectfieldsContainer.SaveCase);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves the " + RecordType + " case");

	}

	@And("^Verify all Client Details of newly created Classic service case on case details page$")
	public void verify_all_Client_Details_of_newly_created_Classic_service_case_on_case_details_page()
			throws Throwable {
		ActionHandler.wait(3);
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.wait(1);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyAccount);
		String AccountName = CaseObjectfieldsContainer.VerifyAccount.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Account Name field with value displayed as " + AccountName);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyContact);
		String ContactName = CaseObjectfieldsContainer.VerifyContact.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Contact Name field with value displayed as " + ContactName);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyFirst);
		String FirstName = CaseObjectfieldsContainer.VerifyFirst.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies First Name field with value displayed as " + FirstName);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyLast);
		String LastName = CaseObjectfieldsContainer.VerifyLast.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Last Name field with value displayed as " + LastName);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyCustomer);
		String CustomerContact = CaseObjectfieldsContainer.VerifyCustomer.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"User verifies Customer Contact field with value field with value displayed as " + CustomerContact);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyRegion);
		String Region = CaseObjectfieldsContainer.VerifyRegion.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Region field with value field with value displayed as as " + Region);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyArea);
		String Area = CaseObjectfieldsContainer.VerifyArea.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Sales Area field with value field with value displayed as as " + Area);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyCountry);
		String Country = CaseObjectfieldsContainer.VerifyCountry.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Country field with value displayed as " + Country);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyWebEmail);
		String WebEmail = CaseObjectfieldsContainer.VerifyWebEmail.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Web Email field with value field with value displayed as as " + WebEmail);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyWebPhone);
		String WebPhone = CaseObjectfieldsContainer.VerifyWebPhone.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Web Phone field with value field with value displayed as as " + WebPhone);
	}

	@Then("^User created Classic Sales case with Vehicle details like RecordType \"([^\"]*)\" Program \"([^\"]*)\" Brand \"([^\"]*)\" and Model \"([^\"]*)\" Series name \"([^\"]*)\" Body Style \"([^\"]*)\" Production Year \"([^\"]*)\" VIN \"([^\"]*)\"$")
	public void user_created_Classic_Sales_case_with_Vehicle_details_like_RecordType_Program_Brand_and_Model_Series_name_Body_Style_Production_Year_VIN(
			String RecordType, String Program, String Brand, String Model, String SeriesName, String BodyStyle,
			String PYear, String VIN) throws Throwable {

		String RecT[] = RecordType.split(",");
		RecordType = CommonFunctions.readExcelMasterData(RecT[0], RecT[1], RecT[2]);

		String A[] = Program.split(",");
		Program = CommonFunctions.readExcelMasterData(A[0], A[1], A[2]);

		String C[] = Brand.split(",");
		Brand = CommonFunctions.readExcelMasterData(C[0], C[1], C[2]);

		String M[] = Model.split(",");
		Model = CommonFunctions.readExcelMasterData(M[0], M[1], C[2]);

		String Sn[] = SeriesName.split(",");
		SeriesName = CommonFunctions.readExcelMasterData(Sn[0], Sn[1], Sn[2]);

		String BS[] = BodyStyle.split(",");
		BodyStyle = CommonFunctions.readExcelMasterData(BS[0], BS[1], BS[2]);

		String V[] = VIN.split(",");
		VIN = CommonFunctions.readExcelMasterData(V[0], V[1], V[2]);

		String spec = "Test";

		ActionHandler.click(CaseObjectfieldsContainer.NewEnquiry);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on new button to create a Case");

		ActionHandler
				.click(driver.findElement(By.xpath(CaseObjectfieldsContainer.CaseRecordTypeSelection(RecordType))));
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Selects Case Record Type as " + RecordType);

		ActionHandler.click(CaseObjectfieldsContainer.NextEnquiry);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Next button to create " + RecordType + " Case");

		ActionHandler.click(CaseObjectfieldsContainer.Programme);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseObjectfieldsContainer.Programme, Program);
		ActionHandler.wait(1);
		ActionHandler.click(CaseObjectfieldsContainer.ProgrammeSearchBar);
		ActionHandler.wait(1);
		ActionHandler.click(driver.findElement(By.xpath(CaseObjectfieldsContainer.AccountSelection(Program))));
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enter Programme as " + Program);

		ActionHandler.click(CaseObjectfieldsContainer.BrandCase);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseObjectfieldsContainer.BrandCase, Brand);
		ActionHandler.wait(1);
		ActionHandler.click(CaseObjectfieldsContainer.BrandSearchBar);
		ActionHandler.wait(1);
		ActionHandler.click(driver.findElement(By.xpath(CaseObjectfieldsContainer.AccountSelection(Brand))));
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enter Brand as " + Brand);

		ActionHandler.click(CaseObjectfieldsContainer.ModelCase);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseObjectfieldsContainer.ModelCase, Model);
		ActionHandler.wait(1);
		ActionHandler.click(CaseObjectfieldsContainer.ModelSearchBar);
		ActionHandler.wait(1);
		ActionHandler.click(driver.findElement(By.xpath(CaseObjectfieldsContainer.AccountSelection(Model))));
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enter Model as " + Model);

		ActionHandler.click(CaseObjectfieldsContainer.SeriesNameCase);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseObjectfieldsContainer.SeriesNameCase, SeriesName);
		ActionHandler.wait(1);
		ActionHandler.click(CaseObjectfieldsContainer.SeriesSearchBar);
		ActionHandler.wait(1);
		ActionHandler.click(driver.findElement(By.xpath(CaseObjectfieldsContainer.AccountSelection(SeriesName))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enter Series Name as " + Model);

		ActionHandler.click(CaseObjectfieldsContainer.BodyStyleCase);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseObjectfieldsContainer.BodyStyleCase, BodyStyle);
		ActionHandler.wait(1);
		ActionHandler.click(CaseObjectfieldsContainer.BodyStyleSearchBar);
		ActionHandler.wait(1);
		ActionHandler.click(driver.findElement(By.xpath(CaseObjectfieldsContainer.AccountSelection(BodyStyle))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enter Body Style Case as " + Model);

		ActionHandler.click(CaseObjectfieldsContainer.VehicleRegistration);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseObjectfieldsContainer.VehicleRegistration, Program);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enter Vehicle Registration as " + Program);

		ActionHandler.click(CaseObjectfieldsContainer.ProdYear);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseObjectfieldsContainer.ProdYear, PYear);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enter Production Year as " + PYear);

		ActionHandler.click(CaseObjectfieldsContainer.HandOfDrive);
		ActionHandler.wait(1);
		Actions act7 = new Actions(driver);
		act7.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act7.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act7.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Select Hand Of Drive");

		ActionHandler.click(CaseObjectfieldsContainer.PrimaryExterior);
		ActionHandler.wait(1);
		Actions act8 = new Actions(driver);
		act8.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act8.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act8.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Select Primary Exterior Colour");

		ActionHandler.click(CaseObjectfieldsContainer.Spec);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseObjectfieldsContainer.Spec, spec);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enter Additional Spec Details");

		ActionHandler.click(CaseObjectfieldsContainer.VINCase);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseObjectfieldsContainer.VINCase, VIN);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enter VIN/Chassis Number as " + VIN);

		ActionHandler.click(CaseObjectfieldsContainer.VehicleCase);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseObjectfieldsContainer.VehicleCase, Model);
		ActionHandler.wait(1);
		Actions actv = new Actions(driver);
		ActionHandler.wait(1);
		actv.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		actv.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Selects Vehicle");

		ActionHandler.click(CaseObjectfieldsContainer.SaveCase);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves the " + RecordType + " case");
	}

	@Then("^Verify all the Vehicle Details of newly created Classic sales case on case details page$")
	public void verify_all_the_Vehicle_Details_of_newly_created_Classic_sales_case_on_case_details_page()
			throws Throwable {
		ActionHandler.wait(3);
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.wait(1);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyProgramme);
		String programme = CaseObjectfieldsContainer.VerifyProgramme.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Programme field with value displayed as " + programme);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyBrand);
		String brand = CaseObjectfieldsContainer.VerifyBrand.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Brand field displayed with value as " + brand);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyModel);
		String model = CaseObjectfieldsContainer.VerifyModel.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Model field displayed as " + model);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifySeriesName);
		String series = CaseObjectfieldsContainer.VerifySeriesName.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies vehicle Series Name field with value displayed as " + series);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyBodyStyle);
		String bodystyle = CaseObjectfieldsContainer.VerifyBodyStyle.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Body Style field with value displayed as " + bodystyle);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyRegistration);
		String Registration = CaseObjectfieldsContainer.VerifyCustomer.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Vehicle Registration field with value displayed as " + Registration);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyPYear);
		String ProdYear = CaseObjectfieldsContainer.VerifyPYear.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Production Year field with value displayed as " + ProdYear);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyHDrive);
		String hod = CaseObjectfieldsContainer.VerifyHDrive.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Hand of Drive field with value displayed as " + hod);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyPrimaryColour);
		String primarycolor = CaseObjectfieldsContainer.VerifyPrimaryColour.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Primary Exterior Colour field with value displayed as " + primarycolor);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifySpec);
		String j = CaseObjectfieldsContainer.VerifySpec.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Additional Spec Details field with value displayed as " + j);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyVIN);
		String k = CaseObjectfieldsContainer.VerifyVIN.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies VIN /Chassis Number field with value displayed as " + k);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyVehicle);
		String l = CaseObjectfieldsContainer.VerifyVehicle.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Vehicle field with value displayed as " + l);
	}

	@And("^User creates new Classic Service case with Enquiry Source and Retailer details like Recordtype \"([^\"]*)\" Source Campaign \"([^\"]*)\" Source KMI \"([^\"]*)\" Preferred Retailer \"([^\"]*)\"$")
	public void user_creates_new_Classic_Service_case_with_Enquiry_Source_and_Retailer_details_like_Recordtype_Source_Campaign_Source_KMI_Preferred_Retailer(
			String RecordType, String Camp, String kmi, String Retailer) throws Throwable {

		String RecT[] = RecordType.split(",");
		RecordType = CommonFunctions.readExcelMasterData(RecT[0], RecT[1], RecT[2]);

		String A[] = Camp.split(",");
		Camp = CommonFunctions.readExcelMasterData(A[0], A[1], A[2]);

		String C[] = kmi.split(",");
		kmi = CommonFunctions.readExcelMasterData(C[0], C[1], C[2]);

		String Re[] = Retailer.split(",");
		Retailer = CommonFunctions.readExcelMasterData(Re[0], Re[1], Re[2]);

		ActionHandler.click(CaseObjectfieldsContainer.NewEnquiry);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on new button to create a Case");

		ActionHandler
				.click(driver.findElement(By.xpath(CaseObjectfieldsContainer.CaseRecordTypeSelection(RecordType))));
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Selects Case Record Type as " + RecordType);

		ActionHandler.click(CaseObjectfieldsContainer.NextEnquiry);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Next button to create " + RecordType + " Case");

		ActionHandler.click(CaseObjectfieldsContainer.SourceCase);
		ActionHandler.wait(1);
		Actions act6 = new Actions(driver);
		act6.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act6.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act6.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Select Source");

		ActionHandler.click(CaseObjectfieldsContainer.Originating);
		ActionHandler.wait(1);
		Actions ac = new Actions(driver);
		ac.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		ac.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		ac.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Select Originating Division");

		ActionHandler.click(CaseObjectfieldsContainer.SourceCamp);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseObjectfieldsContainer.SourceCamp, Camp);
		ActionHandler.wait(1);
		ActionHandler.click(CaseObjectfieldsContainer.CampaignSearchBar);
		ActionHandler.wait(1);
		ActionHandler.click(driver.findElement(By.xpath(CaseObjectfieldsContainer.AccountSelection(Camp))));
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Selects Source Campaign as " + Camp);

		ActionHandler.click(CaseObjectfieldsContainer.SourceKMI);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseObjectfieldsContainer.SourceKMI, kmi);
		ActionHandler.wait(1);
		ActionHandler.click(CaseObjectfieldsContainer.KMISearchBar);
		ActionHandler.wait(1);
		ActionHandler.click(driver.findElement(By.xpath(CaseObjectfieldsContainer.AccountSelection(kmi))));
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Selects Source KMI as " + kmi);

		ActionHandler.click(CaseObjectfieldsContainer.PreffRetailer);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseObjectfieldsContainer.PreffRetailer, Retailer);
		ActionHandler.wait(1);
		ActionHandler.click(CaseObjectfieldsContainer.AccountSearchBar);
		ActionHandler.wait(1);
		ActionHandler.click(driver.findElement(By.xpath(CaseObjectfieldsContainer.AccountSelection(Retailer))));
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Selects Preffered Retailer as " + Retailer);

		ActionHandler.click(CaseObjectfieldsContainer.SaveCase);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves the " + RecordType + " case");
	}

	@Then("^Verify all the details of Enquiry Source section of newly created Classic service case on case details page$")
	public void verify_all_the_details_of_Enquiry_Source_section_of_newly_created_Classic_service_case_on_case_details_page()
			throws Throwable {

		ActionHandler.wait(1);
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.wait(1);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifySource);
		String source = CaseObjectfieldsContainer.VerifySource.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Source field displayed with value as " + source);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyCamp);
		String Campaign = CaseObjectfieldsContainer.VerifyCamp.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Source Campaign field displayed with value as " + Campaign);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyDivision);
		String OriginatingDivision = CaseObjectfieldsContainer.VerifyDivision.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Originating Division field displayed with value as " + OriginatingDivision);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyKMI);
		String KMI = CaseObjectfieldsContainer.VerifyKMI.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Source KMI field displayed with value as " + KMI);

	}

	@And("^Verify all the details of Retailer Details section of newly created Classic service case on case details page$")
	public void verify_all_the_details_of_Retailer_Details_section_of_newly_created_Classic_service_case_on_case_details_page()
			throws Throwable {
		ActionHandler.wait(1);
		ActionHandler.scrollDown();
		ActionHandler.wait(1);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyRetailer);
		String Retailer = CaseObjectfieldsContainer.VerifyRetailer.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Preferred Retailer field displayed with value as " + Retailer);
	}

	@And("^Verify all the details of System Information section of newly created Classic Sales case on case details page$")
	public void Verify_all_the_details_of_System_Information_section_of_newly_created_Classic_Sales_case_on_case_details_page()
			throws Throwable {

		ActionHandler.wait(1);
		ActionHandler.scrollDown();
		ActionHandler.wait(1);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyCreatedBy);
		String CreatedBy = CaseObjectfieldsContainer.VerifyCreatedBy.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Created By field displayed with value as " + CreatedBy);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyModifiedBy);
		String ModifiedBy = CaseObjectfieldsContainer.VerifyModifiedBy.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Last Modified By field displayed with value as " + ModifiedBy);

	}

	@And("^User creates new Classic Sales case with Enquiry Source and Retailer details like Recordtype \"([^\"]*)\" Source Campaign \"([^\"]*)\" Source KMI \"([^\"]*)\" Preferred Retailer \"([^\"]*)\"$")
	public void user_creates_new_Classic_Sales_case_with_Enquiry_Source_and_Retailer_details_like_Recordtype_Source_Campaign_Source_KMI_Preferred_Retailer(
			String RecordType, String Camp, String kmi, String Retailer) throws Throwable {

		String RecT[] = RecordType.split(",");
		RecordType = CommonFunctions.readExcelMasterData(RecT[0], RecT[1], RecT[2]);

		String A[] = Camp.split(",");
		Camp = CommonFunctions.readExcelMasterData(A[0], A[1], A[2]);

		String C[] = kmi.split(",");
		kmi = CommonFunctions.readExcelMasterData(C[0], C[1], C[2]);

		String Re[] = Retailer.split(",");
		Retailer = CommonFunctions.readExcelMasterData(Re[0], Re[1], Re[2]);

		ActionHandler.click(CaseObjectfieldsContainer.NewEnquiry);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on new button to create a Case");

		ActionHandler
				.click(driver.findElement(By.xpath(CaseObjectfieldsContainer.CaseRecordTypeSelection(RecordType))));
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Selects Case Record Type as " + RecordType);

		ActionHandler.click(CaseObjectfieldsContainer.NextEnquiry);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Next button to create " + RecordType + " Case");

		ActionHandler.click(CaseObjectfieldsContainer.SourceCase);
		ActionHandler.wait(1);
		Actions act6 = new Actions(driver);
		act6.sendKeys(Keys.ARROW_DOWN).build().perform();
		act6.sendKeys(Keys.ARROW_DOWN).build().perform();
		act6.sendKeys(Keys.ENTER).build().perform();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Select Source");

		ActionHandler.click(CaseObjectfieldsContainer.Originating);
		ActionHandler.wait(1);
		Actions ac = new Actions(driver);
		ac.sendKeys(Keys.ARROW_DOWN).build().perform();
		ac.sendKeys(Keys.ARROW_DOWN).build().perform();
		ac.sendKeys(Keys.ENTER).build().perform();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Select Originating Division");

		ActionHandler.click(CaseObjectfieldsContainer.SourceCamp);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseObjectfieldsContainer.SourceCamp, Camp);
		ActionHandler.wait(1);
		Actions actv = new Actions(driver);
		actv.sendKeys(Keys.ARROW_DOWN).build().perform();
		actv.sendKeys(Keys.ENTER).build().perform();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Selects Source Campaign as " + Camp);

		ActionHandler.click(CaseObjectfieldsContainer.SourceKMI);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseObjectfieldsContainer.SourceKMI, kmi);
		ActionHandler.wait(1);
		Actions a = new Actions(driver);
		a.sendKeys(Keys.ARROW_DOWN).build().perform();
		a.sendKeys(Keys.ARROW_DOWN).build().perform();
		a.sendKeys(Keys.ENTER).build().perform();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Selects Source KMI as " + kmi);

		ActionHandler.click(CaseObjectfieldsContainer.PreffRetailer);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseObjectfieldsContainer.PreffRetailer, Retailer);
		ActionHandler.wait(1);
		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Selects Preffered Retailer as " + Retailer);

		ActionHandler.click(CaseObjectfieldsContainer.SaveCase);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves the " + RecordType + " case");
	}

	@Then("^Verify all the details of Enquiry Source and Retailer section of newly created Classic Sales case on case details page$")
	public void Verify_all_the_details_of_Enquiry_Source_and_Retailer_section_of_newly_created_Classic_Sales_case_on_case_details_page()
			throws Throwable {

		ActionHandler.wait(1);
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.wait(1);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifySource);
		String source = CaseObjectfieldsContainer.VerifySource.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Source field displayed with value as " + source);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyCamp);
		String Campaign = CaseObjectfieldsContainer.VerifyCamp.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Source Campaign field displayed with value as " + Campaign);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyDivision);
		String OriginatingDivision = CaseObjectfieldsContainer.VerifyDivision.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Originating Division field displayed with value as " + OriginatingDivision);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyKMI);
		String KMI = CaseObjectfieldsContainer.VerifyKMI.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Source KMI field displayed with value as " + KMI);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyRetailer);
		String Retailer = CaseObjectfieldsContainer.VerifyRetailer.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Preferred Retailer field displayed with value as " + Retailer);
	}

	@Then("^Verify all the details of SLA section of newly created Classic service case on case details page$")
	public void Verify_all_the_details_of_SLA_section_of_newly_created_Classic_Service_case_on_case_details_page()
			throws Throwable {

	}

	@Then("^User created Classic Service case with Vehicle details like RecordType \"([^\"]*)\" Program \"([^\"]*)\" Brand \"([^\"]*)\" and Model \"([^\"]*)\" Series name \"([^\"]*)\" Body Style \"([^\"]*)\" Production Year \"([^\"]*)\" VIN \"([^\"]*)\"$")
	public void user_created_Classic_Service_case_with_Vehicle_details_like_RecordType_Program_Brand_and_Model_Series_name_Body_Style_Production_Year_VIN(
			String RecordType, String Program, String Brand, String Model, String SeriesName, String BodyStyle,
			String PYear, String VIN) throws Throwable {

		String RecT[] = RecordType.split(",");
		RecordType = CommonFunctions.readExcelMasterData(RecT[0], RecT[1], RecT[2]);

		String A[] = Program.split(",");
		Program = CommonFunctions.readExcelMasterData(A[0], A[1], A[2]);

		String C[] = Brand.split(",");
		Brand = CommonFunctions.readExcelMasterData(C[0], C[1], C[2]);

		String M[] = Model.split(",");
		Model = CommonFunctions.readExcelMasterData(M[0], M[1], C[2]);

		String Sn[] = SeriesName.split(",");
		SeriesName = CommonFunctions.readExcelMasterData(Sn[0], Sn[1], Sn[2]);

		String BS[] = BodyStyle.split(",");
		BodyStyle = CommonFunctions.readExcelMasterData(BS[0], BS[1], BS[2]);

		String V[] = VIN.split(",");
		VIN = CommonFunctions.readExcelMasterData(V[0], V[1], V[2]);

		String spec = "Test";

		ActionHandler.click(CaseObjectfieldsContainer.NewEnquiry);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on new button to create a Case");

		ActionHandler
				.click(driver.findElement(By.xpath(CaseObjectfieldsContainer.CaseRecordTypeSelection(RecordType))));
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Selects Case Record Type as " + RecordType);

		ActionHandler.click(CaseObjectfieldsContainer.NextEnquiry);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Next button to create " + RecordType + " Case");

		ActionHandler.click(CaseObjectfieldsContainer.Programme);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseObjectfieldsContainer.Programme, Program);
		ActionHandler.wait(1);
		ActionHandler.click(CaseObjectfieldsContainer.ProgrammeSearchBar);
		ActionHandler.wait(1);
		ActionHandler.click(driver.findElement(By.xpath(CaseObjectfieldsContainer.AccountSelection(Program))));
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enter Programme as " + Program);

		ActionHandler.click(CaseObjectfieldsContainer.BrandCase);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseObjectfieldsContainer.BrandCase, Brand);
		ActionHandler.wait(1);
		ActionHandler.click(CaseObjectfieldsContainer.BrandSearchBar);
		ActionHandler.wait(1);
		ActionHandler.click(driver.findElement(By.xpath(CaseObjectfieldsContainer.AccountSelection(Brand))));
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enter Brand as " + Brand);

		ActionHandler.click(CaseObjectfieldsContainer.ModelCase);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseObjectfieldsContainer.ModelCase, Model);
		ActionHandler.wait(1);
		ActionHandler.click(CaseObjectfieldsContainer.ModelSearchBar);
		ActionHandler.wait(1);
		ActionHandler.click(driver.findElement(By.xpath(CaseObjectfieldsContainer.AccountSelection(Model))));
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enter Model as " + Model);

		ActionHandler.click(CaseObjectfieldsContainer.SeriesNameCase);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseObjectfieldsContainer.SeriesNameCase, SeriesName);
		ActionHandler.wait(1);
		ActionHandler.click(CaseObjectfieldsContainer.SeriesSearchBar);
		ActionHandler.wait(1);
		ActionHandler.click(driver.findElement(By.xpath(CaseObjectfieldsContainer.AccountSelection(SeriesName))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enter Series Name as " + Model);

		ActionHandler.click(CaseObjectfieldsContainer.BodyStyleCase);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseObjectfieldsContainer.BodyStyleCase, BodyStyle);
		ActionHandler.wait(1);
		ActionHandler.click(CaseObjectfieldsContainer.BodyStyleSearchBar);
		ActionHandler.wait(1);
		ActionHandler.click(driver.findElement(By.xpath(CaseObjectfieldsContainer.AccountSelection(BodyStyle))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enter Body Style Case as " + Model);

		ActionHandler.click(CaseObjectfieldsContainer.VehicleRegistration);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseObjectfieldsContainer.VehicleRegistration, Program);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enter Vehicle Registration as " + Program);

		ActionHandler.click(CaseObjectfieldsContainer.ProdYear);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseObjectfieldsContainer.ProdYear, PYear);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enter Production Year as " + PYear);

		ActionHandler.click(CaseObjectfieldsContainer.HandOfDrive);
		ActionHandler.wait(1);
		Actions act7 = new Actions(driver);
		act7.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act7.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act7.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Select Hand Of Drive");

		ActionHandler.click(CaseObjectfieldsContainer.PrimaryExterior);
		ActionHandler.wait(1);
		Actions act8 = new Actions(driver);
		act8.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act8.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act8.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Select Primary Exterior Colour");

		ActionHandler.click(CaseObjectfieldsContainer.Spec);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseObjectfieldsContainer.Spec, spec);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enter Additional Spec Details as " + spec);

		ActionHandler.click(CaseObjectfieldsContainer.VINCase);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseObjectfieldsContainer.VINCase, VIN);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enter VIN/Chassis Number as " + VIN);

		ActionHandler.click(CaseObjectfieldsContainer.VehicleCase);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseObjectfieldsContainer.VehicleCase, Model);
		ActionHandler.wait(1);
		Actions actv = new Actions(driver);
		ActionHandler.wait(1);
		actv.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		actv.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Selects Vehicle");

		ActionHandler.click(CaseObjectfieldsContainer.SaveCase);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves the " + RecordType + " case");
	}

	@Then("^Verify all the Vehicle Details of newly created Classic Service case on case details page$")
	public void verify_all_the_Vehicle_Details_of_newly_created_Classic_Service_case_on_case_details_page()
			throws Throwable {
		ActionHandler.wait(3);
		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyProgramme);
		String programme = CaseObjectfieldsContainer.VerifyProgramme.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Programme field with value displayed as " + programme);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyBrand);
		String brand = CaseObjectfieldsContainer.VerifyBrand.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Brand field displayed with value as " + brand);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyModel);
		String model = CaseObjectfieldsContainer.VerifyModel.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Model field displayed as " + model);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifySeriesName);
		String series = CaseObjectfieldsContainer.VerifySeriesName.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies vehicle Series Name field with value displayed as " + series);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyBodyStyle);
		String bodystyle = CaseObjectfieldsContainer.VerifyBodyStyle.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Body Style field with value displayed as " + bodystyle);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyRegistration);
		String Registration = CaseObjectfieldsContainer.VerifyCustomer.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Vehicle Registration field with value displayed as " + Registration);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyPYear);
		String ProdYear = CaseObjectfieldsContainer.VerifyPYear.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Production Year field with value displayed as " + ProdYear);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyHDrive);
		String hod = CaseObjectfieldsContainer.VerifyHDrive.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Hand of Drive field with value displayed as " + hod);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyPrimaryColour);
		String primarycolor = CaseObjectfieldsContainer.VerifyPrimaryColour.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Primary Exterior Colour field with value displayed as " + primarycolor);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyVIN);
		String vin = CaseObjectfieldsContainer.VerifyVIN.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies VIN /Chassis Number field with value displayed as " + vin);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifySpec);
		String AddSpec = CaseObjectfieldsContainer.VerifySpec.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Additional Spec Details field with value displayed as " + AddSpec);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyVehicle);
		String vehicle = CaseObjectfieldsContainer.VerifyVehicle.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Vehicle field with value displayed as " + vehicle);
	}

	@Given("^User logins to SVO portal as a SVO Bespoke User UserName \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void User_Logins_to_SVO_portal_as_a_SVO_Bespoke_User_with_UserName(String userName, String password)
			throws Exception {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");
		onStart();
		driver = getDriver();

		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");
			vCode = checkEmail();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");
			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verifyBtn)) {
				ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			}
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	public String checkEmail() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.outlookSignin)) {

			ActionHandler.click(SVO_OpportunityContainer.outlookSignin);
			CommonFunctions.attachScreenshot();
			ActionHandler.setText(SVO_OpportunityContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVO_OpportunityContainer.outlookNext);
			CommonFunctions.attachScreenshot();
			ActionHandler.setText(SVO_OpportunityContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVO_OpportunityContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(507, 520);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	@Then("^User Verifies that there is no Cases Tab available$")
	public void User_Verifies_that_there_is_no_cases_tab() throws Throwable {

		if (!VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.CasesTab)) {
			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User verifies that there is no Cases Tab available");
		}
	}

	@Given("^Login to the SF SVO portal for Classic Services User with UserName \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void login_to_the_SF_SVO_Portal_for_classic_Service(String userName, String password) throws Throwable {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailForClassicServiceUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verifyBtn)) {
				ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			}
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	@Given("^laterally Access to SVO Portal by user name \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void laterally_Access_to_SVO_Portal_by_User_name_and_password(String UserName, String Password)
			throws Exception {
		driver = getDriver();
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);

		Reporter.addStepLog("User Logins to SVO Portal");

		String s[] = UserName.split(",");
		UserName = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String p[] = Password.split(",");
		Password = CommonFunctions.readExcelMasterData(p[0], p[1], p[2]);

		if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.icon_image)) {
			ActionHandler.click(SVO_OpportunityContainer.icon_image);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Click on Logout");
			ActionHandler.click(SVO_OpportunityContainer.Logout);
		}
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, UserName);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, Password);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();

		ActionHandler.waitForElement(SVOEnquiryContainer.SVOText, 60);
		CommonFunctions.attachScreenshot();
		VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
		Reporter.addStepLog("User navigate to SVO Home Page successfully");
	}

	@Then("^User Navigates to Activity tab and log a call$")
	public void Navigate_to_Activity_and_log_a_call() throws Throwable {

		String Subject = "Call";

		ActionHandler.scrollDown();
		ActionHandler.wait(1);

		ActionHandler.click(CaseObjectfieldsContainer.Activity);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to activity section");

		ActionHandler.wait(1);
		ActionHandler.scrollDown();
		ActionHandler.wait(1);

		ActionHandler.click(CaseObjectfieldsContainer.Add);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("clicks on add");

		ActionHandler.setText(CaseObjectfieldsContainer.subject, Subject);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enter subject as " + Subject);

		ActionHandler.wait(1);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);

		ActionHandler.click(CaseObjectfieldsContainer.saveActivity);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("saves the activity");

	}

	@Then("^User Move the Case stage to Closed state$")
	public void User_Move_Case_Stage_to_Closed() throws Throwable {

		ActionHandler.wait(1);
		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(1);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Verifies that the case status moved to Contacted");

		ActionHandler.click(CaseObjectfieldsContainer.MarkStatusCase);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Move the case status to Discovery");

		ActionHandler.click(CaseObjectfieldsContainer.MarkStatusCase);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Move the case status to Closed status");
	}
	
	

}
