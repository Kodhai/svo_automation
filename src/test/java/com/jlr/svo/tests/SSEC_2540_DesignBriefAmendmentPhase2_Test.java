package com.jlr.svo.tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.containers.SSEC_2159_BespokeOrderForm_CreationofOrderFormRecordsContainer;
import com.jlr.svo.containers.SVOAccountsContainer;
import com.jlr.svo.containers.SVOAdditionalVehicleContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.containers.SVOItemToApproveContainer;
import com.jlr.svo.containers.SVOOpportunityContainer;
import com.jlr.svo.containers.SVOWorkOrderContainer;
import com.jlr.svo.containers.SVO_OpportunityInvoiceContainer;
import com.jlr.svo.containers.SVO_RestrictedPartyScreeningContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class SSEC_2540_DesignBriefAmendmentPhase2_Test extends TestBaseCC {

	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory.getLogger(SVOWorkOrderProgrammeField_Test.class.getName());
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);

	SVOAccountsContainer SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
	SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	SVOAdditionalVehicleContainer SVOAdditionalvehicleContainer = PageFactory.initElements(driver,
			SVOAdditionalVehicleContainer.class);
	SVOWorkOrderContainer SVOWorkOrderContainer = PageFactory.initElements(driver, SVOWorkOrderContainer.class);
	SVO_OpportunityInvoiceContainer SVO_OpportunityInvoiceContainer = PageFactory.initElements(driver,
			SVO_OpportunityInvoiceContainer.class);
	SVOItemToApproveContainer SVOItemToapproveContainer = PageFactory.initElements(driver,
			SVOItemToApproveContainer.class);
	SVO_RestrictedPartyScreeningContainer RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
			SVO_RestrictedPartyScreeningContainer.class);
	SSEC_2159_BespokeOrderForm_CreationofOrderFormRecordsContainer BespokeOrderForm_CreationofOrderFormRecordsContainer = PageFactory.initElements(driver, SSEC_2159_BespokeOrderForm_CreationofOrderFormRecordsContainer.class);
	

	public static String verificationCode;
	public static String vCode;
	public static String veriCode;
	public static String WorkOrderNumber;
	public static String OpportunityName;

	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	public void onStart() {

		setupTest("SVOTest");
		driver = getDriver();

		SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
		SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		SVOAdditionalvehicleContainer = PageFactory.initElements(driver, SVOAdditionalVehicleContainer.class);
		SVOWorkOrderContainer = PageFactory.initElements(driver, SVOWorkOrderContainer.class);
		SSEC_2159_BespokeOrderForm_CreationofOrderFormRecordsContainer BespokeOrderForm_CreationofOrderFormRecordsContainer = PageFactory.initElements(driver, SSEC_2159_BespokeOrderForm_CreationofOrderFormRecordsContainer.class);

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		verificationCode = null;
		vCode = null;
		WorkOrderNumber = null;
		
		OpportunityName = SVOAdditionalvehicleContainer.OpportunityName.getText();


	}

    @Then("^Navigate to the new Bespoke Design Brief record created$")
    public void Navigate_to_new_Bespoke_Design_Brief_record_created() throws Exception{
    	
    	
    }

    //Click on the Headrest cover to select from the picklist
    @And("^Click on the Headrest cover to select from the picklist$")
    public void Click_on_Headrest_cover_to_select_from_picklist() throws Exception{
    	
    	
    }
    
    @And("^Click on the Calfrest cover to select from the picklist$")
    public void Click_on_Calfrest_cover_to_select_from_picklist() throws Exception{
    	
    }
    
    @And("^Click on the Parcel Shelf Interior to choose it from the picklist$")
    public void Click_on_Parcel_Shelf_Interior_to_choose_from_picklist() throws Exception{
    	
    	
    }

    @And("^Click on Parcel Shelf Color to choose depending on its interior material$")
    public void Click_on_ParcelShelfColor_to_choose() throws Exception{
    	
    	
    }
    
    @And("^Click on the Sun Roof Blind dropdown and select with prefix as M8E2$")
    public void Click_on_SunRoofBlind_dropdown_and_select() throws Exception{
    	
    	
    }
    
    @And("^User selects the Radiator Grille finish and Radiator Grille color from the picklist$")
    public void User_selects_RadiatorGrilleFinish_and_RadiatorGrilleColor() throws Exception{
    	
    	
    }
    
    @And("^User selects the Front Tow hook Cover from the front tow eye cover picklist$")
    public void User_selects_FrontTowHookCOver_from_FrontTowEyeCover() throws Exception{
    	
    	
    }
    
    @And("^User selects the Rear Tow hook cover from the Rear tow eye cover picklist$")
    public void User_selects_RearTowHookCover_from_RearTowEyeCover() throws Exception{
    	
    	
    }
    
    @And("^User selects the Script Badge from the picklist$")
    public void User_selects_Scrpt_badge_from_picklist() throws Exception{
    	
    	
    }
    
    @And("^User fills the Content Commodity fields$")
    public void User_fills_Content_Commodity_fields() throws Exception{
    	
    	
    }









}
