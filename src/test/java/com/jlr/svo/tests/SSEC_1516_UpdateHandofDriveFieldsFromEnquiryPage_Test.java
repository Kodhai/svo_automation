package com.jlr.svo.tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.containers.SSEC_1126_AddHandOfDriveToKMIContainer;
import com.jlr.svo.containers.SSEC_1516_UpdateHandofDriveFieldsFromEnquiryPageContainer;
import com.jlr.svo.containers.SVOAccountsContainer;
import com.jlr.svo.containers.SVOAdditionalVehicleContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.containers.SVOItemToApproveContainer;
import com.jlr.svo.containers.SVOOpportunityContainer;
import com.jlr.svo.containers.SVOVehicleManufacturerContainer;
import com.jlr.svo.containers.SVOWorkOrderContainer;
import com.jlr.svo.containers.SVO_CustomerResponsesContainer;
import com.jlr.svo.containers.SVO_EnquiryLostReasonContainer;
import com.jlr.svo.containers.SVO_OpportunityInvoiceContainer;
import com.jlr.svo.containers.SVO_OpportunitySecondaryOwnerContainer;
import com.jlr.svo.containers.SVO_Pricing_LogicContainer;
import com.jlr.svo.containers.SVO_RestrictedPartyScreeningContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.Then;

public class SSEC_1516_UpdateHandofDriveFieldsFromEnquiryPage_Test extends TestBaseCC{

		public ExtentTest extentLogger;
		private WebDriver driver = getDriver();
		CommonFunctions commonFunctions = new CommonFunctions(driver);
		private static final Logger LOGGER = LoggerFactory.getLogger(SSEC_1516_UpdateHandofDriveFieldsFromEnquiryPage_Test.class.getName());
		JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);

		SVOAccountsContainer SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
		SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
		SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
		SVOAdditionalVehicleContainer SVOAdditionalvehicleContainer = PageFactory.initElements(driver,
				SVOAdditionalVehicleContainer.class);
		SVO_RestrictedPartyScreeningContainer RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
				SVO_RestrictedPartyScreeningContainer.class);
		SVOItemToApproveContainer SVOItemToapproveContainer = PageFactory.initElements(driver,
				SVOItemToApproveContainer.class);
		SVO_CustomerResponsesContainer SVO_CustomerResponsesContainer = PageFactory.initElements(driver,
				SVO_CustomerResponsesContainer.class);
		SVO_OpportunitySecondaryOwnerContainer SVO_OpportunitySecondaryOwnerContainer = PageFactory.initElements(driver,
				SVO_OpportunitySecondaryOwnerContainer.class);
		SVO_OpportunityInvoiceContainer SVO_OpportunityInvoiceContainer = PageFactory.initElements(driver,
				SVO_OpportunityInvoiceContainer.class);
		SVO_Pricing_LogicContainer SVO_Pricing_LogicContainer = PageFactory.initElements(driver,
				SVO_Pricing_LogicContainer.class);
		SSEC_1126_AddHandOfDriveToKMIContainer AddHandOfDriveToKMIContainer = PageFactory.initElements(driver,
				SSEC_1126_AddHandOfDriveToKMIContainer.class);
		SSEC_1516_UpdateHandofDriveFieldsFromEnquiryPageContainer UpdateHandofDriveFieldsFromEnquiryPageContainer = PageFactory.initElements(driver,
				SSEC_1516_UpdateHandofDriveFieldsFromEnquiryPageContainer.class);
		SVOWorkOrderContainer SVOWorkOrderContainer = PageFactory.initElements(driver, SVOWorkOrderContainer.class);
		SVO_EnquiryLostReasonContainer SVO_EnquiryLostReasonContainer = PageFactory.initElements(driver,
				SVO_EnquiryLostReasonContainer.class);
		
		public static String verificationCode;
		public static String vCode;
		public static String veriCode;
		public static String OpportunityName;
		public static String KMIName; 
		

		public static double getRandomIntegerBetweenRange(double min, double max) {
			double x = (int) (Math.random() * ((max - min) + 1)) + min;
			return x;
		}
		
		public void onStart() {
			setupTest("SVOTest");
			driver = getDriver();
			javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

			
		}

		
		@Then("^User create new active KMI with all fields from enquiry page Contact details \"([^\"]*)\" Hand of Drive \"([^\"]*)\" Product Offering \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\"$")
		public void user_create_new_active_KMI_with_all_fields_from_enquiry_page(String contactdetails, String handofdrive, String productOffering, String brand, String model) throws Throwable {
		    
			String CD[] = contactdetails.split(",");
			contactdetails = CommonFunctions.readExcelMasterData(CD[0], CD[1], CD[2]);

			String proOff[] = productOffering.split(",");
			productOffering = CommonFunctions.readExcelMasterData(proOff[0], proOff[1], proOff[2]);
			
			String HOD[] = handofdrive.split(",");
			handofdrive = CommonFunctions.readExcelMasterData(HOD[0], HOD[1], HOD[2]);

			String b[] = brand.split(",");
			brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);
			
			String m[] = model.split(",");
			model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);
			
			
			//ActionHandler.click(AddHandOfDriveToKMIContainer.ActiveStatus);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User check box the active field on KMIs page");
			
			ActionHandler.pageScrollDown();
			ActionHandler.wait(2);
			ActionHandler.setText(UpdateHandofDriveFieldsFromEnquiryPageContainer.KMIContactNameSearch, contactdetails);
			CommonFunctions.attachScreenshot();

	        //ActionHandler.click(UpdateHandofDriveFieldsFromEnquiryPageContainer.KMIContactNameSearch);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(4);
			ActionHandler.click(driver.findElement(By.xpath(UpdateHandofDriveFieldsFromEnquiryPageContainer.ValueSelection(contactdetails))));
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects Contact for an MKI");
			
			ActionHandler.click(UpdateHandofDriveFieldsFromEnquiryPageContainer.KMIProductoffering);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(UpdateHandofDriveFieldsFromEnquiryPageContainer.ProdOffSelection(productOffering))));
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User chooses Product Offering for an KMI");
			
			
			ActionHandler.setText(UpdateHandofDriveFieldsFromEnquiryPageContainer.SVOBespokeBrand, brand);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(5);
			ActionHandler.pressEnter();
			ActionHandler.wait(2);
			//ActionHandler.click(UpdateHandofDriveFieldsFromEnquiryPageContainer.SVOBespokeBrandSearch);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(UpdateHandofDriveFieldsFromEnquiryPageContainer.BrandValueSelection(brand))));
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects Brand for an KMI");

			ActionHandler.wait(2);
			ActionHandler.setText(UpdateHandofDriveFieldsFromEnquiryPageContainer.SVOBespokeModel, model);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(5);
			ActionHandler.pressEnter();
			ActionHandler.wait(2);
			//ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(UpdateHandofDriveFieldsFromEnquiryPageContainer.ModelValueSelection(model))));
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects Model for an KMI");
			
			ActionHandler.click(UpdateHandofDriveFieldsFromEnquiryPageContainer.HandofDrive);
			ActionHandler.wait(5);
			ActionHandler.click(driver.findElement(By.xpath(UpdateHandofDriveFieldsFromEnquiryPageContainer.HandofDrive(handofdrive))));
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects hand of drive for an KMI");
			
			ActionHandler.click(UpdateHandofDriveFieldsFromEnquiryPageContainer.SaveKMIBtn);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User saves an KMI");
			
			ActionHandler.click(UpdateHandofDriveFieldsFromEnquiryPageContainer.SaveEditBtn);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			
			ActionHandler.pageScrollDown();
			ActionHandler.pageScrollDown();
			ActionHandler.pageScrollDown();
			ActionHandler.wait(2);
			
			KMIName = UpdateHandofDriveFieldsFromEnquiryPageContainer.KMIName.getText();
			Reporter.addStepLog("KMI Name is = " + KMIName);
		}
		
		@Then("^User edit enquiry to add new KMI$")
		public void User_edit_enquiry_to_add_new_KMI() throws Throwable {
			
			javaScriptUtil.clickElementByJS(UpdateHandofDriveFieldsFromEnquiryPageContainer.EditEnquiryBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User clicks on edit button");
			
			ActionHandler.pageScrollDown();
			ActionHandler.pageScrollDown();
			ActionHandler.pageScrollDown();
			ActionHandler.pageScrollDown();
			ActionHandler.wait(5);
			
			javaScriptUtil.clickElementByJS(UpdateHandofDriveFieldsFromEnquiryPageContainer.SourceKMISearchBox);
			ActionHandler.wait(5);
			ActionHandler.click(UpdateHandofDriveFieldsFromEnquiryPageContainer.NewKMIBtn);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User clicks on new KMI");
			
		}
		
		@Then("^User updates hand of drive details on KMI page Hand of drive \"([^\"]*)\"$")
		public void User_updates_hand_of_drive_details_on_KMI_page(String handofdrive) throws Throwable {
		    

			String HOD[] = handofdrive.split(",");
			handofdrive = CommonFunctions.readExcelMasterData(HOD[0], HOD[1], HOD[2]);
			
			//javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstOpportunityLink);
			//CommonFunctions.attachScreenshot();
			//Reporter.addStepLog("User select KMI");
			driver.navigate().refresh();
			ActionHandler.wait(8);
			javaScriptUtil.clickElementByJS(UpdateHandofDriveFieldsFromEnquiryPageContainer.EditKMIBtn);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User clicks on edit KMI");
			
			ActionHandler.pageScrollDown();
			ActionHandler.wait(5);
			ActionHandler.click(UpdateHandofDriveFieldsFromEnquiryPageContainer.HandofDriveTextArea);
			ActionHandler.wait(2);
			ActionHandler.click(driver.findElement(By.xpath(UpdateHandofDriveFieldsFromEnquiryPageContainer.HandofDriveSelection(handofdrive))));
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(UpdateHandofDriveFieldsFromEnquiryPageContainer.UpdateKMIBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User updates KMI on KMI page");
			
		}
		
		@Then("^User create new Inactive KMI with all fields from enquiry page Contact details \"([^\"]*)\" Hand of Drive \"([^\"]*)\" Product Offering \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\"$")
		public void user_create_new_Inactive_KMI_with_all_fields_from_enquiry_page(String contactdetails, String handofdrive, String productOffering, String brand, String model) throws Throwable {
		    
			String CD[] = contactdetails.split(",");
			contactdetails = CommonFunctions.readExcelMasterData(CD[0], CD[1], CD[2]);

			String proOff[] = productOffering.split(",");
			productOffering = CommonFunctions.readExcelMasterData(proOff[0], proOff[1], proOff[2]);
			
			String HOD[] = handofdrive.split(",");
			handofdrive = CommonFunctions.readExcelMasterData(HOD[0], HOD[1], HOD[2]);

			String b[] = brand.split(",");
			brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);
			
			String m[] = model.split(",");
			model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);
			
			
			ActionHandler.click(UpdateHandofDriveFieldsFromEnquiryPageContainer.ActiveStatus);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User check box the active field on KMIs page");
			
			ActionHandler.pageScrollDown();
			ActionHandler.wait(2);
			ActionHandler.setText(UpdateHandofDriveFieldsFromEnquiryPageContainer.KMIContactNameSearch, contactdetails);
			CommonFunctions.attachScreenshot();

	        //ActionHandler.click(UpdateHandofDriveFieldsFromEnquiryPageContainer.KMIContactNameSearch);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(4);
			ActionHandler.click(driver.findElement(By.xpath(UpdateHandofDriveFieldsFromEnquiryPageContainer.ValueSelection(contactdetails))));
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects Contact for an MKI");
			
			ActionHandler.click(UpdateHandofDriveFieldsFromEnquiryPageContainer.KMIProductoffering);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(UpdateHandofDriveFieldsFromEnquiryPageContainer.ProdOffSelection(productOffering))));
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User chooses Product Offering for an KMI");
			
			
			ActionHandler.setText(UpdateHandofDriveFieldsFromEnquiryPageContainer.SVOBespokeBrand, brand);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(5);
			ActionHandler.pressEnter();
			ActionHandler.wait(2);
			//ActionHandler.click(UpdateHandofDriveFieldsFromEnquiryPageContainer.SVOBespokeBrandSearch);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(UpdateHandofDriveFieldsFromEnquiryPageContainer.BrandValueSelection(brand))));
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects Brand for an KMI");

			ActionHandler.wait(2);
			ActionHandler.setText(UpdateHandofDriveFieldsFromEnquiryPageContainer.SVOBespokeModel, model);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(5);
			ActionHandler.pressEnter();
			ActionHandler.wait(2);
			//ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(UpdateHandofDriveFieldsFromEnquiryPageContainer.ModelValueSelection(model))));
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects Model for an KMI");
			
			ActionHandler.click(UpdateHandofDriveFieldsFromEnquiryPageContainer.HandofDrive);
			ActionHandler.wait(5);
			ActionHandler.click(driver.findElement(By.xpath(UpdateHandofDriveFieldsFromEnquiryPageContainer.HandofDrive(handofdrive))));
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects hand of drive for an KMI");
			
			ActionHandler.click(UpdateHandofDriveFieldsFromEnquiryPageContainer.SaveKMIBtn);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User saves an KMI");
			
			ActionHandler.click(UpdateHandofDriveFieldsFromEnquiryPageContainer.SaveEditBtn);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			
			ActionHandler.pageScrollDown();
			ActionHandler.pageScrollDown();
			ActionHandler.pageScrollDown();
			ActionHandler.wait(2);
			
			KMIName = UpdateHandofDriveFieldsFromEnquiryPageContainer.KMIName.getText();
			Reporter.addStepLog("KMI Name is = " + KMIName);
		}
		
		@Then("^User removes hand of drive details on KMI page Hand of drive \"([^\"]*)\"$")
		public void User_removes_hand_of_drive_details_on_KMI_page(String handofdrive) throws Throwable {
		    

			String HOD[] = handofdrive.split(",");
			handofdrive = CommonFunctions.readExcelMasterData(HOD[0], HOD[1], HOD[2]);
			
			
			driver.navigate().refresh();
			ActionHandler.wait(8);
			javaScriptUtil.clickElementByJS(UpdateHandofDriveFieldsFromEnquiryPageContainer.EditKMIBtn);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User clicks on edit KMI");
			
			ActionHandler.pageScrollDown();
			ActionHandler.wait(5);
			ActionHandler.click(UpdateHandofDriveFieldsFromEnquiryPageContainer.HandofDriveTextArea);
			ActionHandler.wait(2);
			ActionHandler.click(driver.findElement(By.xpath(UpdateHandofDriveFieldsFromEnquiryPageContainer.RemoveHandofDrive())));
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(UpdateHandofDriveFieldsFromEnquiryPageContainer.UpdateKMIBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User Removes HAnd of drive details from KMI page");
			
			
			/*ActionHandler.click(UpdateHandofDriveFieldsFromEnquiryPageContainer.HandofDriveTextArea);
			ActionHandler.wait(2);
			ActionHandler.click(UpdateHandofDriveFieldsFromEnquiryPageContainer.RemoveHandOfDrive);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(UpdateHandofDriveFieldsFromEnquiryPageContainer.UpdateKMIBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User updates KMI on KMI page");*/
			
		}
		
		@Then("^User not able to create new KMI from enquiry page$")
		public void User_not_able_to_create_new_KMI_from_enquiry_page() throws Throwable {
			
			javaScriptUtil.clickElementByJS(UpdateHandofDriveFieldsFromEnquiryPageContainer.EditEnquiryBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User clicks on edit button");
			
			if(!VerifyHandler.verifyElementPresent(UpdateHandofDriveFieldsFromEnquiryPageContainer.SourceKMISearchBox)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User not able to create new KMI from enquiry page");
			}
		}
		
		@Then("^Verify that enquiries tab is not available on Homepage$")
		public void Verify_that_enquiries_tab_is_not_available_on_Homepage() throws Throwable {
			
			if(!VerifyHandler.verifyElementPresent(SVO_EnquiryLostReasonContainer.EnquiriesTab)) {
				CommonFunctions.attachScreenshot();
				Reporter.addStepLog("Verify that enquiries tab is not available on Homepage");
				}
		}
		
		@Then("^Verify that KMI tab is not available on Homepage$")
		public void Verify_that_KMI_tab_is_not_available_on_Homepage() throws Throwable {
			
			if(!VerifyHandler.verifyElementPresent(AddHandOfDriveToKMIContainer.KMIsTab)) {
				CommonFunctions.attachScreenshot();
				Reporter.addStepLog("Verify that KMI tab is not available on Homepage");
				}
		}
		
		@Then("^User create new KMI without hand of drive all fields from enquiry page Contact details \"([^\"]*)\" Hand of Drive \"([^\"]*)\" Product Offering \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\"$")
		public void user_create_new_KMI_without_hand_of_drive_all_fields_from_enquiry_page(String contactdetails, String handofdrive, String productOffering, String brand, String model) throws Throwable {
		    
			String CD[] = contactdetails.split(",");
			contactdetails = CommonFunctions.readExcelMasterData(CD[0], CD[1], CD[2]);

			String proOff[] = productOffering.split(",");
			productOffering = CommonFunctions.readExcelMasterData(proOff[0], proOff[1], proOff[2]);
			
			String HOD[] = handofdrive.split(",");
			handofdrive = CommonFunctions.readExcelMasterData(HOD[0], HOD[1], HOD[2]);

			String b[] = brand.split(",");
			brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);
			
			String m[] = model.split(",");
			model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);
			
			
			//ActionHandler.click(AddHandOfDriveToKMIContainer.ActiveStatus);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User check box the active field on KMIs page");
			
			ActionHandler.pageScrollDown();
			ActionHandler.wait(2);
			ActionHandler.setText(UpdateHandofDriveFieldsFromEnquiryPageContainer.KMIContactNameSearch, contactdetails);
			CommonFunctions.attachScreenshot();

	        //ActionHandler.click(UpdateHandofDriveFieldsFromEnquiryPageContainer.KMIContactNameSearch);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(4);
			ActionHandler.click(driver.findElement(By.xpath(UpdateHandofDriveFieldsFromEnquiryPageContainer.ValueSelection(contactdetails))));
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects Contact for an MKI");
			
			ActionHandler.click(UpdateHandofDriveFieldsFromEnquiryPageContainer.KMIProductoffering);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(UpdateHandofDriveFieldsFromEnquiryPageContainer.ProdOffSelection(productOffering))));
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User chooses Product Offering for an KMI");
			
			
			ActionHandler.setText(UpdateHandofDriveFieldsFromEnquiryPageContainer.SVOBespokeBrand, brand);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(5);
			ActionHandler.pressEnter();
			ActionHandler.wait(2);
			//ActionHandler.click(UpdateHandofDriveFieldsFromEnquiryPageContainer.SVOBespokeBrandSearch);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(UpdateHandofDriveFieldsFromEnquiryPageContainer.BrandValueSelection(brand))));
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects Brand for an KMI");

			ActionHandler.wait(2);
			ActionHandler.setText(UpdateHandofDriveFieldsFromEnquiryPageContainer.SVOBespokeModel, model);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(5);
			ActionHandler.pressEnter();
			ActionHandler.wait(2);
			//ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(driver.findElement(By.xpath(UpdateHandofDriveFieldsFromEnquiryPageContainer.ModelValueSelection(model))));
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects Model for an KMI");
			
			ActionHandler.click(UpdateHandofDriveFieldsFromEnquiryPageContainer.SaveKMIBtn);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User saves an KMI");
			
			ActionHandler.click(UpdateHandofDriveFieldsFromEnquiryPageContainer.SaveEditBtn);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			
			ActionHandler.pageScrollDown();
			ActionHandler.pageScrollDown();
			ActionHandler.pageScrollDown();
			ActionHandler.wait(2);
			
			KMIName = UpdateHandofDriveFieldsFromEnquiryPageContainer.KMIName.getText();
			Reporter.addStepLog("KMI Name is = " + KMIName);
		}
		
		@Then("^Search and select enquiry \"([^\"]*)\"$")
		public void Search_and_select_enquiry(String Status) throws Throwable {

			String b[] = Status.split(",");
			Status = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);
			
			ActionHandler.click(UpdateHandofDriveFieldsFromEnquiryPageContainer.EnquirySearchBox);
			ActionHandler.wait(2);
			ActionHandler.setText(UpdateHandofDriveFieldsFromEnquiryPageContainer.EnquirySearchBox, Status);
			CommonFunctions.attachScreenshot();
			ActionHandler.pressEnter();
			ActionHandler.wait(5);
			javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstOpportunityLink);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User select closed lost enquiry");
			
			VerifyHandler.verifyElementPresent(UpdateHandofDriveFieldsFromEnquiryPageContainer.EnquiryStatus);
			CommonFunctions.attachScreenshot();
			String EnquiryStatus = UpdateHandofDriveFieldsFromEnquiryPageContainer.EnquiryStatus.getText();
			Reporter.addStepLog("Verify that enquiry status as " + EnquiryStatus);
			
		}
		
		@Then("^User views Hand of Drive details from All Active KMIs list view$")
		public void user_views_Hand_of_Drive_details_from_All_Active_KMIs_list_view() throws Throwable {
			javaScriptUtil.clickElementByJS(AddHandOfDriveToKMIContainer.KMIsTab);
			CommonFunctions.attachScreenshot();
			
			ActionHandler.click(AddHandOfDriveToKMIContainer.KMIsListViewDropdon);
			CommonFunctions.attachScreenshot();
			
			VerifyHandler.verifyElementPresent(AddHandOfDriveToKMIContainer.AllActiveKMIsListView);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User Verifies 'All Active KMIs' List view on KMIs page");
			
			ActionHandler.click(AddHandOfDriveToKMIContainer.AllActiveKMIsListView);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User clicks 'All Active KMIs' List view");
			
			VerifyHandler.verifyElementPresent(UpdateHandofDriveFieldsFromEnquiryPageContainer.HandOfDriveSection);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User Verifies hand of drive section on KMI page");
			
			ActionHandler.click(UpdateHandofDriveFieldsFromEnquiryPageContainer.EnquirySearchBox);
			ActionHandler.wait(2);
			ActionHandler.setText(UpdateHandofDriveFieldsFromEnquiryPageContainer.EnquirySearchBox,KMIName);
			CommonFunctions.attachScreenshot();
			ActionHandler.pressEnter();
			ActionHandler.wait(5);
			javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstOpportunityLink);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User select New KMI");
			
		}
		
		@Then("^User views Hand of Drive details from All Inactive KMIs list view$")
		public void user_views_Hand_of_Drive_details_from_All_Inactive_KMIs_list_view() throws Throwable {
			javaScriptUtil.clickElementByJS(AddHandOfDriveToKMIContainer.KMIsTab);
			CommonFunctions.attachScreenshot();
			
			ActionHandler.click(AddHandOfDriveToKMIContainer.KMIsListViewDropdon);
			CommonFunctions.attachScreenshot();
			
			VerifyHandler.verifyElementPresent(AddHandOfDriveToKMIContainer.AllInactiveKMIsListView);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User Verifies 'All InActive KMIs' List view on KMIs page");
			
			ActionHandler.click(AddHandOfDriveToKMIContainer.AllInactiveKMIsListView);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User clicks 'All INActive KMIs' List view");
			
			VerifyHandler.verifyElementPresent(UpdateHandofDriveFieldsFromEnquiryPageContainer.HandOfDriveSection);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User Verifies hand of drive section on KMI page");
			
			ActionHandler.click(UpdateHandofDriveFieldsFromEnquiryPageContainer.EnquirySearchBox);
			ActionHandler.wait(2);
			ActionHandler.setText(UpdateHandofDriveFieldsFromEnquiryPageContainer.EnquirySearchBox,KMIName);
			CommonFunctions.attachScreenshot();
			ActionHandler.pressEnter();
			ActionHandler.wait(5);
			javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstOpportunityLink);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User select New KMI");
			
		}
			
			
		
		

}
