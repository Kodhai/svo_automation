package com.jlr.svo.tests;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;

import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.autotest.utils.Config;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.constants.Constants;
import com.jlr.svo.containers.SSEC_1227_CasesCreationContainer;
import com.jlr.svo.containers.SSEC_1229_Case_QueuesContainer;
import com.jlr.svo.containers.SSEC_1237_CaseClosureContainer;
import com.jlr.svo.containers.SSEC_1562_CaseManagement_Queues_Groups_SharingRulesSetupContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SSEC_1229_Case_Queues extends TestBaseCC {

	private WebDriver driver = getDriver();
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SSEC_1227_CasesCreationContainer CasesContainer = PageFactory.initElements(driver,
			SSEC_1227_CasesCreationContainer.class);
	SSEC_1237_CaseClosureContainer CaseClosureContainer = PageFactory.initElements(driver,
			SSEC_1237_CaseClosureContainer.class);
	SSEC_1229_Case_QueuesContainer Case_QueuesContainer = PageFactory.initElements(driver, SSEC_1229_Case_QueuesContainer.class); 
	SSEC_1562_CaseManagement_Queues_Groups_SharingRulesSetupContainer CaseManagement_Queues_Groups_SharingRulesSetupContainer = PageFactory.initElements(driver, SSEC_1562_CaseManagement_Queues_Groups_SharingRulesSetupContainer.class); 

	//
	public static String verificationCode;
	public static String vCode;
	int randomNum = getRandomNumber(0, 10000);
	String firstName = "Test" + randomNum;
	String lastName = "tes" + randomNum;
	String VINNum = "Test";
	String Chassis = "T" + randomNum;
	String Chassis2 = VINNum + randomNum;

	public static int getRandomNumber(int min, int max) {
		int x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	// onStart
	public void onStart() {
		setupTest("SVOTest");
		driver = getDriver();
		CasesContainer = PageFactory.initElements(driver, SSEC_1227_CasesCreationContainer.class);
		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		Case_QueuesContainer = PageFactory.initElements(driver, SSEC_1229_Case_QueuesContainer.class);
		CaseClosureContainer = PageFactory.initElements(driver, SSEC_1237_CaseClosureContainer.class);
		CaseManagement_Queues_Groups_SharingRulesSetupContainer = PageFactory.initElements(driver, SSEC_1562_CaseManagement_Queues_Groups_SharingRulesSetupContainer.class);

		verificationCode = null;

	}
	
	
	public String checkEmailForClassicPartsTechUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(493, 499);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}
	
	
	@Given("^Access to SVO Portal as Classic Parts User with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void Access_SVO_Portal_Classic_Parts_with_username_password(String userName, String password)
			throws Exception {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		Reporter.addStepLog("User access SF SVO Portal");

		onStart();
		driver = getDriver();
		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailForClassicPartsTechUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}
	
	
	@Given("^Access to SVO Portal as Classic Parts Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void Access_SVO_Portal_Classic_Parts(String userName, String password)
			throws Exception {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		Reporter.addStepLog("User access SF SVO Portal");

		//onStart();
		//driver = getDriver();
		//driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailForClassicPartsTechUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}
	
	
	
	
	@When("^User Navigates to cases tab$")
	public void User_navigates_cases_tab() throws Throwable {

		javaScriptUtil.clickElementByJS(CasesContainer.CasesTab);
		ActionHandler.wait(5);
	}
//
	@Then("^User creates Parts and Technical Record type case with mandatory fields like VIN$")
	public void user_creates_New_case() throws Exception {

		ActionHandler.click(CaseClosureContainer.NewBtn);
		ActionHandler.wait(2);
		ActionHandler.click(Case_QueuesContainer.PartandTechRecordType);
		ActionHandler.wait(2);
		ActionHandler.click(CasesContainer.NextBtn);
		ActionHandler.wait(3);
		
		ActionHandler.setText(CasesContainer.FirstNameBtn, firstName);
		ActionHandler.wait(2);

		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);
		ActionHandler.scrollToView(Case_QueuesContainer.VehicleDet);
		ActionHandler.wait(3);
		ActionHandler.setText(CasesContainer.VINText, Chassis);
		ActionHandler.wait(2);
	}
	
	@Then("^User saves the Case$")
	public void User_saves_the_customer_case() throws Exception {

		ActionHandler.click(Case_QueuesContainer.SaveBtn);
		ActionHandler.wait(3);
	}
	
    @And("^Verify that Case Owner has value as Parts and Technical Queue$")
    public void verify_case_owner_value_as_Parts_and_technical_queue() throws Exception{
    	
    	///Case Information scroll to view
    	ActionHandler.pageScrollDown();
    	ActionHandler.scrollToView(Case_QueuesContainer.CaseInfoText);
    	VerifyHandler.verifyElementHasFocus(Case_QueuesContainer.CaseOwnerTitle);
    	
    	if(VerifyHandler.verifyElementPresent(Case_QueuesContainer.COPartsTechQueue)) {
    		CommonFunctions.attachScreenshot();
    	}
    }
    
    @And("^Verify that user is able to close case for Parts and Technical Queue$")
    public void verify_user_closes_the_case_for_parts_and_technical() throws Exception{
    	
    	ActionHandler.pageCompleteScrollUp();
    	ActionHandler.wait(2);
    	ActionHandler.click(CaseClosureContainer.MarkCaseClose);
    	ActionHandler.wait(5);
    	
		ActionHandler.click(CaseClosureContainer.MarkCaseClose);
		ActionHandler.wait(5);
		
		ActionHandler.click(CaseClosureContainer.MarkCaseClose);
		ActionHandler.wait(5);

		ActionHandler.click(CaseClosureContainer.doneBtn);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User closes the case");
    }


    @And("^User logouts from portal$")
    public void user_logouts_from_portal() throws Exception{
    	ActionHandler.click(CasesContainer.ProfileBtn);
		ActionHandler.wait(3);
		ActionHandler.click(CasesContainer.LogoutBtn);
		ActionHandler.wait(5);
    }
    
    
    public String checkEmailForClassicPartsCSUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(494, 500);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}
    
    @Given("^Access to SVO portal as Customer Services User with UserName \"([^\"]*)\" and Password \"([^\"]*)\"$")
    public void Access_SVO_portal_Customer_service(String Username, String password) throws Exception{
    	
    	String un[] = Username.split(",");
		Username = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		Reporter.addStepLog("User access SF SVO Portal");

		onStart();
		driver = getDriver();
		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, Username);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailForClassicPartsCSUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, Username);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
    }

    
    @And("^User creates Customer services case with all mandatory fields$")
	public void User_creates_new_customer() throws Exception {

    	ActionHandler.click(CaseClosureContainer.NewBtn);
		ActionHandler.wait(2);
		ActionHandler.click(Case_QueuesContainer.CustServiceBtn);
		ActionHandler.wait(2);
		ActionHandler.click(CasesContainer.NextBtn);
		ActionHandler.wait(2);
		ActionHandler.setText(CasesContainer.FirstNameBtn, firstName);
		ActionHandler.wait(3);
	}
    
    @And("Verify that user is able to close case for Customer Service Queue$")
    public void verify_user_closes_case_for_Customer_Service_queue() throws Exception{
    	
    	ActionHandler.pageCompleteScrollUp();
    	ActionHandler.wait(2);
    	
    	ActionHandler.click(CaseClosureContainer.MarkCaseClose);
    	ActionHandler.wait(5);
    	
		ActionHandler.click(CaseClosureContainer.MarkCaseClose);
		ActionHandler.wait(5);
    	
		ActionHandler.click(CaseClosureContainer.MarkCaseClose);
		ActionHandler.wait(5);

		ActionHandler.click(CaseClosureContainer.doneBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User closes the case");
    }


    @And("^Verify that Case Owner has value as Customer Service Queue$")
    public void verify_case_owner_value_as_Customer_Service_queue() throws Exception{
    	
    	ActionHandler.pageScrollDown();
    	ActionHandler.wait(2);
    	ActionHandler.scrollToView(Case_QueuesContainer.CaseInfoText);
    	VerifyHandler.verifyElementPresent(Case_QueuesContainer.CaseOwnerTitle);
    	
    	if(VerifyHandler.verifyElementPresent(Case_QueuesContainer.COCustServQueue)) {
    		ActionHandler.wait(2);
    		CommonFunctions.attachScreenshot();
    	}
    }
    
    @And("^Verify that user is able to view all the Customer Service Queues in Cases tab$")
    public void verify_views_all_customer_services_queues() throws Exception{
    	
    	javaScriptUtil.clickElementByJS(CasesContainer.CasesTab);
		ActionHandler.wait(3);
		
		ActionHandler.click(Case_QueuesContainer.RecViewedCases);
		ActionHandler.wait(3);
		
		ActionHandler.click(Case_QueuesContainer.CustServQueu);
		ActionHandler.wait(2);
    }
    
    @And("^Verify that user is able to view all the Parts and Technical Queues in Cases tab$")
    public void verify_views_all_parts_technical_queues() throws Exception{
    	
    	javaScriptUtil.clickElementByJS(CasesContainer.CasesTab);
		ActionHandler.wait(3);
		
		ActionHandler.click(Case_QueuesContainer.RecViewedCases);
		ActionHandler.wait(3);
		
		ActionHandler.click(Case_QueuesContainer.PartsTechQueue);
		ActionHandler.wait(3);
    	
    }
    
    @And("^User edits the case from Customer Service Queue to Parts and Technical Queue$")
    public void user_edits_customer_service_queue_to_parts_and_technical_queue() throws Exception{
    	
    	ActionHandler.scrollToView(Case_QueuesContainer.CaseInfoText);
    	ActionHandler.wait(3);
    	//ActionHandler.scrollToView(Case_QueuesContainer.EditChangeOwner);
    	//ActionHandler.wait(3);
    	ActionHandler.click(Case_QueuesContainer.EditChangeOwner);
    	ActionHandler.wait(3);
    	
    	ActionHandler.click(Case_QueuesContainer.CaseOwnerDD);
    	ActionHandler.wait(3);
    	ActionHandler.click(Case_QueuesContainer.QueuesDD);
    	ActionHandler.wait(3);
    	ActionHandler.setText(CaseManagement_Queues_Groups_SharingRulesSetupContainer.SearchQueues, "Parts and Technical");
    	ActionHandler.wait(3);
    	
    	Actions act= new Actions(driver);
    	act.sendKeys(Keys.ARROW_DOWN).build().perform();
    	act.sendKeys(Keys.ENTER);
    	
    	ActionHandler.click(Case_QueuesContainer.ChangeOwnerBtn);
    	ActionHandler.wait(3);
    }
     @And("^User verifies that Case Owner is Parts and Technical Queue$")
     public void user_verifies_case_owner_Parts_and_Technical_queue() throws Exception{
    	 
    	 VerifyHandler.verifyElementPresent(Case_QueuesContainer.CaseOwnerTitle);
     	
     	if(VerifyHandler.verifyElementPresent(Case_QueuesContainer.CaseOwnerPartsTechQueue)) {
     		ActionHandler.wait(2);
     		CommonFunctions.attachScreenshot();
     	}
     }
     
     @Then("^User clicks on edit button of Case Owner of Parts and Technical Queue and verifies that remove Case owner is not present$")
     public void user_unable_to_remove_case_owner() throws Exception{
    	 
    	 ActionHandler.wait(2);
    	 ActionHandler.scrollToView(Case_QueuesContainer.CaseInfoText);
    	 ActionHandler.wait(3);
    	 ActionHandler.click(Case_QueuesContainer.EditChangeOwner);
    	 ActionHandler.wait(3);
    	 ActionHandler.click(Case_QueuesContainer.CancelChangeOwner);
    	 ActionHandler.wait(3);
    	 
     }

     
     

        
    	
    	
 





}
