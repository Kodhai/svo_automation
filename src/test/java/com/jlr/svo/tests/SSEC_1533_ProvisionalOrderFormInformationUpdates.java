package com.jlr.svo.tests;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.autotest.utils.Config;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.constants.Constants;
import com.jlr.svo.containers.SSEC_1126_AddHandOfDriveToKMIContainer;
import com.jlr.svo.containers.SSEC_1227_CasesCreationContainer;
import com.jlr.svo.containers.SSEC_1229_Case_QueuesContainer;
import com.jlr.svo.containers.SSEC_1532_AdditionOfMarketDataToKMIContainer;
import com.jlr.svo.containers.SSEC_1533_ProvisionalOrderFormContainer;
import com.jlr.svo.containers.SVOAdditionalVehicleContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.containers.SVOOpportunityContainer;
import com.jlr.svo.containers.SVO_EnquiryLostReasonContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SSEC_1533_ProvisionalOrderFormInformationUpdates extends TestBaseCC {
	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory.getLogger(SVO_CustomerResponse_Test.class.getName());
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);

	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	SVO_EnquiryLostReasonContainer SVO_EnquiryLostReasonContainer = PageFactory.initElements(driver,
			SVO_EnquiryLostReasonContainer.class);
	SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
	SSEC_1533_ProvisionalOrderFormContainer ProvisionalOrderFormContainer = PageFactory.initElements(driver, SSEC_1533_ProvisionalOrderFormContainer.class);
	SVOAdditionalVehicleContainer SVOAdditionalvehicleContainer = PageFactory.initElements(driver,
			SVOAdditionalVehicleContainer.class);
	SSEC_1126_AddHandOfDriveToKMIContainer AddHandOfDriveToKMIContainer = PageFactory.initElements(driver,
			SSEC_1126_AddHandOfDriveToKMIContainer.class);
	SSEC_1532_AdditionOfMarketDataToKMIContainer AdditionOfMarketDataToKMI = PageFactory.initElements(driver,
			SSEC_1532_AdditionOfMarketDataToKMIContainer.class);
	SSEC_1227_CasesCreationContainer CasesContainer = PageFactory.initElements(driver,
			SSEC_1227_CasesCreationContainer.class);
	SSEC_1229_Case_QueuesContainer Case_QueuesContainer = PageFactory.initElements(driver, SSEC_1229_Case_QueuesContainer.class); 

	public static String verificationCode;
	public static String vCode;
	public static String veriCode;
	public static String EnquiryCreatedDate;
	public static String EnquiryName;
	public static String EnquiryTitle;
	public static String EnquiryStatus;
	double randomNum = getRandomIntegerBetweenRange(0, 10000);
	String firstName = "Test" + randomNum;

	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	public void onStart() {

		setupTest("SVOTest");
		driver = getDriver();

		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
		SVO_EnquiryLostReasonContainer = PageFactory.initElements(driver,
				SVO_EnquiryLostReasonContainer.class);
		SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
		ProvisionalOrderFormContainer = PageFactory.initElements(driver, SSEC_1533_ProvisionalOrderFormContainer.class);
		SVOAdditionalvehicleContainer = PageFactory.initElements(driver,
					SVOAdditionalVehicleContainer.class);
		AddHandOfDriveToKMIContainer = PageFactory.initElements(driver,
				SSEC_1126_AddHandOfDriveToKMIContainer.class);
		AdditionOfMarketDataToKMI = PageFactory.initElements(driver,
				SSEC_1532_AdditionOfMarketDataToKMIContainer.class);
		CasesContainer = PageFactory.initElements(driver,
				SSEC_1227_CasesCreationContainer.class);
		Case_QueuesContainer = PageFactory.initElements(driver, SSEC_1229_Case_QueuesContainer.class); 
		
		driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);

		verificationCode = null;
		vCode = null;
		EnquiryCreatedDate = null;
		EnquiryName = null;
		EnquiryTitle = null;
		EnquiryStatus = null;
	}
	
	@Given("^Access to SVO portal for Classic Sales using Username \"([^\"]*)\" and password \"([^\"]*)\"$")
	public void Login_to_the_SVO_Portal_by_classic_Sales(String userName, String password) throws Throwable {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);
		onStart();

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailForClassicSalesUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	public String checkEmailForClassicSalesUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(513, 519);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	@And("^User navigates to Enquiry tab$")
	public void user_navigates_to_the_Enquiries_tab() throws Throwable {

		javaScriptUtil.clickElementByJS(SVO_EnquiryLostReasonContainer.EnquiriesTab);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigates to Enquiries tab");

	}
	
	@Then("^User creates new Classic Service enquiry with record type \"([^\"]*)\"$")
	public void user_creates_an_Enquiry_with_record_type(String RecType) throws Throwable {

		String b[] = RecType.split(",");
		RecType = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);


		ActionHandler.click(SVO_EnquiryLostReasonContainer.EnquiryNewButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks on to Enquiries new button");

		ActionHandler.wait(2);
		//ActionHandler.click(driver.findElement(By.xpath(SVO_EnquiryLostReasonContainer.RecTypeSelection(RecType))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects the enquiry record type");

		
		ActionHandler.click(SVO_EnquiryLostReasonContainer.EnquiryNextButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks on to Enquiries next button");
   }
	
    @And("^Verify that user is unable to add Provisional Order Form Signed Information on the Classic Service enquiry page$")
    public void verify_user_unable_to_add_ProvisionalOrderForm() throws Exception{
    	
		double randomnumber = getRandomIntegerBetweenRange(0, 1000);
    	
    	ActionHandler.setText(SVO_EnquiryLostReasonContainer.EnquiryTitleTextBox, "ClassicService" + randomnumber);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters enquiry title");
		
		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);

		ActionHandler.click(SVO_EnquiryLostReasonContainer.EnquirySaveButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks on to Enquiries Save button");
    }
    
    @Then("^User logout from portal$")
	public void logout_from_SVO_Portal() throws Throwable {

    	ActionHandler.click(CasesContainer.ProfileBtn);
		ActionHandler.wait(2);
		ActionHandler.click(CasesContainer.LogoutBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Logout");

    }
    
    @Then("^User creates new Classic Sales enquiry$")
    public void user_creates_new_Classic_Sales_enquiry() throws Exception{
    	
    	ActionHandler.click(SVO_EnquiryLostReasonContainer.EnquiryNewButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks on to Enquiries new button");

		ActionHandler.wait(2);
		ActionHandler.click(ProvisionalOrderFormContainer.ClassicSalesRecType);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects the enquiry record type");

		ActionHandler.wait(2);
		ActionHandler.click(SVO_EnquiryLostReasonContainer.EnquiryNextButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks on to Enquiries next button");
		
    }
    
    @And("Verify that user is able to add Provisional Order Form Signed information while creating Classic Sales enquiry$")
    public void user_able_to_add_ProvisionalOrderFormSigned() throws Exception{
    	
    	ActionHandler.click(AdditionOfMarketDataToKMI.Owner);
    	ActionHandler.wait(1);

    	ActionHandler.scrollToView(ProvisionalOrderFormContainer.ProvOrderFormText);
		VerifyHandler.verifyElementHasFocus(ProvisionalOrderFormContainer.ProvOrderFormText);
		
		ActionHandler.click(ProvisionalOrderFormContainer.ProvOrderFormCheckbox);
		ActionHandler.click(ProvisionalOrderFormContainer.DepositPaidCheckbox);
		
		ActionHandler.click(SVO_EnquiryLostReasonContainer.EnquirySaveButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks on to Enquiries Save button");
    }

    
    @And("^Creates new KMI with mandatory fields$")
    public void Creates_new_KMI(String contactdetails, String productOffering, String brand, String model) throws Exception{
    	
    	String CD[] = contactdetails.split(",");
		contactdetails = CommonFunctions.readExcelMasterData(CD[0], CD[1], CD[2]);

		String proOff[] = productOffering.split(",");
		productOffering = CommonFunctions.readExcelMasterData(proOff[0], proOff[1], proOff[2]);

		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);
		
		String m[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);
		
    	ActionHandler.click(ProvisionalOrderFormContainer.NewKMIBtn);
    	
    	ActionHandler.setText(AddHandOfDriveToKMIContainer.KMIContacts, contactdetails);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(AddHandOfDriveToKMIContainer.KMIContactNameSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(AddHandOfDriveToKMIContainer.ValueSelection(contactdetails))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Contact for an MKI");
		
		ActionHandler.click(AddHandOfDriveToKMIContainer.KMIProductoffering);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(AddHandOfDriveToKMIContainer.ProdOffSelection(productOffering))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Product Offering for an KMI");
		
		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeBrand, brand);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeBrandSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(brand))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Brand for an KMI");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeModel, model);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(model))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Model for an KMI");
    	
    }
    
    @Then("Verify user is able to add Provisional Order Form Signed information while creating KMI$")
    public void verify_user_is_able_to_add_ProvisionalOrderFormSigned() throws Exception{
    	
    	ActionHandler.click(ProvisionalOrderFormContainer.ProvOrderFormCheckbox);
    	ActionHandler.wait(1);
    	
    	ActionHandler.click(AddHandOfDriveToKMIContainer.SaveKMIBtn);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves an KMI");
    }
    
    @Then("^Verify that user is able to edit added Provisional Order Form Signed information on enquiry page$")
    public void user_able_to_edit_added_ProvisionalOrderForm() throws Exception{
    	
    	ActionHandler.pageScrollDown();
    	ActionHandler.wait(2);
    	ActionHandler.pageScrollDown();
    	ActionHandler.wait(2);
    	ActionHandler.pageScrollDown();
    	ActionHandler.wait(2);
    	
    	ActionHandler.click(ProvisionalOrderFormContainer.EditProvOrderFormBtn);
    	ActionHandler.wait(2);
    	
    	ActionHandler.click(ProvisionalOrderFormContainer.EditProvOrderCheckBox);
    	ActionHandler.wait(2);
    	
    	ActionHandler.click(ProvisionalOrderFormContainer.SaveEditBtn);
    	ActionHandler.wait(2);
    }

    @And("Enter the Enquiry Title for the new Enquiry creation$")
    public void enter_enquiry_title()throws Exception {
    	String enquiryTitle = "Test_Enquiry_";
		double randomNumber = getRandomIntegerBetweenRange(0, 1000);

		ActionHandler.wait(2);
		ActionHandler.setText(SVOEnquiryContainer.EnquiryTitle, enquiryTitle + randomNumber);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		System.out.println("User has entered Enquiry Title");
    }
    
    @Then("^User fills required details such as Product Offering \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\"$")
	public void user_fills_all_the_required_details_like_Product_Offering_Brand_Model(String productOffering, String brand, String model)
			throws Throwable {
    	
		String pOffering[] = productOffering.split(",");
		productOffering = CommonFunctions.readExcelMasterData(pOffering[0], pOffering[1], pOffering[2]);

		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String mo[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(mo[0], mo[1], mo[2]);

//		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.Showkeyfield)) {
//			ActionHandler.wait(3);
//			ActionHandler.click(SVOEnquiryContainer.Showkeyfield);
//			Reporter.addStepLog("Click on show more");
//			CommonFunctions.attachScreenshot();
//		}

		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		
//		ActionHandler.click(SVOEnquiryContainer.Vehicleinfoedit);
//		ActionHandler.wait(3);
//		Reporter.addStepLog("Click on edit button");
//		CommonFunctions.attachScreenshot();

		
		ActionHandler.setText(AdditionOfMarketDataToKMI.BrandSelect, "Land Rover");
		ActionHandler.wait(3);
		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.pressEnter();
		CommonFunctions.attachScreenshot();
		
//		ActionHandler.click(SVOEnquiryContainer.brandSearch);
//		ActionHandler.wait(7);
//		CommonFunctions.attachScreenshot();
//		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.brandSearch(brand))));
//		ActionHandler.wait(3);
//		Reporter.addStepLog("Enter brand information");
//		CommonFunctions.attachScreenshot();

		ActionHandler.setText(AdditionOfMarketDataToKMI.ModelSelect, "Land");
		ActionHandler.wait(2);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.pressEnter();
		CommonFunctions.attachScreenshot();
		
//		ActionHandler.click(SVOEnquiryContainer.modelSearch);
//		ActionHandler.wait(7);
//		CommonFunctions.attachScreenshot();
//		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.brandSearch(model))));
//		ActionHandler.wait(3);
//		Reporter.addStepLog("Enter model information");
//		CommonFunctions.attachScreenshot();

		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(1);
		ActionHandler.click(AdditionOfMarketDataToKMI.productOff);
		ActionHandler.wait(3);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.pressEnter();
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		
//		ActionHandler.click(driver.findElement(By.xpath(SVOEnquiryContainer.SourceSelection(productOffering))));
//		ActionHandler.wait(3);
//		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Input product offering information");

//		ActionHandler.click(SVOEnquiryContainer.Savechanges);
//		ActionHandler.wait(3);
//		CommonFunctions.attachScreenshot();
//		Reporter.addStepLog("click on save");
//
//		ActionHandler.pageScrollDown();
//		ActionHandler.pageArrowUp();
//		ActionHandler.pageArrowUp();
//
//		Reporter.addStepLog("Navigate to Details tab");
//		ActionHandler.click(SVOEnquiryContainer.enqDetails);
//		ActionHandler.wait(10);
//		CommonFunctions.attachScreenshot();
//
//		ActionHandler.wait(2);
//		
//		ActionHandler.click(SVOEnquiryContainer.saveEmailBtn);
//		ActionHandler.wait(3);
//		CommonFunctions.attachScreenshot();
//		Reporter.addStepLog("Save the changes");

	}
    
    @And("^User fills Preferred Contact as \"([^\"]*)\"$")
	public void User_fills_Preferred_Contact(String pContact) throws Throwable {

		String C[] = pContact.split(",");
		pContact = CommonFunctions.readExcelMasterData(C[0], C[1], C[2]);

		ActionHandler.pageDown();
		ActionHandler.click(AdditionOfMarketDataToKMI.PreferredContactBox);
		ActionHandler.wait(3);
		
		
		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.pressEnter();
		
//		ActionHandler.setText(AdditionOfMarketDataToKMI.SearchContactBox, pContact);
//		ActionHandler.wait(2);
//		ActionHandler.click(AdditionOfMarketDataToKMI.SearchElement);
//		ActionHandler.selectByValue(AdditionOfMarketDataToKMI.SearchElement, "Priya Kodhai");
//		ActionHandler.wait(3);

	}
    
    @When("^User Saves the enquiry$")
	public void save_the_enquiry() throws Throwable {
		ActionHandler.wait(3);
		ActionHandler.click(AdditionOfMarketDataToKMI.SaveEditBtn);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves Enquiry");

	}
    
    @Then("^User logs a call in the activity tab$")
	public void User_logs_call_to_activity_tab() throws Exception {

//    	ActionHandler.pageScrollDown();
//    	ActionHandler.wait(4);
//    	
    	ActionHandler.click(ProvisionalOrderFormContainer.ShowLessBtn);
		ActionHandler.click(AdditionOfMarketDataToKMI.ActivityTab);
		ActionHandler.wait(2);

		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);
		ActionHandler.click(AdditionOfMarketDataToKMI.AddBtn);
		ActionHandler.wait(3);

		ActionHandler.click(AdditionOfMarketDataToKMI.SubjectText);
		ActionHandler.wait(2);

		Actions act1 = new Actions(driver);
		ActionHandler.setText(AdditionOfMarketDataToKMI.callActivityText, "Call");
		ActionHandler.wait(3);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();

		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);
		ActionHandler.click(AdditionOfMarketDataToKMI.ActivitySaveBtn);
		ActionHandler.wait(3);

	}
    
    @Then("^User closes the enquiry with status as Qualified KMI$")
	public void Mark_As_Close() throws Exception {

		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(3);

		ActionHandler.click(AdditionOfMarketDataToKMI.ClosedTab);
		ActionHandler.wait(2);

		ActionHandler.click(SVOEnquiryContainer.MarkEnquiryComplete);
		ActionHandler.wait(3);

		ActionHandler.click(SVOEnquiryContainer.closedState);
		ActionHandler.wait(5);

		ActionHandler.click(AdditionOfMarketDataToKMI.qualifiedKMIBtn);
		ActionHandler.wait(3);

		ActionHandler.click(SVOEnquiryContainer.doneClosedState);
		ActionHandler.wait(5);

	}

    @Then("^User verifies new KMI record gets created in Quick Links$")
    public void user_verifies_new_KMI_record_gets_created() throws Exception{
    	
    	ActionHandler.pageDown();
		ActionHandler.wait(3);
		VerifyHandler.verifyElementHasFocus(ProvisionalOrderFormContainer.QuickLinksText);

    }
    
    @And("^Verify that Provisonal Order Form is dispalyed$")
    public void verify_provisional_order_form() throws Exception{
    	
    	VerifyHandler.verifyElementHasFocus(ProvisionalOrderFormContainer.KMIProvOrder);
    }

    @Given("^Access to SVO portal for Classic Service user using Username \"([^\"]*)\" and password \"([^\"]*)\"$")
	public void Login_to_the_SVO_Portal_for_classic_service(String userName, String password) throws Throwable {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);
		onStart();
        
		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailForClassicServiceUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	public String checkEmailForClassicServiceUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(515, 521);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}
	
	
	
	 @Given("^Access to SVO portal for Customer Services User with UserName \"([^\"]*)\" and Password \"([^\"]*)\"$")
	    public void Access_SVO_portal_Customer_service(String Username, String password) throws Exception{
	    	
	    	String un[] = Username.split(",");
			Username = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

			String pass[] = password.split(",");
			password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

			Reporter.addStepLog("User access SF SVO Portal");

			onStart();
			driver = getDriver();
			driver.get(Constants.SVOURL);

			Reporter.addStepLog("User Logins to SVO Portal");
			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, Username);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User requires to enter Verification code received in an email");

			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

				Reporter.addStepLog("Please Open recently received email from Salesforce");

				vCode = checkEmailForClassicCSUser();
				System.out.println("Verification code is = " + vCode);

				driver.get(Constants.SVOURL);
				Reporter.addStepLog("User Logins to SVO Portal");

				ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, Username);
				CommonFunctions.attachScreenshot();

				ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
				CommonFunctions.attachScreenshot();

				ActionHandler.click(SVOEnquiryContainer.loginBtn);
				CommonFunctions.attachScreenshot();

				ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
				CommonFunctions.attachScreenshot();

				ActionHandler.click(SVOEnquiryContainer.verifyBtn);
				CommonFunctions.attachScreenshot();
				Reporter.addStepLog("User logins to SVO Successfully");

			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

				Reporter.addStepLog("User asks to enter mobile number");
				CommonFunctions.attachScreenshot();
				if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

					ActionHandler.click(SVOEnquiryContainer.remindMeLater);
					ActionHandler.wait(15);
					CommonFunctions.attachScreenshot();
				} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

					ActionHandler.click(SVOEnquiryContainer.notRegister);
					ActionHandler.wait(15);
					CommonFunctions.attachScreenshot();
				} else {
					ActionHandler.wait(15);
					CommonFunctions.attachScreenshot();
					VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
					Reporter.addStepLog("User navigate to SVO Home Page successfully");
				}
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();

				if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
					SVOenquiry.navigateToSVO();

				}
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
	    }
	 
	 public String checkEmailForClassicCSUser() throws Exception {
			String parentWindow = driver.getWindowHandle();
			System.out.println("Window Id for SF SVO is = " + parentWindow);

			String userName = Config.getPropertyValue("Outlook_UserName");
			String password = Config.getPropertyValue("Outlook_Password");

			driver.get(Constants.outlookURL);

			Reporter.addStepLog("User need to access Outlook account to get verification code");
			CommonFunctions.attachScreenshot();

			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

				ActionHandler.click(SVOEnquiryContainer.outlookSignin);
				CommonFunctions.attachScreenshot();

				ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
				CommonFunctions.attachScreenshot();

				ActionHandler.click(SVOEnquiryContainer.outlookNext);
				CommonFunctions.attachScreenshot();

				ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
				CommonFunctions.attachScreenshot();

				ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
				CommonFunctions.attachScreenshot();

				ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
				CommonFunctions.attachScreenshot();
			}

			String outlookWindow = driver.getWindowHandle();
			ActionHandler.click(SVOEnquiryContainer.outlookIcon);
			ActionHandler.wait(12);
			Set<String> outlookWindows = driver.getWindowHandles();
			Iterator<String> i = outlookWindows.iterator();
			while (i.hasNext()) {
				String mailWindow = i.next();
				if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
					driver.switchTo().window(mailWindow);
					ActionHandler.click(SVOEnquiryContainer.outlookEmail);

					CommonFunctions.attachScreenshot();
					verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
					vCode = verificationCode.substring(494, 500);
					System.out.println("Verification Code is = " + vCode);
					driver.close();
					driver.switchTo().window(outlookWindow);
				}
			}
			CommonFunctions.attachScreenshot();
			return vCode;
		}


	 @When("^User navigates to the enquiry tab$")
		public void User_navigates_cases_tab() throws Throwable {

			javaScriptUtil.clickElementByJS(CasesContainer.CasesTab);
			ActionHandler.wait(5);
		}


	 @And("^User creates new Customer Services record type$")
		public void User_creates_new_customer() throws Exception {

		    ActionHandler.click(CasesContainer.NewCaseBtn);
			ActionHandler.click(Case_QueuesContainer.CustServiceBtn);
			ActionHandler.wait(2);
			ActionHandler.click(CasesContainer.NextBtn);
			ActionHandler.wait(2);

	 }
	 
	 @And("^User verifies that no Provisional Order Form Signed checkbox is available$")
	 public void user_verifies_no_ProvisionalOrderFormSigned_available() throws Exception{
		 
		 ActionHandler.setText(CasesContainer.FirstNameBtn, firstName);
		 ActionHandler.wait(2);
		 ActionHandler.pageScrollDown();
		 ActionHandler.wait(1);
	 }
	 
	 @Then("^User cancels the case creation$")
	 public void user_cancels_case_creation() throws Exception{
		 
		 ActionHandler.click(ProvisionalOrderFormContainer.CancelCaseBtn);
	 }
	 
	 @And("^Verify that user is able to edit added Provisional Order Form Signed information in KMI section$")
	 public void verify_user_able_to_edit_added_ProvisionalOrderFormSigned() throws Exception{
		 
		 ActionHandler.pageScrollDown();
		 ActionHandler.wait(2);
		 
		 ActionHandler.click(ProvisionalOrderFormContainer.EditProvOrderFormBtn);
		 ActionHandler.wait(2);
		 
		 ActionHandler.click(ProvisionalOrderFormContainer.EditProvOrderCheckBox);
		 ActionHandler.wait(2);
		 
		 ActionHandler.click(ProvisionalOrderFormContainer.SaveEditKMIBtn);
		 ActionHandler.wait(2);
	 }
	 
	 @Then("^Verify that user is able to remove added Provisional Order Form Signed information for that enquiry$")
	 public void verify_user_able_to_remove_added_ProvisionalOrderForm() throws Exception{
		 
		 ActionHandler.pageDown();
		 ActionHandler.wait(2);
		 ActionHandler.pageDown();
		 ActionHandler.wait(2);
		 ActionHandler.pageDown();
		 ActionHandler.wait(2);
		 VerifyHandler.verifyElementHasFocus(ProvisionalOrderFormContainer.ProvOrderFormText);
		 ActionHandler.click(ProvisionalOrderFormContainer.EditProvOrderFormBtn);
		 ActionHandler.click(ProvisionalOrderFormContainer.EditProvOrderCheckBox);
		 ActionHandler.click(ProvisionalOrderFormContainer.SaveEditKMIBtn);

	 }
	 
     @Then("^User verifies new KMI record gets created and navigates to it$")
     public void user_verifies_new_KMI_record_and_navigates() throws Exception{
    	 
    	ActionHandler.pageDown();
    	ActionHandler.pageDown();
 		ActionHandler.wait(3);
 		VerifyHandler.verifyElementHasFocus(ProvisionalOrderFormContainer.QuickLinksText);
 		ActionHandler.click(AdditionOfMarketDataToKMI.LinkKMI);
 		ActionHandler.wait(3);
 		ActionHandler.click(AdditionOfMarketDataToKMI.KMIrecord);
 		ActionHandler.wait(3);
     }
     
     @And("^Verifies that user is not able to view edited Provisional Order Form Signed information from Enquiry page$")
     public void not_able_to_view_edited_ProvOrderForm_from_enquiryPage() throws Exception{
    	 
    	 VerifyHandler.verifyElementHasFocus(ProvisionalOrderFormContainer.KMIProvOrder);
    	 
     }
     
     @Then("^Verify that user is not able to view edited Provisional Order Form Signed information from KMI page$")
     public void verify_user_not_able_toView_edited_ProvOrderForm_from_KMIPage() throws Exception{
    	 
    	 driver.navigate().back();
    	 ActionHandler.wait(2);
    	 driver.navigate().back();
    	 ActionHandler.wait(2);

    	 ActionHandler.pageScrollDown();
    	 ActionHandler.wait(2);
    	 
    	 VerifyHandler.verifyElementHasFocus(ProvisionalOrderFormContainer.ProvOrderFormText);
    	 ActionHandler.wait(2);
     }







}

















