package com.jlr.svo.tests;

import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.autotest.utils.Config;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.constants.Constants;
import com.jlr.svo.containers.SVOAccountsContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.containers.SVOOpportunityContainer;
import com.jlr.svo.containers.SVOVehicleManufacturerContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class SVOVehicleManufacturer extends TestBaseCC
{
	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions=new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory.getLogger(SVOAccounts.class.getName());
	SVOAccountsContainer SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
	SVOVehicleManufacturerContainer SVOVehicleManufacturerContainer = PageFactory.initElements(driver, SVOVehicleManufacturerContainer.class);
	SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	public static String verificationCode;
	public static String vCode;
	public static String veriCode;

	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	public void onStart() {
		setupTest("SVOTest");
		driver = getDriver();
		SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		SVOVehicleManufacturerContainer = PageFactory.initElements(driver, SVOVehicleManufacturerContainer.class);
		SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
		SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
		verificationCode = null;
		vCode = null;
	}
	
	public String checkEmail() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");
		
		ActionHandler.wait(5);
		// onStart();
		driver.get(Constants.outlookURL);
		ActionHandler.wait(30);
		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.outlookSignin);
		ActionHandler.wait(15);
		CommonFunctions.attachScreenshot();
		
		ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		
		ActionHandler.click(SVOEnquiryContainer.outlookNext);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		
		ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		
		ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		
		ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(30);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);
				ActionHandler.wait(20);
				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(510, 515);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}
	
	public void allVehicleManufacturer() throws Exception
	{
		ActionHandler.wait(2);
		ActionHandler.click(SVOVehicleManufacturerContainer.recentlyViewed);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		
		ActionHandler.wait(2);
		ActionHandler.click(SVOVehicleManufacturerContainer.allVehicleManufacturer);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		
		Reporter.addStepLog("User navigates to All Vehicle Manufacturers");
		
	}
	
	// Below code is to navigate for SVO page//
		public void navigateToSVO() throws Exception {
			String SVO = "Special Vehicle Operations";
			ActionHandler.wait(3);
			Reporter.addStepLog("User tries to navigate to SVO Portal");
			if (VerifyHandler.verifyElementPresent(SVOAccountsContainer.jlrText)) {
				// user click on menu button//
				ActionHandler.wait(2);
				ActionHandler.click(SVOAccountsContainer.menuBtn);
				ActionHandler.wait(3);
				CommonFunctions.attachScreenshot();

				// user enter text in searc box//
				ActionHandler.setText(SVOAccountsContainer.searchBox, SVO);
				ActionHandler.wait(3);
				CommonFunctions.attachScreenshot();

				ActionHandler.click(driver.findElement(By.xpath(SVOAccountsContainer.selectMenu(SVO))));
				ActionHandler.wait(10);
				CommonFunctions.attachScreenshot();

				Reporter.addStepLog("User navigate to SVO Portal");
			}
		}

		@Given("^Access SF-SVO Portal$")
		public void Access_SFSVO_Portal() throws Exception
		{
			LOGGER.debug("Beginning scenario execution...");
			Reporter.addStepLog("User access SF SVO Portal");
			onStart();
			driver = getDriver();
			ActionHandler.wait(13);
			driver.get(Constants.SVOURL);
			ActionHandler.wait(15);
			Reporter.addStepLog("User Logins to SVO Portal");

			String UserName = Config.getPropertyValue("Admin_UserName");
			String Password = Config.getPropertyValue("Admin_Password");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, UserName);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, Password);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			ActionHandler.wait(7);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.vCodeTextBox);

			Reporter.addStepLog("User requires to enter Verification code received in an email");
			ActionHandler.wait(5);

			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {
				Reporter.addStepLog("Please Open recently received email from Salesforce");
				ActionHandler.wait(10);
				vCode = checkEmail();
				System.out.println("Verification code is = "+vCode);
				
				driver.get(Constants.SVOURL);
				ActionHandler.wait(15);
				Reporter.addStepLog("User Logins to SVO Portal");
			} 
			else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) 
			{
				ActionHandler.wait(5);
				Reporter.addStepLog("User asks to enter mobile number");
				CommonFunctions.attachScreenshot();
				if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {
					ActionHandler.wait(3);
					ActionHandler.click(SVOEnquiryContainer.remindMeLater);
					ActionHandler.wait(15);
					CommonFunctions.attachScreenshot();
				} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {
					ActionHandler.wait(3);
					ActionHandler.click(SVOEnquiryContainer.notRegister);
					ActionHandler.wait(15);
					CommonFunctions.attachScreenshot();
				} else {
					ActionHandler.wait(15);
					CommonFunctions.attachScreenshot();
					VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
					Reporter.addStepLog("User navigate to SVO Home Page successfully");
				}
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		}
		
		// navigate to vehicle manufacturers tab
		@Then("^Navigate to Vehicle manufacturers tab$")
		public void navigate_to_vehicle_manufacturers_tab() throws Throwable 
		{
			String search = "Vehicle Manufacturers";
			
			ActionHandler.wait(2);
			ActionHandler.click(SVOVehicleManufacturerContainer.sideMenu);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			
			ActionHandler.wait(2);
			ActionHandler.setText(SVOVehicleManufacturerContainer.seachBar, search);
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(1);
			ActionHandler.click(driver.findElement(By.xpath(SVOVehicleManufacturerContainer.menuSelection(search))));
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User navigates to Vehicle Manufacturers");
		}
		
		// click on change owner button
		@Then("^Select multiple Vehicle manufacturers and click on Change Owner$")
		public void select_multiple_vehicle_manufacturers_and_click_on_change_owner() throws Throwable 
		{
			// select vehicle manufacturers
			ActionHandler.wait(2);
			ActionHandler.click(SVO_OpportunityContainer.Vehicle1);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			
			ActionHandler.wait(2);
			ActionHandler.click(SVO_OpportunityContainer.Vehicle2);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			
			ActionHandler.wait(2);
			ActionHandler.click(SVO_OpportunityContainer.Vehicle3);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			// click on change owner
			ActionHandler.wait(2);
			ActionHandler.click(SVO_OpportunityContainer.ChangeOwnBtn);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("user navigates to vehicle manufacturers tab");
		}

		// change owner and save
		@Then("^Change the owner and save$")
		public void change_the_owner_and_save() throws Throwable 
		{
			String owner = Constants.User1;
			
			ActionHandler.wait(5);
			// user changes the owner and saves
			ActionHandler.setText(SVO_OpportunityContainer.inputOwner, owner);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			
			ActionHandler.wait(2);
			Actions act5 = new Actions(driver);
			//act5.sendKeys(Keys.ARROW_DOWN).build().perform();
			act5.sendKeys(Keys.ENTER).build().perform();
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(5);
			
			ActionHandler.wait(2);
			ActionHandler.click(driver.findElement(By.xpath(SVO_OpportunityContainer.StatusSelection(owner))));
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			
			ActionHandler.wait(2);
			ActionHandler.click(SVO_OpportunityContainer.submitOwner);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User changes the owner");
		}
		
		// select the check box for Is JLR owned brand field
		@Then("^Edit for Is JLR owned brand field and select the checkbox$")
		public void Edit_for_Is_JLR_owned_brand_field_and_select_the_checkbox() throws Throwable {
			ActionHandler.click(SVO_OpportunityContainer.SelectJLROwned);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVO_OpportunityContainer.EditJLROwned);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(SVO_OpportunityContainer.CheckJLROwned);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			ActionHandler.pressEnter();
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("user selects the checkbox");
		}

		// save the changes made for Is JLR Owned Brand
		@Then("^Save the changes and verify user is able to make the brand as Is JLR Owned Brand$")
		public void Save_the_changes_and_verify_user_is_able_to_make_the_brand_as_Is_JLR_Owned_Brand() throws Throwable {
			ActionHandler.click(SVO_OpportunityContainer.SaveOrderBtn);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("user saves the changes");
		}

		// open a manufacturer
		@Then("^Select a manufacturer$")
		public void Select_a_manufacturer() throws Throwable 
		{
			ActionHandler.wait(2);
			ActionHandler.click(SVO_OpportunityContainer.SelectManuf);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("user selects a manufacturer");
		}

		// open a model
		@Then("^Select a model$")
		public void Select_a_model() throws Throwable {
			ActionHandler.pageScrollDown();
			ActionHandler.wait(3);
			ActionHandler.click(SVO_OpportunityContainer.SelectModel);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("user selects a model");
		}

		// click on new button for vehicle series
		@Then("^Navigate to Vehicle series and click on New button$")
		public void Navigate_to_Vehicle_series_and_click_on_New_button() throws Throwable {
			ActionHandler.pageScrollDown();
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(SVOVehicleManufacturerContainer.NewVehicleSeries);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("user clicks on new button");
		}

		// enter mandatory details and save
		@Then("^Enter details Name \"([^\"]*)\" model year start \"([^\"]*)\" model year end \"([^\"]*)\" and save$")
		public void Enter_details_name_model_year_start_Model_year_end_and_save(String name, String start, String end)
				throws Throwable {
			ActionHandler.setText(SVO_OpportunityContainer.VehicleName, name);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVO_OpportunityContainer.startYear, start);
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVO_OpportunityContainer.endYear, end);
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVO_OpportunityContainer.SaveVehicle);
			ActionHandler.wait(7);
			Reporter.addStepLog("Click on the Save button");
			CommonFunctions.attachScreenshot();
		}
		
		@And("^User tries to change owner of Vehicle Manufacturer without selecting any Vehicle Manufacturer from list$")
		public void User_tries_change_owner_vehicle_manufacturer() throws Exception
		{
			ActionHandler.wait(2);
			allVehicleManufacturer();
			
			ActionHandler.wait(2);
			ActionHandler.click(SVOVehicleManufacturerContainer.changeOwnerBtn);
			ActionHandler.wait(7);
			CommonFunctions.attachScreenshot();
			
			Reporter.addStepLog("User cannot change owner of Vehicle Manufacturer without selecting any Vehicle Manufacturer");
		}
		
		@Then("^Logout from SF-SVO Portal$")
		public void Logout_SFSVO_Portal() throws Exception
		{
			ActionHandler.wait(5);
			ActionHandler.click(SVO_OpportunityContainer.icon_image);
			ActionHandler.wait(10);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Click on Logout");
			ActionHandler.click(SVO_OpportunityContainer.Logout);
			ActionHandler.wait(10);
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.userNameTextBox)) {
				MarkupHelper.createLabel("Logged out successfully", ExtentColor.GREEN);
			} else {
				MarkupHelper.createLabel("Log out failed", ExtentColor.RED);
			}

			driver.close();
			Reporter.addStepLog("Driver closed");
		}
		

		// navigate to body styles
		@Then("^Navigate to Body Styles and click on New button$")
		public void Navigate_to_body_styles_and_click_on_New_button() throws Throwable {
			ActionHandler.pageScrollDown();
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(SVO_OpportunityContainer.NewBodyStyles);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("user clicks on new button");
		}
		
		// enter mandatory fields and save
		@Then("^Enter Name \"([^\"]*)\" and save$")
		public void Enter_name_and_save(String name) throws Throwable {
			ActionHandler.setText(SVO_OpportunityContainer.VehicleName, name);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVO_OpportunityContainer.SaveVehicle);
			ActionHandler.wait(7);
			Reporter.addStepLog("Click on the Save button");
			CommonFunctions.attachScreenshot();
		}
		
		// navigate to vehicle derivatives
		@Then("^Navigate to Vehicle Derivatives and click on New button$")
		public void Navigate_to_Vehicle_Derivatives_and_click_on_New_button() throws Throwable {
			ActionHandler.pageScrollDown();
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(SVO_OpportunityContainer.NewDerivatives);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("user clicks on new button");
		}

		// enter mandatory fields and save
		@Then("^Enter derivative Name \"([^\"]*)\" model \"([^\"]*)\" series Name \"([^\"]*)\" and save$")
		public void Enter_derivative_name_series_Name_and_save(String name, String Model, String series) throws Throwable {
			String Mod[] = Model.split(",");
			Model = CommonFunctions.readExcelMasterData(Mod[0], Mod[1], Mod[2]);

			double randomNumber = getRandomIntegerBetweenRange(0, 1000);
			ActionHandler.setText(SVO_OpportunityContainer.VehicleName, name + randomNumber);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVO_OpportunityContainer.ClearModel);
			ActionHandler.wait(3);
			ActionHandler.setText(SVO_OpportunityContainer.model, Model);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Actions act = new Actions(driver);
			act.sendKeys(Keys.ARROW_DOWN).build().perform();
			act.sendKeys(Keys.ENTER).build().perform();
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVO_OpportunityContainer.SelectSeries, series);
			ActionHandler.wait(5);
			act.sendKeys(Keys.ARROW_DOWN).build().perform();
			act.sendKeys(Keys.ENTER).build().perform();
			ActionHandler.wait(4);
			Reporter.addStepLog("User enters series Name");
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVO_OpportunityContainer.SaveVehicle);
			ActionHandler.wait(7);
			Reporter.addStepLog("Click on the Save button");
			CommonFunctions.attachScreenshot();
		}
		
		// open sharing
		@Then("^Click on Sharing button$")
		public void Click_on_Sharing_button() throws Throwable {
			ActionHandler.click(SVO_OpportunityContainer.DropdownBtn);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(SVO_OpportunityContainer.SharingBtn);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("user clicks on new button");
		}

		// enter user and save
		@Then("^Enter the user name \"([^\"]*)\" and save$")
		public void Enter_the_user_name_and_save(String name) throws Throwable {
			String ShareName[] = name.split(",");
			name = CommonFunctions.readExcelMasterData(ShareName[0], ShareName[1], ShareName[2]);

			ActionHandler.setText(SVO_OpportunityContainer.SearchUser, name);
			ActionHandler.wait(5);
			Actions act = new Actions(driver);
			act.sendKeys(Keys.ARROW_DOWN).build().perform();
			act.sendKeys(Keys.ENTER).build().perform();
			ActionHandler.wait(4);
			Reporter.addStepLog("User enters sharing Name");
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVO_OpportunityContainer.SaveSharing);
			ActionHandler.wait(7);
			Reporter.addStepLog("Click on the Save button");
			CommonFunctions.attachScreenshot();
		}
		
		// enter model start year later than end year
				@Then("^Enter model start year as \"([^\"]*)\" model end year as \"([^\"]*)\" and save$")
				public void Enter_model_start_year_as_model_end_year_as_and_save(String start, String end) throws Throwable {
					ActionHandler.click(SVO_OpportunityContainer.ModelStart);
					ActionHandler.wait(5);
					CommonFunctions.attachScreenshot();
					// enter start and end year
					ActionHandler.clearAndSetText(SVO_OpportunityContainer.InputModelStart, start);
					ActionHandler.wait(5);
					CommonFunctions.attachScreenshot();
					ActionHandler.clearAndSetText(SVO_OpportunityContainer.InputModelEnd, end);
					ActionHandler.wait(5);
					CommonFunctions.attachScreenshot();
					ActionHandler.click(SVO_OpportunityContainer.SaveVehicle);
					ActionHandler.wait(5);
					CommonFunctions.attachScreenshot();
					Reporter.addStepLog("user enters start and end year");
				}

				// verify error message
				@Then("^Verify the error message displayed for model end year entered before model start year$")
				public void Verify_the_error_message_displayed_for_model_end_year_entered_before_model_start_year()
						throws Throwable {
					VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.YearError);
					ActionHandler.wait(5);
					CommonFunctions.attachScreenshot();
					ActionHandler.click(SVO_OpportunityContainer.cancelVehicle);
					Reporter.addStepLog("user verifies the error message");
				}

				// edit manufacturer name
				@Then("^Edit vehicle manufacturer name as \"([^\"]*)\" and save$")
				public void Enter_vehicle_manufacturer_name_as_and_save(String name) throws Throwable {
					ActionHandler.click(SVO_OpportunityContainer.EditName);
					ActionHandler.wait(5);
					CommonFunctions.attachScreenshot();
					// enter name and save
					ActionHandler.clearAndSetText(SVO_OpportunityContainer.InputName, name);
					ActionHandler.wait(5);
					CommonFunctions.attachScreenshot();
					ActionHandler.click(SVO_OpportunityContainer.SaveVehicle);
					ActionHandler.wait(5);
					CommonFunctions.attachScreenshot();
					Reporter.addStepLog("user enters name and saves");
				}

				// create new manufacturer
				@Then("^Create a new vehicle manufacturer with name \"([^\"]*)\" and save$")
				public void Create_a_new_vehicle_manufacturer_with_name_and_save(String name) throws Throwable 
				{
					double randomNumber = getRandomIntegerBetweenRange(0, 1000);
					
					ActionHandler.wait(2);
					ActionHandler.click(SVOVehicleManufacturerContainer.newVehicleManufacturer);
					ActionHandler.wait(3);
					CommonFunctions.attachScreenshot();
					
					ActionHandler.setText(SVO_OpportunityContainer.InputName, name+randomNumber);
					ActionHandler.wait(5);
					CommonFunctions.attachScreenshot();
					ActionHandler.click(SVO_OpportunityContainer.SaveVehicle);
					ActionHandler.wait(5);
					CommonFunctions.attachScreenshot();
					Reporter.addStepLog("user enters name and saves");
				}

				// change owner of manufacturer
				@Then("^Select only one Vehicle manufacturer and click on Change Owner$")
				public void select_only_one_vehicle_manufacturer_and_click_on_change_owner() throws Throwable
				{
					// select vehicle manufacturers
					ActionHandler.click(SVO_OpportunityContainer.Vehicle1);
					ActionHandler.wait(3);
					CommonFunctions.attachScreenshot();

					// click on change owner
					ActionHandler.click(SVO_OpportunityContainer.ChangeOwnBtn);
					ActionHandler.wait(7);
					CommonFunctions.attachScreenshot();
					Reporter.addStepLog("user clicks on change owner button");
				}
				
				// delete opportunity
				@Then("^Click on Delete button to delete vehicle manufacturer$")
				public void click_on_Delete_button() throws Throwable {
					ActionHandler.wait(5);
					CommonFunctions.attachScreenshot();
					// user deletes the opportunity
					ActionHandler.click(SVO_OpportunityContainer.OppDelete);
					ActionHandler.wait(10);
					CommonFunctions.attachScreenshot();
					ActionHandler.click(SVO_OpportunityContainer.OppDeleteConfirm);
					ActionHandler.wait(20);
					CommonFunctions.attachScreenshot();
					Reporter.addStepLog("Click on Delete button");
				}

				// search vehicle manufacturer
				@Then("^Search for \"([^\"]*)\" in search box$")
				public void Search_for_in_search_box(String manufacturer) throws Throwable {
					ActionHandler.click(SVO_OpportunityContainer.SearchBtn);
					ActionHandler.wait(5);
					CommonFunctions.attachScreenshot();
					ActionHandler.setText(SVO_OpportunityContainer.SearchBox, manufacturer);
					ActionHandler.wait(5);
					CommonFunctions.attachScreenshot();
					ActionHandler.pressEnter();
					Reporter.addStepLog("user searches for vehicle manufacturer section");
				}

				// verify search results
				@Then("^Verify no search results are found for Vehicle manufacturer$")
				public void Verify_no_search_results_are_found_for_Vehicle_manufacturer() throws Throwable {
					ActionHandler.wait(5);
					CommonFunctions.attachScreenshot();
					Reporter.addStepLog("user does not find vehicle manufacturer section");
				}

		
		@And("^User tries to add New Model for any Brand$")
		public void User_tries_to_add_New_Model_for_Brand() throws Exception
		{
			double randomNumber = getRandomIntegerBetweenRange(0, 1000);
			String modelName = "Test Model";
			ActionHandler.wait(2);
			//allVehicleManufacturer();
						
			ActionHandler.wait(2);
			ActionHandler.click(SVOVehicleManufacturerContainer.NewVehicleSeries);
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User clicks on New Model button");
			
			ActionHandler.wait(2);
			ActionHandler.click(SVOVehicleManufacturerContainer.nextRType);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			
			ActionHandler.wait(2);
			ActionHandler.setText(SVOVehicleManufacturerContainer.VehicleName, modelName+randomNumber);
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();
			
			ActionHandler.wait(2);
			ActionHandler.click(SVOVehicleManufacturerContainer.SaveVehicle);
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User adds new model for any brand under Vehicle Manufacturer");
		}
		
		@Then("^User tries to delete a Model of any Brand under Vehicle Manufacturer section$")
		public void User_tries_to_delete_model_of_any_vehicle() throws Exception
		{			
			ActionHandler.wait(2);
			ActionHandler.pageCompleteScrollDown();
			ActionHandler.wait(3);
			ActionHandler.click(SVOVehicleManufacturerContainer.firstModel);
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();
			
			ActionHandler.wait(2);
			ActionHandler.click(SVOVehicleManufacturerContainer.deleteModel);
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();
	
			ActionHandler.wait(2);
			ActionHandler.click(SVOVehicleManufacturerContainer.confirmDeleteModel);
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();
			
			Reporter.addStepLog("User tries to delete model of any brand under vehicle manufacturer section");
			
		}
}
