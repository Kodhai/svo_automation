package com.jlr.svo.tests;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.containers.SVOAdditionalVehicleContainer;
import com.jlr.svo.containers.SVOCommentFieldAdditionContainer;
import com.jlr.svo.containers.SVOWorkOrderContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;



public class SVO_CommentBoxAdditionTest extends TestBaseCC
{
	private WebDriver driver = getDriver();
	private static final Logger LOGGER = LoggerFactory.getLogger(SVO_CommentBoxAdditionTest.class.getName());
	SVOCommentFieldAdditionContainer SVOCommentFieldAdditionContainer = PageFactory.initElements(driver, SVOCommentFieldAdditionContainer.class);
	SVOWorkOrderContainer SVOWorkOrderContainer = PageFactory.initElements(driver, SVOWorkOrderContainer.class);
	public static String WorkOrderNumber;
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
	SVOAdditionalVehicleContainer SVOAdditionalvehicleContainer = PageFactory.initElements(driver, SVOAdditionalVehicleContainer.class);
	
	@And("^User opens first Work Order from the All Work Orders List$")
	public void User_Opens_first_work_order_from_all_work_orders_list() throws Exception
	{
		ActionHandler.wait(2);
		ActionHandler.click(SVOCommentFieldAdditionContainer.FirstWorkOrderfromAllList);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User should be able to view first work order details");
	}
	
	@Then("^User verifies that Comments fields are present on an existing Work Order$")
	public void User_verifies_Comments_fields_present_on_existing_work_order() throws Exception
	{
		ActionHandler.wait(2);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		ActionHandler.pageScrollDown();
		CommonFunctions.attachScreenshot();
		
		ActionHandler.wait(1);
		VerifyHandler.verifyElementPresent(SVOCommentFieldAdditionContainer.BuildStartComments);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifis that Build Start Comments box is present");
		
		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		VerifyHandler.verifyElementPresent(SVOCommentFieldAdditionContainer.StripAndPartsAndBiWComments);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifis that Strip, Parts and BiW Comments box is present");

		ActionHandler.wait(1);
		VerifyHandler.verifyElementPresent(SVOCommentFieldAdditionContainer.ChassisAndPreBuildAndBuildComments);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifis that Chassis, Pre-build and Build Comments box is present");
		
		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		VerifyHandler.verifyElementPresent(SVOCommentFieldAdditionContainer.PaintComments);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifis that Paint Comments box is present");
		
		ActionHandler.wait(1);
		VerifyHandler.verifyElementPresent(SVOCommentFieldAdditionContainer.EngineAndGearBoxAndTrimComments);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifis that Engine, Gearbox and Trim Comments box is present");
		
		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		VerifyHandler.verifyElementPresent(SVOCommentFieldAdditionContainer.SnaggingComments);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifis that Snagging Comments box is present");
		
		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		VerifyHandler.verifyElementPresent(SVOCommentFieldAdditionContainer.ABSComments);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifis that ABS Comments box is present");
		
		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		VerifyHandler.verifyElementPresent(SVOCommentFieldAdditionContainer.MOTComments);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifis that MOT Comments box is present");
	}
	
	@And("^User creates a new Work Order and enter comments in all the required section$")
	public void User_creates_new_work_order_enter_comments_in_all_required_section() throws Exception
	{
		ActionHandler.wait(2);
		ActionHandler.click(SVOWorkOrderContainer.NewButtonForCreateNewWorkOrder);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on new button in order to create new work order");

		ActionHandler.click(SVOWorkOrderContainer.NewBuildRecordType);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Selects New Build Record Type");

		ActionHandler.click(SVOWorkOrderContainer.NextButtonForCreateNewWorkOrder);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on next button");
		
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(SVOCommentFieldAdditionContainer.NewWOBuildStartCommentBox);
		ActionHandler.wait(1);
		ActionHandler.setText(SVOCommentFieldAdditionContainer.NewWOCommentBoxLine, "Test Build Start");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters comment for Build Start section");
		
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(SVOCommentFieldAdditionContainer.NewWOStripPartsBiWCommentBox);
		ActionHandler.wait(1);
		ActionHandler.setText(SVOCommentFieldAdditionContainer.NewWOCommentBoxLine, "Test Strip, Parts and BiW");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters comment for Strip, Parts and BiW section");
		
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(SVOCommentFieldAdditionContainer.NewWOChassisPreBuildBuildCommentBox);
		ActionHandler.wait(1);
		ActionHandler.setText(SVOCommentFieldAdditionContainer.NewWOCommentBoxLine, "Test Chassis, Pre-Build and Build");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters comment for Chassis, Pre-Build and Build section");
		
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(SVOCommentFieldAdditionContainer.NewWOPaintCommentBox);
		ActionHandler.wait(1);
		ActionHandler.setText(SVOCommentFieldAdditionContainer.NewWOCommentBoxLine, "Test Paint");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters comment for Paint section");
		
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(SVOCommentFieldAdditionContainer.NewWOEngineGearboxTrimCommentBox);
		ActionHandler.wait(1);
		ActionHandler.setText(SVOCommentFieldAdditionContainer.NewWOCommentBoxLine, "Test Engine, Gearbox and Trim");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters comment for Engine, Gearbox and Trim section");
		
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(SVOCommentFieldAdditionContainer.NewWOSnaggingCommentBox);
		ActionHandler.wait(1);
		ActionHandler.setText(SVOCommentFieldAdditionContainer.NewWOCommentBoxLine, "Test Snagging");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters comment for Snagging section");
		
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(SVOCommentFieldAdditionContainer.NewWOABSCommentBox);
		ActionHandler.wait(1);
		ActionHandler.setText(SVOCommentFieldAdditionContainer.NewWOCommentBoxLine, "Test ABS");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters comment for ABS section");
		
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(SVOCommentFieldAdditionContainer.NewWOMOTCommentBox);
		ActionHandler.wait(1);
		ActionHandler.setText(SVOCommentFieldAdditionContainer.NewWOCommentBoxLine, "Test MOT");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters comment for MOT section");
		
		ActionHandler.wait(2);
		ActionHandler.click(SVOWorkOrderContainer.SaveButtonForCreateNewWorkOrder);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Save button");

		WorkOrderNumber = SVOWorkOrderContainer.NewlyCreatedWorkOrderNumber.getText();
		Reporter.addStepLog("Work Order Number Name is = " + WorkOrderNumber);
	}
	
	@Then("^User verifies that the comments are added in all required section$")
	public void User_verifies_the_comments_are_added_in_all_required_section() throws Exception
	{
		User_verifies_Comments_fields_present_on_existing_work_order();
		Reporter.addStepLog("User verifies that Comments are added in all required section");
	}
	
	@Then("^User adds Comment under any section of an opened Work Order$")
	public void User_adds_comment_under_any_section_of_an_opened_Work_Order() throws Exception
	{
		ActionHandler.wait(1);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		
		ActionHandler.wait(1);
		javaScriptUtil.clickElementByJS(SVOCommentFieldAdditionContainer.WOBuildStartCommentEditBtn);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Edit button in order to add Comments");
		
		ActionHandler.wait(1);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		//javaScriptUtil.scrollIntoView(SVOCommentFieldAdditionContainer.NewWOChassisPreBuildBuildCommentBox);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		
		ActionHandler.wait(1);
		javaScriptUtil.clickElementByJS(SVOCommentFieldAdditionContainer.NewWOChassisPreBuildBuildCommentBox);
		ActionHandler.wait(1);
		ActionHandler.clearText(SVOCommentFieldAdditionContainer.WOChassisCommentBoxLine);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		
		ActionHandler.wait(2);
		ActionHandler.setText(SVOCommentFieldAdditionContainer.WOChassisCommentBoxLine, "Test New Comments");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User adds one Comment under Chassis, Pre-Build and Build section");
		
		ActionHandler.wait(1);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSaveOpportunity);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves the Work Order after adding comments for Chassis, Pre-Build and Build section");
	}
	
	@And("^User edits added comment under any section of an opened Work Order$")
	public void User_edits_added_comment_under_any_section_an_Opened_Work_Order() throws Exception
	{
		ActionHandler.wait(2);
		ActionHandler.click(SVOCommentFieldAdditionContainer.WorkOrderEditButton);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Edit button");
		
		ActionHandler.wait(1);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		ActionHandler.pageScrollDown();
		//javaScriptUtil.scrollIntoView(SVOCommentFieldAdditionContainer.WOEditChassisCommentBox);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		
		ActionHandler.wait(1);
		javaScriptUtil.clickElementByJS(SVOCommentFieldAdditionContainer.NewWOChassisPreBuildBuildCommentBox);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		ActionHandler.setText(SVOCommentFieldAdditionContainer.WOChassisCommentBoxLine, "Edit Chassis, Prebuild and Build Comment Text");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User edits Chassis, Prebuild and Build Comment Text");
		
		ActionHandler.wait(2);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSaveOpportunity);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves the Work Order after editing comments for Chassis, Pre-Build and Build section");
	}
	
	@Then("^User verify that the comments are updated for an opened Work Order$")
	public void User_verify_that_comments_are_updated_for_opened_Work_Order() throws Exception
	{
		User_verifies_Comments_fields_present_on_existing_work_order();
		Reporter.addStepLog("User verifies that Comments are updated for Chassis, PreBuild and Build section");
	}
	
	@And("^User opens first Work Order from the All Work Orders List after sorting it$")
	public void User_opens_first_work_order_from_all_work_orders_after_sorting_it() throws Exception
	{
		ActionHandler.wait(1);
		ActionHandler.click(SVOCommentFieldAdditionContainer.AllWorkOrderSort);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		
		ActionHandler.wait(1);
		ActionHandler.click(SVOCommentFieldAdditionContainer.FirstWorkOrderfromAllList);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User should be able to view first work order details");
	}
	
	@Then("^User adds Comment with Bold, Italic, Underline, Different Font Style, Font Color and Font Size for any section$")
	public void User_adds_comment_with_Bold_Italic_Underline_Different_Font_Style_Color_Size_section() throws Exception
	{
		ActionHandler.wait(1);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		
		ActionHandler.wait(1);
		javaScriptUtil.clickElementByJS(SVOCommentFieldAdditionContainer.WOBuildStartCommentEditBtn);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Edit button in order to add Comments");
		
		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		
		ActionHandler.wait(1);
		javaScriptUtil.clickElementByJS(SVOCommentFieldAdditionContainer.NewWOStripPartsBiWCommentBox);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		if(!VerifyHandler.verifyElementPresent(SVOCommentFieldAdditionContainer.WOStripPartsCommentBoxWithFormat))
		{
			Reporter.addStepLog("There is no text present inside Comment Box");
			ActionHandler.wait(1);
			ActionHandler.wait(1);
			ActionHandler.click(SVOCommentFieldAdditionContainer.WOStripPartsRemoveFormatting);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			
			ActionHandler.wait(1);
			//javaScriptUtil.clickElementByJS(SVOCommentFieldAdditionContainer.WOStripPartsCommentBoxWithFormat);
			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(1);
			ActionHandler.setText(SVOCommentFieldAdditionContainer.WOStripPartsCommentBoxWithFormatLine, "Enter Comment Text with formats");
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User enters Comment Text with formatting");
			
			Actions act = new Actions(driver);
			act.doubleClick(SVOCommentFieldAdditionContainer.WOStripPartsCommentBoxWithFormatLine);
			act.build().perform();
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			
			ActionHandler.wait(1);
			ActionHandler.click(SVOCommentFieldAdditionContainer.WOStripPartsCommentFontStyle);
			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(1);
			ActionHandler.click(SVOCommentFieldAdditionContainer.WOStripPartsCommentArialFont);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects Font Style as an Arial");
			
			ActionHandler.wait(1);
			ActionHandler.click(SVOCommentFieldAdditionContainer.WOStripPartsCommentFontSize);
			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(1);
			ActionHandler.click(SVOCommentFieldAdditionContainer.WOStripPartsComment16Font);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects Font Size as 16");
			
			ActionHandler.wait(1);
			ActionHandler.click(SVOCommentFieldAdditionContainer.WOStripPartsCommentFontColor);
			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(1);
			ActionHandler.clearText(SVOCommentFieldAdditionContainer.WOStripPartsCommentFontColorHexa);
			ActionHandler.wait(1);
			ActionHandler.setText(SVOCommentFieldAdditionContainer.WOStripPartsCommentFontColorHexa, "#121312");
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(1);
			ActionHandler.click(SVOCommentFieldAdditionContainer.WOStripPartsCommentFontColorDoneBtn);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects Font Color");

			ActionHandler.wait(1);
			ActionHandler.click(SVOCommentFieldAdditionContainer.WOStripPartsCommentTextBold);
			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(SVOCommentFieldAdditionContainer.WOStripPartsCommentTextItalic);
			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(SVOCommentFieldAdditionContainer.WOStripPartsCommentTextUnderline);
			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects Bold, Italic and Underline");
			
			ActionHandler.wait(2);
			ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSaveOpportunity);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User saves the Work Order after entering comment for Strip, Parts/BiW Section");
		}
		else if(VerifyHandler.verifyElementPresent(SVOCommentFieldAdditionContainer.WOStripPartsCommentBoxWithFormat))
		{
			ActionHandler.wait(1);
			ActionHandler.clearText(SVOCommentFieldAdditionContainer.WOStripPartsCommentBoxWithFormat);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User removes the text present inside Comment Box");
			
			ActionHandler.wait(1);
			ActionHandler.click(SVOCommentFieldAdditionContainer.WOStripPartsRemoveFormatting);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			
			ActionHandler.wait(1);
			//javaScriptUtil.clickElementByJS(SVOCommentFieldAdditionContainer.WOStripPartsCommentBoxWithFormat);
			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(1);
			ActionHandler.setText(SVOCommentFieldAdditionContainer.WOStripPartsCommentBoxWithFormatLine, "Enter Comment Text with formats");
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User enters Comment Text with formatting");
			
			Actions act = new Actions(driver);
			act.doubleClick(SVOCommentFieldAdditionContainer.WOStripPartsCommentBoxWithFormatLine);
			act.build().perform();
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			
			ActionHandler.wait(1);
			ActionHandler.click(SVOCommentFieldAdditionContainer.WOStripPartsCommentFontStyle);
			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(1);
			ActionHandler.click(SVOCommentFieldAdditionContainer.WOStripPartsCommentArialFont);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects Font Style as an Arial");
			
			ActionHandler.wait(1);
			ActionHandler.click(SVOCommentFieldAdditionContainer.WOStripPartsCommentFontSize);
			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(1);
			ActionHandler.click(SVOCommentFieldAdditionContainer.WOStripPartsComment16Font);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects Font Size as 16");
			
			ActionHandler.wait(1);
			ActionHandler.click(SVOCommentFieldAdditionContainer.WOStripPartsCommentFontColor);
			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(1);
			ActionHandler.clearText(SVOCommentFieldAdditionContainer.WOStripPartsCommentFontColorHexa);
			ActionHandler.wait(1);
			ActionHandler.setText(SVOCommentFieldAdditionContainer.WOStripPartsCommentFontColorHexa, "#121312");
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(1);
			ActionHandler.click(SVOCommentFieldAdditionContainer.WOStripPartsCommentFontColorDoneBtn);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects Font Color");

			ActionHandler.wait(1);
			ActionHandler.click(SVOCommentFieldAdditionContainer.WOStripPartsCommentTextBold);
			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(SVOCommentFieldAdditionContainer.WOStripPartsCommentTextItalic);
			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(SVOCommentFieldAdditionContainer.WOStripPartsCommentTextUnderline);
			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects Bold, Italic and Underline");
			
			ActionHandler.wait(2);
			ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSaveOpportunity);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User saves the Work Order after entering comment for Strip, Parts/BiW Section");
		}
		
		
	}
	
	@And("^User verifies that the comments are added with formatting under any section$")
	public void User_verifies_the_comments_added_with_formatting() throws Exception
	{
		User_verifies_Comments_fields_present_on_existing_work_order();
		Reporter.addStepLog("User verifies that Comments are added for Strip, Parts/BiW Section");
	}
	
	@Then("^Verify that user is not able to attach link into comments field under any section of an opened work order$")
	public void Verify_that_user_not_attach_link_comments_field_under_any_section_work_order() throws Exception
	{
		User_verifies_Comments_fields_present_on_existing_work_order();
		Reporter.addStepLog("User verified that link cannot be attached in any comments field of an work order");
	}
	@Then("^Verify that user is not able to edit comments field under any section of an opened work order$")
	public void Verify_that_user_is_not_able_to_edit_comments_field_under_any_section_of_an_opened_work_order() throws Exception
	{
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.wait(1);
		
		if(!VerifyHandler.verifyElementPresent(SVOCommentFieldAdditionContainer.WOBuildStartCommentEditBtn)&&!VerifyHandler.verifyElementPresent(SVOCommentFieldAdditionContainer.WOStripAndProcessCommentEditBtn)){
			   CommonFunctions.attachScreenshot();
			   Reporter.addStepLog("user is not able to view edit option for Build start comments");
			  
			   ActionHandler.scrollDown();
			   ActionHandler.wait(1);
			   CommonFunctions.attachScreenshot();
			   Reporter.addStepLog("user is not able to view edit option for Strip and process parts / body in white comments");
			   
			   ActionHandler.scrollDown();
			   ActionHandler.wait(1);
			   CommonFunctions.attachScreenshot();
			   Reporter.addStepLog("user is not able to view edit option forChassis/PreBuild/Build - Comments");
		}	

	}
	@Then("^User cancels the comments window after adding Comment under any section of an opened Work Order$")
	public void User_cancels_the_comments_window_after_adding_Comment_under_any_section_of_an_opened_Work_Order() throws Exception
	{
		ActionHandler.wait(1);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		
		ActionHandler.wait(1);
		javaScriptUtil.clickElementByJS(SVOCommentFieldAdditionContainer.WOBuildStartCommentEditBtn);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Edit button in order to add Comments");
		
		ActionHandler.wait(1);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		if(VerifyHandler.verifyElementPresent(SVOCommentFieldAdditionContainer.NewWOBuildCommentBox)) {
		javaScriptUtil.clickElementByJS(SVOCommentFieldAdditionContainer.NewWOBuildCommentBox);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOCommentFieldAdditionContainer.NewWOBuildCommentBox, "Test Cancel Comments");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User adds one Comment under Chassis, Pre-Build and Build section");
		}
		else {
			javaScriptUtil.clickElementByJS(SVOCommentFieldAdditionContainer.ExistingWOBuildCommentBox);
			ActionHandler.wait(1);
			ActionHandler.clearText(SVOCommentFieldAdditionContainer.ExistingWOBuildCommentBox);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			
			ActionHandler.wait(2);
			ActionHandler.setText(SVOCommentFieldAdditionContainer.NewWOBuildCommentBox, "Test Cancel Comments");
			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User adds one Comment under Chassis, Pre-Build and Build section");
		}
		
		ActionHandler.wait(1);
		ActionHandler.click(SVOCommentFieldAdditionContainer.WOChassisCancelButton);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User cancels the comment addition after adding comments for Chassis, Pre-Build and Build section");
	}
	@Then("^Verify that user is not able to view added comments under any section of an opened work order$")
	public void Verify_that_user_is_not_able_to_view_added_comments_under_any_section_of_an_opened_work_order() throws Exception
	{
		if(!VerifyHandler.verifyElementPresent(SVOCommentFieldAdditionContainer.WOChassisCancelComment)) {
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user is not able to view added comments under any section of an opened work order");
		}
	}
}
