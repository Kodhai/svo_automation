package com.jlr.svo.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.containers.SSEC_1227_CasesCreationContainer;
import com.jlr.svo.containers.SSEC_1229_Case_QueuesContainer;
import com.jlr.svo.containers.SSEC_1237_CaseClosureContainer;
import com.jlr.svo.containers.SSEC_2085_Mass_delete_Classic_PartsContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.And;

public class SSEC_2085_Mass_delete_Classic_PartsTest extends TestBaseCC {
	

	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory.getLogger(SSEC_1227_CasesCreation_Test.class.getName());
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);

	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SSEC_1227_CasesCreationContainer CasesContainer = PageFactory.initElements(driver,
			SSEC_1227_CasesCreationContainer.class);
	SSEC_1237_CaseClosureContainer CaseClosureContainer = PageFactory.initElements(driver,
			SSEC_1237_CaseClosureContainer.class);

	SSEC_1229_Case_QueuesContainer Case_QueuesContainer = PageFactory.initElements(driver, SSEC_1229_Case_QueuesContainer.class); 
	SSEC_2085_Mass_delete_Classic_PartsContainer Mass_delete_Classic_PartsContainer = PageFactory.initElements(driver, SSEC_2085_Mass_delete_Classic_PartsContainer.class);
//

	public static String verificationCode;
	public static String vCode;
	double randomNum = getRandomNumber(0, 10000);
	String firstName = "Test" + randomNum;
	String lastName = "tes" + randomNum;
	String VINNum = "Test";
	String Chassis = VINNum + randomNum;
	String Chassis2 = VINNum + randomNum;

	public static double getRandomNumber(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	// onStart
	public void onStart() {
		setupTest("SVOTest");
		driver = getDriver();
		CasesContainer = PageFactory.initElements(driver, SSEC_1227_CasesCreationContainer.class);
		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		Case_QueuesContainer = PageFactory.initElements(driver, SSEC_1229_Case_QueuesContainer.class); 
		Mass_delete_Classic_PartsContainer = PageFactory.initElements(driver, SSEC_2085_Mass_delete_Classic_PartsContainer.class);

		verificationCode = null;

	}


    @And("^Select a checkbox and click on Mass Delete button$")
    public void Select_a_checkbox_and_click_on_Mass_Delete() throws Exception{
    	
    	ActionHandler.click(Mass_delete_Classic_PartsContainer.SelectItemCheckbox);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Select Item Checkbox");
    	
    	ActionHandler.click(Mass_delete_Classic_PartsContainer.MassDeleteBtn);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on Mass Delete button");
    	
    	ActionHandler.click(Mass_delete_Classic_PartsContainer.DeleteRecordsBtn);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on Delete Records button");
    	
    }
    
    @And("^Select multiple checkbox and click on Mass Delete button$")
    public void Multiple_checkbox_and_click_on_Mass_Delete_button() throws Exception{
    	
        ActionHandler.click(Mass_delete_Classic_PartsContainer.SelectItemCheckbox);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Select Item Checkbox");
    	
        ActionHandler.click(Mass_delete_Classic_PartsContainer.SelectItemCheckbox2);
        ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Select Item Checkbox 2");
    	
    	ActionHandler.click(Mass_delete_Classic_PartsContainer.MassDeleteBtn);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on Mass Delete button");
    	
    	ActionHandler.click(Mass_delete_Classic_PartsContainer.DeleteRecordsBtn);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on Delete Record button");
    	
    }


}
