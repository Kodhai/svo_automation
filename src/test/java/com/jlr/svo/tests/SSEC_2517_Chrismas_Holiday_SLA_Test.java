package com.jlr.svo.tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.constants.Constants;
import com.jlr.svo.containers.SSEC_2489_ClassicSalesforcePreferredContactSelectionsContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.containers.SVOOpportunityContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class SSEC_2517_Chrismas_Holiday_SLA_Test extends TestBaseCC {
	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory
			.getLogger(SSEC_1228_LinkContactAndAccountTocase_Test.class.getName());
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
	SSEC_2489_ClassicSalesforcePreferredContactSelectionsContainer ClassicSalesforcePreferredContactSelectionsContainer = PageFactory.initElements(driver,  SSEC_2489_ClassicSalesforcePreferredContactSelectionsContainer.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
	
	public static String verificationCode;
	public static String vCode;
	public static String veriCode;
	public String NewWindow;
	
	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	public void onStart() {

		setupTest("SVOTest");
		driver = getDriver();

		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
		
		driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);

		verificationCode = null;
		vCode = null;
		veriCode = null;
		NewWindow = null;
		
	}

	@Given("^Access to Outlook account with Username \"([^\"]*)\" and password \"([^\"]*)\"$")
    public void access_to_outlook_account_with_Username_and_password(String UserName, String Password) throws Throwable {
		
        String s[] = UserName.split(",");
        UserName = com.jlr.svo.utilities.CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

        String p[] = Password.split(",");
        Password = CommonFunctions.readExcelMasterData(p[0], p[1], p[2]);

        onStart();
        driver = getDriver();
        ActionHandler.wait(5);
        driver.get(Constants.outlookInbox);

        ActionHandler.setText(SVOEnquiryContainer.outlookUsername, UserName);
        CommonFunctions.attachScreenshot();

        ActionHandler.click(SVOEnquiryContainer.outlookNext);
        CommonFunctions.attachScreenshot();

        ActionHandler.setText(SVOEnquiryContainer.outlookPassword, Password);
        CommonFunctions.attachScreenshot();

 

        ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
        CommonFunctions.attachScreenshot();

 

        ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
        CommonFunctions.attachScreenshot();

    }
	
	@Then("^Customer sends an email to create an enquiry$")
    public void Customer_sends_an_email_to_create_an_enquiry() throws Throwable {

        ActionHandler.wait(5);

        ActionHandler.click(SVO_OpportunityContainer.NewEmail);
        CommonFunctions.attachScreenshot();

        ActionHandler.click(SVO_OpportunityContainer.Tomail);
        ActionHandler.setText(SVO_OpportunityContainer.Tomail,"sales-uat@365pace.com");

        ActionHandler.pressEnter();

 

        CommonFunctions.attachScreenshot();

        ActionHandler.wait(5);
        ActionHandler.click(SVO_OpportunityContainer.SubjectEmail);
        CommonFunctions.attachScreenshot();

        ActionHandler.setText(SVO_OpportunityContainer.SubjectEmail, "Create an enquiry");

        ActionHandler.click(SVO_OpportunityContainer.SendEmail);
    }
	
	@Then("^Customer sends an email to create a bespoke enquiry$")
    public void Customer_sends_an_email_to_create_a_bespoke_enquiry() throws Throwable {

        ActionHandler.wait(5);

        ActionHandler.click(SVO_OpportunityContainer.NewEmail);
        CommonFunctions.attachScreenshot();

        ActionHandler.click(SVO_OpportunityContainer.Tomail);
        ActionHandler.setText(SVO_OpportunityContainer.Tomail,"bespoke-uat@365pace.com");

        ActionHandler.pressEnter();

 

        CommonFunctions.attachScreenshot();

        ActionHandler.wait(5);
        ActionHandler.click(SVO_OpportunityContainer.SubjectEmail);
        CommonFunctions.attachScreenshot();

        ActionHandler.setText(SVO_OpportunityContainer.SubjectEmail, "Create an enquiry");

        ActionHandler.click(SVO_OpportunityContainer.SendEmail);
    }
	
	@Then("^User verify that SLA has paused for created enquiry$")
	public void User_verify_that_SLA_has_Paused_for_created_enquiry() throws Exception {
	    ActionHandler.wait(3);
	    ActionHandler.click(SVOEnquiryContainer.RecentlyViewed);
	    ActionHandler.wait(3);
	    CommonFunctions.attachScreenshot();
	    
	    ActionHandler.click(SVOEnquiryContainer.AllEnquiries);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOEnquiryContainer.firstEnquiryAllList);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.pageCompleteScrollDown();
		Reporter.addStepLog("User verify that SLA has paused for created enquiry");
	}

	@Then("^User verify the start and end date of created new enquiry during Christmas holiday$")
    public void User_startandenddate_of_Christmas_Holiday() throws Exception {
        ActionHandler.wait(3);
        ActionHandler.click(SVOEnquiryContainer.RecentlyViewed);
        ActionHandler.wait(3);
        CommonFunctions.attachScreenshot();
        ActionHandler.click(SVOEnquiryContainer.AllEnquiries);
        ActionHandler.wait(3);
        CommonFunctions.attachScreenshot();
        ActionHandler.click(SVOEnquiryContainer.firstEnquiryAllList);
        ActionHandler.wait(3);
        CommonFunctions.attachScreenshot();
        ActionHandler.pageCompleteScrollDown();
        Reporter.addStepLog("User verify the start and end date of created new enquiry during Christmas holiday");
    }
	
	@Then("^User verify the thank you message after enquiry got created$")
    public void User_verify_the_thank_you() throws Exception {
        ActionHandler.wait(3);
        CommonFunctions.attachScreenshot();
        ActionHandler.click(SVOEnquiryContainer.ClassicSalesEnquiry);
        ActionHandler.wait(3);
        CommonFunctions.attachScreenshot();
        VerifyHandler.verifyElementPresent(SVOEnquiryContainer.ThankYouenquirymsg);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Thank you enquiry message is present");

        driver.close();
    }
	
	 @Then("^User not able to verify the thank you message after enquiry got created$")
	    public void User_not_able_to_verify_the_thank_you() throws Exception {
	        ActionHandler.wait(3);
	        CommonFunctions.attachScreenshot();
	        ActionHandler.click(SVOEnquiryContainer.ClassicSalesEnquiry);
	        ActionHandler.wait(3);
	        CommonFunctions.attachScreenshot();
	        if(!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.ThankYouenquirymsg)) {

	        CommonFunctions.attachScreenshot();
	        Reporter.addStepLog("Thank you enquiry message is present");
	        }

	        driver.close();
	    }


	

}
