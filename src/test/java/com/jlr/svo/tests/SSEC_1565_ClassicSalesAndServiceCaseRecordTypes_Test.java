package com.jlr.svo.tests;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.autotest.utils.Config;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.constants.Constants;
import com.jlr.svo.containers.SSEC_1226_CaseCreationEmail_Container;
import com.jlr.svo.containers.SSEC_1227_CasesCreationContainer;
import com.jlr.svo.containers.SSEC_1565_ClassicSalesAndServiceCaseRecordTypes_Container;
import com.jlr.svo.containers.SSEC_1566_ClassicSalesAndClassicService_CaseObject_fields_Container;
import com.jlr.svo.containers.SVOAdditionalVehicleContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.containers.SVOOpportunityContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SSEC_1565_ClassicSalesAndServiceCaseRecordTypes_Test extends TestBaseCC {
	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory
			.getLogger(SSEC_1565_ClassicSalesAndServiceCaseRecordTypes_Test.class.getName());
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);

	SSEC_1226_CaseCreationEmail_Container CaseCreationEmailContainer = PageFactory.initElements(driver,
			SSEC_1226_CaseCreationEmail_Container.class);
	SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	SVOAdditionalVehicleContainer SVOAdditionalvehicleContainer = PageFactory.initElements(driver,
			SVOAdditionalVehicleContainer.class);
	SSEC_1227_CasesCreationContainer CasesContainer = PageFactory.initElements(driver,
			SSEC_1227_CasesCreationContainer.class);
	SSEC_1565_ClassicSalesAndServiceCaseRecordTypes_Container ClassicSalesAndServiceCaseRecordTypesContainer = PageFactory
			.initElements(driver, SSEC_1565_ClassicSalesAndServiceCaseRecordTypes_Container.class);
	SSEC_1566_ClassicSalesAndClassicService_CaseObject_fields_Container CaseObjectfieldsContainer = PageFactory
			.initElements(driver, SSEC_1566_ClassicSalesAndClassicService_CaseObject_fields_Container.class);

	public static String verificationCode;
	public static String vCode;
	public static String veriCode;
	public static String CaseNumber;
	public String NewWindow;

	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	public void onStart() {

		setupTest("SVOTest");
		driver = getDriver();

		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
		SVOAdditionalvehicleContainer = PageFactory.initElements(driver, SVOAdditionalVehicleContainer.class);
		CasesContainer = PageFactory.initElements(driver, SSEC_1227_CasesCreationContainer.class);
		ClassicSalesAndServiceCaseRecordTypesContainer = PageFactory.initElements(driver,
				SSEC_1565_ClassicSalesAndServiceCaseRecordTypes_Container.class);
		CaseCreationEmailContainer = PageFactory.initElements(driver, SSEC_1226_CaseCreationEmail_Container.class);
		CaseObjectfieldsContainer = PageFactory.initElements(driver,
				SSEC_1566_ClassicSalesAndClassicService_CaseObject_fields_Container.class);

		driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);

		verificationCode = null;
		vCode = null;
		veriCode = null;
		NewWindow = null;
		CaseNumber = null;

	}

	@Given("^Access to SVO Portal as Classic Sales user with Username \"([^\"]*)\" and password \"([^\"]*)\"$")
	public void access_to_SVO_Portal_as_classic_sales_user_with_Username_and_Password(String userName, String password)
			throws Exception {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		onStart();
		driver = getDriver();
		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailForClassicSalesUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			Reporter.addStepLog("User logins to SVO Successfully");

		}
	}

	public String checkEmailForClassicSalesUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(1);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(513, 520);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	@Given("^Access to SF SVO Portal as Classic Service user with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void access_to_SF_SVO_portal_as_classic_service_user_with_Username_and_Password(String userName,
			String password) throws Throwable {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailForclassicserviceUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	public String checkEmailForclassicserviceUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(342, 348);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;

	}

	@Then("^User Logouts from SVO Portal$")
	public void User_Logouts_from_SVO_Portal() throws Throwable {

		ActionHandler.click(SVO_OpportunityContainer.icon_image);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Logout");
		ActionHandler.click(SVO_OpportunityContainer.Logout);

		CommonFunctions.attachScreenshot();
		if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.userNameTextBox)) {
			MarkupHelper.createLabel("Logged out successfully", ExtentColor.GREEN);
		} else {
			MarkupHelper.createLabel("Log out failed", ExtentColor.RED);
		}

		driver.close();
		Reporter.addStepLog("Driver closed");
	}

	@Given("^Access to SF SVO bespoke Portal with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void access_to_sf_SVO_bespoke_Portal_with_Username_and_Password(String userName, String password)
			throws Throwable {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");
		onStart();
		driver = getDriver();
		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");
			vCode = checkEmail();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");
			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	@Given("^User Navigate to cases tab$")
	public void user_Navigate_to_cases_tab() throws Throwable {
		ActionHandler.wait(1);
		javaScriptUtil.clickElementByJS(CaseCreationEmailContainer.CasesTab);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigates to Cases tab");
	}

	@Then("^User creates classic sales case with details like Record Type \"([^\"]*)\" Product Offering \"([^\"]*)\" Rating \"([^\"]*)\" and Case Origin \"([^\"]*)\"$")
	public void user_creates_classic_sales_case_with_details_like_Record_Type_Product_Offering_Rating_and_Case_Origin(
			String RecordType, String ProductOffering, String Rating, String CaseOrigin) throws Throwable {

		String RT[] = RecordType.split(",");
		RecordType = CommonFunctions.readExcelMasterData(RT[0], RT[1], RT[2]);

		String PO[] = ProductOffering.split(",");
		ProductOffering = CommonFunctions.readExcelMasterData(PO[0], PO[1], PO[2]);

		String R[] = Rating.split(",");
		Rating = CommonFunctions.readExcelMasterData(R[0], R[1], R[2]);

		String CO[] = CaseOrigin.split(",");
		CaseOrigin = CommonFunctions.readExcelMasterData(CO[0], CO[1], CO[2]);

		ActionHandler.click(CaseObjectfieldsContainer.NewEnquiry);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on new button to create a Case");

		ActionHandler
				.click(driver.findElement(By.xpath(CaseObjectfieldsContainer.CaseRecordTypeSelection(RecordType))));
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Selects Case Record Type as " + RecordType);

		ActionHandler.click(CaseObjectfieldsContainer.NextEnquiry);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Next button to create " + RecordType + " Case");

		ActionHandler.click(CaseObjectfieldsContainer.ProdOfferingCase);
		ActionHandler.wait(1);
		ActionHandler.click(driver.findElement(By.xpath(CaseObjectfieldsContainer.ValueSelection(ProductOffering))));
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Product Offering " + ProductOffering + " for case creation");

		ActionHandler.click(CaseObjectfieldsContainer.Rating);
		ActionHandler.wait(1);
		ActionHandler.click(driver.findElement(By.xpath(CaseObjectfieldsContainer.ValueSelection(Rating))));
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Rating " + Rating + " for case creation");

		ActionHandler.click(CaseObjectfieldsContainer.CaseOrigin);
		ActionHandler.wait(1);
		ActionHandler.click(driver.findElement(By.xpath(CaseObjectfieldsContainer.ValueSelection(CaseOrigin))));
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects " + CaseOrigin + " for Case creation");

		ActionHandler.click(CaseObjectfieldsContainer.SaveCase);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves the " + RecordType + " case");
	}

	@When("^Verify the case origin of newly created case$")
	public void verify_the_case_origin_of_newly_created_case() throws Throwable {

		ActionHandler.wait(2);
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.wait(1);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyCaseOrigin);
		String CaseOrigin = CaseObjectfieldsContainer.VerifyCaseOrigin.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Case Origin field with value displayed as " + CaseOrigin);

	}

	@When("^Verify the case record type of newly created case$")
	public void verify_the_case_record_type_of_newly_created_case() throws Throwable {

		ActionHandler.wait(2);
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.wait(1);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyRecord);
		String RecordType = CaseObjectfieldsContainer.VerifyRecord.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Case Record Type field with value displayed as " + RecordType);

	}

	@Then("^User creates classic Service case with details like Record Type \"([^\"]*)\" Product Offering \"([^\"]*)\" Rating \"([^\"]*)\" and Case Origin \"([^\"]*)\"$")
	public void user_creates_classic_Service_case_with_details_like_Record_Type_Product_Offering_Rating_and_Case_Origin(
			String RecordType, String ProductOffering, String Rating, String CaseOrigin) throws Throwable {

		String RT[] = RecordType.split(",");
		RecordType = CommonFunctions.readExcelMasterData(RT[0], RT[1], RT[2]);

		String PO[] = ProductOffering.split(",");
		ProductOffering = CommonFunctions.readExcelMasterData(PO[0], PO[1], PO[2]);

		String R[] = Rating.split(",");
		Rating = CommonFunctions.readExcelMasterData(R[0], R[1], R[2]);

		String CO[] = CaseOrigin.split(",");
		CaseOrigin = CommonFunctions.readExcelMasterData(CO[0], CO[1], CO[2]);

		ActionHandler.click(CaseObjectfieldsContainer.NewEnquiry);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on new button to create a Case");

		ActionHandler
				.click(driver.findElement(By.xpath(CaseObjectfieldsContainer.CaseRecordTypeSelection(RecordType))));
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Selects Case Record Type as " + RecordType);

		ActionHandler.click(CaseObjectfieldsContainer.NextEnquiry);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Next button to create " + RecordType + " Case");

		ActionHandler.click(CaseObjectfieldsContainer.ProdOfferingCase);
		ActionHandler.wait(1);
		ActionHandler.click(driver.findElement(By.xpath(CaseObjectfieldsContainer.ValueSelection(ProductOffering))));
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Product Offering " + ProductOffering + " for case creation");

		ActionHandler.click(CaseObjectfieldsContainer.Rating);
		ActionHandler.wait(1);
		ActionHandler.click(driver.findElement(By.xpath(CaseObjectfieldsContainer.ValueSelection(Rating))));
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Rating " + Rating + " for case creation");

		ActionHandler.click(CaseObjectfieldsContainer.CaseOrigin);
		ActionHandler.wait(1);
		ActionHandler.click(driver.findElement(By.xpath(CaseObjectfieldsContainer.ValueSelection(CaseOrigin))));
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects " + CaseOrigin + " for Case creation");

		ActionHandler.click(CaseObjectfieldsContainer.SaveCase);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves the " + RecordType + " case");
	}

	@When("^Verify the status of newly created case on case details page$")
	public void verify_the_status_of_newly_created_case_on_case_details_page() throws Throwable {

		ActionHandler.wait(2);
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.wait(1);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyStatus);
		String CaseStatus = CaseObjectfieldsContainer.VerifyStatus.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Case Status field with value displayed as " + CaseStatus);

	}

	@Then("^User Navigate to Activity tab and logs a call from Activity tab$")
	public void User_Navigate_to_Activity_tab_and_logs_a_call_from_Activity_tab() throws Throwable {

		String Subject = "Call";

		ActionHandler.scrollDown();
		ActionHandler.wait(1);

		ActionHandler.click(CaseObjectfieldsContainer.Activity);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to activity section");

		ActionHandler.wait(1);
		ActionHandler.scrollDown();
		ActionHandler.wait(1);

		ActionHandler.click(CaseObjectfieldsContainer.Add);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("clicks on add");

		ActionHandler.setText(CaseObjectfieldsContainer.subject, Subject);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enter subject as " + Subject);

		ActionHandler.wait(1);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);

		ActionHandler.click(CaseObjectfieldsContainer.saveActivity);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("saves the activity");

	}

	@Then("^User Moves the newly created Case status to Closed$")
	public void User_Moves_the_newly_created_Case_Status_to_Closed() throws Throwable {

		ActionHandler.wait(1);
		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(1);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Verifies that the case status moved to Contacted");

		ActionHandler.click(CaseObjectfieldsContainer.MarkStatusCase);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Move the case status to Discovery");

		ActionHandler.click(CaseObjectfieldsContainer.MarkStatusCase);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Move the case status to Closed status");
	}

	@Then("^Verify the status of case on case details page$")
	public void verify_the_status_of_case_on_case_details_page() throws Throwable {

		ActionHandler.wait(2);
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.wait(1);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyStatus);
		String CaseStatus = CaseObjectfieldsContainer.VerifyStatus.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Case Status field with value displayed as " + CaseStatus);

	}

	@Then("^Verify the case status of cloned case on case details page$")
	public void verify_the_case_status_of_cloned_case_on_case_details_page() throws Throwable {

		ActionHandler.wait(2);
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.wait(1);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyStatus);
		String CaseStatus = CaseObjectfieldsContainer.VerifyStatus.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Case Status field with value displayed as " + CaseStatus);

	}

	@Then("^Move newly created case to Contacted status$")
	public void move_newly_created_case_to_Contacted_status() throws Throwable {
		ActionHandler.wait(1);
		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(1);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Verifies that the case status moved to Contacted");

	}

	@Given("^User logins to SVO portal as SVO Bespoke User UserName \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void User_Logins_to_SVO_portal_as_SVO_Bespoke_User_with_UserName(String userName, String password)
			throws Exception {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");
		onStart();
		driver = getDriver();

		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");
			vCode = checkEmail();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");
			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verifyBtn)) {
				ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			}
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	public String checkEmail() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.outlookSignin)) {

			ActionHandler.click(SVO_OpportunityContainer.outlookSignin);
			CommonFunctions.attachScreenshot();
			ActionHandler.setText(SVO_OpportunityContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVO_OpportunityContainer.outlookNext);
			CommonFunctions.attachScreenshot();
			ActionHandler.setText(SVO_OpportunityContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVO_OpportunityContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(507, 520);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	@Then("^User Verify that there is no Cases Tab available$")
	public void User_Verify_that_there_is_no_cases_tab() throws Throwable {

		if (!VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.CasesTab)) {
			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User verifies that there is no Cases Tab available");
		}
	}

	@When("^Verify the case record type of closed case on case details page$")
	public void verify_the_case_record_type_of_closed_case_on_case_details_page() throws Throwable {

		ActionHandler.wait(2);
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.wait(1);

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyRecord);
		String RecordType = CaseObjectfieldsContainer.VerifyRecord.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Case Record Type field with value displayed as " + RecordType);

	}

	@Then("^Change the record type of closed classic sales case$")
	public void change_the_record_type_of_closed_classic_sales_case() throws Throwable {

		ActionHandler.click(ClassicSalesAndServiceCaseRecordTypesContainer.ChangeRecordTypeButton);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on change record type button");

		ActionHandler.click(ClassicSalesAndServiceCaseRecordTypesContainer.ChangeRecordTypeNextButton);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on change record type button");

		ActionHandler.click(ClassicSalesAndServiceCaseRecordTypesContainer.ChangeRecordTypeSaveButton);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Save button after changing Record type of selected case");

	}

	@Then("^Verify that user is able to delete the Classic service case which is in Contacted status$")
	public void verify_that_user_is_able_to_delete_the_Classic_service_case_which_is_in_Contacted_status()
			throws Throwable {

		VerifyHandler.verifyElementPresent(ClassicSalesAndServiceCaseRecordTypesContainer.CaseNumberField);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		CaseNumber = ClassicSalesAndServiceCaseRecordTypesContainer.CaseNumberField.getText();
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Case Number : " + CaseNumber);

		user_Navigate_to_cases_tab();

		ActionHandler.click(ClassicSalesAndServiceCaseRecordTypesContainer.SelectOpportunityListView);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on select list view tab");

		ActionHandler.click(ClassicSalesAndServiceCaseRecordTypesContainer.AllCasesListView);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses to view All cases list view");

		ActionHandler.click(ClassicSalesAndServiceCaseRecordTypesContainer.CasesSearchTextBox);
		ActionHandler.wait(1);
		ActionHandler.setText(ClassicSalesAndServiceCaseRecordTypesContainer.CasesSearchTextBox, CaseNumber);
		ActionHandler.wait(1);
		ActionHandler.pressEnter();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user searches for the case that is moved to contacted status");

		VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.FirstOpportunityLink);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(ClassicSalesAndServiceCaseRecordTypesContainer.CaseMoreActionsButton);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on more actions drop down corresponding to case");

		ActionHandler.click(ClassicSalesAndServiceCaseRecordTypesContainer.CaseDeleteButton);
		ActionHandler.wait(1);
		ActionHandler.click(ClassicSalesAndServiceCaseRecordTypesContainer.CaseDeleteConfirmButton);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on delete case button to delete contacted classic service case");

		VerifyHandler.verifyElementPresent(ClassicSalesAndServiceCaseRecordTypesContainer.DeleteConfirmMessage);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User deletes the contacted classic service case from salesforce");

	}

	@Given("^verify that no cases tab is available for case creation$")
	public void verify_that_no_cases_tab_is_available_for_case_creation() throws Throwable {

		if (!VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.CasesTab)) {
			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User verifies that there is no Cases Tab available for case creation");
		}
	}

	@Then("^Change the owner of case to \"([^\"]*)\"$")
	public void change_the_owner_of_case_to(String Owner) throws Throwable {

		String owner[] = Owner.split(",");
		Owner = CommonFunctions.readExcelMasterData(owner[0], owner[1], owner[2]);

		ActionHandler.click(ClassicSalesAndServiceCaseRecordTypesContainer.ChangeOwnerButton);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on change owner button");

		ActionHandler.click(ClassicSalesAndServiceCaseRecordTypesContainer.SearchUserTxtBx);
		ActionHandler.wait(1);
		ActionHandler.setText(ClassicSalesAndServiceCaseRecordTypesContainer.SearchUserTxtBx, Owner);
		ActionHandler.wait(1);
		ActionHandler.click(ClassicSalesAndServiceCaseRecordTypesContainer.UserSearchBar);
		ActionHandler.wait(1);
		ActionHandler.click(
				driver.findElement(By.xpath(ClassicSalesAndServiceCaseRecordTypesContainer.OwnerSelection(Owner))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"User searches and selects " + Owner + " user for updating case owner to " + Owner + "User");

		ActionHandler.click(ClassicSalesAndServiceCaseRecordTypesContainer.ChangeOwnerSubmitButton);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on change owner submit button");

		VerifyHandler.verifyElementPresent(CaseObjectfieldsContainer.VerifyOwner);
		String CaseOwner = CaseObjectfieldsContainer.VerifyOwner.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Case Owner is updates to " + CaseOwner);
	}

	@Then("^Verify that user is able to move closed classic service case to New status$")
	public void verify_that_user_is_able_to_move_closed_classic_service_case_to_New_status() throws Throwable {

	}

	@Then("^Verify that user is not able to Change the owner of case to \"([^\"]*)\"$")
	public void verify_that_user_is_not_able_to_Change_the_owner_of_case_to(String Owner) throws Throwable {
		String owner[] = Owner.split(",");
		Owner = CommonFunctions.readExcelMasterData(owner[0], owner[1], owner[2]);

		ActionHandler.click(ClassicSalesAndServiceCaseRecordTypesContainer.ChangeOwnerButton);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on change owner button");

		ActionHandler.click(ClassicSalesAndServiceCaseRecordTypesContainer.SearchUserTxtBx);
		ActionHandler.wait(1);
		ActionHandler.setText(ClassicSalesAndServiceCaseRecordTypesContainer.SearchUserTxtBx, Owner);
		ActionHandler.wait(1);
		ActionHandler.click(ClassicSalesAndServiceCaseRecordTypesContainer.UserSearchBar);
		ActionHandler.wait(1);
		ActionHandler.click(
				driver.findElement(By.xpath(ClassicSalesAndServiceCaseRecordTypesContainer.OwnerSelection(Owner))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"User searches and selects " + Owner + " user for updating case owner to " + Owner + "User");

		ActionHandler.click(ClassicSalesAndServiceCaseRecordTypesContainer.ChangeOwnerSubmitButton);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on change owner submit button");

		VerifyHandler.verifyElementPresent(ClassicSalesAndServiceCaseRecordTypesContainer.ChangeOwnerErrorMessage);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies an error message displayed to change the owner of case to " + Owner);

		ActionHandler.click(ClassicSalesAndServiceCaseRecordTypesContainer.ChangeOwnerCancelButton);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on change owner cancel button to close pop up window");

	}

	@And("^User reopen the closed classic service case$")
	public void User_reopen_the_closed_classic_service_case() throws Throwable {

		ActionHandler.wait(1);
		ActionHandler.scrollDown();
		ActionHandler.wait(1);

		javaScriptUtil.clickElementByJS(ClassicSalesAndServiceCaseRecordTypesContainer.EditStatus);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Edit status pencil icon");

		ActionHandler.click(ClassicSalesAndServiceCaseRecordTypesContainer.SelectStatus);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(ClassicSalesAndServiceCaseRecordTypesContainer.StatusNew);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects New as Status");

		ActionHandler.click(ClassicSalesAndServiceCaseRecordTypesContainer.SaveEdit);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks on Save button");

		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(1);

		VerifyHandler.verifyElementPresent(ClassicSalesAndServiceCaseRecordTypesContainer.CaseStatus);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verify the Case status as New");

	}

}
