package com.jlr.svo.tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.containers.SSEC_1227_CasesCreationContainer;
import com.jlr.svo.containers.SSEC_1237_CaseClosureContainer;
import com.jlr.svo.containers.SSEC_1238_CaseLifecycle_Container;
import com.jlr.svo.containers.SSEC_2294_ClassicPartsStatusUpdatefor_New_Customer_Container;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class SSEC_2294_ClassicPartsStatusUpdatefor_New_Customer_Response_EndToEndTesting_Test extends TestBaseCC {

	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory.getLogger(SVOWorkOrderProgrammeField_Test.class.getName());
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	com.jlr.svo.containers.SSEC_2294_ClassicPartsStatusUpdatefor_New_Customer_Container SSEC_2294_ClassicPartsStatusUpdatefor_New_Customer_Response_EndToEndTesting_Container = PageFactory
			.initElements(driver, SSEC_2294_ClassicPartsStatusUpdatefor_New_Customer_Container.class);
	SSEC_1238_CaseLifecycle_Container CaseLifecycle_Container = PageFactory.initElements(driver,
			SSEC_1238_CaseLifecycle_Container.class);
	SSEC_1227_CasesCreationContainer CasesContainer = PageFactory.initElements(driver,
			SSEC_1227_CasesCreationContainer.class);
	SSEC_1237_CaseClosureContainer CaseClosureContainer = PageFactory.initElements(driver,
			SSEC_1237_CaseClosureContainer.class);

	static double randomNum = getRandomIntegerBetweenRange(0, 10000);
	public static String verificationCode;
	public static String vCode;

	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	public void onStart() {

		setupTest("SVOTest");
		driver = getDriver();

		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		SSEC_2294_ClassicPartsStatusUpdatefor_New_Customer_Response_EndToEndTesting_Container = PageFactory
				.initElements(driver, SSEC_2294_ClassicPartsStatusUpdatefor_New_Customer_Container.class);

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

	}

	@Then("^User Verify the status as New$")
	public void User_Verify_the_status_as_New() throws Throwable {
		ActionHandler.wait(5);
		ActionHandler.scrollToView(SSEC_2294_ClassicPartsStatusUpdatefor_New_Customer_Container.Status);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		VerifyHandler.verifyElementPresent(SSEC_2294_ClassicPartsStatusUpdatefor_New_Customer_Container.New);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
	}

	@Then("^User Verify the status as Open$")
	public void User_Verify_the_status_as_Open() throws Throwable {

		String Status = "New";

		ActionHandler.wait(5);
		ActionHandler.scrollToView(SSEC_2294_ClassicPartsStatusUpdatefor_New_Customer_Container.Status);
		ActionHandler.wait(3);
		ActionHandler.scrollDown();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		javaScriptUtil.clickElementByJS(SSEC_2294_ClassicPartsStatusUpdatefor_New_Customer_Container.Editstatus);
		ActionHandler.wait(2);
		ActionHandler.click(SSEC_2294_ClassicPartsStatusUpdatefor_New_Customer_Container.Statusdropdown);
		ActionHandler.wait(2);
		ActionHandler.click(driver.findElement(
				By.xpath(SSEC_2294_ClassicPartsStatusUpdatefor_New_Customer_Container.RecordType(Status))));
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SSEC_2294_ClassicPartsStatusUpdatefor_New_Customer_Container.Save);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		VerifyHandler.verifyElementPresent(SSEC_2294_ClassicPartsStatusUpdatefor_New_Customer_Container.Open);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

	}

	@Then("^User Verify the status as closed$")
	public void User_Verify_the_status_as_closed() throws Throwable {
	}

	@And("^User creates new Customer service record type with Case Origin as Email$")
	public void User_created_new_Customer_service_with_Case_Origin_Email() throws Exception {

		String FN = "test" + randomNum;

		ActionHandler.click(CasesContainer.Newbtn);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on new button");

		ActionHandler.click(SSEC_2294_ClassicPartsStatusUpdatefor_New_Customer_Container.Customerservice);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on customer service record type");

		ActionHandler.click(CasesContainer.NextBtn);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Next button");

		ActionHandler.click(CasesContainer.FirstNameBtn);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on First Name text");

		ActionHandler.setText(CasesContainer.FirstNameBtn, FN);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters First Name");

//		Select CaseOriginDD = new Select(CaseClosureContainer.CaseOrigin);
//		CaseOriginDD.selectByVisibleText("Email");
//
//		ActionHandler.scrollToView(CasesContainer.VINText);
//		ActionHandler.wait(2);
//		CommonFunctions.attachScreenshot();
//		Reporter.addStepLog("User scrolls to view VIN header");
//
//		ActionHandler.setText(CasesContainer.VINText, Chassis);
//		ActionHandler.wait(2);
//		CommonFunctions.attachScreenshot();
//		Reporter.addStepLog("User sets the VIN");
	}

	@And("^User creates new Customer service record type with Case Origin as Web$")
	public void User_created_new_Customer_service_with_Case_Origin_Web() throws Exception {

		String FN = "test" + randomNum;

		ActionHandler.click(CasesContainer.Newbtn);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on new button");

		ActionHandler.click(SSEC_2294_ClassicPartsStatusUpdatefor_New_Customer_Container.Customerservice);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on customer service record type");

		ActionHandler.click(CasesContainer.NextBtn);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Next button");

		ActionHandler.click(CasesContainer.FirstNameBtn);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on First Name text");

		ActionHandler.setText(CasesContainer.FirstNameBtn, FN);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters First Name");

		ActionHandler.click(SSEC_2294_ClassicPartsStatusUpdatefor_New_Customer_Container.WebEmail);
		ActionHandler.wait(2);
		ActionHandler.setText(SSEC_2294_ClassicPartsStatusUpdatefor_New_Customer_Container.WebEmail,
				"svotest@gmail.com");
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User the web email");

//		Select CaseOriginDD = new Select(CaseClosureContainer.CaseOrigin);
//		CaseOriginDD.selectByVisibleText("Email");
//
//		ActionHandler.scrollToView(CasesContainer.VINText);
//		ActionHandler.wait(2);
//		CommonFunctions.attachScreenshot();
//		Reporter.addStepLog("User scrolls to view VIN header");
//
//		ActionHandler.setText(CasesContainer.VINText, Chassis);
//		ActionHandler.wait(2);
//		CommonFunctions.attachScreenshot();
//		Reporter.addStepLog("User sets the VIN");
	}

}