package com.jlr.svo.tests;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.containers.SSEC_1226_CaseCreationEmail_Container;
import com.jlr.svo.containers.SSEC_2119_AdditionalVehicleName_AutoNumberContainer;
import com.jlr.svo.containers.SSEC_2221_Mass_upload_ETracker_URL_AdditionalVehicleContainer;
import com.jlr.svo.containers.SSEC_2237_Continuation_Work_OrdersContainer;
import com.jlr.svo.containers.SVOAccountsContainer;
import com.jlr.svo.containers.SVOAdditionalVehicleContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.containers.SVOItemToApproveContainer;
import com.jlr.svo.containers.SVOOpportunityContainer;
import com.jlr.svo.containers.SVO_CustomerResponsesContainer;
import com.jlr.svo.containers.SVO_EnquiryLostReasonContainer;
import com.jlr.svo.containers.SVO_RestrictedPartyScreeningContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class SSEC_2237_Continuation_Work_Orders extends TestBaseCC {
	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory.getLogger(SSEC_1226_CaseCreationEmail_Test.class.getName());
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);

	SVOAccountsContainer SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
	SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	SVOAdditionalVehicleContainer SVOAdditionalvehicleContainer = PageFactory.initElements(driver,
			SVOAdditionalVehicleContainer.class);
	SVO_RestrictedPartyScreeningContainer RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
			SVO_RestrictedPartyScreeningContainer.class);
	SVOItemToApproveContainer SVOItemToapproveContainer = PageFactory.initElements(driver,
			SVOItemToApproveContainer.class);
	SVO_CustomerResponsesContainer SVO_CustomerResponsesContainer = PageFactory.initElements(driver,
			SVO_CustomerResponsesContainer.class);
	SVO_EnquiryLostReasonContainer SVO_EnquiryLostReasonContainer = PageFactory.initElements(driver,
			SVO_EnquiryLostReasonContainer.class);
	SSEC_1226_CaseCreationEmail_Container CaseCreationEmailContainer = PageFactory.initElements(driver,
			SSEC_1226_CaseCreationEmail_Container.class);
	SSEC_2119_AdditionalVehicleName_AutoNumberContainer AdditionalVehicleName_AutoNumberContainer = PageFactory.initElements(driver, SSEC_2119_AdditionalVehicleName_AutoNumberContainer.class);
	SSEC_2221_Mass_upload_ETracker_URL_AdditionalVehicleContainer Mass_upload_ETracker_URL_AdditionalVehicleContainer = PageFactory.initElements(driver, SSEC_2221_Mass_upload_ETracker_URL_AdditionalVehicleContainer.class);
	SSEC_2237_Continuation_Work_OrdersContainer Continuation_Work_OrdersContainer = PageFactory.initElements(driver, SSEC_2237_Continuation_Work_OrdersContainer.class);
	
	public static String verificationCode;
	public static String vCode;
	public static String veriCode;
	public String NewWindow;
	public static String CaseCreated;
	public static String CaseNumber;

	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
		
	}

	public void onStart() {

		setupTest("SVOTest");
		driver = getDriver();

		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
		SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
		SVOAdditionalvehicleContainer = PageFactory.initElements(driver, SVOAdditionalVehicleContainer.class);

		RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
				SVO_RestrictedPartyScreeningContainer.class);
		SVOItemToapproveContainer = PageFactory.initElements(driver, SVOItemToApproveContainer.class);
		SVO_CustomerResponsesContainer = PageFactory.initElements(driver, SVO_CustomerResponsesContainer.class);
		SVO_EnquiryLostReasonContainer = PageFactory.initElements(driver, SVO_EnquiryLostReasonContainer.class);
		CaseCreationEmailContainer = PageFactory.initElements(driver, SSEC_1226_CaseCreationEmail_Container.class);
		AdditionalVehicleName_AutoNumberContainer = PageFactory.initElements(driver, SSEC_2119_AdditionalVehicleName_AutoNumberContainer.class);
		Mass_upload_ETracker_URL_AdditionalVehicleContainer = PageFactory.initElements(driver, SSEC_2221_Mass_upload_ETracker_URL_AdditionalVehicleContainer.class);
		Continuation_Work_OrdersContainer = PageFactory.initElements(driver, SSEC_2237_Continuation_Work_OrdersContainer.class);
		
		driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);

		verificationCode = null;
		vCode = null;
		veriCode = null;
		NewWindow = null;
		CaseCreated = null;
		CaseNumber = null;

	}


    @And("^User navigates to Opportunities tab$")
    public void User_navigates_to_Opportunities_tab() throws Exception{
    	
    	ActionHandler.click(Continuation_Work_OrdersContainer.OpportunitiesLink);
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Click on Opportunity link");
        
    }
    
    @And("^Create a new Continuation Opportunity record type$")
    public void Create_new_Continaution_Opportunity_record_type() throws Exception{
    	
    	ActionHandler.click(Continuation_Work_OrdersContainer.NewOpportunityBtn);
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Click on New Oppotunity button");
        
    	ActionHandler.click(Continuation_Work_OrdersContainer.ClassicContinuationRadio);
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Click on Classic Continuation Radio button");
        
    	ActionHandler.click(Continuation_Work_OrdersContainer.NextBtn);
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Click on Next button");
        
    	
    }
    
    public static String selectFutureDate() {
		final DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date currentDate = new Date();
		String currentDateSTR = dateFormat.format(currentDate);
		Calendar c = Calendar.getInstance();
		c.setTime(currentDate);
		c.add(Calendar.DATE, 1);
		c.add(Calendar.MONTH, 1);
		Date currentDatePlusOne = c.getTime();
		String FutureDate = dateFormat.format(currentDatePlusOne);
		System.out.println("Future Date is = " + FutureDate);

		return FutureDate;

	}
    
    @Then("^Fill all the mandatory details Stage \"([^\"]*)\" Region \"([^\"]*)\" Market \"([^\"]*)\" AccName \"([^\"]*)\" PreferredContact \"([^\"]*)\" Programme \"([^\"]*)\" VAT \"([^\"]*)\" Source \"([^\"]*)\"$")
    public void Fill_all_mandatory_details(String Stage, String Region, String Market, String AccountName, 
    		String Programme, String VATQualify, String Source, String PreferredContact) throws Exception{
    	
    	double randomNum = getRandomIntegerBetweenRange(1, 1000);

    	String OpportunityName = "Test_Opportunity" + randomNum;
    	
    	String Stg[] = Stage.split(",");
    	Stage = CommonFunctions.readExcelMasterData(Stg[0], Stg[1], Stg[2]);

    	String Reg[] = Region.split(",");
    	Region = CommonFunctions.readExcelMasterData(Reg[0], Reg[1], Reg[2]);

    	String Mar[] = Market.split(",");
    	Market = CommonFunctions.readExcelMasterData(Mar[0], Mar[1], Mar[2]);

    	String AccName[] = AccountName.split(",");
    	AccountName = CommonFunctions.readExcelMasterData(AccName[0], AccName[1], AccName[2]);

    	String PrefContact[] = PreferredContact.split(",");
    	PreferredContact = CommonFunctions.readExcelMasterData(PrefContact[0], PrefContact[1], PrefContact[2]);
    			
    	String Prog[] = Programme.split(",");
    	Programme = CommonFunctions.readExcelMasterData(Prog[0], Prog[1], Prog[2]);

    	String VAT[] = VATQualify.split(",");
    	VATQualify = CommonFunctions.readExcelMasterData(VAT[0], VAT[1], VAT[2]);

    	String Src[] = Source.split(",");
    	Source = CommonFunctions.readExcelMasterData(Src[0], Src[1], Src[2]);

        ActionHandler.click(Continuation_Work_OrdersContainer.OpportunityNameLabel);
        ActionHandler.wait(1);
        ActionHandler.setText(Continuation_Work_OrdersContainer.OpportunityNameLabel, OpportunityName);
        ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Click on Opportunuty Name");
        
    	ActionHandler.click(Continuation_Work_OrdersContainer.CloseDateLabel);
        ActionHandler.setText(Continuation_Work_OrdersContainer.CloseDateLabel, selectFutureDate());
        ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Click on Close Date label");
        
    	ActionHandler.click(Continuation_Work_OrdersContainer.StageLabel);
    	ActionHandler.click(driver.findElement(By.xpath(Continuation_Work_OrdersContainer.Stage(Stage))));
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Click on Stage");
        
        ActionHandler.click(Continuation_Work_OrdersContainer.RegionLabel);
    	ActionHandler.click(driver.findElement(By.xpath(Continuation_Work_OrdersContainer.Stage(Region))));
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Click on Region");
        
    	ActionHandler.click(Continuation_Work_OrdersContainer.MarketLabel);
    	ActionHandler.click(driver.findElement(By.xpath(Continuation_Work_OrdersContainer.Stage(Market))));
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Click on Market");
        
    	ActionHandler.click(Continuation_Work_OrdersContainer.AccountNameLabel);
    	ActionHandler.setText(Continuation_Work_OrdersContainer.AccountNameLabel, "mauli");
    	ActionHandler.click(driver.findElement(By.xpath(Continuation_Work_OrdersContainer.AccountName(AccountName))));
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Click on Account Name label");
        
    	ActionHandler.click(Continuation_Work_OrdersContainer.PreferredContactLabel);
    	ActionHandler.setText(Continuation_Work_OrdersContainer.PreferredContactLabel, "mauli");
    	ActionHandler.click(driver.findElement(By.xpath(Continuation_Work_OrdersContainer.AccountName(PreferredContact))));
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Click on Preferred Contact label");
        
    	ActionHandler.click(Continuation_Work_OrdersContainer.ProgrammeLabel);
    	ActionHandler.click(driver.findElement(By.xpath(Continuation_Work_OrdersContainer.Stage(Programme))));
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Click on Programme label");
        
    	ActionHandler.click(Continuation_Work_OrdersContainer.RetailPriceLabel);
    	ActionHandler.setText(Continuation_Work_OrdersContainer.RetailPriceLabel, "0");
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Click on Retail Price label");
        
    	ActionHandler.click(Continuation_Work_OrdersContainer.VATQualifyingLabel);
    	ActionHandler.wait(1);
    	ActionHandler.click(driver.findElement(By.xpath(Continuation_Work_OrdersContainer.Stage(VATQualify))));
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Click on VAT Qualifying");
        
    	ActionHandler.click(Continuation_Work_OrdersContainer.SourceLabel);
    	ActionHandler.wait(1);
    	ActionHandler.click(driver.findElement(By.xpath(Continuation_Work_OrdersContainer.Stage(Source))));
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Click on Source label");
        
    }
    
    @And("^Click on Save button$")
    public void Click_on_Save_button() throws Exception{
    	
    	ActionHandler.click(Continuation_Work_OrdersContainer.SaveOpportunityBtn);
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Click on Save Opportunity");
        
    }
    
    @And("^Adds the Donor field$")
    public void Adds_donor_field(String VehicleVIN) throws Exception{
    	
    	String Vehicle[] = VehicleVIN.split(",");
    	VehicleVIN = CommonFunctions.readExcelMasterData(Vehicle[0], Vehicle[1], Vehicle[2]);

    	ActionHandler.pageDown();
    	ActionHandler.pageDown();
    	
    	ActionHandler.click(Continuation_Work_OrdersContainer.EditVehicleVIN);
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Click on Edit Vehicle VIN");
        
    	ActionHandler.click(Continuation_Work_OrdersContainer.VehicleLabel);
    	ActionHandler.wait(1);
    	ActionHandler.click(driver.findElement(By.xpath(Continuation_Work_OrdersContainer.Stage(VehicleVIN))));
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Click on Vehicle label");
        
    	ActionHandler.click(Continuation_Work_OrdersContainer.SaveEditOpportunity);
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Click on SaveEdit Opportunity");
        
    }
    
    @And("^Fill BodyStyle$")
    public void Fill_BodyStyle() throws Exception{
    	
    	String BodyStyle = "90";
    	
    	ActionHandler.click(Continuation_Work_OrdersContainer.EditBodyStyle);
    	ActionHandler.setText(Continuation_Work_OrdersContainer.EditBodyStyle, BodyStyle);
    	ActionHandler.pressEnter();
    	ActionHandler.click(Continuation_Work_OrdersContainer.BodyStyleLink);
    	ActionHandler.wait(1);
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Click on Body Style Link");
    	
    	
    }
    
    @Then("^Fills Specifications and Options section HandOfDrive \"([^\"]*)\" Speedometer \"([^\"]*)\"$")
    public void Fills_Specifications_Options(String HandOfDrive, String Speedometer) throws Exception{
    	
    	String HandoDrive[] = HandOfDrive.split(",");
    	HandOfDrive = CommonFunctions.readExcelMasterData(HandoDrive[0], HandoDrive[1], HandoDrive[2]);

    	String Speed[] = Speedometer.split(",");
    	Speedometer = CommonFunctions.readExcelMasterData(Speed[0], Speed[1], Speed[2]);

        //ActionHandler.click(Continuation_Work_OrdersContainer.EditHandOfDrive);
    	
    	ActionHandler.click(Continuation_Work_OrdersContainer.HandOfDriveLabel);
    	ActionHandler.click(driver.findElement(By.xpath(Continuation_Work_OrdersContainer.Stage(HandOfDrive))));
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Click on HandOfDrive");
        
    	ActionHandler.click(Continuation_Work_OrdersContainer.SpeedometerLabel);
    	ActionHandler.click(driver.findElement(By.xpath(Continuation_Work_OrdersContainer.Stage(Speedometer))));
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Click on Speedometer");
        
    	ActionHandler.click(Continuation_Work_OrdersContainer.SaveEditOpportunity);
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Click on Save Edit Opportunity");
        
    }
    
    @And("^Click on Mark Stage as Complete$")
    public void Click_on_Mark_Stage_as_Complete() throws Exception{
    	
    	ActionHandler.click(Continuation_Work_OrdersContainer.MarkStageCompleteBtn);
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Click on Mark Stage Complete button");
        
    }
    
    @Then("^User navigates to WorkOrder from Orders tab$")
    public void User_navigates_to_WorkOrder_from_Orders_tab() throws Exception{
    	
    	ActionHandler.click(Continuation_Work_OrdersContainer.OrdersTab);
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Click on Orders Tab");
        
    	ActionHandler.click(Continuation_Work_OrdersContainer.OrderRec);
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Click on Orders record");
        
    	ActionHandler.click(Continuation_Work_OrdersContainer.WorkOrderRec);
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Click on work order record");
        
    	VerifyHandler.verifyElementPresent(Continuation_Work_OrdersContainer.VINTextWorkOrder);
    	String VINTextWorkOrder = Continuation_Work_OrdersContainer.VINTextWorkOrder.getText();
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Click on VIN Text Work Order");
        
    }
    
    @Then("^Verify that Orders tab is blank$")
    public void Verify_that_Orders_tab_is_blank() throws Exception{
    	
    	ActionHandler.click(Continuation_Work_OrdersContainer.OrdersTab);
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Click on Orders tab");
        
    	VerifyHandler.verifyElementPresent(Continuation_Work_OrdersContainer.OrdersEmptyPage);
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Click on Orders Empty page");
        
    }










}
