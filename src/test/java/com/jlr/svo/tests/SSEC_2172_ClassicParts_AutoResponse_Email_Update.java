package com.jlr.svo.tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.containers.SSEC_1227_CasesCreationContainer;
import com.jlr.svo.containers.SSEC_1237_CaseClosureContainer;
import com.jlr.svo.containers.SSEC_2172_ClassicParts_AutoResponseEmailUpdateContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.containers.SVOOpportunityContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class SSEC_2172_ClassicParts_AutoResponse_Email_Update extends TestBaseCC {

	private WebDriver driver = getDriver();
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SSEC_1227_CasesCreationContainer CasesContainer = PageFactory.initElements(driver,
			SSEC_1227_CasesCreationContainer.class);
	SSEC_1237_CaseClosureContainer CaseClosureContainer = PageFactory.initElements(driver,
			SSEC_1237_CaseClosureContainer.class);
	SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
	SSEC_2172_ClassicParts_AutoResponseEmailUpdateContainer AutoResponseEmailUpdateContainer = PageFactory.initElements(driver, SSEC_2172_ClassicParts_AutoResponseEmailUpdateContainer.class);
	
	
	public static String verificationCode;
	public static String vCode;
	static double randomNum = getRandomNumber(0, 10000);
	public static String firstName = "Test" + randomNum;
	public static String lastName = "S" + randomNum;
	public static String Chassis = "T" + randomNum;

	public static double getRandomNumber(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	// onStart
	public void onStart() {
		setupTest("SVOTest");
		driver = getDriver();
		SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		CasesContainer = PageFactory.initElements(driver, SSEC_1227_CasesCreationContainer.class);
		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		CaseClosureContainer = PageFactory.initElements(driver, SSEC_1237_CaseClosureContainer.class);
		SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
		AutoResponseEmailUpdateContainer = PageFactory.initElements(driver, SSEC_2172_ClassicParts_AutoResponseEmailUpdateContainer.class);
		
		verificationCode = null;

		driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
	}
	
	
    @Then("^User goes to Activity tab$")
    public void User_goes_to_Activity_Tab() throws Exception{
    	
    	ActionHandler.click(AutoResponseEmailUpdateContainer.ActivityTab);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on Activity button");
    	
    }

    @And("^Clicks on Compose button and then clicks on insert template and selects the template$")
    public void Click_on_Compose_btn_and_select_template() throws Exception{
    	
    	ActionHandler.click(AutoResponseEmailUpdateContainer.ComposeBtn);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("CLick on Compose button");
    	
    	ActionHandler.pageDown();
    	ActionHandler.wait(2);
    	
    	ActionHandler.click(AutoResponseEmailUpdateContainer.InsertTemplateBtn);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("CLick on Insert Template button");
    	
    	ActionHandler.click(AutoResponseEmailUpdateContainer.TemplateSelectionOption);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Select the template from drop down");
    	
    }

    @Then("^User clicks on Send button and verifies that the email is sent$")
    public void User_clicks_on_Send_btn() throws Exception{
    	
    	ActionHandler.click(AutoResponseEmailUpdateContainer.SendEmailBtn);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("CLick on Send button");
    	
    }
    
    @Then("^edit the auto response template$")
    public void edit_the_auto_response_template() throws Exception{
    	
    	ActionHandler.setText(AutoResponseEmailUpdateContainer.BodyEmail, "Thanks & Regards");
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Edit the Body of Auto response email");
    	
    }
    
    @Then("^User clicks on Send button$")
    public void User_clicks_on_Send_button() throws Exception{
    	
    	ActionHandler.click(AutoResponseEmailUpdateContainer.SendEmailBtn);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on Send button");
    	
    }
    
    @Then("^User navigates to Emails tab from Quick Links and click on Email record$")
    public void User_navigates_to_Emails_tab_from_Quick_Links() throws Exception{
    	
    	ActionHandler.click(AutoResponseEmailUpdateContainer.EmailsQuickLink);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on Emails tab in Quick Links");
    	
    	ActionHandler.click(AutoResponseEmailUpdateContainer.EmailRecord);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on Email record button");
    	
    }
    


}