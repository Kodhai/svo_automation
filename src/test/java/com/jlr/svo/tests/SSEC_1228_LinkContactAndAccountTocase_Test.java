package com.jlr.svo.tests;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.autotest.utils.Config;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.constants.Constants;
import com.jlr.svo.containers.SSEC_1226_CaseCreationEmail_Container;
import com.jlr.svo.containers.SSEC_1228_LinkContactAndAccountTocase_Container;
import com.jlr.svo.containers.SSEC_1234_CaseListViews_Container;
import com.jlr.svo.containers.SVOAccountsContainer;
import com.jlr.svo.containers.SVOAdditionalVehicleContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.containers.SVOItemToApproveContainer;
import com.jlr.svo.containers.SVOOpportunityContainer;
import com.jlr.svo.containers.SVO_CustomerResponsesContainer;
import com.jlr.svo.containers.SVO_EnquiryLostReasonContainer;
import com.jlr.svo.containers.SVO_RestrictedPartyScreeningContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SSEC_1228_LinkContactAndAccountTocase_Test extends TestBaseCC {
	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory
			.getLogger(SSEC_1228_LinkContactAndAccountTocase_Test.class.getName());
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);

	SVOAccountsContainer SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
	SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	SVOAdditionalVehicleContainer SVOAdditionalvehicleContainer = PageFactory.initElements(driver,
			SVOAdditionalVehicleContainer.class);
	SVO_RestrictedPartyScreeningContainer RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
			SVO_RestrictedPartyScreeningContainer.class);
	SVOItemToApproveContainer SVOItemToapproveContainer = PageFactory.initElements(driver,
			SVOItemToApproveContainer.class);
	SVO_CustomerResponsesContainer SVO_CustomerResponsesContainer = PageFactory.initElements(driver,
			SVO_CustomerResponsesContainer.class);
	SVO_EnquiryLostReasonContainer SVO_EnquiryLostReasonContainer = PageFactory.initElements(driver,
			SVO_EnquiryLostReasonContainer.class);
	SSEC_1226_CaseCreationEmail_Container CaseCreationEmailContainer = PageFactory.initElements(driver,
			SSEC_1226_CaseCreationEmail_Container.class);
	SSEC_1228_LinkContactAndAccountTocase_Container LinkContactAndAccountTocaseContainer = PageFactory
			.initElements(driver, SSEC_1228_LinkContactAndAccountTocase_Container.class);
	SSEC_1234_CaseListViews_Test CaseListViews_Test = PageFactory.initElements(driver,
			SSEC_1234_CaseListViews_Test.class);
	SSEC_1234_CaseListViews_Container CaseListViewsContainer = PageFactory.initElements(driver,
			SSEC_1234_CaseListViews_Container.class);

	public static String verificationCode;
	public static String vCode;
	public static String veriCode;
	public String NewWindow;
	public static String CaseCreated;
	public static String CaseNumber;
	public static String ContactName;

	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	public void onStart() {

		setupTest("SVOTest");
		driver = getDriver();

		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
		SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
		SVOAdditionalvehicleContainer = PageFactory.initElements(driver, SVOAdditionalVehicleContainer.class);

		RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
				SVO_RestrictedPartyScreeningContainer.class);
		SVOItemToapproveContainer = PageFactory.initElements(driver, SVOItemToApproveContainer.class);
		SVO_CustomerResponsesContainer = PageFactory.initElements(driver, SVO_CustomerResponsesContainer.class);
		SVO_EnquiryLostReasonContainer = PageFactory.initElements(driver, SVO_EnquiryLostReasonContainer.class);
		CaseCreationEmailContainer = PageFactory.initElements(driver, SSEC_1226_CaseCreationEmail_Container.class);
		LinkContactAndAccountTocaseContainer = PageFactory.initElements(driver,
				SSEC_1228_LinkContactAndAccountTocase_Container.class);
		CaseListViewsContainer = PageFactory.initElements(driver, SSEC_1234_CaseListViews_Container.class);
		CaseListViews_Test = PageFactory.initElements(driver, SSEC_1234_CaseListViews_Test.class);

		driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);

		verificationCode = null;
		vCode = null;
		veriCode = null;
		NewWindow = null;
		CaseCreated = null;
		CaseNumber = null;
		ContactName = null;

	}

	@Given("^Access to SVO Portal as admin by using Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void access_to_an_SVO_Portal_as_admin_by_using_Username_and_Password(String userName, String password)
			throws Throwable {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		onStart();
		driver = getDriver();
		driver.get(Constants.SVOURL);
		Reporter.addStepLog("User Logins to SVO Portal");

		ActionHandler.setText(SVO_OpportunityContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVO_OpportunityContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVO_OpportunityContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailAdmin();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVO_OpportunityContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVO_OpportunityContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVO_OpportunityContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.remindMeLater)) {

				ActionHandler.click(SVO_OpportunityContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.notRegister)) {

				ActionHandler.click(SVO_OpportunityContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	public String checkEmailAdmin() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();
			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();
			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);

				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(510, 517);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	@Then("^login to SVO portal by customer service user \"([^\"]*)\" via setup login$")
	public void login_to_SVO_portal_by_customer_service_user_role_login(String User) throws Throwable {
		String U[] = User.split(",");
		User = CommonFunctions.readExcelMasterData(U[0], U[1], U[2]);

		javaScriptUtil.clickElementByJS(SVOAccountsContainer.setUpBtn);
		ActionHandler.wait(3);
		ActionHandler.click(SVOItemToapproveContainer.SetUpIcon);
		ActionHandler.wait(10);

		NewWindow = driver.getWindowHandle();

		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String SetUpWindow = i.next();
			if (!NewWindow.equalsIgnoreCase(SetUpWindow)) {
				driver.switchTo().window(SetUpWindow);

				ActionHandler.click(SVOAccountsContainer.SearchSetup);
				ActionHandler.wait(2);
				ActionHandler.setText(SVOAccountsContainer.SearchSetup, User);
				ActionHandler.wait(5);

				ActionHandler.click(SVOItemToapproveContainer.DesignBriefApprovalUser);
				Reporter.addStepLog("selects the user role");
				CommonFunctions.attachScreenshot();

				ActionHandler.wait(20);
				driver.navigate().refresh();
				ActionHandler.wait(20);
				driver.switchTo().frame(0);
				ActionHandler.click(SVOItemToapproveContainer.SetUpUserLoginBtn);
				Reporter.addStepLog("User is logged in successfully");
				CommonFunctions.attachScreenshot();

				driver.switchTo().parentFrame();

			}
		}

	}

	@And("^User Navigate to Cases Tab$")
	public void User_Navigate_to_Cases_Tab() throws Throwable {

		ActionHandler.wait(1);
		javaScriptUtil.clickElementByJS(CaseCreationEmailContainer.CasesTab);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigates to Cases tab");
	}

	@Then("^Logouts from the SVO Portal$")
	public void logouts_from_the_SVO_Portal() throws Throwable {

		ActionHandler.click(SVO_OpportunityContainer.icon_image);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Logout");
		ActionHandler.click(SVO_OpportunityContainer.Logout);

		CommonFunctions.attachScreenshot();
		if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.userNameTextBox)) {
			MarkupHelper.createLabel("Logged out successfully", ExtentColor.GREEN);
		} else {
			MarkupHelper.createLabel("Log out failed", ExtentColor.RED);

		}
		driver.quit();
	}

	@And("^Create a new case of record type \"([^\"]*)\" by linking case with existing Account \"([^\"]*)\" and Contact \"([^\"]*)\" details$")
	public void create_a_new_case_of_record_type_by_linking_case_with_existing_Account_and_Contact_details(
			String RecordType, String Account, String Contact) throws Throwable {

		String RT[] = RecordType.split(",");
		RecordType = CommonFunctions.readExcelMasterData(RT[0], RT[1], RT[2]);

		String Acc[] = Account.split(",");
		Account = CommonFunctions.readExcelMasterData(Acc[0], Acc[1], Acc[2]);

		String C[] = Contact.split(",");
		Contact = CommonFunctions.readExcelMasterData(C[0], C[1], C[2]);

		double randomNumber = getRandomIntegerBetweenRange(0, 1000);

		ActionHandler.wait(1);
		ActionHandler.click(LinkContactAndAccountTocaseContainer.NewCaseButton);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on New button to create a case");

		ActionHandler.click(
				driver.findElement(By.xpath(LinkContactAndAccountTocaseContainer.CaseRecordTypeSelection(RecordType))));
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects the case record type as " + RecordType);

		ActionHandler.click(LinkContactAndAccountTocaseContainer.CaseNextButton);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Case Next button");

		ActionHandler.setText(LinkContactAndAccountTocaseContainer.FirstNameTxtBox, RecordType + randomNumber);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters first name for the new case");

		ActionHandler.setText(LinkContactAndAccountTocaseContainer.LastNameTxtBox, "Test");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Last name for the new case");

		ActionHandler.click(LinkContactAndAccountTocaseContainer.AccountNameTxtBx);
		ActionHandler.wait(1);
		ActionHandler.setText(LinkContactAndAccountTocaseContainer.AccountNameTxtBx, Account);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(LinkContactAndAccountTocaseContainer.AccountSearchBar);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(LinkContactAndAccountTocaseContainer.AccountName);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Search and select an existing Account to link for the new case");

		ActionHandler.click(LinkContactAndAccountTocaseContainer.ContactNameTxtBx);
		ActionHandler.wait(1);
		ActionHandler.setText(LinkContactAndAccountTocaseContainer.ContactNameTxtBx, Contact);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(LinkContactAndAccountTocaseContainer.ContactSearchBar);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(LinkContactAndAccountTocaseContainer.AccountName);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Search and select an existing Contact to link with new case");

		ActionHandler.click(LinkContactAndAccountTocaseContainer.Vintxtbx);
		ActionHandler.wait(1);
		ActionHandler.setText(LinkContactAndAccountTocaseContainer.Vintxtbx, "67891762");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters VIN/chassis number");

		ActionHandler.click(LinkContactAndAccountTocaseContainer.SaveCaseButton);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Case save button");

		VerifyHandler.verifyElementHasFocus(LinkContactAndAccountTocaseContainer.CaseTitle);
		ActionHandler.wait(1);
		CaseNumber = LinkContactAndAccountTocaseContainer.CaseTitle.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Case is created successfully with case number : " + CaseNumber);

	}

	@Given("^Login to SVO Portal by Classic Parts and Technical user with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void login_to_an_SVO_portal_by_Classic_Parts_and_Technical_user_with_Username_and_Password(String userName,
			String password) throws Throwable {

		onStart();
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailOnClassicPartsAndTechnicalUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				CommonFunctions.attachScreenshot();

			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				CommonFunctions.attachScreenshot();

			} else {

				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {

			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	public String checkEmailOnClassicPartsAndTechnicalUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(1);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(493, 500);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	@Given("^Login to SVO Portal by Classic Customer Service user with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void login_to_an_SVO_portal_by_Classic_Customer_Service_user_with_Username_and_Password(String userName,
			String password) throws Throwable {

		onStart();
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailOnCustomerServiceUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			ActionHandler.wait(1);
			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				CommonFunctions.attachScreenshot();

			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				CommonFunctions.attachScreenshot();

			} else {

				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {

			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	public String checkEmailOnCustomerServiceUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(1);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(494, 500);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	@Then("^logout from user login via set up$")
	public void logout_from_user_login_via_set_up() throws Throwable {

		driver.switchTo().parentFrame();
		ActionHandler.click(SVOItemToapproveContainer.BespokeUserLogoutlink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("logout from SVO Portal as bespoke user");

		driver.close();
		driver.switchTo().window(NewWindow);

	}

	@Then("^Verify that newly created case through web is linked with existing Account and Contact$")
	public void verify_that_newly_created_case_through_web_is_linked_with_existing_Account_and_Contact()
			throws Throwable {

		ActionHandler.wait(1);
		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);

		VerifyHandler.verifyElementHasFocus(LinkContactAndAccountTocaseContainer.AccountNameField);
		ActionHandler.wait(1);
		String AccountName = LinkContactAndAccountTocaseContainer.AccountNameField.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Case is linked with the Account : " + AccountName);

		VerifyHandler.verifyElementHasFocus(LinkContactAndAccountTocaseContainer.ContactNameField);
		ActionHandler.wait(1);
		String ContactName = LinkContactAndAccountTocaseContainer.ContactNameField.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Case is linked with the Contact : " + ContactName);

	}

	@Then("^Verify that newly created case is linked with existing Account and Contact$")
	public void verify_that_newly_created_case_is_linked_with_existing_Account_and_Contact() throws Throwable {

		ActionHandler.wait(1);
		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);

		VerifyHandler.verifyElementHasFocus(LinkContactAndAccountTocaseContainer.AccountNameField);
		ActionHandler.wait(1);
		String AccountName = LinkContactAndAccountTocaseContainer.AccountNameField.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Case is linked with the Account : " + AccountName);

		VerifyHandler.verifyElementHasFocus(LinkContactAndAccountTocaseContainer.ContactNameField);
		ActionHandler.wait(1);
		String ContactName = LinkContactAndAccountTocaseContainer.ContactNameField.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Case is linked with the Contact : " + ContactName);

		VerifyHandler.verifyElementHasFocus(LinkContactAndAccountTocaseContainer.ContactEmailField);
		ActionHandler.wait(1);
		String ContactEmail = LinkContactAndAccountTocaseContainer.ContactEmailField.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(ContactEmail + " Email is autopopulated for linked contact details");

	}

	@And("^Create a new case of record type \"([^\"]*)\" by linking case with newly created Contact$")
	public void create_a_new_case_of_record_type_by_linking_case_with_new_Contact(String RecordType) throws Throwable {

		String RT[] = RecordType.split(",");
		RecordType = CommonFunctions.readExcelMasterData(RT[0], RT[1], RT[2]);

		double randomNumber = getRandomIntegerBetweenRange(0, 1000);

		ActionHandler.wait(1);
		ActionHandler.click(LinkContactAndAccountTocaseContainer.NewCaseButton);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on New button to create a case");

		ActionHandler.click(
				driver.findElement(By.xpath(LinkContactAndAccountTocaseContainer.CaseRecordTypeSelection(RecordType))));
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects the case record type as " + RecordType);

		ActionHandler.click(LinkContactAndAccountTocaseContainer.CaseNextButton);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Case Next button");

		ActionHandler.setText(LinkContactAndAccountTocaseContainer.FirstNameTxtBox, RecordType + randomNumber);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters first name for the new case");

		ActionHandler.setText(LinkContactAndAccountTocaseContainer.LastNameTxtBox, "Test");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Last name for the new case");

		ActionHandler.click(LinkContactAndAccountTocaseContainer.ContactNameTxtBx);
		ActionHandler.wait(1);
		ActionHandler.setText(LinkContactAndAccountTocaseContainer.ContactNameTxtBx, ContactName);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(LinkContactAndAccountTocaseContainer.ContactSearchBar);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(LinkContactAndAccountTocaseContainer.AccountName);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Search and select an existing Contact to link with new case");

		ActionHandler.click(LinkContactAndAccountTocaseContainer.SaveCaseButton);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Case save button");

		VerifyHandler.verifyElementHasFocus(LinkContactAndAccountTocaseContainer.CaseTitle);
		ActionHandler.wait(1);
		CaseNumber = LinkContactAndAccountTocaseContainer.CaseTitle.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Case is created successfully with case number : " + CaseNumber);

	}

	@Then("^Verify that case is linked with newly created contact along with autopopulated Account field$")
	public void Verify_that_case_is_linked_with_newly_created_contact_along_with_autopopulated_Account_field()
			throws Throwable {

		ActionHandler.wait(1);
		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);

		VerifyHandler.verifyElementHasFocus(LinkContactAndAccountTocaseContainer.AccountNameField);
		ActionHandler.wait(1);
		String AccountName = LinkContactAndAccountTocaseContainer.AccountNameField.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Case is linked with the Account : " + AccountName);

		VerifyHandler.verifyElementHasFocus(LinkContactAndAccountTocaseContainer.ContactNameField);
		ActionHandler.wait(1);
		String ContactName = LinkContactAndAccountTocaseContainer.ContactNameField.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Case is linked with the Contact : " + ContactName);

		Reporter.addStepLog(
				"User Verified that the case is linked with newly created contact along with autopopulated Account field");

	}

	@Then("^User Navigate to Contacts Tab$")
	public void user_Navigate_to_Contacts_Tab() throws Throwable {

		ActionHandler.wait(1);
		javaScriptUtil.clickElementByJS(LinkContactAndAccountTocaseContainer.ContactsTab);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigates to Contacts tab");
	}

	@Then("^Create a new contact with contact name \"([^\"]*)\" and AccountName \"([^\"]*)\"$")
	public void create_a_new_contact_with_contact_name_and_AccountName(String Contact, String Account)
			throws Throwable {

		String CT[] = Contact.split(",");
		Contact = CommonFunctions.readExcelMasterData(CT[0], CT[1], CT[2]);

		String AT[] = Account.split(",");
		Account = CommonFunctions.readExcelMasterData(AT[0], AT[1], AT[2]);

		double randomNumber = getRandomIntegerBetweenRange(0, 1000);

		ActionHandler.wait(1);
		ActionHandler.click(LinkContactAndAccountTocaseContainer.NewCaseButton);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on New button to create a Contact");

		ActionHandler.click(LinkContactAndAccountTocaseContainer.ContactNameTextBox);
		ActionHandler.wait(1);
		ActionHandler.setText(LinkContactAndAccountTocaseContainer.ContactNameTextBox, Contact + randomNumber);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters first name for the new case");

		ActionHandler.click(LinkContactAndAccountTocaseContainer.AccountTxtBx);
		ActionHandler.wait(1);
		ActionHandler.setText(LinkContactAndAccountTocaseContainer.AccountTxtBx, Account);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(LinkContactAndAccountTocaseContainer.AccountSearch);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(LinkContactAndAccountTocaseContainer.AccountName);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Search and select an Account to create a contact");

		ActionHandler.click(LinkContactAndAccountTocaseContainer.SaveContactButton);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Save contact button");

	}

	@Then("^Verify new contact is created on salesforce$")
	public void verify_new_contact_is_created_on_salesforce() throws Throwable {

		VerifyHandler.verifyElementPresent(LinkContactAndAccountTocaseContainer.ContactTitle);
		ActionHandler.wait(1);
		ContactName = LinkContactAndAccountTocaseContainer.ContactTitle.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Contact is created successfully with Title : " + ContactName);

	}

	@Given("^Login to email account with Username \"([^\"]*)\" and password \"([^\"]*)\"$")
	public void login_to_email_account_with_Username_and_password(String UserName, String Password) throws Throwable {

		String s[] = UserName.split(",");
		UserName = com.jlr.svo.utilities.CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String p[] = Password.split(",");
		Password = CommonFunctions.readExcelMasterData(p[0], p[1], p[2]);

		onStart();
		driver = getDriver();
		ActionHandler.wait(5);
		driver.get(Constants.gmailURL);

		Reporter.addStepLog("User tries to login to Gmail");
		CommonFunctions.attachScreenshot();

		// ActionHandler.click(SVO_CustomerResponsesContainer.signinbutton);
		ActionHandler.wait(1);
		ActionHandler.setText(SVO_CustomerResponsesContainer.outlookEmailId, UserName);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(1);
		ActionHandler.click(SVO_CustomerResponsesContainer.gmailNext);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVO_CustomerResponsesContainer.gmailPassword, Password);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(1);
		ActionHandler.click(SVO_CustomerResponsesContainer.gmailNext);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User logged into Gmail Account");

		VerifyHandler.verifyElementPresent(SVO_CustomerResponsesContainer.gmailText);

	}

	// Customer sends case creation mail to user
	@When("^Customer sends a case creation mail to user \"([^\"]*)\"$")
	public void customer_sends_a_case_creation_mail_to_user(String User) throws Throwable {

		String p[] = User.split(",");
		User = CommonFunctions.readExcelMasterData(p[0], p[1], p[2]);

		String CaseSubject = "Case creation mail for Test_";
		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		String CaseTitle = "Case creation mail for Test";
		ActionHandler.click(SVO_CustomerResponsesContainer.NewMailCompose);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Compose email icon");

		ActionHandler.click(SVO_CustomerResponsesContainer.GmailToTextBox);
		ActionHandler.wait(1);
		ActionHandler.setText(SVO_CustomerResponsesContainer.GmailToTextBox, User);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Enters To address to compose an email");

		ActionHandler.click(SVO_CustomerResponsesContainer.GmailSubjectTextBox);
		ActionHandler.wait(1);
		ActionHandler.setText(SVO_CustomerResponsesContainer.GmailSubjectTextBox, CaseSubject + randomNumber);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters subject to the mail as Case creation mail for Test");

		ActionHandler.click(SVO_CustomerResponsesContainer.GmailBodyTextBox);
		ActionHandler.wait(1);
		ActionHandler.setText(SVO_CustomerResponsesContainer.GmailBodyTextBox, CaseTitle);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Body text to the mail as Case creation request");

		ActionHandler.click(SVO_CustomerResponsesContainer.SendMailBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on send email button");

		Reporter.addStepLog(User + " : sent an email to create an Case on salesforce");

	}

	@Then("^Logout from the user email account$")
	public void logout_from_the_user_email_account() throws Throwable {

		ActionHandler.click(SVO_CustomerResponsesContainer.GmailaccountLogo);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Gmail Account logo");

		driver.switchTo().frame(SVO_CustomerResponsesContainer.mailframe);
		ActionHandler.click(SVO_CustomerResponsesContainer.signoutGmailIcon);
		ActionHandler.wait(5);
		driver.switchTo().parentFrame();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Logout button");

	}

	@And("^Verify User receives an outlook email notification on Parts and Technical case creation$")
	public void Verify_User_receives_an_outlook_email_notification_on_Parts_and_Technical_case_creation()
			throws Throwable {

		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get Case creation email");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		} else {
			ActionHandler.click(CaseCreationEmailContainer.SignInLink);
			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(1);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(CaseCreationEmailContainer.caseoutlookEmail);
				CommonFunctions.attachScreenshot();
				Reporter.addStepLog("User receives an case creation email");

				CaseCreated = CaseCreationEmailContainer.CaseNumber.getText();
				CaseNumber = CaseCreated.substring(278, 286);
				System.out.println("Created case = " + CaseNumber);
				Reporter.addStepLog("Case created : " + CaseNumber);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
	}

	@And("^Verify User receives an outlook email notification on Classic Service case creation$")
	public void Verify_User_receives_an_outlook_email_notification_on_Classic_Service_case_creation() throws Throwable {

		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get Case creation email");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		} else {
			ActionHandler.click(CaseCreationEmailContainer.SignInLink);
			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(1);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(CaseCreationEmailContainer.caseoutlookEmail);
				CommonFunctions.attachScreenshot();
				Reporter.addStepLog("User receives an case creation email");

				CaseCreated = CaseCreationEmailContainer.CaseNumber.getText();
				CaseNumber = CaseCreated.substring(271, 280);
				Reporter.addStepLog("User Verifies the case number that is created");
				System.out.println("Created case = " + CaseNumber);
				Reporter.addStepLog("Case created : " + CaseNumber);

				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
	}

	@Given("^Access to SVO Portal as Classic Parts and Technical user with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void Access_to_an_SVO_portal_as_Classic_Parts_and_Technical_user_with_Username_and_Password(String userName,
			String password) throws Throwable {

		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailOnClassicPartsAndTechnicalUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				CommonFunctions.attachScreenshot();

			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				CommonFunctions.attachScreenshot();

			} else {

				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {

			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	@And("^Verify that Parts and Technical case is automatically created on SVO Portal$")
	public void verify_that_Parts_and_Technical_case_is_automatically_created_on_SVO_Portal() throws Throwable {

		ActionHandler.click(SVO_CustomerResponsesContainer.SelectOpportunityListView);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on select list view tab");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		ActionHandler.click(CaseCreationEmailContainer.PartsAndTechnicalQueue);
		ActionHandler.wait(3);
		Reporter.addStepLog("User chooses to view selected to Parts And Technical queue list");
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(CaseCreationEmailContainer.SearchCaseTxtBox, CaseNumber);
		ActionHandler.wait(2);
		ActionHandler.pressEnter();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User searches for the Case that is created automatically");

		VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.FirstOpportunityLink);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Verifies that the new Parts and Technical Case is created on SVO Portal");

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstOpportunityLink);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects the case that is created automatically on salesforce");

	}

	@And("^User link the case with newly created contact$")
	public void user_link_the_case_with_newly_created_contact() throws Throwable {

		ActionHandler.wait(1);
		ActionHandler.click(LinkContactAndAccountTocaseContainer.EditCaseButton);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Edit Case button");

		if (!VerifyHandler.verifyElementPresent(LinkContactAndAccountTocaseContainer.LastNameTxtBox)) {
			ActionHandler.click(LinkContactAndAccountTocaseContainer.EditCaseButton1);
			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();

		}
		ActionHandler.click(LinkContactAndAccountTocaseContainer.LastNameTxtBox);
		ActionHandler.wait(1);
		ActionHandler.setText(LinkContactAndAccountTocaseContainer.LastNameTxtBox, "Test");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Last name for the new case");

		ActionHandler.click(LinkContactAndAccountTocaseContainer.Vintxtbx);
		ActionHandler.wait(1);
		ActionHandler.setText(LinkContactAndAccountTocaseContainer.Vintxtbx, "67891763");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters VIN/chassis number");

		if (VerifyHandler.verifyElementPresent(LinkContactAndAccountTocaseContainer.ClearAccountIcon)) {
			ActionHandler.click(LinkContactAndAccountTocaseContainer.ClearAccountIcon);
			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User clears existing Account field");
		}
		if (VerifyHandler.verifyElementPresent(LinkContactAndAccountTocaseContainer.ClearContactIcon)) {
			ActionHandler.click(LinkContactAndAccountTocaseContainer.ClearContactIcon);
			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User clears existing Contact field");
		}

		ActionHandler.click(LinkContactAndAccountTocaseContainer.ContactNameTxtBx);
		ActionHandler.wait(1);
		ActionHandler.setText(LinkContactAndAccountTocaseContainer.ContactNameTxtBx, ContactName);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(LinkContactAndAccountTocaseContainer.ContactSearchBar);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(LinkContactAndAccountTocaseContainer.AccountName);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Search and select an existing Contact to link with new case");

		ActionHandler.click(LinkContactAndAccountTocaseContainer.SaveCaseButton);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Case save button");

		VerifyHandler.verifyElementHasFocus(LinkContactAndAccountTocaseContainer.CaseTitle);
		ActionHandler.wait(1);
		CaseNumber = LinkContactAndAccountTocaseContainer.CaseTitle.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User linked newly created contact to the parts and technical case created through mail");

	}

	@Given("^Login to SVO Portal with Classic Sales user with the Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void login_to_an_SVO_Portal_with_classic_sales_user_with_the_Username_and_Password(String userName,
			String password) throws Throwable {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		onStart();
		driver = getDriver();
		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailForClassicSalesUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			Reporter.addStepLog("User logins to SVO Successfully");

		}
	}

	public String checkEmailForClassicSalesUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(1);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(513, 520);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	@And("^Verify that cases tab is not available so that user is not able to link Contact to Customer Service case$")
	public void Verify_that_cases_tab_is_not_available_so_that_user_is_not_able_to_link_Contact_to_Customer_Service_case()
			throws Throwable {
		if (!VerifyHandler.verifyElementPresent(CaseCreationEmailContainer.CasesTab)) {
			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog(
					"User Verifies that cases tab is not available so that user is not able to link Contact to Customer Service case");
		}

	}

	@Then("^Create Parts and Technical Case from web form with Email \"([^\"]*)\" First name \"([^\"]*)\" Last name \"([^\"]*)\" Country \"([^\"]*)\" Phone number \"([^\"]*)\" VIN number \"([^\"]*)\"$")
	public void Create_Parts_and_Technical_case_from_web_form_with_email(String email, String first, String last,
			String country, String phone, String vin) throws Throwable {

		String em[] = email.split(",");
		email = CommonFunctions.readExcelMasterData(em[0], em[1], em[2]);

		String fname[] = first.split(",");
		first = CommonFunctions.readExcelMasterData(fname[0], fname[1], fname[2]);

		String lname[] = last.split(",");
		last = CommonFunctions.readExcelMasterData(lname[0], lname[1], lname[2]);

		String cty[] = country.split(",");
		country = CommonFunctions.readExcelMasterData(cty[0], cty[1], cty[2]);

		String num[] = phone.split(",");
		phone = CommonFunctions.readExcelMasterData(num[0], num[1], num[2]);

		String v[] = vin.split(",");
		vin = CommonFunctions.readExcelMasterData(v[0], v[1], v[2]);

		// enter first name
		ActionHandler.setText(LinkContactAndAccountTocaseContainer.FirstNameTxtBx, first);
		ActionHandler.wait(1);
		ActionHandler.setText(LinkContactAndAccountTocaseContainer.FirstNameTxtBx, first);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters First name as : " + first);

		// enter last name
		ActionHandler.setText(LinkContactAndAccountTocaseContainer.LastNameTxtBx, last);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Last name as : " + last);

		ActionHandler.wait(1);
		ActionHandler.setText(LinkContactAndAccountTocaseContainer.InputEmailAddress, email);
		ActionHandler.wait(1);
		ActionHandler.setText(LinkContactAndAccountTocaseContainer.InputEmailAddress, email);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters email as : " + email);

		// enter phone number
		ActionHandler.setText(LinkContactAndAccountTocaseContainer.PhoneNumberTxtBx, phone);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(LinkContactAndAccountTocaseContainer.HelpSelect);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act1.sendKeys(Keys.ENTER).build().perform();
		Reporter.addStepLog("User Selects Parts and Technical Enquiry from HelpSelect Drop down list");

		ActionHandler.click(LinkContactAndAccountTocaseContainer.VINNumber);
		ActionHandler.wait(1);
		ActionHandler.setText(LinkContactAndAccountTocaseContainer.VINNumber, vin);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters the vin number");

		// enter country
		ActionHandler.setText(LinkContactAndAccountTocaseContainer.CountrySelection, country);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		ActionHandler.pressEnter();
		ActionHandler.wait(1);
		Reporter.addStepLog("User enters country as : " + country);

		String comment = "test";
		ActionHandler.setText(LinkContactAndAccountTocaseContainer.CommentText, comment);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters the comment text");

		// click on continue
		javaScriptUtil.clickElementByJS(LinkContactAndAccountTocaseContainer.SubmitBtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Submit");
		Reporter.addStepLog("User verifies that contact us form submitted successfully");
	}

	@Given("^Login to an Classic Parts portal$")
	public void login_to_an_Classic_Parts_portal() throws Throwable {
		onStart();

		driver.get("https://" + "iweb_26b06a" + ":" + "PJuR7ifMuPqr" + "@" + Constants.ClassicParts);
		ActionHandler.wait(1);
		Reporter.addStepLog("User access to Classic Parts portal");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(LinkContactAndAccountTocaseContainer.AllowCookies);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User accepts cookie policies");

	}

	@And("^Navigate to Contact Us section$")
	public void Navigate_to_Contact_Us_section() throws Throwable {

		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(1);
		javaScriptUtil.clickElementByJS(LinkContactAndAccountTocaseContainer.ContactUsBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Contact Us section");

	}

	@And("^Verify that Classic Customer Service case is automatically created on SVO Portal$")
	public void verify_that_Classic_Customer_Service_case_is_automatically_created_on_SVO_Portal() throws Throwable {

		ActionHandler.click(SVO_CustomerResponsesContainer.SelectOpportunityListView);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on select list view tab");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		ActionHandler.click(CaseCreationEmailContainer.CustomerServiceQueue);
		ActionHandler.wait(3);
		Reporter.addStepLog("User chooses to view selected to Customer Service Queue queue list");
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(CaseCreationEmailContainer.SearchCaseTxtBox, CaseNumber);
		ActionHandler.wait(2);
		ActionHandler.pressEnter();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User searches for the Case that is created automatically");

		VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.FirstOpportunityLink);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Verifies that the new Parts and Technical Case is created on SVO Portal");

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstOpportunityLink);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects the case that is created automatically on salesforce");

	}

	@Given("^Login to the SVO Portal by Classic Customer Service user with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void login_to_the_SVO_portal_by_Classic_Customer_Service_user_with_Username_and_Password(String userName,
			String password) throws Throwable {

		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailOnCustomerServiceUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			ActionHandler.wait(1);
			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				CommonFunctions.attachScreenshot();

			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				CommonFunctions.attachScreenshot();

			} else {

				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {

			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	@Then("^Create Customer Service Case from web form with Email \"([^\"]*)\" First name \"([^\"]*)\" Last name \"([^\"]*)\" Country \"([^\"]*)\" Phone number \"([^\"]*)\" VIN number \"([^\"]*)\"$")
	public void Create_Customer_Service_case_from_web_form_with_email(String email, String first, String last,
			String country, String phone, String vin) throws Throwable {

		String em[] = email.split(",");
		email = CommonFunctions.readExcelMasterData(em[0], em[1], em[2]);

		String fname[] = first.split(",");
		first = CommonFunctions.readExcelMasterData(fname[0], fname[1], fname[2]);

		String lname[] = last.split(",");
		last = CommonFunctions.readExcelMasterData(lname[0], lname[1], lname[2]);

		String cty[] = country.split(",");
		country = CommonFunctions.readExcelMasterData(cty[0], cty[1], cty[2]);

		String num[] = phone.split(",");
		phone = CommonFunctions.readExcelMasterData(num[0], num[1], num[2]);

		String v[] = vin.split(",");
		vin = CommonFunctions.readExcelMasterData(v[0], v[1], v[2]);

		// enter first name
		ActionHandler.setText(LinkContactAndAccountTocaseContainer.FirstNameTxtBx, first);
		ActionHandler.wait(1);
		ActionHandler.setText(LinkContactAndAccountTocaseContainer.FirstNameTxtBx, first);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters First name as : " + first);

		// enter last name
		ActionHandler.setText(LinkContactAndAccountTocaseContainer.LastNameTxtBx, last);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Last name as : " + last);

		ActionHandler.wait(1);
		ActionHandler.setText(LinkContactAndAccountTocaseContainer.InputEmailAddress, email);
		ActionHandler.wait(1);
		ActionHandler.setText(LinkContactAndAccountTocaseContainer.InputEmailAddress, email);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters email as : " + email);

		// enter phone number
		ActionHandler.setText(LinkContactAndAccountTocaseContainer.PhoneNumberTxtBx, phone);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(LinkContactAndAccountTocaseContainer.HelpSelect);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act1.sendKeys(Keys.ENTER).build().perform();
		Reporter.addStepLog("User Selects Customer Service Enquiry from HelpSelect Drop down list");

		// enter country
		ActionHandler.click(LinkContactAndAccountTocaseContainer.CountrySelection);
		ActionHandler.wait(1);
		ActionHandler.setText(LinkContactAndAccountTocaseContainer.CountrySelection, country);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		ActionHandler.pressEnter();
		ActionHandler.wait(1);
		Reporter.addStepLog("User enters country as : " + country);

		ActionHandler.setText(LinkContactAndAccountTocaseContainer.VINNumber, vin);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters the vin number");

		String comment = "test";
		ActionHandler.setText(LinkContactAndAccountTocaseContainer.CommentText, comment);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters the comment text");

		// click on continue
		javaScriptUtil.clickElementByJS(LinkContactAndAccountTocaseContainer.SubmitBtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Submit");
		Reporter.addStepLog("User verifies that contact us form submitted successfully");
	}

	@And("^Verify that Classic Customer Service case is not created on SVO Portal$")
	public void verify_that_Classic_Customer_Service_case_is_not_created_on_SVO_Portal() throws Throwable {

		ActionHandler.click(SVO_CustomerResponsesContainer.SelectOpportunityListView);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on select list view tab");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		ActionHandler.click(CaseCreationEmailContainer.CustomerServiceQueue);
		ActionHandler.wait(3);
		Reporter.addStepLog("User chooses to view selected to Customer Service Queue queue list");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.FirstOpportunityLink)) {
			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User Verifies Classic Customer Service case is not created on SVO Portal");
		}
	}

	@Given("^One Time Login to SVO Portal as Classic Customer Service user with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void One_Time_login_to_an_SVO_portal_by_Classic_Customer_Service_user_with_Username_and_Password(
			String UserName, String Password) throws Throwable {

		driver = getDriver();
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);

		ActionHandler.wait(2);
		if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.userNameTextBox)) {
			ActionHandler.wait(1);
			ActionHandler.click(SVOEnquiryContainer.icon_image);
			ActionHandler.wait(6);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Click on Logout");
			ActionHandler.click(SVOEnquiryContainer.Logout);
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logout from SVO Portal");
		}

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		Reporter.addStepLog("User Logins to SVO Portal");

		String s[] = UserName.split(",");
		UserName = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String p[] = Password.split(",");
		Password = CommonFunctions.readExcelMasterData(p[0], p[1], p[2]);

		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, UserName);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, Password);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();

		ActionHandler.waitForElement(SVOEnquiryContainer.SVOText, 60);
		CommonFunctions.attachScreenshot();
		VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
		Reporter.addStepLog("User navigate to SVO Home Page successfully");
	}

}
