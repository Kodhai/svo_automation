package com.jlr.svo.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.containers.SSEC_1227_CasesCreationContainer;
import com.jlr.svo.containers.SSEC_1229_Case_QueuesContainer;
import com.jlr.svo.containers.SSEC_1237_CaseClosureContainer;
import com.jlr.svo.containers.SSEC_2095_ClassicPartsWebToCaseContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class SSEC_2095_ClassicPart_WebToCase_Test extends TestBaseCC {
	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory.getLogger(SSEC_1227_CasesCreation_Test.class.getName());
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);

	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SSEC_1227_CasesCreationContainer CasesContainer = PageFactory.initElements(driver,
			SSEC_1227_CasesCreationContainer.class);
	SSEC_1237_CaseClosureContainer CaseClosureContainer = PageFactory.initElements(driver,
			SSEC_1237_CaseClosureContainer.class);

	SSEC_1229_Case_QueuesContainer Case_QueuesContainer = PageFactory.initElements(driver, SSEC_1229_Case_QueuesContainer.class); 
	SSEC_2095_ClassicPartsWebToCaseContainer SSEC_2095_ClassicPartsWebToCaseContainer = PageFactory.initElements(driver, SSEC_2095_ClassicPartsWebToCaseContainer.class);
//

	public static String verificationCode;
	public static String vCode;
	double randomNum = getRandomNumber(0, 10000);
	String firstName = "Test" + randomNum;
	String lastName = "tes" + randomNum;
	String VINNum = "Test";
	String Chassis = VINNum + randomNum;
	String Chassis2 = VINNum + randomNum;

	public static double getRandomNumber(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	// onStart
	public void onStart() {
		setupTest("SVOTest");
		driver = getDriver();
		CasesContainer = PageFactory.initElements(driver, SSEC_1227_CasesCreationContainer.class);
		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		Case_QueuesContainer = PageFactory.initElements(driver, SSEC_1229_Case_QueuesContainer.class); 
		SSEC_2095_ClassicPartsWebToCaseContainer = PageFactory.initElements(driver, SSEC_2095_ClassicPartsWebToCaseContainer.class);
	
		verificationCode = null;

	}
	
    @And("^Open an existing case record$")
    public void Open_an_existing_case_record() throws Exception{
    	
    	ActionHandler.click(SSEC_2095_ClassicPartsWebToCaseContainer.CaseRec);
    	ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Cases Record");
    }
    
    @And("^Verify all the fields in the Cases record$")
    public void Verify_all_the_fields_in_the_cases_record() throws Exception{
    	
    	VerifyHandler.verifyElementPresent(SSEC_2095_ClassicPartsWebToCaseContainer.FirstNameLabel);
    	String FirstName = SSEC_2095_ClassicPartsWebToCaseContainer.FirstNameLabel.getText();
    	ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies First Name label");
		
    	VerifyHandler.verifyElementPresent(SSEC_2095_ClassicPartsWebToCaseContainer.LastNameLabel);
    	String LastName = SSEC_2095_ClassicPartsWebToCaseContainer.FirstNameLabel.getText();
    	ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies last name label");
		
    	VerifyHandler.verifyElementPresent(SSEC_2095_ClassicPartsWebToCaseContainer.CaseRecordType);
    	String CaseRecordType = SSEC_2095_ClassicPartsWebToCaseContainer.FirstNameLabel.getText();
    	ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Case Record Type");
		
    	VerifyHandler.verifyElementPresent(SSEC_2095_ClassicPartsWebToCaseContainer.WebEmailLabel);
    	String WebEmail = SSEC_2095_ClassicPartsWebToCaseContainer.FirstNameLabel.getText();
    	ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies WebEmail");
		
    	VerifyHandler.verifyElementPresent(SSEC_2095_ClassicPartsWebToCaseContainer.WebPhoneLabel);
    	String WebPhone = SSEC_2095_ClassicPartsWebToCaseContainer.FirstNameLabel.getText();
    	ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies WebPhone");
    	
    }
    
    @Then("^Edit First name of the case$")
    public void Edit_First_Name_of_the_case() throws Exception{
    	
    	String FirstName = "test" + randomNum;
    	
    	ActionHandler.click(SSEC_2095_ClassicPartsWebToCaseContainer.EditFirstName);
    	ActionHandler.clearAndSetText(SSEC_2095_ClassicPartsWebToCaseContainer.EditFirstName, FirstName);
    	ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Edit Phone Number");
		
    	ActionHandler.click(SSEC_2095_ClassicPartsWebToCaseContainer.SaveEditBtn);
    	ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click son Save Edit button");
		
    }
    
    @And("^Verify that the case has Parts and Technical Queue as Case Owner$")
    public void Verify_that_case_has_Parts_and_Technical_Queue() throws Exception{
    	
    	String caseOwnerString = SSEC_2095_ClassicPartsWebToCaseContainer.CaseOwnerText.getText();
    	ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Case Owner");
    }
    
    



}
