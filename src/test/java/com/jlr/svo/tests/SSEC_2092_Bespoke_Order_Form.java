package com.jlr.svo.tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.containers.SSEC_1039_SVOEnquiryEmailRemaindersContainer;
import com.jlr.svo.containers.SSEC_1503_Clasic_Email_Signature_On_Salesforce_Container;
import com.jlr.svo.containers.SSEC_2159_BespokeOrderForm_CreationofOrderFormRecordsContainer;
import com.jlr.svo.containers.SVOAccountsContainer;
import com.jlr.svo.containers.SVOAdditionalVehicleContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.containers.SVOItemToApproveContainer;
import com.jlr.svo.containers.SVOOpportunityContainer;
import com.jlr.svo.containers.SVO_CustomerResponsesContainer;
import com.jlr.svo.containers.SVO_EnquiryLostReasonContainer;
import com.jlr.svo.containers.SVO_RestrictedPartyScreeningContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class SSEC_2092_Bespoke_Order_Form extends TestBaseCC {

	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory
			.getLogger(SSEC_1039_SVOEnquiryEmailRemaindersTest.class.getName());
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);

	SVOAccountsContainer SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
	SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	SVOAdditionalVehicleContainer SVOAdditionalvehicleContainer = PageFactory.initElements(driver,
			SVOAdditionalVehicleContainer.class);
	SVO_RestrictedPartyScreeningContainer RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
			SVO_RestrictedPartyScreeningContainer.class);
	SVOItemToApproveContainer SVOItemToapproveContainer = PageFactory.initElements(driver,
			SVOItemToApproveContainer.class);
	SVO_CustomerResponsesContainer SVO_CustomerResponsesContainer = PageFactory.initElements(driver,
			SVO_CustomerResponsesContainer.class);
	SVO_EnquiryLostReasonContainer SVO_EnquiryLostReasonContainer = PageFactory.initElements(driver,
			SVO_EnquiryLostReasonContainer.class);
	SSEC_1039_SVOEnquiryEmailRemaindersContainer SVOEnquiryEmailRemaindersContainer = PageFactory.initElements(driver,
			SSEC_1039_SVOEnquiryEmailRemaindersContainer.class);
	SSEC_1503_Clasic_Email_Signature_On_Salesforce_Container Clasic_Email_Signature_On_Salesforce_Container = PageFactory.initElements(driver,
			SSEC_1503_Clasic_Email_Signature_On_Salesforce_Container.class);
	SSEC_2159_BespokeOrderForm_CreationofOrderFormRecordsContainer BespokeOrderForm_CreationofOrderFormRecordsContainer = PageFactory.initElements(driver, SSEC_2159_BespokeOrderForm_CreationofOrderFormRecordsContainer.class);

	public static String verificationCode;
	public static String vCode;
	public static String veriCode;
	public static String EnquiryName;

	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}
	

	public void onStart() {

		setupTest("SVOTest");
		driver = getDriver();

		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
		SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
		SVOAdditionalvehicleContainer = PageFactory.initElements(driver, SVOAdditionalVehicleContainer.class);

		RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
				SVO_RestrictedPartyScreeningContainer.class);
		SVOItemToapproveContainer = PageFactory.initElements(driver, SVOItemToApproveContainer.class);
		SVO_CustomerResponsesContainer = PageFactory.initElements(driver, SVO_CustomerResponsesContainer.class);
		SVO_EnquiryLostReasonContainer = PageFactory.initElements(driver, SVO_EnquiryLostReasonContainer.class);
		SVOEnquiryEmailRemaindersContainer = PageFactory.initElements(driver,
				SSEC_1039_SVOEnquiryEmailRemaindersContainer.class);
		Clasic_Email_Signature_On_Salesforce_Container = PageFactory.initElements(driver,
				SSEC_1503_Clasic_Email_Signature_On_Salesforce_Container.class);
		BespokeOrderForm_CreationofOrderFormRecordsContainer = PageFactory.initElements(driver, SSEC_2159_BespokeOrderForm_CreationofOrderFormRecordsContainer.class);

		
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		

		verificationCode = null;
		vCode = null;
		EnquiryName = null;

	}

	   @And("^User creates a new Bespoke Opportunity where it selects model as 460AD$")
	   public void user_creates_a_new_Bespoke_Opportunity_and_selects_model_as_L460() throws Exception{
		
	}
	
	   @Then("^User verifies that new Opportunity is created$")
	   public void user_verifies_new_oppportunity_is_created() throws Exception{
		
	}

	   @Then("^User navigates to Quotes from Related Links$")
	   public void user_navigates_to_Quotes_from_Related_Links() throws Exception{
		
		   ActionHandler.click(BespokeOrderForm_CreationofOrderFormRecordsContainer.QuotesQuickLink);
	}
	
	   @And("^User creates new Bespoke Design Brief quotes \"([^\"]*)\" with mandatory details$")
	   public void user_creates_new_Bespoke_Design_Brief_Quotes(String RetailerCon) throws Exception{
		
		   String RetContact[] = RetailerCon.split(",");
		   RetailerCon = CommonFunctions.readExcelMasterData(RetContact[0], RetContact[1], RetContact[2]);

		   double randomNum = getRandomIntegerBetweenRange(0, 10000);

		   String QuoteName = "test" + randomNum;
		   
		   ActionHandler.click(BespokeOrderForm_CreationofOrderFormRecordsContainer.QuoteName);
		   ActionHandler.wait(2);
		   ActionHandler.setText(BespokeOrderForm_CreationofOrderFormRecordsContainer.QuoteName, QuoteName);
		   ActionHandler.wait(4);
		   CommonFunctions.attachScreenshot();
		   Reporter.addStepLog("User clicks on Quote Name");
		   
		   ActionHandler.click(BespokeOrderForm_CreationofOrderFormRecordsContainer.RetailerContactLabel);
		   ActionHandler.wait(2);
		   ActionHandler.click(driver.findElement(By.xpath(BespokeOrderForm_CreationofOrderFormRecordsContainer.RetailerContact(RetailerCon))));
		   ActionHandler.wait(4);
		   CommonFunctions.attachScreenshot();
		   Reporter.addStepLog("User clicks on Retailer Contact");
			
		   ActionHandler.click(BespokeOrderForm_CreationofOrderFormRecordsContainer.SaveQuoteBtn);
		   ActionHandler.wait(5);
		   CommonFunctions.attachScreenshot();
		   Reporter.addStepLog("User clicks on Save Quote button");
   
	}
	
	   @Then("^User verifies that new quote is created$")
	   public void user_verifies_new_uote_is_created() throws Exception{
		   
	       VerifyHandler.verifyElementPresent(BespokeOrderForm_CreationofOrderFormRecordsContainer.VerifyQuoteName);
	       ActionHandler.wait(5);
		   CommonFunctions.attachScreenshot();
		   Reporter.addStepLog("User verifies Quote Name");
	   }
	   
	   @Then("^Navigate to newly created Bespoke Design Brief quote then navigate to Design Brief in the related quick links$")
	   public void Navigate_to_newly_created_quote_and_then_to_Design_brief_record() throws Exception{
		   
	    	ActionHandler.click(BespokeOrderForm_CreationofOrderFormRecordsContainer.QuoteCreatedLink);
	    	ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User clicks on Quote created link");
			
	    	ActionHandler.click(BespokeOrderForm_CreationofOrderFormRecordsContainer.DesignBriefsQuickLink);
	    	ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User clicks on Design Brief link");
			
	    	ActionHandler.click(BespokeOrderForm_CreationofOrderFormRecordsContainer.DesignBriefsText);
	    	ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User clicks on Design Brief text ");

	   }
	   
	   @And("^Click on Design Brief quote name and click on Submit button$")
	   public void Click_on_Design_Brief_quote_name_and_click_on_Submit_button() throws Exception{
		   
		   ActionHandler.click(BespokeOrderForm_CreationofOrderFormRecordsContainer.DesignBriefRecord);
		   ActionHandler.wait(5);
		   CommonFunctions.attachScreenshot();
		   Reporter.addStepLog("User clicks on Design Brief record");
		   
		   ActionHandler.click(BespokeOrderForm_CreationofOrderFormRecordsContainer.SubmitBtn);
		   ActionHandler.wait(5);
		   CommonFunctions.attachScreenshot();
		   Reporter.addStepLog("User clicks on Submit button");
			
		   ActionHandler.click(BespokeOrderForm_CreationofOrderFormRecordsContainer.SaveDesignBriefBtn);
		   ActionHandler.wait(5);
		   CommonFunctions.attachScreenshot();
		   Reporter.addStepLog("User clicks on Save button for Design Brief");

	   }
	   
	   @Then("^Click on return render pack button$")
	   public void Click_on_return_render_pack_button() throws Exception{
		   
		   ActionHandler.click(BespokeOrderForm_CreationofOrderFormRecordsContainer.ReturnRenderPackBtn);
		   ActionHandler.wait(5);
		   CommonFunctions.attachScreenshot();
		   Reporter.addStepLog("User clicks on Return Render pack button");
			
		   ActionHandler.click(BespokeOrderForm_CreationofOrderFormRecordsContainer.SaveReturnRenderPackBtn);
		   ActionHandler.wait(5);
		   CommonFunctions.attachScreenshot();
		   Reporter.addStepLog("User clicks on Save button for Return Render pack button");
		   

	   }
	   
	   @And("^Click on first record and verify that model is 460AD$")
	   public void Click_on_first_record_and_verify_that_model_is_L460() throws Exception{
		   
	   }


	   @And("^User creates a new Bespoke Opportunity where it selects model as 460AH$")
	   public void user_creates_a_new_Bespoke_Opportunity_and_selects_model_as_460AH() throws Exception{
		
	}

	   @And("^Click on first record and verify that model is 460AH$")
	   public void Click_on_first_record_and_verify_that_model_is_460AH() throws Exception{
		   
	   }

	   @And("^User creates a new Bespoke Opportunity where it selects model as 460AM$")
	   public void user_creates_a_new_Bespoke_Opportunity_and_selects_model_as_460AM() throws Exception{
		
	}

	   @And("^Click on first record and verify that model is 460AM$")
	   public void Click_on_first_record_and_verify_that_model_is_460AM() throws Exception{
		   
	   }
	   
	   @And("^User creates a new Bespoke Opportunity where it selects model as 460AS$")
	   public void user_creates_a_new_Bespoke_Opportunity_and_selects_model_as_460AS() throws Exception{
		
	}

	   @And("^Click on first record and verify that model is 460AS$")
	   public void Click_on_first_record_and_verify_that_model_is_460AS() throws Exception{
		   
	   }

	   @And("^User creates a new Bespoke Opportunity where it selects model as 460AW$")
	   public void user_creates_a_new_Bespoke_Opportunity_and_selects_model_as_460AW() throws Exception{
		
	}

	   @And("^Click on first record and verify that model is 460AW$")
	   public void Click_on_first_record_and_verify_that_model_is_460AW() throws Exception{
		   
	   }
	   
	   @And("^User creates a new Bespoke Opportunity where it selects model as 460BB$")
	   public void user_creates_a_new_Bespoke_Opportunity_and_selects_model_as_460BB() throws Exception{
		
	}

	   @And("^Click on first record and verify that model is 460BB$")
	   public void Click_on_first_record_and_verify_that_model_is_460BB() throws Exception{
		   
	   }
	   
	   @And("^User creates a new Bespoke Opportunity where it selects model as 460BV$")
	   public void user_creates_a_new_Bespoke_Opportunity_and_selects_model_as_460BV() throws Exception{
		
	}

	   @And("^Click on first record and verify that model is 460BV$")
	   public void Click_on_first_record_and_verify_that_model_is_460BV() throws Exception{
		   
	   }
	   
	   @And("^User creates a new Bespoke Opportunity where it selects model as 460BZ$")
	   public void user_creates_a_new_Bespoke_Opportunity_and_selects_model_as_460BZ() throws Exception{
		
	}

	   @And("^Click on first record and verify that model is 460BZ$")
	   public void Click_on_first_record_and_verify_that_model_is_460BZ() throws Exception{
		   
	   }
	   
	   @And("^User creates a new Bespoke Opportunity where it selects model as 460CD$")
	   public void user_creates_a_new_Bespoke_Opportunity_and_selects_model_as_460CD() throws Exception{
		
	}

	   @And("^Click on first record and verify that model is 460CD$")
	   public void Click_on_first_record_and_verify_that_model_is_460CD() throws Exception{
		   
	   }
	   
	   @And("^User creates a new Bespoke Opportunity where it selects model as 460CH$")
	   public void user_creates_a_new_Bespoke_Opportunity_and_selects_model_as_460CH() throws Exception{
		
	}

	   @And("^Click on first record and verify that model is 460CH$")
	   public void Click_on_first_record_and_verify_that_model_is_460CH() throws Exception{
		   
	   }
	   
	   @And("^User creates a new Bespoke Opportunity where it selects model as 460CX$")
	   public void user_creates_a_new_Bespoke_Opportunity_and_selects_model_as_460CX() throws Exception{
		
	}

	   @And("^Click on first record and verify that model is 460CX$")
	   public void Click_on_first_record_and_verify_that_model_is_460CX() throws Exception{
		   
	   }
	   
	   @And("^User creates a new Bespoke Opportunity where it selects model as 460CZ$")
	   public void user_creates_a_new_Bespoke_Opportunity_and_selects_model_as_460CZ() throws Exception{
		
	}

	   @And("^Click on first record and verify that model is 460CZ$")
	   public void Click_on_first_record_and_verify_that_model_is_460CZ() throws Exception{
		   
	   }




	
	


}
