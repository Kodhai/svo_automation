package com.jlr.svo.tests;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.autotest.utils.Config;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.constants.Constants;
import com.jlr.svo.containers.SSEC_1226_CaseCreationEmail_Container;
import com.jlr.svo.containers.SSEC_1228_LinkContactAndAccountTocase_Container;
import com.jlr.svo.containers.SSEC_1234_CaseListViews_Container;
import com.jlr.svo.containers.SVOAccountsContainer;
import com.jlr.svo.containers.SVOAdditionalVehicleContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.containers.SVOItemToApproveContainer;
import com.jlr.svo.containers.SVOOpportunityContainer;
import com.jlr.svo.containers.SVO_CustomerResponsesContainer;
import com.jlr.svo.containers.SVO_EnquiryLostReasonContainer;
import com.jlr.svo.containers.SVO_RestrictedPartyScreeningContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class SSEC_1234_CaseListViews_Test extends TestBaseCC {
	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory.getLogger(SSEC_1234_CaseListViews_Test.class.getName());
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);

	SVOAccountsContainer SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
	SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	SVOAdditionalVehicleContainer SVOAdditionalvehicleContainer = PageFactory.initElements(driver,
			SVOAdditionalVehicleContainer.class);
	SVO_RestrictedPartyScreeningContainer RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
			SVO_RestrictedPartyScreeningContainer.class);
	SVOItemToApproveContainer SVOItemToapproveContainer = PageFactory.initElements(driver,
			SVOItemToApproveContainer.class);
	SVO_CustomerResponsesContainer SVO_CustomerResponsesContainer = PageFactory.initElements(driver,
			SVO_CustomerResponsesContainer.class);
	SVO_EnquiryLostReasonContainer SVO_EnquiryLostReasonContainer = PageFactory.initElements(driver,
			SVO_EnquiryLostReasonContainer.class);
	SSEC_1226_CaseCreationEmail_Container CaseCreationEmailContainer = PageFactory.initElements(driver,
			SSEC_1226_CaseCreationEmail_Container.class);
	SSEC_1228_LinkContactAndAccountTocase_Container LinkContactAndAccountTocaseContainer = PageFactory
			.initElements(driver, SSEC_1228_LinkContactAndAccountTocase_Container.class);
	SSEC_1234_CaseListViews_Container CaseListViewsContainer = PageFactory.initElements(driver,
			SSEC_1234_CaseListViews_Container.class);

	public static String verificationCode;
	public static String vCode;
	public static String veriCode;
	public String NewWindow;
	public static String CaseCreated;
	public static String CaseNumber;
	public static String ContactName;

	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	public void onStart() {

		setupTest("SVOTest");
		driver = getDriver();

		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
		SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
		SVOAdditionalvehicleContainer = PageFactory.initElements(driver, SVOAdditionalVehicleContainer.class);

		RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
				SVO_RestrictedPartyScreeningContainer.class);
		SVOItemToapproveContainer = PageFactory.initElements(driver, SVOItemToApproveContainer.class);
		SVO_CustomerResponsesContainer = PageFactory.initElements(driver, SVO_CustomerResponsesContainer.class);
		SVO_EnquiryLostReasonContainer = PageFactory.initElements(driver, SVO_EnquiryLostReasonContainer.class);
		CaseCreationEmailContainer = PageFactory.initElements(driver, SSEC_1226_CaseCreationEmail_Container.class);
		LinkContactAndAccountTocaseContainer = PageFactory.initElements(driver,
				SSEC_1228_LinkContactAndAccountTocase_Container.class);
		CaseListViewsContainer = PageFactory.initElements(driver, SSEC_1234_CaseListViews_Container.class);

		driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);

		verificationCode = null;
		vCode = null;
		veriCode = null;
		NewWindow = null;
		CaseCreated = null;
		CaseNumber = null;
		ContactName = null;

	}

	@Given("^Login to Salesforce Portal with Classic Sales user with the Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void login_to_Salesforce_Portal_with_classic_sales_user_with_the_Username_and_Password(String userName,
			String password) throws Throwable {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		onStart();
		driver = getDriver();
		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailForClassicSalesUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			Reporter.addStepLog("User logins to SVO Successfully");

		}
	}

	public String checkEmailForClassicSalesUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(1);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(513, 520);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	@Given("^Login to Salesforce Portal by Classic Parts and Technical user with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void login_to_Salesforce_portal_by_Classic_Parts_and_Technical_user_with_Username_and_Password(
			String userName, String password) throws Throwable {

		onStart();
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailOnClassicPartsAndTechnicalUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				CommonFunctions.attachScreenshot();

			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				CommonFunctions.attachScreenshot();

			} else {

				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {

			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	public String checkEmailOnClassicPartsAndTechnicalUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(1);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(493, 500);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	@Given("^Login to the Salesforce Portal by Classic Customer Service user with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void login_to_the_Salesforce_portal_by_Classic_Customer_Service_user_with_Username_and_Password(
			String userName, String password) throws Throwable {

		onStart();
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailOnCustomerServiceUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			ActionHandler.wait(1);
			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				CommonFunctions.attachScreenshot();

			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				CommonFunctions.attachScreenshot();

			} else {

				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {

			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	public String checkEmailOnCustomerServiceUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(1);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(494, 500);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	@And("^Verify \"([^\"]*)\" cases list view on cases tab$")
	public void Verify_cases_list_view_on_cases_tab(String CasesListView) throws Throwable {

		String c[] = CasesListView.split(",");
		CasesListView = CommonFunctions.readExcelMasterData(c[0], c[1], c[2]);

		ActionHandler.click(SVO_CustomerResponsesContainer.SelectOpportunityListView);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on select list view tab");

		ActionHandler.click(driver.findElement(By.xpath(CaseListViewsContainer.CaseListViewSelection(CasesListView))));
		ActionHandler.wait(3);
		Reporter.addStepLog("User chooses to view " + CasesListView + " list");
		CommonFunctions.attachScreenshot();

		VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.FirstOpportunityLink);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Verifies that the " + CasesListView + " cases list view on cases tab");

	}

	@And("^Verify fields like CaseNumber Subject Status CaseOwner Priority added to the list view$")
	public void Verify_fields_like_CaseNumber_Subject_Status_CaseOwner_Priority_added_to_the_list_view()
			throws Throwable {

		VerifyHandler.verifyElementPresent(CaseListViewsContainer.CaseNumberField);
		String CaseNumberField = CaseListViewsContainer.CaseNumberField.getText();
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Verifies that case number field is displayed as" + CaseNumberField);

		VerifyHandler.verifyElementPresent(CaseListViewsContainer.ContactNameField);
		String ContactNameField = CaseListViewsContainer.ContactNameField.getText();
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Verifies that contact name field is displayed as" + ContactNameField);

		VerifyHandler.verifyElementPresent(CaseListViewsContainer.SubjectField);
		String SubjectField = CaseListViewsContainer.SubjectField.getText();
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Verifies that subject field is displayed as" + SubjectField);

		VerifyHandler.verifyElementPresent(CaseListViewsContainer.StatusField);
		String StatusField = CaseListViewsContainer.StatusField.getText();
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Verifies that Status field is displayed as" + StatusField);

		VerifyHandler.verifyElementPresent(CaseListViewsContainer.PriorityField);
		String PriorityField = CaseListViewsContainer.PriorityField.getText();
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Verifies that Priority field is displayed as" + PriorityField);

		VerifyHandler.verifyElementPresent(CaseListViewsContainer.CaseOwnerAliasField);
		String CaseOwnerAliasField = CaseListViewsContainer.CaseOwnerAliasField.getText();
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Verifies that CaseOwner Alias field is displayed as" + CaseOwnerAliasField);

		VerifyHandler.verifyElementPresent(CaseListViewsContainer.CaseRecordTypeField);
		String CaseRecordTypeField = CaseListViewsContainer.CaseRecordTypeField.getText();
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Verifies that Case RecordType field is displayed as" + CaseRecordTypeField);
	}

	@And("^Verify that cases tab is not available so that user is not able to view Cases List view$")
	public void Verify_that_cases_tab_is_not_available_so_that_user_is_not_able_to_view_Cases_List_view()
			throws Throwable {
		if (!VerifyHandler.verifyElementPresent(CaseCreationEmailContainer.CasesTab)) {
			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog(
					"User Verifies that that cases tab is not available so that user is not able to view Cases List view");
		}

	}

	@And("^Verify that user is not able to view All Parts and Technical Closed Cases$")
	public void Verify_that_user_is_not_able_to_view_All_Parts_and_Technical_Closed_Cases() throws Throwable {

		ActionHandler.click(SVO_CustomerResponsesContainer.SelectOpportunityListView);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on select list view tab");

		if (!VerifyHandler.verifyElementPresent(CaseListViewsContainer.CasesDueClosure))
			;
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User is not able to view All Parts and Technical Closed Cases");
	}

	@And("^User Navigate to the Cases Tab$")
	public void User_Navigate_to_the_Cases_Tab() throws Throwable {

		ActionHandler.wait(1);
		javaScriptUtil.clickElementByJS(CaseCreationEmailContainer.CasesTab);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigates to Cases tab");
	}

	@Then("^Logouts from the Salesforce Portal$")
	public void logouts_from_the_Salesforce_Portal() throws Throwable {

		ActionHandler.click(SVO_OpportunityContainer.icon_image);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Logout");
		ActionHandler.click(SVO_OpportunityContainer.Logout);

		CommonFunctions.attachScreenshot();
		if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.userNameTextBox)) {
			MarkupHelper.createLabel("Logged out successfully", ExtentColor.GREEN);
		} else {
			MarkupHelper.createLabel("Log out failed", ExtentColor.RED);

		}
		driver.quit();
	}

}
