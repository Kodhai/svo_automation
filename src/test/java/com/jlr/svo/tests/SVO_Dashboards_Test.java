package com.jlr.svo.tests;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.autotest.utils.Config;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.constants.Constants;
import com.jlr.svo.containers.SVOAdditionalVehicleContainer;
import com.jlr.svo.containers.SVODashboardsContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.containers.SVOItemToApproveContainer;
import com.jlr.svo.containers.SVOOpportunityContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SVO_Dashboards_Test extends TestBaseCC {

	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory.getLogger(SVO_Item_to_approve_Test.class.getName());
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);

	SVOItemToApproveContainer SVOItemToapproveContainer = PageFactory.initElements(driver,
			SVOItemToApproveContainer.class);
	SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	SVOAdditionalVehicleContainer SVOAdditionalvehicleContainer = PageFactory.initElements(driver,
			SVOAdditionalVehicleContainer.class);
	SVODashboardsContainer SVOdashboardsContainer = PageFactory.initElements(driver, SVODashboardsContainer.class);

	public static String verificationCode;
	public static String vCode;
	public static String veriCode;
	public static String DashBoardCreatedBy;
	public static String DashBoardViewingAs;
	public static String DashBoardLastModifiedData;
	public static String DashboardCopy;
	public static String NewDashboardName;

	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	public void onStart() {

		setupTest("SVOTest");
		driver = getDriver();
		SVOItemToapproveContainer = PageFactory.initElements(driver, SVOItemToApproveContainer.class);
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
		SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
		SVOAdditionalvehicleContainer = PageFactory.initElements(driver, SVOAdditionalVehicleContainer.class);
		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		SVOdashboardsContainer = PageFactory.initElements(driver, SVODashboardsContainer.class);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		verificationCode = null;
		vCode = null;
		DashBoardCreatedBy = null;
		DashBoardViewingAs = null;
		DashBoardLastModifiedData = null;
		DashboardCopy = null;
		NewDashboardName = null;

	}

	@Given("^Access to SVO bespoke operation user Portal with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void access_to_an_SVO_bespoke_operation_user_Portal_with_Username_and_Password(String userName,
			String password) throws Throwable {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		onStart();
		driver = getDriver();
		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailbespokeoperationUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	public String checkEmailbespokeoperationUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		// if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookIcon)) {

		ActionHandler.click(SVOEnquiryContainer.outlookSignin);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.outlookNext);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
		CommonFunctions.attachScreenshot();
		// }

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(338, 344);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	@Given("^Access to SVO as bespoke operation user with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void access_to_an_SVO_as_bespoke_operation_user_with_Username_and_Password(String userName, String password)
			throws Throwable {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		ActionHandler.click(SVO_OpportunityContainer.icon_image);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Logout");

		ActionHandler.click(SVOdashboardsContainer.AddUsername);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Add Username");

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailForbespokeoperationUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	public String checkEmailForbespokeoperationUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(338, 344);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	@Given("^Access to SVO Portal as Classic service user role with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void Access_to_SVO_Portal_as_Classic_service_user_role_with_Username_and_Password(String userName,
			String password) throws Throwable {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		onStart();
		driver = getDriver();
		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailForClassicServiceUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	public String checkEmailForClassicServiceUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		 if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

		ActionHandler.click(SVOEnquiryContainer.outlookSignin);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.outlookNext);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
		CommonFunctions.attachScreenshot();
		 }

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(342, 348);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	@Given("^Access to SVO bespoke uat user Portal with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void access_to_an_SVO_bespoke_uat_user_Portal_with_Username_and_Password(String userName, String password)
			throws Throwable {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		onStart();
		driver = getDriver();
		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailBespokeUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	public String checkEmailBespokeUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(334, 340);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	@Given("^Access to SVO Private office user Portal with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void access_to_an_SVO_private_office_user_Portal_with_Username_and_Password(String userName, String password)
			throws Throwable {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		onStart();
		driver = getDriver();
		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailPrivateOficeUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	public String checkEmailPrivateOficeUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(341, 347);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	@Given("^Access to SVO Classic Operations user Portal with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void access_to_an_SVO_Classic_Operations_user_Portal_with_Username_and_Password(String userName,
			String password) throws Throwable {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		onStart();
		driver = getDriver();
		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailClassicOperationsUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	public String checkEmailClassicOperationsUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookIcon)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(338, 344);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	@Then("^Logout from an SVO Portal$")
	public void logout_from_an_SVO_Portal() throws Throwable {

		ActionHandler.click(SVO_OpportunityContainer.icon_image);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Logout");
		ActionHandler.click(SVO_OpportunityContainer.Logout);

		CommonFunctions.attachScreenshot();
		if (VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.userNameTextBox)) {
			MarkupHelper.createLabel("Logged out successfully", ExtentColor.GREEN);
		} else {
			MarkupHelper.createLabel("Log out failed", ExtentColor.RED);
		}
		driver.quit();
	}

	@Then("^Navigate to Dashboards page$")
	public void navigate_to_Dashboards_page() throws Throwable {

		if (VerifyHandler.verifyElementPresent(SVOdashboardsContainer.DashboardsTab)) {
			javaScriptUtil.clickElementByJS(SVOdashboardsContainer.DashboardsTab);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User Navigate to Dashboards page");
		} else {
			ActionHandler.click(SVOdashboardsContainer.MoreButton);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User Click on More button");

			javaScriptUtil.clickElementByJS(SVOdashboardsContainer.DashBoardDropDownTab);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User Selects dashboard tab from 'More' drop down list");
		}

	}

	@Then("^Verify that dashboard Created by user deatils are updated on All dashboards page$")
	public void verify_that_dashboard_Created_by_user_deatils_are_updated_on_All_dashboards_page() throws Throwable {

		ActionHandler.wait(6);
		ActionHandler.click(SVOdashboardsContainer.AllDashBoardsfolder);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigated to All Dashboards folder");

		VerifyHandler.verifyElementPresent(SVOdashboardsContainer.DashBoardCreatedByUser);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Dash Board created by user details are updated and displayed on Performance dashboard");

		DashBoardCreatedBy = SVOdashboardsContainer.DashBoardCreatedByUser.getText();
		Reporter.addStepLog(
				"Classic Consent dashboard of perfromance folder is created by user  = " + DashBoardCreatedBy);

	}

	@Then("^Verify that viewing as user details is updated and displayed on Performance dashboard$")
	public void verify_that_viewing_as_user_details_is_updated_and_displayed_on_Performance_dashboard()
			throws Throwable {

		ActionHandler.wait(5);
		javaScriptUtil.clickElementByJS(SVOdashboardsContainer.ClassicConsentDashboardLink);

		ActionHandler.wait(5);
		driver.switchTo().frame(0);
		ActionHandler.wait(5);
		VerifyHandler.verifyElementPresent(SVOdashboardsContainer.DashBoardViewingAsUser);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Dash Board viewing as user details are updated and displayed on Performance dashboard");

		DashBoardViewingAs = SVOdashboardsContainer.DashBoardViewingAsUser.getText();
		Reporter.addStepLog("Classic Consent dashboard of perfromance folder is " + DashBoardViewingAs);

	}

	@Then("^Verify that all the details of classic consent dashboard are displayed$")
	public void verify_that_all_the_details_of_classic_consent_dashboard_are_displayed() throws Throwable {

		VerifyHandler.verifyElementPresent(SVOdashboardsContainer.ConsentedToEmailDetails);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Consented To Email Marketing Details are displayed on Performance Dashboard");

		VerifyHandler.verifyElementPresent(SVOdashboardsContainer.ConsentedToPhoneMarketingDetails);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Consented To Phone Marketing Details are displayed on Performance Dashboard");

		VerifyHandler.verifyElementPresent(SVOdashboardsContainer.ConsentedToSMSMarketingDetails);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Consented To SMS Marketing Details are displayed on Performance Dashboard");

		VerifyHandler.verifyElementPresent(SVOdashboardsContainer.ConsentedToPostMarketingDetails);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Consented To Post Marketing Details are displayed on Performance Dashboard");
		driver.switchTo().parentFrame();
	}

	@When("^User is not able to view dashboards tab on Home Page of SVO portal$")
	public void User_is_not_able_to_view_dashboards_tab_on_Home_Page_of_SVO_portal() throws Throwable {
		if (VerifyHandler.verifyElementPresent(SVOdashboardsContainer.DashboardsTab)) {
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User is able to view dashboards tab on Home Page of SVO portal");
		} else {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User is not able to view dashboards tab on Home Page of SVO portal");
		}
	}

	@Then("^Verify that Last Modified data is visible on Dashboards page$")
	public void verify_that_Last_Modified_data_is_visible_on_Dashboards_page() throws Throwable {

		driver.switchTo().frame(0);
		ActionHandler.wait(5);
		VerifyHandler.verifyElementPresent(SVOdashboardsContainer.LastModifiedData);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Last modified data of DashBoard is updated and displayed on Performance dashboard");

		DashBoardLastModifiedData = SVOdashboardsContainer.LastModifiedData.getText();
		Reporter.addStepLog("Last modified data of Classic Consent dashboard is " + DashBoardLastModifiedData);

		driver.switchTo().parentFrame();
	}

	@Then("^User is able to create new dashboard in special operation overview folder Folder Name \"([^\"]*)\" Username \"([^\"]*)\"$")
	public void user_is_able_to_create_new_dashboard_in_special_operation_overview_folder_Folder_Name_Username(
			String foldername, String Usersearch) throws Throwable {

		String fn[] = foldername.split(",");
		foldername = CommonFunctions.readExcelMasterData(fn[0], fn[1], fn[2]);

		String US[] = Usersearch.split(",");
		Usersearch = CommonFunctions.readExcelMasterData(US[0], US[1], US[2]);

		ActionHandler.click(SVOdashboardsContainer.NewDashboardBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on new dashboard button");

		String DashboardName = "New dashboard_";
		String Description = "Test";
		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		driver.switchTo().frame(SVOdashboardsContainer.CopyDashboardframe);
		ActionHandler.click(SVOdashboardsContainer.DashboardNameBox);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(SVOdashboardsContainer.DashboardNameBox, DashboardName + randomNumber);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOdashboardsContainer.DashboardDescriptionBox, Description);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOdashboardsContainer.SelectFolderBtn);
		CommonFunctions.attachScreenshot();

		driver.switchTo().parentFrame();
		ActionHandler.click(SVOdashboardsContainer.FolderSearchBox);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(SVOdashboardsContainer.FolderSearchBox, foldername);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOdashboardsContainer.SelectFolder);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOdashboardsContainer.SelectFolderBtn1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects special operations overview as folder name");

		driver.switchTo().frame(SVOdashboardsContainer.Dashboardframe);
		ActionHandler.click(SVOdashboardsContainer.CreateBtn);
		CommonFunctions.attachScreenshot();

		driver.switchTo().parentFrame();
		driver.navigate().refresh();
		ActionHandler.wait(15);
		driver.switchTo().frame(SVOdashboardsContainer.Dashboardframe);

		ActionHandler.click(SVOdashboardsContainer.EditDashboardPropertiesBtn);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOdashboardsContainer.AnotherPersonRadioBtn);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOdashboardsContainer.CancelAdminUser);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOdashboardsContainer.AnotherPersonSearchBox, Usersearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		ActionHandler.click(SVOdashboardsContainer.SelectUsername);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOdashboardsContainer.SaveBtn);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOdashboardsContainer.DoneBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects kelsey chand user as viewing user");

		ActionHandler.click(SVOdashboardsContainer.SaveDashboard);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User create new dashboard on dashboard page");

		VerifyHandler.verifyElementPresent(SVOdashboardsContainer.NewDashboardName);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to view new dashboard name on dashboard page");

		NewDashboardName = SVOdashboardsContainer.NewDashboardName.getText();
		Reporter.addStepLog("New dashboard name is " + NewDashboardName);

		VerifyHandler.verifyElementPresent(SVOdashboardsContainer.DashBoardViewingAsUser);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Dashboard viewing as user details are updated dashboard page");

		DashBoardViewingAs = SVOdashboardsContainer.DashBoardViewingAsUser.getText();
		Reporter.addStepLog("Dashboard viewer name is " + DashBoardViewingAs);

		driver.switchTo().parentFrame();

	}

	@Then("^User is able to download save and create new dashboard on dashboard page$")
	public void user_is_able_to_download_save_and_create_new_dashboard_on_dashboard_page() throws Throwable {

		driver.switchTo().frame(SVOdashboardsContainer.CopyDashboardframe);
		ActionHandler.wait(5);

		ActionHandler.click(SVOdashboardsContainer.DropdownBtn);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOdashboardsContainer.DashboardDownloadBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User download the dashboard on dashboard page");

		ActionHandler.click(SVOdashboardsContainer.DropdownBtn);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOdashboardsContainer.DashboardSaveAsBtn);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOdashboardsContainer.CreateBtn);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);

		driver.switchTo().parentFrame();
		driver.navigate().refresh();
		ActionHandler.wait(15);

		driver.switchTo().frame(SVOdashboardsContainer.Dashboardframe);
		DashboardCopy = SVOdashboardsContainer.CopyOfDashboard.getText();
		Reporter.addStepLog("Saved copy of Classic Consent dashboard is " + DashboardCopy);
		Reporter.addStepLog("User Saves the copy of dashboard on dashboard page");

		ActionHandler.click(SVOdashboardsContainer.DropdownBtn);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOdashboardsContainer.CreateNewDashboardFromDashboard);
		CommonFunctions.attachScreenshot();

		driver.switchTo().parentFrame();
		driver.navigate().refresh();
		ActionHandler.wait(15);

		driver.switchTo().frame(SVOdashboardsContainer.Dashboardframe);
		String DashboardName = "New dashboard";
		double randomNumber = getRandomIntegerBetweenRange(0, 10000);
		ActionHandler.setText(SVOdashboardsContainer.DashboardNameBox, DashboardName + randomNumber);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOdashboardsContainer.CreateBtn);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);

		driver.switchTo().parentFrame();
		driver.navigate().refresh();
		ActionHandler.wait(15);
		driver.switchTo().frame(SVOdashboardsContainer.Dashboardframe);

		ActionHandler.click(SVOdashboardsContainer.SaveDashboardBtn);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOdashboardsContainer.DoneBtn);
		CommonFunctions.attachScreenshot();

		NewDashboardName = SVOdashboardsContainer.NewDashboardName.getText();
		Reporter.addStepLog("New dashboard name is " + NewDashboardName);
		Reporter.addStepLog("User create new dashboard from dashboard");

		driver.switchTo().parentFrame();
	}

	@Then("^User is not able to edit dashboard which is created by other user$")
	public void user_is_not_able_to_edit_dashboard_which_is_created_by_other_user() throws Throwable {

		if (VerifyHandler.verifyElementPresent(SVOdashboardsContainer.DashboardsTab)) {
			javaScriptUtil.clickElementByJS(SVOdashboardsContainer.DashboardsTab);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User Navigate to Dashboards page");
		} else {
			ActionHandler.click(SVOdashboardsContainer.MoreButton);
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User Click on More button");

			javaScriptUtil.clickElementByJS(SVOdashboardsContainer.DashBoardDropDownTab);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User Selects dashboard tab from 'More' drop down list");

		}

		ActionHandler.click(SVOdashboardsContainer.AllDashBoardsfolder);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigated to All Dashboards folder");

		driver.navigate().refresh();
		ActionHandler.wait(15);

		ActionHandler.click(SVOdashboardsContainer.DashboardSearchBox);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(SVOdashboardsContainer.DashboardSearchBox, NewDashboardName);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);

		VerifyHandler.verifyElementPresent(SVOdashboardsContainer.DashBoardCreatedByUser);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Dash Board created by user details are updated and displayed on dashboard page");

		DashBoardCreatedBy = SVOdashboardsContainer.DashBoardCreatedByUser.getText();
		Reporter.addStepLog("Dashboard is created by user  = " + DashBoardCreatedBy);

		javaScriptUtil.clickElementByJS(SVOdashboardsContainer.NewDashboardName);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects dashboards which is created by other user");

		driver.switchTo().frame(0);
		ActionHandler.wait(5);

		if (VerifyHandler.verifyElementPresent(SVOdashboardsContainer.EditBtn)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User is able to edit dashboard which is created by other user");
		} else {
			Reporter.addStepLog("User is not able to edit dashboard which is created by other user");
		}
		driver.switchTo().parentFrame();

	}

}
