package com.jlr.svo.tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.containers.SSEC_1226_CaseCreationEmail_Container;
import com.jlr.svo.containers.SSEC_2119_AdditionalVehicleName_AutoNumberContainer;
import com.jlr.svo.containers.SSEC_2221_Mass_upload_ETracker_URL_AdditionalVehicleContainer;
import com.jlr.svo.containers.SSEC_2237_Continuation_Work_OrdersContainer;
import com.jlr.svo.containers.SSEC_2327_ContinuationWorkOrder_SectionAlignmentContainer;
import com.jlr.svo.containers.SVOAccountsContainer;
import com.jlr.svo.containers.SVOAdditionalVehicleContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.containers.SVOItemToApproveContainer;
import com.jlr.svo.containers.SVOOpportunityContainer;
import com.jlr.svo.containers.SVO_CustomerResponsesContainer;
import com.jlr.svo.containers.SVO_EnquiryLostReasonContainer;
import com.jlr.svo.containers.SVO_RestrictedPartyScreeningContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.Then;

public class SSEC_2327_ContinuationWordOrders_SectionAlignmentTest extends TestBaseCC {
	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory.getLogger(SSEC_1226_CaseCreationEmail_Test.class.getName());
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);

	SVOAccountsContainer SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
	SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	SVOAdditionalVehicleContainer SVOAdditionalvehicleContainer = PageFactory.initElements(driver,
			SVOAdditionalVehicleContainer.class);
	SVO_RestrictedPartyScreeningContainer RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
			SVO_RestrictedPartyScreeningContainer.class);
	SVOItemToApproveContainer SVOItemToapproveContainer = PageFactory.initElements(driver,
			SVOItemToApproveContainer.class);
	SVO_CustomerResponsesContainer SVO_CustomerResponsesContainer = PageFactory.initElements(driver,
			SVO_CustomerResponsesContainer.class);
	SVO_EnquiryLostReasonContainer SVO_EnquiryLostReasonContainer = PageFactory.initElements(driver,
			SVO_EnquiryLostReasonContainer.class);
	SSEC_1226_CaseCreationEmail_Container CaseCreationEmailContainer = PageFactory.initElements(driver,
			SSEC_1226_CaseCreationEmail_Container.class);
	SSEC_2119_AdditionalVehicleName_AutoNumberContainer AdditionalVehicleName_AutoNumberContainer = PageFactory.initElements(driver, SSEC_2119_AdditionalVehicleName_AutoNumberContainer.class);
	SSEC_2221_Mass_upload_ETracker_URL_AdditionalVehicleContainer Mass_upload_ETracker_URL_AdditionalVehicleContainer = PageFactory.initElements(driver, SSEC_2221_Mass_upload_ETracker_URL_AdditionalVehicleContainer.class);
	SSEC_2237_Continuation_Work_OrdersContainer Continuation_Work_OrdersContainer = PageFactory.initElements(driver, SSEC_2237_Continuation_Work_OrdersContainer.class);
	com.jlr.svo.containers.SSEC_2327_ContinuationWorkOrder_SectionAlignmentContainer SSEC_2327_ContinuationWorkOrder_SectionAlignmentContainer = PageFactory.initElements(driver, SSEC_2327_ContinuationWorkOrder_SectionAlignmentContainer.class);
	
	public static String verificationCode;
	public static String vCode;
	public static String veriCode;
	public String NewWindow;
	public static String CaseCreated;
	public static String CaseNumber;

	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
		
	}

	public void onStart() {

		setupTest("SVOTest");
		driver = getDriver();

		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
		SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
		SVOAdditionalvehicleContainer = PageFactory.initElements(driver, SVOAdditionalVehicleContainer.class);

		RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
				SVO_RestrictedPartyScreeningContainer.class);
		SVOItemToapproveContainer = PageFactory.initElements(driver, SVOItemToApproveContainer.class);
		SVO_CustomerResponsesContainer = PageFactory.initElements(driver, SVO_CustomerResponsesContainer.class);
		SVO_EnquiryLostReasonContainer = PageFactory.initElements(driver, SVO_EnquiryLostReasonContainer.class);
		CaseCreationEmailContainer = PageFactory.initElements(driver, SSEC_1226_CaseCreationEmail_Container.class);
		AdditionalVehicleName_AutoNumberContainer = PageFactory.initElements(driver, SSEC_2119_AdditionalVehicleName_AutoNumberContainer.class);
		Mass_upload_ETracker_URL_AdditionalVehicleContainer = PageFactory.initElements(driver, SSEC_2221_Mass_upload_ETracker_URL_AdditionalVehicleContainer.class);
		Continuation_Work_OrdersContainer = PageFactory.initElements(driver, SSEC_2237_Continuation_Work_OrdersContainer.class);
		SSEC_2327_ContinuationWorkOrder_SectionAlignmentContainer = PageFactory.initElements(driver, SSEC_2327_ContinuationWorkOrder_SectionAlignmentContainer.class);
		
		driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);

		verificationCode = null;
		vCode = null;
		veriCode = null;
		NewWindow = null;
		CaseCreated = null;
		CaseNumber = null;

	}
	
    @Then("^Verify Opportunity Record Type and Owner$")
    public void Verify_Oppportunity_Record_Type_and_Owner() throws Exception{
    	
    	ActionHandler.pageDown();
    	
    	String OpportunityRecordType = SSEC_2327_ContinuationWorkOrder_SectionAlignmentContainer.OpportunityRecordType.getText();
        ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Verify Opportunity Record Type");
        
    	String OpportunityOwner = SSEC_2327_ContinuationWorkOrder_SectionAlignmentContainer.OpportunityOwner.getText();
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Verify Opportunity Owner");
    }
    
    @Then("^Verify user is able to edit Vehicle section$")
    public void Verify_user_able_to_edit_Vehicle_section() throws Exception{
    	
    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	
    	ActionHandler.click(SSEC_2327_ContinuationWorkOrder_SectionAlignmentContainer.EditProductionYear);
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Click on Edit Producion Year button");
        
    	ActionHandler.click(SSEC_2327_ContinuationWorkOrder_SectionAlignmentContainer.ProductionYearTextBox);
    	ActionHandler.wait(1);
    	ActionHandler.setText(SSEC_2327_ContinuationWorkOrder_SectionAlignmentContainer.ProductionYearTextBox, "9000");
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Click on Production Year textbox");
        
    	ActionHandler.click(Continuation_Work_OrdersContainer.SaveEditOpportunity);
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Click on  Save Edit Opportunity button");
    	
    }

    @Then("^Verify user is able to edit Donor section$")
    public void Verify_user_able_to_edit_Donor_section() throws Exception{
    	
    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	
    	ActionHandler.click(Continuation_Work_OrdersContainer.EditVehicleDonorSection);
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Click on Edit Vehicle button in Donor section");
        
    	ActionHandler.click(Continuation_Work_OrdersContainer.SearchVehicleOpportunityDonor);
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Click on Search Vehicle Opportunity");
        
    	ActionHandler.click(Continuation_Work_OrdersContainer.SaveEditOpportunity);
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Click on Save Edit Opportunity button");
        
    	
    }
    
    @Then("^Verify that Donor section is present after Vehicle section$")
    public void Verify_Donor_section_is_present_after_Vehicle_section() throws Exception{
    	
    	VerifyHandler.verifyElementPresent(Continuation_Work_OrdersContainer.VehicleDetails);
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Verify Vehicle Details section");
        
    	VerifyHandler.verifyElementPresent(Continuation_Work_OrdersContainer.DonorDetails);
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Verify Donor Section");
        
    }
    
    @Then("^Verify that user is able to Mark Stage as Complete$")
    public void Verify_user_able_to_Mark_Stage_as_Complete() throws Exception{
    	
    	ActionHandler.click(Continuation_Work_OrdersContainer.MarkStageCompleteBtn);
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Click on Mark Stage as Complete button");
    	
    }






	
}
