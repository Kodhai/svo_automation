package com.jlr.svo.tests;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.autotest.utils.Config;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.constants.Constants;
import com.jlr.svo.containers.SSEC_1225_CaseCreationWebtoCaseContainer;
import com.jlr.svo.containers.SSEC_1226_CaseCreationEmail_Container;
import com.jlr.svo.containers.SSEC_1227_CasesCreationContainer;
import com.jlr.svo.containers.SSEC_1228_LinkContactAndAccountTocase_Container;
import com.jlr.svo.containers.SSEC_1234_CaseListViews_Container;
import com.jlr.svo.containers.SVOAccountsContainer;
import com.jlr.svo.containers.SVOAdditionalVehicleContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.containers.SVOItemToApproveContainer;
import com.jlr.svo.containers.SVOOpportunityContainer;
import com.jlr.svo.containers.SVO_CustomerResponsesContainer;
import com.jlr.svo.containers.SVO_EnquiryLostReasonContainer;
import com.jlr.svo.containers.SVO_RestrictedPartyScreeningContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class SSEC_1225_CaseCreationWebToCase extends TestBaseCC {
	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory
			.getLogger(SSEC_1228_LinkContactAndAccountTocase_Test.class.getName());
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);

	SVOAccountsContainer SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
	SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	SVOAdditionalVehicleContainer SVOAdditionalvehicleContainer = PageFactory.initElements(driver,
			SVOAdditionalVehicleContainer.class);
	SVO_RestrictedPartyScreeningContainer RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
			SVO_RestrictedPartyScreeningContainer.class);
	SVOItemToApproveContainer SVOItemToapproveContainer = PageFactory.initElements(driver,
			SVOItemToApproveContainer.class);
	SVO_CustomerResponsesContainer SVO_CustomerResponsesContainer = PageFactory.initElements(driver,
			SVO_CustomerResponsesContainer.class);
	SVO_EnquiryLostReasonContainer SVO_EnquiryLostReasonContainer = PageFactory.initElements(driver,
			SVO_EnquiryLostReasonContainer.class);
	SSEC_1226_CaseCreationEmail_Container CaseCreationEmailContainer = PageFactory.initElements(driver,
			SSEC_1226_CaseCreationEmail_Container.class);
	SSEC_1228_LinkContactAndAccountTocase_Container LinkContactAndAccountTocaseContainer = PageFactory
			.initElements(driver, SSEC_1228_LinkContactAndAccountTocase_Container.class);
	SSEC_1234_CaseListViews_Test CaseListViews_Test = PageFactory.initElements(driver,
			SSEC_1234_CaseListViews_Test.class);
	SSEC_1234_CaseListViews_Container CaseListViewsContainer = PageFactory.initElements(driver,
			SSEC_1234_CaseListViews_Container.class);
	SSEC_1225_CaseCreationWebtoCaseContainer CaseCreationWebtoCaseContainer = PageFactory.initElements(driver,
			SSEC_1225_CaseCreationWebtoCaseContainer.class);
	


	SSEC_1227_CasesCreationContainer CasesContainer = PageFactory.initElements(driver,
			SSEC_1227_CasesCreationContainer.class);

	public static String verificationCode;
	public static String vCode;
	public static String veriCode;
	public String NewWindow;
	public static String CaseCreated;
	public static String CaseNumber;
	public static String ContactName;

	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	public void onStart() {

		setupTest("SVOTest");
		driver = getDriver();

		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
		SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
		SVOAdditionalvehicleContainer = PageFactory.initElements(driver, SVOAdditionalVehicleContainer.class);

		RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
				SVO_RestrictedPartyScreeningContainer.class);
		SVOItemToapproveContainer = PageFactory.initElements(driver, SVOItemToApproveContainer.class);
		SVO_CustomerResponsesContainer = PageFactory.initElements(driver, SVO_CustomerResponsesContainer.class);
		SVO_EnquiryLostReasonContainer = PageFactory.initElements(driver, SVO_EnquiryLostReasonContainer.class);
		CaseCreationEmailContainer = PageFactory.initElements(driver, SSEC_1226_CaseCreationEmail_Container.class);
		LinkContactAndAccountTocaseContainer = PageFactory.initElements(driver,
				SSEC_1228_LinkContactAndAccountTocase_Container.class);
		CaseListViewsContainer = PageFactory.initElements(driver, SSEC_1234_CaseListViews_Container.class);
		CaseListViews_Test = PageFactory.initElements(driver, SSEC_1234_CaseListViews_Test.class);
		CaseCreationWebtoCaseContainer = PageFactory.initElements(driver,
				SSEC_1225_CaseCreationWebtoCaseContainer.class);
		CasesContainer = PageFactory.initElements(driver, SSEC_1227_CasesCreationContainer.class);
		

		driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);

		verificationCode = null;
		vCode = null;
		veriCode = null;
		NewWindow = null;
		CaseCreated = null;
		CaseNumber = null;
		ContactName = null;

	}
	
	
	
	
	
	

	@Then("^Submit Contact Us form with Email \"([^\"]*)\" First name \"([^\"]*)\" Last name \"([^\"]*)\" Country \"([^\"]*)\" Phone number \"([^\"]*)\" Enquiry Type \"([^\"]*)\" VIN number \"([^\"]*)\"$")
	public void Submit_Contact_Us_form(String email, String first, String last, String country, String phone,
			String enquiryType, String vin) throws Throwable {

		String em[] = email.split(",");
		email = CommonFunctions.readExcelMasterData(em[0], em[1], em[2]);

		String fname[] = first.split(",");
		first = CommonFunctions.readExcelMasterData(fname[0], fname[1], fname[2]);

		String lname[] = last.split(",");
		last = CommonFunctions.readExcelMasterData(lname[0], lname[1], lname[2]);

		String cty[] = country.split(",");
		country = CommonFunctions.readExcelMasterData(cty[0], cty[1], cty[2]);

		String num[] = phone.split(",");
		phone = CommonFunctions.readExcelMasterData(num[0], num[1], num[2]);

		String enq[] = enquiryType.split(",");
		enquiryType = CommonFunctions.readExcelMasterData(enq[0], enq[1], enq[2]);

		String v[] = vin.split(",");
		vin = CommonFunctions.readExcelMasterData(v[0], v[1], v[2]);

		// enter first name
		ActionHandler.setText(CaseCreationWebtoCaseContainer.FirstNameTxtBx, first);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseCreationWebtoCaseContainer.FirstNameTxtBx, first);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters First name as : " + first);

		// enter last name
		ActionHandler.setText(CaseCreationWebtoCaseContainer.LastNameTxtBx, last);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Last name as : " + last);

		ActionHandler.wait(1);
		ActionHandler.setText(CaseCreationWebtoCaseContainer.EmailTxtBox, email);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseCreationWebtoCaseContainer.EmailTxtBox, email);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters email as : " + email);

		// enter phone number
		ActionHandler.setText(CaseCreationWebtoCaseContainer.PhoneNumberTxtBx, phone);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		// select the appropriate enquiry type
		ActionHandler.selectByVisibleText(CaseCreationWebtoCaseContainer.HelpSelect, enquiryType);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Help select");

		// enter country
		ActionHandler.setText(CaseCreationWebtoCaseContainer.CountrySelection, country);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		ActionHandler.pressEnter();
		ActionHandler.wait(1);
		Reporter.addStepLog("User enters country as : " + country);

		ActionHandler.setText(CaseCreationWebtoCaseContainer.VINNumber, vin);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters the vin number");

		String comment = "test";
		ActionHandler.setText(CaseCreationWebtoCaseContainer.CommentText, comment);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters the comment text");

		// click on continue
		javaScriptUtil.clickElementByJS(CaseCreationWebtoCaseContainer.SubmitBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Submit");

		VerifyHandler.verifyElementPresent(CaseCreationWebtoCaseContainer.SubmitSuccessfullMessage);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies that contact us form submitted successfully");
	}

	@Then("^Submit Contact Us form with Email \"([^\"]*)\" First name \"([^\"]*)\" Last name \"([^\"]*)\" Country \"([^\"]*)\" Enquiry Type \"([^\"]*)\" VIN number \"([^\"]*)\"$")
	public void Submit_Contact_Us_form(String email, String first, String last, String country, String enquiryType,
			String vin) throws Throwable {

		String em[] = email.split(",");
		email = CommonFunctions.readExcelMasterData(em[0], em[1], em[2]);

		String fname[] = first.split(",");
		first = CommonFunctions.readExcelMasterData(fname[0], fname[1], fname[2]);

		String lname[] = last.split(",");
		last = CommonFunctions.readExcelMasterData(lname[0], lname[1], lname[2]);

		String cty[] = country.split(",");
		country = CommonFunctions.readExcelMasterData(cty[0], cty[1], cty[2]);

		String enq[] = enquiryType.split(",");
		enquiryType = CommonFunctions.readExcelMasterData(enq[0], enq[1], enq[2]);

		String v[] = vin.split(",");
		vin = CommonFunctions.readExcelMasterData(v[0], v[1], v[2]);

		// enter first name
		ActionHandler.setText(CaseCreationWebtoCaseContainer.FirstNameTxtBx, first);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseCreationWebtoCaseContainer.FirstNameTxtBx, first);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters First name as : " + first);

		// enter last name
		ActionHandler.setText(CaseCreationWebtoCaseContainer.LastNameTxtBx, last);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Last name as : " + last);

		ActionHandler.wait(1);
		ActionHandler.setText(CaseCreationWebtoCaseContainer.EmailTxtBox, email);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseCreationWebtoCaseContainer.EmailTxtBox, email);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters email as : " + email);

		// select the appropriate enquiry type
		ActionHandler.selectByVisibleText(CaseCreationWebtoCaseContainer.HelpSelect, enquiryType);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Help select");

		// enter country
		ActionHandler.setText(CaseCreationWebtoCaseContainer.CountrySelection, country);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		ActionHandler.pressEnter();
		ActionHandler.wait(1);
		Reporter.addStepLog("User enters country as : " + country);

		ActionHandler.setText(CaseCreationWebtoCaseContainer.VINNumber, vin);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters the vin number");

		String comment = "test";
		ActionHandler.setText(CaseCreationWebtoCaseContainer.CommentText, comment);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters the comment text");

		// click on continue
		javaScriptUtil.clickElementByJS(CaseCreationWebtoCaseContainer.SubmitBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Submit");

		VerifyHandler.verifyElementPresent(CaseCreationWebtoCaseContainer.SubmitSuccessfullMessage);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies that contact us form submitted successfully");
	}

	@Then("^Verify that error message while submitting contact us form$")
	public void Verify_that_error_message_while_submitting_contact_us_form() throws Throwable {

		VerifyHandler.verifyElementPresent(CaseCreationWebtoCaseContainer.ContactUsErrorMessage);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"User verifies that error message while submitting contact us form without mandatory fields");
	}

	@And("^User navigates to the Enquiry tab and validate that no new enquiry has been created$")
	public void user_navigates_to_the_Enquiries_tab() throws Throwable {

		javaScriptUtil.clickElementByJS(SVO_EnquiryLostReasonContainer.EnquiriesTab);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigates to Enquiries tab");

	}

	@Then("^Navigate to contact us section$")
	public void Navigate_to_contact_us_section() throws Throwable {

		ActionHandler.wait(2);
		ActionHandler.click(CaseCreationWebtoCaseContainer.contantUsHeader);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks the Contact us button");
	}

	@Then("^close the browser$")
	public void close_the_browser() throws Throwable {

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Logout from BG Portal");
		driver.quit();
	}

	public String checkEmailForClassicPartsCaseUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(493, 499);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	@Then("^User Access to SVO Portal as Classic Parts Technical user with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void Access_SVO_Portal_Classic_Parts_tech_with_username_password(String userName, String password)
			throws Exception {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		Reporter.addStepLog("User access SF SVO Portal");

		onStart();
		driver = getDriver();
		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailForClassicPartsCaseUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	@Then("^Search the Case number in the Search box$")
	public void Search_case_number_in_search() throws Exception {

		ActionHandler.setText(CaseCreationWebtoCaseContainer.SearchBox, CaseNumber);
		ActionHandler.wait(5);
		ActionHandler.click(CaseCreationWebtoCaseContainer.CaseSearch);
		ActionHandler.wait(5);

	}

	@And("^Verify the Case Origin Case Owner and Web Email$")
	public void verify_case_origin_case_owner_web_email() throws Exception {

		if (VerifyHandler.verifyTitleContainsText("Web")) {
			CommonFunctions.attachScreenshot();
		}

		if (VerifyHandler.verifyElementPresent(CaseCreationWebtoCaseContainer.CaseOwnerBox)) {
			CommonFunctions.attachScreenshot();
		}

		if (VerifyHandler.verifyElementPresent(CaseCreationWebtoCaseContainer.WebEmailBox)) {
			CommonFunctions.attachScreenshot();
		}

	}

	@And("^user logout from the portal$")
	public void user_logout_from_portal() throws Exception {

		ActionHandler.click(CasesContainer.ProfileBtn);
		ActionHandler.wait(1);
		ActionHandler.click(CasesContainer.LogoutBtn);
		ActionHandler.wait(1);
	}

}