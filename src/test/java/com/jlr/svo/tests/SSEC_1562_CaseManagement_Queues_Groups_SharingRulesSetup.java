package com.jlr.svo.tests;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;

import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.autotest.utils.Config;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.constants.Constants;
import com.jlr.svo.containers.SSEC_1227_CasesCreationContainer;
import com.jlr.svo.containers.SSEC_1229_Case_QueuesContainer;
import com.jlr.svo.containers.SSEC_1237_CaseClosureContainer;
import com.jlr.svo.containers.SSEC_1532_AdditionOfMarketDataToKMIContainer;
import com.jlr.svo.containers.SSEC_1562_CaseManagement_Queues_Groups_SharingRulesSetupContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class SSEC_1562_CaseManagement_Queues_Groups_SharingRulesSetup extends TestBaseCC {

	private WebDriver driver = getDriver();
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SSEC_1227_CasesCreationContainer CasesContainer = PageFactory.initElements(driver,
			SSEC_1227_CasesCreationContainer.class);
	SSEC_1237_CaseClosureContainer CaseClosureContainer = PageFactory.initElements(driver,
			SSEC_1237_CaseClosureContainer.class);
	SSEC_1229_Case_QueuesContainer Case_QueuesContainer = PageFactory.initElements(driver, SSEC_1229_Case_QueuesContainer.class); 
	SSEC_1562_CaseManagement_Queues_Groups_SharingRulesSetupContainer CaseManagement_Queues_Groups_SharingRulesSetupContainer = PageFactory.initElements(driver, SSEC_1562_CaseManagement_Queues_Groups_SharingRulesSetupContainer.class); 
	SSEC_1532_AdditionOfMarketDataToKMIContainer AdditionOfMarketDataToKMI = PageFactory.initElements(driver,
			SSEC_1532_AdditionOfMarketDataToKMIContainer.class);
	
	public static String verificationCode;
	public static String vCode;
	int randomNum = getRandomNumber(0, 10000);
	String firstName = "Test" + randomNum;
	String lastName = "tes" + randomNum;
	String VINNum = "Test";
	String Chassis = "T" + randomNum;
	String Chassis2 = VINNum + randomNum;

	public static int getRandomNumber(int min, int max) {
		int x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	// onStart
	public void onStart() {
		setupTest("SVOTest");
		driver = getDriver();
		CasesContainer = PageFactory.initElements(driver, SSEC_1227_CasesCreationContainer.class);
		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		Case_QueuesContainer = PageFactory.initElements(driver, SSEC_1229_Case_QueuesContainer.class);
		CaseClosureContainer = PageFactory.initElements(driver, SSEC_1237_CaseClosureContainer.class);
		CaseManagement_Queues_Groups_SharingRulesSetupContainer = PageFactory.initElements(driver, SSEC_1562_CaseManagement_Queues_Groups_SharingRulesSetupContainer.class);
		AdditionOfMarketDataToKMI = PageFactory.initElements(driver,
				SSEC_1532_AdditionOfMarketDataToKMIContainer.class);
		verificationCode = null;

		driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);


	}
	
	
	public String checkEmailForClassicSalesUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(513, 519);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}
	
	
	@Given("^Access to SVO portal as Username \"([^\"]*)\" and password \"([^\"]*)\"$")
	public void Access_SVO_Portal_Classic_Sales_with_username_password(String userName, String password)
			throws Exception {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		Reporter.addStepLog("User access SF SVO Portal");

		onStart();
		driver = getDriver();
		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailForClassicSalesUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

    @And("^user navigate to Cases tab$")
    public void user_navigates_Cases_tab() throws Exception{
    	
    	javaScriptUtil.clickElementByJS(CasesContainer.CasesTab);
		ActionHandler.wait(5);
    }
    
    @Then("^User creates Classic Sales record type with mandatory fields like first name$")
    public void user_creates_Classic_Sales_record_type() throws Exception{
    	
    	ActionHandler.click(CaseClosureContainer.NewBtn);
		ActionHandler.wait(2);
		
		ActionHandler.click(CaseManagement_Queues_Groups_SharingRulesSetupContainer.ClassicSalesRecType);
		ActionHandler.wait(2);
		
		ActionHandler.click(CasesContainer.NextBtn);
		ActionHandler.wait(3);
		
		ActionHandler.setText(CasesContainer.FirstNameBtn, firstName);
		ActionHandler.wait(2);
		
    }

    @Then("^User saves the new case$")
	public void User_saves_the_customer_case() throws Exception {

		ActionHandler.click(Case_QueuesContainer.SaveBtn);
		ActionHandler.wait(3);
	}
    
    @And("^Verify that Case Owner is Classic Sales Queue$")
    public void verify_case_owner_value_as_ClassicSales_queue() throws Exception{
    	
    	ActionHandler.pageScrollDown();
    	ActionHandler.scrollToView(Case_QueuesContainer.CaseInfoText);
    	VerifyHandler.verifyElementHasFocus(Case_QueuesContainer.CaseOwnerTitle);
    	
    	if(VerifyHandler.verifyElementPresent(CaseManagement_Queues_Groups_SharingRulesSetupContainer.COClassicSales)) {
    		CommonFunctions.attachScreenshot();
    	}
    }
    	
    @Then("^user logs a call on the activity tab$")
    public void user_logs_call_to_activity_tab() throws Exception{
    	
    	ActionHandler.pageCompleteScrollUp();
    	ActionHandler.wait(2);
    	ActionHandler.click(AdditionOfMarketDataToKMI.ActivityTab);
		ActionHandler.wait(2);

		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);
		ActionHandler.click(AdditionOfMarketDataToKMI.AddBtn);
		ActionHandler.wait(3);

		ActionHandler.click(AdditionOfMarketDataToKMI.SubjectText);
		ActionHandler.wait(2);

		Actions act1 = new Actions(driver);
		ActionHandler.setText(AdditionOfMarketDataToKMI.callActivityText, "Call");
		ActionHandler.wait(3);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		act1.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.pressEnter();
		ActionHandler.wait(3);

		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);
		ActionHandler.click(AdditionOfMarketDataToKMI.ActivitySaveBtn);
		ActionHandler.wait(3);
    }
    
    @Then("^User closes the case$")
	public void Mark_As_Close() throws Exception {

		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(3);

		ActionHandler.click(CaseManagement_Queues_Groups_SharingRulesSetupContainer.MarkCompleteBtn);
		ActionHandler.wait(2);

		ActionHandler.click(CaseManagement_Queues_Groups_SharingRulesSetupContainer.MarkCompleteBtn);
		ActionHandler.wait(3);

		ActionHandler.click(CaseManagement_Queues_Groups_SharingRulesSetupContainer.MarkCurrentStatusBtn);
		ActionHandler.wait(5);

	}
    
    @And("^User logouts from the SVO portal$")
    public void user_logouts_from_portal() throws Exception{
    	
    	ActionHandler.click(CasesContainer.ProfileBtn);
		ActionHandler.wait(2);
		
		ActionHandler.click(CasesContainer.LogoutBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Logout");
    }

    
    @Given("^Access to SVO portal as username \"([^\"]*)\" and password \"([^\"]*)\"$")
	public void Access_SVO_Portal_Classic_Service_with_username_password(String userName, String password)
			throws Exception {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		Reporter.addStepLog("User access SF SVO Portal");

		onStart();
		driver = getDriver();
		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailForClassicServiceUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}
    
    public String checkEmailForClassicServiceUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(515, 521);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}
    
    @Then("^User creates Classic Service record type with mandatory fields like first name$")
    public void user_creates_new_Classic_Service_record_type() throws Exception{
    	
    	ActionHandler.click(CaseClosureContainer.NewBtn);
		ActionHandler.wait(2);
		
		ActionHandler.click(CaseManagement_Queues_Groups_SharingRulesSetupContainer.ClassicServiceRecType);
		ActionHandler.wait(2);
		
		ActionHandler.click(CasesContainer.NextBtn);
		ActionHandler.wait(3);
		
		ActionHandler.setText(CasesContainer.FirstNameBtn, firstName);
		ActionHandler.wait(2);
    }

    @And("^Verify that Case Owner is Classic Service Queue$")
    public void verify_Case_owner_Classic_Service_Queue() throws Exception{
    	
    	ActionHandler.pageScrollDown();
    	ActionHandler.scrollToView(Case_QueuesContainer.CaseInfoText);
    	VerifyHandler.verifyElementHasFocus(Case_QueuesContainer.CaseOwnerTitle);
    	
    	if(VerifyHandler.verifyElementPresent(CaseManagement_Queues_Groups_SharingRulesSetupContainer.COClassicService)) {
    		CommonFunctions.attachScreenshot();
    	}
    }
    	
    @And("^User verify that All Classic Service queues are dispalyed in Cases tab$")
    public void user_verify_All_Classic_Service_queues_is_displayed() throws Exception{
    	
    	javaScriptUtil.clickElementByJS(CasesContainer.CasesTab);
		ActionHandler.wait(3);
		
		ActionHandler.click(Case_QueuesContainer.RecViewedCases);
		ActionHandler.wait(3);
		
		ActionHandler.pageDown();
		ActionHandler.wait(2);
		ActionHandler.click(CaseManagement_Queues_Groups_SharingRulesSetupContainer.AllClassicServ);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
        Reporter.addStepLog("All Classic Sales queue is displayed");
    }
    
    @And("^User verify that All Classic Sales queues are dispalyed in Cases tab$")
    public void verify_All_Classic_Sales_Queue_is_displayed() throws Exception{
    	
    	javaScriptUtil.clickElementByJS(CasesContainer.CasesTab);
		ActionHandler.wait(3);
		
		ActionHandler.click(Case_QueuesContainer.RecViewedCases);
		ActionHandler.wait(3);
		
		ActionHandler.setText(CaseManagement_Queues_Groups_SharingRulesSetupContainer.CaseDDSearchbox, "All Classic Sales");
        ActionHandler.wait(3);

		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
        ActionHandler.pressEnter();
        ActionHandler.wait(2);
        
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("All Classic Sales queue is displayed");
    }
    
    @And("^User edits the Case Owner from Classic Sales Queue to Classic Service Queue$")
    public void User_edits_Case_owner() throws Exception{
    	
    	ActionHandler.pageCompleteScrollUp();
    	ActionHandler.wait(2);

    	ActionHandler.click(Case_QueuesContainer.EditChangeOwner);
    	ActionHandler.wait(2);
    	
    	ActionHandler.click(Case_QueuesContainer.CaseOwnerDD);
    	ActionHandler.wait(3);
    	
    	ActionHandler.click(Case_QueuesContainer.QueuesDD);
    	ActionHandler.wait(3);
    	
    	ActionHandler.setText(CaseManagement_Queues_Groups_SharingRulesSetupContainer.SearchQueues, "Classic ");
    	ActionHandler.wait(3);
    	ActionHandler.click(CaseManagement_Queues_Groups_SharingRulesSetupContainer.SearchQueues);
    	ActionHandler.wait(3);
    	
    	Actions act= new Actions(driver);
    	act.sendKeys(Keys.ARROW_DOWN).build().perform();
    	act.sendKeys(Keys.ARROW_DOWN).build().perform();
    	ActionHandler.pressEnter();
    	ActionHandler.wait(3);

    	ActionHandler.click(Case_QueuesContainer.ChangeOwnerBtn);
    	ActionHandler.wait(3);
    }
    
    @And("^User edits the Case Owner from Classic Sales Queue to Classic Service Queue at the top$")
    public void User_edits_Case_owner_at_the_top() throws Exception{
    	
    	ActionHandler.click(CaseManagement_Queues_Groups_SharingRulesSetupContainer.ChangeOwnerOneBtn);
    	ActionHandler.wait(2);
    	
    	ActionHandler.click(Case_QueuesContainer.CaseOwnerDD);
    	ActionHandler.wait(3);
    	
    	ActionHandler.click(Case_QueuesContainer.QueuesDD);
    	ActionHandler.wait(3);
    	
    	ActionHandler.setText(CaseManagement_Queues_Groups_SharingRulesSetupContainer.SearchQueues, "Classic ");
    	ActionHandler.wait(4);
    	ActionHandler.click(CaseManagement_Queues_Groups_SharingRulesSetupContainer.SearchQueues);
    	ActionHandler.wait(3);
    	
    	Actions act= new Actions(driver);
    	act.sendKeys(Keys.ARROW_DOWN).build().perform();
    	act.sendKeys(Keys.ARROW_DOWN).build().perform();
    	ActionHandler.pressEnter();
    	ActionHandler.wait(3);

    	ActionHandler.click(CaseManagement_Queues_Groups_SharingRulesSetupContainer.SubmitBtnCaseChngOwner);
    	ActionHandler.wait(3);
    }
    
    @Then("^User verify that Case Owner is changed to Classic Service Queue$")
    public void user_verify_Case_Owner_as_Classic_Service_Queue() throws Exception{
    	
   	 VerifyHandler.verifyElementPresent(Case_QueuesContainer.CaseOwnerTitle);

   	 if(VerifyHandler.verifyElementPresent(CaseManagement_Queues_Groups_SharingRulesSetupContainer.COClassicService)) {
   		 ActionHandler.wait(2);
   		 CommonFunctions.attachScreenshot();
   	 }
    }
    
    @Then("^User clicks on Change Owner button on Cases page at the top$")
    public void user_clicks_Change_Owner_button_on_Cases_page() throws Exception{
    	
    	ActionHandler.click(CaseManagement_Queues_Groups_SharingRulesSetupContainer.ChangeOwnerBtn);
    	ActionHandler.wait(2);
  	    CommonFunctions.attachScreenshot();
    }

    @And("^User edits the Case Origin as Email from the Cases page$")
    public void user_edits_Case_Origin_as_Email() throws Exception{
    	
//    	 ActionHandler.wait(2);
//         ActionHandler.scrollToView(Case_QueuesContainer.CaseInfoText);
//    	 ActionHandler.wait(3);
//    	 ActionHandler.pageDown();
//    	 ActionHandler.wait(2);
    	 Actions act1 = new Actions(driver);
    	 act1.sendKeys(Keys.ARROW_DOWN).build().perform();
    	 act1.sendKeys(Keys.ARROW_DOWN).build().perform();
    	 act1.sendKeys(Keys.ARROW_DOWN).build().perform();

    	 ActionHandler.scrollToView(CaseManagement_Queues_Groups_SharingRulesSetupContainer.CaseOriginLabel);
    	 ActionHandler.wait(3);

    	 VerifyHandler.verifyElementHasFocus(CaseManagement_Queues_Groups_SharingRulesSetupContainer.CaseOriginLabel);
    	 ActionHandler.wait(3);

    	 ActionHandler.click(CaseManagement_Queues_Groups_SharingRulesSetupContainer.CaseOriginLabel);
    	 ActionHandler.wait(2);
    	 
    	 act1.sendKeys(Keys.ARROW_DOWN).build().perform();
         ActionHandler.pressEnter();
    	 ActionHandler.wait(2);
    	 
    }
    
    @Then("^User saves the edited case$")
    public void user_saves_edited_case() throws Exception{
    	
    	ActionHandler.click(CaseManagement_Queues_Groups_SharingRulesSetupContainer.SaveEditCaseBtn);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    }
    
    @And("^User edits the Case Origin as Web from the Cases page$")
    public void user_edits_Case_Origin_as_Web() throws Exception{
    	
//     ActionHandler.wait(2);
//   	 ActionHandler.scrollToView(Case_QueuesContainer.CaseInfoText);
//   	 ActionHandler.wait(3);
     Actions act1 = new Actions(driver);
   	 act1.sendKeys(Keys.ARROW_DOWN).build().perform();
   	 act1.sendKeys(Keys.ARROW_DOWN).build().perform();
   	 act1.sendKeys(Keys.ARROW_DOWN).build().perform();
   	 
   	 ActionHandler.scrollToView(CaseManagement_Queues_Groups_SharingRulesSetupContainer.CaseOriginLabel);
   	 ActionHandler.wait(3);

   	 VerifyHandler.verifyElementHasFocus(CaseManagement_Queues_Groups_SharingRulesSetupContainer.CaseOriginLabel);
   	 ActionHandler.wait(3);
   	 
   	 ActionHandler.click(CaseManagement_Queues_Groups_SharingRulesSetupContainer.CaseOriginLabel);
	 ActionHandler.wait(2);
	 
	 act1.sendKeys(Keys.ARROW_DOWN).build().perform();
	 act1.sendKeys(Keys.ARROW_DOWN).build().perform();
	 act1.sendKeys(Keys.ARROW_DOWN).build().perform();
	 ActionHandler.pressEnter();
	 ActionHandler.wait(2);

    }
    
    @And("^Click on edit button of Case Owner and verify that no remove option is available$")
    public void verify_case_owner_is_unable_to_remove()throws Exception {
    	
    	ActionHandler.click(Case_QueuesContainer.EditChangeOwner);
    	ActionHandler.wait(3);
    	
    	ActionHandler.click(CaseManagement_Queues_Groups_SharingRulesSetupContainer.CancelCaseOwnerBtn);
    	ActionHandler.wait(3);
        CommonFunctions.attachScreenshot();
    	
    }






    




}
