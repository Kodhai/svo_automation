package com.jlr.svo.tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.containers.SSEC_1039_SVOEnquiryEmailRemaindersContainer;
import com.jlr.svo.containers.SSEC_1503_Clasic_Email_Signature_On_Salesforce_Container;
import com.jlr.svo.containers.SSEC_2157_DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer;
import com.jlr.svo.containers.SSEC_2159_BespokeOrderForm_CreationofOrderFormRecordsContainer;
import com.jlr.svo.containers.SSEC_2285_Design_Brief_FeedbacksContainer;
import com.jlr.svo.containers.SVOAccountsContainer;
import com.jlr.svo.containers.SVOAdditionalVehicleContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.containers.SVOItemToApproveContainer;
import com.jlr.svo.containers.SVOOpportunityContainer;
import com.jlr.svo.containers.SVO_CustomerResponsesContainer;
import com.jlr.svo.containers.SVO_EnquiryLostReasonContainer;
import com.jlr.svo.containers.SVO_RestrictedPartyScreeningContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.And;

public class SSEC_2285_Design_Brief_Amendments extends TestBaseCC {

	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory
			.getLogger(SSEC_1039_SVOEnquiryEmailRemaindersTest.class.getName());
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);

	SVOAccountsContainer SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
	SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	SVOAdditionalVehicleContainer SVOAdditionalvehicleContainer = PageFactory.initElements(driver,
			SVOAdditionalVehicleContainer.class);
	SVO_RestrictedPartyScreeningContainer RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
			SVO_RestrictedPartyScreeningContainer.class);
	SVOItemToApproveContainer SVOItemToapproveContainer = PageFactory.initElements(driver,
			SVOItemToApproveContainer.class);
	SVO_CustomerResponsesContainer SVO_CustomerResponsesContainer = PageFactory.initElements(driver,
			SVO_CustomerResponsesContainer.class);
	SVO_EnquiryLostReasonContainer SVO_EnquiryLostReasonContainer = PageFactory.initElements(driver,
			SVO_EnquiryLostReasonContainer.class);
	SSEC_1039_SVOEnquiryEmailRemaindersContainer SVOEnquiryEmailRemaindersContainer = PageFactory.initElements(driver,
			SSEC_1039_SVOEnquiryEmailRemaindersContainer.class);
	SSEC_1503_Clasic_Email_Signature_On_Salesforce_Container Clasic_Email_Signature_On_Salesforce_Container = PageFactory.initElements(driver,
			SSEC_1503_Clasic_Email_Signature_On_Salesforce_Container.class);
	SSEC_2159_BespokeOrderForm_CreationofOrderFormRecordsContainer BespokeOrderForm_CreationofOrderFormRecordsContainer = PageFactory.initElements(driver, SSEC_2159_BespokeOrderForm_CreationofOrderFormRecordsContainer.class);
	SSEC_2157_DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer = PageFactory.initElements(driver, SSEC_2157_DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer.class);
	SSEC_2285_Design_Brief_FeedbacksContainer Design_Brief_FeedbacksContainer = PageFactory.initElements(driver, SSEC_2285_Design_Brief_FeedbacksContainer.class);
	
	public static String verificationCode;
	public static String vCode;
	public static String veriCode;
	public static String EnquiryName;

	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}
	

	public void onStart() {

		setupTest("SVOTest");
		driver = getDriver();

		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
		SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
		SVOAdditionalvehicleContainer = PageFactory.initElements(driver, SVOAdditionalVehicleContainer.class);

		RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
				SVO_RestrictedPartyScreeningContainer.class);
		SVOItemToapproveContainer = PageFactory.initElements(driver, SVOItemToApproveContainer.class);
		SVO_CustomerResponsesContainer = PageFactory.initElements(driver, SVO_CustomerResponsesContainer.class);
		SVO_EnquiryLostReasonContainer = PageFactory.initElements(driver, SVO_EnquiryLostReasonContainer.class);
		SVOEnquiryEmailRemaindersContainer = PageFactory.initElements(driver,
				SSEC_1039_SVOEnquiryEmailRemaindersContainer.class);
		Clasic_Email_Signature_On_Salesforce_Container = PageFactory.initElements(driver,
				SSEC_1503_Clasic_Email_Signature_On_Salesforce_Container.class);
		BespokeOrderForm_CreationofOrderFormRecordsContainer = PageFactory.initElements(driver, SSEC_2159_BespokeOrderForm_CreationofOrderFormRecordsContainer.class);
		DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer = PageFactory.initElements(driver, SSEC_2157_DesignBriefAmendments_Seatbelts_Carpets_Fridge_HeadlinerContainer.class);
		Design_Brief_FeedbacksContainer = PageFactory.initElements(driver, SSEC_2285_Design_Brief_FeedbacksContainer.class);
		
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		

		verificationCode = null;
		vCode = null;
		EnquiryName = null;

	}
	
	
    @And("^Verify Reear Center console is present before and then Interiors$")
    public void Verify_Rear_Center_console_is_present_first() throws Exception{
    	
    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	
    	VerifyHandler.verifyElementPresent(Design_Brief_FeedbacksContainer.RearCenterConsole);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Verify Rear Center Console section in Design Brief Page");
    	
    	VerifyHandler.verifyElementPresent(Design_Brief_FeedbacksContainer.Interior);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Verify Interior section in Design Brief Page");
    	
    	
    }
    
    @And("^Verify Interiors is present first and then Carpets$")
    public void Verify_Interiors_is_present_before() throws Exception{
    	
    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	ActionHandler.pageDown();
    	ActionHandler.wait(1);
    	
    	VerifyHandler.verifyElementPresent(Design_Brief_FeedbacksContainer.Interior);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Verify Interior section in Design Brief Page");
    	
    	VerifyHandler.verifyElementPresent(Design_Brief_FeedbacksContainer.Carpets);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Verify Carpets section in Design Brief Page");
    	
    }
    
    @And("^Verify Finishes drop down is removed in the Finishes section$")
    public void Verify_Finishes_drop_down_is_removed() throws Exception{
    	
    	ActionHandler.pageCompleteScrollDown();
    	ActionHandler.wait(1);
    	
        VerifyHandler.verifyElementPresent(Design_Brief_FeedbacksContainer.FinishesSection);
        ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Verify Finishes section in Design Brief Page");
    	
    	VerifyHandler.verifyElementPresent(Design_Brief_FeedbacksContainer.VeneerColor);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Verify VeneerColor section in Design Brief Page");
    	
    	VerifyHandler.verifyElementPresent(Design_Brief_FeedbacksContainer.SVCeramics);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Verify SV Ceramics section in Design Brief Page");
    	
    	VerifyHandler.verifyElementPresent(Design_Brief_FeedbacksContainer.ChromePacks);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Verify Chrome Pack section in Design Brief Page");
    	
    }
    
    @And("^Verify Design Brief is submitted$")
    public void Verify_Design_Brief_is_submitted() throws Exception{
    	
    	ActionHandler.click(Design_Brief_FeedbacksContainer.SubmitBtn);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Clicks on Submit button");
    	
    	ActionHandler.click(Design_Brief_FeedbacksContainer.SaveSubmitBtn);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on Save button for Submit");
    	
    }
    
    @And("^Verify in Paint Colors drop down additional colors are added$")
    public void Verify_in_Paint_colors_additional_colors_added() throws Exception{
    	
    	ActionHandler.click(Design_Brief_FeedbacksContainer.PaintDetail);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on Paint Detail section");
    	
    	ActionHandler.click(Design_Brief_FeedbacksContainer.NewBtnPaint);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on New button in Paint Detail section");
    	
    	VerifyHandler.verifyElementPresent(Design_Brief_FeedbacksContainer.PaintColorText);
    	ActionHandler.wait(1);
    	ActionHandler.click(Design_Brief_FeedbacksContainer.PaintColorText);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on Paint Color drop down");
    	
    	ActionHandler.click(Design_Brief_FeedbacksContainer.CancelPaintBtn);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on Cancel button in Paint section");
    }
    
    @And("^Verify User is able to fill Paint Details in the Paint section PaintColor \"([^\"]*)\" PaintCode \"([^\"]*)\"$")
    public void Verify_user_is_able_to_fill_paint_details(String PaintColor, String PaintCode) throws Exception{
    	
    	String PColor[] = PaintColor.split(",");
    	PaintColor = CommonFunctions.readExcelMasterData(PColor[0], PColor[1], PColor[2]);

    	String PCode[] = PaintCode.split(",");
    	PaintCode = CommonFunctions.readExcelMasterData(PCode[0], PCode[1], PCode[2]);

        ActionHandler.click(Design_Brief_FeedbacksContainer.PaintDetail);
        ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on Paint Detail section");
    	
    	ActionHandler.click(Design_Brief_FeedbacksContainer.NewBtnPaint);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on New button in Paint Detail section");
    	
    	ActionHandler.click(Design_Brief_FeedbacksContainer.PaintColorText);
    	ActionHandler.wait(1);
    	ActionHandler.click(driver.findElement(By.xpath(Design_Brief_FeedbacksContainer.PaintColor(PaintColor))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on Paint Color drop down");
    	
    	ActionHandler.click(Design_Brief_FeedbacksContainer.PaintCodeText);
    	ActionHandler.wait(1);
    	ActionHandler.click(driver.findElement(By.xpath(Design_Brief_FeedbacksContainer.PaintColor(PaintCode))));
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on Paint Code drop down");
    	
    	ActionHandler.click(Design_Brief_FeedbacksContainer.SaveBtnPaint);
    	ActionHandler.wait(2);
    	CommonFunctions.attachScreenshot();
    	Reporter.addStepLog("Click on Save button for Paint");
    	
    }






	
}