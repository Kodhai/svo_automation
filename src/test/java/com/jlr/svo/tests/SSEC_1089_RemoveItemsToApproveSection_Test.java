package com.jlr.svo.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.containers.SSEC_1089_RemoveItemsToApproveSectionContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.containers.SVOOpportunityContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.Then;

public class SSEC_1089_RemoveItemsToApproveSection_Test extends TestBaseCC{

	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory.getLogger(SSEC_1089_RemoveItemsToApproveSection_Test.class.getName());
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
	SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	SSEC_1089_RemoveItemsToApproveSectionContainer RemoveItemsToApproveSectionContainer = PageFactory.initElements(driver, SSEC_1089_RemoveItemsToApproveSectionContainer.class);

	public static String verificationCode;
	public static String vCode;
	public static String veriCode;
/////
	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}
	
	@Then("^Verify that Items to approve section is not available on home page$")
	public void verify_that_Items_to_approve_section_is_not_available_on_home_page() throws Throwable {
	    
		if (!VerifyHandler.verifyElementPresent(RemoveItemsToApproveSectionContainer.ItemsToApproveSection)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User is not able to view Items to approve section on home page of SVO portal");
		} else {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User is able to view Items to approve section on home page of SVO portal");
		}
	}

}
