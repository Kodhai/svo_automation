package com.jlr.svo.tests;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.autotest.utils.Config;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.constants.Constants;
import com.jlr.svo.containers.SVOAccountsContainer;
import com.jlr.svo.containers.SVOAdditionalVehicleContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.containers.SVOItemToApproveContainer;
import com.jlr.svo.containers.SVOOpportunityContainer;
import com.jlr.svo.containers.SVO_Pricing_LogicContainer;
import com.jlr.svo.containers.SVO_RestrictedPartyScreeningContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class SVO_Pricing_Logic_Test extends TestBaseCC {
	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory.getLogger(SVO_Pricing_Logic_Test.class.getName());
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);

	SVOAccountsContainer SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
	SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	SVOAdditionalVehicleContainer SVOAdditionalvehicleContainer = PageFactory.initElements(driver,
			SVOAdditionalVehicleContainer.class);
	SVO_Pricing_LogicContainer SVO_Pricing_LogicContainer = PageFactory.initElements(driver,
			SVO_Pricing_LogicContainer.class);
	SVO_RestrictedPartyScreeningContainer RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
			SVO_RestrictedPartyScreeningContainer.class);
	SVOItemToApproveContainer SVOItemToapproveContainer = PageFactory.initElements(driver,
			SVOItemToApproveContainer.class);
	

	public static String OpportunityName;
	public static String verificationCode;
	public static String vCode;
	public static String veriCode;
	public static String AdditionalVehicleRetailPrice;
	public static String DesignBriefRequestname;
	public static String TotalRetailPrice;
	public static String PrivateOfficeQuoteName;
	public static String TotalRetailPriceOnOrdersPage;
	public static String TotalListPrice;
	public static String TotalCostPrice;
	public static String TotalContribution;
	public static String TotalRevenue;
	public static String TotalAdditionalVehicleRetailPrice;
	public static String TotalAdditionalVehicleListPrice;
	public static String TotalAdditionalVehicleCostPrice;
	public static String TotalAdditionalVehicleContribution;
	public static String TotalAdditionalVehicleRevenue;
	public static String AdditionalVehicleCostPrice;
	public static String AdditionalVehicleListPrice;
	public static String AdditionalVehicleContribution;
	public static String AdditionalVehicleRevenue;

	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
		
	}
	public void onStart() {
	setupTest("SVOTest");
	javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);

	driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}

	@Then("^User is not able to view Additional vehicles section/Additional vehicles hyper link for Private office opportunity$")
	public void user_is_not_able_to_view_Additional_vehicles_section_Additional_vehicles_hyper_link_for_Private_office_opportunity()
			throws Throwable {

		if (!VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.AdditionalVehicleLink)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User is not able to view Additional vehicles section for Private office opportunity");
		} else {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User is able to view Additional vehicles section for Private office opportunity");
		}
	}

	@Then("^User is not able to view send sales agreement button on quote page$")
	public void user_is_not_able_to_view_send_sales_agreement_button_on_quote_page() throws Throwable {

		ActionHandler.pageDown();
		CommonFunctions.attachScreenshot();

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuotesLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Quotes page");

		ActionHandler.wait(4);
		driver.navigate().refresh();
		ActionHandler.wait(8);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstOpportunityLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens created quote details");

		if (!VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.DesignBriefLink)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User is not able to view Design brief hyper link on quote page");
		} else {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User is able to view Design brief hyper link on quote page");
		}

		ActionHandler.wait(8);
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(8);
		Reporter.addStepLog("Quote status moves to Price stage");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(8);
		Reporter.addStepLog("Quote status moves to Proposal Sent stage");

		driver.navigate().refresh();
		ActionHandler.wait(7);

		if (!VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.SendSalesAgreementBtn)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User is not able to view send sales agreement button on quote page");
		} else {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User is able to view send sales agreement button on quote page");
		}
	}


	

	@And("^User adds one Vehicle Record with all details Brand \"([^\"]*)\", Model \"([^\"]*)\", Series Name \"([^\"]*)\", Derivative \"([^\"]*)\" and Model Year \"([^\"]*)\"$")
	public void User_adds_one_Vehicle_Record_with_all_details_Brand(String Brand, String Model, String seriesName,
			String derivative, String modelYear) throws Exception {

		AdditionalVehicleRetailPrice = null;

		String b[] = Brand.split(",");
		Brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String m[] = Model.split(",");
		Model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		String series[] = seriesName.split(",");
		seriesName = CommonFunctions.readExcelMasterData(series[0], series[1], series[2]);

		String deri[] = derivative.split(",");
		derivative = CommonFunctions.readExcelMasterData(deri[0], deri[1], deri[2]);

		String my[] = modelYear.split(",");
		modelYear = CommonFunctions.readExcelMasterData(my[0], my[1], my[2]);

		driver.navigate().refresh();
		ActionHandler.wait(10);
		ActionHandler.click(SVOAdditionalvehicleContainer.NewAdditionalVehicleBtn1);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on New button after navigating to Additional vehicle section");

		ActionHandler.wait(1);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(1);
		ActionHandler.setText(SVOAdditionalvehicleContainer.AdditionalVehicleBrand, Brand);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeBrandSearch);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(Brand))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Brand for an Additional vehicle record");

		ActionHandler.wait(1);
		ActionHandler.setText(SVOAdditionalvehicleContainer.AdditionalVehicleModel, Model);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(Model))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Model for an Additional vehicle record");

		ActionHandler.wait(1);
		ActionHandler.setText(SVOAdditionalvehicleContainer.AdditionalVehicleSeriesName, seriesName);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleVehicleSeriesNameSearch);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(seriesName))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Series Name for an Additional vehicle record");

		ActionHandler.wait(1);
		ActionHandler.setText(SVOAdditionalvehicleContainer.AdditionalVehicleDerivative, derivative);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleVehicleDerivativeSearch);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(derivative))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Derivative for an Additional vehicle record");

		ActionHandler.wait(1);
		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleModelYear);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ModelYearSelection(modelYear))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects model year for an Additional vehicle record");

		ActionHandler.wait(1);
		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleWheelBase);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleWheelBaseStandard);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Wheel base as Standard Wheelbase for an Additional vehicle record");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(1);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSaveOpportunity);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("One additional vehicle record with pricing details has been added successfully");

		

	}


	@Then("^User is able to verify that additional vehicle pricing details are not dispayed on additional vehicle page$")
	public void user_is_able_to_verify_that_additional_vehicle_pricing_details_are_not_dispayed_on_additional_vehicle_page()
			throws Throwable {

		AdditionalVehicleCostPrice = null;
		AdditionalVehicleRetailPrice = null;
		AdditionalVehicleListPrice = null;
		AdditionalVehicleContribution = null;
		AdditionalVehicleRevenue = null;

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.AdditionalVehicleLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate to additional vehicle quick link");

		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleSectionFirstAVRecord);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens details of created additional vehicle");

		ActionHandler.pageDown();
		ActionHandler.wait(2);
		ActionHandler.scrollToView(SVO_Pricing_LogicContainer.AdditionalVehicleCostPriceDetails);
		ActionHandler.wait(2);

		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.AdditionalVehicleCostPriceDetails);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Cost price of additional vehicle is not displayed without accepting quote");

		AdditionalVehicleCostPrice = SVO_Pricing_LogicContainer.AdditionalVehicleCostPriceDetails.getText();
		Reporter.addStepLog("Additional vehicle cost Price is = " + AdditionalVehicleCostPrice);

		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.AdditionalVehicleContributionPriceDetails);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Contribution of additional vehicle is not displayed without accepting quote");

		AdditionalVehicleContribution = SVO_Pricing_LogicContainer.AdditionalVehicleContributionPriceDetails.getText();
		Reporter.addStepLog("Additional Vehicle contribution is = " + AdditionalVehicleContribution);

		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.AdditionalVehicleRevenuePriceDetails);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Revenue of additional vehicle is not displayed without accepting quote");

		AdditionalVehicleRevenue = SVO_Pricing_LogicContainer.AdditionalVehicleRevenuePriceDetails.getText();
		Reporter.addStepLog("Additional Vehicle revenue is = " + AdditionalVehicleRevenue);

		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.AdditionalVehicleListPriceDetails);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("List price of additional vehicle is not displayed without accepting quote");

		AdditionalVehicleListPrice = SVO_Pricing_LogicContainer.AdditionalVehicleListPriceDetails.getText();
		Reporter.addStepLog("Additional vehicle List Price is = " + AdditionalVehicleListPrice);

		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.AdditionalVehicleRetailPriceDetails);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Retail price of additional vehicle is not displayed without accepting quote");

		AdditionalVehicleRetailPrice = SVO_Pricing_LogicContainer.AdditionalVehicleRetailPriceDetails.getText();
		Reporter.addStepLog("Additional vehicle retail price is = " + AdditionalVehicleRetailPrice);

		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(3);
	}


	@Then("^User adds second Vehicle Record with all details like Brand \"([^\"]*)\" Model \"([^\"]*)\" Series Name \"([^\"]*)\" Derivative \"([^\"]*)\" and Model Year \"([^\"]*)\"$")
	public void user_adds_second_Vehicle_Record_with_all_detials_like_Brand_Model_Series_Name_Derivative_and_Model_Year(

			String Brand, String Model, String seriesName, String derivative, String modelYear) throws Exception {

		AdditionalVehicleRetailPrice = null;

		String b[] = Brand.split(",");
		Brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String m[] = Model.split(",");
		Model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		String series[] = seriesName.split(",");
		seriesName = CommonFunctions.readExcelMasterData(series[0], series[1], series[2]);

		String deri[] = derivative.split(",");
		derivative = CommonFunctions.readExcelMasterData(deri[0], deri[1], deri[2]);

		String my[] = modelYear.split(",");
		modelYear = CommonFunctions.readExcelMasterData(my[0], my[1], my[2]);

		driver.navigate().refresh();
		ActionHandler.wait(10);
		ActionHandler.click(SVOAdditionalvehicleContainer.NewAdditionalVehicleBtn1);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on New button after navigating to Additional vehicle section");

		ActionHandler.wait(1);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(1);
		ActionHandler.setText(SVOAdditionalvehicleContainer.AdditionalVehicleBrand, Brand);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeBrandSearch);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(Brand))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Brand for an Additional vehicle record");

		ActionHandler.wait(1);
		ActionHandler.setText(SVOAdditionalvehicleContainer.AdditionalVehicleModel, Model);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(Model))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Model for an Additional vehicle record");

		ActionHandler.wait(1);
		ActionHandler.setText(SVOAdditionalvehicleContainer.AdditionalVehicleSeriesName, seriesName);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleVehicleSeriesNameSearch);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(seriesName))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Series Name for an Additional vehicle record");

		ActionHandler.wait(1);
		ActionHandler.setText(SVOAdditionalvehicleContainer.AdditionalVehicleDerivative, derivative);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleVehicleDerivativeSearch);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(derivative))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Derivative for an Additional vehicle record");

		ActionHandler.wait(1);
		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleModelYear);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ModelYearSelection(modelYear))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects model year for an Additional vehicle record");

		ActionHandler.wait(1);
		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleWheelBase);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleWheelBaseStandard);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Wheel base as Standard Wheelbase for an Additional vehicle record");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(1);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSaveOpportunity);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("One additional vehicle record with pricing details has been added successfully");

		AdditionalVehicleRetailPrice = SVO_Pricing_LogicContainer.SecondAdditionalVehicleRetailPrice.getText();
		Reporter.addStepLog("Second Additional Vehicle Retail Price is = " + AdditionalVehicleRetailPrice);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Retail Price of second additional vehicle record is displayed on Additional vehicle page");

	}

	@Then("^User create a Design Brief Quote with fields like Retailer Contact \"([^\"]*)\"$")
	public void User_create_a_Design_Brief_Quote_with__field(String retailerContact) throws Exception {
		DesignBriefRequestname = null;

		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		String quoteName = "SVO Bespoke Test Quote";
		String expirtaionDate = CommonFunctions.selectFutureDate();

		ActionHandler.click(SVO_Pricing_LogicContainer.OpportunityNameOnAdditionalVehiclePage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate back to the Opportunity");

		ActionHandler.pageDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuotesLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Quotes page");

		ActionHandler.wait(1);
		ActionHandler.click(SVOAdditionalvehicleContainer.NewQuoteBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on New button to create new Quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteNameBox, quoteName + randomNumber);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User writes quote name for new quote");

		ActionHandler.wait(1);
		ActionHandler.scrollDown();
		ActionHandler.wait(2);

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRetailerNameBox, retailerContact);
		ActionHandler.wait(3);
		ActionHandler.click(SVOItemToapproveContainer.QuoteRetailerContact);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters retailer contact details for new quote");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteCostPrice, "123");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Cost Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteListPrice, "203");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters List Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteContribution, "23");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Contribution for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRetailPrice, "213");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Retail Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRevenue, "113");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Revenue for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteExpirationDate, expirtaionDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Expiration Date for new quote");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteSaveBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User save new quote");

		ActionHandler.wait(3);
		DesignBriefRequestname = SVOAdditionalvehicleContainer.FirstQuote.getText();
		Reporter.addStepLog("Quote Name is = " + DesignBriefRequestname);
		ActionHandler.wait(5);

		driver.navigate().refresh();
		ActionHandler.wait(10);
		ActionHandler.click(SVO_Pricing_LogicContainer.OpportunityNameOnAdditionalVehiclePage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate back to the Opportunity");
	}

	@Then("^User send sales agreement document of design brief quote$")
	public void User_send_sales_agreement_document_of_design_brief_quote() throws Throwable {
		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.sendSalesAgreement);
		ActionHandler.wait(180);
		Reporter.addStepLog("User clicks the Send Sales Agreement button");
		CommonFunctions.attachScreenshot();

		if (!VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.draftText)) {

			driver.navigate().back();
			Reporter.addStepLog("User navigate to quotes page");
			CommonFunctions.attachScreenshot();

			ActionHandler.wait(5);
			driver.navigate().refresh();
			ActionHandler.wait(15);
			ActionHandler.pageScrollDown();
			ActionHandler.wait(3);
			javaScriptUtil.clickElementByJS(SVO_Pricing_LogicContainer.SalesAgreementListLink);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User clicks on sales agreement link under related list quick links");

			javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstDesignBrief);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User clicks on Sales Agreement that is generated");
		}

	}

	@Then("^User verifies the pricing details of main vehicle and Additional vehicles on sales agreement document$")
	public void User_verifies_the_pricing_details_of_main_vehicle_and_Additional_vehicles_on_sales_agreement_document()
			throws Throwable {
		ActionHandler.wait(3);

		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(SVO_Pricing_LogicContainer.SalesAgreementPdfdocument);
		ActionHandler.wait(15);

		String parentWindow = driver.getWindowHandle();
		Set<String> PdfWindows = driver.getWindowHandles();
		Iterator<String> i = PdfWindows.iterator();
		while (i.hasNext()) {
			String SalesAgreementWindow = i.next();
			if (!parentWindow.equalsIgnoreCase(SalesAgreementWindow)) {
				driver.switchTo().window(SalesAgreementWindow);
				String currenturl = driver.getCurrentUrl();

				URL url = new URL(currenturl);
				InputStream is = url.openStream();
				BufferedInputStream fileParse = new BufferedInputStream(is);
				PDDocument document = null;

				document = PDDocument.load(fileParse);
				String PdfContent = new PDFTextStripper().getText(document);
				Reporter.addStepLog(
						"Total price details of main vehicle and additional vehicles are displayed with content as: "
								+ PdfContent);
				Assert.assertTrue(PdfContent.contains("Total Price"));
				CommonFunctions.attachScreenshot();
				Reporter.addStepLog("User is able to view pricing details of main vehicle on sales agreement document");

				Assert.assertTrue(PdfContent.contains("Total Price - Additional Vehicles"));
				CommonFunctions.attachScreenshot();
				Reporter.addStepLog(
						"User is able to view pricing details of Additional vehicle on sales agreement document");

				driver.close();
				driver.switchTo().window(parentWindow);
			}
		}
		Reporter.addStepLog("User is able to view pricing details of vehicles on sales agreement document");

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"User is able to view pricing details of both main vehicle and Additional vehicle on sales agreement document");

	}

	@Then("^User verifies the pricing details of only main vehicle on sales agreement document$")
	public void User_verifies_the_pricing_details_of_only_main_vehicle_on_sales_agreement_document() throws Throwable {
		ActionHandler.wait(3);

		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(SVO_Pricing_LogicContainer.SalesAgreementPdfdocument);
		ActionHandler.wait(15);

		String parentWindow = driver.getWindowHandle();
		Set<String> PdfWindows = driver.getWindowHandles();
		Iterator<String> i = PdfWindows.iterator();
		while (i.hasNext()) {
			String SalesAgreementWindow = i.next();
			if (!parentWindow.equalsIgnoreCase(SalesAgreementWindow)) {
				driver.switchTo().window(SalesAgreementWindow);
				String currenturl = driver.getCurrentUrl();

				URL url = new URL(currenturl);
				InputStream is = url.openStream();
				BufferedInputStream fileParse = new BufferedInputStream(is);
				PDDocument document = null;

				document = PDDocument.load(fileParse);
				String PdfContent = new PDFTextStripper().getText(document);
				Reporter.addStepLog(
						"Total price details of main vehicle and additional vehicles are displayed with content as: "
								+ PdfContent);
				Assert.assertTrue(PdfContent.contains("Total Price"));
				CommonFunctions.attachScreenshot();
				Reporter.addStepLog(
						"User is able to view pricing details of only main vehicle on sales agreement document");

				driver.close();
				driver.switchTo().window(parentWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User is able to view pricing details of only main vehicle on sales agreement document");
	}

	@Then("^User is able to view pricing details of first additional vehicle$")
	public void User_is_able_to_view_pricing_details_of_first_additional_vehicle() throws Throwable {

		ActionHandler.wait(3);
		ActionHandler.click(SVO_Pricing_LogicContainer.FirstAdditionalVehicleLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to First Additional vehicle details section");

		driver.navigate().refresh();
		ActionHandler.wait(10);
		ActionHandler.scrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);

		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.AdditionalVehicleCostPriceDetails);
		String AdditionalVehicleCostPrice = SVO_Pricing_LogicContainer.AdditionalVehicleCostPriceDetails.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"User is able to view Cost price of an additional vehicle as :" + AdditionalVehicleCostPrice);

		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.AdditionalVehicleContributionPriceDetails);
		CommonFunctions.attachScreenshot();
		String AdditionalVehicleContributionPriceDetails = SVO_Pricing_LogicContainer.AdditionalVehicleContributionPriceDetails
				.getText();
		Reporter.addStepLog("User is able to view Contribution price of an additional vehicle as :"
				+ AdditionalVehicleContributionPriceDetails);

		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.AdditionalVehicleRevenuePriceDetails);
		CommonFunctions.attachScreenshot();
		String AdditionalVehicleRevenuePriceDetails = SVO_Pricing_LogicContainer.AdditionalVehicleRevenuePriceDetails
				.getText();
		Reporter.addStepLog("User is able to view Revenue price of an additional vehicle as :"
				+ AdditionalVehicleRevenuePriceDetails);

		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.AdditionalVehicleListPriceDetails);
		CommonFunctions.attachScreenshot();
		String AdditionalVehicleListPriceDetails = SVO_Pricing_LogicContainer.AdditionalVehicleListPriceDetails
				.getText();
		Reporter.addStepLog(
				"User is able to view List price of an additional vehicle as :" + AdditionalVehicleListPriceDetails);

		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.AdditionalVehicleRetailPriceDetails);
		CommonFunctions.attachScreenshot();
		String AdditionalVehicleRetailPriceDetails = SVO_Pricing_LogicContainer.AdditionalVehicleRetailPriceDetails
				.getText();
		Reporter.addStepLog("User is able to view Retail price of an additional vehicle as :"
				+ AdditionalVehicleRetailPriceDetails);

		driver.navigate().back();
		ActionHandler.wait(8);

	}

	@Then("^User is able to view pricing details of second additional vehicle$")
	public void User_is_able_to_view_pricing_details_of_second_additional_vehicle() throws Throwable {

		driver.navigate().refresh();
		ActionHandler.wait(10);
		ActionHandler.click(SVO_Pricing_LogicContainer.SecondAdditionalVehicleLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Second Additional vehicle details section");
		driver.navigate().refresh();
		ActionHandler.wait(10);
		ActionHandler.scrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);

		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.AdditionalVehicleCostPriceDetails);
		String AdditionalVehicleCostPrice = SVO_Pricing_LogicContainer.AdditionalVehicleCostPriceDetails.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"User is able to view Cost price of an additional vehicle as :" + AdditionalVehicleCostPrice);

		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.AdditionalVehicleContributionPriceDetails);
		CommonFunctions.attachScreenshot();
		String AdditionalVehicleContributionPriceDetails = SVO_Pricing_LogicContainer.AdditionalVehicleContributionPriceDetails
				.getText();
		Reporter.addStepLog("User is able to view Contribution price of an additional vehicle as :"
				+ AdditionalVehicleContributionPriceDetails);

		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.AdditionalVehicleRevenuePriceDetails);
		CommonFunctions.attachScreenshot();
		String AdditionalVehicleRevenuePriceDetails = SVO_Pricing_LogicContainer.AdditionalVehicleRevenuePriceDetails
				.getText();
		Reporter.addStepLog("User is able to view Revenue price of an additional vehicle as :"
				+ AdditionalVehicleRevenuePriceDetails);

		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.AdditionalVehicleListPriceDetails);
		CommonFunctions.attachScreenshot();
		String AdditionalVehicleListPriceDetails = SVO_Pricing_LogicContainer.AdditionalVehicleListPriceDetails
				.getText();
		Reporter.addStepLog(
				"User is able to view List price of an additional vehicle as :" + AdditionalVehicleListPriceDetails);

		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.AdditionalVehicleRetailPriceDetails);
		CommonFunctions.attachScreenshot();
		String AdditionalVehicleRetailPriceDetails = SVO_Pricing_LogicContainer.AdditionalVehicleRetailPriceDetails
				.getText();
		Reporter.addStepLog("User is able to view Retail price of an additional vehicle as :"
				+ AdditionalVehicleRetailPriceDetails);

	}

	@And("^User creates Bespoke Opportunity with mandatory fields like Stage \"([^\"]*)\" Product Offering \"([^\"]*)\" Region \"([^\"]*)\" Client \"([^\"]*)\" Restricted Party Screening Stage \"([^\"]*)\" Preferred Contact \"([^\"]*)\" Account Name \"([^\"]*)\" Retailer Contact \"([^\"]*)\" Source \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\"$")
	public void User_creates_Bespoke_Opportunity(String stage, String productOffering, String region, String client,
			String Checkcleared, String preferredContact, String accountName, String retailerContact, String source,
			String brand, String model) throws Exception {
		
		OpportunityName = null;
		String oppName = "Test_SVO_Bespoke_Opportunity_";
		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		String closeDate = CommonFunctions.selectFutureDate();
		Reporter.addStepLog("Opportunity Close Date is = " + closeDate);

		String s[] = stage.split(",");
		stage = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String proOff[] = productOffering.split(",");
		productOffering = CommonFunctions.readExcelMasterData(proOff[0], proOff[1], proOff[2]);

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String c[] = client.split(",");
		client = CommonFunctions.readExcelMasterData(c[0], c[1], c[2]);

		String RPS[] = Checkcleared.split(",");
		Checkcleared = CommonFunctions.readExcelMasterData(RPS[0], RPS[1], RPS[2]);

		String preCon[] = preferredContact.split(",");
		preferredContact = CommonFunctions.readExcelMasterData(preCon[0], preCon[1], preCon[2]);

		String accName[] = accountName.split(",");
		accountName = CommonFunctions.readExcelMasterData(accName[0], accName[1], accName[2]);

		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		String so[] = source.split(",");
		source = CommonFunctions.readExcelMasterData(so[0], so[1], so[2]);

		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String m[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		ActionHandler.wait(3);
		Reporter.addStepLog("User starts creating new SVO Bespoke Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.NewOpportunity);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
	

		if (VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.QuoteNextBtn)) {
			ActionHandler.click(SVOItemToapproveContainer.SVOBespokeOpportunity);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User selects opportunity record type as SVO bespoke");

			ActionHandler.click(SVOItemToapproveContainer.QuoteNextBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User clicks on next button");

		}

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeOppName, oppName + randomNumber);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Name");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeClosedDate, closeDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Closed date");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeStage);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.StageSelection(stage))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Stage for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeProductOffering);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ProOfferingSelection(productOffering))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Product Offering for an Opportunity");

		ActionHandler.scrollToView(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		ActionHandler.wait(2);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.RegionSelection(region))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Region for an Opportunity");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.SVOBespokeClient);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeClient, client);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeClientNameSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(client))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Client for an Opportunity");

		VerifyHandler.verifyElementPresent(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(RestrictedPartyScreeningContainer.ValueSelection(Checkcleared))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects restricted party screening as Check cleared for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokePreferredContact, preferredContact);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		act1.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(3);

		Reporter.addStepLog("User selects Preferred Contact for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeAccountName, accountName);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeAccountNameSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(accountName))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Account Name for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeRetailerContact, retailerContact);
		ActionHandler.click(SVOItemToapproveContainer.SVOBespokePreferredContactname);
		Reporter.addStepLog("User selects Retailer Contact for an Opportunity");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSource);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.SourceSelection(source))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Source for an Opportunity");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeBrand, brand);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeBrandSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(brand))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Brand for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeModel, model);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(model))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Model for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSaveOpportunity);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves SVO Bespoke Opportunity");

		OpportunityName = SVOAdditionalvehicleContainer.OpportunityName.getText();
		Reporter.addStepLog("Opportunity Name is = " + OpportunityName);
	}

	@Then("^User adds pricing details on Opportunity page$")
	public void user_adds_pricing_details_on_Opportunity_page() throws Throwable {

		ActionHandler.pageDown();
		ActionHandler.pageDown();
		ActionHandler.wait(5);

		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(SVO_Pricing_LogicContainer.PricingDetailsEditBtn);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);

		ActionHandler.setText(SVO_Pricing_LogicContainer.MainVehicleRetailPrice, "500");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Cost Price for main vehicle on opportunity page");

		ActionHandler.setText(SVO_Pricing_LogicContainer.MainVehicleListPrice, "550");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters List Price for main vehicle on opportunity page");

		ActionHandler.setText(SVO_Pricing_LogicContainer.MainVehicleCostPrice, "600");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Contribution for main vehicle on opportunity page");

		ActionHandler.setText(SVO_Pricing_LogicContainer.MainVehicleContribution, "650");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Retail Price for main vehicle on opportunity page");

		ActionHandler.setText(SVO_Pricing_LogicContainer.MainVehicleRevenue, "700");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Revenue for main vehicle on opportunity page");

		ActionHandler.wait(1);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSaveOpportunity);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("pricing details are added to an opportunity successfully");

		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(3);

	}

	@Then("^User verifies total pricing details of both main and additional vehicles on opportunity page$")
	public void user_verifies_total_pricing_details_of_both_main_and_additional_vehicles_on_opportunity_page()
			throws Throwable {

		String TotalRetailPrice = null;
		String TotalListPrice = null;
		String TotalCostPrice = null;
		String TotalContribution = null;
		String TotalRevenue = null;

		ActionHandler.click(SVO_Pricing_LogicContainer.OpportunityNameOnAdditionalVehiclePage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate back to the Opportunity");

		ActionHandler.pageDown();
		ActionHandler.wait(2);
		ActionHandler.scrollToView(SVO_Pricing_LogicContainer.TotalRetailPrice);
		ActionHandler.wait(2);

		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.TotalRetailPrice);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the total retail price of an opportunity");

		TotalRetailPrice = SVO_Pricing_LogicContainer.TotalRetailPrice.getText();
		Reporter.addStepLog("Total Retail Price is = " + TotalRetailPrice);

		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.TotalListPrice);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the total list price of an opportunity");

		TotalListPrice = SVO_Pricing_LogicContainer.TotalListPrice.getText();
		Reporter.addStepLog("Total List Price is = " + TotalListPrice);

		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.TotalCostPrice);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the total cost price of an opportunity");

		TotalCostPrice = SVO_Pricing_LogicContainer.TotalCostPrice.getText();
		Reporter.addStepLog("Total Cost Price is = " + TotalCostPrice);

		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.TotalContribution);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the total contribution of an opportunity");

		TotalContribution = SVO_Pricing_LogicContainer.TotalContribution.getText();
		Reporter.addStepLog("Total Contribution is = " + TotalContribution);

		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.TotalRevenue);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the total revenue of an opportunity");

		TotalRevenue = SVO_Pricing_LogicContainer.TotalRevenue.getText();
		Reporter.addStepLog("Total Revenue is = " + TotalRevenue);

		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(3);
	}

	@Given("^User views total pricing details of same opportunity$")
	public void user_views_total_pricing_details_of_same_opportunity() throws Throwable {

		String TotalRetailPrice = null;
		String TotalListPrice = null;
		String TotalCostPrice = null;
		String TotalContribution = null;
		String TotalRevenue = null;

		javaScriptUtil.clickElementByJS(SVO_OpportunityContainer.OpportTab);
		CommonFunctions.attachScreenshot();

		javaScriptUtil.clickElementByJS(SVO_Pricing_LogicContainer.OpportunityDropdown);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);

		javaScriptUtil.clickElementByJS(SVO_Pricing_LogicContainer.AllOpportunities);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Reporter.addStepLog("User selects All Opportunities in the list");

		ActionHandler.setText(SVOAdditionalvehicleContainer.OpportunitySearchBox, OpportunityName);
		Actions act5 = new Actions(driver);
		act5.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstOpportunityLink);
		CommonFunctions.attachScreenshot();
		ActionHandler.scrollToView(SVO_Pricing_LogicContainer.TotalRetailPrice);
		ActionHandler.wait(2);

		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.TotalRetailPrice);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the total retail price of an opportunity");

		TotalRetailPrice = SVO_Pricing_LogicContainer.TotalRetailPrice.getText();
		Reporter.addStepLog("Total Retail Price is = " + TotalRetailPrice);

		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.TotalListPrice);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the total list price of an opportunity");

		TotalListPrice = SVO_Pricing_LogicContainer.TotalListPrice.getText();
		Reporter.addStepLog("Total List Price is = " + TotalListPrice);

		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.TotalCostPrice);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the total cost price of an opportunity");

		TotalCostPrice = SVO_Pricing_LogicContainer.TotalCostPrice.getText();
		Reporter.addStepLog("Total Cost Price is = " + TotalCostPrice);

		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.TotalContribution);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the total contribution of an opportunity");

		TotalContribution = SVO_Pricing_LogicContainer.TotalContribution.getText();
		Reporter.addStepLog("Total Contribution is = " + TotalContribution);

		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.TotalRevenue);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the total revenue of an opportunity");

		TotalRevenue = SVO_Pricing_LogicContainer.TotalRevenue.getText();
		Reporter.addStepLog("Total Revenue is = " + TotalRevenue);

		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(3);
	}

	@Then("^User edit main vehicle pricing details on opportunity page$")
	public void user_edit_main_vehicle_pricing_details_on_opportunity_page() throws Throwable {

		javaScriptUtil.clickElementByJS(SVO_Pricing_LogicContainer.PricingDetailsEditBtn);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);

		ActionHandler.setText(SVO_Pricing_LogicContainer.MainVehicleRetailPrice, "100");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User edits Retail Price for main vehicle on opportunity page");

		ActionHandler.setText(SVO_Pricing_LogicContainer.MainVehicleListPrice, "200");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User edits List Price for main vehicle on opportunity page");

		ActionHandler.setText(SVO_Pricing_LogicContainer.MainVehicleCostPrice, "250");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User edits Cost for main vehicle on opportunity page");

		ActionHandler.setText(SVO_Pricing_LogicContainer.MainVehicleContribution, "300");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User edits Contribution for main vehicle on opportunity page");

		ActionHandler.setText(SVO_Pricing_LogicContainer.MainVehicleRevenue, "350");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User edits Revenue for main vehicle on opportunity page");

		ActionHandler.wait(1);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSaveOpportunity);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("pricing details of main vehicle has been edited on opportunity page successfully");

	}

	@Then("^User views updated total pricing details on opportunity page$")
	public void user_views_updated_total_pricing_details_on_opportunity_page() throws Throwable {

		String TotalRetailPrice = null;
		String TotalListPrice = null;
		String TotalCostPrice = null;
		String TotalContribution = null;
		String TotalRevenue = null;

		ActionHandler.scrollToView(SVO_Pricing_LogicContainer.TotalRetailPrice);
		ActionHandler.wait(2);

		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.TotalRetailPrice);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the total updated retail price of an opportunity");

		TotalRetailPrice = SVO_Pricing_LogicContainer.TotalRetailPrice.getText();
		Reporter.addStepLog("Total Retail Price is = " + TotalRetailPrice);

		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.TotalListPrice);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the updated total list price of an opportunity");

		TotalListPrice = SVO_Pricing_LogicContainer.TotalListPrice.getText();
		Reporter.addStepLog("Total List Price is = " + TotalListPrice);

		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.TotalCostPrice);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the updated total cost price of an opportunity");

		TotalCostPrice = SVO_Pricing_LogicContainer.TotalCostPrice.getText();
		Reporter.addStepLog("Total Cost Price is = " + TotalCostPrice);

		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.TotalContribution);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the updated total contribution of an opportunity");

		TotalContribution = SVO_Pricing_LogicContainer.TotalContribution.getText();
		Reporter.addStepLog("Total Contribution is = " + TotalContribution);

		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.TotalRevenue);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies updated the total revenue of an opportunity");

		TotalRevenue = SVO_Pricing_LogicContainer.TotalRevenue.getText();
		Reporter.addStepLog("Total Revenue is = " + TotalRevenue);
	}

	


	@Then("^User edit additional vehicle pricing details on additional vehicle page$")
	public void user_edit_additional_vehicle_pricing_details_on_additional_vehicle_page() throws Throwable {

		ActionHandler.wait(120);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.AdditionalVehicleLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate to additional vehicle quick link");

		driver.navigate().refresh();
		ActionHandler.wait(7);
		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(SVO_Pricing_LogicContainer.AdditionalVehicleShowMoreDropdownBtn);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(8);
		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleEditBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks on edit button");

		ActionHandler.wait(1);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(1);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSaveOpportunity);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("additional vehicle pricing details has been edited successfully");

		

		ActionHandler.click(SVO_Pricing_LogicContainer.OpportunityNameOnAdditionalVehiclePage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate back to the Opportunity");
	}


	@And("^User verifies total retail price on order page after accepting the Quote for Retailer Contact \"([^\"]*)\"$")
	public void User_verifies_total_retail_price_on_order_page_after_accepting_the_quote(String retailerContact)
			throws Exception {
		String TotalRetailPriceOnOrdersPage = null;

		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		String quoteName = "SVO Bespoke Test Quote";
		String expirtaionDate = CommonFunctions.selectFutureDate();

		ActionHandler.pageDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuotesLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Quotes page");

		ActionHandler.click(SVOAdditionalvehicleContainer.NewQuoteBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on New button to create new Quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteNameBox, quoteName);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User writes quote name for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRetailerNameBox, retailerContact);
		ActionHandler.click(SVOItemToapproveContainer.QuoteRetailerContact);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters retailer contact details for new quote");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteCostPrice, "123");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Cost Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteListPrice, "203");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters List Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteContribution, "23");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Contribution for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRetailPrice, "213");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Retail Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRevenue, "113");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Revenue for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteExpirationDate, expirtaionDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Expiration Date for new quote");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteSaveBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User save new quote");

		ActionHandler.wait(3);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstQuote);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens created quote details");

		ActionHandler.wait(3);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.DesignBriefLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Design Brief Section");

		ActionHandler.wait(3);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstDesignBrief);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens first design brief from the list");

		ActionHandler.wait(3);
		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefSubmit);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Design Brief status moves to Submitted from Draft");

		ActionHandler.wait(3);
		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefSaveSubmission);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves Design Brief Submission");

		ActionHandler.wait(3);
		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefReturnRenderPack);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User returns render pack for Design Brief");

		ActionHandler.wait(3);
		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefSaveRenderPack);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		Reporter.addStepLog("User saves Design Brief for Return Render Pack");

		ActionHandler.wait(3);
		javaScriptUtil.clickElementByJS(SVO_OpportunityContainer.OpportTab);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		ActionHandler.click(SVOAdditionalvehicleContainer.FirstOpportunityLink);
		CommonFunctions.attachScreenshot();

		ActionHandler.pageDown();
		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuotesLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Quotes page");

		ActionHandler.wait(5);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstQuoteFromOpportunity);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens created quote details");

		ActionHandler.wait(5);
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("Quote status moves to Price stage");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("Quote status moves to Proposal Sent stage");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(10);

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDrpDwn);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeAccepted);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User mark accepeted as Quote Outcome");

		ActionHandler.wait(3);
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDetailDrpDwn);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDetailAccepted);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDoneBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User accepeted the Quote");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuoteOpportunityLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate to Opportunity");

		driver.navigate().refresh();
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.OrdersLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Orders link");

		ActionHandler.wait(3);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstOrder);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens Order details for the opportunity");

		ActionHandler.wait(3);
		ActionHandler.click(RestrictedPartyScreeningContainer.CommonOrderNumber1);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		ActionHandler.setText(SVOAdditionalvehicleContainer.CommonOrderNumberBox, "123456");
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		ActionHandler.click(RestrictedPartyScreeningContainer.SaveCommonOrderNumber1);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User saves common order number");

		driver.navigate().refresh();
		ActionHandler.wait(15);
		ActionHandler.pageDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.TotalRetailPriceOnOrdersPage);
		Reporter.addStepLog("User verifies the total retail price of an opportunity on orders page");

		TotalRetailPriceOnOrdersPage = SVO_Pricing_LogicContainer.TotalRetailPriceOnOrdersPage.getText();
		Reporter.addStepLog("Total Retail Price is = " + TotalRetailPriceOnOrdersPage);

		if (TotalRetailPriceOnOrdersPage.equals(TotalRetailPrice)) {
			Reporter.addStepLog(
					"User verifies the total retail price of an opportunity on orders page is same as the total retail price on opportunity page");
		} else {
			Reporter.addStepLog(
					"User verifies the total retail price of an opportunity on orders page is not same as the total retail price on opportunity page");
		}

	}

	@Given("^Access to SVO Portal by bespoke username with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void access_to_an_SVO_portal_by_bespoke_username_with_Username_and_Password(String userName, String password)
			throws Throwable {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		ActionHandler.click(SVO_OpportunityContainer.icon_image);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Logout");

		ActionHandler.click(SVO_Pricing_LogicContainer.AddUsername);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Add Username");

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailOnBespokeUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();

			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				CommonFunctions.attachScreenshot();

			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				CommonFunctions.attachScreenshot();

			} else {

				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	public String checkEmailOnBespokeUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(334, 340);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	// Agreement section not present under related quick links
	@Then("^Verify the Agreement section is not present under related quick links$")
	public void Agreement_section_is_not_present_under_related_quick_links() throws Throwable {

		ActionHandler.wait(5);
		if(!VerifyHandler.verifyElementPresent(SVO_OpportunityContainer.agreementsSection));{
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Agreement section is not present under related quick links");
		}
	}
		
	

	// Click on edit button
	@Then("^Verify that user is not able to Edit pricing details of main vehicle on opportunity page$")
	public void Verify_that_user_is_not_able_to_Edit_pricing_details_of_main_vehicle_on_opportunity_page()
			throws Throwable {

		ActionHandler.wait(5);
		if (!VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.EditOpp)) {

			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User is not able to edit the main vehicle's pricing details on opportunity page");
		}
	}

	// Click on edit button
	@Then("^Verify that user is not able to Edit pricing details of Additional vehicle on opportunity page$")
	public void Verify_that_user_is_not_able_to_Edit_pricing_details_of_additional_vehicle_on_opportunity_page()
			throws Throwable {

		ActionHandler.wait(5);
		if (!VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.EditOpp)) {

			CommonFunctions.attachScreenshot();
			Reporter.addStepLog(
					"User is not able to edit the Additional vehicle's pricing details on opportunity page");
		}
	}

	// edit pricing details
	@Then("^User edits the pricing details and saves the opportunity$")
	public void User_edits_the_pricing_details_and_saves_the_opportunity() throws Throwable {

		// javaScriptUtil.scrollIntoView(SVO_OpportunityContainer.pricingDetailsSB);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		ActionHandler.clearAndSetText(SVO_OpportunityContainer.costPriceSB, "100");
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(1);
		ActionHandler.click(SVO_OpportunityContainer.SaveButton);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User edits pricing details");
	}

	// view pricing details section
	@Then("^User is able to view total pricing details of main vehicle on the opportunity page$")
	public void User_is_able_to_view_total_pricing_details_of_main_vehicle_on_the_opportunity_page() throws Throwable {

		driver.navigate().refresh();
		ActionHandler.wait(5);
	
		

		ActionHandler.wait(2);
		ActionHandler.scrollToView(SVO_Pricing_LogicContainer.PricingDetailsOpportunity);
		ActionHandler.wait(1);
		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.TotalCostPrice);
		ActionHandler.wait(1);
		Reporter.addStepLog("User views total cost price details");
		
		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.TotalContribution);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		Reporter.addStepLog("User views total contribution details");
		
		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.TotalRevenue);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		Reporter.addStepLog("User views total revenue details");
		
		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.TotalListPrice);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		Reporter.addStepLog("User views total list price details");
		
		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.TotalRetailPrice);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		Reporter.addStepLog("User views total retail price details");
		ActionHandler.scrollDown();
		CommonFunctions.attachScreenshot();
		
	}

	


	
	// select the opportunity created by Admin
	@Then("^Select the opportunity created by Admin$")
	public void select_the_opportunity_created_by_Admin() throws Throwable {
		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(SVO_OpportunityContainer.recentlyViewed);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(4);
		Reporter.addStepLog("User is able to navigate to the list view");

		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.BespokeOpp);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User is able to list all Bespoke Opportunities");

		// user searches for opportunity and opens it
		ActionHandler.wait(2);
		ActionHandler.click(SVO_OpportunityContainer.OpportunitySel1);
		ActionHandler.wait(3);
		Reporter.addStepLog("Select opportunity");
		CommonFunctions.attachScreenshot();

		driver.navigate().refresh();
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.AdditionalVehicleSectionFirstAVRecord);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens details of created additional vehicle");

		ActionHandler.wait(3);
		ActionHandler.click(SVO_Pricing_LogicContainer.AddVehDropdown);
		ActionHandler.wait(3);
		ActionHandler.click(SVO_Pricing_LogicContainer.EditAddVeh);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on edit button");

	}

	@And("^User moves an Oppotunities to Order Placed stage after accepting the Quote for Retailer Contact \"([^\"]*)\"$")
	public void User_moves_an_opportunities_to_Order_Placed_after_accepting_the_quote(String retailerContact)
			throws Exception {

		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		String quoteName = "SVO Bespoke Test Quote";
		String expirtaionDate = CommonFunctions.selectFutureDate();

		ActionHandler.pageDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuotesLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Quotes page");

		ActionHandler.click(SVOAdditionalvehicleContainer.NewQuoteBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on New button to create new Quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteNameBox, quoteName);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User writes quote name for new quote");
		
		ActionHandler.wait(1);
		ActionHandler.scrollDown();
		ActionHandler.wait(2);

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRetailerNameBox, retailerContact);
		ActionHandler.wait(3);
		ActionHandler.click(SVOItemToapproveContainer.QuoteRetailerContactSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		ActionHandler.click(SVOItemToapproveContainer.QuoteRetailerContacts);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters retailer contact details for new quote");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteCostPrice, "600");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Cost Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteListPrice, "550");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters List Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteContribution, "650");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Contribution for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRetailPrice, "500");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Retail Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRevenue, "700");
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User enters Revenue for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteExpirationDate, expirtaionDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Expiration Date for new quote");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteSaveBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User save new quote");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstQuote);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens created quote details");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.DesignBriefLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Design Brief Section");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstDesignBrief);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens first design brief from the list");

		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefSubmit);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Design Brief status moves to Submitted from Draft");

		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefSaveSubmission);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves Design Brief Submission");

		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefReturnRenderPack);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User returns render pack for Design Brief");

		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefSaveRenderPack);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		Reporter.addStepLog("User saves Design Brief for Return Render Pack");
		ActionHandler.wait(5);
		Reporter.addStepLog("User Navigate to Opportunity");
		driver.navigate().refresh();
		ActionHandler.wait(10);
		javaScriptUtil.clickElementByJS(SVO_Pricing_LogicContainer.OpportTab);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstOpportunityLink);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.pageDown();
		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuotesLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Quotes page");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstQuoteFromOpportunity);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens created quote details");

		ActionHandler.wait(8);
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(8);
		Reporter.addStepLog("Quote status moves to Price stage");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(8);
		Reporter.addStepLog("Quote status moves to Proposal Sent stage");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(10);

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDrpDwn);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeAccepted);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User mark accepeted as Quote Outcome");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDetailDrpDwn);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDetailAccepted);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDoneBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User accepeted the Quote");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuoteOpportunityLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate to Opportunity");

	}

	@Then("^User is able to view total pricing details of both main vehicle and additional vehicle on opportunity page$")
	public void user_is_able_to_view_total_pricing_details_of_both_main_vehicle_and_additional_vehicle_on_opportunity_page()
			throws Throwable {
		
		driver.navigate().refresh();
		ActionHandler.wait(5);
		ActionHandler.pageDown();
		ActionHandler.wait(5);
		ActionHandler.click(SVO_Pricing_LogicContainer.ExistingOpportunityLink);

		ActionHandler.wait(2);
		ActionHandler.scrollToView(SVO_Pricing_LogicContainer.PricingDetailsOpportunity);
		ActionHandler.wait(1);
		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.costPrice);
		ActionHandler.wait(1);
		Reporter.addStepLog("User views total cost price details");
		
		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.contribution);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		Reporter.addStepLog("User views total contribution details");
		
		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.revenue);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		Reporter.addStepLog("User views total revenue details");
		
		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.listPrice);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		Reporter.addStepLog("User views total list price details");
		
		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.retailPrice);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		Reporter.addStepLog("User views total retail price details");
		ActionHandler.scrollDown();
		CommonFunctions.attachScreenshot();

	}

	@Then("^User moves opportunity to Contract Signed stage$")
	public void User_moves_an_Opportunity_to_Contract_Signed_stage() throws Exception {

		ActionHandler.wait(5);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.MarkStageAsComplete);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User moves the Opportunity to Commission from Qualified Stage");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.MarkStageAsComplete);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User moves the Opportunity to Design Brief & Quote from Commission Stage");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.MarkStageAsComplete);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User moves the Opportunity to Contract signed Stage");

	}

	@Then("^User verify the total pricing details of additional vehicle on orders page before adding additional vehicle records$")

	public void User_verify_the_total_pricing_details_of_additional_vehicle_on_orders_page_before_adding_additional_vehicle_records()
			throws Exception {

		TotalRetailPrice = null;

		driver.navigate().refresh();
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.OrdersLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Orders link");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstOrder);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens Order details for the opportunity");

		ActionHandler.click(RestrictedPartyScreeningContainer.CommonOrderNumber1);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(SVOAdditionalvehicleContainer.CommonOrderNumberBox, "123456");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(RestrictedPartyScreeningContainer.SaveCommonOrderNumber1);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User saves common order number");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);

		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);

		TotalRetailPrice = SVO_Pricing_LogicContainer.TotalRetailPriceOnOrdersPage.getText();
		Reporter.addStepLog("Total Retail Price is = " + TotalRetailPrice);

		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.TotalRetailPriceOnOrdersPage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"User verifies the total retail price of an opportunity before adding additional vehicle to the opportunity");

	}

	@Then("^User verify the total pricing details of additional vehicle on orders page after adding additional vehicle records$")
	public void User_verify_the_total_pricing_details_of_additional_vehicle_on_orders_page_after_adding_additional_vehicle_records()
			throws Exception {

		TotalRetailPrice = null;

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.OrdersLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Orders link");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstOrder);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens Order details for the opportunity");

		ActionHandler.click(RestrictedPartyScreeningContainer.CommonOrderNumber1);
		ActionHandler.wait(3);
		ActionHandler.click(RestrictedPartyScreeningContainer.SaveCommonOrderNumber1);
		ActionHandler.wait(5);

		driver.navigate().refresh();
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();

		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);

		TotalRetailPrice = SVO_Pricing_LogicContainer.TotalRetailPriceOnOrdersPage.getText();
		Reporter.addStepLog("Total Retail Price is = " + TotalRetailPrice);

		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.TotalRetailPriceOnOrdersPage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"User verifies the total retail price of an opportunity after adding additional vehicle to the opportunity");

	}

	@Then("^User is able to edit the pricing details of additional vehicle$")
	public void user_is_able_to_edit_the_pricing_details_of_additional_vehicle() throws Throwable {
		ActionHandler.wait(2);
		ActionHandler.pageCompleteScrollUp();
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.AdditionalVehicleSection);
		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.AdditionalVehicleOption);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Additional Vehicle section");

	}

	@Then("^User enters mandatory fields for an Opportunity like Stage \"([^\"]*)\" Product Offering \"([^\"]*)\" Region \"([^\"]*)\" Client \"([^\"]*)\" Restricted Party Screening Stage \"([^\"]*)\" Preferred Contact \"([^\"]*)\" Account Name \"([^\"]*)\" Retailer Contact \"([^\"]*)\" Source \"([^\"]*)\" Origination Divission \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\"$")
	public void user_enters_mandatory_fields_for_an_Opportunity_like_Stage_Product_Offering_Region_Client_Restricted_Party_Screening_Stage_Preferred_Contact_Account_Name_Retailer_Contact_Source_Origination_Divission_Brand_Model(
			String stage, String productOffering, String region, String client, String Checkcleared,
			String preferredContact, String accountName, String retailerContact, String source,
			String originatingdivission, String brand, String model) throws Exception {

		String oppName = "Test_Classic_Service_Opportunity_";
		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		String closeDate = CommonFunctions.selectFutureDate();
		Reporter.addStepLog("Opportunity Close Date is = " + closeDate);

		String s[] = stage.split(",");
		stage = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String proOff[] = productOffering.split(",");
		productOffering = CommonFunctions.readExcelMasterData(proOff[0], proOff[1], proOff[2]);

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String c[] = client.split(",");
		client = CommonFunctions.readExcelMasterData(c[0], c[1], c[2]);

		String RPS[] = Checkcleared.split(",");
		Checkcleared = CommonFunctions.readExcelMasterData(RPS[0], RPS[1], RPS[2]);

		String preCon[] = preferredContact.split(",");
		preferredContact = CommonFunctions.readExcelMasterData(preCon[0], preCon[1], preCon[2]);

		String accName[] = accountName.split(",");
		accountName = CommonFunctions.readExcelMasterData(accName[0], accName[1], accName[2]);

		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		String so[] = source.split(",");
		source = CommonFunctions.readExcelMasterData(so[0], so[1], so[2]);

		String OD[] = originatingdivission.split(",");
		originatingdivission = CommonFunctions.readExcelMasterData(OD[0], OD[1], OD[2]);

		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String m[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		Reporter.addStepLog("User starts creating new Classic Service Opportunity");
		ActionHandler.click(SVOAdditionalvehicleContainer.NewOpportunity);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeOppName, oppName + randomNumber);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Name");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeClosedDate, closeDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Closed date");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeStage);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.StageSelection(stage))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Stage for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeProductOffering);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(
				driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ProOfferingSelection(productOffering))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Product Offering for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.RegionSelection(region))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Region for an Opportunity");

		ActionHandler.wait(2);
		VerifyHandler.verifyElementPresent(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		CommonFunctions.attachScreenshot();
		ActionHandler
				.click(driver.findElement(By.xpath(RestrictedPartyScreeningContainer.ValueSelection(Checkcleared))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects restricted party screening as Check cleared for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeAccountName, accountName);
		ActionHandler.pressEnter();
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeAccountNameSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(accountName))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Account Name for an Opportunity");

		ActionHandler.wait(1);
		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokePreferredContact, preferredContact);
		ActionHandler.click(SVOItemToapproveContainer.SVOBespokePreferredContactname);
		Reporter.addStepLog("User selects Preferred Contact for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeBrand, brand);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeBrandSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(brand))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Brand for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeModel, model);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(model))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Model for an Opportunity");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSource);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.SourceSelection(source))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Source for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSaveOpportunity);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves Full Bespoke Opportunity");

		OpportunityName = SVOAdditionalvehicleContainer.OpportunityName.getText();
		Reporter.addStepLog("Opportunity Name is = " + OpportunityName);
	}

	@Then("^User is not able to view the Total Retail Price filed in Opportunity page$")

	public void user_is_not_able_to_view_the_Total_Retail_Price_filed_in_Opportunity_page() throws Exception {

		javaScriptUtil.scrollIntoView(SVO_Pricing_LogicContainer.RetailPriceOpportunity);
		if (SVO_Pricing_LogicContainer.TotalRetailPriceOpportunity.isDisplayed() == false) {

			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User is not able to view the total Retail field in Opportunity page");
		}

	}

	@Then("^User moves opportunity to Design Brief and Quote stage$")
	public void User_moves_an_Opportunity_to_Design_brief_and_Quote_Stage() throws Exception {

		ActionHandler.click(SVO_Pricing_LogicContainer.OpportunityNameOnAdditionalVehiclePage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate back to the Opportunity");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.MarkStageAsComplete);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User moves the Opportunity to Commission from Qualified Stage");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.MarkStageAsComplete);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User moves the Opportunity to Design Brief & Quote from Commission Stage");

	}

	@Then("^User create private office Quote with all fields like Retailer Contact \"([^\"]*)\"$")
	public void User_create_private_office_Quote_with_all_field(String retailerContact) throws Exception {

		PrivateOfficeQuoteName = null;

		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		String quoteName = "private office Test Quote";
		String expirtaionDate = CommonFunctions.selectFutureDate();

		ActionHandler.pageDown();

		CommonFunctions.attachScreenshot();
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuotesLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Quotes page");

		ActionHandler.click(SVOAdditionalvehicleContainer.NewQuoteBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on New button to create new Quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteNameBox, quoteName + randomNumber);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User writes quote name for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRetailerNameBox, retailerContact);
		ActionHandler.click(RestrictedPartyScreeningContainer.RetailerContactSearch);
		ActionHandler.click(driver
				.findElement(By.xpath(RestrictedPartyScreeningContainer.RetailserContactSelection(retailerContact))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters retailer contact details for new quote");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteCostPrice, "123");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Cost Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteListPrice, "203");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters List Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteContribution, "23");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Contribution for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRetailPrice, "213");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Retail Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRevenue, "113");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Revenue for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteExpirationDate, expirtaionDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Expiration Date for new quote");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteSaveBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User save new quote");

		ActionHandler.wait(3);
		PrivateOfficeQuoteName = RestrictedPartyScreeningContainer.FirstPrivateOfficeQuote.getText();
		Reporter.addStepLog("Quote Name is = " + PrivateOfficeQuoteName);

		javaScriptUtil.clickElementByJS(RestrictedPartyScreeningContainer.FirstPrivateOfficeQuote);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(4);
		Reporter.addStepLog("User opens created quote details");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(4);
		Reporter.addStepLog("Quote status moves to Price stage");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(4);
		Reporter.addStepLog("Quote status moves to Proposal Sent stage");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(4);

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDrpDwn);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeAccepted);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User mark accepeted as Quote Outcome");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDetailDrpDwn);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDetailAccepted);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDoneBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User accepeted the Quote");

	}

	@And("^User verifies total retail price on Quotes page after accepting the Quote for Retailer Contact \"([^\"]*)\"$")
	public void User_verifies_total_retail_price_on_Quotes_page_after_accepting_the_quote(String retailerContact)
			throws Exception {
		String TotalRetailPriceOnOrdersPage = null;
		String TotalAddtionalVehPriceQuotePage = null;

		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		String quoteName = "SVO Bespoke Test Quote";
		ActionHandler.wait(4);
		javaScriptUtil.clickElementByJS(SVO_Pricing_LogicContainer.OpportunityNameOnAdditionalVehiclePage);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		ActionHandler.pageDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuotesLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Quotes page");

		ActionHandler.click(SVOAdditionalvehicleContainer.NewQuoteBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on New button to create new Quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteNameBox, quoteName);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User writes quote name for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRetailerNameBox, retailerContact);
		ActionHandler.click(SVOItemToapproveContainer.QuoteRetailerContact);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters retailer contact details for new quote");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteCostPrice, "123");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Cost Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteListPrice, "203");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters List Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteContribution, "23");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Contribution for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRetailPrice, "213");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Retail Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRevenue, "113");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Revenue for new quote");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteSaveBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User save new quote");

		ActionHandler.wait(3);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstQuote);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens created quote details");

		ActionHandler.wait(3);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.DesignBriefLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Design Brief Section");

		ActionHandler.wait(3);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstDesignBrief);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens first design brief from the list");

		ActionHandler.wait(3);
		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefSubmit);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Design Brief status moves to Submitted from Draft");

		ActionHandler.wait(3);
		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefSaveSubmission);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves Design Brief Submission");

		ActionHandler.wait(3);
		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefReturnRenderPack);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User returns render pack for Design Brief");

		ActionHandler.wait(3);
		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefSaveRenderPack);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		Reporter.addStepLog("User saves Design Brief for Return Render Pack");

		ActionHandler.wait(3);
		javaScriptUtil.clickElementByJS(SVO_OpportunityContainer.OpportTab);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		ActionHandler.click(SVOAdditionalvehicleContainer.FirstOpportunityLink);
		CommonFunctions.attachScreenshot();

		ActionHandler.pageDown();
		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuotesLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Quotes page");

		ActionHandler.wait(3);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstQuoteFromOpportunity);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens created quote details");

		ActionHandler.wait(5);
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Quote status moves to Price stage");

		ActionHandler.wait(5);
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Quote status moves to Proposal Sent stage");

		ActionHandler.wait(5);
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(5);
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDrpDwn);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeAccepted);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User mark accepted as Quote Outcome");

		ActionHandler.wait(5);
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDetailDrpDwn);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDetailAccepted);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(5);
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDoneBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User accepted the Quote");

		ActionHandler.pageDown();
		ActionHandler.pageDown();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.TotalRetailPriceOnOrdersPage);
		Reporter.addStepLog("User verifies the total retail price of an opportunity on Quotes page");

		TotalRetailPriceOnOrdersPage = SVO_Pricing_LogicContainer.TotalRetailPriceOnOrdersPage.getText();
		Reporter.addStepLog("Total Retail Price is = " + TotalRetailPriceOnOrdersPage);

		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.TotalAddtionalVehPriceQuotePage);
		Reporter.addStepLog("User verifies the total additional vehicle price of an opportunity on Quotes page");

		TotalAddtionalVehPriceQuotePage = SVO_Pricing_LogicContainer.TotalAddtionalVehPriceQuotePage.getText();
		Reporter.addStepLog("Total Additional Vehicle Price is = " + TotalAddtionalVehPriceQuotePage);

	}

	@Then("^User is not able to view Total Retail price and Total Additional vehicle price fields on Quotes page of an Opportunity$")
	public void User_is_not_able_to_view_Total_Retail_price_and_Total_Additional_vehicle_price_fields_on_Quotes_page_of_an_Opportunity()
			throws Exception {

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		if (!VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.TotalRetailPrice)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog(
					"User is not able to view total retail price of vehicles on quotes page for Private office opportunity");
		} else {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog(
					"User is able to view total retail price of vehicles on quotes page for Private office opportunity");
		}

		if (!VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.TotalAdditionalVehiclePriceSection)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog(
					"User is not able to view total additional vehicle price on quotes page for Private office opportunity");
		} else {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog(
					"User is able to view total additional vehicle price on quotes page for Private office opportunity");
		}
	}

	@Then("^User is not able to view sales agreement document under agreement section without sending sales agreement$")
	public void User_is_not_able_to_view_sales_agreement_document_under_agreement_section_without_sending_sales_agreement()
			throws Exception {

		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.pageDown();
		ActionHandler.wait(3);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuotesLink);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Quotes page");

		ActionHandler.wait(1);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstQuoteFromOpportunity);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens created quote details");

		ActionHandler.wait(3);
		ActionHandler.click(SVO_Pricing_LogicContainer.SalesAgreementListLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on sales agreement link under related list quick links");

		if (!VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.FirstDesignBrief)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog(
					"User is not able to view sales agreement document under Agreements section on quotes page");
		} else {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog(
					"User is able to view sales agreement document under Agreements section on quotes page");
		}
	}

	@Then("^User is not able to view Total Retail price and Total Additional vehicle price fields on Orders page of an Opportunity$")
	public void User_is_not_able_to_view_Total_Retail_price_and_Total_Additional_vehicle_price_fields_on_Orders_page_of_an_Opportunity()
			throws Exception {

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuoteOpportunityLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate to Opportunity");

		driver.navigate().refresh();
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.OrdersLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Orders link");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstOrder);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens Order details for the opportunity");

		ActionHandler.click(RestrictedPartyScreeningContainer.CommonOrderNumber1);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(SVOAdditionalvehicleContainer.CommonOrderNumberBox, "123456");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(RestrictedPartyScreeningContainer.SaveCommonOrderNumber1);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User saves common order number");

		if (!VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.TotalRetailPrice)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog(
					"User is not able to view total retail price of vehicles on Orders page for Private office opportunity");
		} else {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog(
					"User is able to view total retail price of vehicles on Orders page for Private office opportunity");
		}

		if (!VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.TotalAdditionalVehiclePriceSection)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog(
					"User is not able to view total additional vehicle price on Orders page for Private office opportunity");
		} else {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog(
					"User is able to view total additional vehicle price on Orders page for Private office opportunity");
		}
	}

	@Then("^User is able to view and clear the Restricted Party Screening under details section of an private office opportunity with Restricted Party Screening stage \"([^\"]*)\"$")
	public void user_is_able_to_view_and_clear_the_Restricted_Party_Screening_under_details_section_of_an_private_office_opportunity_with(
			String Checkcleared) throws Throwable {

		String CC[] = Checkcleared.split(",");
		Checkcleared = CommonFunctions.readExcelMasterData(CC[0], CC[1], CC[2]);

		ActionHandler.pageDown();
		ActionHandler.wait(2);

		javaScriptUtil.clickElementByJS(RestrictedPartyScreeningContainer.RestrictedPartyScreeningEditBtn);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		ActionHandler.click(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		CommonFunctions.attachScreenshot();

		ActionHandler
				.click(driver.findElement(By.xpath(RestrictedPartyScreeningContainer.ValueSelection(Checkcleared))));
		CommonFunctions.attachScreenshot();
		ActionHandler.click(RestrictedPartyScreeningContainer.SaveOpportunityDetailsBtn);
		CommonFunctions.attachScreenshot();
		ActionHandler.pageTop();
		ActionHandler.wait(3);
		Reporter.addStepLog("User selects restricted party screening as Check cleared for an Opportunity");

	}

	@Then("^User navigates to quotes page$")
	public void User_navigates_to_quotes_page() throws Throwable {
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.pageDown();
		ActionHandler.wait(3);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuotesLink);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Quotes page");

		ActionHandler.wait(1);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstQuoteFromOpportunity);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens created quote details");
	}

	@Then("^User send sales agreement document of quote$")
	public void User_send_sales_agreement_document_of_quote() throws Throwable {
		ActionHandler.wait(3);
		ActionHandler.click(SVO_OpportunityContainer.sendSalesAgreement);
		ActionHandler.wait(70);
		Reporter.addStepLog("User clicks the Send Sales Agreement button");
		CommonFunctions.attachScreenshot();
		driver.navigate().back();
		ActionHandler.wait(10);

		driver.navigate().refresh();
		ActionHandler.wait(10);

		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);
		javaScriptUtil.clickElementByJS(SVO_Pricing_LogicContainer.SalesAgreementListLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on sales agreement link under related list quick links");

		ActionHandler.click(SVOAdditionalvehicleContainer.SecondDesignBrief);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on Sales Agreement that is generated");

	}

	@Then("^User delete the additional vehicle record of an opportunity$")
	public void User_delete_the_additional_vehicle_record_of_an_opportunity() throws Throwable {

		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleSectionFirstAVRecord);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens details of created additional vehicle");

		ActionHandler.click(SVO_Pricing_LogicContainer.DeleteAdditionalVehicle);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_Pricing_LogicContainer.DeleteAdditionalVehicleBtn);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		Reporter.addStepLog("User deletes the additional vehicle successfully");
	}

	@Then("^User verifies total additional vehicle price on opportunity page$")
	public void user_verifies_total_additional_vehicle_price_on_opportunity_page() throws Throwable {

		TotalAdditionalVehicleRetailPrice = null;
		TotalAdditionalVehicleListPrice = null;
		TotalAdditionalVehicleCostPrice = null;
		TotalAdditionalVehicleContribution = null;
		TotalAdditionalVehicleRevenue = null;

		ActionHandler.click(SVO_Pricing_LogicContainer.OpportunityNameOnAdditionalVehiclePage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate back to the Opportunity");

		driver.navigate().refresh();
		ActionHandler.wait(10);

		ActionHandler.pageDown();
		ActionHandler.wait(2);
		ActionHandler.scrollToView(SVO_Pricing_LogicContainer.TotalAdditionalVehicleRetailPrice);
		ActionHandler.wait(2);

		ActionHandler.pageUp();
		ActionHandler.wait(2);
		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.TotalAdditionalVehicleRetailPrice);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the total additional vehicle retail price of an opportunity");

		TotalAdditionalVehicleRetailPrice = SVO_Pricing_LogicContainer.TotalAdditionalVehicleRetailPrice.getText();
		Reporter.addStepLog("Total Retail Price is = " + TotalAdditionalVehicleRetailPrice);

		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.TotalAdditionalVehicleListPrice);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the total additional vehicle List price on opportunity page");

		TotalAdditionalVehicleListPrice = SVO_Pricing_LogicContainer.TotalAdditionalVehicleListPrice.getText();
		Reporter.addStepLog("Total List Price is = " + TotalAdditionalVehicleListPrice);

		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.TotalAdditionalVehicleCostPrice);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the total additional vehicle cost price of an opportunity");

		TotalAdditionalVehicleCostPrice = SVO_Pricing_LogicContainer.TotalAdditionalVehicleCostPrice.getText();
		Reporter.addStepLog("Total Cost Price is = " + TotalAdditionalVehicleCostPrice);

		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.TotalAdditionalVehicleContribution);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the total additional vehicle contribution of an opportunity");

		TotalAdditionalVehicleContribution = SVO_Pricing_LogicContainer.TotalAdditionalVehicleContribution.getText();
		Reporter.addStepLog("Total Contribution is = " + TotalAdditionalVehicleContribution);

		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.TotalAdditionalVehicleRevenue);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the total additional vehicle revenue of an opportunity");

		TotalAdditionalVehicleRevenue = SVO_Pricing_LogicContainer.TotalAdditionalVehicleRevenue.getText();
		Reporter.addStepLog("Total Revenue is = " + TotalAdditionalVehicleRevenue);

		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(3);
	}

	@Then("^User is able to see additional vehicle price in Quote page$")
	public void user_is_able_to_see_additional_vehicle_price_in_Quote_page() throws Throwable {

		String TotalAdditionalVehiclePriceQuote = null;
		String TotalRetailPriceQuote = null;

		javaScriptUtil.clickElementByJS(SVO_OpportunityContainer.OpportTab);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigate to Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstOpportunityLink);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.scrollDown();
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuotesLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Quotes page");

		ActionHandler.wait(1);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstQuote);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens created quote details");

		ActionHandler.scrollToView(SVO_Pricing_LogicContainer.TotalAdditionalVehiclePriceQuote);
		ActionHandler.wait(5);
		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.TotalAdditionalVehiclePriceQuote);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the total additional vehicle price of an quote");
		TotalAdditionalVehiclePriceQuote = SVO_Pricing_LogicContainer.TotalAdditionalVehiclePriceQuoteValue.getText();
		Reporter.addStepLog("Total Retail Price is = " + TotalAdditionalVehiclePriceQuote);

		ActionHandler.wait(5);
		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.TotalRetailPriceQuote);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the total Retail price of an opportunity");
		TotalRetailPriceQuote = SVO_Pricing_LogicContainer.TotalRetailPriceQuoteValue.getText();
		Reporter.addStepLog("Total Retail Price is = " + TotalRetailPriceQuote);

		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(3);

	}

	@Then("^User create and accept a Design Brief Quote with all fields like Retailer Contact \"([^\"]*)\"$")
	public void User_create_and_accept_a_Design_Brief_Quote_with_all_field(String retailerContact) throws Throwable {
		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		String quoteName = "SVO Bespoke Test Quote";
		String expirtaionDate = CommonFunctions.selectFutureDate();

		ActionHandler.wait(2);
		ActionHandler.pageDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuotesLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Quotes page");

		ActionHandler.wait(1);
		ActionHandler.click(SVOAdditionalvehicleContainer.NewQuoteBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on New button to create new Quote");

		ActionHandler.wait(1);
		if (VerifyHandler.verifyElementPresent(SVOItemToapproveContainer.QuoteNextBtn)) {
			ActionHandler.click(SVOItemToapproveContainer.QuoteNextBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User clicks on Next button to create new Quote");
		}

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteNameBox, quoteName + randomNumber);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User writes quote name for new quote");

		ActionHandler.wait(1);
		ActionHandler.scrollDown();
		ActionHandler.wait(2);

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRetailerNameBox, retailerContact);
		ActionHandler.wait(3);
		ActionHandler.click(SVOItemToapproveContainer.QuoteRetailerContactSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		ActionHandler.click(SVOItemToapproveContainer.QuoteRetailerContacts);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters retailer contact details for new quote");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteCostPrice, "123");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Cost Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteListPrice, "203");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters List Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteContribution, "23");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Contribution for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRetailPrice, "213");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Retail Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRevenue, "113");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Revenue for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteExpirationDate, expirtaionDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Expiration Date for new quote");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteSaveBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User save new quote");

		ActionHandler.wait(3);
		DesignBriefRequestname = SVOAdditionalvehicleContainer.FirstQuote.getText();
		Reporter.addStepLog("Quote Name is = " + DesignBriefRequestname);
		ActionHandler.wait(3);

		ActionHandler.wait(3);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstQuote);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens created quote details");

		ActionHandler.wait(3);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.DesignBriefLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Design Brief Section");

		ActionHandler.wait(3);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstDesignBrief);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens first design brief from the list");

		ActionHandler.wait(3);
		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefSubmit);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Design Brief status moves to Submitted from Draft");

		ActionHandler.wait(3);
		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefSaveSubmission);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves Design Brief Submission");

		ActionHandler.wait(3);
		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefReturnRenderPack);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User returns render pack for Design Brief");

		ActionHandler.wait(3);
		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefSaveRenderPack);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		Reporter.addStepLog("User saves Design Brief for Return Render Pack");

		driver.navigate().refresh();
		ActionHandler.wait(10);
		javaScriptUtil.clickElementByJS(SVO_OpportunityContainer.OpportTab);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		ActionHandler.click(SVOAdditionalvehicleContainer.FirstOpportunityLink);
		CommonFunctions.attachScreenshot();

		ActionHandler.pageDown();
		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuotesLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Quotes page");

		ActionHandler.wait(3);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstQuoteFromOpportunity);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens created quote details");

		ActionHandler.wait(5);
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("Quote status moves to Price stage");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("Quote status moves to Proposal Sent stage");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(10);

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDrpDwn);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeAccepted);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User mark accepeted as Quote Outcome");

		ActionHandler.wait(3);
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDetailDrpDwn);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDetailAccepted);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDoneBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User accepeted the Quote");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuoteOpportunityLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate to Opportunity");

	}

	@Then("^Verify that updated pricing details for additional vehicle record on additional vehicle page$")
	public void Verify_that_updated_pricing_details_for_additional_vehicle_record_on_additional_vehicle_page()
			throws Exception {

		AdditionalVehicleCostPrice = null;
		AdditionalVehicleRetailPrice = null;
		AdditionalVehicleListPrice = null;
		AdditionalVehicleContribution = null;
		AdditionalVehicleRevenue = null;

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.AdditionalVehicleLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate to additional vehicle quick link");

		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleSectionFirstAVRecord);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens details of created additional vehicle");

		ActionHandler.pageDown();
		ActionHandler.wait(2);
		ActionHandler.scrollToView(SVO_Pricing_LogicContainer.AdditionalVehicleCostPriceDetails);
		ActionHandler.wait(2);

		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.AdditionalVehicleCostPriceDetails);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the cost price of additional vehicle");

		AdditionalVehicleCostPrice = SVO_Pricing_LogicContainer.AdditionalVehicleCostPriceDetails.getText();
		Reporter.addStepLog("Additional vehicle cost Price is = " + AdditionalVehicleCostPrice);

		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.AdditionalVehicleContributionPriceDetails);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the contribution price of additional vehicle");

		AdditionalVehicleContribution = SVO_Pricing_LogicContainer.AdditionalVehicleContributionPriceDetails.getText();
		Reporter.addStepLog("Additional Vehicle contribution is = " + AdditionalVehicleContribution);

		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.AdditionalVehicleRevenuePriceDetails);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the revenue of additional vehicle");

		AdditionalVehicleRevenue = SVO_Pricing_LogicContainer.AdditionalVehicleRevenuePriceDetails.getText();
		Reporter.addStepLog("Additional Vehicle revenue is = " + AdditionalVehicleRevenue);

		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.AdditionalVehicleListPriceDetails);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the List price of additional vehicle");

		AdditionalVehicleListPrice = SVO_Pricing_LogicContainer.AdditionalVehicleListPriceDetails.getText();
		Reporter.addStepLog("Additional vehicle List Price is = " + AdditionalVehicleListPrice);

		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.AdditionalVehicleRetailPriceDetails);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the Retail price of additional vehicle");

		AdditionalVehicleRetailPrice = SVO_Pricing_LogicContainer.AdditionalVehicleRetailPriceDetails.getText();
		Reporter.addStepLog("Additional vehicle retail price is = " + AdditionalVehicleRetailPrice);

		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(3);
	}

	@And("^User adds multiple Vehicle Record with fields like Brand \"([^\"]*)\", Model \"([^\"]*)\", Series Name \"([^\"]*)\", Derivative \"([^\"]*)\" and Model Year \"([^\"]*)\"$")
	public void User_adds_multiple_Vehicle_Record_With_Fields(String Brand, String Model, String seriesName,
			String derivative, String modelYear) throws Exception {
		String b[] = Brand.split(",");
		Brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String m[] = Model.split(",");
		Model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		String series[] = seriesName.split(",");
		seriesName = CommonFunctions.readExcelMasterData(series[0], series[1], series[2]);

		String deri[] = derivative.split(",");
		derivative = CommonFunctions.readExcelMasterData(deri[0], deri[1], deri[2]);

		String my[] = modelYear.split(",");
		modelYear = CommonFunctions.readExcelMasterData(my[0], my[1], my[2]);

		driver.navigate().refresh();
		ActionHandler.wait(10);
		ActionHandler.click(SVOAdditionalvehicleContainer.NewAdditionalVehicleBtn1);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on New button after navigating to Additional vehicle section");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);

		CommonFunctions.attachScreenshot();

		

		ActionHandler.setText(SVOAdditionalvehicleContainer.AdditionalVehicleBrand, Brand);

		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeBrandSearch);

		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(Brand))));

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Brand for an Additional vehicle record");

		ActionHandler.setText(SVOAdditionalvehicleContainer.AdditionalVehicleModel, Model);

		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);

		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(Model))));

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Model for an Additional vehicle record");

		ActionHandler.setText(SVOAdditionalvehicleContainer.AdditionalVehicleSeriesName, seriesName);

		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleVehicleSeriesNameSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(seriesName))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Series Name for an Additional vehicle record");

		ActionHandler.setText(SVOAdditionalvehicleContainer.AdditionalVehicleDerivative, derivative);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleVehicleDerivativeSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(derivative))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Derivative for an Additional vehicle record");

		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleModelYear);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ModelYearSelection(modelYear))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects model year for an Additional vehicle record");

		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleWheelBase);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleWheelBaseStandard);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Wheel base as Standard Wheelbase for an Additional vehicle record");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSaveOpportunity);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("One additional vehicle record has been added successfully");

	}

	@And("^Verify that user is not able to add new additional vehicle to the opportunity after accepting the quote with Brand \"([^\"]*)\", Model \"([^\"]*)\", Series Name \"([^\"]*)\", Derivative \"([^\"]*)\" and Model Year \"([^\"]*)\"$")
	public void Verify_that_user_is_not_able_to_add_new_additional_vehicle_to_the_opportunity_after_accepting_the_quote_with(
			String Brand, String Model, String seriesName, String derivative, String modelYear) throws Exception {

		AdditionalVehicleRetailPrice = null;

		String b[] = Brand.split(",");
		Brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String m[] = Model.split(",");
		Model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		String series[] = seriesName.split(",");
		seriesName = CommonFunctions.readExcelMasterData(series[0], series[1], series[2]);

		String deri[] = derivative.split(",");
		derivative = CommonFunctions.readExcelMasterData(deri[0], deri[1], deri[2]);

		String my[] = modelYear.split(",");
		modelYear = CommonFunctions.readExcelMasterData(my[0], my[1], my[2]);

		ActionHandler.wait(3);
		driver.navigate().refresh();
		ActionHandler.wait(10);

		ActionHandler.click(SVOAdditionalvehicleContainer.NewAdditionalVehicleBtn1);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on New button after navigating to Additional vehicle section");

		ActionHandler.wait(1);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(1);
		ActionHandler.setText(SVOAdditionalvehicleContainer.AdditionalVehicleBrand, Brand);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeBrandSearch);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(Brand))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Brand for an Additional vehicle record");

		ActionHandler.wait(1);
		ActionHandler.setText(SVOAdditionalvehicleContainer.AdditionalVehicleModel, Model);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(Model))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Model for an Additional vehicle record");

		ActionHandler.wait(1);
		ActionHandler.setText(SVOAdditionalvehicleContainer.AdditionalVehicleSeriesName, seriesName);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleVehicleSeriesNameSearch);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(seriesName))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Series Name for an Additional vehicle record");

		ActionHandler.wait(1);
		ActionHandler.setText(SVOAdditionalvehicleContainer.AdditionalVehicleDerivative, derivative);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleVehicleDerivativeSearch);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(derivative))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Derivative for an Additional vehicle record");

		ActionHandler.wait(1);
		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleModelYear);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ModelYearSelection(modelYear))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects model year for an Additional vehicle record");

		ActionHandler.wait(1);
		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleWheelBase);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.AdditionalVehicleWheelBaseStandard);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Wheel base as Standard Wheelbase for an Additional vehicle record");

		ActionHandler.wait(1);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSaveOpportunity);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.ErrorMessageOnAdditionalVehiclePage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"User verifies the error message stating 'You cannot add a new vehicle as Quote is already Accepted ' on additional vehicle page");

		ActionHandler.click(SVO_Pricing_LogicContainer.CancelBtnOnAddtitionalVehiclePage);


		}
		
	@Then("^Verify that pricing details are not updated on quotes page$")
	public void Verify_that_pricing_details_are_not_updated_on_quotes_page() throws Throwable {
	    
	    
	    ActionHandler.pageCompleteScrollUp();
	    ActionHandler.wait(1);
	    ActionHandler.pageTop();
	    ActionHandler.wait(1);
	    javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuotesLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Quotes page");
		
		ActionHandler.wait(3);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstQuote);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens created quote details");
		
	    VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.TotalRetailPriceOnQuotePage);
		Reporter.addStepLog("User verifies the total retail price of an opportunity on Quotes page");

		TotalRetailPriceOnOrdersPage = SVO_Pricing_LogicContainer.TotalRetailPriceOnOrdersPage.getText();
		Reporter.addStepLog("Total Retail Price on Quotes page is = " + TotalRetailPriceOnOrdersPage);
		Reporter.addStepLog("Verify that updated Total retail price on opportunity page is not flow to quotes page");
	}
		
	@Then("^User verifies total pricing details of both main and additional vehicle on opportunity page$")
	public void user_verifies_total_pricing_details_of_both_main_and_additional_vehicle_on_opportunity_page()
			throws Throwable {

		String TotalRetailPrice = null;
		String TotalListPrice = null;
		String TotalCostPrice = null;
		String TotalContribution = null;
		String TotalRevenue = null;

		/*ActionHandler.click(SVO_Pricing_LogicContainer.OpportunityNameOnAdditionalVehiclePage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate back to the Opportunity");*/

		//ActionHandler.pageDown();
		ActionHandler.wait(2);
		ActionHandler.scrollToView(SVO_Pricing_LogicContainer.TotalRetailPrice);
		ActionHandler.wait(2);

		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.TotalRetailPrice);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the total retail price of an opportunity");

		TotalRetailPrice = SVO_Pricing_LogicContainer.TotalRetailPrice.getText();
		Reporter.addStepLog("Total Retail Price is = " + TotalRetailPrice);

		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.TotalListPrice);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the total list price of an opportunity");

		TotalListPrice = SVO_Pricing_LogicContainer.TotalListPrice.getText();
		Reporter.addStepLog("Total List Price is = " + TotalListPrice);

		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.TotalCostPrice);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the total cost price of an opportunity");

		TotalCostPrice = SVO_Pricing_LogicContainer.TotalCostPrice.getText();
		Reporter.addStepLog("Total Cost Price is = " + TotalCostPrice);

		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.TotalContribution);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the total contribution of an opportunity");

		TotalContribution = SVO_Pricing_LogicContainer.TotalContribution.getText();
		Reporter.addStepLog("Total Contribution is = " + TotalContribution);

		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.TotalRevenue);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the total revenue of an opportunity");

		TotalRevenue = SVO_Pricing_LogicContainer.TotalRevenue.getText();
		Reporter.addStepLog("Total Revenue is = " + TotalRevenue);

		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(3);
	}
	
	@Then("^User navigate back to an Opportunity page$")
	public void user_navigate_back_to_an_Opportunity_page() throws Throwable {

		javaScriptUtil.clickElementByJS(SVO_OpportunityContainer.OpportTab);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Navigate to Opportunity");

		/*
		 * ActionHandler.click(SVOItemToapproveContainer.SelectOpportunityListView);
		 * CommonFunctions.attachScreenshot();
		 * Reporter.addStepLog("user searches for opportunities list to display");
		 * 
		 * ActionHandler.click(SVOItemToapproveContainer.AllOpportunitiestab);
		 * 
		 * Reporter.addStepLog("User chooses to view selected type opportunities");
		 * CommonFunctions.attachScreenshot();
		 */

		ActionHandler.click(SVOItemToapproveContainer.OpportunitysearchBox);
		ActionHandler.wait(2);
		ActionHandler.setText(SVOItemToapproveContainer.OpportunitysearchBox, OpportunityName);
		ActionHandler.wait(3);
		ActionHandler.pressEnter();
		ActionHandler.wait(6);

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstOpportunityLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate back to the opportunity page");

	}

	@Then("^User is able to view total pricing details of main vehicle and additional vehicle on opportunity page$")
	public void user_is_able_to_view_total_pricing_details_of_main_vehicle_and_additional_vehicle_on_opportunity_page()
			throws Throwable {
		
		
		ActionHandler.wait(5);
		ActionHandler.click(SVO_Pricing_LogicContainer.OpportunityNameLink);

		ActionHandler.wait(5);
		driver.navigate().refresh();
		ActionHandler.wait(10);
	
		

		ActionHandler.wait(2);
		ActionHandler.scrollToView(SVO_Pricing_LogicContainer.PricingDetailsOpportunity);
		ActionHandler.wait(1);
		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.TotalCostPrice);
		ActionHandler.wait(1);
		Reporter.addStepLog("User views total cost price details");
		
		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.TotalContribution);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		Reporter.addStepLog("User views total contribution details");
		
		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.TotalRevenue);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		Reporter.addStepLog("User views total revenue details");
		
		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.TotalListPrice);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		Reporter.addStepLog("User views total list price details");
		
		VerifyHandler.verifyElementPresent(SVO_Pricing_LogicContainer.TotalRetailPrice);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		Reporter.addStepLog("User views total retail price details");
		ActionHandler.scrollDown();
		CommonFunctions.attachScreenshot();
		

	}

	}



