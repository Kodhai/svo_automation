package com.jlr.svo.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.containers.SVOAdditionalVehicleContainer;
import com.jlr.svo.containers.SVOCommentFieldAdditionContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.containers.SVOItemToApproveContainer;
import com.jlr.svo.containers.SVOOpportunityContainer;
import com.jlr.svo.containers.SVOWorkOrderContainer;
import com.jlr.svo.containers.SVO_CancelOrderAtAnyStageContainer;
import com.jlr.svo.containers.SVO_RestrictedPartyScreeningContainer;
import com.jlr.svo.containers.SVO_WorkOrder_AdditionalOwner_Container;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class SVO_CancelOrderAtAnyStage_Test extends TestBaseCC {
	private WebDriver driver = getDriver();
	private static final Logger LOGGER = LoggerFactory.getLogger(SVO_CancelOrderAtAnyStage_Test.class.getName());
	SVOCommentFieldAdditionContainer SVOCommentFieldAdditionContainer = PageFactory.initElements(driver,
			SVOCommentFieldAdditionContainer.class);
	SVOWorkOrderContainer SVOWorkOrderContainer = PageFactory.initElements(driver, SVOWorkOrderContainer.class);

	public static String WorkOrderNumber;
	public static String verificationCode;
	public static String vCode;
	public static String veriCode;
	public static String OwnerName;
	public static String AdditionalOwnerName;
	public static String OpportunityName;
	public static String OpportunityStage;
	public static String OrderStatus;
	public static String OrderOutcome;
	public static String LostReason;

	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	SVO_RestrictedPartyScreeningContainer RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
			SVO_RestrictedPartyScreeningContainer.class);

	SVOItemToApproveContainer SVOItemToapproveContainer = PageFactory.initElements(driver,
			SVOItemToApproveContainer.class);
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
	SVOAdditionalVehicleContainer SVOAdditionalvehicleContainer = PageFactory.initElements(driver,
			SVOAdditionalVehicleContainer.class);
	SVO_WorkOrder_AdditionalOwner_Container SVO_WorkOrder_AdditionalOwner_Container = PageFactory.initElements(driver,
			SVO_WorkOrder_AdditionalOwner_Container.class);
	SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SVO_CancelOrderAtAnyStageContainer SVO_CancelOrderAtAnyStageContainer = PageFactory.initElements(driver,
			SVO_CancelOrderAtAnyStageContainer.class);

	@Then("^User is not able to cancel the order once the order is retailed on Orders page with outcome stage \"([^\"]*)\"$")
	public void user_is_not_able_to_cancel_the_order_once_the_order_is_retailed_on_Orders_page_with_outcome_stage(
			String OutcomeStage) throws Throwable {

		String OS[] = OutcomeStage.split(",");
		OutcomeStage = CommonFunctions.readExcelMasterData(OS[0], OS[1], OS[2]);

		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);

		javaScriptUtil.clickElementByJS(SVO_CancelOrderAtAnyStageContainer.EditOutcomeBtn);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVO_CancelOrderAtAnyStageContainer.OutcomeDropdown);
		CommonFunctions.attachScreenshot();

		ActionHandler
				.click(driver.findElement(By.xpath(SVO_CancelOrderAtAnyStageContainer.OrderOucomeStage(OutcomeStage))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Outcome for an Order");

		ActionHandler.click(SVO_CancelOrderAtAnyStageContainer.SaveOrder);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVO_CancelOrderAtAnyStageContainer.ErrorMessageOnOrdersPage)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User not able to cancel the order once the order is retailed on Orders page");
		} else {
			Reporter.addStepLog("User not able to cancel the order once the order is retailed on Orders page");
		}

		ActionHandler.click(SVO_CancelOrderAtAnyStageContainer.CancelOrder);
		CommonFunctions.attachScreenshot();
	}

	@Then("^User is not able to cancel order at any stage without moving order to outcome stage$")
	public void user_is_not_able_to_cancel_order_at_any_stage_without_moving_order_to_outcome_stage() throws Throwable {

		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(3);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);

		OrderStatus = SVO_CancelOrderAtAnyStageContainer.OrderStatus.getText();
		Reporter.addStepLog("Order status is = " + OrderStatus);
		Reporter.addStepLog("User verifies the order status is set to be 'Created' on orders page");

		OrderOutcome = SVO_CancelOrderAtAnyStageContainer.OrderOutcomeStatus.getText();
		Reporter.addStepLog("Order outcome is = " + OrderOutcome);
		Reporter.addStepLog("User is not able to cancel order at any stage without moving order to outcome stage");
	}

	@And("^User moves an Oppotunity to Order Placed stage after accepting the Quote \"([^\"]*)\"$")
	public void User_moves_an_opportunity_to_Order_Placed_accepting_quote(String retailerContact) throws Exception {
		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		String quoteName = "SVO Bespoke Test Quote";
		String expirtaionDate = CommonFunctions.selectFutureDate();

		ActionHandler.pageDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuotesLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Quotes page");

		ActionHandler.click(SVOAdditionalvehicleContainer.NewQuoteBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on New button to create new Quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteNameBox, quoteName);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User writes quote name for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRetailerNameBox, retailerContact);
		ActionHandler.click(SVOItemToapproveContainer.QuoteRetailerContact);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters retailer contact details for new quote");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteCostPrice, "123");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Cost Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteListPrice, "203");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters List Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteContribution, "23");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Contribution for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRetailPrice, "213");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Retail Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRevenue, "113");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Revenue for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteExpirationDate, expirtaionDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Expiration Date for new quote");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteSaveBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User save new quote");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstQuote);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens created quote details");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.DesignBriefLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Design Brief Section");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstDesignBrief);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens first design brief from the list");

		ActionHandler.wait(10);
		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefSubmit);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Design Brief status moves to Submitted from Draft");

		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefSaveSubmission);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves Design Brief Submission");

		ActionHandler.wait(6);
		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefReturnRenderPack);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User returns render pack for Design Brief");

		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefSaveRenderPack);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves Design Brief for Return Render Pack");

		javaScriptUtil.clickElementByJS(SVO_OpportunityContainer.OpportTab);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(8);
		ActionHandler.click(SVOAdditionalvehicleContainer.FirstOpportunityLink);
		CommonFunctions.attachScreenshot();

		ActionHandler.pageDown();
		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuotesLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Quotes page");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstQuoteFromOpportunity);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens created quote details");

		ActionHandler.wait(8);
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(8);
		Reporter.addStepLog("Quote status moves to Price stage");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(8);
		Reporter.addStepLog("Quote status moves to Proposal Sent stage");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDrpDwn);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeAccepted);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User mark accepeted as Quote Outcome");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDetailDrpDwn);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDetailAccepted);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDoneBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User accepeted the Quote");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuoteOpportunityLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate to Opportunity");

		driver.navigate().refresh();
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.OrdersLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Orders link");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstOrder);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens Order details for the opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.CommonOrderNumber);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(SVOAdditionalvehicleContainer.CommonOrderNumberBox, "123456");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(RestrictedPartyScreeningContainer.SaveCommonOrderNumber1);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User saves common order number");

	}


	@Then("^User cancel the order with outcome stage \"([^\"]*)\" on Orders page$")
	public void user_cancel_the_order_with_outcome_stage_on_Orders_page(String OutcomeStage) throws Throwable {

		String OS[] = OutcomeStage.split(",");
		OutcomeStage = CommonFunctions.readExcelMasterData(OS[0], OS[1], OS[2]);

		javaScriptUtil.clickElementByJS(SVO_CancelOrderAtAnyStageContainer.OrderOutcome);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on outcome stage");

		ActionHandler.click(SVO_CancelOrderAtAnyStageContainer.MarkStatusAsComplete);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Mark status");

		ActionHandler.wait(10);
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDrpDwn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Order outcome stage drop down list");

		ActionHandler.click(driver
				.findElement(By.xpath(SVO_CancelOrderAtAnyStageContainer.OrderOucomeStageSelection(OutcomeStage))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Region for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDoneBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on done button");
		Reporter.addStepLog("User cancel the order on orders page");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);

		OrderStatus = SVO_CancelOrderAtAnyStageContainer.OrderStatus.getText();
		Reporter.addStepLog("Order status is = " + OrderStatus);
		Reporter.addStepLog("User verifies the order status is set to be outcome after cancel the order");

		OrderOutcome = SVO_CancelOrderAtAnyStageContainer.OrderOutcomeStatus.getText();
		Reporter.addStepLog("Order outcome is = " + OrderOutcome);
		Reporter.addStepLog("User verifies the order outcome is set to be cancelled after cancel the order");

	}

	@Then("^User move the order to committed to build stage$")
	public void user_move_the_order_to_committed_to_build_stage() throws Throwable {

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(15);
		Reporter.addStepLog("User moves the Opportunity to Committed to Build Stage");
	}

	@Then("^User move the order to Built stage$")
	public void user_move_the_order_to_Built_stage() throws Throwable {

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User moves the Opportunity to Committed to Build Stage");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User moves the Opportunity to built Stage");
	}

	@Then("^User move the order to Accepted by sales stage$")
	public void user_move_the_order_to__Accepted_by_sales_stage() throws Throwable {
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User moves the Opportunity to Committed to Build Stage");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User moves the Opportunity to built Stage");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User moves the Opportunity to Accepted by sales");
	}

	@Then("^User move the order to Retailed stage$")
	public void user_move_the_order_to_Retailed_stage() throws Throwable {
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(10);
		Reporter.addStepLog("User moves the Opportunity to Committed to Build Stage");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(10);
		Reporter.addStepLog("User moves the Opportunity to built Stage");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(10);
		Reporter.addStepLog("User moves the Opportunity to Accepted by sales");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(10);
		Reporter.addStepLog("User moves the Opportunity to Retailed stage");
	}

	@Then("^User Retail the order with outcome stage \"([^\"]*)\" on Orders page$")
	public void user_Retail_the_order_with_outcome_stage_on_Orders_page(String OutcomeStage) throws Throwable {
		String OS[] = OutcomeStage.split(",");
		OutcomeStage = CommonFunctions.readExcelMasterData(OS[0], OS[1], OS[2]);

		javaScriptUtil.clickElementByJS(SVO_CancelOrderAtAnyStageContainer.OrderOutcome);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on outcome stage");

		ActionHandler.click(SVO_CancelOrderAtAnyStageContainer.MarkStatusAsComplete);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Mark status");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDrpDwn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Order outcome stage drop down list");

		ActionHandler.click(driver
				.findElement(By.xpath(SVO_CancelOrderAtAnyStageContainer.OrderOucomeStageSelection(OutcomeStage))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Region for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDoneBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on done button");
		Reporter.addStepLog("User Retail the order on orders page");

		OrderStatus = SVO_CancelOrderAtAnyStageContainer.OrderStatus.getText();
		Reporter.addStepLog("Order status is = " + OrderStatus);
		Reporter.addStepLog("User verifies the order status is set to be outcome after retail the order");

		OrderOutcome = SVO_CancelOrderAtAnyStageContainer.OrderOutcomeStatus.getText();
		Reporter.addStepLog("Order outcome is = " + OrderOutcome);
		Reporter.addStepLog("User verifies the order outcome is set to be retailed after retail the order");
	}

	@Then("^Verify that order status is displayed with outcome as cancelled by customer$")
	public void Verify_that_order_status_is_displayed_with_outcome_as_cancelled_by_customer() throws Throwable {

		ActionHandler.wait(5);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);

		VerifyHandler.verifyElementPresent(SVO_CancelOrderAtAnyStageContainer.StatusOutcome);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Order is status is updated to Outcome");

		VerifyHandler.verifyElementPresent(SVO_CancelOrderAtAnyStageContainer.OutcomeCancelledByCustomer);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Order outcome is displayed as cancelled by customer");
	}

	@Then("^Create a quote with Retailer Contact \"([^\"]*)\" ,accept the created quote and add order number$")
	public void create_a_quote_with_Retailer_Contact_accept_the_created_quote_and_add_order_number(
			String retailerContact) throws Throwable {
		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		String quoteName = "SVO Bespoke Test Quote";

		ActionHandler.pageDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuotesLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Quotes page");

		ActionHandler.click(SVOAdditionalvehicleContainer.NewQuoteBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on New button to create new Quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteNameBox, quoteName);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User writes quote name for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRetailerNameBox, retailerContact);
		ActionHandler.wait(3);
		ActionHandler.click(SVOItemToapproveContainer.QuoteRetailerContact);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters retailer contact details for new quote");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteCostPrice, "123");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Cost Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteListPrice, "203");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters List Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteContribution, "23");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Contribution for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRetailPrice, "213");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Retail Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRevenue, "113");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Revenue for new quote");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteSaveBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User save new quote");

		ActionHandler.wait(3);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstQuote);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens created quote details");

		ActionHandler.wait(3);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.DesignBriefLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Design Brief Section");

		ActionHandler.wait(3);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstDesignBrief);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens first design brief from the list");

		ActionHandler.wait(3);
		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefSubmit);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Design Brief status moves to Submitted from Draft");

		ActionHandler.wait(3);
		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefSaveSubmission);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves Design Brief Submission");

		ActionHandler.wait(3);
		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefReturnRenderPack);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User returns render pack for Design Brief");

		ActionHandler.wait(3);
		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefSaveRenderPack);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		Reporter.addStepLog("User saves Design Brief for Return Render Pack");

		ActionHandler.wait(3);
		javaScriptUtil.clickElementByJS(SVO_OpportunityContainer.OpportTab);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		ActionHandler.setText(SVOAdditionalvehicleContainer.OpportunitySearchBox, OpportunityName);
		Actions act5 = new Actions(driver);
		act5.sendKeys(Keys.ENTER).build().perform();
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		ActionHandler.click(SVOAdditionalvehicleContainer.FirstOpportunityLink);
		CommonFunctions.attachScreenshot();

		ActionHandler.pageDown();
		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuotesLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Quotes page");

		ActionHandler.wait(3);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstQuoteFromOpportunity);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens created quote details");

		ActionHandler.wait(5);
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("Quote status moves to Price stage");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("Quote status moves to Proposal Sent stage");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(10);

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDrpDwn);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeAccepted);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User mark accepeted as Quote Outcome");

		ActionHandler.wait(3);
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDetailDrpDwn);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDetailAccepted);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDoneBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User accepeted the Quote");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuoteOpportunityLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate to Opportunity");

		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.OrdersLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Orders link");

		ActionHandler.wait(3);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstOrder);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens Order details for the opportunity");

		ActionHandler.wait(3);
		ActionHandler.click(RestrictedPartyScreeningContainer.CommonOrderNumber1);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		ActionHandler.setText(SVOAdditionalvehicleContainer.CommonOrderNumberBox, "123456");
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		ActionHandler.click(RestrictedPartyScreeningContainer.SaveCommonOrderNumber1);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User saves common order number");
	}

	@Then("^Move the order from Created to Committed to Build stage$")
	public void Move_the_order_from_created_to_Committed_to_Build_stage() throws Throwable {
		driver.navigate().refresh();
		ActionHandler.wait(20);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(8);
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(8);
		Reporter.addStepLog("Order moves to Committed to build stage");

	}

	@Then("^Move the order from Created to Built stage$")
	public void Move_the_order_from_created_to_built_stage() throws Throwable {
		driver.navigate().refresh();
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(8);
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(8);
		Reporter.addStepLog("Order moves to Committed to build stage");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(8);
		Reporter.addStepLog("Order moves to Built stage");

	}

	@Then("^Move the order from Created to Accepted by Sales stage$")
	public void Move_the_order_from_created_to_Accepted_by_Sales_stage() throws Throwable {
		driver.navigate().refresh();
		ActionHandler.wait(25);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(8);
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(8);
		Reporter.addStepLog("Order moves to Committed to build stage");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(8);
		Reporter.addStepLog("Order moves to Built stage");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(8);
		Reporter.addStepLog("Order moves to Accepted by Sales stage");

	}

	@Then("^Edit the order status to \"([^\"]*)\" and outcome as \"([^\"]*)\"$")
	public void edit_the_order_status_to_and_outcome_as(String Status, String Outcome) throws Throwable {
		String OS[] = Outcome.split(",");
		Outcome = CommonFunctions.readExcelMasterData(OS[0], OS[1], OS[2]);

		javaScriptUtil.clickElementByJS(SVO_CancelOrderAtAnyStageContainer.OrderOutcome);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on outcome stage");

		ActionHandler.click(SVO_CancelOrderAtAnyStageContainer.MarkStatusAsComplete);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Mark status");

		ActionHandler.wait(10);
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDrpDwn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Order outcome stage drop down list");

		ActionHandler.click(
				driver.findElement(By.xpath(SVO_CancelOrderAtAnyStageContainer.OrderOucomeStageSelection(Outcome))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Region for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDoneBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on done button");
		Reporter.addStepLog("User cancel the order on orders page");
	}

	@Then("^Navigate back to opportunity and verify the opportunity is in 'Closed Won' stage$")
	public void navigate_back_to_opportunity_and_verify_the_opportunity_is_in_Closed_Won_stage() throws Throwable {
		ActionHandler.wait(3);
		ActionHandler.click(SVO_CancelOrderAtAnyStageContainer.OpportunityLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to opportunity");

		VerifyHandler.verifyElementPresent(SVO_CancelOrderAtAnyStageContainer.CheckStage);
		Reporter.addStepLog("User verifies opportunity is in Closed Won stage");

	}

	@Then("^User verifies that opportunity stage on opportunity page$")
	public void user_verifies_that_opportunity_stage_on_opportunity_page() throws Throwable {

		OpportunityStage = null;
		LostReason = null;

		OpportunityStage = SVO_CancelOrderAtAnyStageContainer.OpportunityStage.getText();
		Reporter.addStepLog("Opportunity stage is = " + OpportunityStage);
		Reporter.addStepLog("The opportunity stage is displayed as " + OpportunityStage + " on opportunity page");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);

		if (OpportunityStage.equals("Closed Lost")) {
			LostReason = SVO_CancelOrderAtAnyStageContainer.LostReasonOnOpportunityPage.getText();
			Reporter.addStepLog("Lost reason is = " + LostReason);
			Reporter.addStepLog("Opportunities lost reason is displayed as " + LostReason + "on opportunity page");
		}

	}

	@Then("^Create a quote with Retailer Contact \"([^\"]*)\" and accept the created quote$")
	public void create_a_quote_with_Retailer_Contact_and_accept_the_created_quote(String retailerContact)
			throws Throwable {
		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		String quoteName = "SVO Bespoke Test Quote";
		String expirtaionDate = CommonFunctions.selectFutureDate();

		ActionHandler.pageDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuotesLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Quotes page");

		ActionHandler.click(SVOAdditionalvehicleContainer.NewQuoteBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on New button to create new Quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteNameBox, quoteName);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User writes quote name for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRetailerNameBox, retailerContact);
		ActionHandler.click(SVOItemToapproveContainer.QuoteRetailerContact);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters retailer contact details for new quote");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteCostPrice, "123");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Cost Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteListPrice, "203");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters List Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteContribution, "23");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Contribution for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRetailPrice, "213");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Retail Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRevenue, "113");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Revenue for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteExpirationDate, expirtaionDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Expiration Date for new quote");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteSaveBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User save new quote");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstQuote);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens created quote details");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.DesignBriefLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Design Brief Section");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstDesignBrief);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens first design brief from the list");

		ActionHandler.wait(10);
		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefSubmit);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Design Brief status moves to Submitted from Draft");

		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefSaveSubmission);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves Design Brief Submission");

		ActionHandler.wait(6);
		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefReturnRenderPack);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User returns render pack for Design Brief");

		ActionHandler.click(SVOAdditionalvehicleContainer.DesignBriefSaveRenderPack);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves Design Brief for Return Render Pack");

		javaScriptUtil.clickElementByJS(SVO_OpportunityContainer.OpportTab);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(8);
		ActionHandler.click(SVOAdditionalvehicleContainer.FirstOpportunityLink);
		CommonFunctions.attachScreenshot();

		ActionHandler.pageDown();
		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuotesLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Quotes page");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstQuoteFromOpportunity);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens created quote details");

		ActionHandler.wait(8);
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(8);
		Reporter.addStepLog("Quote status moves to Price stage");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(8);
		Reporter.addStepLog("Quote status moves to Proposal Sent stage");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDrpDwn);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeAccepted);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User mark accepeted as Quote Outcome");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDetailDrpDwn);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDetailAccepted);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteOutcomeDoneBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User accepeted the Quote");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuoteOpportunityLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate to Opportunity");

		driver.navigate().refresh();
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.OrdersLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Orders link");

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstOrder);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens Order details for the opportunity");

	}

	@Then("^Verify the error message when user is not able to cancel the order$")
	public void Verify_the_error_message_when_user_is_not_able_to_cancel_the_order() throws Throwable {

		ActionHandler.wait(5);
		VerifyHandler.verifyElementPresent(SVO_CancelOrderAtAnyStageContainer.CancelErrorMsg);
		CommonFunctions.attachScreenshot();
		// ActionHandler.click(SVO_CancelOrderAtAnyStageContainer.QuoteCancelBtn);
		Reporter.addStepLog(
				"User verifies the error message error message when user is not able to cancel the order on order page");
	}

	@Then("^Move the order from Created to At Retailer stage$")
	public void Move_the_order_from_created_to_At_Retailer_stage() throws Throwable {
		driver.navigate().refresh();
		ActionHandler.wait(25);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(8);
		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(8);
		Reporter.addStepLog("Order moves to Committed to build stage");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(8);
		Reporter.addStepLog("Order moves to Built stage");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(8);
		Reporter.addStepLog("Order moves to Accepted by Sales stage");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteMarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(8);
		Reporter.addStepLog("Order moves to At Retailer stage");
	}

	@Then("^Verify the error message when user is not able to retail the order$")
	public void Verify_the_error_message_when_user_is_not_able_to_retail_the_order() throws Throwable {

		ActionHandler.wait(5);
		VerifyHandler.verifyElementPresent(SVO_CancelOrderAtAnyStageContainer.RetailErrorMsg);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_CancelOrderAtAnyStageContainer.QuoteCancelBtn);
		Reporter.addStepLog("User verifies the error message when user is not able to retail the order on order page");
	}

	@And("^User creates new Classic Service Opportunity with mandatory fields like Stage \"([^\"]*)\" Product Offering \"([^\"]*)\" Region \"([^\"]*)\" Account \"([^\"]*)\" Restricted Party Screening Stage \"([^\"]*)\" Preferred Contact \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\"$")
	public void User_creates_new_classic_service_Opportunity(String stage, String proOffering, String region,
			String client, String Checkcleared, String preferredContact, String brand, String model) throws Exception {
		String oppName = "Test_SVO_Bespoke_Opportunity_";
		double randomNumber = getRandomIntegerBetweenRange(0, 10000);

		String closeDate = CommonFunctions.selectFutureDate();
		Reporter.addStepLog("Opportunity Close Date is = " + closeDate);

		String s[] = stage.split(",");
		stage = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		String proOff[] = proOffering.split(",");
		proOffering = CommonFunctions.readExcelMasterData(proOff[0], proOff[1], proOff[2]);

		String r[] = region.split(",");
		region = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		String c[] = client.split(",");
		client = CommonFunctions.readExcelMasterData(c[0], c[1], c[2]);

		String RPS[] = Checkcleared.split(",");
		Checkcleared = CommonFunctions.readExcelMasterData(RPS[0], RPS[1], RPS[2]);

		String preCon[] = preferredContact.split(",");
		preferredContact = CommonFunctions.readExcelMasterData(preCon[0], preCon[1], preCon[2]);

		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String m[] = model.split(",");
		model = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		ActionHandler.wait(3);
		Reporter.addStepLog("User starts creating new classic service Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.NewOpportunity);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVO_CancelOrderAtAnyStageContainer.RecordType);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVO_CancelOrderAtAnyStageContainer.NextBtn);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeOppName, oppName + randomNumber);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Name");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeClosedDate, closeDate);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Opportunity Closed date");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeStage);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.StageSelection(stage))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Stage for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeProductOffering);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler
				.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ProOfferingSelection(proOffering))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Product Offering for an Opportunity");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeRegion);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.RegionSelection(region))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Region for an Opportunity");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.SVOClassicClient);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOClassicClient, client);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		Actions act4 = new Actions(driver);
		act4.sendKeys(Keys.ARROW_DOWN).build().perform();
		act4.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Account for an Opportunity");

		VerifyHandler.verifyElementPresent(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(RestrictedPartyScreeningContainer.RestrictedPartyScreening);
		CommonFunctions.attachScreenshot();
		ActionHandler
				.click(driver.findElement(By.xpath(RestrictedPartyScreeningContainer.ValueSelection(Checkcleared))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects restricted party screening as Check cleared for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOClassicPreferredContact, preferredContact);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		act1.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(3);
		Reporter.addStepLog("User selects Preferred Contact for an Opportunity");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeBrand, brand);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeBrandSearch);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(brand))));
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Brand for an Opportunity");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeModel, model);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(model))));
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Model for an Opportunity");

		ActionHandler.setText(SVO_CancelOrderAtAnyStageContainer.Vehicle, model);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Actions act2 = new Actions(driver);
		act2.sendKeys(Keys.ARROW_DOWN).build().perform();
		act2.sendKeys(Keys.ENTER).build().perform();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Vehicle for an Opportunity");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		ActionHandler.setText(SVO_CancelOrderAtAnyStageContainer.RetailPrice, "100");
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVO_CancelOrderAtAnyStageContainer.VATDropdown);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Actions act3 = new Actions(driver);
		act3.sendKeys(Keys.ARROW_DOWN).build().perform();
		act3.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(3);
		Reporter.addStepLog("User saves VAT");

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSaveOpportunity);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves classic service Opportunity");

		ActionHandler.wait(4);
		OpportunityName = SVOAdditionalvehicleContainer.OpportunityName.getText();
		Reporter.addStepLog("Opportunity Name is = " + OpportunityName);
	}

	@Then("^Move the opportunity from Qualified to Order Placed stage$")
	public void Move_the_order_from_Qualified_to_Order_Placed_stage() throws Throwable {
		ActionHandler.wait(8);
		ActionHandler.click(SVOAdditionalvehicleContainer.MarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(8);
		Reporter.addStepLog("opportunity moves to Estimate stage");

		ActionHandler.click(SVOAdditionalvehicleContainer.MarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(8);
		Reporter.addStepLog("opportunity moves to Contracting stage");

		ActionHandler.click(SVOAdditionalvehicleContainer.MarkStageAsComplete);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(8);
		Reporter.addStepLog("opportunity moves to Order placed stage");

	}

	@Then("^Navigate to Orders and verify user is not able to view Committed to Build stage$")
	public void Navigate_to_Orders_and_verify_user_is_not_able_to_view_Committed_to_Build_stage() throws Throwable {
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.OrdersLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Orders link");

		ActionHandler.wait(7);
		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.FirstOrder);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens Order details for the opportunity");

		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User is not able to view Committed to Build stage on Orders page");
	}

	@Then("^Verify that user is not able to retail the order without moving an order to outcome stage$")
	public void verify_that_user_is_not_able_to_retail_the_order_without_moving_an_order_to_outcome_stage()
			throws Throwable {
		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);

		javaScriptUtil.clickElementByJS(SVO_CancelOrderAtAnyStageContainer.EditOutcomeIcon);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on edit outcome icon on orders page");

		VerifyHandler.verifyElementPresent(SVO_CancelOrderAtAnyStageContainer.DisabledOutcomeDropDown);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"User is not able to update the outcome of an order if the Order Status is selected other than ‘Outcome’ for any order");

		ActionHandler.click(SVO_CancelOrderAtAnyStageContainer.OrderOucomeCancelButton);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on cancel button");
	}

	@Then("^User create new design brief quote for an opportunity \"([^\"]*)\"$")
	public void user_create_new_design_brief_quote_for_an_opportunity(String retailerContact) throws Throwable {

		String retCon[] = retailerContact.split(",");
		retailerContact = CommonFunctions.readExcelMasterData(retCon[0], retCon[1], retCon[2]);

		String quoteName = "SVO Bespoke Test Quote";
		String expirtaionDate = CommonFunctions.selectFutureDate();

		ActionHandler.pageDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.QuotesLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Quotes page");

		ActionHandler.click(SVOAdditionalvehicleContainer.NewQuoteBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on New button to create new Quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteNameBox, quoteName);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User writes quote name for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRetailerNameBox, retailerContact);
		ActionHandler.click(SVOItemToapproveContainer.QuoteRetailerContact);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters retailer contact details for new quote");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteCostPrice, "123");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Cost Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteListPrice, "203");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters List Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteContribution, "23");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Contribution for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRetailPrice, "213");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Retail Price for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteRevenue, "113");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Revenue for new quote");

		ActionHandler.setText(SVOAdditionalvehicleContainer.QuoteExpirationDate, expirtaionDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Expiration Date for new quote");

		ActionHandler.click(SVOAdditionalvehicleContainer.QuoteSaveBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User save new quote");

	}

	@Then("^Verify that no order is placed without accepting quote$")
	public void verify_that_no_order_is_placed_without_accepting_quote() throws Throwable {

		javaScriptUtil.clickElementByJS(SVO_OpportunityContainer.OpportTab);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstOpportunityLink);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(5);
		driver.navigate().refresh();
		CommonFunctions.attachScreenshot();

		javaScriptUtil.clickElementByJS(SVOAdditionalvehicleContainer.OrdersLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Orders link");

		if (!VerifyHandler.verifyElementPresent(SVOAdditionalvehicleContainer.FirstOrder)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User is not able to view any orders on order page");
		}
	}

	@Then("^Verify that user is not able to move an order to Created stage from Outcome stage$")
	public void verify_that_user_is_not_able_to_move_an_order_to_Created_stage_from_Outcome_stage() throws Throwable {

		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);

		javaScriptUtil.clickElementByJS(SVO_CancelOrderAtAnyStageContainer.EditOutcomeIcon);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on edit outcome icon on orders page");

		ActionHandler.click(SVO_CancelOrderAtAnyStageContainer.OrderStatusOutcomeDropDown);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on status outcome drop down list on orders page");

		ActionHandler.click(SVO_CancelOrderAtAnyStageContainer.OrderStatusBuilt);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects order status as buit stage");

		ActionHandler.click(SVO_CancelOrderAtAnyStageContainer.SaveOrderStatusButton);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on save button");

		VerifyHandler.verifyElementPresent(SVO_CancelOrderAtAnyStageContainer.OrderStatusErrorMessage);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"Verify that an error message displayed stating 'you cannot change the stage once in outcome' stage");

		ActionHandler.click(SVO_CancelOrderAtAnyStageContainer.OrderOucomeCancelButton);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on cancel button");
	}

	@Then("^User verifies the opportunity stage and lost reason on opportunity page$")
	public void user_verifies_the_opportunity_stage_and_lost_reason_on_opportunity_page() throws Throwable {

		OpportunityStage = null;
		LostReason = null;

		OpportunityStage = SVO_CancelOrderAtAnyStageContainer.OpportunityStage.getText();
		Reporter.addStepLog("Opportunity stage is = " + OpportunityStage);
		Reporter.addStepLog("The opportunity stage is displayed as " + OpportunityStage + " on opportunity page");
		CommonFunctions.attachScreenshot();
		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);

		if (OpportunityStage.equals("Closed Lost")) {
			LostReason = SVO_CancelOrderAtAnyStageContainer.LostReasonOnOpportunityPage.getText();
			Reporter.addStepLog("Lost reason is = " + LostReason);
			CommonFunctions.attachScreenshot();

			Reporter.addStepLog("Opportunities lost reason is displayed as " + LostReason + "on opportunity page");
			CommonFunctions.attachScreenshot();
		}

	}

	@Then("^User is not able to retail the order once the order is cancelled on Orders page with outcome stage \"([^\"]*)\"$")
	public void user_is_not_able_to_retail_the_order_once_the_order_is_cancelled_on_Orders_page_with_outcome_stage(
			String OutcomeStage) throws Throwable {

		String OS[] = OutcomeStage.split(",");
		OutcomeStage = CommonFunctions.readExcelMasterData(OS[0], OS[1], OS[2]);

		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);

		javaScriptUtil.clickElementByJS(SVO_CancelOrderAtAnyStageContainer.EditOutcomeBtn);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVO_CancelOrderAtAnyStageContainer.OutcomeDropdown);
		CommonFunctions.attachScreenshot();

		ActionHandler
				.click(driver.findElement(By.xpath(SVO_CancelOrderAtAnyStageContainer.OrderOucomeStage(OutcomeStage))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Outcome for an Order");

		ActionHandler.click(SVO_CancelOrderAtAnyStageContainer.SaveOrder);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVO_CancelOrderAtAnyStageContainer.ErrorMessageOnOrdersPage)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User not able to retail the order once the order is cancelled on Orders page");
		} else {
			Reporter.addStepLog("User not able to retail the order once the order is cancelled on Orders page");
		}

		ActionHandler.click(SVO_CancelOrderAtAnyStageContainer.CancelOrder);
		CommonFunctions.attachScreenshot();
	}

}
