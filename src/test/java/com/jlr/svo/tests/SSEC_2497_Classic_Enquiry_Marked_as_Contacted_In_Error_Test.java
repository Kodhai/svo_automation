package com.jlr.svo.tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.containers.SSEC_1503_Clasic_Email_Signature_On_Salesforce_Container;
import com.jlr.svo.containers.SSEC_2489_ClassicSalesforcePreferredContactSelectionsContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.containers.SVOOpportunityContainer;
import com.jlr.svo.containers.SVO_CustomerResponsesContainer;
import com.jlr.svo.containers.SSEC_2497_ClassicEnquiryMarkedAsContactedInErrorContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class SSEC_2497_Classic_Enquiry_Marked_as_Contacted_In_Error_Test extends TestBaseCC {
	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory
			.getLogger(SSEC_1228_LinkContactAndAccountTocase_Test.class.getName());
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
	SSEC_2489_ClassicSalesforcePreferredContactSelectionsContainer ClassicSalesforcePreferredContactSelectionsContainer = PageFactory.initElements(driver,  SSEC_2489_ClassicSalesforcePreferredContactSelectionsContainer.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
	SVO_CustomerResponsesContainer SVO_CustomerResponsesContainer = PageFactory.initElements(driver, SVO_CustomerResponsesContainer.class);
	SSEC_1503_Clasic_Email_Signature_On_Salesforce_Container Clasic_Email_Signature_On_Salesforce_Container = PageFactory.initElements(driver, SSEC_1503_Clasic_Email_Signature_On_Salesforce_Container.class);
	SSEC_2497_ClassicEnquiryMarkedAsContactedInErrorContainer SSEC_2497_ClassicEnquiryMarkedAsContactedInErrorContainer = PageFactory.initElements(driver, SSEC_2497_ClassicEnquiryMarkedAsContactedInErrorContainer.class);
	
	public static String verificationCode;
	public static String vCode;
	public static String veriCode;
	public String NewWindow;
	
	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	public void onStart() {

		setupTest("SVOTest");
		driver = getDriver();

		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		SSEC_2489_ClassicSalesforcePreferredContactSelectionsContainer ClassicSalesforcePreferredContactSelectionsContainer = PageFactory.initElements(driver,  SSEC_2489_ClassicSalesforcePreferredContactSelectionsContainer.class);
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
		SVO_CustomerResponsesContainer = PageFactory.initElements(driver, SVO_CustomerResponsesContainer.class);
		Clasic_Email_Signature_On_Salesforce_Container = PageFactory.initElements(driver, SSEC_1503_Clasic_Email_Signature_On_Salesforce_Container.class);
		
		driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);

		verificationCode = null;
		vCode = null;
		veriCode = null;
		NewWindow = null;
		
	}

	@Then("^User send Holding Response email to customer \"([^\"]*)\" with Business unit \"([^\"]*)\"$")
    public void user_send_an_email_to_customer_with_Business_unit_and_subject(String Customer, String BusinessUnit)
            throws Throwable {

        String p[] = BusinessUnit.split(",");
        BusinessUnit = CommonFunctions.readExcelMasterData(p[0], p[1], p[2]);

        String s[] = Customer.split(",");
        Customer = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

        ActionHandler.pageDown();
        ActionHandler.wait(2);
        javaScriptUtil.clickElementByJS(SVO_CustomerResponsesContainer.EmailsSection);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("User Navigates to email section on Enquiry page");

        ActionHandler.wait(10);
        ActionHandler.scrollToView(SVO_CustomerResponsesContainer.e2aSendEmailButton);
        ActionHandler.wait(2);
        javaScriptUtil.clickElementByJS(SVO_CustomerResponsesContainer.e2aSendEmailButton);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("User clicks on e2a send an email button");

        ActionHandler.wait(5);
        //driver.switchTo().frame(0);
        driver.switchTo().frame(Clasic_Email_Signature_On_Salesforce_Container.EmailFrame);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("user navigate to email frame");

        ActionHandler.wait(10);
        //ActionHandler.click(SVO_CustomerResponsesContainer.BusinessUnitDropDown);
        ActionHandler.wait(1);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("User clicks on Business unit drop down list");

        //ActionHandler.click(driver.findElement(By.xpath(SVO_CustomerResponsesContainer.BusinessUnitSelection(BusinessUnit))));
        ActionHandler.wait(10);
        CommonFunctions.attachScreenshot();
        //Reporter.addStepLog("User selects the " + BusinessUnit + " Business unit from drop down list");

        ActionHandler.scrollDown();
        ActionHandler.wait(2);

        ActionHandler.click(SVO_CustomerResponsesContainer.EmailToTextBox);
        ActionHandler.wait(1);
        ActionHandler.setText(SVO_CustomerResponsesContainer.EmailToTextBox, Customer);
        ActionHandler.wait(1);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("User Enters " + Customer + " in To section of an e2a email");

        ActionHandler.click(SVO_CustomerResponsesContainer.EmailSubjectTextBox);
        ActionHandler.wait(1);
        ActionHandler.setText(SVO_CustomerResponsesContainer.EmailSubjectTextBox, "Holding Email");
        ActionHandler.wait(1);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("User Enters text in subject section of an e2a email");

        ActionHandler.pageCompleteScrollDown();
        ActionHandler.wait(2);

        ActionHandler.click(SVO_CustomerResponsesContainer.EmailSendButton);
        ActionHandler.wait(10);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("User Clicks on send an email button");

        driver.switchTo().parentFrame();
        
    }
	
	@Then("^Log a call to the Enquiry$")
    public void log_a_call_to_the_Enquiry_() throws Throwable {

        String commentText = "Customer has requested a  report";

        ActionHandler.wait(3);
        ActionHandler.pageScrollDown();
        ActionHandler.wait(2);
        ActionHandler.pageArrowUp();
        ActionHandler.wait(2);
        ActionHandler.pageArrowUp();

        ActionHandler.wait(3);

        Reporter.addStepLog("User Navigate to Activity Tab");
        ActionHandler.click(SVOEnquiryContainer.activityTab);
        ActionHandler.wait(7);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Click on Activity tab");

        Reporter.addStepLog("User clicks on Create a log call");
        ActionHandler.wait(3);
        ActionHandler.click(SVOEnquiryContainer.newLogCall);
        ActionHandler.wait(7);
        Reporter.addStepLog("click on create a log");

        Actions act = new Actions(driver);
        act.sendKeys(Keys.ARROW_DOWN).build().perform();
        act.sendKeys(Keys.ARROW_DOWN).build().perform();
        ActionHandler.wait(3);
        CommonFunctions.attachScreenshot();

        ActionHandler.wait(2);
        ActionHandler.click(SVOEnquiryContainer.subjectDD);
        ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Actions act1 = new Actions(driver);
        act1.sendKeys(Keys.ARROW_DOWN).build().perform();
        act1.sendKeys(Keys.ENTER).build().perform();
        // ActionHandler.click(SVOEnquiryContainer.callLog);
        ActionHandler.wait(3);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Select subject as call");

        ActionHandler.setText(SVOEnquiryContainer.commentText, commentText);
        ActionHandler.wait(3);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Input comment");

        ActionHandler.click(SVOEnquiryContainer.saveLog);
        ActionHandler.wait(20);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Save the log");
    }
	

	
	@Then("^user Verify that created Classic Service enquiry status set to New stage$")
    public void user_Verify_that_created_Classic_Service_enquiry_status_set_to_New_stage() throws Throwable {

       VerifyHandler.verifyElementPresent(SSEC_2497_ClassicEnquiryMarkedAsContactedInErrorContainer.NewEnquiryStatus);
        ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("user Verify that created Classic Service enquiry status set to New stage");
    }
	

	
	@And("^Verify that created Classic sales enquiry stage not moved to contacted stage$")
    public void Verify_that_created_Classic_sales_enquiry_stage_not_moved_to_contacted_stage() throws Throwable {

       if(!VerifyHandler.verifyElementPresent(SSEC_2497_ClassicEnquiryMarkedAsContactedInErrorContainer.ContactedStatus)) {
        ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("user Verify that created Classic Service enquiry status set to New stage");
       }
    }
}
