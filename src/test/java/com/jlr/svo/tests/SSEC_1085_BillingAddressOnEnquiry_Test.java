package com.jlr.svo.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.containers.SSEC_1005_AutopoulatedFieldsOnOrderWorkOrderContainer;
import com.jlr.svo.containers.SSEC_1085_BillingAddressOnEnquiryContainer;
import com.jlr.svo.containers.SVOAccountsContainer;
import com.jlr.svo.containers.SVOAdditionalVehicleContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.containers.SVOItemToApproveContainer;
import com.jlr.svo.containers.SVOOpportunityContainer;
import com.jlr.svo.containers.SVO_EnquiryLostReasonContainer;
import com.jlr.svo.containers.SVO_Pricing_LogicContainer;
import com.jlr.svo.containers.SVO_RestrictedPartyScreeningContainer;
import com.jlr.svo.containers.SVO_WorkOrder_AdditionalOwner_Container;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.PendingException;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SSEC_1085_BillingAddressOnEnquiry_Test extends TestBaseCC {
	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory
			.getLogger(SSEC_1085_BillingAddressOnEnquiry_Test.class.getName());
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);

	SVOAccountsContainer SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
	SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	SVOAdditionalVehicleContainer SVOAdditionalvehicleContainer = PageFactory.initElements(driver,
			SVOAdditionalVehicleContainer.class);
	SVO_Pricing_LogicContainer SVO_Pricing_LogicContainer = PageFactory.initElements(driver,
			SVO_Pricing_LogicContainer.class);
	SVO_RestrictedPartyScreeningContainer RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
			SVO_RestrictedPartyScreeningContainer.class);
	SVOItemToApproveContainer SVOItemToapproveContainer = PageFactory.initElements(driver,
			SVOItemToApproveContainer.class);
	SVO_EnquiryLostReasonContainer SVO_EnquiryLostReasonContainer = PageFactory.initElements(driver,
			SVO_EnquiryLostReasonContainer.class);
	SSEC_1085_BillingAddressOnEnquiryContainer BillingAddressOnEnquiryContainer = PageFactory.initElements(driver,
			SSEC_1085_BillingAddressOnEnquiryContainer.class);
	SSEC_1005_AutopoulatedFieldsOnOrderWorkOrderContainer AutopoulatedFieldsOnOrderWorkOrder = PageFactory
			.initElements(driver, SSEC_1005_AutopoulatedFieldsOnOrderWorkOrderContainer.class);
	SVO_WorkOrder_AdditionalOwner_Container SVO_WorkOrder_AdditionalOwner_Container = PageFactory.initElements(driver,
			SVO_WorkOrder_AdditionalOwner_Container.class);

	public static String verificationCode;
	public static String vCode;
	public static String veriCode;
	public static String EnquiryName;
	public static String SLAStatus;

	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	@Then("^User creates an Enquiry with record type \"([^\"]*)\" productOffering \"([^\"]*)\" Region \"([^\"]*)\" Client \"([^\"]*)\" PreferredContact \"([^\"]*)\" Programme \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\" BodyStyle \"([^\"]*)\" OrginatingDivision \"([^\"]*)\"$")
	public void user_creates_an_Enquiry_with_record_type_productOffering_Region_Client_PreferredContact_Programme_Brand_Model_BodyStyle_OrginatingDivision(
			String RecType, String productOffering, String Region, String Client, String PreferredContact,
			String Programme, String Brand, String Model, String BodyStyle, String OrginatingDivision)
			throws Throwable {

		String a[] = RecType.split(",");
		RecType = CommonFunctions.readExcelMasterData(a[0], a[1], a[2]);

		String b[] = productOffering.split(",");
		productOffering = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String C[] = Region.split(",");
		Region = CommonFunctions.readExcelMasterData(C[0], C[1], C[2]);

		String D[] = Client.split(",");
		Client = CommonFunctions.readExcelMasterData(D[0], D[1], D[2]);

		String E[] = PreferredContact.split(",");
		PreferredContact = CommonFunctions.readExcelMasterData(E[0], E[1], E[2]);

		String F[] = Programme.split(",");
		Programme = CommonFunctions.readExcelMasterData(F[0], F[1], F[2]);

		String G[] = Brand.split(",");
		Brand = CommonFunctions.readExcelMasterData(G[0], G[1], G[2]);

		String H[] = Model.split(",");
		Model = CommonFunctions.readExcelMasterData(H[0], H[1], H[2]);

		String I[] = BodyStyle.split(",");
		BodyStyle = CommonFunctions.readExcelMasterData(I[0], I[1], I[2]);

		String J[] = OrginatingDivision.split(",");
		OrginatingDivision = CommonFunctions.readExcelMasterData(J[0], J[1], J[2]);

		double randomnumber = getRandomIntegerBetweenRange(0, 1000);

		ActionHandler.click(SVO_EnquiryLostReasonContainer.EnquiryNewButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks on to Enquiries new button");

		ActionHandler.wait(2);
		ActionHandler.click(driver.findElement(By.xpath(SVO_EnquiryLostReasonContainer.RecTypeSelection(RecType))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects the enquiry record type");

		ActionHandler.wait(2);
		ActionHandler.click(SVO_EnquiryLostReasonContainer.EnquiryNextButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks on to Enquiries next button");

		ActionHandler.setText(SVO_EnquiryLostReasonContainer.EnquiryTitleTextBox, RecType + randomnumber);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters enquiry title");

		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquiryProdOfferDropDown);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		ActionHandler
				.click(driver.findElement(By.xpath(BillingAddressOnEnquiryContainer.POSelection(productOffering))));
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Selects product offering of an enquiry as " + productOffering);

		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquiryClientSearchBox);
		ActionHandler.wait(1);
		ActionHandler.setText(BillingAddressOnEnquiryContainer.EnquiryClientSearchBox, Client);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeClientNameSearch);
		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquiryClientSelect);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Selects client of an enquiry as " + Client);

		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquiryRegionDropDown);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(BillingAddressOnEnquiryContainer.POSelection(Region))));
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Selects Region of an enquiry as " + Region);

		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquirySalesAreaDropDown);
		ActionHandler.wait(1);
		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act1.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Selects Sales area of an enquiry as NSC(UK)");

		ActionHandler.scrollToView(BillingAddressOnEnquiryContainer.EnquiryMarketDropDown);
		ActionHandler.wait(2);
		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquiryMarketDropDown);
		ActionHandler.wait(1);
		Actions act2 = new Actions(driver);
		act2.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act2.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Selects Market of an enquiry ad UK");

		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquiryPreferredContactTextBox);
		ActionHandler.wait(1);
		ActionHandler.setText(BillingAddressOnEnquiryContainer.EnquiryPreferredContactTextBox, PreferredContact);
		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquiryPreferredContactSearchbar);
		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquiryClientSelect);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Selects Preferred contact of an enquiry as " + PreferredContact);

		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquiryProgrammeTextBox);
		ActionHandler.wait(1);
		ActionHandler.setText(BillingAddressOnEnquiryContainer.EnquiryProgrammeTextBox, Programme);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquiryProgrammeSearchbar);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquiryClientSelect);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Programme for an Enquiry as " + Programme);

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeBrand, Brand);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeBrandSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(Brand))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Brand of a vehicle on Enquiry Page Layout");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeModel, Model);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(Model))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Model of a vehicle on Enquiry Page Layout");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.BodyStyleTextBox);
		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.BodyStyleTextBox, Model);
		ActionHandler.wait(4);
		//ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.TestDefenderBodystyle);
		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects body style of a vehicle on Enquiry page layout");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.HandOfDrivedropDown);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.HandOfDriveLHD);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Hand of drive on Opportunity");

		ActionHandler.click(BillingAddressOnEnquiryContainer.VehicleRecordTextBox);
		ActionHandler.wait(1);
		ActionHandler.setText(BillingAddressOnEnquiryContainer.VehicleRecordTextBox, Model);
		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquiryVehicleRecord);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Vehicle record on enquiry page layout");

		ActionHandler.wait(1);
		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquiryPreferredRetailerTextBox);
		ActionHandler.wait(1);
		ActionHandler.setText(BillingAddressOnEnquiryContainer.EnquiryPreferredRetailerTextBox, Client);
		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquiryPreferredRetailerSearchBar);
		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquiryClientSelect);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Selects Preferred retailer of an enquiry as " + Client);

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSource);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.SourceSelection("Email"))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Source for an Enquiry");

		ActionHandler.click(RestrictedPartyScreeningContainer.SVOOriginatingDivission);
		CommonFunctions.attachScreenshot();
		//ActionHandler.click(driver.findElement(By.xpath(RestrictedPartyScreeningContainer.OriginatingDivisionSelection(OrginatingDivision))));
		Actions act3 = new Actions(driver);
		act3.sendKeys(Keys.ARROW_DOWN).build().perform();
		act3.sendKeys(Keys.ARROW_DOWN).build().perform();
		act3.sendKeys(Keys.ARROW_DOWN).build().perform();
		act3.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Originating Divission for an Enquiry");

		ActionHandler.click(SVO_EnquiryLostReasonContainer.EnquirySaveButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks on to Enquiries Save button");

		VerifyHandler.verifyElementPresent(SVO_EnquiryLostReasonContainer.EnquiryName);

		EnquiryName = SVO_EnquiryLostReasonContainer.EnquiryName.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enquiry is created successfully with Enquiry name : " + EnquiryName);

	}

	@When("^User enters customer contact details \"([^\"]*)\" and Billing Address details on enquiry page$")
	public void user_enters_customer_contact_details_and_Billing_Address_details_on_enquiry_page(String ContactName)
			throws Throwable {

		String b[] = ContactName.split(",");
		ContactName = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		ActionHandler.wait(1);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);

		ActionHandler.setText(SVO_EnquiryLostReasonContainer.EnquiryContactTextBox, ContactName);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters contact detail on enquiry page");

		ActionHandler.click(SVO_EnquiryLostReasonContainer.EnquiryContactSearch);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(ContactName))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects contact details on enquiry page");
		
		VerifyHandler.verifyElementPresent(BillingAddressOnEnquiryContainer.BillingAddress);
		String EnquiryStatusField = BillingAddressOnEnquiryContainer.BillingAddress.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User is able to view autopopulated billing address under contact details section");

		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
	}

	@Then("^User moves an enquiry to discovery stage$")
	public void user_moves_an_enquiry_to_discovery_stage() throws Throwable {

		ActionHandler.click(SVO_EnquiryLostReasonContainer.MarkEnquiryStatusAsCompleteBtn);
		ActionHandler.wait(6);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Move the enquiry stage to Discovery on enquiries page");
	}

	@Then("^User close the enquiry with closed state \"([^\"]*)\" on Enquiry page$")
	public void user_close_the_enquiry_with_closed_state_on_Enquiry_page(String state) throws Throwable {
		String s[] = state.split(",");
		state = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		driver.navigate().refresh();
		ActionHandler.wait(10);
		ActionHandler.click(SVO_EnquiryLostReasonContainer.ClosedEnquiryStatusTab);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Closed enquiry tab");

		ActionHandler.click(SVO_EnquiryLostReasonContainer.MarkAsCurrentEnquiryStatusBtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Mark as current enquiry status button");

		ActionHandler.click(SVO_EnquiryLostReasonContainer.ClosedStateDropDownList);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Closed State drop down list");

		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.RPSSelection(state))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects closed state of an enquiry on enquiry page as: " + state);

		ActionHandler.click(SVO_EnquiryLostReasonContainer.EnquiryClosedDoneButton);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on enquiry Close pop up window done button");

	}

	@Then("^Verify the auto-populated fields of enqiry closed state on Enquiry page$")
	public void verify_the_auto_populated_fields_of_enqiry_closed_state_on_Enquiry_page() throws Throwable {

		ActionHandler.wait(3);
		VerifyHandler.verifyElementPresent(SVO_EnquiryLostReasonContainer.EnquiryClosedState);
		String closedstate = SVO_EnquiryLostReasonContainer.EnquiryClosedState.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enquiry closed state is displayed as: " + closedstate + " on Enquiry page");
	}

	@Then("^User select an existing closed lost classic sales enquiry from list view$")
	public void user_select_an_existing_closed_lost_classic_sales_enquiry_from_list_view() throws Throwable {

		ActionHandler.wait(2);
		ActionHandler.click(SVOItemToapproveContainer.SelectOpportunityListView);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on select list view tab");

		ActionHandler.click(SVO_EnquiryLostReasonContainer.AllClassicSalesEnquirytab);
		Reporter.addStepLog("User chooses to view selected to classic sales enquiries list");
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		ActionHandler.click(SVO_EnquiryLostReasonContainer.EnquirysearchBox);
		ActionHandler.wait(2);
		ActionHandler.setText(SVO_EnquiryLostReasonContainer.EnquirysearchBox, "Closed Lost");

		ActionHandler.wait(3);
		ActionHandler.pressEnter();
		ActionHandler.wait(5);

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstOpportunityLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects SLA breached Classic sales enquiry");

	}

	@Then("^Verify that user is not able to view customer billing address details on enquiry page$")
	public void verify_that_user_is_not_able_to_view_customer_billing_address_details_on_enquiry_page()
			throws Throwable {
		if(!VerifyHandler.verifyElementPresent(BillingAddressOnEnquiryContainer.BillingAddress)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Verify that user is not able to view customer billing address details under contact details section");
		}
		else {
			Reporter.addStepLog("Verify that user is able to view customer billing address details under contact details section");
		}
	}

	@When("^User edits customer contact details \"([^\"]*)\" and Billing Address details on enquiry page$")
	public void user_edits_customer_contact_details_and_Billing_Address_details_on_enquiry_page(String ContactName)
			throws Throwable {

		String b[] = ContactName.split(",");
		ContactName = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		ActionHandler.wait(1);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		
		ActionHandler.click(BillingAddressOnEnquiryContainer.EditAccountNameBtn);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(BillingAddressOnEnquiryContainer.ClearAccountName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVO_EnquiryLostReasonContainer.EnquiryContactTextBox, ContactName);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters contact detail on enquiry page");

		ActionHandler.click(SVO_EnquiryLostReasonContainer.EnquiryContactSearch);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(ContactName))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects contact details on enquiry page");

		
		ActionHandler.click(BillingAddressOnEnquiryContainer.SaveContactNameBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User edits contact details on enquiry page");
		
		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
	}
	
	@Then("^User close the enquiry with closed state \"([^\"]*)\" closed reason \"([^\"]*)\" on Enquiry page$")
	public void user_close_the_enquiry_with_closed_state_and_closed_reason_on_Enquiry_page(String state, String reason) throws Throwable {
		String s[] = state.split(",");
		state = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);
		
		String r[] = reason.split(",");
		reason = CommonFunctions.readExcelMasterData(r[0], r[1], r[2]);

		ActionHandler.click(SVO_EnquiryLostReasonContainer.ClosedEnquiryStatusTab);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Closed enquiry tab");

		ActionHandler.click(SVO_EnquiryLostReasonContainer.MarkAsCurrentEnquiryStatusBtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Mark as current enquiry status button");

		ActionHandler.click(SVO_EnquiryLostReasonContainer.ClosedStateDropDownList);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Closed State drop down list");

		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.RPSSelection(state))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects closed state of an enquiry on enquiry page as: " + state);
		
		ActionHandler.click(BillingAddressOnEnquiryContainer.ClosedReasonDropdonList);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.RPSSelection(state))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects closed reason of an enquiry on enquiry page as: " + reason);

		ActionHandler.click(SVO_EnquiryLostReasonContainer.EnquiryClosedDoneButton);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on enquiry Close pop up window done button");

	}
	
	@Then("^Verify that updated billing address details of the customer on enquiry page$")
	public void verify_that_updated_billing_address_details_of_the_customer_on_enquiry_page() throws Throwable {
	   
	}
	
	@Then("^Update the contact details under contact details section of an enquiry$")
	public void update_the_contact_details_under_contact_details_section_of_an_enquiry() throws Throwable {
	   
	}

	@Then("^Verify that billing address details is updated with respect to contact details$")
	public void verify_that_billing_address_details_is_updated_with_respect_to_contact_details() throws Throwable {
	   
	}

	@Then("^User remove customer contact details from contact details section of an enquiry$")
	public void user_remove_customer_contact_details_from_contact_details_section_of_an_enquiry() throws Throwable {
	    
	}

	@Then("^Verify that billing address details of the customer is removed within customer contact details section of an enquiry$")
	public void verify_that_billing_address_details_of_the_customer_is_removed_within_customer_contact_details_section_of_an_enquiry() throws Throwable {
	    
	}

	@Then("^Verify that error message while Close the Classic sales Enquiry with qualified state without adding contact details and billing address details with closed state on Enquiry page$")
	public void verify_that_error_message_while_Close_the_Classic_sales_Enquiry_with_qualified_state_without_adding_contact_details_and_billing_address_details_with_closed_state_on_Enquiry_page() throws Throwable {
	    
	}

	@Then("^Verify that error message while removing the contact details after enquiry is closed$")
	public void verify_that_error_message_while_removing_the_contact_details_after_enquiry_is_closed() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}
	
	@Then("^User creates an Enquiry with address details record type \"([^\"]*)\" productOffering \"([^\"]*)\" Region \"([^\"]*)\" Client \"([^\"]*)\" PreferredContact \"([^\"]*)\" Programme \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\" BodyStyle \"([^\"]*)\" OrginatingDivision \"([^\"]*)\"$")
	public void user_creates_an_Enquiry_with_address_details_record_type_productOffering_Region_Client_PreferredContact_Programme_Brand_Model_BodyStyle_OrginatingDivision(
			String RecType, String productOffering, String Region, String Client, String PreferredContact,
			String Programme, String Brand, String Model, String BodyStyle, String OrginatingDivision)
			throws Throwable {

		String a[] = RecType.split(",");
		RecType = CommonFunctions.readExcelMasterData(a[0], a[1], a[2]);

		String b[] = productOffering.split(",");
		productOffering = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String C[] = Region.split(",");
		Region = CommonFunctions.readExcelMasterData(C[0], C[1], C[2]);

		String D[] = Client.split(",");
		Client = CommonFunctions.readExcelMasterData(D[0], D[1], D[2]);

		String E[] = PreferredContact.split(",");
		PreferredContact = CommonFunctions.readExcelMasterData(E[0], E[1], E[2]);

		String F[] = Programme.split(",");
		Programme = CommonFunctions.readExcelMasterData(F[0], F[1], F[2]);

		String G[] = Brand.split(",");
		Brand = CommonFunctions.readExcelMasterData(G[0], G[1], G[2]);

		String H[] = Model.split(",");
		Model = CommonFunctions.readExcelMasterData(H[0], H[1], H[2]);

		String I[] = BodyStyle.split(",");
		BodyStyle = CommonFunctions.readExcelMasterData(I[0], I[1], I[2]);

		String J[] = OrginatingDivision.split(",");
		OrginatingDivision = CommonFunctions.readExcelMasterData(J[0], J[1], J[2]);

		double randomnumber = getRandomIntegerBetweenRange(0, 1000);

		ActionHandler.click(SVO_EnquiryLostReasonContainer.EnquiryNewButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks on to Enquiries new button");

		ActionHandler.wait(2);
		ActionHandler.click(driver.findElement(By.xpath(SVO_EnquiryLostReasonContainer.RecTypeSelection(RecType))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects the enquiry record type");

		ActionHandler.wait(2);
		ActionHandler.click(SVO_EnquiryLostReasonContainer.EnquiryNextButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks on to Enquiries next button");

		ActionHandler.setText(SVO_EnquiryLostReasonContainer.EnquiryTitleTextBox, RecType + randomnumber);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters enquiry title");

		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquiryProdOfferDropDown);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		ActionHandler
				.click(driver.findElement(By.xpath(BillingAddressOnEnquiryContainer.POSelection(productOffering))));
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Selects product offering of an enquiry as " + productOffering);

		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquiryClientSearchBox);
		ActionHandler.wait(1);
		ActionHandler.setText(BillingAddressOnEnquiryContainer.EnquiryClientSearchBox, Client);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeClientNameSearch);
		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquiryClientSelect);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Selects client of an enquiry as " + Client);

		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquiryRegionDropDown);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(BillingAddressOnEnquiryContainer.POSelection(Region))));
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Selects Region of an enquiry as " + Region);

		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquirySalesAreaDropDown);
		ActionHandler.wait(1);
		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act1.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Selects Sales area of an enquiry as NSC(UK)");

		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquiryMarketDropDown);
		ActionHandler.wait(1);
		Actions act2 = new Actions(driver);
		act2.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act2.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Selects Market of an enquiry ad UK");

		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquiryPreferredContactTextBox);
		ActionHandler.wait(1);
		ActionHandler.setText(BillingAddressOnEnquiryContainer.EnquiryPreferredContactTextBox, PreferredContact);
		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquiryPreferredContactSearchbar);
		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquiryClientSelect);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Selects Preferred contact of an enquiry as " + PreferredContact);

		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquiryProgrammeTextBox);
		ActionHandler.wait(1);
		ActionHandler.setText(BillingAddressOnEnquiryContainer.EnquiryProgrammeTextBox, Programme);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquiryProgrammeSearchbar);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquiryClientSelect);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Programme for an Enquiry as " + Programme);

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeBrand, Brand);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeBrandSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(Brand))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Brand of a vehicle on Enquiry Page Layout");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeModel, Model);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(Model))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Model of a vehicle on Enquiry Page Layout");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.BodyStyleTextBox);
		ActionHandler.setText(SVO_WorkOrder_AdditionalOwner_Container.BodyStyleTextBox, Model);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.TestDefenderBodystyle);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects body style of a vehicle on Enquiry page layout");

		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.HandOfDrivedropDown);
		ActionHandler.click(SVO_WorkOrder_AdditionalOwner_Container.HandOfDriveLHD);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Hand of drive on Opportunity");

		ActionHandler.click(BillingAddressOnEnquiryContainer.VehicleRecordTextBox);
		ActionHandler.wait(1);
		ActionHandler.setText(BillingAddressOnEnquiryContainer.VehicleRecordTextBox, Model);
		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquiryVehicleRecord);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Vehicle record on enquiry page layout");

		ActionHandler.wait(1);
		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquiryPreferredRetailerTextBox);
		ActionHandler.wait(1);
		ActionHandler.setText(BillingAddressOnEnquiryContainer.EnquiryPreferredRetailerTextBox, Client);
		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquiryPreferredRetailerSearchBar);
		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquiryClientSelect);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Selects Preferred retailer of an enquiry as " + Client);

		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSource);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.SourceSelection("Email"))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Source for an Enquiry");

		ActionHandler.click(RestrictedPartyScreeningContainer.SVOOriginatingDivission);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(
				driver.findElement(By.xpath(RestrictedPartyScreeningContainer.ValueSelection(OrginatingDivision))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Originating Divission for an Enquiry");

		ActionHandler.click(SVO_EnquiryLostReasonContainer.EnquirySaveButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks on to Enquiries Save button");

		VerifyHandler.verifyElementPresent(SVO_EnquiryLostReasonContainer.EnquiryName);

		EnquiryName = SVO_EnquiryLostReasonContainer.EnquiryName.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enquiry is created successfully with Enquiry name : " + EnquiryName);

	}
	
	@Then("^Verify that billing address is autopopulated under contact details section$")
	public void verify_that_billing_address_is_autopopulated_under_contact_details_section() throws Throwable {
	    
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		VerifyHandler.verifyElementPresent(BillingAddressOnEnquiryContainer.BillingAddress);
		String EnquiryStatusField = BillingAddressOnEnquiryContainer.BillingAddress.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User is able to view autopopulated billing address under contact details section");
		
		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(2);
	}
	
	@Then("^Verify that the enquiry is in New stage$")
	public void verify_that_the_enquiry_is_in_New_stage() throws Throwable {
		VerifyHandler.verifyElementPresent(BillingAddressOnEnquiryContainer.EnquiryStatusAsNew);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verify that Enquiry is in New stage");
	}
	
	@Then("^Verify that the enquiry is in Closed Lost stage$")
	public void verify_that_the_enquiry_is_in_Closed_Lost_stage() throws Throwable {
		VerifyHandler.verifyElementPresent(BillingAddressOnEnquiryContainer.EnquiryStatusAsClosedLost);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verify that Enquiry is in Closed Lost stage");
	}
	
	@Then("^User creates an Private Office Enquiry with record type \"([^\"]*)\" productOffering \"([^\"]*)\" Region \"([^\"]*)\" Client \"([^\"]*)\" PreferredContact \"([^\"]*)\" Programme \"([^\"]*)\" Brand \"([^\"]*)\" Model \"([^\"]*)\" BodyStyle \"([^\"]*)\" OrginatingDivision \"([^\"]*)\"$")
	public void user_creates_an_Private_Office_Enquiry_with_record_type_productOffering_Region_Client_PreferredContact_Programme_Brand_Model_BodyStyle_OrginatingDivision(
			String RecType, String productOffering, String Region, String Client, String PreferredContact,
			String Programme, String Brand, String Model, String BodyStyle, String OrginatingDivision)
			throws Throwable {

		String a[] = RecType.split(",");
		RecType = CommonFunctions.readExcelMasterData(a[0], a[1], a[2]);

		String b[] = productOffering.split(",");
		productOffering = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		String C[] = Region.split(",");
		Region = CommonFunctions.readExcelMasterData(C[0], C[1], C[2]);

		String D[] = Client.split(",");
		Client = CommonFunctions.readExcelMasterData(D[0], D[1], D[2]);

		String E[] = PreferredContact.split(",");
		PreferredContact = CommonFunctions.readExcelMasterData(E[0], E[1], E[2]);

		String F[] = Programme.split(",");
		Programme = CommonFunctions.readExcelMasterData(F[0], F[1], F[2]);

		String G[] = Brand.split(",");
		Brand = CommonFunctions.readExcelMasterData(G[0], G[1], G[2]);

		String H[] = Model.split(",");
		Model = CommonFunctions.readExcelMasterData(H[0], H[1], H[2]);

		String I[] = BodyStyle.split(",");
		BodyStyle = CommonFunctions.readExcelMasterData(I[0], I[1], I[2]);

		String J[] = OrginatingDivision.split(",");
		OrginatingDivision = CommonFunctions.readExcelMasterData(J[0], J[1], J[2]);

		double randomnumber = getRandomIntegerBetweenRange(0, 1000);

		ActionHandler.click(SVO_EnquiryLostReasonContainer.EnquiryNewButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks on to Enquiries new button");

		ActionHandler.wait(2);
		ActionHandler.click(driver.findElement(By.xpath(SVO_EnquiryLostReasonContainer.RecTypeSelection(RecType))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects the enquiry record type");

		ActionHandler.wait(2);
		ActionHandler.click(SVO_EnquiryLostReasonContainer.EnquiryNextButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks on to Enquiries next button");

		ActionHandler.setText(SVO_EnquiryLostReasonContainer.EnquiryTitleTextBox, RecType + randomnumber);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters enquiry title");
		
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeSource);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.SourceSelection("Email"))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Source for an Enquiry");
		
		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquiryRegionDropDown);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(BillingAddressOnEnquiryContainer.POSelection(Region))));
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Selects Region of an enquiry as " + Region);
		
		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquirySalesAreaDropDown);
		ActionHandler.wait(1);
		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act1.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Selects Sales area of an enquiry as NSC(UK)");

		ActionHandler.scrollToView(BillingAddressOnEnquiryContainer.EnquiryMarketDropDown);
		ActionHandler.wait(2);
		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquiryMarketDropDown);
		ActionHandler.wait(1);
		Actions act2 = new Actions(driver);
		act2.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act2.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Selects Market of an enquiry ad UK");

		ActionHandler.scrollToView(BillingAddressOnEnquiryContainer.EnquiryProdOfferDropDown);
		ActionHandler.wait(2);
		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquiryProdOfferDropDown);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(BillingAddressOnEnquiryContainer.POSelection(productOffering))));
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Selects product offering of an enquiry as " + productOffering);
		
		ActionHandler.click(RestrictedPartyScreeningContainer.SVOOriginatingDivission);
		CommonFunctions.attachScreenshot();
		Actions act3 = new Actions(driver);
		act3.sendKeys(Keys.ARROW_DOWN).build().perform();
		act3.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User chooses Originating Divission for an Enquiry");

		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquiryClientSearchBox);
		ActionHandler.wait(1);
		ActionHandler.setText(BillingAddressOnEnquiryContainer.EnquiryClientSearchBox, Client);
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeClientNameSearch);
		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquiryClientSelect);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Selects client of an enquiry as " + Client);

		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquiryPreferredContactTextBox);
		ActionHandler.wait(1);
		ActionHandler.setText(BillingAddressOnEnquiryContainer.EnquiryPreferredContactTextBox, PreferredContact);
		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquiryPreferredContactSearchbar);
		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquiryClientSelect);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Selects Preferred contact of an enquiry as " + PreferredContact);
		
		ActionHandler.wait(1);
		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquiryPreferredRetailerTextBox);
		ActionHandler.wait(1);
		ActionHandler.setText(BillingAddressOnEnquiryContainer.EnquiryPreferredRetailerTextBox, Client);
		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquiryPreferredRetailerSearchBar);
		ActionHandler.click(BillingAddressOnEnquiryContainer.EnquiryClientSelect);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Selects Preferred retailer of an enquiry as " + Client);
		
		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeBrand, Brand);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeBrandSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(Brand))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Brand of a vehicle on Enquiry Page Layout");

		ActionHandler.setText(SVOAdditionalvehicleContainer.SVOBespokeModel, Model);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(SVOAdditionalvehicleContainer.SVOBespokeModelSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(SVOAdditionalvehicleContainer.ValueSelection(Model))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Model of a vehicle on Enquiry Page Layout");

		ActionHandler.click(SVO_EnquiryLostReasonContainer.EnquirySaveButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Clicks on to Enquiries Save button");

		VerifyHandler.verifyElementPresent(SVO_EnquiryLostReasonContainer.EnquiryName);

		EnquiryName = SVO_EnquiryLostReasonContainer.EnquiryName.getText();
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enquiry is created successfully with Enquiry name : " + EnquiryName);

	}
	
}
