package com.jlr.svo.tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.containers.SSEC_1226_CaseCreationEmail_Container;
import com.jlr.svo.containers.SSEC_2119_AdditionalVehicleName_AutoNumberContainer;
import com.jlr.svo.containers.SVOAccountsContainer;
import com.jlr.svo.containers.SVOAdditionalVehicleContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.containers.SVOItemToApproveContainer;
import com.jlr.svo.containers.SVOOpportunityContainer;
import com.jlr.svo.containers.SVO_CustomerResponsesContainer;
import com.jlr.svo.containers.SVO_EnquiryLostReasonContainer;
import com.jlr.svo.containers.SVO_RestrictedPartyScreeningContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class SSEC_2119_AdditionalVehicleName_AutoNumber extends TestBaseCC {
	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory.getLogger(SSEC_1226_CaseCreationEmail_Test.class.getName());
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);

	SVOAccountsContainer SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
	SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	SVOAdditionalVehicleContainer SVOAdditionalvehicleContainer = PageFactory.initElements(driver,
			SVOAdditionalVehicleContainer.class);
	SVO_RestrictedPartyScreeningContainer RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
			SVO_RestrictedPartyScreeningContainer.class);
	SVOItemToApproveContainer SVOItemToapproveContainer = PageFactory.initElements(driver,
			SVOItemToApproveContainer.class);
	SVO_CustomerResponsesContainer SVO_CustomerResponsesContainer = PageFactory.initElements(driver,
			SVO_CustomerResponsesContainer.class);
	SVO_EnquiryLostReasonContainer SVO_EnquiryLostReasonContainer = PageFactory.initElements(driver,
			SVO_EnquiryLostReasonContainer.class);
	SSEC_1226_CaseCreationEmail_Container CaseCreationEmailContainer = PageFactory.initElements(driver,
			SSEC_1226_CaseCreationEmail_Container.class);
	SSEC_2119_AdditionalVehicleName_AutoNumberContainer AdditionalVehicleName_AutoNumberContainer = PageFactory.initElements(driver, SSEC_2119_AdditionalVehicleName_AutoNumberContainer.class);
	
	public static String verificationCode;
	public static String vCode;
	public static String veriCode;
	public String NewWindow;
	public static String CaseCreated;
	public static String CaseNumber;

	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	public void onStart() {

		setupTest("SVOTest");
		driver = getDriver();

		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
		SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
		SVOAdditionalvehicleContainer = PageFactory.initElements(driver, SVOAdditionalVehicleContainer.class);

		RestrictedPartyScreeningContainer = PageFactory.initElements(driver,
				SVO_RestrictedPartyScreeningContainer.class);
		SVOItemToapproveContainer = PageFactory.initElements(driver, SVOItemToApproveContainer.class);
		SVO_CustomerResponsesContainer = PageFactory.initElements(driver, SVO_CustomerResponsesContainer.class);
		SVO_EnquiryLostReasonContainer = PageFactory.initElements(driver, SVO_EnquiryLostReasonContainer.class);
		CaseCreationEmailContainer = PageFactory.initElements(driver, SSEC_1226_CaseCreationEmail_Container.class);
		AdditionalVehicleName_AutoNumberContainer = PageFactory.initElements(driver, SSEC_2119_AdditionalVehicleName_AutoNumberContainer.class);
		
		driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);

		verificationCode = null;
		vCode = null;
		veriCode = null;
		NewWindow = null;
		CaseCreated = null;
		CaseNumber = null;

	}

    @Then("^User navigates to Additional Vehicles from Quick Links$")
    public void User_navigates_to_Additional_Vehicles_from_Quick_Links() throws Exception{
    	
    	ActionHandler.click(AdditionalVehicleName_AutoNumberContainer.AdditionalVehicle);
        ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Click on Additional Vehicle tab");
    }
    
    @And("^Creates a new Additional Vehicle$")
    public void Creates_a_New_Additional_Vehicles() throws Exception{
    	
    	ActionHandler.click(AdditionalVehicleName_AutoNumberContainer.NewBtnAdditionalVehicle);
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Click on New Button of Additional Vehicle tab");
        
    	ActionHandler.click(AdditionalVehicleName_AutoNumberContainer.SaveEditAdditionalVehicle);
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Click on Save button of Additional Vehicle tab");
        
    }
    
    @Then("^Verify new additional vehicle is created$")
    public void Verify_new_additional_vehicle_is_Created() throws Exception{
    	
    	ActionHandler.click(AdditionalVehicleName_AutoNumberContainer.AdditionalVehicleVerify);
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Additional Vehicle tab");
        
    }
    
    @Then("^Verify that new additional vehicle created is in the sequential form starting with SVO$")
    public void Verify_that_new_additional_vehicle_created_is_in_the_sequential_form_starting_with_SVO() throws Exception{
    	
    	String AdditionalVehicleRefNum;
    	
    	AdditionalVehicleRefNum = AdditionalVehicleName_AutoNumberContainer.AdditionalVehicleRefNum.getText();
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Verify the additional vehicle created has name in the the sequential form");
        
    }
    
    @Then("^Verify that the opportunity field has the opportunity name$")
    public void Verify_that_the_opportunity_field_has_the_opportunity_name() throws Exception{

    	String OpportunityText;
    	
    	OpportunityText = AdditionalVehicleName_AutoNumberContainer.OpportunityText.getText();
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Verify Opportunity name of Additional Vehicle tab");
        
    }
    
    @Then("^User clicks on Edit button$")
    public void User_clicks_on_Edit_button() throws Exception{
    	
    	ActionHandler.click(AdditionalVehicleName_AutoNumberContainer.DropDownAdditionalVehicle);
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Click on Drop down button of Additional Vehicle tab");
        
    	ActionHandler.click(AdditionalVehicleName_AutoNumberContainer.EditOptionAdditionalVehicle);
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Select Edit option from Additional Vehicle tab");
        
    }

    @And("^Verify that Additional Vehicle Auto Number is uneditable$")
    public void Verify_that_Additional_Vehicle_Auto_Number_is_uneditable() throws Exception{
    	
    	VerifyHandler.verifyElementPresent(AdditionalVehicleName_AutoNumberContainer.AdditionalVehicleRefNum);
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Verify Additional Vehicle Refernce Number is uneditable");
        
    	ActionHandler.click(AdditionalVehicleName_AutoNumberContainer.CancelEditAdditionalVehicle);
    	ActionHandler.wait(2);
        CommonFunctions.attachScreenshot();
        Reporter.addStepLog("Click on Cancel button");
        
    }

    
    




}
