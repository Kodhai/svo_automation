package com.jlr.svo.tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.containers.SSEC_1238_CaseLifecycle_Container;
import com.jlr.svo.containers.SSEC_2293_ClassicPartsOwnerHelpForm_Container;
import com.jlr.svo.containers.SSEC_2432_ClassicPartsOwnerHelpform_AnalysisDevlopmentContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.And;

public class SSEC_2432_ClassicPartsOwnerHelpformAnalysisDevlopment_Test extends TestBaseCC {

	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory.getLogger(SVOWorkOrderProgrammeField_Test.class.getName());
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	com.jlr.svo.containers.SSEC_2293_ClassicPartsOwnerHelpForm_Container SSEC_2293_ClassicPartsOwnerHelpForm_Container = PageFactory
			.initElements(driver, SSEC_2293_ClassicPartsOwnerHelpForm_Container.class);
	SSEC_1238_CaseLifecycle_Container CaseLifecycle_Container = PageFactory.initElements(driver,
			SSEC_1238_CaseLifecycle_Container.class);
	SSEC_2432_ClassicPartsOwnerHelpform_AnalysisDevlopmentContainer SSEC_2432_ClassicPartsOwnerHelpform_AnalysisDevlopmentContainer = PageFactory.initElements(driver, SSEC_2432_ClassicPartsOwnerHelpform_AnalysisDevlopmentContainer.class);

	public static String verificationCode;
	public static String vCode;

	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	public void onStart() {

		setupTest("SVOTest");
		driver = getDriver();

		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		SSEC_2293_ClassicPartsOwnerHelpForm_Container = PageFactory.initElements(driver,
				SSEC_2293_ClassicPartsOwnerHelpForm_Container.class);

		SSEC_2432_ClassicPartsOwnerHelpform_AnalysisDevlopmentContainer = PageFactory.initElements(driver, SSEC_2432_ClassicPartsOwnerHelpform_AnalysisDevlopmentContainer.class);

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

	}

	@And("^User Verify the Salesforce and assigned to the Queue$")
	public void User_Verify_the_Salesforce_and_assigned_to_the_Queue() throws Throwable {

		VerifyHandler.verifyElementPresent(SSEC_2432_ClassicPartsOwnerHelpform_AnalysisDevlopmentContainer.queue);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

	}

	@And("^User Verify that the record successfully deleted$")
	public void User_Verify_that_the_record_successfully_deleted() throws Throwable {

		ActionHandler.click(SSEC_2432_ClassicPartsOwnerHelpform_AnalysisDevlopmentContainer.Delete);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click the delete button");

		ActionHandler.click(SSEC_2432_ClassicPartsOwnerHelpform_AnalysisDevlopmentContainer.deletebtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate back to the opportunity page");

//		VerifyHandler.verifyElementPresent(SSEC_2432_ClassicPartsOwnerHelpform_AnalysisDevlopmentContainer.delete);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

	}

	@And("^User is able to edit the record details$")
	public void User_is_able__to_edit_the_record_details() throws Throwable {

		ActionHandler.wait(4);
		ActionHandler.click(SSEC_2432_ClassicPartsOwnerHelpform_AnalysisDevlopmentContainer.Edit);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SSEC_2432_ClassicPartsOwnerHelpform_AnalysisDevlopmentContainer.Firstname);
		ActionHandler.wait(1);
		ActionHandler.setText(SSEC_2432_ClassicPartsOwnerHelpform_AnalysisDevlopmentContainer.Firstname, "Test9");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SSEC_2432_ClassicPartsOwnerHelpform_AnalysisDevlopmentContainer.Save);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
	}

	@And("^User fill all the mandatory details with upload picture$")
	public void User_fill_all_the_mandatory_details_with_upload_picture() throws Throwable {
		if (VerifyHandler.verifyElementPresent(SSEC_2293_ClassicPartsOwnerHelpForm_Container.Allowall)) {

			ActionHandler.click(SSEC_2293_ClassicPartsOwnerHelpForm_Container.Allowall);
			CommonFunctions.attachScreenshot();

		}

		double randomNumber = getRandomIntegerBetweenRange(1, 1000);

		ActionHandler.click(SSEC_2293_ClassicPartsOwnerHelpForm_Container.FirstName);
		ActionHandler.wait(1);
		ActionHandler.setText(SSEC_2293_ClassicPartsOwnerHelpForm_Container.FirstName, "Test");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SSEC_2293_ClassicPartsOwnerHelpForm_Container.Lastname);
		ActionHandler.wait(1);
		ActionHandler.setText(SSEC_2293_ClassicPartsOwnerHelpForm_Container.Lastname, "SVO" + randomNumber);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SSEC_2293_ClassicPartsOwnerHelpForm_Container.Town);
		ActionHandler.wait(1);
		ActionHandler.setText(SSEC_2293_ClassicPartsOwnerHelpForm_Container.Town, "India");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SSEC_2293_ClassicPartsOwnerHelpForm_Container.Email);
		ActionHandler.wait(1);
		ActionHandler.setText(SSEC_2293_ClassicPartsOwnerHelpForm_Container.Email, "enquiryRequest2022@gmail.com");

//		ActionHandler.setText(SSEC_2293_ClassicPartsOwnerHelpForm_Container.Email,
//				"Test" + randomNumber + "@gmail.com");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SSEC_2293_ClassicPartsOwnerHelpForm_Container.PhoneNumber);
		ActionHandler.wait(1);
		ActionHandler.setText(SSEC_2293_ClassicPartsOwnerHelpForm_Container.PhoneNumber, "09" + randomNumber);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SSEC_2293_ClassicPartsOwnerHelpForm_Container.VINforOwnerHelpForm);
		ActionHandler.wait(1);
		ActionHandler.setText(SSEC_2293_ClassicPartsOwnerHelpForm_Container.VINforOwnerHelpForm, "07" + randomNumber);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SSEC_2293_ClassicPartsOwnerHelpForm_Container.Mileage);
		ActionHandler.wait(1);
		ActionHandler.setText(SSEC_2293_ClassicPartsOwnerHelpForm_Container.Mileage, "5" + randomNumber);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SSEC_2293_ClassicPartsOwnerHelpForm_Container.Symptom);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act1.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SSEC_2293_ClassicPartsOwnerHelpForm_Container.Faultlocation);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		Actions act2 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act1.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SSEC_2293_ClassicPartsOwnerHelpForm_Container.Description);
		ActionHandler.wait(1);
		ActionHandler.setText(SSEC_2293_ClassicPartsOwnerHelpForm_Container.Description, "TEST");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SSEC_2432_ClassicPartsOwnerHelpform_AnalysisDevlopmentContainer.Upload);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		CommonFunctions.uploadFile("image.png");
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Upload files button");

		ActionHandler.click(SSEC_2293_ClassicPartsOwnerHelpForm_Container.Submit);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
	}

}