package com.jlr.svo.tests;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.autotest.utils.Config;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.constants.Constants;
import com.jlr.svo.containers.SSEC_1238_CaseLifecycle_Container;
import com.jlr.svo.containers.SVOAccountsContainer;
import com.jlr.svo.containers.SVOAdditionalVehicleContainer;
import com.jlr.svo.containers.SVOEnquiryContainer;
import com.jlr.svo.containers.SVOItemToApproveContainer;
import com.jlr.svo.containers.SVOOpportunityContainer;
import com.jlr.svo.containers.SVO_CustomerResponsesContainer;
import com.jlr.svo.utilities.CommonFunctions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SSEC_1238_CaseLifecycle_Test extends TestBaseCC {

	public ExtentTest extentLogger;
	private WebDriver driver = getDriver();
	CommonFunctions commonFunctions = new CommonFunctions(driver);
	private static final Logger LOGGER = LoggerFactory.getLogger(SSEC_1238_CaseLifecycle_Test.class.getName());
	JavaScriptUtil javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);

	SVOAccountsContainer SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
	SVOItemToApproveContainer SVOItemToapproveContainer = PageFactory.initElements(driver,
			SVOItemToApproveContainer.class);
	SVOOpportunityContainer SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
	SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	SVOEnquiry SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
	SVOAdditionalVehicleContainer SVOAdditionalvehicleContainer = PageFactory.initElements(driver,
			SVOAdditionalVehicleContainer.class);
	SSEC_1238_CaseLifecycle_Container CaseLifecycle_Container = PageFactory.initElements(driver,
			SSEC_1238_CaseLifecycle_Container.class);
	SVO_CustomerResponsesContainer SVO_CustomerResponsesContainer = PageFactory.initElements(driver,
			SVO_CustomerResponsesContainer.class);

	public static String verificationCode;
	public static String vCode;
	public static String veriCode;
	public static String CaseNumber;

	public static double getRandomIntegerBetweenRange(double min, double max) {
		double x = (int) (Math.random() * ((max - min) + 1)) + min;
		return x;
	}

	public void onStart() {

		setupTest("SVOTest");
		driver = getDriver();
		javaScriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		CaseLifecycle_Container = PageFactory.initElements(driver, SSEC_1238_CaseLifecycle_Container.class);
		SVOAccountsContainer = PageFactory.initElements(driver, SVOAccountsContainer.class);
		SVOItemToapproveContainer = PageFactory.initElements(driver, SVOItemToApproveContainer.class);
		SVO_OpportunityContainer = PageFactory.initElements(driver, SVOOpportunityContainer.class);
		SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
		SVOenquiry = PageFactory.initElements(driver, SVOEnquiry.class);
		SVOAdditionalvehicleContainer = PageFactory.initElements(driver, SVOAdditionalVehicleContainer.class);
		SVO_CustomerResponsesContainer = PageFactory.initElements(driver, SVO_CustomerResponsesContainer.class);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		verificationCode = null;
		vCode = null;
		veriCode = null;
		CaseNumber = null;

	}

	public String checkEmailForClassicPartsUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(493, 499);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	@Given("^Access to Salesforce Portal as Classic Parts user with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void Access_Salesforce_Portal_Classic_Parts_with_username_password(String userName, String password)
			throws Exception {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		onStart();
		driver = getDriver();
		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailForClassicPartsUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	@Given("^User Access to SVO Portal as Classic Parts user with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void User_Access_SVO_Portal_Classic_Parts_with_username_password(String userName, String password)
			throws Exception {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		driver = getDriver();
		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailForClassicPartsUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	public String checkEmailForCustomerServicesUser() throws Exception {
		String parentWindow = driver.getWindowHandle();
		System.out.println("Window Id for SF SVO is = " + parentWindow);

		String userName = Config.getPropertyValue("Outlook_UserName");
		String password = Config.getPropertyValue("Outlook_Password");

		driver.get(Constants.outlookURL);

		Reporter.addStepLog("User need to access Outlook account to get verification code");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.outlookSignin)) {

			ActionHandler.click(SVOEnquiryContainer.outlookSignin);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookUsername, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookNext);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.outlookPassword, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookSignInBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.outlookConfirmYes);
			CommonFunctions.attachScreenshot();
		}

		String outlookWindow = driver.getWindowHandle();
		ActionHandler.click(SVOEnquiryContainer.outlookIcon);
		ActionHandler.wait(12);
		Set<String> outlookWindows = driver.getWindowHandles();
		Iterator<String> i = outlookWindows.iterator();
		while (i.hasNext()) {
			String mailWindow = i.next();
			if (!outlookWindow.equalsIgnoreCase(mailWindow)) {
				driver.switchTo().window(mailWindow);
				ActionHandler.click(SVOEnquiryContainer.outlookEmail);

				CommonFunctions.attachScreenshot();
				verificationCode = SVOEnquiryContainer.outlookVerificationCode.getText();
				vCode = verificationCode.substring(493, 499);
				System.out.println("Verification Code is = " + vCode);
				driver.close();
				driver.switchTo().window(outlookWindow);
			}
		}
		CommonFunctions.attachScreenshot();
		return vCode;
	}

	@Given("^Access to SVO Portal as Customer Service user with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void Access_SVO_Portal_Custmer_Service_with_username_password(String userName, String password)
			throws Exception {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		onStart();
		driver = getDriver();
		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailForCustomerServicesUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	@Given("^User Access to SVO Portal as Customer Service user with Username \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void User_Access_SVO_Portal_Custmer_Service_with_username_password(String userName, String password)
			throws Exception {
		String un[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(un[0], un[1], un[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Beginning scenario execution...");
		Reporter.addStepLog("User access SF SVO Portal");

		driver = getDriver();
		driver.get(Constants.SVOURL);

		Reporter.addStepLog("User Logins to SVO Portal");
		ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(SVOEnquiryContainer.loginBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User requires to enter Verification code received in an email");

		if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.verificationCode)) {

			Reporter.addStepLog("Please Open recently received email from Salesforce");

			vCode = checkEmailForCustomerServicesUser();
			System.out.println("Verification code is = " + vCode);

			driver.get(Constants.SVOURL);
			Reporter.addStepLog("User Logins to SVO Portal");

			ActionHandler.setText(SVOEnquiryContainer.userNameTextBox, userName);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.passwordTextBox, password);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.loginBtn);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(SVOEnquiryContainer.vCodeTextBox, vCode);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(SVOEnquiryContainer.verifyBtn);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User logins to SVO Successfully");

		} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.mobileNumText)) {

			Reporter.addStepLog("User asks to enter mobile number");
			CommonFunctions.attachScreenshot();
			if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.remindMeLater)) {

				ActionHandler.click(SVOEnquiryContainer.remindMeLater);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else if (VerifyHandler.verifyElementPresent(SVOEnquiryContainer.notRegister)) {

				ActionHandler.click(SVOEnquiryContainer.notRegister);
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
			} else {
				ActionHandler.wait(15);
				CommonFunctions.attachScreenshot();
				VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
				Reporter.addStepLog("User navigate to SVO Home Page successfully");
			}
		} else {
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			if (!VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText)) {
				SVOenquiry.navigateToSVO();

			}
			VerifyHandler.verifyElementPresent(SVOEnquiryContainer.SVOText);
			Reporter.addStepLog("User navigate to SVO Home Page successfully");
		}
	}

	@Then("^User create new case with record type \"([^\"]*)\"$")
	public void user_create_new_case_with_record_type(String recordtype) throws Throwable {

		String s[] = recordtype.split(",");
		recordtype = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(CaseLifecycle_Container.CasesTab);
		ActionHandler.wait(1);
		Reporter.addStepLog("User navigates to cases tab");

		ActionHandler.click(CaseLifecycle_Container.NewBtn);
		ActionHandler.wait(2);
		ActionHandler.click(driver.findElement(By.xpath(CaseLifecycle_Container.CaseRecordType(recordtype))));
		CommonFunctions.attachScreenshot();
		ActionHandler.click(CaseLifecycle_Container.NextBtn);
		Reporter.addStepLog("User chooses record type for an case");

		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(1);
		ActionHandler.click(CaseLifecycle_Container.ChassisNumber);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseLifecycle_Container.ChassisNumber, "123456");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters chassis number for case");

		ActionHandler.click(CaseLifecycle_Container.SaveBtn);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves case");

		CaseNumber = CaseLifecycle_Container.CaseNumber.getText();
		Reporter.addStepLog("Case number is = " + CaseNumber);
	}

	@Then("^User close the case with status \"([^\"]*)\"$")
	public void user_close_the_case_with_status(String caseclosurestatus) throws Throwable {

		String s[] = caseclosurestatus.split(",");
		caseclosurestatus = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		ActionHandler.wait(5);
		String CaseOrigin = CaseLifecycle_Container.CaseOrigin.getText();
		Reporter.addStepLog("Case origin is = " + CaseOrigin);

		driver.navigate().refresh();
		ActionHandler.wait(10);
		javaScriptUtil.clickElementByJS(CaseLifecycle_Container.MarkCaseLifecycleAsComplete);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on mark case lifecycle as complete");

		javaScriptUtil.clickElementByJS(CaseLifecycle_Container.MarkCaseLifecycleAsComplete);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on mark case lifecycle as complete");
		javaScriptUtil.clickElementByJS(CaseLifecycle_Container.MarkCaseLifecycleAsComplete);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on mark case lifecycle as complete");
		/*
		 * ActionHandler.click(CaseLifecycle_Container.ClosedStatus);
		 * ActionHandler.wait(2);
		 * ActionHandler.click(CaseLifecycle_Container.MarkCurrentCaseLifecycle);
		 * ActionHandler.wait(2);
		 */
		ActionHandler.click(CaseLifecycle_Container.CaseClosureReasonDropdown);
		ActionHandler.wait(2);
		ActionHandler.click(driver.findElement(By.xpath(CaseLifecycle_Container.CaseRecordType(caseclosurestatus))));
		CommonFunctions.attachScreenshot();
		ActionHandler.click(CaseLifecycle_Container.DoneBtn);
		Reporter.addStepLog("User close the case with case closure reason");
	}

	@Then("^Verify that case closure status set to be closed under case closure details$")
	public void verify_that_case_closure_status_set_to_be_closed_under_case_closure_details() throws Throwable {
		VerifyHandler.verifyElementPresent(CaseLifecycle_Container.CaseClosureStatus);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies that case closure status is set to be closed");

		VerifyHandler.verifyElementPresent(CaseLifecycle_Container.CaseClosureLifecycle);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies that case closure lifecycle is set to be closed");
	}

	@Then("^User edits case closure status \"([^\"]*)\"$")
	public void user_edits_case_closure_status(String status) throws Throwable {

		String s[] = status.split(",");
		status = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		ActionHandler.wait(3);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		// javaScriptUtil.clickElementByJS(CaseLifecycle_Container.CaseEditBtn);
		javaScriptUtil.clickElementByJS(CaseLifecycle_Container.EditCaseClosureBtn);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.scrollToView(CaseLifecycle_Container.CaseClosureStatusDetails);
		ActionHandler.wait(2);
		ActionHandler.click(CaseLifecycle_Container.CaseClosureStatusDetails);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		ActionHandler.click(driver.findElement(By.xpath(CaseLifecycle_Container.StatusSelection(status))));
		CommonFunctions.attachScreenshot();
		ActionHandler.click(CaseLifecycle_Container.SaveEditBtn);
		Reporter.addStepLog("User edit case closure status");

	}

	@Then("^Verify that updated case closure status details$")
	public void verify_that_updated_case_closure_status_details() throws Throwable {

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);

		VerifyHandler.verifyElementPresent(CaseLifecycle_Container.CaseClosureStatus);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies that case closure status is set to be On Hold");
	}

	@Then("^User move the case to open status$")
	public void user_move_the_case_to_open_status() throws Throwable {

		javaScriptUtil.clickElementByJS(CaseLifecycle_Container.MarkCaseLifecycleAsComplete);
		ActionHandler.wait(1);
		Reporter.addStepLog("User clicks on mark case lifecycle as complete");
	}

	@Then("^User move the case to inprogres status$")
	public void user_move_the_case_to_inprogres_status() throws Throwable {
		javaScriptUtil.clickElementByJS(CaseLifecycle_Container.MarkCaseLifecycleAsComplete);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on mark case lifecycle as complete");

		javaScriptUtil.clickElementByJS(CaseLifecycle_Container.MarkCaseLifecycleAsComplete);
		ActionHandler.wait(10);
		Reporter.addStepLog("User clicks on mark case lifecycle as complete");
	}

	@Then("^User is able to move the case back to new lifecycle after closed successfully \"([^\"]*)\"$")
	public void user_is_able_to_move_the_case_back_to_new_lifecycle_after_closed_successfully(String lifecycle)
			throws Throwable {
		String s[] = lifecycle.split(",");
		lifecycle = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		ActionHandler.wait(3);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);

		javaScriptUtil.clickElementByJS(CaseLifecycle_Container.EditCaseClosureBtn);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.scrollToView(CaseLifecycle_Container.CaseClosureLifecycleDetails);
		ActionHandler.wait(2);
		ActionHandler.click(CaseLifecycle_Container.CaseClosureLifecycleDetails);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		ActionHandler.click(driver.findElement(By.xpath(CaseLifecycle_Container.StatusSelection(lifecycle))));
		CommonFunctions.attachScreenshot();
		ActionHandler.click(CaseLifecycle_Container.SaveEditBtn);
		Reporter.addStepLog("User edit case closure status");

		VerifyHandler.verifyElementPresent(CaseLifecycle_Container.CaseClosureLifecycle);
		CommonFunctions.attachScreenshot();
		String CaseLifecycle = CaseLifecycle_Container.CaseClosureLifecycle.getText();
		Reporter.addStepLog(
				"User is able to move the case back to " + CaseLifecycle + " lifecycle after case has been closed");

	}

	@Then("^User is able to edit case closure reason after case has been closed \"([^\"]*)\"$")
	public void user_is_able_to_edit_case_closure_reason_after_case_has_been_closed(String caseclosurereason)
			throws Throwable {
		String s[] = caseclosurereason.split(",");
		caseclosurereason = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		ActionHandler.wait(3);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);

		javaScriptUtil.clickElementByJS(CaseLifecycle_Container.EditCaseClosureBtn);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);

		ActionHandler.scrollToView(CaseLifecycle_Container.CaseClosureReasonDetails);
		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(CaseLifecycle_Container.CaseClosureReasonDetails);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(CaseLifecycle_Container.StatusSelection(caseclosurereason))));
		CommonFunctions.attachScreenshot();
		ActionHandler.click(CaseLifecycle_Container.SaveEditBtn);
		Reporter.addStepLog("User edit case closure status");

		VerifyHandler.verifyElementPresent(CaseLifecycle_Container.CaseClosureReason);
		CommonFunctions.attachScreenshot();
		String CaseClosureReasn = CaseLifecycle_Container.CaseClosureReason.getText();
		Reporter.addStepLog(
				"Verifies that User edits case closure reason to " + CaseClosureReasn + " after case has been closed");

	}

	@Then("^User is not able to edit case closure status once the case has been closed status \"([^\"]*)\"$")
	public void user_is_not_able_to_edit_case_closure_status_once_the_case_has_been_closed_status(String status)
			throws Throwable {
		String s[] = status.split(",");
		status = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		ActionHandler.wait(3);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		// javaScriptUtil.clickElementByJS(CaseLifecycle_Container.CaseEditBtn);
		javaScriptUtil.clickElementByJS(CaseLifecycle_Container.EditCaseClosureBtn);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.scrollToView(CaseLifecycle_Container.CaseClosureStatusDetails);
		ActionHandler.wait(2);
		ActionHandler.click(CaseLifecycle_Container.CaseClosureStatusDetails);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(CaseLifecycle_Container.StatusSelection(status))));
		CommonFunctions.attachScreenshot();
		ActionHandler.click(CaseLifecycle_Container.SaveEditBtn);
		Reporter.addStepLog("User edit case closure status");

		VerifyHandler.verifyElementPresent(CaseLifecycle_Container.CaseClosureStatus);
		CommonFunctions.attachScreenshot();
		String CaseStatus = CaseLifecycle_Container.CaseClosureStatus.getText();
		Reporter.addStepLog("Verifies that case closure status is not edited from " + CaseStatus + " stage");
	}

	@When("^User select customer service case under respective queue \"([^\"]*)\"$")
	public void user_select_customer_service_case_under_respective_queue(String queue) throws Throwable {

		String s[] = queue.split(",");
		queue = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(CaseLifecycle_Container.CasesTab);
		ActionHandler.wait(1);
		Reporter.addStepLog("User navigates to cases tab");

		ActionHandler.wait(2);
		ActionHandler.click(SVO_CustomerResponsesContainer.SelectOpportunityListView);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on select list view tab");

		ActionHandler.click(CaseLifecycle_Container.SelectCustomerServiceQueue);
		// ActionHandler.click(driver.findElement(By.xpath(CaseLifecycle_Container.QueueSelection(queue))));
		ActionHandler.wait(3);
		Reporter.addStepLog("User chooses to view selected to parts and technical case list");
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User Verifies that the new case is created automatically on SVO Portal");

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstOpportunityLink);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects the case that is created automatically on salesforce");
	}

	@When("^Enter VIN number$")
	public void enter_VIN_number() throws Throwable {

		javaScriptUtil.clickElementByJS(CaseLifecycle_Container.EditCaseClosureBtn);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.scrollToView(CaseLifecycle_Container.VINNumberOnCaseDetailsPage);
		ActionHandler.wait(1);
		ActionHandler.click(CaseLifecycle_Container.VINNumberOnCaseDetailsPage);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseLifecycle_Container.VINNumberOnCaseDetailsPage, "123456");
		ActionHandler.wait(3);
		ActionHandler.click(CaseLifecycle_Container.SaveEditBtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters VIN number for case");
	}

	@When("^Verify that error message while closing the case without VIN number$")
	public void Verify_that_error_message_while_closing_the_case_without_VIN_number() throws Throwable {

		javaScriptUtil.clickElementByJS(CaseLifecycle_Container.MarkCaseLifecycleAsComplete);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on mark case lifecycle as complete");

		VerifyHandler.verifyElementPresent(CaseLifecycle_Container.ErrorMessageForWithoutVinNumber);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verify that error message while closing the case without VIN number");
	}

	@Given("^Access to the Classic Parts portal$")
	public void access_to_the_Classic_Parts_portal() throws Throwable {
		onStart();

		driver.get("https://" + "iweb_26b06a" + ":" + "PJuR7ifMuPqr" + "@" + Constants.ClassicParts);
		ActionHandler.wait(1);
		Reporter.addStepLog("User access to Classic Parts portal");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(CaseLifecycle_Container.AllowCookies);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User accepts cookie policies");

	}

	@Then("^Submit Contact Us form with Email \"([^\"]*)\" First name \"([^\"]*)\" Last name \"([^\"]*)\" Country \"([^\"]*)\" Phone number \"([^\"]*)\" VIN number \"([^\"]*)\"$")
	public void Submit_Contact_Us_form(String email, String first, String last, String country, String phone,
			String vin) throws Throwable {

		String em[] = email.split(",");
		email = CommonFunctions.readExcelMasterData(em[0], em[1], em[2]);

		String fname[] = first.split(",");
		first = CommonFunctions.readExcelMasterData(fname[0], fname[1], fname[2]);

		String lname[] = last.split(",");
		last = CommonFunctions.readExcelMasterData(lname[0], lname[1], lname[2]);

		String cty[] = country.split(",");
		country = CommonFunctions.readExcelMasterData(cty[0], cty[1], cty[2]);

		String num[] = phone.split(",");
		phone = CommonFunctions.readExcelMasterData(num[0], num[1], num[2]);

		String v[] = vin.split(",");
		vin = CommonFunctions.readExcelMasterData(v[0], v[1], v[2]);

		// enter first name
		ActionHandler.setText(CaseLifecycle_Container.FirstNameTxtBx, first);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseLifecycle_Container.FirstNameTxtBx, first);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters First name as : " + first);

		// enter last name
		ActionHandler.setText(CaseLifecycle_Container.LastNameTxtBx, last);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Last name as : " + last);

		ActionHandler.wait(1);
		ActionHandler.setText(CaseLifecycle_Container.InputEmailAddress, email);
		ActionHandler.wait(1);
		ActionHandler.setText(CaseLifecycle_Container.InputEmailAddress, email);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters email as : " + email);

		// enter phone number
		ActionHandler.setText(CaseLifecycle_Container.PhoneNumberTxtBx, phone);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		Select select = new Select(driver.findElement(By.xpath(CaseLifecycle_Container.SelectHowCanHelp())));
		select.selectByVisibleText("Parts & Technical Jaguar Enquiry");
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		/*
		 * ActionHandler.click(CaseLifecycle_Container.HelpSelect);
		 * CommonFunctions.attachScreenshot(); ActionHandler.wait(1); Actions act1 = new
		 * Actions(driver); act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		 * ActionHandler.wait(1); act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		 * act1.sendKeys(Keys.ENTER).build().perform();
		 */
		Reporter.addStepLog("User clicks on Help select");

		// enter country
		/*
		 * ActionHandler.setText(CaseLifecycle_Container.CountrySelection, country);
		 * ActionHandler.wait(1); CommonFunctions.attachScreenshot();
		 * ActionHandler.pressEnter(); ActionHandler.wait(1);
		 * Reporter.addStepLog("User enters country as : " + country);
		 */

		ActionHandler.setText(CaseLifecycle_Container.VINNumber, vin);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters the vin number");

		String comment = "test";
		ActionHandler.setText(CaseLifecycle_Container.CommentText, comment);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters the comment text");

		// click on continue
		javaScriptUtil.clickElementByJS(CaseLifecycle_Container.SubmitBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Submit");

		/*
		 * VerifyHandler.verifyElementPresent(CaseLifecycle_Container.
		 * SubmitSuccessfullMessage); ActionHandler.wait(1);
		 * CommonFunctions.attachScreenshot();
		 */
		Reporter.addStepLog("User verifies that contact us form submitted successfully");
	}

	@Then("^User not able to create new case without VIN number record type \"([^\"]*)\"$")
	public void user_not_able_to_create_new_case_without_VIN_number_record_type(String recordtype) throws Throwable {

		String s[] = recordtype.split(",");
		recordtype = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(CaseLifecycle_Container.CasesTab);
		ActionHandler.wait(1);
		Reporter.addStepLog("User navigates to cases tab");

		ActionHandler.click(CaseLifecycle_Container.NewBtn);
		ActionHandler.wait(2);
		ActionHandler.click(driver.findElement(By.xpath(CaseLifecycle_Container.CaseRecordType(recordtype))));
		CommonFunctions.attachScreenshot();
		ActionHandler.click(CaseLifecycle_Container.NextBtn);
		Reporter.addStepLog("User chooses record type for an case");

		ActionHandler.wait(5);
		ActionHandler.click(CaseLifecycle_Container.SaveBtn);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves case");
		ActionHandler.wait(2);
		VerifyHandler.verifyElementPresent(CaseLifecycle_Container.VINNumberErrorMessage);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User not able to create case without VIN number");

		ActionHandler.click(CaseLifecycle_Container.CancelBtn);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

	}

	@Then("^User close the case without case closure reason status \"([^\"]*)\"$")
	public void user_close_the_case_with_without_case_closure_reason_status(String caseclosurestatus) throws Throwable {

		String s[] = caseclosurestatus.split(",");
		caseclosurestatus = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		ActionHandler.wait(5);
		String CaseOrigin = CaseLifecycle_Container.CaseOrigin.getText();
		Reporter.addStepLog("Case origin is = " + CaseOrigin);

		driver.navigate().refresh();
		ActionHandler.wait(10);

		ActionHandler.click(CaseLifecycle_Container.ClosedStatus);
		ActionHandler.wait(2);
		ActionHandler.click(CaseLifecycle_Container.MarkCurrentCaseLifecycle);
		ActionHandler.wait(2);
		Reporter.addStepLog("User close the case without case closure reason");
	}

	@When("^User select case under respective queue \"([^\"]*)\"$")
	public void user_select_case_under_respective_queue(String queue) throws Throwable {

		String s[] = queue.split(",");
		queue = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		ActionHandler.wait(2);
		javaScriptUtil.clickElementByJS(CaseLifecycle_Container.CasesTab);
		ActionHandler.wait(1);
		Reporter.addStepLog("User navigates to cases tab");

		ActionHandler.wait(2);
		ActionHandler.click(SVO_CustomerResponsesContainer.SelectOpportunityListView);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on select list view tab");

		ActionHandler.click(CaseLifecycle_Container.SelectPartsAndTechnicalQueue);
		ActionHandler.wait(3);
		Reporter.addStepLog("User chooses to view selected to parts and technical case list");
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User Verifies that the new case is created automatically on SVO Portal");

		ActionHandler.click(SVOAdditionalvehicleContainer.FirstOpportunityLink);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects the case that is created automatically on salesforce");
	}

}
