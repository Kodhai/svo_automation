package com.jlr.svo.constants;

public class Constants {

	public static final String SVOSCREENSHOTPATH = "C:\\SVO_Screenshots\\";

	public static final String SCREENSHOTFORMAT = "H_mm_ss";

	public static final String Master_Excel_FILE_PATH = System.getProperty("user.dir") + "/Master Data/";

	public static final String SVOURL = "https://jlr-global--jlrglobuat.sandbox.my.salesforce.com/";

	public static final String outlookURL = "https://www.office.com/";

	public static final String Contact_one = "Kodhai priya";

	public static final String email = "pkodhai@jaguarlandrover.com";

	public static final String User1 = "Priya Kodhai";

	public static final String Contact_two = "Mauli Soni";

	public static final String email_two = "msoni3@partner.jaguarlandrover.com";

	public static final String User_two = "Mauli Soni";

	public static final String ClassicParts = "partsdev.jaguarlandroverclassic.com/";

	public static final String Accessories = "partsdev.jaguarlandroverclassic.com/accessories";

	public static final String gmailURL = "https://mail.google.com/mail/u/1/#inbox";

	public static final String JIRA = "https://jira.devops.jlr-apps.com/secure/RapidBoard.jspa?rapidView=4120&projectKey=SSEC&view=planning&issueLimit=100";

	public static final String outlookInbox ="https://outlook.office365.com/mail/";

	public static final String OwnerHelp = "https://parts.jaguarlandroverclassic.com/ownerhelp";
	
	public static final String outlook = "https://outlook.office365.com/mail/";
	
	public static final String Contactus = "file:/C:/Users/prajna.p.pradhan/Downloads/web%20to%20case%20for%20UAT.html";

}