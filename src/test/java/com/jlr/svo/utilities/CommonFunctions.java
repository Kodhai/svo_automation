package com.jlr.svo.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.utils.Config;
import com.jlr.autotest.utils.Utility;
import com.jlr.base.TestBaseCC;
import com.jlr.svo.constants.Constants;
import com.jlr.svo.containers.SVOEnquiryContainer;

public class CommonFunctions extends TestBaseCC {

	public static WebDriver driver = getDriver();
	final SVOEnquiryContainer SVOEnquiryContainer = PageFactory.initElements(driver, SVOEnquiryContainer.class);
	private static final Logger LOGGER = LoggerFactory.getLogger(CommonFunctions.class.getName());
	public static String verificationCode;
	public static String vCode;
	public static String veriCode;
	

	public CommonFunctions(WebDriver commanDriver) {

		driver = commanDriver;
	}

	public static void attachScreenshot() throws IOException {
		ActionHandler.wait(1);
		SimpleDateFormat sdf = new SimpleDateFormat(Constants.SCREENSHOTFORMAT);
		Reporter.addScreenCaptureFromPath(Utility.takeScreenShotWithPathName(
				Constants.SVOSCREENSHOTPATH + Config.getPropertyValue("svo.testscript") + "\\"
						+ Config.getPropertyValue("svo.testscript") + "_" + sdf.format(Utility.fetchCurrentDate())));
		LOGGER.info("Screenshot");

	}

	public static String readExcelMasterData(String sheetName, String Column, String row1) throws Exception {
		XSSFWorkbook wb = null;
		XSSFSheet sh = null;
		String excelData = null;
		try {
			int Row = Integer.parseInt(row1);
			FileInputStream ft = new FileInputStream(new File(Constants.Master_Excel_FILE_PATH + "SVOTestData.xlsx"));
			LOGGER.info(Constants.Master_Excel_FILE_PATH);
			wb = new XSSFWorkbook(ft);
			sh = wb.getSheet(sheetName);

			int totalRows = getRowNum(sh);

			LOGGER.info("Total line of Records : " + totalRows);
			DataFormatter df = new DataFormatter();

			System.out.println("Fetching cell value from " + Column + Row);
			Row row = sh.getRow(Row - 1);
			String cellvalue = df.formatCellValue(row.getCell(CellReference.convertColStringToIndex(Column)));
			excelData = cellvalue;

		} catch (NullPointerException e) {
			LOGGER.info("Exception - Delete blank rows after last line from Excel file :" + e);
		}

		catch (Exception e) {
			LOGGER.info("Exception : " + e);
		}
		return excelData;
	}

	public static int getRowNum(XSSFSheet sh) {
		return sh.getLastRowNum();
	}

	public int getColNum(XSSFSheet sh, int rownum) {
		return sh.getRow(rownum).getLastCellNum();

	}

	public static String selectFutureDate() {
		final DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date currentDate = new Date();
		String currentDateSTR = dateFormat.format(currentDate);
		Calendar c = Calendar.getInstance();
		c.setTime(currentDate);
		c.add(Calendar.DATE, 1);
		c.add(Calendar.MONTH, 1);
		Date currentDatePlusOne = c.getTime();
		String FutureDate = dateFormat.format(currentDatePlusOne);
		System.out.println("Future Date is = " + FutureDate);

		return FutureDate;

	}

	public static String selectFutureDate1() {
		final DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date currentDate = new Date();
		String currentDateSTR = dateFormat.format(currentDate);
		Calendar c = Calendar.getInstance();
		c.setTime(currentDate);
		c.add(Calendar.DATE, 1);
		c.add(Calendar.MONTH, 2);
		Date currentDatePlusOne = c.getTime();
		String FutureDate = dateFormat.format(currentDatePlusOne);
		System.out.println("Future Date is = " + FutureDate);

		return FutureDate;

	}

	public static boolean setCellData(String sheetName, String colNumber, String rowNumb, String value) {
		int rowNum = Integer.parseInt(rowNumb);

		FileInputStream fis = null;
		FileOutputStream fos = null;
		XSSFWorkbook workbook = null;
		XSSFSheet sheet = null;
		XSSFRow row = null;
		XSSFCell cell = null;
		try {
			fis = new FileInputStream(Constants.Master_Excel_FILE_PATH + "SVOTestData.xlsx");
			workbook = new XSSFWorkbook(fis);
			fis.close();

			sheet = workbook.getSheet(sheetName);
			row = sheet.getRow(rowNum);
			if (row == null)
				row = sheet.createRow(rowNum);

			cell = row.getCell(CellReference.convertColStringToIndex(colNumber));
			if (cell == null)
				cell = row.createCell(CellReference.convertColStringToIndex(colNumber));

			cell.setCellValue(value);

			fos = new FileOutputStream(Constants.Master_Excel_FILE_PATH + "SVOTestData.xlsx");
			workbook.write(fos);
			fos.close();
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
		return true;
	}
	
	public static Process uploadFile(String file) throws IOException {
        ActionHandler.wait(1);
        return Runtime.getRuntime().exec(System.getProperty("user.dir") + "/upload.exe" + " "
                + System.getProperty("user.dir") + "\\Files\\" + file);

 

    }

}
